package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2017/8/4.
 */

public class HomeHosDocAdapter extends RecyclerView.Adapter<HomeHosDocAdapter.ViewHolder> {

    private final List<HomeTaoData> mTaoData;
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final String TAG = "HomeHosDocAdapter";
    private boolean isHome = true;

    public HomeHosDocAdapter(Context context, List<HomeTaoData> taoData) {
        this(context, taoData, true);
    }

    public HomeHosDocAdapter(Context context, List<HomeTaoData> taoData, boolean isHome) {
        this.mContext = context;
        this.mTaoData = taoData;
        this.isHome = isHome;
        mInflater = LayoutInflater.from(mContext);
        Log.e(TAG, "mTaoData的个数 === " + mTaoData.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_item_diary_hosdoc, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        View view = holder.itemView;
        ViewGroup.LayoutParams params = view.getLayoutParams();
        if (position == 0) {                      //第一个
            params.width = Utils.dip2px(mContext, 119);
            view.setPadding(Utils.dip2px(mContext, 15), 0, Utils.dip2px(mContext, 6), 0);
        } else if (position == getItemCount() - 1) {        //最后一个
            params.width = Utils.dip2px(mContext, 119);
            view.setPadding(Utils.dip2px(mContext, 6), 0, Utils.dip2px(mContext, 15), 0);
        } else {                            //其他
            params.width = Utils.dip2px(mContext, 110);
            view.setPadding(Utils.dip2px(mContext, 6), 0, Utils.dip2px(mContext, 6), 0);
        }
        view.setLayoutParams(params);

        if (isHome) {
            if (position == getItemCount() - 1) {         //最后一个
                holder.diaryHosdocImg.setVisibility(View.INVISIBLE);
                holder.diaryHosdocPrice.setVisibility(View.INVISIBLE);
                holder.diaryHosdocMore.setVisibility(View.VISIBLE);
            } else {
                initData(holder, position);
            }
        } else {
            initData(holder, position);
        }


    }

    /**
     * 适配数据
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    private void initData(ViewHolder holder, int position) {
        HomeTaoData tuijHosTao = mTaoData.get(position);

        Glide.with(mContext).load(tuijHosTao.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(7))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.diaryHosdocImg);

        holder.diaryHosdocTitle.setText(tuijHosTao.getTitle());
        holder.diaryHosdocPrice.setText("￥" + tuijHosTao.getPrice_discount());
        String member_price = mTaoData.get(position).getMember_price();
        int i = Integer.parseInt(member_price);
        //隐藏plus价格
//        if (i >= 0) {
//            holder.plusVibisity.setVisibility(View.VISIBLE);
//            holder.plusPrcie.setText("¥" + member_price);
//        } else {
//            holder.plusVibisity.setVisibility(View.GONE);
//        }

        //设置大促显示
        if (!TextUtils.isEmpty(tuijHosTao.getImg66())) {
            holder.diaryHosdocImgTag.setVisibility(View.VISIBLE);
        } else {
            holder.diaryHosdocImgTag.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (isHome) {
            return mTaoData.size() + 1;
        } else {
            return mTaoData.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView diaryHosdocImg;
        ImageView diaryHosdocImgTag;
        LinearLayout diaryHosdocMore;
        TextView diaryHosdocTitle;
        TextView diaryHosdocPrice;
        TextView plusPrcie;
        LinearLayout plusVibisity;

        public ViewHolder(View itemView) {
            super(itemView);
            diaryHosdocImg = itemView.findViewById(R.id.item_diary_hosdoc_img);
            diaryHosdocImgTag = itemView.findViewById(R.id.item_diary_hosdoc_img_tag);
            diaryHosdocMore = itemView.findViewById(R.id.item_diary_hosdoc_more);
            diaryHosdocTitle = itemView.findViewById(R.id.item_diary_hosdoc_title);
            diaryHosdocPrice = itemView.findViewById(R.id.item_diary_hosdoc_price);
            plusVibisity = itemView.findViewById(R.id.tao_plus_vibility);
            plusPrcie = itemView.findViewById(R.id.tao_plus_price);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            diaryHosdocMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickMoreListener != null) {
                        Log.e(TAG, "pos == " + mTaoData.size());
                        onItemClickMoreListener.onItemClickMore(mTaoData.size());
                    }
                }
            });
        }
    }

    //item点击事件回调
    public interface OnItemClickListener {
        void onItemClick(View view, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    //查看更多点击事件回调
    public interface OnItemClickMoreListener {
        void onItemClickMore(int pos);
    }

    private OnItemClickMoreListener onItemClickMoreListener;

    public void setOnItemClickMoreListener(OnItemClickMoreListener onItemClickMoreListener) {
        this.onItemClickMoreListener = onItemClickMoreListener;
    }

}