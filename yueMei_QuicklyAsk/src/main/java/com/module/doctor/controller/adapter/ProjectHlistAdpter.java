package com.module.doctor.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.module.doctor.model.bean.GroupDiscData;
import com.module.doctor.view.ProjectDetailListFragment;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

public class ProjectHlistAdpter extends BaseAdapter {

	private final String TAG = "ProjectHlistAdpter";

	private List<GroupDiscData> mGroupData = new ArrayList<GroupDiscData>();
	private Context mContext;
	private LayoutInflater inflater;
	private GroupDiscData groupData;
	ViewHolder viewHolder;

	public ProjectHlistAdpter(Context mContext, List<GroupDiscData> mGroupData) {
		this.mContext = mContext;
		this.mGroupData = mGroupData;
		inflater = LayoutInflater.from(mContext);
	}

	static class ViewHolder {
		public TextView groupNameTV;
	}

	@Override
	public int getCount() {
		return mGroupData.size();
	}

	@Override
	public Object getItem(int position) {
		return mGroupData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "NewApi", "InlinedApi" })
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_tao_pop1, null);
			viewHolder = new ViewHolder();
			viewHolder.groupNameTV = convertView
					.findViewById(R.id.pop_tao_item_name_tv);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		groupData = mGroupData.get(position);

		if (position == ProjectDetailListFragment.mmPosition) {
			viewHolder.groupNameTV.setTextColor(mContext.getResources()
					.getColor(R.color.red_title));
		} else {
			viewHolder.groupNameTV.setTextColor(mContext.getResources()
					.getColor(R.color._33));
		}

		// 填充布局
		if (groupData != null) {
			viewHolder.groupNameTV.setText(groupData.getCate_name());
			// viewHolder.groupNameTV.setTextColor(mContext.getResources()
			// .getColor(R.color.tabfontbackgroud));
		}

		return convertView;
	}

	public void add(List<GroupDiscData> infos) {
		mGroupData.addAll(infos);
	}
}
