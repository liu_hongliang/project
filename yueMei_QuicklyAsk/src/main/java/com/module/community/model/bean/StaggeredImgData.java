package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2018/7/16.
 */
public class StaggeredImgData {
    private String img;
    private String width;
    private String height;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
