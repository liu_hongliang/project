package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.quicklyask.activity.R;
import com.module.doctor.controller.activity.WantBeautifulActivity548;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 400页
 * 
 * @author Rubin
 * 
 */
public class Page400Activity extends BaseActivity {

	private Context mContext;

	@BindView(id = R.id.page400_back, click = true)
	private RelativeLayout back;// 返回
	@BindView(id = R.id.page400_leave_note, click = true)
	private RelativeLayout leaveNote;// 去留言
	@BindView(id = R.id.page400_to_phone, click = true)
	private ImageView toPhoneYuemie;// 给悦美打电话

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_page400);
	}

	@Override
	protected void initWidget() {
		super.initWidget();
		mContext = Page400Activity.this;
		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						Page400Activity.this.finish();
					}
				});

	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("NewApi")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.page400_back:
			// finish();
			onBackPressed();
			break;
		case R.id.page400_leave_note:
			Intent it = new Intent();
			it.putExtra("po", "2270");
			it.putExtra("hosid", "0");
			it.putExtra("docid", "0");
			it.putExtra("shareid", "0");
			it.putExtra("cityId", "0");
			it.putExtra("partId", "0");
			it.putExtra("partId_two", "0");
			it.setClass(mContext, WantBeautifulActivity548.class);
			startActivity(it);
			break;
		case R.id.page400_to_phone:
			showDialog();
			break;
		}
	}

	void showDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setMessage("     拨打400-056-7118？");
		builder.setPositiveButton("拨打", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				ViewInject.toast("正在拨打中·····");
				Intent it = new Intent(Intent.ACTION_CALL, Uri
						.parse("tel:4000567118"));
				startActivity(it);
			}
		});
		builder.setNegativeButton("取消", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
		builder.create().show();
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
