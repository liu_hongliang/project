package com.module.my.view.view;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * @author 裴成浩
 * @data 2019/10/11
 */
public class EssaySkuDialog extends Dialog {

    private BtnClickListener mBtnClickListener;

    public EssaySkuDialog(Context context) {
        super(context, R.style.mystyle1);
    }

    /**
     * 设置布局
     */
    public void initView(DialogType type, String message) {
        // 指定布局
        this.setContentView(R.layout.essay_sku_dialog_view);

        ImageView topImage = findViewById(R.id.essay_sku_dialog_top_image);
        TextView topTitle = findViewById(R.id.essay_sku_dialog_top_title);
        TextView topContent = findViewById(R.id.essay_sku_dialog_top_content);
        TextView mLeftBtn = findViewById(R.id.essay_sku_dialog_left_btn);
        TextView mRightBtn = findViewById(R.id.essay_sku_dialog_right_btn);

        //提示语设置
        topContent.setText(message);

        switch (type) {
            case TYPE1:
                topImage.setVisibility(View.GONE);
                topTitle.setVisibility(View.GONE);
                ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) topContent.getLayoutParams();
                layoutParams.topMargin = Utils.dip2px(30);
                layoutParams.bottomMargin = Utils.dip2px(30);
                topContent.setLayoutParams(layoutParams);
                mLeftBtn.setText("就不");
                mRightBtn.setText("好的");
                break;
            case TYPE2:
                topImage.setVisibility(View.VISIBLE);
                topTitle.setVisibility(View.VISIBLE);
                topTitle.setText("评价发表完毕");
                mLeftBtn.setText("返回");
                mRightBtn.setText("完善日记");
                break;
            case TYPE3:
                topImage.setVisibility(View.VISIBLE);
                topTitle.setVisibility(View.VISIBLE);
                topTitle.setText("评价发表成功");
                mLeftBtn.setText("以后再说");
                mRightBtn.setText("完善日记");
                break;
        }


        //左边点击事件
        mLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mBtnClickListener != null) {
                    mBtnClickListener.leftBtnClick();
                }
            }
        });

        //右边点击事件
        mRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mBtnClickListener != null) {
                    mBtnClickListener.rightBtnClick();
                }
            }
        });

        setCanceledOnTouchOutside(false);
    }

    public interface BtnClickListener {
        void leftBtnClick();

        void rightBtnClick();
    }

    public void setBtnClickListener(BtnClickListener btnClickListener) {
        mBtnClickListener = btnClickListener;
    }

    public enum DialogType {
        TYPE1, TYPE2, TYPE3
    }
}