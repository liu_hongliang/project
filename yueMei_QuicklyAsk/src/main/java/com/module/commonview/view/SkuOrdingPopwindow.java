package com.module.commonview.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.module.shopping.controller.activity.MakeSureOrderActivity;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoDetailBean;

public class SkuOrdingPopwindow extends PopupWindow {

    private Context mContext;
    private ImageView mImgClose;
    private TextView mYmpriceTxt;
    private TextView mYmprice;
    private TextView mOnlineTxt;
    private TextView mOnline;
    private TextView mDaoyuanTxt;
    private TextView mDaoyuan;
    private TextView mShengyu;
    private TextView mNum;
    private NumberAddSubView mNumberAddSubView;
    private Button mBtnOrding;
    private LinearLayout skuOrdingHead;
    private int mNumber;

    public int getNumber() {
        return mNumber;
    }

    public void setNumber(int number) {
        mNumber = number;
    }

    public SkuOrdingPopwindow(Context context, final String type, TaoDetailBean taoDetailBean, final String source, final String objid, final String flag, final String mU) {

        mContext = context;
        final View view = View.inflate(mContext, R.layout.sku_ording_pop, null);
        setAnimationStyle(R.style.AnimBottom);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setContentView(view);
        update();

        mImgClose = view.findViewById(R.id.sku_ording_close);
        mYmpriceTxt = view.findViewById(R.id.sku_ording_ymprice_txt);
        mYmprice = view.findViewById(R.id.sku_ording_ymprice);
        mOnlineTxt = view.findViewById(R.id.sku_ording_online_txt);
        mOnline = view.findViewById(R.id.sku_ording_online);
        mDaoyuanTxt = view.findViewById(R.id.sku_ording_daoyuan_txt);
        mDaoyuan = view.findViewById(R.id.sku_ording_yaoyuan);
        mShengyu = view.findViewById(R.id.sku_ording_num_txt);
        mNum = view.findViewById(R.id.sku_ording_shengyu);
        mNumberAddSubView = view.findViewById(R.id.sku_ording_numberadd);
        mBtnOrding = view.findViewById(R.id.sku_ording_btn);
        skuOrdingHead = view.findViewById(R.id.goods_param_head);


        //优先级   拼团>秒杀>大促>常规
        final TaoDetailBean.BasedataBean basedata = taoDetailBean.getBasedata();
        final TaoDetailBean.GroupInfoBean groupInfo = taoDetailBean.getGroupInfo();//拼团
        final TaoDetailBean.RegInfoBean regInfo = taoDetailBean.getRegInfo();
        final TaoDetailBean.ProInfoBean proInfo = taoDetailBean.getProInfo();
        final TaoDetailBean.PayPriceBean pay_price = taoDetailBean.getPay_price();
        mNumberAddSubView.setValue(Integer.parseInt(basedata.getStart_number()));
        mNumberAddSubView.setMinValue(Integer.parseInt(basedata.getStart_number()));



        //        type 1 正常下单 2，拼团
        //         flag 0 参团 1发起拼团
        if ("1".equals(type)) {
            mYmpriceTxt.setText("悦美价");
            mYmprice.setText("¥" + pay_price.getPayPrice());
            mOnline.setText("¥" + pay_price.getDingjin());
            mDaoyuan.setText("¥" + pay_price.getHos_price());
            String number = basedata.getNumber();
            int groupNumber = Integer.parseInt(number);
            mNumberAddSubView.setMaxValue(groupNumber);
        } else {
            String is_group = groupInfo.getIs_group();
            if ("1".equals(is_group)) {
                mYmpriceTxt.setText("拼团价");
                String group_price = groupInfo.getGroup_price();
                String group_dingjin = groupInfo.getGroup_dingjin();
                mYmprice.setText("¥" + group_price);
                mOnline.setText("¥" + group_dingjin);
                String group_number = groupInfo.getGroup_number();
                int groupNumber = Integer.parseInt(group_number);
                mNumberAddSubView.setMaxValue(groupNumber);
                int groupPrice = Integer.parseInt(group_price);
                int groupDingjin = Integer.parseInt(group_dingjin);
                int daoyuan = groupPrice - groupDingjin;
                mDaoyuan.setText("¥" + daoyuan);
                String show_str = basedata.getShow_str();
                if (!TextUtils.isEmpty(show_str)){
                    mNum.setVisibility(View.VISIBLE);
                    mNum.setText(show_str);
                }else {
                    mNum.setVisibility(View.GONE);
                }
            } else {
                if ("1".equals(regInfo.getIs_seg())) {
                    mYmpriceTxt.setText("秒杀价");
                    mYmprice.setText("¥" + pay_price.getPayPrice());
                    String show_str = regInfo.getShow_str();
                    if (!TextUtils.isEmpty(show_str)){
                        mNum.setVisibility(View.VISIBLE);
                        mNum.setText(show_str);
                    }else {
                        mNum.setVisibility(View.GONE);
                    }
                } else {
                    if ("1".equals(proInfo.getIsBigPromotion())) {
                        mYmpriceTxt.setText("大促价");
                        mYmprice.setText("¥" + pay_price.getPayPrice());
                        String show_str = proInfo.getShow_str();
                        if (!TextUtils.isEmpty(show_str)){
                            mNum.setVisibility(View.VISIBLE);
                            mNum.setText(show_str);
                        }else {
                            mNum.setVisibility(View.GONE);
                        }
                    }
                }
                String group_number = basedata.getNumber();
                int groupNumber = Integer.parseInt(group_number);
                mNumberAddSubView.setMaxValue(groupNumber);
                mOnline.setText("¥" + pay_price.getDingjin());
                mDaoyuan.setText("¥" + pay_price.getHos_price());

            }
        }

        mBtnOrding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MakeSureOrderActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("tao_id", basedata.getId());
                bundle.putString("number", mNumberAddSubView.getValue() + "");
                bundle.putString("source", source);
                bundle.putString("objid", objid);
                bundle.putString("buy_for_cart", "0");
                bundle.putString("u", mU);
                if ("2".equals(type) && groupInfo.getGroup().size() != 0&&"0".equals(flag)) {
                    bundle.putString("group_id", groupInfo.getGroup().get(mNumber).getGroup_id());
                    Log.e("SkuOrdingPopwindow",mNumber+"mnumber========>"+groupInfo.getGroup().get(mNumber).getGroup_id());
                }
                if ("2".equals(type) || "1".equals(flag)){
                    bundle.putString("is_group", "1");
                }else {
                    bundle.putString("is_group", "0");
                }
                intent.putExtra("data", bundle);
                mContext.startActivity(intent);
                dismiss();
            }
        });
        mImgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        skuOrdingHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mNumberAddSubView.setOnButtonClickListenter(new NumberAddSubView.OnButtonClickListenter() {
            @Override
            public void onButtonAddClick(int value, boolean b) {
                String is_group = groupInfo.getIs_group();
                if ("1".equals(is_group)){
                    String group_price = groupInfo.getGroup_price();
                    String group_dingjin = groupInfo.getGroup_dingjin();
                    int groupPrice = Integer.parseInt(group_price);
                    int groupDingjin = Integer.parseInt(group_dingjin);
                    mYmprice.setText("¥" + groupPrice*(value));
                    mOnline.setText("¥" + groupDingjin*(value));
                    int daoyuan = groupPrice - groupDingjin;
                    mDaoyuan.setText("¥" + daoyuan*(value));
                }else {
                    if ("1".equals(regInfo.getIs_seg())){
                        String payPrice = pay_price.getPayPrice();
                        int payPrice1 = Integer.parseInt(payPrice);
                        mYmprice.setText("¥" + payPrice1*(value));
                    }else {
                        if ("1".equals(proInfo.getIsBigPromotion())) {
                            String payPrice = pay_price.getPayPrice();
                            int payPrice1 = Integer.parseInt(payPrice);
                            mYmprice.setText("¥" + payPrice1*(value));
                        }
                    }
                    String dingjin = pay_price.getDingjin();
                    int dingjin1 = Integer.parseInt(dingjin);

                    String hos_price = pay_price.getHos_price();
                    int hosPrice = Integer.parseInt(hos_price);
                    mOnline.setText("¥"+dingjin1*value);
                    mDaoyuan.setText("¥"+hosPrice*value);
                }

            }

            @Override
            public void onTextViewClick(int value) {

            }

            @Override
            public void onButtonSubClick(int value, boolean b) {
                String is_group = groupInfo.getIs_group();
                if ("1".equals(is_group)){
                    String group_price = groupInfo.getGroup_price();
                    String group_dingjin = groupInfo.getGroup_dingjin();
                    int groupPrice = Integer.parseInt(group_price);
                    int groupDingjin = Integer.parseInt(group_dingjin);
                    mYmprice.setText("¥" + groupPrice*(value));
                    mOnline.setText("¥" + groupDingjin*(value));
                    int daoyuan = groupPrice - groupDingjin;
                    mDaoyuan.setText("¥" + daoyuan*(value));
                }else {
                    if ("1".equals(regInfo.getIs_seg())){
                        String payPrice = pay_price.getPayPrice();
                        int payPrice1 = Integer.parseInt(payPrice);
                        mYmprice.setText("¥" + payPrice1*(value));
                    }else {
                        if ("1".equals(proInfo.getIsBigPromotion())) {
                            String payPrice = pay_price.getPayPrice();
                            int payPrice1 = Integer.parseInt(payPrice);
                            mYmprice.setText("¥" + payPrice1*(value));
                        }
                    }
                    String dingjin = pay_price.getDingjin();
                    int dingjin1 = Integer.parseInt(dingjin);

                    String hos_price = pay_price.getHos_price();
                    int hosPrice = Integer.parseInt(hos_price);
                    mOnline.setText("¥"+dingjin1*value);
                    mDaoyuan.setText("¥"+hosPrice*value);
                }
            }
        });
    }
}
