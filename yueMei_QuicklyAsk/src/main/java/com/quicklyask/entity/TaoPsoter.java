package com.quicklyask.entity;

public class TaoPsoter {

    /**
     * flag_id : 3
     * t_id : 11586
     * colour_value : #987654
     * width : 0
     * description : 都是附件黄色的经复i受到核辐射的
     * photo :
     * color_value : #123456
     * metro_type : 1
     * height : 0
     * to_link : https://m.yuemei.com/tao_zt/11586.html
     */

    private String flag_id;
    private String t_id;
    private String colour_value;
    private String width;
    private String description;
    private String photo;
    private String color_value;
    private String metro_type;
    private String height;
    private String to_link;

    public String getFlag_id() {
        return flag_id;
    }

    public void setFlag_id(String flag_id) {
        this.flag_id = flag_id;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getColour_value() {
        return colour_value;
    }

    public void setColour_value(String colour_value) {
        this.colour_value = colour_value;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getColor_value() {
        return color_value;
    }

    public void setColor_value(String color_value) {
        this.color_value = color_value;
    }

    public String getMetro_type() {
        return metro_type;
    }

    public void setMetro_type(String metro_type) {
        this.metro_type = metro_type;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getTo_link() {
        return to_link;
    }

    public void setTo_link(String to_link) {
        this.to_link = to_link;
    }
}
