package com.module.shopping.model.bean;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2018/12/12
 */
public class MakeSureOrderDepositCoupons {
    private ArrayList<YouHuiCoupons> nousemember;
    private ArrayList<YouHuiCoupons> usemember;

    public ArrayList<YouHuiCoupons> getNousemember() {
        return nousemember;
    }

    public void setNousemember(ArrayList<YouHuiCoupons> nousemember) {
        this.nousemember = nousemember;
    }

    public ArrayList<YouHuiCoupons> getUsemember() {
        return usemember;
    }

    public void setUsemember(ArrayList<YouHuiCoupons> usemember) {
        this.usemember = usemember;
    }
}
