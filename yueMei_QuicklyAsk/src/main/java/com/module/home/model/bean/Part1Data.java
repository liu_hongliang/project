package com.module.home.model.bean;

/**
 * 板块选择 1级分类数据
 * 
 * @author Rubin
 * 
 */
public class Part1Data {

	private String _id;

	private String cate_name;

	private String p_id;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getCate_name() {
		return cate_name;
	}

	public void setCate_name(String cate_name) {
		this.cate_name = cate_name;
	}

	public String getP_id() {
		return p_id;
	}

	public void setP_id(String p_id) {
		this.p_id = p_id;
	}

}
