package com.module.my.controller.other;

import android.content.Intent;
import android.util.Log;

import com.module.MainTableActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.my.controller.activity.MyPointsActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by 裴成浩 on 2018/1/18.
 */

public class MyPointsWebViewClient implements BaseWebViewClientCallback {

    private Intent intent;
    private MyPointsActivity mActivity;
    private String uid;
    private String TAG = "MyPointsWebViewClient";
    private static final int SHOW_TIME = 1000;

    public MyPointsWebViewClient(MyPointsActivity activity) {
        this.mActivity = activity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 返现跳日记

                String link = obj.getString("link");
                String qid = obj.getString("id");

                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                intent.putExtra("url", link);
                intent.putExtra("qid", qid);
                mActivity.startActivity(intent);

                break;
            case "4":// 签到
                String flag4 = obj.getString("flag");
                String qd4 = obj.getString("qd");
                if ("1".equals(qd4)) {
                    sumitHttpCode(flag4);
                } else {
                    MyToast.makeImgAndTextToast(mActivity, mActivity.getResources().getDrawable(R.drawable.tips_smile), "今日已经签到", SHOW_TIME).show();
                }
                break;

            case "6":// 问答详情

                String link6 = obj.getString("link");
                String qid6 = obj.getString("id");

                intent.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER + link6);
                intent.putExtra("qid", qid6);
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                mActivity.startActivity(intent);

                break;
            case "6133":    //新手好礼
                String flag6133 = obj.getString("flag");
                sumitHttpCode(flag6133);

                break;
            case "6104"://回复回帖/分享帖子

                Log.e("TAG", "11111");
                mActivity.finish();

//                MainTableActivity.tabHost.setCurrentTab(2);
//                MainTableActivity.bnBottom[0].setChecked(false);
//                MainTableActivity.bnBottom[1].setChecked(false);
//                MainTableActivity.bnBottom[2].setChecked(false);
//                MainTableActivity.bnBottom[3].setChecked(false);
//                MainTableActivity.bnBottom[4].setChecked(false);
//                MainTableActivity.bnBottom[5].setChecked(true);
//
//                MainTableActivity.bnBottom[2].setVisibility(View.GONE);
//                MainTableActivity.bnBottom[5].setVisibility(View.VISIBLE);
                MainTableActivity.mainBottomBar.setCheckedPos(2);
                break;
        }
    }

    void sumitHttpCode(final String flag) {
        SumitHttpAip sumitHttpAip = new SumitHttpAip();
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", flag);
        maps.put("uid", uid);
        sumitHttpAip.getCallBack(mActivity, maps, new BaseCallBackListener<JFJY1Data>() {
            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                if (jfjyData != null) {
                    String jifenNu = jfjyData.getIntegral();
                    String jyNu = jfjyData.getExperience();

                    Log.e(TAG, "code==1__flag=" + flag);
                    if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                        MyToast.makeTexttext4Toast(mActivity, jifenNu, jyNu, SHOW_TIME).show();
                    } else {
                        if (!jifenNu.equals("0")) {
                            MyToast.makeTexttext2Toast(mActivity, jifenNu, SHOW_TIME).show();
                        } else {
                            if (!jyNu.equals("0")) {
                                MyToast.makeTexttext3Toast(mActivity, jyNu, SHOW_TIME).show();
                            }
                        }
                    }
                    mActivity.LodUrl1(FinalConstant.MY_POINTS_URL);
                }

            }
        });
    }
}
