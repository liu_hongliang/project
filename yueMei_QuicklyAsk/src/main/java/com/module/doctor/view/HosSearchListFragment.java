package com.module.doctor.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.cpoopc.scrollablelayoutlib.ScrollableHelper;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.loadmore.LoadMoreListView;
import com.module.base.refresh.loadmore.LoadMoreListener;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.model.bean.BBsListData550;
import com.module.home.controller.adapter.TaoAdpter623;
import com.module.home.view.LoadingProgress;
import com.module.home.view.fragment.ScrollAbleFragment;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.api.LodHotDataApi;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.HosSearchTaoData;
import com.quicklyask.entity.SearchResultData23;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;
import java.util.List;

/**
 * 搜本院
 * Created by 裴成浩 on 2018/3/28.
 */

public class HosSearchListFragment extends ScrollAbleFragment implements ScrollableHelper.ScrollableContainer {

    private LoadMoreListView mList;
    private LinearLayout nodataTv;
    private FragmentActivity mActivity;
    private LoadingProgress mProgress;
    private String mKey;
    private String mType;
    private String mHosId;
    private LodHotDataApi mLodHotDataApi;
    private List<HomeTaoData> mLodTaoData;
    private TaoAdpter623 mTaoAdpter623;
    private List<BBsListData550> mLodDiaryData;
    private BBsListAdapter bBsListAdapter;
    private int mPage = 1;
    private boolean isNoMore = false;


    @Override
    public View getScrollableView() {
        return mList;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_list2, container, false);

        mList = view.findViewById(R.id.hos_tao_search_list);
        nodataTv = view.findViewById(R.id.my_collect_post_tv_nodata1);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
        mProgress = new LoadingProgress(mActivity);
        mLodHotDataApi = new LodHotDataApi();

        if (isAdded()) {
            mKey = getArguments().getString("key");
            mType = getArguments().getString("type");
            mHosId = getArguments().getString("hosId");
        }

        initList();

        lodHotIssueData();
    }

    /**
     * 初始化回调事件
     */
    void initList() {

        //点击事件
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
                switch (mType) {
                    case "3":
                        List<BBsListData550> bBsListData550s = bBsListAdapter.getmHotIssues();
                        if (position != bBsListData550s.size()) {
                            String url = bBsListData550s.get(position).getUrl();
                            if (url.length() > 0) {
                                String appmurl = bBsListData550s.get(position).getAppmurl();
                                WebUrlTypeUtil.getInstance(mActivity).urlToApp(appmurl, "0", "0");
                            }
                        }

                        break;
                    case "4":
                        List<HomeTaoData> homeTaoDatas = mTaoAdpter623.getmHotIssues();
                        if (position != homeTaoDatas.size()) {
                            String id = homeTaoDatas.get(position).get_id();
                            Intent it1 = new Intent();
                            it1.putExtra("id", id);
                            it1.putExtra("source", "2");
                            it1.putExtra("objid", "0");
                            it1.setClass(getActivity(), TaoDetailActivity.class);
                            startActivity(it1);
                        }
                        break;
                }
            }
        });

        /**
         * 上拉加载更多
         */
        mList.setLoadMoreListener(new LoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isNoMore) {
                    lodHotIssueData();
                } else {
                    mList.loadMoreEnd();
                }
            }
        });
    }

    /**
     * 联网请求获取数据
     */
    private void lodHotIssueData() {
        mProgress.startLoading();
        HashMap<String, Object> lodParam = new HashMap<>();
        lodParam.put("key", mKey);
        lodParam.put("type", mType);
        lodParam.put("id", mHosId);
        lodParam.put("page", mPage + "");
        mLodHotDataApi.getCallBack(mActivity, lodParam, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mProgress.stopLoading();
                try {
                    if ("1".equals(serverData.code)) {
                        mPage++;
                        switch (mType) {
                            case "3":                               //日记列表
                                setDiaryList(serverData);
                                break;
                            case "4":                               //淘列表
                                setTaoList(serverData);
                                break;
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    /**
     * 设置日记列表
     *
     * @param serverData
     * @throws Exception
     */
    private void setDiaryList(ServerData serverData) throws Exception {
        if (!isNoMore) {
            SearchResultData23 searchResultData23 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData23.class);
            mLodDiaryData = searchResultData23.getList();

            if (bBsListAdapter == null) {
                bBsListAdapter = new BBsListAdapter(mActivity, mLodDiaryData);
                mList.setAdapter(bBsListAdapter);
            } else {
                bBsListAdapter.add(mLodDiaryData);
                bBsListAdapter.notifyDataSetChanged();
            }
        }

        if (bBsListAdapter.getmHotIssues().size() > 0) {
            if (mLodDiaryData.size() < 10) {
                isNoMore = true;
                mList.loadMoreEnd();
            } else {
                mList.loadMoreComplete();
            }
            nodataTv.setVisibility(View.GONE);
        } else {
            nodataTv.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 设置淘列表
     *
     * @param serverData
     * @throws Exception
     */
    private void setTaoList(ServerData serverData){
        if (!isNoMore) {
            HosSearchTaoData searchResultData1 = JSONUtil.TransformSingleBean(serverData.data, HosSearchTaoData.class);
            mLodTaoData = searchResultData1.getList();

            if (mTaoAdpter623 == null) {

                mTaoAdpter623 = new TaoAdpter623(mActivity, mLodTaoData);
                mList.setAdapter(mTaoAdpter623);

            } else {
                mTaoAdpter623.setDataList(mLodTaoData);
                mTaoAdpter623.notifyDataSetChanged();
            }
        }

        if (mTaoAdpter623.getmHotIssues().size() > 0) {
            if (mLodTaoData.size() < 10) {
                isNoMore = true;
                mList.loadMoreEnd();
            } else {
                mList.loadMoreComplete();
            }
            nodataTv.setVisibility(View.GONE);
        } else {
            nodataTv.setVisibility(View.VISIBLE);
        }

    }

}
