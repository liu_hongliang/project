package com.module.home.model.bean;

import java.util.HashMap;

public class BannerData {

    /**
     * qid : 11431
     * url : https://m.yuemei.com/tao_zt/11431.html
     * flag : 0
     * img : https://p11.yuemei.com/tag/1569401899cdb44.jpg
     * img_new : https://p11.yuemei.com/tag/1569401899cdb44.jpg
     * title : 悦美
     * type : 0
     * h_w : 0.33333333333333
     * metro_line : 0
     * event_params : {"to_page_type":16,"to_page_id":11431,"event_others":500422}
     */

    private String qid;
    private String url;
    private String flag;
    private String img;
    private String img_new;
    private String title;
    private String type;
    private String h_w;
    private String metro_line;
    private HashMap<String ,String> event_params;

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_new() {
        return img_new;
    }

    public void setImg_new(String img_new) {
        this.img_new = img_new;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getH_w() {
        return h_w;
    }

    public void setH_w(String h_w) {
        this.h_w = h_w;
    }

    public String getMetro_line() {
        return metro_line;
    }

    public void setMetro_line(String metro_line) {
        this.metro_line = metro_line;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
