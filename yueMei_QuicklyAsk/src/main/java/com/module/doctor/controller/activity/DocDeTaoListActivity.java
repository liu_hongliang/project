package com.module.doctor.controller.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.doctor.model.api.LodHotIssueData;
import com.module.home.controller.adapter.TaoAdpter623;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

/**
 * 医生详情页更多淘整形服务
 *
 * @author Robin
 */
public class DocDeTaoListActivity extends YMBaseActivity {

    private final String TAG = "DocDeTaoListActivity";
    private DocDeTaoListActivity mContext;
    // List
    @BindView(R.id.bar_only_list_refresh)
    SmartRefreshLayout listRefresh;
    @BindView(R.id.bar_only_list_view)
    ListView mListView;
    @BindView(R.id.bar_only_list_refresh_more)
    YMLoadMore listRefreshmore;

    @BindView(R.id.bar_only_top)
    CommonTopBar mTop;// 返回
    @BindView(R.id.my_collect_doc_tv_nodata)
    LinearLayout nodataTv;

    private int mCurPage = 1;

    private TaoAdpter623 hotAdpter;

    private String docid;

    @Override
    protected int getLayoutId() {
        return R.layout.acty_my_collect_tao;
    }

    @Override
    protected void initView() {
        mContext = DocDeTaoListActivity.this;
        mTop.setCenterText("专家服务");

        docid = getIntent().getStringExtra("docid");
    }

    @Override
    protected void initData() {

        initList();
        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout.setOnSildingFinishListener(new OnSildingFinishListener() {

            @Override
            public void onSildingFinish() {
                DocDeTaoListActivity.this.finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    private void initList() {

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        lodHotIssueData();

        //下拉刷新
        listRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mCurPage = 1;
                hotAdpter = null;
                lodHotIssueData();
            }
        });

        //上拉加载更多
        listRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                lodHotIssueData();
            }
        });

        //点击事件
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (hotAdpter != null) {
                    String _id = hotAdpter.getData().get(position).get_id();
                    Intent it1 = new Intent();
                    it1.putExtra("id", _id);
                    it1.putExtra("source", "0");
                    it1.putExtra("objid", "0");
                    it1.setClass(mContext, TaoDetailActivity.class);
                    startActivity(it1);
                }
            }
        });
    }

    private void lodHotIssueData() {

        HashMap<String, Object> maps = new HashMap<>();
        maps.put("page", mCurPage + "");
        maps.put("id", docid);
        new LodHotIssueData().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mCurPage++;
                if ("1".equals(serverData.code)) {
                    ArrayList<HomeTaoData> lvHotIssueData = JSONUtil.jsonToArrayList(serverData.data, HomeTaoData.class);
                    Log.e(TAG, "lvHotIssueData:==" + lvHotIssueData.size());
                    if (hotAdpter == null) {
                        listRefresh.finishRefresh();

                        hotAdpter = new TaoAdpter623(mContext, lvHotIssueData);
                        mListView.setAdapter(hotAdpter);
                    } else {
                        if (lvHotIssueData.size() < 32) {
                            listRefresh.finishLoadMoreWithNoMoreData();
                        } else {
                            listRefresh.finishLoadMore();
                        }

                        hotAdpter.add(lvHotIssueData);
                        hotAdpter.notifyDataSetChanged();
                    }

                }
            }
        });
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
