package com.quicklyask.entity;

import com.module.community.model.bean.UserClickData;

import java.util.HashMap;
import java.util.List;

public class AppraisalListBean {

    private String appmurl;
    private List<Pic> pic;
    private String id;
    private String view_num;
    private String title;
    private String user_id;
    private String time;
    private String user_img;
    private String user_center_url;
    private String user_name;
    private String answer_num;
    private String rateSale;
    private String agree_num;
    private HashMap<String,String> event_params;
    private String have_pic;
    private UserClickData userClickData;

    public String getAppmurl() {
        return appmurl;
    }

    public void setAppmurl(String appmurl) {
        this.appmurl = appmurl;
    }

    public List<Pic> getPic() {
        return pic;
    }

    public void setPic(List<Pic> pic) {
        this.pic = pic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getView_num() {
        return view_num;
    }

    public void setView_num(String view_num) {
        this.view_num = view_num;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAnswer_num() {
        return answer_num;
    }

    public void setAnswer_num(String answer_num) {
        this.answer_num = answer_num;
    }

    public String getRateSale() {
        return rateSale;
    }

    public void setRateSale(String rateSale) {
        this.rateSale = rateSale;
    }

    public String getAgree_num() {
        return agree_num;
    }

    public void setAgree_num(String agree_num) {
        this.agree_num = agree_num;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public String getHave_pic() {
        return have_pic;
    }

    public void setHave_pic(String have_pic) {
        this.have_pic = have_pic;
    }

    public String getUser_center_url() {
        return user_center_url;
    }

    public void setUser_center_url(String user_center_url) {
        this.user_center_url = user_center_url;
    }

    public UserClickData getUserClickData() {
        return userClickData;
    }

    public void setUserClickData(UserClickData userClickData) {
        this.userClickData = userClickData;
    }
}
