package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/7.
 */
public class DiaryVideoData implements Parcelable {
    private String img;
    private String video_url;

    public DiaryVideoData() {
    }


    protected DiaryVideoData(Parcel in) {
        img = in.readString();
        video_url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
        dest.writeString(video_url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryVideoData> CREATOR = new Creator<DiaryVideoData>() {
        @Override
        public DiaryVideoData createFromParcel(Parcel in) {
            return new DiaryVideoData(in);
        }

        @Override
        public DiaryVideoData[] newArray(int size) {
            return new DiaryVideoData[size];
        }
    };

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }
}
