package com.module.commonview.module.bean;

import java.util.HashMap;

public class QuickQuestion {

    private String show_content;
    private String send_content;
    private String quick_reply;

    public String getShow_content() {
        return show_content;
    }

    public void setShow_content(String show_content) {
        this.show_content = show_content;
    }

    public String getSend_content() {
        return send_content;
    }

    public void setSend_content(String send_content) {
        this.send_content = send_content;
    }

    public String getQuick_reply() {
        return quick_reply;
    }

    public void setQuick_reply(String quick_reply) {
        this.quick_reply = quick_reply;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    private HashMap<String,String> event_params;

}
