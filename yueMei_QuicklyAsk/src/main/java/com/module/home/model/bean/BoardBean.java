package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2017/7/31.
 */

public class BoardBean {

    private String id;
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
