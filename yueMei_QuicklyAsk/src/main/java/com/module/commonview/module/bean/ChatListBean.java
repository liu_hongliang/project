package com.module.commonview.module.bean;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/12/11.
 */

public class ChatListBean {


    private List<ListBean> list;
    private RecomendBean recomend;

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public RecomendBean getRecomend() {
        return recomend;
    }

    public void setRecomend(RecomendBean recomend) {
        this.recomend = recomend;
    }

    public static class ListBean {
        /**
         * hos_id : 5255
         * hos_name : 西安西京医院整形外科
         * group_id : 4696
         * groupUserId : 4696/675727
         * noread : 0
         * message : [您收到一个红包]
         * dateTime : 1551175836
         * timeSet : 前天 18:10
         * userImg : https://p21.yuemei.com/avatar/000/07/11/50_avatar_120_120.jpg
         * fromName : 西安西京医院整形外科
         * id : 71150
         */

        private String hos_id;
        private String hos_name;
        private String group_id;
        private String groupUserId;
        private String noread;
        private String message;
        private String dateTime;
        private String timeSet;
        private String userImg;
        private String fromName;
        private String id;
        private String label_color;
        private String label_title;
        private String label_icon;
        private HashMap<String,String>event_params;

        public String getHos_id() {
            return hos_id;
        }

        public void setHos_id(String hos_id) {
            this.hos_id = hos_id;
        }

        public String getHos_name() {
            return hos_name;
        }

        public void setHos_name(String hos_name) {
            this.hos_name = hos_name;
        }

        public String getGroup_id() {
            return group_id;
        }

        public void setGroup_id(String group_id) {
            this.group_id = group_id;
        }

        public String getGroupUserId() {
            return groupUserId;
        }

        public void setGroupUserId(String groupUserId) {
            this.groupUserId = groupUserId;
        }

        public String getNoread() {
            return noread;
        }

        public void setNoread(String noread) {
            this.noread = noread;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

        public String getTimeSet() {
            return timeSet;
        }

        public void setTimeSet(String timeSet) {
            this.timeSet = timeSet;
        }

        public String getUserImg() {
            return userImg;
        }

        public void setUserImg(String userImg) {
            this.userImg = userImg;
        }

        public String getFromName() {
            return fromName;
        }

        public void setFromName(String fromName) {
            this.fromName = fromName;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLabel_color() {
            return label_color;
        }

        public void setLabel_color(String label_color) {
            this.label_color = label_color;
        }

        public String getLabel_title() {
            return label_title;
        }

        public void setLabel_title(String label_title) {
            this.label_title = label_title;
        }

        public String getLabel_icon() {
            return label_icon;
        }

        public void setLabel_icon(String label_icon) {
            this.label_icon = label_icon;
        }

        public HashMap<String, String> getEvent_params() {
            return event_params;
        }

        public void setEvent_params(HashMap<String, String> event_params) {
            this.event_params = event_params;
        }
    }

    public static class RecomendBean {
        /**
         * title : 对瘦脸针感兴趣的人都在咨询
         * list : [{"id":"85652638","hos_id":"12268","hos_name":"北京蜜邦医疗美容诊所","fromName":"北京蜜邦医疗美容诊所","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/15142599422305.jpg","business_id":"419","chatUserNum":687069},{"id":"86429887","hos_id":"13078","hos_name":"北京凤凰妇儿医院","fromName":"北京凤凰妇儿医院","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/15301779214744.jpg","business_id":"1010","chatUserNum":108351},{"id":"85280507","hos_id":"3354","hos_name":"北京润美玉之光医疗美容门诊部","fromName":"北京润美玉之光医疗美容门诊部","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/15514950472845.jpg","business_id":"716","chatUserNum":541749},{"id":"509491","hos_id":"3501","hos_name":"北京欧华医疗美容诊所","fromName":"北京欧华医疗美容诊所","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/14780692391080.png","business_id":"262","chatUserNum":312842},{"id":"85007051","hos_id":"10755","hos_name":"北京华悦府医疗美容诊所","fromName":"北京华悦府医疗美容诊所","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/14826484498989.jpg","business_id":"174","chatUserNum":230879},{"id":"85382353","hos_id":"12053","hos_name":"北京美莱医疗美容医院","fromName":"北京美莱医疗美容医院","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/15439747032707.jpg","business_id":"476","chatUserNum":362318},{"id":"85279475","hos_id":"2864","hos_name":"煤炭总医院","fromName":"煤炭总医院","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/14294151358472.png","business_id":"714","chatUserNum":34347},{"id":"85282827","hos_id":"11111","hos_name":"北京京通医院","fromName":"北京京通医院","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/14951853608883.jpg","business_id":"222","chatUserNum":136414},{"id":"85047575","hos_id":"8721","hos_name":"北京世熙医疗美容门诊部","fromName":"北京世熙医疗美容门诊部","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/14875565063643.png","business_id":"449","chatUserNum":188151},{"id":"84945547","hos_id":"9835","hos_name":"北京彤美医疗美容门诊部","fromName":"北京彤美医疗美容门诊部","group_id":"0","groupUserId":"0","noread":"0","message":"","dateTime":"","timeSet":"","userImg":"https://ymt.yuemei.com/upload/vip/120_120/14903384768436.jpg","business_id":"54","chatUserNum":62200}]
         */

        private String title;
        private List<ListBean> list;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * id : 85652638
             * hos_id : 12268
             * hos_name : 北京蜜邦医疗美容诊所
             * fromName : 北京蜜邦医疗美容诊所
             * group_id : 0
             * groupUserId : 0
             * noread : 0
             * message :
             * dateTime :
             * timeSet :
             * userImg : https://ymt.yuemei.com/upload/vip/120_120/15142599422305.jpg
             * business_id : 419
             * chatUserNum : 687069
             */

            private String id;
            private String hos_id;
            private String hos_name;
            private String fromName;
            private String group_id;
            private String groupUserId;
            private String noread;
            private String message;
            private String dateTime;
            private String timeSet;
            private String userImg;
            private String business_id;
            private int chatUserNum;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getHos_id() {
                return hos_id;
            }

            public void setHos_id(String hos_id) {
                this.hos_id = hos_id;
            }

            public String getHos_name() {
                return hos_name;
            }

            public void setHos_name(String hos_name) {
                this.hos_name = hos_name;
            }

            public String getFromName() {
                return fromName;
            }

            public void setFromName(String fromName) {
                this.fromName = fromName;
            }

            public String getGroup_id() {
                return group_id;
            }

            public void setGroup_id(String group_id) {
                this.group_id = group_id;
            }

            public String getGroupUserId() {
                return groupUserId;
            }

            public void setGroupUserId(String groupUserId) {
                this.groupUserId = groupUserId;
            }

            public String getNoread() {
                return noread;
            }

            public void setNoread(String noread) {
                this.noread = noread;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getDateTime() {
                return dateTime;
            }

            public void setDateTime(String dateTime) {
                this.dateTime = dateTime;
            }

            public String getTimeSet() {
                return timeSet;
            }

            public void setTimeSet(String timeSet) {
                this.timeSet = timeSet;
            }

            public String getUserImg() {
                return userImg;
            }

            public void setUserImg(String userImg) {
                this.userImg = userImg;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public int getChatUserNum() {
                return chatUserNum;
            }

            public void setChatUserNum(int chatUserNum) {
                this.chatUserNum = chatUserNum;
            }
        }
    }
}
