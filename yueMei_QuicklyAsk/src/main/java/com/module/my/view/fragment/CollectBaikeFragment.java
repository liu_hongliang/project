/**
 *
 */
package com.module.my.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.loadmore.LoadMoreListView;
import com.module.base.refresh.loadmore.LoadMoreListener;
import com.module.commonview.activity.BaikeFourActivity671;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.my.controller.adapter.BaikeItemAdapter;
import com.module.my.model.api.CollectBaikeApi;
import com.module.my.model.bean.BaikeItemData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 百科收藏
 *
 * @author lenovo17
 */
public class CollectBaikeFragment extends Fragment {

    private final String TAG = "CollectBaikeFragment";

    private Activity mContext;

    // List
    private LoadMoreListView mlist;
    private int mCurPage = 1;

    private List<BaikeItemData> lvBaikeData = new ArrayList<>();
    private BaikeItemAdapter baikeItemAdapter;

    private LinearLayout nodataTv;

    private String uid;
    private CollectBaikeApi collectBaikeApi;
    private HashMap<String, Object> collectBaikeMap = new HashMap<>();
    private boolean isNoMore = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_collect_san_550, container, false);
        nodataTv = v.findViewById(R.id.my_collect_post_tv_nodata);
        mlist = v.findViewById(R.id.collect_san_list);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();

        collectBaikeApi = new CollectBaikeApi();

        if (isAdded()) {
            uid = Utils.getUid();
        }
        initList();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("MainScreen"); // 统计页面
        StatService.onResume(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        // Log.e(TAG, "onStart");
        uid = Utils.getUid();
    }

    void initList() {

        lodHotIssueData();


        mlist.setLoadMoreListener(new LoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isNoMore) {
                    lodHotIssueData();
                } else {
                    mlist.loadMoreEnd();
                }
            }
        });


        mlist.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                DeleteDialog(pos);
                return true;
            }
        });

        mlist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos, long arg3) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }

                List<BaikeItemData> baikeItemData = baikeItemAdapter.getmBaikeItemData();
                Log.e(TAG, "baikeItemData === " + baikeItemData.size());
                Log.e(TAG, "pos === " + pos);
                if (pos != baikeItemData.size()) {
                    String name = baikeItemData.get(pos).getTitle();
                    String id = baikeItemData.get(pos).get_id();
                    String url = FinalConstant.BAIKE_FOR + id;
                    String shareurl = baikeItemData.get(pos).getShareurl();
                    String sharetitle = baikeItemData.get(pos).getSharetitle();
                    Intent it = new Intent();
                    it.putExtra("name", name);
                    it.putExtra("url", url);
                    it.putExtra("id", id);
                    it.putExtra("shareurl", shareurl);
                    it.putExtra("sharetitle", sharetitle);
                    it.setClass(mContext, BaikeFourActivity671.class);
                    startActivity(it);
                }
            }
        });
    }

    void lodHotIssueData() {
        collectBaikeMap.put("id", uid);
        collectBaikeMap.put("page", mCurPage + "");
        collectBaikeApi.getCallBack(mContext, collectBaikeMap, new BaseCallBackListener<List<BaikeItemData>>() {
            @Override
            public void onSuccess(List<BaikeItemData> baikeItemDatas) {
                mCurPage++;
                lvBaikeData = baikeItemDatas;

                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (baikeItemAdapter == null) {
                            baikeItemAdapter = new BaikeItemAdapter(getActivity(), lvBaikeData);
                            mlist.setAdapter(baikeItemAdapter);
                        } else {
                            baikeItemAdapter.addData(lvBaikeData);
                            baikeItemAdapter.notifyDataSetChanged();
                        }

                        Log.e(TAG, "lvBaikeData.size() === " + lvBaikeData.size());
                        if (lvBaikeData.size() < 20) {
                            isNoMore = true;
                        }

                        if (isNoMore) {
                            mlist.loadMoreEnd();
                        } else {
                            mlist.loadMoreComplete();
                        }

                    }
                });
            }
        });

    }

    /**
     * 长按item删除对话框
     *
     * @param pos
     */
    private void DeleteDialog(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("      确定取消对该百科的收藏？");
        // 确定按钮监听
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String userid = lvBaikeData.get(pos).get_id();
                deleteCollect(userid);

                lvBaikeData.remove(pos);

                if (lvBaikeData.equals("") || lvBaikeData == null) {
                    nodataTv.setVisibility(View.VISIBLE);
                }

                if (lvBaikeData.size() == 1 && pos == 0) {
                    nodataTv.setVisibility(View.VISIBLE);
                    mlist.setVisibility(View.GONE);
                }

                baikeItemAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
        builder.create().setCanceledOnTouchOutside(false);
    }

    /**
     * 取消对百科的收藏
     *
     * @param
     */
    void deleteCollect(String hosid) {
        CancelCollectApi cancelCollectApi = new CancelCollectApi();
        Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        params.put("objid", hosid);
        params.put("type", "5");
        cancelCollectApi.getCallBack(mContext, params, new BaseCallBackListener() {
            @Override
            public void onSuccess(Object o) {

            }
        });
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("MainScreen");
        StatService.onPause(getActivity());
    }
}
