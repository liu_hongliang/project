package com.module.shopping.model.api;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.bean.MakeSureOrderData;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.view.MyToast;

import java.util.HashMap;
import java.util.Map;

/**
 * 下单页面订单
 * Created by 裴成浩 on 2018/12/12
 */
public class MakeSureOrderApi implements BaseCallBackApi {
    private String TAG = "MakeSureOrderApi";
    private HashMap<String, Object> mHashMap;  //传值容器

    public MakeSureOrderApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(final Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.CART, "settlement", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if ("1".equals(mData.code)) {
                    Log.e(TAG, "mData == " + mData.toString());
                    MakeSureOrderData makeSureOrderData = JSONUtil.TransformSingleBean(mData.data, MakeSureOrderData.class);
                    listener.onSuccess(makeSureOrderData);
                } else {
                    MyToast.makeTextToast2(context, mData.message, MyToast.SHOW_TIME).show();
                    if (context instanceof Activity) {
                        ((Activity) context).finish();
                    }
                }
                Log.e(TAG, "mData = " + mData.toString());
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
