package com.quicklyask.view;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.TrafficStats;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.danikula.videocache.HttpProxyCacheServer;
import com.module.MyApplication;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.VideoViewState;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.NetworkUtil;
import com.quicklyask.util.Utils;

import java.util.HashMap;


/**
 * 悦美视频播放器
 * Created by 裴成浩 on 2018/5/31.
 */
public class YueMeiVideoView extends FrameLayout {

    private Context mContext;
    private LayoutInflater mInflater;
    private final String TAG = "YueMeiVideoView";

    private FrameLayout mVideoViewContainer;//视频容器
    private FixedTextureVideoView mVideoView;           //媒体播放器
    private ImageView mVideoThumbnail;      //视频缩略图
    private FrameLayout mVideoToPlayControl;  //视频播放状态控制
    private FrameLayout mControlPanel;     //视频总控制
    private FrameLayout mVideoControlContainer;//视频暂停、播放控制容器
    private ImageView mVideoControl;        //视频暂停、播放控制
    private SeekBar mVideoSeekbar;          //视频进度控制
    private TextView mVideoStartTime;       //视频进行时间
    private TextView mVideoEndTime;         //视频总时间
    private FrameLayout mVideoMaxContainer; //视频放大容器
    private SeekBar mVideoBottomSeekbar;    //视频底部进度控制
    private int duration = 0;               //视频时长

    private int suspendPosition = 0;        //文件挂起时的进度
    private VideoViewState mVideoViewState = VideoViewState.START_PLAY; //视频播放状态（默认是开始播放状态）
    private String mVideoPath;              //视频路径
    private int secondaryProgress;          //缓存进度
    private boolean hideControl = false;    //是否隐藏进度条

    private final int PROGRESS = 360;           //进度
    private final int CONTROL_HIDDEN = 361;     //控制器显示隐藏

    private int videoWidth;                 //视频宽度
    private int videoHeight;                //视频高度

    private IntentFilter mIntentFilter;
    private HttpProxyCacheServer mProxy;                    //缓存

    private boolean networkChangesisNo;        //网络变化监听设置
    private int videoPlayerTime;              //控制器显示隐藏时间
    private boolean isReplay;                //是否重复播放
    private int seekBarTime;                 //可以拖动进度条的最小时间
    private int VIDEO_PAUSE;                //暂停按钮
    private int VIDEO_PLAY;                 //开始按钮

    private String taoId;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case PROGRESS:                                              //进度
                    int currentPosition = getCurrentPosition();
                    mVideoSeekbar.setProgress(currentPosition);                 //进度条进度
                    mVideoBottomSeekbar.setProgress(currentPosition);           //进度条进度

                    secondaryProgress = mVideoView.getBufferPercentage() * mVideoSeekbar.getMax() / 100;
                    mVideoSeekbar.setSecondaryProgress(secondaryProgress);
                    mVideoBottomSeekbar.setSecondaryProgress(secondaryProgress);

                    if (currentPosition <= YueMeiVideoView.this.duration) {                        //没有播放完成
                        videoRemoveMessage(PROGRESS);
                        sendEmptyMessageDelayed(PROGRESS, 100);
                    }

                    //进度回调
                    if (onProgressClick != null) {
                        onProgressClick.onProgressClickk(currentPosition, duration);
                    }

                    break;
                case CONTROL_HIDDEN:                                        //控制器显示隐藏
                    Log.e(TAG, "11111");
                    progressControl(GONE, VISIBLE);
                    videoRemoveMessage(CONTROL_HIDDEN);
                    break;
            }
        }
    };

    //网络状态变化时的广播
    private BroadcastReceiver netReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!mProxy.isCached(mVideoPath)) {
                int netWorkStates = NetworkUtil.getNetWorkStates(context);
                switch (netWorkStates) {
                    case NetworkUtil.TYPE_NONE:  //变为无网络
                        Log.e("aaaa", "无网络");
                        if (getCurrentPosition() == secondaryProgress) {
                            setVideoViewState(VideoViewState.NETWORK_ERROR, VISIBLE);
                        }
                        break;
                    case NetworkUtil.TYPE_MOBILE:   //变为移动网络下
                        Log.e("aaaa", "变为移动网络下");
                        if ("0".equals(Cfg.loadStr(mContext, FinalConstant.NOT_WIFI_PLAY, "0"))) {
                            setVideoViewState(VideoViewState.NOT_WIFI, VISIBLE);
                        } else {
                            videoPlayer();
                        }

                        break;
                    case NetworkUtil.TYPE_WIFI: //变为wifi网络下
                        Log.e("aaaa", "变为wifi网络下");
                        videoPlayer();
                        break;
                }
            }
        }
    };

    public YueMeiVideoView(Context context) {
        this(context, null);
    }

    public YueMeiVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);

        //视频缓存
        mProxy = MyApplication.getProxy(mContext);

        initAttr(attrs);
        initListening();
    }

    /**
     * 设置属性
     *
     * @param attrs
     */
    private void initAttr(AttributeSet attrs) {
        TypedArray ta = mContext.obtainStyledAttributes(attrs, R.styleable.video_play_attribute);
        //控制器样式
        initView(ta.getInt(R.styleable.video_play_attribute_controller_style, 1));
        //控制器隐藏时间
        setVideoPlayerTime(ta.getInt(R.styleable.video_play_attribute_video_player_time, 3000));
        //播放之前是否显示中间播放按钮，默认显示
        if (ta.getBoolean(R.styleable.video_play_attribute_satrt_paly_show, true)) {
            setVideoViewState(VISIBLE);
        }
        //设置播放器背景，默认黑色
        setVideoPlayerBackground(ta.getResourceId(R.styleable.video_play_attribute_video_player_background, R.color.black));
        //网络变化监听设置,默认设置
        setNetworkChanges(ta.getBoolean(R.styleable.video_play_attribute_networkChangesisNo, true));
        //是否重复播放,默认不重复播放
        setReplay(ta.getBoolean(R.styleable.video_play_attribute_isReplay, false));
        //可以拖动进度条的最小时间
        setSeekVideoTime(ta.getInt(R.styleable.video_play_attribute_seek_bar_video_time, 15000));
        ta.recycle();
    }


    /**
     * 初始化布局
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initView(int controllerStyle) {
        View rootView = mInflater.inflate(R.layout.yuemei_video_view, this);
        mVideoViewContainer = rootView.findViewById(R.id.yuemei_viodeo_container);
        mVideoView = rootView.findViewById(R.id.yuemei_viodeo_view);
        mVideoThumbnail = rootView.findViewById(R.id.yuemei_viodeo_view_thumbnail);
        mVideoToPlayControl = rootView.findViewById(R.id.yuemei_viodeo_view_to_play_control);
        mControlPanel = rootView.findViewById(R.id.yuemei_video_control_panel);
        mVideoBottomSeekbar = rootView.findViewById(R.id.yuemei_video_bottom_seekbar);

        //设置控制器样式
        if (controllerStyle == 1) {
            VIDEO_PAUSE = R.drawable.video_pause_2x;
            VIDEO_PLAY = R.drawable.video_play_2x;
            View controlView1 = View.inflate(mContext, R.layout.yuemei_video_control_view1, mControlPanel);
            mVideoControlContainer = controlView1.findViewById(R.id.yuemei_video_control_container1);
            mVideoControl = controlView1.findViewById(R.id.yuemei_video_control1);
            mVideoSeekbar = controlView1.findViewById(R.id.yuemei_video_seekbar1);
            mVideoStartTime = controlView1.findViewById(R.id.yuemei_video_start_time1);
            mVideoEndTime = controlView1.findViewById(R.id.yuemei_video_end_time1);
            mVideoMaxContainer = controlView1.findViewById(R.id.yuemei_video_max_container1);
        } else {
            VIDEO_PAUSE = R.drawable.video_list_view_pause;
            VIDEO_PLAY = R.drawable.video_list_view_play;
            View controlView2 = View.inflate(mContext, R.layout.yuemei_video_control_view2, mControlPanel);
            mVideoControlContainer = controlView2.findViewById(R.id.yuemei_video_control_container2);
            mVideoControl = controlView2.findViewById(R.id.yuemei_video_control2);
            mVideoSeekbar = controlView2.findViewById(R.id.yuemei_video_seekbar2);
            mVideoStartTime = controlView2.findViewById(R.id.yuemei_video_start_time2);
            mVideoEndTime = controlView2.findViewById(R.id.yuemei_video_end_time2);
        }

        //自带进度条隐藏
        MediaController mMediaController = new MediaController(mContext);
        mMediaController.setVisibility(View.GONE);
        mVideoView.setMediaController(mMediaController);

        //设置屏幕常亮
        mVideoView.setKeepScreenOn(true);

        // 进度条监听
        mVideoSeekbar.setOnSeekBarChangeListener(new VideoOnSeekBarChangeListener());

        //底部进度条不可拖动
        mVideoBottomSeekbar.setEnabled(false);
    }

    /**
     * 初始化监听
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initListening() {
        //第一次准备开始播放回调(在每次调用setVideoPath()之后回调，只回调一次)
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoWidth = mediaPlayer.getVideoWidth();
                videoHeight = mediaPlayer.getVideoHeight();
                Log.e(TAG, "videoWidth" + videoWidth + "--videoHeight" + videoHeight);

                mediaPlayer.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);       //充满屏幕显示，保持比例，如果屏幕比例不对，则进行裁剪

                setVideoSize(videoWidth, videoHeight);

                //获取视频时长
                duration = mVideoView.getDuration();

                mVideoSeekbar.setMax(duration);                  //进度条设置
                mVideoBottomSeekbar.setMax(duration);
                mVideoEndTime.setText(msConversion(duration));   //设置视频总进度
                if (mVideoPath.startsWith("http") && duration < seekBarTime) {        //如果视频小于15s且是网络视频
                    mVideoSeekbar.setEnabled(false);
                } else {
                    mVideoSeekbar.setEnabled(true);
                }
            }
        });

        //每次结束播放回调
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                if (isReplay) {
                    videoStartPlayer();
                } else {
                    videoPausePlayer();

                    mVideoSeekbar.setProgress(duration);
                    mVideoBottomSeekbar.setProgress(duration);
                    setVideoViewState(VideoViewState.TO_PLAY, VISIBLE);
                    Log.e(TAG, "2222");
                    progressControl(GONE, VISIBLE);

                    videoRemoveMessage(PROGRESS);
                }

                if (onCompletionListener != null) {
                    onCompletionListener.onCompletionListener(videoWidth,videoHeight);
                }
            }
        });

        //视频播放出错(视频源出错或者没有网络都会出现错误)
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e(TAG, "what == " + what);
                Log.e(TAG, "extra == " + extra);
                if (what != 1) {                              //非网络中断错误
                    setVideoViewState(VideoViewState.VIDEO_ERROR, VISIBLE);
                } else {
                    setVideoViewState(VideoViewState.NETWORK_ERROR, VISIBLE);
                }
                return true;
            }
        });

        //视频暂停播放按钮
        mVideoControlContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlaying()) {
                    videoPausePlayer();
                } else {
                    videoStartPlayer();
                }

                if (onControllerClick != null) {
                    onControllerClick.onControllerClickk(isPlaying());
                }
            }
        });

        //视频容器点击事件不向下分发
        mVideoViewContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        if (mControlPanel.getVisibility() == GONE && mVideoSeekbar.getProgress() != duration) {

                            Log.e(TAG, "33333");
                            progressControl(VISIBLE, GONE);
                            if (isPlaying()) {
                                mHandler.sendEmptyMessageDelayed(CONTROL_HIDDEN, videoPlayerTime);
                            }

                        } else {
                            Log.e(TAG, "44444");
                            progressControl(GONE, VISIBLE);
                            videoRemoveMessage(CONTROL_HIDDEN);
                        }
                        break;
                }
                return true;
            }
        });

        //视频最大化点击
        if (mVideoMaxContainer != null) {
            mVideoMaxContainer.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onMaxVideoClickListener != null) {
                        onMaxVideoClickListener.onMaxVideoClick(v);
                    }
                }
            });
        }
    }

    /**
     * 判断当前是否是否正在播放
     *
     * @return true:正在播放
     */
    public boolean isPlaying() {
        return mVideoView.isPlaying();
    }

    /**
     * 开始播放视频判断
     */
    public void videoStartPlayer() {
        Log.e(TAG, "videoStartPlayer...");
        Log.e(TAG, "播放proxyUrl == " + mProxy.getProxyUrl(mVideoPath));
        Log.e(TAG, "mProxy.isCached(mVideoPath) == " + mProxy.isCached(mVideoPath));
        if (mProxy.isCached(mVideoPath)) {
            videoPlayer();
        } else {
            switch (Utils.getAPNType(mContext)) {
                case 0:        //无网络
                    setVideoViewState(VideoViewState.NETWORK_ERROR, VISIBLE);
                    break;
                case 1:        //wifi网络下
                    videoPlayer();
                    break;
                default:      //移动网络下
                    if ("0".equals(Cfg.loadStr(mContext, FinalConstant.NOT_WIFI_PLAY, "0"))) {
                        setVideoViewState(VideoViewState.NOT_WIFI, VISIBLE);
                    } else {
                        videoPlayer();
                    }
                    break;
            }
        }

    }

    public String getTaoId() {
        return taoId;
    }

    public void setTaoId(String taoId) {
        this.taoId = taoId;
    }

    /**
     * 开始播放视频
     */
    private void videoPlayer() {
        Log.e(TAG, "videoPlayer.....");
        //统计播放按钮点击
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id",taoId);
        hashMap.put("tao_id",taoId);
        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SKU_INFO_VIDEO_PLAY, "0"),hashMap,new ActivityTypeData("2"));

        mVideoThumbnail.setVisibility(GONE);    //缩略图隐藏
        setVideoViewState(GONE);

        mVideoView.requestFocus();              //获取焦点
        mVideoView.start();                     //开始播放

        mVideoControl.setImageResource(VIDEO_PAUSE);
        videoSendMessage(PROGRESS);
//        videoSendMessage(CONTROL_HIDDEN, videoPlayerTime);

        if (mIntentFilter == null && networkChangesisNo) {
            mIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            mContext.registerReceiver(netReceiver, mIntentFilter);
        }
    }

    /**
     * 视频暂停、结束播放
     */
    public void videoPausePlayer() {
        mVideoView.pause();                     //暂停播放
        mVideoControl.setImageResource(VIDEO_PLAY);
        videoRemoveMessage(CONTROL_HIDDEN);
        Log.e(TAG, "55555");
        progressControl(VISIBLE, GONE);
    }

    /**
     * 是否隐藏进度条
     *
     * @param hideControl
     */
    public void hideVideoControl(boolean hideControl) {
        this.hideControl = hideControl;
    }

    /**
     * 视频进度设置
     *
     * @param progress
     */
    public void setSeekTo(int progress) {
        mVideoView.seekTo(progress);
    }

    /**
     * 获取视频播放进度
     */
    public int getCurrentPosition() {
//        Log.e(TAG, "mVideoView.getCurrentPosition() == " + mVideoView.getCurrentPosition());
        return mVideoView.getCurrentPosition();
    }

    /**
     * 恢复挂起的播放器 （onRestart中调用）
     */
    public void videoRestart() {
        videoRestart(suspendPosition);
    }

    /**
     * 恢复挂起的播放器 （onRestart中调用）
     *
     * @param position :指定视频播放位置
     */
    public void videoRestart(int position) {
        setSeekTo(position);
        mVideoView.resume();
        videoSendMessage(PROGRESS);
    }

    /**
     * 挂起视频文件（onPause中调用）
     */
    public void videoSuspend() {
        suspendPosition = getCurrentPosition();
        videoRemoveMessage(PROGRESS);
        mVideoView.suspend();
    }

    /**
     * 停止视频操作（onDestroy中调用）
     */
    public void videoStopPlayback() {
        mVideoView.stopPlayback();
        videoRemoveMessage(PROGRESS);

        if (mIntentFilter != null && netReceiver != null && networkChangesisNo) {
            mContext.unregisterReceiver(netReceiver);
        }

    }

    /**
     * 开始发送消息
     *
     * @param what
     */
    private void videoSendMessage(int what) {
        mHandler.sendEmptyMessage(what);
    }

    /**
     * 开始发延迟消息
     *
     * @param what:消息
     * @param delayed：延迟时间
     */
    private void videoSendMessage(int what, int delayed) {
        mHandler.sendEmptyMessageDelayed(what, delayed);
    }

    /**
     * 进度条的控制显示和隐藏
     *
     * @param visibility1 : 上边进度条显示和隐藏
     * @param visibility2 ：下边进度条的显示和隐藏
     */
    private void progressControl(int visibility1, int visibility2) {
        if (hideControl) {
            mControlPanel.setVisibility(GONE);
            mVideoBottomSeekbar.setVisibility(VISIBLE);
            if (onProgressBarStateClick != null) {
                onProgressBarStateClick.onProgressBarStateClick(GONE);
            }
        } else {
            mControlPanel.setVisibility(visibility1);
            mVideoBottomSeekbar.setVisibility(visibility2);

            if (onProgressBarStateClick != null) {
                onProgressBarStateClick.onProgressBarStateClick(visibility1);
            }
        }

    }

    /**
     * 停止发送消息
     */
    private void videoRemoveMessage(int what) {
        mHandler.removeMessages(what);
    }

    /**
     * 设置视频缩略图和视频路径
     *
     * @param thumvnail：视频缩略图
     * @param videoPath：本地路径和服务器路径都可以
     */

    public void setVideoParameter(String thumvnail, String videoPath) {
        mVideoPath = videoPath;
        String proxyUrl = mProxy.getProxyUrl(mVideoPath);
        if (!TextUtils.isEmpty(thumvnail)) {
            //缩略图宽高
//            Glide.with(mContext).load(thumvnail).centerCrop().into(mVideoThumbnail);
            Glide.with(mContext).load(thumvnail).centerCrop().into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    int intrinsicWidth = resource.getIntrinsicWidth();
                    int intrinsicHeight = resource.getIntrinsicHeight();
                    videoWidth = intrinsicWidth;
                    videoHeight = (videoWidth * intrinsicHeight) / intrinsicWidth;
                    mVideoThumbnail.setImageDrawable(resource);
                }
            });
        }
        mVideoView.setVideoPath(proxyUrl);
    }

    public void setVideoParameter(String thumvnail, String videoPath, final int windowsWight) {
        mVideoPath = videoPath;

        Log.e(TAG, "thumvnail === " + thumvnail);
        Log.e(TAG, "videoPath === " + videoPath);
        Glide.with(mContext).load(thumvnail).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                int intrinsicWidth = resource.getIntrinsicWidth();
                int intrinsicHeight = resource.getIntrinsicHeight();

                videoWidth = windowsWight;
                videoHeight = (videoWidth * intrinsicHeight) / intrinsicWidth;

                Log.e(TAG, "videoWidth === " + videoWidth);
                Log.e(TAG, "videoHeight === " + videoHeight);

                mVideoThumbnail.setImageDrawable(resource);

                if (onSizeVideoClickListener != null) {
                    onSizeVideoClickListener.onLoadedVideoClick(videoWidth, videoHeight);
                }

            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);

                if (onSizeVideoClickListener != null) {
                    onSizeVideoClickListener.onLoadedVideoClick(0, 0);
                }
            }
        });

        String proxyUrl = mProxy.getProxyUrl(mVideoPath);
        Log.e(TAG, "proxyUrl == " + proxyUrl);
        mVideoView.setVideoPath(proxyUrl);
    }

    /**
     * 设置视频的宽和高
     *
     * @param videoWidth  :视频宽度
     * @param videoHeight ：视频高度
     */
    private void setVideoSize(int videoWidth, int videoHeight) {
//        int width = getWidth();
//        int height = getHeight();
//
//        RelativeLayout.LayoutParams layoutParams;
//        if (videoWidth == videoHeight) {
//            layoutParams = new RelativeLayout.LayoutParams(width > height ? height : width, width > height ? height : width);
//        } else if (videoWidth > videoHeight) {
//            layoutParams = new RelativeLayout.LayoutParams(width, width * videoHeight / videoWidth);
//        } else {
//            layoutParams = new RelativeLayout.LayoutParams(height * videoWidth / videoHeight, height);
//        }
//
//        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
//
//        mVideoView.setLayoutParams(layoutParams);
//
//        if (onSizeVideoClickListener != null) {
//            onSizeVideoClickListener.onLoadedVideoClick(width, height);
//        }

        mVideoView.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoView.getWidth();
                int height = mVideoView.getHeight();
                //视频宽高
                mVideoView.setFixedSize(width, height);
                mVideoView.invalidate();
                //缩略图宽高
                ViewGroup.LayoutParams layoutParams = mVideoThumbnail.getLayoutParams();
                layoutParams.width = width;
                layoutParams.height = height;
                mVideoThumbnail.setLayoutParams(layoutParams);
            }
        });
    }

    /**
     * 把毫秒转化为 mm:ss格式
     *
     * @return
     */
    private String msConversion(int ms) {
        String results;

        int seconds = ms / 1000;            //多少秒
        int points = seconds / 60;          //多少分
        int remaMs = seconds % 60;          //总数 - 分钟 = 剩余秒数

        if (points == 0) {        //时间不超过一分钟
            String s = (seconds < 10) ? "0" + seconds : seconds + "";
            results = "00:" + s;
        } else {             //时间超过一分钟
            String s = (points < 10) ? "0" + points : points + "";

            String s1 = (remaMs < 10) ? "0" + remaMs : remaMs + "";
            results = s + ":" + s1;
        }

        return results;
    }

    /**
     * 视频播放状态
     *
     * @param visibility：显示或者隐藏
     */
    private void setVideoViewState(int visibility) {
        setVideoViewState(mVideoViewState, visibility);
    }

    /**
     * 视频播放状态
     *
     * @param videoViewState    ：视频状态
     * @param visibility：显示或者隐藏
     */
    public void setVideoViewState(VideoViewState videoViewState, int visibility) {
        if (mVideoViewState != videoViewState) {
            mVideoToPlayControl.removeAllViews();
            mVideoViewState = videoViewState;
        }
        mVideoToPlayControl.setVisibility(visibility);
        if (mVideoToPlayControl.getVisibility() == VISIBLE) {
            switch (videoViewState) {
                case START_PLAY:               //开始播放
                    View startPlayView = mInflater.inflate(R.layout.video_start_play, mVideoToPlayControl);
                    ImageView startPlay = startPlayView.findViewById(R.id.yuemei_viodeo_start_play);
                    startPlay.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            videoStartPlayer();
                        }
                    });
                    break;
                case TO_PLAY:                  //重新播放
                    View toPlayView = mInflater.inflate(R.layout.video_to_play, mVideoToPlayControl);
                    ImageView toPlay = toPlayView.findViewById(R.id.yuemei_viodeo_to_play);
                    toPlay.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            videoStartPlayer();
                        }
                    });

                    break;
                case IN_LOAD:                  //加载中
                    View inLoadView = mInflater.inflate(R.layout.video_in_load, mVideoToPlayControl);
                    ImageView inLoad = inLoadView.findViewById(R.id.yuemei_viodeo_in_load);
                    TextView networkSpeed = inLoadView.findViewById(R.id.yuemei_viodeo_network_speed);

                    inLoad.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));//动画设置
                    networkSpeed.setText(showNetSpeed());       //网速设置

                    break;
                case NETWORK_ERROR:             //网络中断
                    View networkErrorView = mInflater.inflate(R.layout.video_network_error, mVideoToPlayControl);
                    ImageView networRetry = networkErrorView.findViewById(R.id.network_error_retry);
                    networRetry.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            videoStartPlayer();
                        }
                    });
                    break;
                case VIDEO_ERROR:              //视频源错误

                    mInflater.inflate(R.layout.video_video_error, mVideoToPlayControl);

                    break;
                case NOT_WIFI:            //非wifi下提示
                    View notWifiView = mInflater.inflate(R.layout.video_not_wifi, mVideoToPlayControl);
                    TextView notWifi = notWifiView.findViewById(R.id.video_not_wifi_continue);
                    notWifi.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.getAPNType(mContext) == 1) {
                                //当前是Wifi网络时
                                videoStartPlayer();
                            } else {
                                showDialogExitEdit();
                            }
                        }
                    });
                    break;
            }

        }
    }

    /**
     * 当前不是wifi网络提示
     */
    private void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_phone_zhuan);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
        titleTv77.setText("非wifi提示");

        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("您当前处于非wifi下，确定继续浏览该视频？");

        titleTv88.setHeight(50);

        //确定
        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Cfg.saveStr(mContext, FinalConstant.NOT_WIFI_PLAY, "1");
                videoStartPlayer();
                editDialog.dismiss();
            }
        });

        //取消
        Button cancelBt99 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt99.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    /**
     * 获取当前网速
     *
     * @param context
     * @return
     */
    private long lastTotalRxBytes = 0;
    private long lastTimeStamp = 0;

    public String showNetSpeed() {
        long nowTotalRxBytes = TrafficStats.getUidRxBytes(mContext.getApplicationInfo().uid) == TrafficStats.UNSUPPORTED ? 0 : (TrafficStats.getTotalRxBytes() / 1024);//转为KB;
        long nowTimeStamp = System.currentTimeMillis();
        long speed = ((nowTotalRxBytes - lastTotalRxBytes) * 1000 / (nowTimeStamp - lastTimeStamp));//毫秒转换
        lastTimeStamp = nowTimeStamp;
        lastTotalRxBytes = nowTotalRxBytes;
        String Netspeed = String.valueOf(speed) + " kb/s";
        return Netspeed;
    }

    /**
     * 控制器显示隐藏时间
     *
     * @param time
     */
    public void setVideoPlayerTime(int time) {
        this.videoPlayerTime = time;
    }

    /**
     * 设置视频背景
     *
     * @param color
     */
    public void setVideoPlayerBackground(int color) {
        mVideoViewContainer.setBackgroundResource(color);
    }

    /**
     * 设置是否使用网络监听
     *
     * @param isNo
     */
    public void setNetworkChanges(boolean isNo) {
        this.networkChangesisNo = isNo;
    }

    /**
     * 是否重复播放
     *
     * @param isReplay
     */
    public void setReplay(boolean isReplay) {
        this.isReplay = isReplay;
    }

    /**
     * 设置可以拖动进度条的最小时间
     *
     * @param time
     */
    public void setSeekVideoTime(int time) {
        seekBarTime = time;
    }

    class VideoOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        /**
         * 当SeekBar 改变的时候回调这个方法
         *
         * @param seekBar
         * @param progress
         * @param fromUser 自动false,用户操作true（自己播放是false,用户拖动是true）
         */
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {     //只响应用户拖动操作
                setSeekTo(progress);
            }
            mVideoStartTime.setText(msConversion(progress));     //视频进行时间
        }

        /**
         * 拖动按下时调用
         *
         * @param seekBar
         */
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            videoRemoveMessage(CONTROL_HIDDEN);
        }

        /**
         * 拖动松开时调用
         *
         * @param seekBar
         */
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mHandler.sendEmptyMessageDelayed(CONTROL_HIDDEN, videoPlayerTime);
        }
    }

    //放大视频的监听
    private OnMaxVideoClickListener onMaxVideoClickListener;

    public interface OnMaxVideoClickListener {
        void onMaxVideoClick(View v);
    }

    public void setOnMaxVideoClickListener(OnMaxVideoClickListener onMaxVideoClickListener) {
        this.onMaxVideoClickListener = onMaxVideoClickListener;
    }

    //视频宽高获取
    private OnSizeVideoClickListener onSizeVideoClickListener;

    public interface OnSizeVideoClickListener {

        void onLoadedVideoClick(int width, int height);
    }

    public void setOnSizeVideoClickListener(OnSizeVideoClickListener onSizeVideoClickListener) {
        this.onSizeVideoClickListener = onSizeVideoClickListener;
    }

    //视频播放器显示隐藏
    private OnProgressBarStateClick onProgressBarStateClick;

    public interface OnProgressBarStateClick {
        void onProgressBarStateClick(int visibility);
    }

    public void setOnProgressBarStateClick(OnProgressBarStateClick onProgressBarStateClick) {
        this.onProgressBarStateClick = onProgressBarStateClick;
    }

    //视频进度回调
    private OnProgressClick onProgressClick;

    public interface OnProgressClick {
        void onProgressClickk(int pogress, int duration);
    }

    public void setOnProgressClick(OnProgressClick onProgressClick) {
        this.onProgressClick = onProgressClick;
    }

    //视频播放暂停按钮点击回调
    private OnControllerClick onControllerClick;

    public interface OnControllerClick {
        void onControllerClickk(boolean isPlaying);
    }

    public void setOnControllerClick(OnControllerClick onControllerClick) {
        this.onControllerClick = onControllerClick;
    }

    //播放完成回调
    private OnCompletionListener onCompletionListener;

    public interface OnCompletionListener {
        void onCompletionListener(int videoWidth,int videoHeight);
    }

    public void setOnCompletionListener(OnCompletionListener onCompletionListener) {
        this.onCompletionListener = onCompletionListener;
    }
}
