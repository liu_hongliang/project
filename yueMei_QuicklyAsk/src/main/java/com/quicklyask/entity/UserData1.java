package com.quicklyask.entity;

import java.util.List;

public class UserData1 {


	/**
	 * _id : 85299573
	 * img : https://p31.yuemei.com/avatar/085/29/95/73_avatar_120_120.jpg?ver=1524021200
	 * nickname : 我一个小时的
	 * sex : 2
	 * birthday : 1990-6-1
	 * province : 江西
	 * city : 通辽
	 * evaluate : 1
	 * integral : 5209
	 * level : 1
	 * alipay :
	 * real_name :
	 * card_id :
	 * full_name :
	 * mobile :
	 * doparts : [{"id":"78","name":"眼部","checked":"0"},{"id":"134","name":"鼻部","checked":"0"},{"id":"292","name":"面部轮廓","checked":"0"},{"id":"392","name":"除皱紧致提升","checked":"0"},{"id":"538","name":"皮肤美容美妆","checked":"0"},{"id":"591","name":"胸部","checked":"0"},{"id":"640","name":"减肥塑形","checked":"0"}]
	 * parts : [{"id":"78","name":"眼部","checked":"0"},{"id":"134","name":"鼻部","checked":"1"},{"id":"206","name":"唇部整形","checked":"0"},{"id":"292","name":"面部轮廓","checked":"0"},{"id":"329","name":"瘦脸","checked":"0"},{"id":"359","name":"垫下巴","checked":"0"},{"id":"392","name":"除皱紧致提升","checked":"0"},{"id":"538","name":"皮肤美容美妆","checked":"0"},{"id":"542","name":"美白","checked":"0"},{"id":"591","name":"胸部","checked":"0"},{"id":"640","name":"减肥塑形","checked":"0"},{"id":"688","name":"瘦手臂（蝴蝶袖）","checked":"0"},{"id":"691","name":"瘦大腿","checked":"0"},{"id":"701","name":"瘦小腿","checked":"0"},{"id":"8063","name":"瘦腰腹（游泳圈）","checked":"0"},{"id":"8064","name":"半永久化妆","checked":"0"},{"id":"8271","name":"祛斑","checked":"0"}]
	 * mynumber : 152*****731
	 * talent : 0
	 * yuemeiinfo : rBLZcgVejt1WMGv375vUDFeqwnJzBsUCVeWt31IZQorVf0sH93VXIeW7UZRFlmnt3FId5qH+rc0NWM5q1Of0bZlvdo5z7vi5mMB0P9RMPDyUOMRFGiNgjUew1T/+En8TUsVsKoGX3JSRQnJFXic8mp1qXYGXKS3ZEtTieZ0RUvD1g0Yy9bLwhD5zKVI0gIbV
	 * group_id : 1
	 * loginphone : 152*****731
	 * phone : 152*****731
	 * isphone : 1
	 * yqma : 0
	 * sixin : 0
	 * postnum : 0
	 * replynum : 17265
	 * signButtonTitle : 签到 +2颜值币
	 * sharenum : 1
	 * followingnum : 0
	 * followingmenum : 0
	 * browsenum : 0
	 * nopaynum : 0
	 * paynum : 0
	 * nodiarynum : 1
	 * is_show : 0
	 * is_signin : 0
	 */

	private String _id;
	private String img;
	private String nickname;
	private String sex;
	private String birthday;
	private String province;
	private String city;
	private String evaluate;
	private String integral;
	private String level;
	private String alipay;
	private String real_name;
	private String card_id;
	private String full_name;
	private String mobile;
	private String mynumber;
	private String talent;
	private String yuemeiinfo;
	private String group_id;
	private String loginphone;
	private String phone;
	private String isphone;
	private String yqma;
	private String sixin;
	private String postnum;
	private String replynum;
	private String signButtonTitle;
	private String sharenum;
	private String followingnum;
	private String followingmenum;
	private String browsenum;
	private String nopaynum;
	private String paynum;
	private String nodiarynum;
	private String is_show;
	private String is_signin;
	private List<UserDopartsData> doparts;
	private List<UserDopartsData> parts;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEvaluate() {
		return evaluate;
	}

	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}

	public String getIntegral() {
		return integral;
	}

	public void setIntegral(String integral) {
		this.integral = integral;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}

	public String getReal_name() {
		return real_name;
	}

	public void setReal_name(String real_name) {
		this.real_name = real_name;
	}

	public String getCard_id() {
		return card_id;
	}

	public void setCard_id(String card_id) {
		this.card_id = card_id;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMynumber() {
		return mynumber;
	}

	public void setMynumber(String mynumber) {
		this.mynumber = mynumber;
	}

	public String getTalent() {
		return talent;
	}

	public void setTalent(String talent) {
		this.talent = talent;
	}

	public String getYuemeiinfo() {
		return yuemeiinfo;
	}

	public void setYuemeiinfo(String yuemeiinfo) {
		this.yuemeiinfo = yuemeiinfo;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getLoginphone() {
		return loginphone;
	}

	public void setLoginphone(String loginphone) {
		this.loginphone = loginphone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIsphone() {
		return isphone;
	}

	public void setIsphone(String isphone) {
		this.isphone = isphone;
	}

	public String getYqma() {
		return yqma;
	}

	public void setYqma(String yqma) {
		this.yqma = yqma;
	}

	public String getSixin() {
		return sixin;
	}

	public void setSixin(String sixin) {
		this.sixin = sixin;
	}

	public String getPostnum() {
		return postnum;
	}

	public void setPostnum(String postnum) {
		this.postnum = postnum;
	}

	public String getReplynum() {
		return replynum;
	}

	public void setReplynum(String replynum) {
		this.replynum = replynum;
	}

	public String getSignButtonTitle() {
		return signButtonTitle;
	}

	public void setSignButtonTitle(String signButtonTitle) {
		this.signButtonTitle = signButtonTitle;
	}

	public String getSharenum() {
		return sharenum;
	}

	public void setSharenum(String sharenum) {
		this.sharenum = sharenum;
	}

	public String getFollowingnum() {
		return followingnum;
	}

	public void setFollowingnum(String followingnum) {
		this.followingnum = followingnum;
	}

	public String getFollowingmenum() {
		return followingmenum;
	}

	public void setFollowingmenum(String followingmenum) {
		this.followingmenum = followingmenum;
	}

	public String getBrowsenum() {
		return browsenum;
	}

	public void setBrowsenum(String browsenum) {
		this.browsenum = browsenum;
	}

	public String getNopaynum() {
		return nopaynum;
	}

	public void setNopaynum(String nopaynum) {
		this.nopaynum = nopaynum;
	}

	public String getPaynum() {
		return paynum;
	}

	public void setPaynum(String paynum) {
		this.paynum = paynum;
	}

	public String getNodiarynum() {
		return nodiarynum;
	}

	public void setNodiarynum(String nodiarynum) {
		this.nodiarynum = nodiarynum;
	}

	public String getIs_show() {
		return is_show;
	}

	public void setIs_show(String is_show) {
		this.is_show = is_show;
	}

	public String getIs_signin() {
		return is_signin;
	}

	public void setIs_signin(String is_signin) {
		this.is_signin = is_signin;
	}

	public List<UserDopartsData> getDoparts() {
		return doparts;
	}

	public void setDoparts(List<UserDopartsData> doparts) {
		this.doparts = doparts;
	}

	public List<UserDopartsData> getParts() {
		return parts;
	}

	public void setParts(List<UserDopartsData> parts) {
		this.parts = parts;
	}
}
