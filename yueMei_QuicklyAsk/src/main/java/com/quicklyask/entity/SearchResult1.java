package com.quicklyask.entity;

public class SearchResult1 {
	private String code;
	private String message;
	private SearchResultData1 data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SearchResultData1 getData() {
		return data;
	}

	public void setData(SearchResultData1 data) {
		this.data = data;
	}

}
