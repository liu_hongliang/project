package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.UserBrowsetaoApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.UserBrowseTao;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 李鹏 on 2017/03/01 0001.
 */

@SuppressLint({"SetTextI18n", "InflateParams"})
public class DropDownMultiPagerView extends Dialog {

    private Context context;
    private MultiViewPager pager;
    private OnDropDownMultiPagerViewItemClick onDropDownMultiPagerViewItemClick;
    private int mTouchSlop;
    private int mWindowW;
    private int mLeftRegion;
    private String mSkuId;
    private ArrayList<UserBrowseTao> mUserBrowseTaos;
    private List<View> mList;
    private int currentPos = 0;


    public DropDownMultiPagerView(Context context, String skuId) {
        super(context, R.style.DropDown);
        this.context = context;
        this.mSkuId = skuId;
    }

    public void setOnDropDownMultiPagerViewItemClick(OnDropDownMultiPagerViewItemClick onDropDownMultiPagerViewItemClick) {
        this.onDropDownMultiPagerViewItemClick = onDropDownMultiPagerViewItemClick;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_dropdownfootprint, null);

        setContentView(view);
        setCanceledOnTouchOutside(true);
        setCancelable(true);

        Window dialogWindow = getWindow();
        assert dialogWindow != null;
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        lp.width = dm.widthPixels;
        int windowH = Cfg.loadInt(context, FinalConstant.WINDOWS_H, 0);
        mWindowW = Cfg.loadInt(context, FinalConstant.WINDOWS_W, 0);
        mLeftRegion = mWindowW / 3;
        int relH = (int) (windowH * 0.32f);
        int dip2px = Utils.px2dip(1920);
        lp.height = dip2px(context, 290);
        dialogWindow.setAttributes(lp);
        dialogWindow.setGravity(Gravity.TOP);
        dialogWindow.setWindowAnimations(R.style.DropDown);

        //解决 状态栏变色的bug
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            dialogWindow.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            dialogWindow.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            dialogWindow.setStatusBarColor(Color.TRANSPARENT);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                try {
//                    Class decorViewClazz = Class.forName("com.android.internal.policy.DecorView");
//                    Field field = decorViewClazz.getDeclaredField("mSemiTransparentStatusBarColor");
//                    field.setAccessible(true);
//                    field.setInt(getWindow().getDecorView(), Color.WHITE);  //去掉高版本蒙层改为透明
//                } catch (Exception e) {
//                }
//            }
//        }


        pager = (MultiViewPager) view.findViewById(R.id.pager);

        pager.setPageTransformer(true, new ZoomPageTransformer());
        ViewConfiguration configuration = ViewConfiguration.get(context);
        mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(configuration);

        onclick();

        setTopRefreshData();


    }


    /**
     * 头部下拉数据
     */
    private void setTopRefreshData() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("tao_id", mSkuId);
        hashMap.put("uid", Utils.getUid());
        new UserBrowsetaoApi().getCallBack(context, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    if (mUserBrowseTaos != null) {
                        mUserBrowseTaos = null;
                    }
                    mUserBrowseTaos = JSONUtil.jsonToArrayList(serverData.data, UserBrowseTao.class);
                    if (null != mUserBrowseTaos) {
                        mList = new ArrayList<>();
                        for (int i = 0; i < mUserBrowseTaos.size(); i++) {
                            DropDownMultiPagerItem item = new DropDownMultiPagerItem(context, i, mUserBrowseTaos);
                            if (i == 0){
                                item.setTextNum("我的足迹（" + (i + 1) + "/" + mUserBrowseTaos.size() + ")");
                            }
                            mList.add(item);

                        }

                        DropDownMultiPagerAdapter adapter = new DropDownMultiPagerAdapter(mList);
                        pager.setAdapter(adapter);
                        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int i, float v, int i1) {

                            }

                            @Override
                            public void onPageSelected(int i) {
                                DropDownMultiPagerItem view = (DropDownMultiPagerItem) mList.get(i);
                                view.setTextNum("我的足迹（" + (i + 1) + "/" + mList.size() + ")");

                                DropDownMultiPagerItem view1 = (DropDownMultiPagerItem) mList.get(currentPos);
                                view1.setTextNum("");
                                currentPos = i;
                            }

                            @Override
                            public void onPageScrollStateChanged(int i) {

                            }
                        });
                        adapter.notifyDataSetChanged();

                    } else {
                        if (Utils.isLogin()) {
                            MyToast.makeTextToast2(context, serverData.message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    private int dip2px(Context context, float dpValue) {
        try {
            return (int) (dpValue * context.getResources().getDisplayMetrics().density + 0.5f);
        } catch (Exception e) {
            e.printStackTrace();
            return (int) (dpValue * 1 + 0.5f);
        }
    }


    private boolean clickable = false;

    /**
     * 点击事件
     */
    @SuppressLint("ClickableViewAccessibility")
    private void onclick() {
        pager.setOnTouchListener(new View.OnTouchListener() {
            float x = 0, y = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        clickable = true;
                        x = event.getX();
                        y = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        float xDiff = Math.abs(event.getX() - x);
                        float yDiff = Math.abs(event.getY() - y);
                        if (xDiff < mTouchSlop && xDiff >= yDiff)
                            clickable = true;
                        else
                            clickable = false;
                        break;
                    case MotionEvent.ACTION_UP:
                        if (clickable) {
                            int currentItem = pager.getCurrentItem();
                            float x = event.getX();
                            if (x < mLeftRegion) {
                                if (currentItem != 0) {
                                    pager.setCurrentItem(currentItem - 1);
                                }
                            } else if (x > mLeftRegion && x < (mLeftRegion * 2)) {
                                String id = mUserBrowseTaos.get(currentItem).getId();
                                HashMap<String, String> eventParams = mUserBrowseTaos.get(currentItem).getEvent_params();
                                onDropDownMultiPagerViewItemClick.onItemClick(currentItem, id,eventParams);
                            } else {
                                if (currentItem < mUserBrowseTaos.size() - 1)
                                    pager.setCurrentItem(currentItem + 1);
                            }
                        }
                        break;

                }
                return false;
            }
        });
    }


    public interface OnDropDownMultiPagerViewItemClick {
        void onItemClick(int position, String skuId, HashMap<String, String> eventParams);
    }
}
