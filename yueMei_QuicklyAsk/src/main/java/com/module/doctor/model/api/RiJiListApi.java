package com.module.doctor.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.community.model.bean.BBsListData550;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Administrator on 2017/10/25.
 */

public class RiJiListApi implements BaseCallBackApi {
    private String TAG = "RiJiListApi";
    private String json;

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.DOCTOR, "usershare", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                json = mData.data;
                if ("1".equals(mData.code)){
                    try {
                        ArrayList<BBsListData550> bBsListData550 = JSONUtil.jsonToArrayList(mData.data, BBsListData550.class);
                        listener.onSuccess(bBsListData550);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
