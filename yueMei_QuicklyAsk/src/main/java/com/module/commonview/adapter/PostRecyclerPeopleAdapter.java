package com.module.commonview.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.PostPeoplePeopleList;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * 帖子人数列表
 * Created by 裴成浩 on 2018/8/21.
 */
public class PostRecyclerPeopleAdapter extends RecyclerView.Adapter<PostRecyclerPeopleAdapter.ViewHolder> {

    private int windowsWight;
    private Activity mContext;
    private List<PostPeoplePeopleList> mDatas;
    private final LayoutInflater mInflater;
    private String TAG = "PostRecyclerPeopleAdapter";

    public PostRecyclerPeopleAdapter(Activity context, List<PostPeoplePeopleList> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(context);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_post_people_gridview, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PostPeoplePeopleList postPeoplePeopleList = mDatas.get(position);
        String userImg = postPeoplePeopleList.getUserImg();

        //图片大小
        int userImgSize = (windowsWight - Utils.dip2px(40) - (8 * Utils.dip2px(7))) / 9;

        //设置图片宽高
        ViewGroup.LayoutParams layoutParams = holder.mPostPeople.getLayoutParams();
        layoutParams.width = userImgSize;
        layoutParams.height = userImgSize;
        holder.mPostPeople.setLayoutParams(layoutParams);

        //加载图片
        Glide.with(mContext).load(userImg).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.mPostPeople);

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mPostPeople;

        public ViewHolder(View itemView) {
            super(itemView);
            mPostPeople = itemView.findViewById(R.id.item_post_people_gridview);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemCallBackListener != null) {
                        itemCallBackListener.onItemClick(v, mDatas.get(getLayoutPosition()).getUserUrl());
                    }
                }
            });
        }
    }

    public List<PostPeoplePeopleList> getmDatas() {
        return mDatas;
    }

    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, String userUrl);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }
}