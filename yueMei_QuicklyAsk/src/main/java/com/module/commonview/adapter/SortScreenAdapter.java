package com.module.commonview.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.commonview.view.DoubleSlideSeekBar;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 筛选活动
 * Created by 裴成浩 on 2019/5/17
 */
public class SortScreenAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mContext;
    private List<ProjcetData> mDatas;

    private final int ITEM_TYPE_ONE = 1;
    private final int ITEM_TYPE_TWO = 2;
    private final LayoutInflater mLayoutInflater;
    private HashMap<Integer, SortScreenItemAdapter> mHashMap = new HashMap<>();

    public SortScreenAdapter(Activity content, ArrayList<ProjcetData> data) {
        this.mContext = content;
        this.mDatas = data;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getItemViewType(int position) {
        switch (mDatas.get(position).getId()) {
            case 3:
                return ITEM_TYPE_TWO;
            default:
                return ITEM_TYPE_ONE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_TWO:
                return new ViewHolder2(mLayoutInflater.inflate(R.layout.item_sort_screen_view2, viewGroup, false));
            default:
                return new ViewHolder1(mLayoutInflater.inflate(R.layout.item_sort_screen_view1, viewGroup, false));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder1) {
            setView1((ViewHolder1) holder, position);
        } else if (holder instanceof ViewHolder2) {
            setView2((ViewHolder2) holder, position);
        }
    }

    //设置样式1
    private void setView1(ViewHolder1 holder, int position) {
        ProjcetData data = mDatas.get(position);
        //设置标题
        holder.mName1.setText(data.getName());

        //设置适配器
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 3);
        gridLayoutManager.setScrollEnable(false);
        for (ProjcetList dataasdas : data.getList()) {
            Log.e("adasdasdasas", "dataasdas11111 == " + dataasdas.getTitle());
        }
        SortScreenItemAdapter sortScreenItemAdapter = new SortScreenItemAdapter(mContext, data.getList());
        holder.mlist1.setLayoutManager(gridLayoutManager);
        holder.mlist1.setAdapter(sortScreenItemAdapter);

        mHashMap.put(data.getId(), sortScreenItemAdapter);
    }


    //设置样式2
    private void setView2(ViewHolder2 holder, int position) {
        ProjcetData data = mDatas.get(position);
        //设置标题
        holder.mName2.setText(data.getName());

        //设置价格进度条
//        holder.mSeek2.setProgressLow(0);                 //滑动前的数据
//        holder.mSeek2.setProgressHigh(20000);            //滑动后的数据

        //设置适配器
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 3);
        gridLayoutManager.setScrollEnable(false);
        for (ProjcetList dataasdas : data.getList()) {
            Log.e("adasdasdasas", "dataasdas222222 == " + dataasdas.getTitle());
        }
        SortScreenItemAdapter sortScreenItemAdapter = new SortScreenItemAdapter(mContext, data.getList(),true);
        holder.mlist2.setLayoutManager(gridLayoutManager);
        holder.mlist2.setAdapter(sortScreenItemAdapter);
        mHashMap.put(data.getId(), sortScreenItemAdapter);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    //样式一
    public class ViewHolder1 extends RecyclerView.ViewHolder {
        TextView mName1;
        RecyclerView mlist1;

        public ViewHolder1(@NonNull View itemView) {
            super(itemView);
            mName1 = itemView.findViewById(R.id.sort_screen_popwin_name1);
            mlist1 = itemView.findViewById(R.id.sort_screen_popwin_list1);
        }
    }

    //样式二
    public class ViewHolder2 extends RecyclerView.ViewHolder {
        TextView mName2;
        DoubleSlideSeekBar mSeek2;
        RecyclerView mlist2;

        public ViewHolder2(@NonNull View itemView) {
            super(itemView);
            mName2 = itemView.findViewById(R.id.sort_screen_popwin_name2);
            mSeek2 = itemView.findViewById(R.id.sort_screen_popwin_seek2);
            mlist2 = itemView.findViewById(R.id.sort_screen_popwin_list2);

        }
    }

    /**
     * 获取选中数据
     *
     * @return : 选中的数据集合
     */
    public ArrayList<ProjcetList> getSelectedData() {
        ArrayList<ProjcetList> selectedData = new ArrayList<>();
        for (ProjcetData data : mDatas) {
            selectedData.addAll(mHashMap.get(data.getId()).getSelectedData());
        }
        return selectedData;
    }

    /**
     * 重置数据
     */
    public void resetData() {
        for (int i = 0; i < mDatas.size(); i++) {
            ProjcetData data = mDatas.get(i);
            SortScreenItemAdapter sortScreenItemAdapter = mHashMap.get(data.getId());
            sortScreenItemAdapter.resetData();
        }
    }

}
