package com.module.community.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.base.view.FunctionManager;
import com.module.community.model.bean.BBsListData550;
import com.module.community.model.bean.Img310;
import com.module.community.model.bean.TaoListData;
import com.module.community.model.bean.TaoListDataEnum;
import com.module.community.model.bean.TaoListDataType;
import com.module.community.model.bean.TaoListDoctors;
import com.module.community.model.bean.TaoListDoctorsCompared;
import com.module.community.model.bean.TaoListDoctorsComparedData;
import com.module.community.model.bean.TaoListDoctorsEnum;
import com.module.community.model.bean.TaoListType;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebData;
import com.module.community.web.WebUtil;
import com.module.home.model.bean.ComparedBean;
import com.module.home.model.bean.DcotorComparData;
import com.module.home.model.bean.SearchActivityData;
import com.module.home.model.bean.SearchResultDoctor;
import com.module.home.model.bean.SearchResultDoctorControlParams;
import com.module.home.model.bean.SearchResultDoctorList;
import com.module.home.model.bean.SearchResultLike;
import com.module.home.model.bean.SearchResultTaoData;
import com.module.home.model.bean.SearchResultTaoData2;
import com.module.home.model.bean.SearchTao;
import com.module.home.model.bean.SearchTaolistData;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.Promotion;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.ItemTimerTextView;
import com.quicklyask.view.MyToast;

import org.apache.commons.collections.CollectionUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaoListStaggeredAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = "TaoListStaggeredAdapter";
    private Activity mContext;

    private FunctionManager mFunctionManager;
    private LayoutInflater mLayoutInflater;

    private final int DATA = 1;
    private final int DOCTOR_LIST = 2;
    private final int LIKE = 3;
    private final int NOT_MORE = 4;
    private final int FLASH_SALE = 5;//秒杀
    private final int LEADER_BOARD = 6;//月销量排行
    private final int BIG_PROMOTION = 7;//大促
    private final int RECOMMEND = 8;//推荐

    private List<TaoListData> mTaoDatas;
    private int windowsWight;
    private boolean isHeadView = false;
    private  int itemWidth;
    private LinkedHashMap<Integer, Integer> imgHeights = new LinkedHashMap();


    /**
     * 搜索页面多类型
     *
     * @param context
     * @param data
     */
    public TaoListStaggeredAdapter(Activity context, SearchResultTaoData2 data) {
        initData(context, data);
        windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
        itemWidth = (windowsWight - Utils.dip2px(15)) / 2;
    }

    private void initData(Activity context, SearchResultTaoData2 data) {
        this.mContext = context;
        mFunctionManager = new FunctionManager(mContext);
        mLayoutInflater = LayoutInflater.from(mContext);
        mTaoDatas = setSearctTaoListData(data);
    }



    private List<TaoListData> setSearctTaoListData(SearchResultTaoData2 data) {
        //调整后的数据
        List<TaoListData> lists = new ArrayList<>();

        List<SearchTao> mTaoDatas = data.getList();

        List<SearchTao> mRecomendList = data.getRecomend_list();
        List<SearchResultLike> mLikeLists = data.getParts();
        String mRecomendTips = data.getRecomend_tips();

        //要插入的数据
        @SuppressLint("UseSparseArrays")
        Map<Integer, TaoListData> insertData = new HashMap<>();
        //主数据设置
        setTaoSearchData(mTaoDatas, TaoListDataEnum.LIST, lists);

        //设置对比咨询数据
        int firstInsert=0;
        for (SearchTao mTaoData : mTaoDatas) {
            DcotorComparData compared = mTaoData.getCompared();
            if (compared != null){
                ComparedBean comparedBean = compared.getCompared();
                String showSkuListPosition = comparedBean.getShowSkuListPosition();
                int showSkuListPositionA = Integer.parseInt(showSkuListPosition);

                TaoListDoctorsCompared taoListDoctorsCompared = new TaoListDoctorsCompared();
                if (firstInsert == 0){
                    taoListDoctorsCompared.setDoctorsEnum(TaoListDoctorsEnum.TOP);
                    firstInsert ++;
                }else {
                    taoListDoctorsCompared.setDoctorsEnum(TaoListDoctorsEnum.BOTTOM);
                }

                taoListDoctorsCompared.setShowSkuListPosition(comparedBean.getShowSkuListPosition());
                taoListDoctorsCompared.setChangeOneEventParams(comparedBean.getChangeOneEventParams());
                taoListDoctorsCompared.setExposureEventParams(comparedBean.getExposureEventParams());

                List<TaoListDoctorsComparedData> taoListDoctorsComparedData = new ArrayList<>();

                List<List<SearchResultDoctorList>> doctorsList = comparedBean.getDoctorsList();
                for (int i = 0; i < doctorsList.size(); i++) {
                    List<SearchResultDoctorList> searchResultDoctorLists = doctorsList.get(i);
                    TaoListDoctorsComparedData taoListDoctorsComparedData1 = new TaoListDoctorsComparedData();
                    taoListDoctorsComparedData1.setList(searchResultDoctorLists);
                    if (i == 0) {
                        taoListDoctorsComparedData1.setSelected(true);
                    } else {
                        taoListDoctorsComparedData1.setSelected(false);
                    }
                    taoListDoctorsComparedData.add(taoListDoctorsComparedData1);
                }

                taoListDoctorsCompared.setDoctorsList(taoListDoctorsComparedData);


                TaoListDoctors taoListDoctors = new TaoListDoctors();
                taoListDoctors.setComparedTitle(compared.getComparedTitle());
                taoListDoctors.setCompared(taoListDoctorsCompared);
                if (showSkuListPositionA < lists.size()){
                    lists.get(showSkuListPositionA).setDoctors(taoListDoctors);
                }else {
                    lists.get(lists.size()-1).setDoctors(taoListDoctors);
                }
            }
        }

        //猜你喜欢数据
        if (mLikeLists != null && mLikeLists.size() > 0) {
            TaoListData taoListData = new TaoListData();
            taoListData.setLikeList(mLikeLists);
            taoListData.setType(TaoListType.LIKE);
            insertData.put(11, taoListData);
        }

        //排序
        List<Integer> keys = new ArrayList<>(insertData.keySet());
        Collections.sort(keys);                 //正序排序
        Collections.reverse(keys);              //反转之后变成倒序 排序
        for (Integer key : keys) {
            Log.e(TAG, "key == " + key);
        }

        //没有更多数据提示
        if (!TextUtils.isEmpty(mRecomendTips) && mRecomendList != null && mRecomendList.size() > 0) {
            TaoListData taoListData = new TaoListData();
            taoListData.setRecomendTips(mRecomendTips);
            taoListData.setType(TaoListType.NOT_MORE);
            lists.add(taoListData);
        }

        //推荐数据设置
        setTaoSearchData(mRecomendList, TaoListDataEnum.RECOMMENDED, lists);

        return lists;
    }

    /**
     * 设置淘列表+推荐列表数据
     *
     * @param datas     ：要插入的数据
     * @param list      ：插入数据类型
     * @param mTaoDatas ：要插入到的父数据列表
     */
    private void setTaoData(List<HomeTaoData> datas, TaoListDataEnum list, List<TaoListData> mTaoDatas) {
        if (datas != null && datas.size() > 0) {
            for (HomeTaoData tao : datas) {
                TaoListDataType taoListDataType = new TaoListDataType();
                taoListDataType.setType(list);
                taoListDataType.setTao(tao);
                TaoListData taoListData = new TaoListData();
                taoListData.setType(TaoListType.DATA);
                taoListData.setTaoList(taoListDataType);
                mTaoDatas.add(taoListData);
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        TaoListData taoListData = mTaoDatas.get(position);
        TaoListDataType taoList = taoListData.getTaoList();
        if (taoList != null){
            SearchTao searchTao = taoListData.getTaoList().getSearchTao();
            if (searchTao != null){
                TaoListType type = searchTao.getTaolistType();
                Log.e(TAG,"position ==>"+position);
                if (type != null) {
                    switch (type) {
                        case DATA:
                            return DATA;
                        case DOCTOR_LIST:
                            return DOCTOR_LIST;
                        case BIG_PROMOTION:
                            return BIG_PROMOTION;
                        case FLASH_SALE:
                            return FLASH_SALE;
                        case LEADER_BOARD:
                            return LEADER_BOARD;
                        case RECOMMEND:
                            return RECOMMEND;
                        case NOT_MORE:
                            return NOT_MORE;
                    }
                }
            }
        }
        return DATA;

    }

    /**
     * 设置淘列表+推荐列表数据
     *
     * @param datas     ：要插入的数据
     * @param list      ：插入数据类型
     * @param mTaoDatas ：要插入到的父数据列表
     */
    private void setTaoSearchData(List<SearchTao> datas, TaoListDataEnum list, List<TaoListData> mTaoDatas) {
        if (datas != null && datas.size() > 0) {
            for (SearchTao tao : datas) {
                TaoListDataType taoListDataType = new TaoListDataType();
                taoListDataType.setType(list);
                taoListDataType.setSearchTao(tao);
                tao.handleListType();
                TaoListData taoListData = new TaoListData();
                taoListData.setType(tao.getTaolistType());
                taoListData.setTaoList(taoListDataType);
                mTaoDatas.add(taoListData);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case DOCTOR_LIST:
                return new DoctorViewHolder(mLayoutInflater.inflate(R.layout.item_tao_staggered_doctor, parent, false));
            case NOT_MORE:
                return new NotMoreViewHolder(mLayoutInflater.inflate(R.layout.list_searh_not_more_view, parent, false));
            case BIG_PROMOTION:
                return new PromotionViewHolder(mLayoutInflater.inflate(R.layout.item_tao_staggered_promotion, parent, false));
            case FLASH_SALE:
            case LEADER_BOARD:
                return new ActiveViewHolder(mLayoutInflater.inflate(R.layout.item_tao_staggered_active, parent, false));
            case RECOMMEND:
                return new RecommendViewHolder(mLayoutInflater.inflate(R.layout.item_tao_staggered_recommened, parent, false));
            case DATA:
                return new TaoViewHolder(mLayoutInflater.inflate(R.layout.item_tao_staggered_view, parent, false));
            default:
                return new TaoViewHolder(mLayoutInflater.inflate(R.layout.item_tao_staggered_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof TaoViewHolder) {
            setDataView((TaoViewHolder) viewHolder, position);
        } else if (viewHolder instanceof DoctorViewHolder) {
            setDoctorView((DoctorViewHolder) viewHolder, position);
        } else if (viewHolder instanceof NotMoreViewHolder) {
            setNotMoreView((NotMoreViewHolder) viewHolder, position);
        }else if (viewHolder instanceof PromotionViewHolder){
            setPromotionView((PromotionViewHolder) viewHolder, position);
        }else if (viewHolder instanceof ActiveViewHolder){
            setActiveView((ActiveViewHolder) viewHolder, position);
        }else if (viewHolder instanceof RecommendViewHolder){
            setRecommendView((RecommendViewHolder) viewHolder, position);
        }
    }

    private void setRecommendView(RecommendViewHolder viewHolder, int position) {
        TaoListData taoListData = mTaoDatas.get(position);
        TaoListDataType taoList = taoListData.getTaoList();
        final SearchActivityData recommend = taoList.getSearchTao().getRecommend();

        viewHolder.mTitle.setText(recommend.getTitle());
        viewHolder.mSubTitle.setText(recommend.getSubtitle());
        List<SearchTaolistData> taoList1 = recommend.getTaoList();
        final ArrayList<SearchTaolistData> taolistData = new ArrayList<>();
        for (int i = 0; i < taoList1.size(); i++) {
            if (i < 4){
                taolistData.add(taoList1.get(i));
            }
        }
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        viewHolder.mRecyclerView.setLayoutManager(gridLayoutManager);
        ItemStaggerGridAdapter itemStaggerGridAdapter = new ItemStaggerGridAdapter(R.layout.item_staggered_gride_item, taolistData);
        viewHolder.mRecyclerView.setAdapter(itemStaggerGridAdapter);
        itemStaggerGridAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HashMap<String, String> event_params = taolistData.get(position).getEvent_params();
                event_params.put("show_style","692_bilateral");
                YmStatistics.getInstance().tongjiApp(event_params);
                WebUrlTypeUtil.getInstance(mContext).urlToApp(recommend.getJumpUrl());
            }
        });
        viewHolder.mTitleClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> event_params = recommend.getEvent_params();
                event_params.put("show_style","692_bilateral");
                YmStatistics.getInstance().tongjiApp(event_params);
                WebUrlTypeUtil.getInstance(mContext).urlToApp(recommend.getJumpUrl());
            }
        });
    }

    private void setActiveView(ActiveViewHolder viewHolder, final int position) {
        SearchActivityData activityData = null;
        TaoListData taoListData = mTaoDatas.get(position);
        final TaoListDataType taoList = taoListData.getTaoList();
        TaoListType taolistType = taoList.getSearchTao().getTaolistType();
        switch (taolistType){
            case FLASH_SALE:
                activityData = taoList.getSearchTao().getFlash_sale();
                viewHolder.mActiveBackground.setBackgroundResource(R.drawable.flash_sale_bg);
                viewHolder.mActiveImg.setBackgroundResource(R.drawable.flash_sale_clock);
                break;
            case LEADER_BOARD:
                activityData = taoList.getSearchTao().getLeader_board();
                viewHolder.mActiveBackground.setBackgroundResource(R.drawable.leader_board_bg);
                viewHolder.mActiveImg.setBackgroundResource(R.drawable.item_king);
                break;
        }


        if (activityData != null){
            viewHolder.mActiveTitle.setText(activityData.getTitle());
            String end_time = activityData.getEnd_time();
            viewHolder.mActiveContainer.removeAllViews();
            if (!TextUtils.isEmpty(end_time) && !"0".equals(end_time)){
                Date date = new Date();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String currentTime = simpleDateFormat.format(date);
                String endTimeAct = Utils.stampToPhpDate(end_time);
                long[] timeSubAct = Utils.getTimeSub(currentTime, endTimeAct);
                long[] timesAct = {timeSubAct[0], timeSubAct[1], timeSubAct[2], timeSubAct[3]};
                ItemTimerTextView timerTextView = new ItemTimerTextView(mContext);
                timerTextView.setSkuTimeDesc("还剩");
                timerTextView.setTimes(timesAct);
                if (!timerTextView.isRun()) {
                    timerTextView.beginRun();
                }
                viewHolder.mActiveContainer.addView(timerTextView);
            }else {
                TextView textView = new TextView(mContext);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                textView.setLayoutParams(layoutParams);
                textView.setTextColor(ContextCompat.getColor(mContext,R.color._99));
                textView.setTextSize(11);
                textView.setText(activityData.getSubtitle());
                viewHolder.mActiveContainer.addView(textView);
            }
            int imgs [] = new int[]{R.drawable.item_one,R.drawable.item_two,R.drawable.item_three};
            List<SearchTaolistData> searchTaolistData = activityData.getTaoList();
            final ArrayList<SearchTaolistData> taolistData = new ArrayList<>();
            for (int i = 0; i < searchTaolistData.size(); i++) {
                if (i < 3){
                    SearchTaolistData data = searchTaolistData.get(i);
                    if (taolistType == TaoListType.LEADER_BOARD){
                        data.setImg_resouce(imgs[i]);
                    }
                    taolistData.add(data);
                }
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
            viewHolder.mActiveList.setLayoutManager(linearLayoutManager);
            SearchActiveItemAdapter searchActiveItemAdapter = new SearchActiveItemAdapter(R.layout.search_active_list_item,taolistData);
            viewHolder.mActiveList.setAdapter(searchActiveItemAdapter);
            searchActiveItemAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    HashMap<String, String> event_params = taolistData.get(position).getEvent_params();
                    event_params.put("show_style","692_bilateral");
                    YmStatistics.getInstance().tongjiApp(event_params);
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(taolistData.get(position).getApp_url());
                }
            });
            final SearchActivityData finalActivityData = activityData;
            viewHolder.mActiveBackground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> event_params = finalActivityData.getEvent_params();
                    event_params.put("show_style","692_bilateral");
                    YmStatistics.getInstance().tongjiApp(event_params);
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(finalActivityData.getJumpUrl());
                }
            });

        }

    }

    private void setPromotionView(PromotionViewHolder viewHolder, int position) {
        TaoListData taoListData = mTaoDatas.get(position);
        TaoListDataType taoList = taoListData.getTaoList();
        SearchActivityData bigPromotion = taoList.getSearchTao().getBig_promotion();
        Glide.with(mContext).load(bigPromotion.getMain_pic()).into(viewHolder.mMainImg);
        Glide.with(mContext).load(bigPromotion.getSecondary_pic()).into(viewHolder.mSubImg);
        viewHolder.mTitle.setText(bigPromotion.getTitle());
        viewHolder.mSubTitle.setText(bigPromotion.getSubtitle());
    }


    @Override
    public int getItemCount() {
        return mTaoDatas.size();
    }

    /**
     * 淘数据
     *
     * @param holder
     * @param position
     */
    private void setDataView(TaoViewHolder holder,int position){
        TaoListData taoListData = mTaoDatas.get(position);
        TaoListDataType taoList = taoListData.getTaoList();
        if (taoList != null){
            SearchTao searchTao = taoList.getSearchTao();
            if (searchTao != null){
                HomeTaoData tao = searchTao.getTao();
                if (tao != null){
                    Glide.with(mContext).load(tao.getImg())
                            .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(holder.mCommonImg);
                    String bilateralCoupons = tao.getBilateral_coupons();
                    String bilateralDesc = tao.getHospital_top().getBilateral_desc();
                    List<Promotion> promotion = tao.getPromotion();
                    if (CollectionUtils.isNotEmpty(promotion)){
                        for (int i = 0; i < promotion.size(); i++) {
                            Promotion promotion1 = (Promotion) promotion.get(i);
                            String styleType = promotion1.getStyle_type();
                            if ("2".equals(styleType) && TextUtils.isEmpty(bilateralDesc)){
                                holder.taoNearLook.setVisibility(View.VISIBLE);
                                break;
                            }else {
                                holder.taoNearLook.setVisibility(View.GONE);
                            }
                        }
                    }else {
                        holder.taoNearLook.setVisibility(View.GONE);
                    }
                    if (!TextUtils.isEmpty(bilateralDesc)){
                        holder.taoStaggerContanier.setVisibility(View.VISIBLE);
                        holder.taoCoupons.setVisibility(View.GONE);
                        holder.taoTagTitle.setText(bilateralDesc);
                    }else {
                        holder.taoStaggerContanier.setVisibility(View.GONE);
                        if (!TextUtils.isEmpty(bilateralCoupons)){
                            holder.taoCoupons.setVisibility(View.VISIBLE);
                            holder.taoCoupons.setText(bilateralCoupons);
                        }else {
                            holder.taoCoupons.setVisibility(View.GONE);
                        }
                    }
                    String highlightTitle = tao.getHighlight_title();
                    if (!TextUtils.isEmpty(highlightTitle)){
                        try {
                            Log.e(TAG,"highlightTitleOrgin =="+highlightTitle);
                            String htmlTitle = URLDecoder.decode(highlightTitle.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "utf-8");
                            Log.e(TAG,"highlightTitle =="+htmlTitle);
                            //设置标题
                            holder.mCommonTxt.setText(Html.fromHtml(htmlTitle));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
//                    holder.mCommonTxt.setText("<" + tao.getTitle() + "> " + tao.getSubtitle());
                    String appSlideLogo = tao.getApp_slide_logo();
                    if (!TextUtils.isEmpty(appSlideLogo)){
                        holder.mCommonPromottonImg.setVisibility(View.VISIBLE);
                        Glide.with(mContext).load(appSlideLogo).into(holder.mCommonPromottonImg);
                    }else {
                        holder.mCommonPromottonImg.setVisibility(View.GONE);
                    }

                    holder.mCommonPrice.setText(tao.getPrice_discount());
                    String doc_name = tao.getDoc_name();
                    String hos_name = tao.getHos_name();
                    holder.mCommonDocname.setText(doc_name+"  "+hos_name);
                    holder.mCommonDistance.setText(tao.getDistance());
                }
            }

        }

    }

    /**
     * 对比咨询数据
     * @param holder
     * @param position
     */
    private void setDoctorView(DoctorViewHolder holder,int position){
        TaoListData taoListData = mTaoDatas.get(position);
        TaoListDoctors mDoctors = taoListData.getDoctors();
        if (mDoctors != null){
            List<TaoListDoctorsComparedData> doctorsList = mDoctors.getCompared().getDoctorsList();
            TaoListDoctorsComparedData taoListDoctorsComparedData = doctorsList.get(getDoctorSelectPos(doctorsList));
            List<SearchResultDoctorList> comparedDoctors = taoListDoctorsComparedData.getList();
            SearchResultDoctorList doctor1 = comparedDoctors.get(0);
            SearchResultDoctorList doctor2 = comparedDoctors.get(1);
            holder.mDoctorContent.setText(mDoctors.getComparedTitle());

            //头像1
            mFunctionManager.setCircleImageSrc(holder.mDoctorImg1, doctor1.getDoctorsImg());
            //医生标签1
            holder.mDoctorTag1.setText(doctor1.getDoctorsTag());
            setTagBackground(holder.mDoctorTag1, doctor1.getDoctorsTagID());
            //医生名称1
            holder.mDoctorName1.setText(doctor1.getDoctorsName());
            //医生职称1
            holder.mDoctorTitle1.setText(doctor1.getDoctorsTitle());
            //医生所在医院1
            holder.mDoctorHosName1.setText(doctor1.getHospitalName());
            //医生口碑1
            holder.mDoctorScore1.setText("口碑：" + doctor1.getDiary_pf());
            //医生预定数1
            holder.mDoctorBooking1.setText(doctor1.getSku_order_num() + "人预订");

            //头像2
            mFunctionManager.setCircleImageSrc(holder.mDoctorImg2, doctor2.getDoctorsImg());
            //医生标签2
            holder.mDoctorTag2.setText(doctor2.getDoctorsTag());
            setTagBackground(holder.mDoctorTag2, doctor2.getDoctorsTagID());
            //医生名称2
            holder.mDoctorName2.setText(doctor2.getDoctorsName());
            //医生职称2
            holder.mDoctorTitle2.setText(doctor2.getDoctorsTitle());
            //医生所在医院2
            holder.mDoctorHosName2.setText(doctor2.getHospitalName());
            //医生口碑2
            holder.mDoctorScore2.setText("口碑：" + doctor2.getDiary_pf());
            //医生预定数2
            holder.mDoctorBooking2.setText(doctor2.getSku_order_num() + "人预订");
        }

    }


    /**
     * 获取对比咨询选中的下标
     *
     * @return
     */
    public int getDoctorSelectPos(List<TaoListDoctorsComparedData> datas) {
        for (int i = 0; i < datas.size(); i++) {
            TaoListDoctorsComparedData data = datas.get(i);
            if (data.isSelected()) {
                return i;
            }
        }
        return 0;
    }


    /**
     * 设置标签背景
     *
     * @param tag
     * @param tag_id
     */
    private void setTagBackground(TextView tag, String tag_id) {
        switch (tag_id) {
            case "1":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag1);
                break;
            case "2":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag2);
                break;
            case "3":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag3);
                break;
            case "4":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag4);
                break;
            case "5":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag5);
                break;
            case "6":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag6);
                break;
            case "7":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag7);
                break;
            case "8":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag8);
                break;
        }
    }

    /**
     * 没有更多
     *
     * @param holder
     * @param position
     */
    private void setNotMoreView(NotMoreViewHolder holder, int position) {

    }


    /**
     * 设置图片
     *
     * @param holder
     * @param position
     * @param post
     */
    private void setImage(@NonNull final TaoViewHolder holder, final int position, BBsListData550 post) {
        if (imgHeights.containsKey(position)) {
            int imgHeight = imgHeights.get(position);

            ViewGroup.LayoutParams params = holder.mCommonImg.getLayoutParams();
            params.width = itemWidth;
            params.height = imgHeight;

            holder.mCommonImg.setLayoutParams(params);

            Glide.with(mContext).load(post.getImg310().getImg())
                    .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(holder.mCommonImg);

        } else {
            Img310 img310 = post.getImg310();
            String imgImg = img310.getImg();

            int intrinsicWidth;
            try {
                intrinsicWidth = Integer.parseInt(img310.getWidth());
            }catch (NumberFormatException e){
                intrinsicWidth = 0;
            }
            int intrinsicHeight;
            try {
                intrinsicHeight = Integer.parseInt(img310.getHeight());
            }catch (NumberFormatException e){
                intrinsicHeight = 0;
            }

            if (intrinsicWidth != 0 && intrinsicHeight != 0) {
                int imgHeight = (itemWidth * intrinsicHeight) / intrinsicWidth;

                ViewGroup.LayoutParams params = holder.mCommonImg.getLayoutParams();
                params.width = itemWidth;
                params.height = imgHeight;

                Log.e(TAG, "111width == " + intrinsicWidth);
                Log.e(TAG, "111height == " + intrinsicHeight);
                holder.mCommonImg.setLayoutParams(params);

                imgHeights.put(position, imgHeight);

                Glide.with(mContext)
                        .load(imgImg)
                        .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.TOP))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(holder.mCommonImg);

            } else {

                Glide.with(mContext)
                        .load(imgImg)
                        .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.TOP))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                int intrinsicWidth = resource.getIntrinsicWidth();
                                int intrinsicHeight = resource.getIntrinsicHeight();
                                Log.e(TAG, "intrinsicWidth == " + intrinsicWidth);
                                Log.e(TAG, "intrinsicHeight == " + intrinsicHeight);

                                int imgHeight = (itemWidth * intrinsicHeight) / intrinsicWidth;

                                Log.e(TAG, "imgWidth == " + itemWidth);
                                Log.e(TAG, "imgHeight == " + imgHeight);


                                ViewGroup.LayoutParams params = holder.mCommonImg.getLayoutParams();
                                params.width = itemWidth;
                                params.height = imgHeight;
                                holder.mCommonImg.setLayoutParams(params);

                                imgHeights.put(position, imgHeight);

                                holder.mCommonImg.setImageDrawable(resource);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                int imageHeight = 0;
                                ViewGroup.LayoutParams params = holder.mCommonImg.getLayoutParams();
                                params.width = itemWidth;
                                params.height = imageHeight;
                                holder.mCommonImg.setLayoutParams(params);
                                imgHeights.put(position, imageHeight);

                            }
                        });

            }
        }
    }



    public class TaoViewHolder extends RecyclerView.ViewHolder{

        private  LinearLayout mCommonContainer;
        private  LinearLayout taoStaggerContanier;
        private ImageView mCommonImg;
        private TextView mCommonTxt;
        private TextView taoCoupons;
        private TextView taoNearLook;
        private TextView taoTagTitle;
        private TextView mCommonPrice;
        private TextView mCommonDocname;
        private ImageView mCommonPromottonImg;
        private TextView mCommonDistance;

        public TaoViewHolder(@NonNull View itemView) {
            super(itemView);
            mCommonContainer = itemView.findViewById(R.id.staggered_common_container);
            taoStaggerContanier = itemView.findViewById(R.id.tao_staggered_tagcontianer);
            taoTagTitle = itemView.findViewById(R.id.tao_staggered_tagtitle);
            taoCoupons = itemView.findViewById(R.id.item_coupons);
            taoNearLook = itemView.findViewById(R.id.item_nearlook);
            mCommonImg = itemView.findViewById(R.id.staggered_common_img);
            mCommonTxt = itemView.findViewById(R.id.staggered_common_title);
            mCommonPrice = itemView.findViewById(R.id.staggered_common_price);
            mCommonPromottonImg = itemView.findViewById(R.id.staggered_promotton_img);
            mCommonDocname = itemView.findViewById(R.id.staggered_common_docname);
            mCommonDistance = itemView.findViewById(R.id.staggered_common_distance);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int layoutPosition = getLayoutPosition();
                    Log.e(TAG, "layoutPosition == " + layoutPosition);
                    if (onItemClickListener != null) {
                        if (layoutPosition >= 0) {
                            TaoListDataType taoList = mTaoDatas.get(layoutPosition).getTaoList();
                            if (taoList != null) {
                                onItemClickListener.onItemClick(taoList, layoutPosition);
                            }
                        }
                    }
                }
            });
        }
    }

    public class DoctorViewHolder extends RecyclerView.ViewHolder{

        private TextView mDoctorContent;
        private LinearLayout mDoctorChangeClick;
        private LinearLayout mDoctorClick1,mDoctorClick2;
        private ImageView mDoctorImg1,mDoctorImg2;
        private TextView mDoctorName1,mDoctorName2;
        private TextView mDoctorTitle1,mDoctorTitle2;
        private TextView mDoctorTag1,mDoctorTag2;
        private TextView mDoctorHosName1,mDoctorHosName2;
        private TextView mDoctorScore1,mDoctorScore2;
        private TextView mDoctorBooking1,mDoctorBooking2;
        private RelativeLayout mDoctorAsk1,mDoctorAsk2;

        public DoctorViewHolder(@NonNull View itemView) {
            super(itemView);
            mDoctorContent = itemView.findViewById(R.id.staggered_doctor_content);
            mDoctorChangeClick = itemView.findViewById(R.id.staggered_doctor_change_click);
            mDoctorClick1 = itemView.findViewById(R.id.staggered_doctor_click1);
            mDoctorImg1 = itemView.findViewById(R.id.staggered_doctor_img1);
            mDoctorName1 = itemView.findViewById(R.id.staggered_doctor_name1);
            mDoctorTitle1 = itemView.findViewById(R.id.staggered_doctor_title1);
            mDoctorTag1 = itemView.findViewById(R.id.staggered_doctor_tag1);
            mDoctorHosName1 = itemView.findViewById(R.id.staggered_doctor_hosname1);
            mDoctorScore1 = itemView.findViewById(R.id.staggered_doctor_score1);
            mDoctorBooking1 = itemView.findViewById(R.id.staggered_doctor_booking1);
            mDoctorAsk1 = itemView.findViewById(R.id.staggered_doctor_ask1);

            mDoctorClick2 = itemView.findViewById(R.id.staggered_doctor_click2);
            mDoctorImg2 = itemView.findViewById(R.id.staggered_doctor_img2);
            mDoctorName2 = itemView.findViewById(R.id.staggered_doctor_name2);
            mDoctorTitle2 = itemView.findViewById(R.id.staggered_doctor_title2);
            mDoctorTag2 = itemView.findViewById(R.id.staggered_doctor_tag2);
            mDoctorHosName2 = itemView.findViewById(R.id.staggered_doctor_hosname2);
            mDoctorScore2 = itemView.findViewById(R.id.staggered_doctor_score2);
            mDoctorBooking2 = itemView.findViewById(R.id.staggered_doctor_booking2);
            mDoctorAsk2 = itemView.findViewById(R.id.staggered_doctor_ask2);
            //问医生
            mDoctorClick1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onDoctorChatlickListener != null) {
                            int layoutPosition = getLayoutPosition();
                            if (isHeadView) {
                                layoutPosition--;
                            }

                            TaoListData taoListData = mTaoDatas.get(layoutPosition);
                            if (taoListData.getType() == TaoListType.DOCTOR_LIST) {
                                TaoListDoctors doctors = taoListData.getDoctors();
                                if (doctors != null){
                                    TaoListDoctorsCompared compared = doctors.getCompared();
                                    if (compared != null){
                                        List<TaoListDoctorsComparedData> doctorsList = compared.getDoctorsList();
                                        int doctorSelectPos = getDoctorSelectPos(doctorsList);
                                        TaoListDoctorsComparedData taoListDoctorsComparedData = doctorsList.get(doctorSelectPos);
                                        List<SearchResultDoctorList> list = taoListDoctorsComparedData.getList();
                                        SearchResultDoctorList searchResultDoctorList = list.get(0);
                                        SearchResultDoctorControlParams typeControlParams = searchResultDoctorList.getTypeControlParams();

                                        WebData webData = new WebData(searchResultDoctorList.getJumpUrl());
                                        webData.setShowTitle("0".equals(typeControlParams.getIsHide()));
                                        webData.setShowRefresh("1".equals(typeControlParams.getIsRefresh()));
                                        WebUtil.getInstance().startWebActivity(mContext, webData);
                                        HashMap<String, String> event_params = searchResultDoctorList.getEvent_params();
                                        onDoctorChatlickListener.onDoctorChatClick(event_params);
                                    }
                                }
                            }
                        }
                    }
                }
            });

            //问医生2
            mDoctorClick2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onDoctorChatlickListener != null) {
                            int layoutPosition = getLayoutPosition();
                            if (isHeadView) {
                                layoutPosition--;
                            }
                            TaoListData taoListData = mTaoDatas.get(layoutPosition);
                            if (taoListData.getType() == TaoListType.DOCTOR_LIST) {
                                TaoListDoctors doctors = taoListData.getDoctors();
                                if (doctors != null){
                                    TaoListDoctorsCompared compared = doctors.getCompared();
                                    if (compared != null){
                                        List<TaoListDoctorsComparedData> doctorsList = compared.getDoctorsList();
                                        int doctorSelectPos = getDoctorSelectPos(doctorsList);
                                        TaoListDoctorsComparedData taoListDoctorsComparedData = doctorsList.get(doctorSelectPos);
                                        List<SearchResultDoctorList> list = taoListDoctorsComparedData.getList();
                                        SearchResultDoctorList searchResultDoctorList = list.get(1);
                                        SearchResultDoctorControlParams typeControlParams = searchResultDoctorList.getTypeControlParams();

                                        WebData webData = new WebData(searchResultDoctorList.getJumpUrl());
                                        webData.setShowTitle("0".equals(typeControlParams.getIsHide()));
                                        webData.setShowRefresh("1".equals(typeControlParams.getIsRefresh()));
                                        WebUtil.getInstance().startWebActivity(mContext, webData);
                                        HashMap<String, String> event_params = searchResultDoctorList.getEvent_params();
                                        onDoctorChatlickListener.onDoctorChatClick(event_params);
                                    }

                                }
                            }
                        }


                    }
                }
            });

            //换一换
            mDoctorChangeClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int layoutPosition = getLayoutPosition();
                    if (isHeadView) {
                        layoutPosition--;
                    }

                    TaoListData taoListData = mTaoDatas.get(layoutPosition);

                    if (taoListData.getType() == TaoListType.DOCTOR_LIST) {
                        TaoListDoctors doctors = taoListData.getDoctors();
                        if (doctors == null) {
                            mFunctionManager.showShort("没有更多医生");
                            return;
                        }
                        TaoListDoctorsCompared compared = doctors.getCompared();
                        if (compared == null) {
                            mFunctionManager.showShort("没有更多医生");
                            return;
                        }
                        List<TaoListDoctorsComparedData> doctorsList = compared.getDoctorsList();
                        int doctorSelectPos = getDoctorSelectPos(doctorsList);
                        doctorSelectPos++;
                        if (doctorSelectPos >= doctorsList.size()) {
                            doctorSelectPos = 0;
                            mFunctionManager.showShort("没有更多医生");
                        }

                        setDoctorSelectPos(compared.getDoctorsEnum(), doctorSelectPos);
                        notifyDataSetChanged();

                        if (onDoctorChatlickListener != null) {
                            onDoctorChatlickListener.onChangeClick(compared, doctorSelectPos);
                        }
                    }
                }
            });
        }
    }


    /**
     * 设置医生选中下标
     */
    private void setDoctorSelectPos(TaoListDoctorsEnum doctorsEnum, int doctorSelectPos) {
        for (int i = 0; i < mTaoDatas.size(); i++) {
            TaoListData data = mTaoDatas.get(i);
            if (data.getType() == TaoListType.DOCTOR_LIST) {
                TaoListDoctorsCompared compared = data.getDoctors().getCompared();
                if (compared.getDoctorsEnum() == doctorsEnum) {
                    List<TaoListDoctorsComparedData> doctorsList = compared.getDoctorsList();
                    for (int i1 = 0; i1 < doctorsList.size(); i1++) {
                        TaoListDoctorsComparedData taoListDoctorsComparedData = doctorsList.get(i1);
                        if (i1 == doctorSelectPos) {
                            taoListDoctorsComparedData.setSelected(true);
                        } else {
                            taoListDoctorsComparedData.setSelected(false);
                        }
                    }
                }
            }
        }
    }



    public class RecommendViewHolder extends RecyclerView.ViewHolder{

        private final RelativeLayout mTitleClick;
        private final TextView mTitle;
        private final TextView mSubTitle;
        private final RecyclerView mRecyclerView;

        public RecommendViewHolder(@NonNull View itemView) {
            super(itemView);
            mTitleClick = itemView.findViewById(R.id.recommened_click);
            mTitle = itemView.findViewById(R.id.recommened_title_tv);
            mSubTitle = itemView.findViewById(R.id.recommened_subtitle_tv);
            mRecyclerView = itemView.findViewById(R.id.recommened_list);
        }
    }




    public class PromotionViewHolder extends RecyclerView.ViewHolder{


        private final ImageView mMainImg;
        private final ImageView mSubImg;
        private final TextView mTitle;
        private final TextView mSubTitle;

        public PromotionViewHolder(@NonNull View itemView) {
            super(itemView);
            mMainImg = itemView.findViewById(R.id.promotion_img);
            mSubImg = itemView.findViewById(R.id.promotion_subimg);
            mTitle = itemView.findViewById(R.id.promotion_title);
            mSubTitle = itemView.findViewById(R.id.promotion_subtitle);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int layoutPosition = getLayoutPosition();
                    Log.e(TAG, "layoutPosition == " + layoutPosition);
                    if (onItemClickListener != null) {
                        if (layoutPosition >= 0) {
                            TaoListDataType taoList = mTaoDatas.get(layoutPosition).getTaoList();
                            SearchTao searchTao = taoList.getSearchTao();
                            if (searchTao != null){
                                SearchActivityData bigPromotion = searchTao.getBig_promotion();
                                if (bigPromotion != null){
                                    HashMap<String, String> event_params = bigPromotion.getEvent_params();
                                    event_params.put("show_style","692_bilateral");
                                    YmStatistics.getInstance().tongjiApp(event_params);
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(bigPromotion.getJumpUrl());
                                }
                            }
                        }
                    }
                }
            });
        }
    }


    public class ActiveViewHolder extends RecyclerView.ViewHolder{


        private final RelativeLayout mActiveBackground;
        private final TextView mActiveTitle;
        private final ImageView mActiveImg;
        private final LinearLayout mActiveContainer;
        private final RecyclerView mActiveList;

        public ActiveViewHolder(@NonNull View itemView) {
            super(itemView);
            mActiveBackground = itemView.findViewById(R.id.active_background);
            mActiveTitle = itemView.findViewById(R.id.active_title);
            mActiveImg = itemView.findViewById(R.id.acitve_img);
            mActiveContainer = itemView.findViewById(R.id.active_container);
            mActiveList = itemView.findViewById(R.id.active_list);
        }
    }

    /**
     * 没有更多了，不妨看看这些项目吧~
     */
    public class NotMoreViewHolder extends RecyclerView.ViewHolder {

        public NotMoreViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    /**
     * 刷新数据
     *
     * @param data
     */
    public void refreshData(SearchResultTaoData2 data) {
        mTaoDatas = setSearctTaoListData(data);
        notifyDataSetChanged();
    }

    /**
     * 分页加载
     *
     * @param datas
     * @param recomendList
     */
    public void addData(List<SearchTao> datas, List<SearchTao> recomendList) {
        int size = mTaoDatas.size();
        setTaoSearchData(datas, TaoListDataEnum.LIST, mTaoDatas);
        setTaoSearchData(recomendList, TaoListDataEnum.RECOMMENDED, mTaoDatas);
        notifyItemRangeInserted(size + 1, mTaoDatas.size() - size);
    }


    public List<HomeTaoData> getSearchData() {
        List<HomeTaoData> datas = new ArrayList<>();
        for (TaoListData data : mTaoDatas) {
            if (data.getType() == TaoListType.DATA) {
                datas.add(data.getTaoList().getSearchTao().getTao());
            }
        }
        return datas;
    }


    public List<TaoListData> getTaoDatas() {
        return mTaoDatas;
    }

    /**
     * 获取医院数据
     *
     * @return
     */
    public List<TaoListDoctors> getComparChatData() {
        List<TaoListDoctors> doctors = new ArrayList<>();
        for (TaoListData data : mTaoDatas) {
            if (data.getType() == TaoListType.DOCTOR_LIST) {
                doctors.add(data.getDoctors());
            }
        }
        return doctors;
    }

    public List<SearchTao> getActiveData(){
        List<SearchTao> searchTaos = new ArrayList<>();
        for (TaoListData data : mTaoDatas) {
            searchTaos.add(data.getTaoList().getSearchTao());
        }
        return searchTaos;
    }




    //item点击
    public interface OnItemClickListener {
        void onItemClick(TaoListDataType taoData, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    //医生咨询
    public interface OnDoctorChatlickListener {
        void onDoctorChatClick(HashMap<String, String> event_params);

        /**
         * @param compared  : 换一换item位所有数据
         * @param changePos :换一换的pos
         */
        void onChangeClick(TaoListDoctorsCompared compared, int changePos);
    }

    private OnDoctorChatlickListener onDoctorChatlickListener;

    public void setOnDoctorChatlickListener(OnDoctorChatlickListener onDoctorChatlickListener) {
        this.onDoctorChatlickListener = onDoctorChatlickListener;
    }

}
