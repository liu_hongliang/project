package com.module.my.model.bean;

import java.util.List;

/**
 * 关注的数据
 * Created by 裴成浩 on 2018/4/19.
 */

public class MyFocusData {
    private List<MyFansData> data;
    private List<MyFansData> interest;

    public List<MyFansData> getData() {
        return data;
    }

    public void setData(List<MyFansData> data) {
        this.data = data;
    }

    public List<MyFansData> getInterest() {
        return interest;
    }

    public void setInterest(List<MyFansData> interest) {
        this.interest = interest;
    }
}
