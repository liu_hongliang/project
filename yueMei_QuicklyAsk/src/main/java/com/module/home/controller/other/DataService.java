package com.module.home.controller.other;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;

import com.module.home.model.bean.Tuijshare;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class DataService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public DataService(String name) {
        super(name);
    }

    public static void startService(Context context, ArrayList<Tuijshare> mTuiDatas, String subtype) {
        Intent intent = new Intent(context, DataService.class);
        intent.putParcelableArrayListExtra("data", (ArrayList<? extends Parcelable>) mTuiDatas);
        intent.putExtra("subtype", subtype);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent( Intent intent) {
        if (intent == null) {
            return;
        }
        List<Tuijshare> datas = intent.getParcelableArrayListExtra("data");
        String subtype = intent.getStringExtra("subtype");
        handleGirlItemData(datas, subtype);
    }

    private void handleGirlItemData(List<Tuijshare> datas, String subtype) {
        if (datas.size() == 0) {
            EventBus.getDefault().post("finish");
            return;
        }
//        for (Tuijshare data : datas) {
//            Bitmap bitmap = ImageLoader.load(this, data.getPost().getImg310().getImg());
//            if (bitmap != null) {
//                data.setWidth(bitmap.getWidth());
//                data.setHeight(bitmap.getHeight());
//            }
//
//            data.setSubtype(subtype);
//        }
//        EventBus.getDefault().post(datas);
    }
}
