package com.module.my.controller.other;

import android.content.Intent;

import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.my.controller.activity.QuestionDetailsActivity;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;

import org.json.JSONObject;

/**
 * Created by 裴成浩 on 2018/1/22.
 */

public class QuestionDetailsWebViewClient  implements BaseWebViewClientCallback {

    private final QuestionDetailsActivity mActivity;
    private final Intent intent;
    private String uid;
    private String TAG = "QuestionDetailsWebViewClient";

    public QuestionDetailsWebViewClient(QuestionDetailsActivity activity) {
        this.mActivity = activity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "6341":                    // 问题解答
                String question_id = obj.getString("question_id");
                mActivity.initBottHttp();

                break;
        }
    }
}
