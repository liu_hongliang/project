package com.module.taodetail.model.bean;

import com.module.home.model.bean.SearchResultDoctor;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/1/29.
 */

public class HomeTaoDataS {
    private List<HomeTaoData> data;
    private List<HomeTaoData> tuijian;
    private String tuijian_title;
    private SearchResultDoctor comparedConsultative;

    public List<HomeTaoData> getData() {
        return data;
    }

    public void setData(List<HomeTaoData> data) {
        this.data = data;
    }

    public List<HomeTaoData> getTuijian() {
        return tuijian;
    }

    public void setTuijian(List<HomeTaoData> tuijian) {
        this.tuijian = tuijian;
    }

    public String getTuijian_title() {
        return tuijian_title;
    }

    public void setTuijian_title(String tuijian_title) {
        this.tuijian_title = tuijian_title;
    }

    public SearchResultDoctor getComparedConsultative() {
        return comparedConsultative;
    }

    public void setComparedConsultative(SearchResultDoctor comparedConsultative) {
        this.comparedConsultative = comparedConsultative;
    }
}
