package com.module.my.view.view;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 禁止点击的RadioButton
 * Created by 裴成浩 on 2018/9/6.
 */
public class BannedClickRadioButton extends AppCompatRadioButton {
    public BannedClickRadioButton(Context context) {
        this(context, null);
    }

    public BannedClickRadioButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BannedClickRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return false;
    }
}
