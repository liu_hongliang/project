package com.module.commonview.module.bean;

/**
 * 日记本页面点赞举报接口
 * Created by 裴成浩 on 2018/6/27.
 */
public class DiariesReportLikeData {
    private String id;             //点赞举报的id
    private int pos = -1;          //日记列表，评论列表点赞的下标
    private int posPos = -1;       //举报楼中楼时的下标，不存在时为-1
    private String flag;           //1:点赞，2：举报
    private String is_reply;       //0：日记帖子，1:评论列表，2底部点赞（在请求时要把2改为0，在分类时使用2）

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getPosPos() {
        return posPos;
    }

    public void setPosPos(int posPos) {
        this.posPos = posPos;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getIs_reply() {
        return is_reply;
    }

    public void setIs_reply(String is_reply) {
        this.is_reply = is_reply;
    }
}
