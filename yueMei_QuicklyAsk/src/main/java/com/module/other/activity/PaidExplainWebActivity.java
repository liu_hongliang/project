package com.module.other.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.view.MyElasticScrollView.OnRefreshListener1;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 悦美赔付声明
 * 
 * @author Rubin
 * 
 */
public class PaidExplainWebActivity extends BaseActivity {

	private final String TAG = "PaidExplainWebActivity";

	@BindView(id = R.id.wan_beautifu_web_det_scrollview3, click = true)
	private MyElasticScrollView scollwebView;
	@BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.pay_explain_top)
	private CommonTopBar mTop;// 返回

	private WebView docDetWeb;

	private PaidExplainWebActivity mContex;

	public JSONObject obj_http;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_pay_explain);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = PaidExplainWebActivity.this;
		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);

		mTop.setCenterText("悦美整形全额赔付");
		scollwebView.GetLinearLayout(contentWeb);
		initWebview();

		LodUrl1(FinalConstant.MONEY_BACK_INFO);
		scollwebView.setonRefreshListener(new OnRefreshListener1() {

			@Override
			public void onRefresh() {
				webReload();
			}
		});

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						PaidExplainWebActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	public Dialog dialog = null;

	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		contentWeb.addView(docDetWeb);
	}

	protected void OnReceiveData(String str) {
		scollwebView.onRefreshComplete();
	}

	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {
		baseWebViewClientMessage.startLoading();
		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
