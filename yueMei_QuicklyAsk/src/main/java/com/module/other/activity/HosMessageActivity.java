/**
 * 
 */
package com.module.other.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.view.MyElasticScrollView.OnRefreshListener1;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 医院介绍
 * 
 * @author Rubin
 * 
 */
public class HosMessageActivity extends BaseActivity {

	private final String TAG = "MyOrdersActivity";

	@BindView(id = R.id.wan_beautifu_web_det_scrollview4, click = true)
	private MyElasticScrollView scollwebView;
	@BindView(id = R.id.wan_beautifu_linearlayout4, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.hos_message_top)
	private CommonTopBar mTop;// 返回

	private WebView docDetWeb;

	private Context mContext;

	public JSONObject obj_http;
	private String uid;
	private String url;
	private BaseWebViewClientMessage mBaseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_hos_message);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = HosMessageActivity.this;
		uid = Utils.getUid();
		mTop.setCenterText("医院介绍");
		Intent it = getIntent();
		url = it.getStringExtra("url");
		url = FinalConstant.baseUrl + FinalConstant.VER + url;
		scollwebView.GetLinearLayout(contentWeb);
		mBaseWebViewClientMessage = new BaseWebViewClientMessage(HosMessageActivity.this);
		mBaseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
			@Override
			public void otherJump(String urlStr) {
				showWebDetail(urlStr);
			}
		});

		initWebview();

		LodUrl1(url);

		scollwebView.setonRefreshListener(new OnRefreshListener1() {

			@Override
			public void onRefresh() {
				webReload();
			}
		});

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						HosMessageActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}


	/**
	 * 处理webview里面的按钮
	 *
	 * @param urlStr
	 */
	@SuppressLint("NewApi")
	public void showWebDetail(String urlStr) {
		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			if (obj.getString("type").equals("1")) {// 医生详情页
				try {
					Intent intent = new Intent();
					String id1 = obj.getString("id");
					intent.putExtra("docId", id1);
					intent.putExtra("docName", "");
					intent.putExtra("partId", "");
					intent.setClass(mContext, DoctorDetailsActivity592.class);
					startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContext);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(mBaseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	protected void OnReceiveData(String str) {
		scollwebView.onRefreshComplete();
	}

	public void webReload() {
		if (docDetWeb != null) {
			mBaseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {
		mBaseWebViewClientMessage.startLoading();

		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
