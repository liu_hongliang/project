package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 画报数据
 * Created by 裴成浩 on 2018/4/13.
 */

public class SharePictorial implements Parcelable {

    private String img;
    private String title;
    private String price_discount;
    private String price;
    private String order_num;
    private String feeScale;
    private String hos_name;
    private String hos_rate;
    private String hos_rateScale;
    private String sun_img;

    public SharePictorial(Parcel in){
        img = in.readString();
        title = in.readString();
        price_discount = in.readString();
        price = in.readString();
        order_num = in.readString();
        feeScale = in.readString();
        hos_name = in.readString();
        hos_rate = in.readString();
        hos_rateScale = in.readString();
        sun_img = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
        dest.writeString(title);
        dest.writeString(price_discount);
        dest.writeString(price);
        dest.writeString(order_num);
        dest.writeString(feeScale);
        dest.writeString(hos_name);
        dest.writeString(hos_rate);
        dest.writeString(hos_rateScale);
        dest.writeString(sun_img);
    }

    public static final Parcelable.Creator<SharePictorial> CREATOR = new Parcelable.Creator<SharePictorial>() {
        public SharePictorial createFromParcel(Parcel in) {
            return new SharePictorial(in);
        }

        public SharePictorial[] newArray(int size) {
            return new SharePictorial[size];
        }
    };

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    public String getFeeScale() {
        return feeScale;
    }

    public void setFeeScale(String feeScale) {
        this.feeScale = feeScale;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getHos_rate() {
        return hos_rate;
    }

    public void setHos_rate(String hos_rate) {
        this.hos_rate = hos_rate;
    }

    public String getHos_rateScale() {
        return hos_rateScale;
    }

    public void setHos_rateScale(String hos_rateScale) {
        this.hos_rateScale = hos_rateScale;
    }

    public String getSun_img() {
        return sun_img;
    }

    public void setSun_img(String sun_img) {
        this.sun_img = sun_img;
    }

}
