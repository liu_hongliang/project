package com.module.base.refresh.loadmore;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

/**
 * 自定义上拉加载中的效果
 * Created by 裴成浩 on 2018/3/1.
 */

public class ProgressView extends LinearLayout{
    private ImageView footerLoading;
    private TextView loadingTitle;

    public ProgressView(Context context) {
        super(context);
        initView(context);
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public void initView(Context context) {
        setGravity(Gravity.CENTER);
        setLayoutParams(new AbsListView.LayoutParams(
                AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.footer_layout_load_view, null);
        footerLoading = view.findViewById(R.id.footer_loading);
        loadingTitle = view.findViewById(R.id.loading_title);

        loadingTitle.setText("正在加载中...");
        loadingTitle.setEnabled(false);
        rotatingAnimation();


        addView(view);
    }

    /**
     * 前边的旋转动画
     */
    public void rotatingAnimation() {
        //创建旋转动画
        final RotateAnimation animation =new RotateAnimation(0f,360f, Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF,0.5f);
        animation.setDuration(1000);
        animation.setRepeatCount(100);//动画的重复次数
        animation.setFillAfter(true);//设置为true，动画转化结束后被应用
        footerLoading.startAnimation(animation);//开始动画
    }


}
