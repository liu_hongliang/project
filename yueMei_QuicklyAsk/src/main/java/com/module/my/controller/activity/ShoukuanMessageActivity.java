package com.module.my.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.api.SaveAiliMessageApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.ShouKuan;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.view.EditExitDialog;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

import static android.R.id.message;

/**
 * 收款信息
 * <p/>
 * Created by dwb on 16/11/
 */
public class ShoukuanMessageActivity extends BaseActivity {

    private final String TAG = "ShoukuanMessageActivity";

    private Context mContext;

    @BindView(id = R.id.register_back, click = true)
    private RelativeLayout colseRly;
    @BindView(id = R.id.myprofile_head_sumbit_rly, click = true)
    private RelativeLayout saveRly;

    @BindView(id = R.id.reg_username)
    private EditText aliPayEt;// 支付宝账号
    @BindView(id = R.id.reg_rale_name_et)
    private EditText realNameEt;// 真实姓名
    @BindView(id = R.id.reg_rale_id_et)
    private EditText realIdEt;// 身份证号

    @BindView(id = R.id.order_time_all_ly)
    private LinearLayout allcontent;

    @BindView(id = R.id.btn_del_shoukuan, click = true)
    private Button btnDel;

    @BindView(id = R.id.tv_ts_shoukuan)
    private TextView tvTs;

    private String alipay;
    private String real_name;
    private static final int BACK3 = 33;
    private String card_id;


    @Override
    public void setRootView() {
        setContentView(R.layout.acty_shoukuan_message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = ShoukuanMessageActivity.this;


        Intent it = getIntent();
        alipay = it.getStringExtra("alipay");
        real_name = it.getStringExtra("real_name");
        card_id = it.getStringExtra("card_id");

        if (null != alipay && alipay.length() > 0 && !alipay.equals("0")) {
            aliPayEt.setText(alipay);
        }

        if (null != real_name && real_name.length() > 0 && !real_name.equals("0")) {
            realNameEt.setText(real_name);
        }

        if (null != card_id && card_id.length() > 0 && !card_id.equals("0")) {
            realIdEt.setText(card_id);
        }

        if(null != alipay && alipay.length() > 0 && !alipay.equals("0") &&
                null != real_name && real_name.length() > 0 && !real_name.equals("0")&&
                null != card_id && card_id.length() > 0 && !card_id.equals("0")){       //有收款信息

            btnDel.setVisibility(View.VISIBLE);
            tvTs.setVisibility(View.GONE);
            saveRly.setVisibility(View.GONE);
            aliPayEt.setEnabled(false);
            realNameEt.setEnabled(false);
            realIdEt.setEnabled(false);

        }else {     //没有收款信息
            btnDel.setVisibility(View.GONE);
            tvTs.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.register_back:
                finish();
                break;
            case R.id.myprofile_head_sumbit_rly:
                String aliPayStr = aliPayEt.getText().toString();
                String real_nameStr = realNameEt.getText().toString();
                String real_IdStr = realIdEt.getText().toString();
                if (aliPayStr.length() >= 5) {
                    if (real_nameStr.length() >= 2) {
                        if(real_IdStr.length() == 18){
                            saveAiliMessage(aliPayStr, real_nameStr,real_IdStr);
                        }else {
                            ViewInject.toast("请输入正确的身份证号");
                        }

                    } else {
                        ViewInject.toast("请输入正确的真实姓名");
                    }
                } else {
                    ViewInject.toast("请输入正确的支付宝账号");
                }
                break;

            case R.id.btn_del_shoukuan:
                showDialogExitEdit3("确定删除该收款信息？");
                break;
        }
    }

    /**
     * 保存接口
     * @param aa
     * @param bb
     */
    void saveAiliMessage(final String aa, final String bb,final String cc) {
        Map<String,Object> maps=new HashMap<>();
        maps.put("real_name", bb);
        maps.put("alipay", aa);
        maps.put("card_id", cc);
        new SaveAiliMessageApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "保存json === " + serverData.data);
                if (null != serverData) {
                    ShouKuan.DataBean data = JSONUtil.TransformShouKuan(serverData.data);
                    String alipay = data.getAlipay();
                    String real_name = data.getReal_name();
                    String card_id = data.getCard_id();
                    if (serverData.code.equals("1")) {
                        Log.e(TAG, "message == " + message);
                        if(serverData.message.length()>0){
                            if("".equals(aa)&&"".equals(bb)&&"".equals(cc)){
                                ViewInject.toast("删除成功");
                            }else {
                                ViewInject.toast(serverData.message);
                            }

                        }

                        Intent it = new Intent();
                        it.setClass(mContext, ModifyMyDataActivity.class);
                        it.putExtra("alipay", alipay);
                        it.putExtra("real_name", real_name);
                        it.putExtra("card_id", card_id);
                        setResult(BACK3, it);
                        finish();

                    } else {
                        ViewInject.toast(serverData.message);
                    }
                }
            }

        });

    }

    //dialog提示
    void showDialogExitEdit3(String neirong) {
        final EditExitDialog editDialog = new EditExitDialog(ShoukuanMessageActivity.this, R.style.mystyle, R.layout.dialog_fenqi_pop);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText(neirong);

        titleTv88.setHeight(60);

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });

        //删除信息
        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
                saveAiliMessage("", "","");
            }
        });
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}