package com.module.home.controller.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.fragment.ChannelBottomFragment;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.home.view.ChannelTagDialog;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.HashMap;

import butterknife.BindView;

/**
 * 频道二级页面
 */
public class ChannelTwoActivity extends YMBaseActivity {

    @BindView(R.id.two_channel_top)
    CommonTopBar mTop;

    public static final String TITLE = "title";
    public static final String TWO_ID = "two_id";
    public static final String SELECTED_ID = "selected_id";
    public static final String HOME_SOURCE = "home_source";

    private final String TAG = "ChannelTwoActivity";

    private TextView mCenterText;
    private ChannelTagDialog channelTagDialog;

    private String mTitle;          //标题
    public String mTwoId;          //二级id
    private String mSelectedId;     //选中id
    private String mHomeSource;
    private ProjectDetailsBean detailsBean;
    private ChannelBottomFragment channelBottomFragment;
    private boolean isUnfold = false;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_two_channel;
    }

    @Override
    protected void initView() {

        Bundle bundle = getIntent().getBundleExtra("data");
        mTitle = bundle.getString(TITLE);
        mTwoId = bundle.getString(TWO_ID);
        mSelectedId = bundle.getString(SELECTED_ID);
        mHomeSource = bundle.getString(HOME_SOURCE);
        Log.e(TAG, "mTwoId == " + mTwoId);
        Log.e(TAG, "mSelectedId == " + mSelectedId);

        //要传入的值
        detailsBean = new ProjectDetailsBean();
        detailsBean.setTwoLabelId(mTwoId);
        detailsBean.setHomeSource(mHomeSource);

        mTop.setCenterText(mTitle);
        mCenterText = mTop.getTv_center();

        Log.e(TAG, "mSelectedId === " + mSelectedId);
        Log.e(TAG, "mTwoId === " + mTwoId);
        if (!TextUtils.isEmpty(mSelectedId) && !"0".equals(mSelectedId)) {
            channelTagDialog = new ChannelTagDialog(mContext, mTop, mSelectedId);
        } else {
            channelTagDialog = new ChannelTagDialog(mContext, mTop, mTwoId);
        }

        setTopContent(isUnfold, true);

        //头部点击事件
        mCenterText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isUnfold) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("id", mTwoId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SURG_PART_DROP_DOWN), hashMap, new ActivityTypeData("96"));
                }

                setTopContent(!isUnfold);
            }
        });

        //弹层回调
        channelTagDialog.setItemClickListener(new ChannelTagDialog.ItemClickListener() {
            @Override
            public void onItemClick(String level, String id, String name, HashMap<String, String> params) {
                YmStatistics.getInstance().tongjiApp(params);
                Log.e(TAG, "params == " + params);
                switch (level) {
                    case "2":
                        setTopContent(!isUnfold);
                        Intent intent3 = new Intent(mContext, ChannelPartsActivity.class);
                        intent3.putExtra("id", id);
                        intent3.putExtra("title", name);
                        intent3.putExtra("home_source", mHomeSource);
                        startActivity(intent3);
                        break;
                    case "3":
                        mTop.setCenterText(name);
                        detailsBean.setTwoLabelId(id);
                        setTopContent(!isUnfold, true);
                        break;
                    case "4":
                        setTopContent(!isUnfold);
                        Intent intent4 = new Intent(mContext, ChannelFourActivity.class);
                        intent4.putExtra("id", id);
                        intent4.putExtra("title", name);
                        startActivity(intent4);
                        break;
                }
            }
        });

        View view = View.inflate(mContext, R.layout.channel_shopping_cart_view, null);
        RelativeLayout cartView = view.findViewById(R.id.channel_shopping_cart_view);
        TextView num = view.findViewById(R.id.channel_shopping_cart_num);
        setCartNum(num);
        mTop.setCustomContainer(view, Utils.dip2px(30),Utils.dip2px(35));
        //跳转购物车页面
        cartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", mTwoId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SHOPPING_CART_CLICK), hashMap, new ActivityTypeData("96"));
                startActivity(new Intent(mContext, ShoppingCartActivity.class));
            }
        });

    }

    @Override
    protected void initData() {

    }

    private void setTopContent(boolean isAn) {
        setTopContent(isAn, false);
    }

    private void setTopContent(boolean isAn, boolean isRefresh) {
        this.isUnfold = isAn;
        Drawable drawable;
        if (isUnfold) {
            drawable = Utils.getLocalDrawable(mContext, R.drawable.channel_an_open);
            channelTagDialog.showPop();
        } else {
            drawable = Utils.getLocalDrawable(mContext, R.drawable.channel_an_close);
            channelTagDialog.dismiss();
            if (isRefresh) {
                if (channelBottomFragment == null) {
                    channelBottomFragment = ChannelBottomFragment.newInstance(detailsBean);
                    setActivityFragment(R.id.two_channel_fragment, channelBottomFragment);
                } else {
                    channelBottomFragment.setDetailsBean(detailsBean);
                }
            }
        }
        drawable.setBounds(0, 0, Utils.dip2px(12), Utils.dip2px(12));
        mCenterText.setCompoundDrawables(null, null, drawable, null);
        mCenterText.setCompoundDrawablePadding(Utils.dip2px(5));
    }


    /**
     * 设置购物车数量
     */
    public void setCartNum(TextView num) {
        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            num.setVisibility(View.VISIBLE);
            num.setText(cartNumber);
        } else {
            num.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        if (channelTagDialog != null) {
            channelTagDialog.dismiss();
        }
        super.onDestroy();
    }
}
