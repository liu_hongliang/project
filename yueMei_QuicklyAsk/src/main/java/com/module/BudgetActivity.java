package com.module;

import com.module.base.view.YMBaseActivity;
import com.quicklyask.activity.R;

public class BudgetActivity extends YMBaseActivity {
//    public static final String TAG = "BudgetActivity";
//    @BindView(R.id.budget_tv_next)
//    TextView mBudgetTvNext;
//    @BindView(R.id.budget_btn_ok)
//    Button mBudgetBtnOk;
//    @BindView(R.id.budget_txt)
//    TextView mBudgetTxt;
//    @BindView(R.id.bubble)
//    BubblePicker mBubble;
//    @BindView(R.id.budget_rl_next)
//    RelativeLayout mBudgetRlNext;
//    private ArrayList<BudgetBean> mBudgetList;
//    private ArrayList<String> mTagname;
//    private String mBudget = "";
//    private ArrayList<PickerItem> mPickerItems = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_yu_suan;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
//        mBudgetList = getIntent().getParcelableArrayListExtra("budget");
//        Log.e(TAG,"mBudgetList"+mBudgetList.toString());
//        mTagname = getIntent().getStringArrayListExtra("tagname");
//        if (mTagname != null && mTagname.size() > 0) {
//            StringBuilder tagname = new StringBuilder();
//            for (int i = 0; i < mTagname.size(); i++) {
//                tagname.append(mTagname.get(i));
//                if (i < mTagname.size() - 1) {
//                    tagname.append("+");
//                }
//
//            }
//            mBudgetTxt.setText(tagname + "预算");
//        }
//        final int size = mBudgetList.size();
//        for (int i = 0; i <size ; i++) {
//            PickerItem pickerItem = new PickerItem();
//            pickerItem.setTitle(mBudgetList.get(i).getName());
//            pickerItem.setGradient(new BubbleGradient(ContextCompat.getColor(mContext,R.color.interent), ContextCompat.getColor(mContext,R.color.interent), BubbleGradient.VERTICAL));
//            pickerItem.setTextColor(ContextCompat.getColor(mContext, android.R.color.white));
//            pickerItem.setBackgroundImage(ContextCompat.getDrawable(mContext,R.drawable.interestballbg));
//            mPickerItems.add(pickerItem);
//        }
//        mBubble.setItems(mPickerItems);
//        mBubble.setBubbleSize(20);
//        mBubble.setMaxSelectedCount(1);
//        mBubble.setListener(new BubblePickerListener() {
//            @Override
//            public void onBubbleSelected(@NotNull PickerItem item) {
//                for (int i = 0; i < mBudgetList.size(); i++) {
//                    if (item.getTitle().equals(mBudgetList.get(i).getName())) {
//                        mBudget = mBudgetList.get(i).getId() + "";
//                        HashMap event_params = mBudgetList.get(i).getEvent_params();
//                        YmStatistics.getInstance().tongjiApp(event_params);
//                    }
//                }
//                Log.e(TAG, mBudget);
//                setButtonState();
//            }
//
//            @Override
//            public void onBubbleDeselected(@NotNull PickerItem item) {
//                for (int i = 0; i < mBudgetList.size(); i++) {
//                    if (item.getTitle().equals(mBudgetList.get(i).getName())) {
//                        mBudget = "";
//                    }
//                }
//                Log.e(TAG, mBudget);
//                setButtonState();
//            }
//        });

    }

//    private void setButtonState() {
//        if (TextUtils.isEmpty(mBudget)) {
//            mBudgetBtnOk.setBackgroundResource(R.drawable.select_interest_unselect);
//            mBudgetBtnOk.setText("请选择你的预算");
//            mBudgetBtnOk.setEnabled(false);
//        } else {
//            mBudgetBtnOk.setBackgroundResource(R.drawable.select_interest_shap);
//            mBudgetBtnOk.setText("进入首页");
//            mBudgetBtnOk.setEnabled(true);
//        }
//    }

//    @OnClick({R.id.budget_tv_next, R.id.budget_btn_ok,R.id.budget_rl_next})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.budget_tv_next:
//                if (Utils.isFastDoubleClick()) return;
//                startActivity(new Intent(BudgetActivity.this, MainTableActivity.class));
//                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COLD_START_BUDGET, "jump"));
//                finish();
//                break;
//            case R.id.budget_btn_ok:
//                if (Utils.isFastDoubleClick()) return;
//                HashMap<String, Object> maps = new HashMap<>();
//                maps.put("budget", mBudget);
//                maps.put("type", "2");
//                new CollectUserDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
//                    @Override
//                    public void onSuccess(ServerData serverData) {
//                        if ("1".equals(serverData.code)) {
//                            startActivity(new Intent(mContext, MainTableActivity.class));
//                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COLD_START_BUDGET, "ok"));
//                            finish();
//                        } else {
//                            Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                });
//                break;
//            case R.id.budget_rl_next:
//                if (Utils.isFastDoubleClick()) return;
//                startActivity(new Intent(BudgetActivity.this, MainTableActivity.class));
//                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COLD_START_BUDGET, "ok"));
//                finish();
//                break;
//        }
//
//    }


    @Override
    protected void onResume() {
        super.onResume();
//        mBubble.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        mBubble.onPause();
    }
}
