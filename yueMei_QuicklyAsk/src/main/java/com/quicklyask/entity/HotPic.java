package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 大家都在聊 图片
 * 
 * @author Rubin
 * 
 */
public class HotPic implements Parcelable {

	private String img;
	private String width;
	private String height;

	protected HotPic(Parcel in) {
		img = in.readString();
		width = in.readString();
		height = in.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(img);
		dest.writeString(width);
		dest.writeString(height);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<HotPic> CREATOR = new Creator<HotPic>() {
		@Override
		public HotPic createFromParcel(Parcel in) {
			return new HotPic(in);
		}

		@Override
		public HotPic[] newArray(int size) {
			return new HotPic[size];
		}
	};

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "HotPic{" +
				"img='" + img + '\'' +
				", width='" + width + '\'' +
				", height='" + height + '\'' +
				'}';
	}
}
