package com.quicklyask.entity;

import java.util.List;

public class DiscBBs {
	private String code;
	private String message;
	private List<DiscBBsData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<DiscBBsData> getData() {
		return data;
	}

	public void setData(List<DiscBBsData> data) {
		this.data = data;
	}


}
