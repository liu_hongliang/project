package com.module.commonview.view;

import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;

import com.alan.imagebrowser.gestureimage.GestureImageView;
import com.bumptech.glide.Glide;
import com.quicklyask.activity.R;

public class ImageViewFragment extends Fragment {
    private String imageUrl;
    private ProgressBar loadBar;
    private GestureImageView imageGiv;

    public View onCreateView(android.view.LayoutInflater inflater,
                             android.view.ViewGroup container,
                             android.os.Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_images_view_item, container,
                false);
        init(view);
        loadImage(imageUrl);
        return view;
    }

    private void init(View mView) {
        loadBar = mView.findViewById(R.id.imageView_loading_pb);
        imageGiv = mView
                .findViewById(R.id.imageView_item_giv);
        imageGiv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    public void loadImage(String url) {
        imageGiv.setVisibility(View.VISIBLE);
        Glide.with(getActivity()).load(url).into(imageGiv);
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
