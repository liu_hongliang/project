package com.module.commonview.module.bean;

public class ChatBackSuccessBean {
    private String rewardImg;
    private String successNoticeTitle;
    private String successNoticeDesc;
    private String rewardType;
    private String rewardMoney;

    public String getRewardImg() {
        return rewardImg;
    }

    public void setRewardImg(String rewardImg) {
        this.rewardImg = rewardImg;
    }

    public String getSuccessNoticeTitle() {
        return successNoticeTitle;
    }

    public void setSuccessNoticeTitle(String successNoticeTitle) {
        this.successNoticeTitle = successNoticeTitle;
    }

    public String getSuccessNoticeDesc() {
        return successNoticeDesc;
    }

    public void setSuccessNoticeDesc(String successNoticeDesc) {
        this.successNoticeDesc = successNoticeDesc;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getRewardMoney() {
        return rewardMoney;
    }

    public void setRewardMoney(String rewardMoney) {
        this.rewardMoney = rewardMoney;
    }
}
