package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2019/8/28
 */
public class CalendarReminderData {
    private String title;               //标题
    private String desc;                //事件描述
    private String startTime;           //开始时间 （精确到毫秒的时间戳）
    private String endTime;             //结束时间 （精确到毫秒的时间戳）
    private String advanceTime;         //提前提醒时间（精确到分钟）


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getAdvanceTime() {
        return advanceTime;
    }

    public void setAdvanceTime(String advanceTime) {
        this.advanceTime = advanceTime;
    }
}
