package com.module.community.web;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/6/20
 */
public class WebUrlData {
    private String type;
    private HashMap<String,String> parameter;

    public WebUrlData(String type, HashMap<String, String> parameter) {
        this.type = type;
        this.parameter = parameter;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public HashMap<String, String> getParameter() {
        return parameter;
    }

    public void setParameter(HashMap<String, String> parameter) {
        this.parameter = parameter;
    }
}
