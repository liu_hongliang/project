package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/8/15.
 */
public class PostPeoplePeopleList implements Parcelable {
    private String userImg;
    private String userUrl;

    public PostPeoplePeopleList(){

    }

    protected PostPeoplePeopleList(Parcel in) {
        userImg = in.readString();
        userUrl = in.readString();
    }

    public static final Creator<PostPeoplePeopleList> CREATOR = new Creator<PostPeoplePeopleList>() {
        @Override
        public PostPeoplePeopleList createFromParcel(Parcel in) {
            return new PostPeoplePeopleList(in);
        }

        @Override
        public PostPeoplePeopleList[] newArray(int size) {
            return new PostPeoplePeopleList[size];
        }
    };

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userImg);
        dest.writeString(userUrl);
    }
}
