/**
 * 
 */
package com.module.my.model.bean;

/**
 * @author Robin
 * 
 */
public class BaikeItemData {

	private String _id;
	private String title;
	private String suitable_crowd;
	private String welcome;
	private String safety;
	private String complexity;
	private String sharetitle;
	private String shareurl;

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the suitable_crowd
	 */
	public String getSuitable_crowd() {
		return suitable_crowd;
	}

	/**
	 * @param suitable_crowd
	 *            the suitable_crowd to set
	 */
	public void setSuitable_crowd(String suitable_crowd) {
		this.suitable_crowd = suitable_crowd;
	}

	/**
	 * @return the welcome
	 */
	public String getWelcome() {
		return welcome;
	}

	/**
	 * @param welcome
	 *            the welcome to set
	 */
	public void setWelcome(String welcome) {
		this.welcome = welcome;
	}

	/**
	 * @return the safety
	 */
	public String getSafety() {
		return safety;
	}

	/**
	 * @param safety
	 *            the safety to set
	 */
	public void setSafety(String safety) {
		this.safety = safety;
	}

	/**
	 * @return the complexity
	 */
	public String getComplexity() {
		return complexity;
	}

	/**
	 * @param complexity
	 *            the complexity to set
	 */
	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}

	/**
	 * @return the sharetitle
	 */
	public String getSharetitle() {
		return sharetitle;
	}

	/**
	 * @param sharetitle
	 *            the sharetitle to set
	 */
	public void setSharetitle(String sharetitle) {
		this.sharetitle = sharetitle;
	}

	/**
	 * @return the shareurl
	 */
	public String getShareurl() {
		return shareurl;
	}

	/**
	 * @param shareurl
	 *            the shareurl to set
	 */
	public void setShareurl(String shareurl) {
		this.shareurl = shareurl;
	}



}
