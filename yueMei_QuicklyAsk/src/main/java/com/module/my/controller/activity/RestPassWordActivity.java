/**
 * 
 */
package com.module.my.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Selection;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.doctor.model.api.CallAndSecurityCodeApi;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.ForgetPasswordApi;
import com.module.my.model.api.SendSecurityCode;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.GetPhoneCodePopWindow;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.HashMap;
import java.util.Map;

/**
 * 重置密码
 * 
 * @author Robin
 * 
 */
public class RestPassWordActivity extends BaseActivity {

	private final String TAG = "RestPassWordActivity";

	private RestPassWordActivity mContext;

	@BindView(id = R.id.register_back, click = true)
	private RelativeLayout colseRly;

	@BindView(id = R.id.reg_username)
	private EditText regUerEt;// 用户手机号
	@BindView(id = R.id.reg_password)
	private EditText regPasswordEt;// 密码
	@BindView(id = R.id.reg_yanzheng_code)
	private EditText regCodeEt;// 输入验证码

	@BindView(id = R.id.reg_bt, click = true)
	private Button regBt;// 注册

	@BindView(id = R.id.reg_send_code_rly, click = true)
	private RelativeLayout sendCodeRly;// 发送验证码按钮
	@BindView(id = R.id.reg_send_code_tv)
	private TextView sendCodeTv;// 发送验证码文字

	@BindView(id = R.id.restpassword_title_tv)
	private TextView titleTv;// 头标题

	private final int BACK3 = 3;
	private String type;

	@BindView(id = R.id.password_if_ming_iv, click = true)
	private ImageView passIfShowIv;
	private boolean passIsShow = false;

	@BindView(id = R.id.nocde_message_tv, click = true)
	private TextView noCodeTv;// 没收到验证码

	private PopupWindows yuyinCodePop;
	@BindView(id = R.id.order_time_all_ly)
	private LinearLayout allcontent;

	private GetPhoneCodePopWindow phoneCodePop;

	ImageOptions imageOptions;
	private LoadingProgress mDialog;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_rest_password);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = RestPassWordActivity.this;
		mDialog = new LoadingProgress(mContext);

		imageOptions = new ImageOptions.Builder()
				.setUseMemCache(false)
				.setImageScaleType(ImageView.ScaleType.FIT_XY)
				.build();

		Intent it = getIntent();
		type = it.getStringExtra("type");

		if (type.equals("1")) {
			titleTv.setText("重置密码");
		} else if (type.equals("2")) {
			titleTv.setText("修改密码");
		}
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.register_back:
			finish();
			break;
		case R.id.reg_send_code_rly:// 发送验证码
			String phoneNumber = regUerEt.getText().toString();
			if (null != phoneNumber && phoneNumber.length() > 0) {
				if (judgeEmailAndPhone(phoneNumber)) {
					sendCode(phoneNumber);
					noCodeTv.setVisibility(View.VISIBLE);
					noCodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？" + "</u>"));
				} else {
					ViewInject.toast("请输入正确的手机号");
				}
			} else {
				ViewInject.toast("请输入手机号");
			}
			break;
		case R.id.reg_bt:// 保存
			String codeStr = regCodeEt.getText().toString();
			String password = regPasswordEt.getText().toString();
			String phone = regUerEt.getText().toString();
			if (null != phone && phone.length() > 0) {

				if (null != codeStr && codeStr.length() > 0) {
					if (null != password && password.length() > 0) {
						if (password.length() > 5 && password.length() < 26) {
							registerData(codeStr, password, phone);
						} else {
							ViewInject.toast("请输入6到25个字符密码");
						}
					} else {
						ViewInject.toast("请输入密码");
					}
				} else {
					ViewInject.toast("请输入验证码");
				}
			} else {
				ViewInject.toast("请输入手机号");
			}
			break;
		case R.id.password_if_ming_iv:// 密码是否铭文
			if (passIsShow) {
				passIfShowIv.setBackgroundResource(R.drawable.miwen_);
				regPasswordEt.setInputType(InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_PASSWORD);
				Editable etext = regPasswordEt.getText();
				Selection.setSelection(etext, etext.length());
				passIsShow = false;
			} else {
				passIfShowIv.setBackgroundResource(R.drawable.mingwen_);
				regPasswordEt
						.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
				Editable etext = regPasswordEt.getText();
				Selection.setSelection(etext, etext.length());
				passIsShow = true;
			}
			break;
		case R.id.nocde_message_tv:// 没有收到验证码
			yuyinCodePop = new PopupWindows(mContext, allcontent);
			yuyinCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

			Glide.with(mContext).load(FinalConstant.TUXINGCODE).into(codeIv);

			break;
		}
	}

	EditText codeEt;
	ImageView codeIv;

	/**
	 * 获取手机号 并验证
	 * 
	 * @author dwb
	 * 
	 */
	public class PopupWindows extends PopupWindow {

		@SuppressWarnings("deprecation")
		public PopupWindows(Context mContext, View parent) {

			final View view = View.inflate(mContext, R.layout.pop_yuyincode,
					null);

			view.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.fade_ins));

			setWidth(LayoutParams.MATCH_PARENT);
			setHeight(LayoutParams.MATCH_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			// showAtLocation(parent, Gravity.BOTTOM, 0, 0);
			update();

			Button cancelBt = view.findViewById(R.id.cancel_bt);
			Button tureBt = view.findViewById(R.id.zixun_bt);
			codeEt = view.findViewById(R.id.no_pass_login_code_et);
			codeIv = view.findViewById(R.id.yuyin_code_iv);

			RelativeLayout rshCodeRly = view
					.findViewById(R.id.no_pass_yanzheng_code_rly);

			rshCodeRly.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					x.image().bind(codeIv,FinalConstant.TUXINGCODE,imageOptions);
				}
			});

			tureBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String codes = codeEt.getText().toString();
					if (codes.length() > 1) {
						yanzhengCode(codes);
					} else {
						ViewInject.toast("请输入图中数字");
					}
				}
			});

			cancelBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

		}
	}

	void yanzhengCode(String codes) {
		String phones = regUerEt.getText().toString().trim();
		Map<String,Object> maps=new HashMap<>();
		maps.put("phone",phones);
		maps.put("code",codes);
		maps.put("flag","setpass");
		new CallAndSecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData s) {
				if ("1".equals(s.code)){
					yuyinCodePop.dismiss();
					ViewInject.toast("正在拨打您的电话，请注意接听");
				}else{
					ViewInject.toast("数字错误，请重新输入");
				}
			}

		});
//		KJHttp kjh = new KJHttp();
//		KJStringParams params = new KJStringParams();
//		params.put("phone", phones);
//		params.put("code", codes);
//		params.put("flag", "setpass");
//		kjh.post(FinalConstant.YANZCODE, params, new StringCallBack() {
//
//			@Override
//			public void onSuccess(String json) {
//				if (null != json && json.length() > 0) {
//					String code = JSONUtil
//							.resolveJson(json, FinalConstant.CODE);
//					String message = JSONUtil.resolveJson(json,
//							FinalConstant.MESSAGE);
//					if (code.equals("1")) {
//						yuyinCodePop.dismiss();
//						ViewInject.toast("正在拨打您的电话，请注意接听");
//					} else {
//						ViewInject.toast("数字错误，请重新输入");
//					}
//
//				}
//			}
//		});
	}

	private boolean judgeEmailAndPhone(String nameStr) {
		if (nameStr.contains("@")) {
            return Utils.emailFormat(nameStr);
		} else {
            return Utils.isMobile(nameStr);
		}
    }

	void sendCode(String phoneNumber) {
		Map<String,Object>maps=new HashMap<>();
		maps.put("phone",phoneNumber);
		new ForgetPasswordApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					ViewInject.toast(serverData.message);

					regPasswordEt.requestFocus();
					sendCodeRly.setClickable(false);
					new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

						@Override
						public void onTick(long millisUntilFinished) {
							sendCodeRly
									.setBackgroundResource(R.drawable.biankuang_hui);
							sendCodeTv.setTextColor(getResources()
									.getColor(R.color.button_zi));
							sendCodeTv.setText("("
									+ millisUntilFinished / 1000
									+ ")重新获取");
						}

						@Override
						public void onFinish() {
							sendCodeRly
									.setBackgroundResource(R.drawable.shape_bian_ff5c77);
							sendCodeTv
									.setTextColor(getResources()
											.getColor(
													R.color.button_bian_hong1));
							sendCodeRly.setClickable(true);
							sendCodeTv.setText("重发验证码");
						}
					}.start();
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
//		KJHttp kjh = new KJHttp();
//		KJStringParams params = new KJStringParams();
//		params.put("phone", phoneNumber);
//		kjh.post(FinalConstant.MIMA1 + Utils.getTokenStr(), params,
//				new StringCallBack() {
//
//					@Override
//					public void onSuccess(String json) {
//						if (null != json) {
//							String code = JSONUtil.resolveJson(json,
//									FinalConstant.CODE);
//							String message = JSONUtil.resolveJson(json,
//									FinalConstant.MESSAGE);
//							if (code.equals("1")) {
//								ViewInject.toast(message);
//
//								regPasswordEt.requestFocus();
//								sendCodeRly.setClickable(false);
//								new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。
//
//									@Override
//									public void onTick(long millisUntilFinished) {
//										sendCodeRly
//												.setBackgroundResource(R.drawable.biankuang_hui);
//										sendCodeTv.setTextColor(getResources()
//												.getColor(R.color.button_zi));
//										sendCodeTv.setText("("
//												+ millisUntilFinished / 1000
//												+ ")重新获取");
//									}
//
//									@Override
//									public void onFinish() {
//										sendCodeRly.setBackgroundResource(R.drawable.shape_bian_ff5c77);
//										sendCodeTv.setTextColor(getResources()
//														.getColor(
//																R.color.button_bian_hong1));
//										sendCodeRly.setClickable(true);
//										sendCodeTv.setText("重发验证码");
//									}
//								}.start();
//							} else {
//								ViewInject.toast(message);
//							}
//						}
//					}
//				});

	}

	void registerData(String code, String pass, String phone) {
		Map<String,Object>maps=new HashMap<>();
		maps.put("flag","2");
		maps.put("code",code);
		maps.put("phone",phone);
		maps.put("firstpwd",pass);
		new SendSecurityCode().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					// 修改密码成功
					ViewInject.toast(serverData.message);
					showActivity(aty, ModifySuccessActivity.class);
					finish();
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
//		KJHttp kjh = new KJHttp();
//		KJStringParams params = new KJStringParams();
//
//		params.put("flag", "2");
//		params.put("code", code);
//		params.put("phone", phone);
//		params.put("firstpwd", pass);
//		kjh.post(FinalConstant.MIMA2 + Utils.getTokenStr(), params,
//				new StringCallBack() {
//
//					@Override
//					public void onSuccess(String json) {
//						if (null != json) {
//							String code = JSONUtil.resolveJson(json,
//									FinalConstant.CODE);
//							String message = JSONUtil.resolveJson(json,
//									FinalConstant.MESSAGE);
//							if (code.equals("1")) {
//								// 修改密码成功
//								ViewInject.toast(message);
//								showActivity(aty, ModifySuccessActivity.class);
//								finish();
//							} else {
//								ViewInject.toast(message);
//							}
//						}
//					}
//				});

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
