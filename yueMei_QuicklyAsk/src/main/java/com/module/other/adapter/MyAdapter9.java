package com.module.other.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.doctor.model.bean.TaoPopItemIvData;
import com.module.other.activity.HomeDiarySXActivity;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by dwb on 16/11/1.
 */
public class MyAdapter9 extends BaseAdapter {

    private final String TAG = "MyAdapter9";

    private List<TaoPopItemIvData> mTaoPopItemData;
    private Context mContext;
    private LayoutInflater inflater;
    private TaoPopItemIvData TaoPopItemData;
    ViewHolder viewHolder;

    public MyAdapter9(Context mContext, List<TaoPopItemIvData> mTaoPopItemData) {
        this.mContext = mContext;
        this.mTaoPopItemData = mTaoPopItemData;
        inflater = LayoutInflater.from(mContext);
    }

    static class ViewHolder {
        public TextView mPart1NameTV;
        public ImageView mIv;
    }

    @Override
    public int getCount() {
        return mTaoPopItemData.size();
    }

    @Override
    public Object getItem(int position) {
        return mTaoPopItemData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_item1, null);
            viewHolder = new ViewHolder();

            viewHolder.mPart1NameTV = convertView
                    .findViewById(R.id.tv);
            viewHolder.mIv = convertView.findViewById(R.id.iv);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (position == HomeDiarySXActivity.mPosition) {
            convertView.setBackgroundResource(R.color.white);
            viewHolder.mPart1NameTV.setTextColor(mContext.getResources()
                    .getColor(R.color.red_title));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#eeeeee"));
            viewHolder.mPart1NameTV.setTextColor(Color.parseColor("#333333"));
        }

        TaoPopItemData = mTaoPopItemData.get(position);
        viewHolder.mPart1NameTV.setText(TaoPopItemData.getName());

//        x.image().bind(viewHolder.mIv, TaoPopItemData.getImg(), imageOptions);

        return convertView;
    }

    public void add(List<TaoPopItemIvData> infos) {
        mTaoPopItemData.addAll(infos);
    }
}
