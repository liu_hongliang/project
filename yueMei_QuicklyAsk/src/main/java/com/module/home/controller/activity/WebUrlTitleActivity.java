/**
 *
 */
package com.module.home.controller.activity;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.PageJumpManager;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.home.controller.other.UrlTitleWebViewClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.Map;

/**
 * 传链接与头标题的web
 *
 * @author Robin
 */
public class WebUrlTitleActivity extends BaseActivity {

    private final String TAG = "WebUrlTitleActivity";

    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private LinearLayout contentWeb;

    private CommonTopBar mTop;// 返回

    private WebView docDetWeb;

    private WebUrlTitleActivity mContex;

    public JSONObject obj_http;

    private String url;
    private String title;

    private int bTheight;
    private int windowsH;
    private int statusBarHeight;
    private int windowsW;



    @BindView(id = R.id.all_content)
    private LinearLayout contentLy;
    private String uid;
    private Map<String, String> singStr;
    private String isRsa = "0";
    private String city = "全国";
    private String full_name;
    private String mobile;
    private String slidingFinish;
    private PageJumpManager pageJumpManager;
    private BaseWebViewClientMessage viewClientMessage;
    private String mTitle;


    @Override
    public void setRootView() {
        slidingFinish = getIntent().getStringExtra("slidingFinish");
        if (slidingFinish == null) {
            slidingFinish = "1";
            setContentView(R.layout.acty_peifu_basic_web);
            mTop = findViewById(R.id.basic_web_top);
        }else {
            setContentView(R.layout.acty_peifu_basic_web1);
            mTop = findViewById(R.id.peifu_casic_top);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = WebUrlTitleActivity.this;

        pageJumpManager = new PageJumpManager(mContex);

        uid = Utils.getUid();
        windowsH = Cfg.loadInt(mContex, FinalConstant.WINDOWS_H, 0);
        windowsW = Cfg.loadInt(mContex, FinalConstant.WINDOWS_W, 0);

        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        contentLy.measure(w, h);
        bTheight = contentLy.getMeasuredHeight();

        Rect rectangle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        statusBarHeight = rectangle.top;

        Intent it = getIntent();
        url = it.getStringExtra("link");
        isRsa = it.getStringExtra("isRsa");
        mTitle = it.getStringExtra("title");

        if (isRsa == null) {
            isRsa = "0";
        }
        String str = Cfg.loadStr(mContex, FinalConstant.DWCITY, "");
        if (!TextUtils.isEmpty(str)) {
            city=str;
        }else city = "全国";

        viewClientMessage = new BaseWebViewClientMessage(mContex);
        viewClientMessage.setBaseWebViewClientCallback(new UrlTitleWebViewClient(mContex));
        viewClientMessage.setView(contentLy);

        if ("0".equals(isRsa)) {
            Log.d(TAG,"url===="+url);
            url = FinalConstant.baseUrl + FinalConstant.VER + url;
            Log.d(TAG,"url====>>>"+url);
            initWebview();
            LodUrl1(url);
        } else {
            WebSignData addressAndHead = SignUtils.getAddressAndHead(FinalConstant.baseUrl + FinalConstant.VER + url);
            url = addressAndHead.getUrl();
            singStr = addressAndHead.getHttpHeaders();
            initWebview();
            Log.d(TAG,"url:=="+url);
            LodUrl2(url, singStr);
        }

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void LodUrl2(String urlstr, Map<String, String> singStr) {
        viewClientMessage.startLoading();

        docDetWeb.loadUrl(urlstr, singStr);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }


    public void initWebview() {
        docDetWeb = new WebView(mContex);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            docDetWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        docDetWeb.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        docDetWeb.getSettings().setUseWideViewPort(true);	//设置webview推荐使用的窗口，使html界面自适应屏幕
        docDetWeb.getSettings().setLoadWithOverviewMode(true);  //设置webview加载的页面的模式
        docDetWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(viewClientMessage);
        docDetWeb.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onReceivedTitle(WebView view, String title) {
                mTop.setCenterText(mTitle);
                super.onReceivedTitle(view, title);
            }
        });
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) docDetWeb.getLayoutParams();
        linearParams.height = windowsH - bTheight - statusBarHeight * 2 - 60;
        linearParams.weight = windowsW;
        docDetWeb.setLayoutParams(linearParams);


        contentWeb.addView(docDetWeb);
    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        viewClientMessage.startLoading();
        WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

}