package com.module.commonview.module.bean;

import com.quicklyask.entity.SearchResultBoard;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2019/6/27
 */
public class ProjectDetailBoard {
    private ArrayList<SearchResultBoard> screen_board;

    public ArrayList<SearchResultBoard> getScreen_board() {
        return screen_board;
    }

    public void setScreen_board(ArrayList<SearchResultBoard> screen_board) {
        this.screen_board = screen_board;
    }
}
