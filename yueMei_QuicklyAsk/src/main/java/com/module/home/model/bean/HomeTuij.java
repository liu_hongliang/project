package com.module.home.model.bean;

/**
 * Created by dwb on 16/2/22.
 */
public class HomeTuij {

    private String _id;
    private String type;
    private String link;
    private String title;
    private String docname;
    private String subtitle;
    private String img;
    private String totalappoitment;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTotalappoitment() {
        return totalappoitment;
    }

    public void setTotalappoitment(String totalappoitment) {
        this.totalappoitment = totalappoitment;
    }
}
