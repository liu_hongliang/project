package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.HotBean;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.List;

/**
 * 超值特卖的适配器
 * Created by 裴成浩 on 2019/4/2
 */
public class ProjectDetailsSalesAdapter extends RecyclerView.Adapter<ProjectDetailsSalesAdapter.ViewHolder> {

    private String TAG = "ProjectDetailsSalesAdapter";
    private final Context mContext;
    private List<HotBean> mDatas;

    public ProjectDetailsSalesAdapter(Context context, List<HotBean> data) {
        this.mContext = context;
        this.mDatas = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_project_details_sales, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) viewHolder.mSales.getLayoutParams();
        if (position == 0) {
            params.bottomMargin = Utils.dip2px(5);
        } else if (position == mDatas.size() - 1) {
            params.topMargin = Utils.dip2px(5);
        } else {
            params.topMargin = Utils.dip2px(5);
            params.bottomMargin = Utils.dip2px(5);
        }
        viewHolder.mSales.setLayoutParams(params);

        HotBean hotBean = mDatas.get(position);

        //设置图片
        Glide.with(mContext)
                .load(hotBean.getImg())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.LEFT))
                .into(viewHolder.mSalesImage);

        viewHolder.mSalesTitle.setText(hotBean.getTitle());
        viewHolder.mSalesContent.setText(hotBean.getDesc());
        viewHolder.mSalesPrice.setText(hotBean.getPrice());

        String memberPrice = hotBean.getMember_price();
        if (Integer.parseInt(memberPrice) >= 0 && Integer.parseInt(memberPrice) < 10000) {
            viewHolder.mSalesOldPriceClick.setVisibility(View.VISIBLE);
            viewHolder.mSalesOldPrice.setText("¥" + memberPrice);
        } else {
            viewHolder.mSalesOldPriceClick.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mSales;
        ImageView mSalesImage;
        LinearLayout mSalesClick;
        TextView mSalesTitle;
        TextView mSalesContent;
        TextView mSalesPrice;
        LinearLayout mSalesOldPriceClick;
        TextView mSalesOldPrice;
        TextView mSalesBut;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mSales = itemView.findViewById(R.id.item_project_details_sales);
            mSalesImage = itemView.findViewById(R.id.item_project_details_sales_image);
            mSalesClick = itemView.findViewById(R.id.item_project_details_sales_click);
            mSalesTitle = itemView.findViewById(R.id.item_project_details_sales_title);
            mSalesContent = itemView.findViewById(R.id.item_project_details_sales_content);
            mSalesPrice = itemView.findViewById(R.id.item_project_details_sales_price);
            mSalesOldPriceClick = itemView.findViewById(R.id.item_project_details_sales_old_price_click);
            mSalesOldPrice = itemView.findViewById(R.id.item_project_details_sales_old_price);
            mSalesBut = itemView.findViewById(R.id.item_project_details_sales_but);

            //去购买点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HotBean hotBean = mDatas.get(getLayoutPosition());
                    String url = hotBean.getUrl();
                    Log.e(TAG, "url == " + url);
                    Log.e(TAG, "hotBean.getEvent_params() == " + hotBean.getEvent_params().toString());
                    if (!TextUtils.isEmpty(url)) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_JINGTUI, (getLayoutPosition() + 1) + ""), hotBean.getEvent_params());
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                }
            });
        }
    }
}
