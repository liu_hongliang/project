package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2019/2/27
 */
public class ProjectDetailsTag implements Parcelable {
    private String id;
    private String name;
    private ArrayList<ProjectDetailsListData> list;


    protected ProjectDetailsTag(Parcel in) {
        id = in.readString();
        name = in.readString();
        list = in.createTypedArrayList(ProjectDetailsListData.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeTypedList(list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProjectDetailsTag> CREATOR = new Creator<ProjectDetailsTag>() {
        @Override
        public ProjectDetailsTag createFromParcel(Parcel in) {
            return new ProjectDetailsTag(in);
        }

        @Override
        public ProjectDetailsTag[] newArray(int size) {
            return new ProjectDetailsTag[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ProjectDetailsListData> getList() {
        return list;
    }

    public void setList(ArrayList<ProjectDetailsListData> list) {
        this.list = list;
    }
}
