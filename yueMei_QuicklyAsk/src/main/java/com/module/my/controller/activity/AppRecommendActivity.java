/**
 *
 */
package com.module.my.controller.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.doctor.controller.activity.WantBeautifulActivity548;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.view.ElasticScrollView;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 热门应用推荐
 *
 * @author Robin
 *
 */
public class AppRecommendActivity extends BaseActivity {

	private final String TAG = "AppRecommendActivity";

	@BindView(id = R.id.wan_beautifu_web_det_scrollview3, click = true)
	private ElasticScrollView scollwebView;
	@BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.wan_beautiful_web_back, click = true)
	private RelativeLayout back;// 返回

	private WebView docDetWeb;

	private AppRecommendActivity mContex;

	public JSONObject obj_http;
	private LoadingProgress mDialog;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_app_recommend);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = AppRecommendActivity.this;

		mDialog = new LoadingProgress(mContex);

		scollwebView.GetLinearLayout(contentWeb);
		initWebview();

		LodUrl1(FinalConstant.HOTAPP);
		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						AppRecommendActivity.this.finish();
					}
				});
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			docDetWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
		}
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	protected void OnReceiveData(String str) {
		scollwebView.onRefreshComplete();
	}

	public void webReload() {
		if (docDetWeb != null) {
			mDialog.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 处理webview里面的按钮
	 *
	 * @param urlStr
	 */
	public void showWebDetail(String urlStr) {
		// Log.e(TAG, urlStr);
		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			if (obj.getString("type").equals("1")) {// 在线留言
				Intent it = new Intent();
				it.putExtra("po", "2270");
				it.putExtra("hosid", "0");
				it.putExtra("docid", "0");
				it.putExtra("shareid", "0");
				it.putExtra("cityId", "0");
				it.putExtra("partId", "0");
				it.putExtra("partId_two", "0");
				it.setClass(mContex, WantBeautifulActivity548.class);
				startActivity(it);
			}

			if (obj.getString("type").equals("2")) {// 打电话
				String tel = obj.getString("tel");
				showDialog(tel);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	TextView telMeesageTv;
	TextView telTitleTv;

	void showDialog(String tel) {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContex);
		View view = View.inflate(mContex, R.layout.dialog_exit_login, null);
		builder.setView(view);
		telMeesageTv = view.findViewById(R.id.dialog_tel_tv);
		telMeesageTv.setVisibility(View.VISIBLE);
		telTitleTv = view.findViewById(R.id.dialog_exit_content_tv);
		telTitleTv.setText("拨打" + tel + "？");
		// builder.setTitle("退出");
		builder.setPositiveButton("拨打", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {

				ViewInject.toast("正在拨打中·····");
				Intent it = new Intent(Intent.ACTION_CALL, Uri
						.parse("tel:4000567118"));
				if (ActivityCompat.checkSelfPermission(AppRecommendActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
					// TODO: Consider calling
					//    ActivityCompat#requestPermissions
					// here to request the missing permissions, and then overriding
					//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
					//                                          int[] grantResults)
					// to handle the case where the user grants the permission. See the documentation
					// for ActivityCompat#requestPermissions for more details.
					return;
				}
				startActivity(it);
			}
		});
		builder.setNegativeButton("取消", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
		builder.create().show();
	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {

		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		if (null != docDetWeb) {
			docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
		}
	}

	@SuppressLint("NewApi")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.wan_beautiful_web_back:
			onBackPressed();
			break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
