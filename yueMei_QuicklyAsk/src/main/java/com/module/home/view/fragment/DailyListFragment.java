package com.module.home.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.cpoopc.scrollablelayoutlib.ScrollableHelper;
import com.module.base.api.BaseCallBackListener;
import com.module.community.model.bean.BBsListData550;
import com.module.community.model.bean.UserClickData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.adapter.DiaryListAdapter;
import com.module.doctor.model.api.LodHotIssueDataApi;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultData23;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.DropDownListView;
import com.umeng.analytics.MobclickAgent;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 搜索后的日记贴
 *
 * @author Rubin
 */
public class DailyListFragment extends ScrollAbleFragment implements ScrollableHelper.ScrollableContainer {

    private final String TAG = "DailyListFragment";

    private Activity mCotext;

    private LinearLayout docText;
    // List
    private DropDownListView mlist;
    private int mCurPage = 1;
    private Handler mHandler;

    private List<BBsListData550> lvBBslistData = new ArrayList<>();
    private List<BBsListData550> lvBBslistMoreData = new ArrayList<>();
    private DiaryListAdapter bbsListAdapter;

    private String id = "";
    private String city = "";
    private String hosId = "";

    private String key;
    private LinearLayout nodataTv;
    private LoadingProgress progress;


    public static DailyListFragment newInstance(String key, String city) {
        return newInstance(key, city, "");
    }

    public static DailyListFragment newInstance(String key, String city, String hosId) {

        DailyListFragment listFragment = new DailyListFragment();
        Bundle b = new Bundle();
        b.putString("key", key);
        b.putString("city", city);
        b.putString("hosId", hosId);
        listFragment.setArguments(b);
        return listFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search_list, container, false);

        mlist = v.findViewById(R.id.list_searh);
        nodataTv = v.findViewById(R.id.my_collect_post_tv_nodata1);

        progress = new LoadingProgress(mCotext);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCotext = getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
        super.onActivityCreated(savedInstanceState);

        if (isAdded()) {
            key = getArguments().getString("key");
            city = getArguments().getString("city");
            hosId = getArguments().getString("hosId");
            if (null == hosId) {
                hosId = "";
            }
        }
        initList();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    void initList() {

        mHandler = getHandler();
        progress.startLoading();
        lodBBsListData550(true);

        mlist.setOnBottomListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                lodBBsListData550(false);
            }
        });

        mlist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos,
                                    long arg3) {
                String url = lvBBslistData.get(pos).getUrl();
                if (url.length() > 0) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH,"diarylist|"+(pos+1)+"|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE),lvBBslistData.get(pos).getEvent_params(),new ActivityTypeData("40"));
                    String qid = lvBBslistData.get(pos).getQ_id();
                    String appmurl = lvBBslistData.get(pos).getAppmurl();
                    WebUrlTypeUtil.getInstance(mCotext).urlToApp(appmurl, "0", "0");
                }
            }
        });

    }

    void lodBBsListData550(final boolean isDonwn) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, Object> maps = new HashMap<>();
                maps.put("key", URLEncoder.encode(key));
                maps.put("type", "3");
                maps.put("high", "1");
                if (!"".equals(hosId)) {
                    maps.put("id", hosId);
                }
                maps.put("page", mCurPage + "");
                if (isDonwn) {
                    if (mCurPage == 1) {
                        Log.e("url222", "11111");
                        new LodHotIssueDataApi().getCallBack(mCotext, maps, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData serverData) {
                                if ("1".equals(serverData.code)) {
                                    try {
                                        SearchResultData23 searchResultData23 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData23.class);
                                        lvBBslistData = searchResultData23.getList();
                                        Message message = mHandler.obtainMessage(1);
                                        message.sendToTarget();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                    }
                } else {
                    mCurPage++;
                    maps.put("page", mCurPage + "");
                    Log.e("url222", "22222");
                    new LodHotIssueDataApi().getCallBack(mCotext, maps, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData) {
                            if ("1".equals(serverData.code)) {
                                try {
                                    SearchResultData23 searchResultData23 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData23.class);
                                    lvBBslistMoreData = searchResultData23.getList();
                                    Message message = mHandler.obtainMessage(2);
                                    message.sendToTarget();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                    //lvBBslistMoreData = HttpData.loadSearchBBsListData(url,key, mCurPage, "3", city, hosId);
                }
            }
        }).start();

    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (lvBBslistData != null) {
                            nodataTv.setVisibility(View.GONE);
                            progress.stopLoading();
                            if (null != getActivity()) {
                                bbsListAdapter = new DiaryListAdapter(getActivity(), lvBBslistData, "");
                                mlist.setAdapter(bbsListAdapter);
                                bbsListAdapter.setOnItemPersonClickListener(new DiaryListAdapter.OnItemPersonClickListener() {
                                    @Override
                                    public void onItemPersonClick(String id, int pos) {
                                        String user_center_url = lvBBslistData.get(pos).getUser_center_url();
                                        UserClickData userClickData = lvBBslistData.get(pos).getUserClickData();
                                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_SHARELIST_USER_CLICK,(pos+1)+""),userClickData.getEvent_params(),new ActivityTypeData("40"));
                                        WebUrlTypeUtil.getInstance(mCotext).urlToApp(user_center_url);
                                    }
                                });
                            }


                            mlist.onBottomComplete();
                        } else {
                            progress.stopLoading();
                            nodataTv.setVisibility(View.VISIBLE);
                            mlist.setVisibility(View.GONE);
                        }
                        break;
                    case 2:
                        if (null != lvBBslistMoreData && lvBBslistMoreData.size() > 0) {
                            bbsListAdapter.add(lvBBslistMoreData);
                            bbsListAdapter.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {
                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }

    @Override
    public View getScrollableView() {
        return mlist;
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("MainScreen"); // 统计页面
        StatService.onResume(getActivity());
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("MainScreen");
        StatService.onPause(getActivity());
    }


}