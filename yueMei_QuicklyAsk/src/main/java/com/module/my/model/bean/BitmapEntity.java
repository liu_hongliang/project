package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2017/6/27.
 */

public class BitmapEntity {
    private String title;
    private String path;
    private long size;
    private String uri;
    private long duration;

    public BitmapEntity(String title, String path, long size, String uri_thumb, long duration) {
        this.title = title;
        this.path = path;
        this.size = size;
        this.uri = uri_thumb;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
