package com.module.my.model.bean;


public class MyIconBean  {
    private String name;
    private int icon;
    private String num;
    private String isSign;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }


    public String getIsSign() {
        return isSign;
    }

    public void setIsSign(String isSign) {
        this.isSign = isSign;
    }

    @Override
    public String toString() {
        return "MyIconBean{" +
                "name='" + name + '\'' +
                ", icon=" + icon +
                ", num='" + num + '\'' +
                '}';
    }
}
