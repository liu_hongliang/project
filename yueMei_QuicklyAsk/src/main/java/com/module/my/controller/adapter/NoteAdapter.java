package com.module.my.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.my.model.bean.NoteBookListData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import org.xutils.image.ImageOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dwb on 16/3/21.
 */
public class NoteAdapter extends BaseAdapter {

    private final String TAG = "NoteAdapter";

    private List<NoteBookListData> mNoteBookListData = new ArrayList<NoteBookListData>();
    private Context mContext;
    private LayoutInflater inflater;
    private NoteBookListData noteBookListData;
    ViewHolder viewHolder;

    ImageOptions imageOptions;

    public NoteAdapter(Context mContext, List<NoteBookListData> mNoteBookListData) {
        this.mContext = mContext;
        this.mNoteBookListData = mNoteBookListData;
        inflater = LayoutInflater.from(mContext);

        imageOptions = new ImageOptions.Builder()
//				.setSize(DensityUtil.dip2px(120), DensityUtil.dip2px(120))
//				.setRadius(DensityUtil.dip2px(5))
                // 如果ImageView的大小不是定义为wrap_content, 不要crop.
                .setCrop(true) // 很多时候设置了合适的scaleType也不需要它.
                // 加载中或错误图片的ScaleType
                //.setPlaceholderScaleType(ImageView.ScaleType.MATRIX)
                .setImageScaleType(ImageView.ScaleType.FIT_XY)
                .setLoadingDrawableId(R.drawable.radius_gray80)
                .setFailureDrawableId(R.drawable.radius_gray80)
                .build();
    }

    static class ViewHolder {
        public ImageView mIv;
        public TextView mTitleTv;
        public TextView mPageTv;
        public TextView mSubTitleTv;
        public ImageView mIsNewIv;
        public LinearLayout mFxTipsLy;
    }

    @Override
    public int getCount() {
        return mNoteBookListData.size();
    }

    @Override
    public Object getItem(int position) {
        return mNoteBookListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_note_select, null);
            viewHolder = new ViewHolder();

            viewHolder.mIv = convertView
                    .findViewById(R.id.note_select_list_iv);
            viewHolder.mTitleTv = convertView
                    .findViewById(R.id.note_select_title_tv);
            viewHolder.mPageTv = convertView
                    .findViewById(R.id.note_select_page_tv);
            viewHolder.mSubTitleTv = convertView
                    .findViewById(R.id.note_select_subtitle);
            viewHolder.mIsNewIv = convertView
                    .findViewById(R.id.note_select_isnew_iv);
            viewHolder.mFxTipsLy= convertView
                    .findViewById(R.id.note_fanxian_tips_ly);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        noteBookListData=mNoteBookListData.get(position);

//        bitmapUtils.display(viewHolder.mIv,noteBookListData.getImg(),bitmapConfig);

//        x.image().bind(viewHolder.mIv,noteBookListData.getImg(),imageOptions);

        Glide.with(mContext)
                .load(noteBookListData.getImg())
                .transform(new GlideRoundTransform(mContext, Utils.dip2px(2)))
                .placeholder(R.drawable.radius_gray80)
                .error(R.drawable.radius_gray80)
                .into(viewHolder.mIv);

        viewHolder.mTitleTv.setText(noteBookListData.getTitle());

        String isFx=noteBookListData.getIs_fanxian();
        if(isFx.equals("1")){
            viewHolder.mFxTipsLy.setVisibility(View.VISIBLE);
        }else {
            viewHolder.mFxTipsLy.setVisibility(View.GONE);
        }

        String isnew=noteBookListData.getIs_new();
        if(isnew.equals("1")){
            viewHolder.mSubTitleTv.setText("");
            viewHolder.mIsNewIv.setVisibility(View.VISIBLE);
            viewHolder.mPageTv.setText(noteBookListData.getSubtitle());

        }else {
            viewHolder.mSubTitleTv.setText(noteBookListData.getSubtitle()+"最后编辑");
            viewHolder.mIsNewIv.setVisibility(View.GONE);
            viewHolder.mPageTv.setText("更新至第"+noteBookListData.getPage()+"页");
        }

        return convertView;
    }

    public void add(List<NoteBookListData> infos) {
        mNoteBookListData.addAll(infos);
    }
}
