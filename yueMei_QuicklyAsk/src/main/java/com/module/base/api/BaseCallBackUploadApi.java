package com.module.base.api;

import android.content.Context;

import com.lzy.okgo.model.HttpParams;

import java.util.Map;

/**
 * Created by 裴成浩 on 2017/11/22.
 */

public interface BaseCallBackUploadApi {
    void getCallBack(Context context, Map<String, Object> maps, HttpParams uploadPamas, BaseCallBackListener listener);
}
