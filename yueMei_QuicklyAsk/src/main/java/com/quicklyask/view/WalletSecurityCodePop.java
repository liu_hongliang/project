package com.quicklyask.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.util.Util;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.api.TiXianCodeApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;

public class WalletSecurityCodePop extends PopupWindow {

    private final Context mContext;
    private String TAG = "WalletSecurityCodePop";

    public WalletSecurityCodePop(final Context context) {
        this.mContext = context;
        final View view = View.inflate(context, R.layout.wallet_security_code, null);
        view.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_ins));
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();
        TextView tip = view.findViewById(R.id.wallet_pop_tip);
        Button cancelBt = view.findViewById(R.id.cancel_bt);
        Button tureBt = view.findViewById(R.id.zixun_bt);

        final EditText input = view.findViewById(R.id.no_pass_login_code_et);
        final EditText inputCode = view.findViewById(R.id.wallet_pop_edt);
        final ImageView imageView = view.findViewById(R.id.yuyin_code_iv);
        final TextView yanzhenCodeTv = view.findViewById(R.id.yanzheng_code_tv);
        RelativeLayout imgClick = view.findViewById(R.id.no_pass_yanzheng_code_rly);
        final RelativeLayout getCodeClick = view.findViewById(R.id.wallet_pop_getcode);
        String loginPhone = Utils.getLoginPhone();
        tip.setText(loginPhone + "的短信验证码");
        Glide.with(context).load("https://user.yuemei.com/user/getCaptcha/code/9/")
                .skipMemoryCache(true) // 不使用内存缓存
                .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                .into(imageView);

        //获取验证码
        getCodeClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                String inputContent = input.getText().toString().trim();
                if (!TextUtils.isEmpty(inputContent)) {
                    HashMap<String, Object> maps = new HashMap<>();
                    maps.put("code", inputContent);
                    Log.e(TAG, "inputContent == " + inputContent);
                    new TiXianCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData s) {
                            if ("1".equals(s.code)) {
                                ViewInject.toast(s.message);
                                getCodeClick.setClickable(false);
                                inputCode.requestFocus();
                                new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                                    @Override
                                    public void onTick(long millisUntilFinished) {
                                        getCodeClick.setBackgroundResource(R.drawable.biankuang_hui);
                                        yanzhenCodeTv.setTextColor(mContext.getResources().getColor(R.color.button_zi));
                                        yanzhenCodeTv.setText("(" + millisUntilFinished / 1000 + ")重新获取");
                                    }

                                    @Override
                                    public void onFinish() {
                                        getCodeClick.setBackgroundResource(R.drawable.shape_bian_ff5c77);
                                        yanzhenCodeTv.setTextColor(mContext.getResources().getColor(R.color.button_bian_hong1));
                                        getCodeClick.setClickable(true);
                                        yanzhenCodeTv.setText("重发验证码");

                                        if (Util.isOnMainThread()) {
                                            Glide.with(mContext.getApplicationContext()).load(FinalConstant.TUXINGCODE)
                                                    .skipMemoryCache(true) // 不使用内存缓存
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                                                    .into(imageView);
                                        }

                                        input.setText("");

                                    }
                                }.start();
                            } else {
                                MyToast.makeTextToast2(mContext, s.message, MyToast.SHOW_TIME).show();
                            }
                        }
                    });
                } else {
                    MyToast.makeTextToast2(mContext, "请输入图片中的数字", MyToast.SHOW_TIME).show();
                }
            }
        });

        //点击图片刷新
        imgClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glide.with(context).load(FinalConstant.TUXINGCODE)
                        .skipMemoryCache(true) // 不使用内存缓存
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                        .into(imageView);
            }
        });

        //确定按钮
        tureBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = inputCode.getText().toString().trim();
                if (onTrueClickListener != null) {
                    onTrueClickListener.onTrueClick(code);
                }
            }
        });

        //取消按钮
        cancelBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnTrueClickListener {
        void onTrueClick(String codes);
    }

    private OnTrueClickListener onTrueClickListener;

    public void setOnTureClickListener(OnTrueClickListener onTureClickListener) {
        this.onTrueClickListener = onTureClickListener;
    }
}
