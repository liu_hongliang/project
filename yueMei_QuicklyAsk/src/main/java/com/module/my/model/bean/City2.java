package com.module.my.model.bean;

import java.util.List;

public class City2 {
	// {
	// "code": "1",
	private String code;
	// "message": "返回成功",
	private String message;
	// "data": [
	private List<Province2ListData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Province2ListData> getData() {
		return data;
	}

	public void setData(List<Province2ListData> data) {
		this.data = data;
	}

}
