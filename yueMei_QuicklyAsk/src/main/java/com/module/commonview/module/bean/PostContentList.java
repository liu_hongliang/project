package com.module.commonview.module.bean;

import com.module.community.model.bean.BBsListData550;
import com.module.doctor.model.bean.DocListData;
import com.module.doctor.model.bean.HosListData;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/8/15.
 */
public class PostContentList{

    private PostContentText text;
    private PostContentImage image;
    private PostContentVideo video;                 //视频
    private List<DiaryTaoData> tao;                        //淘数据
    private List<BBsListData550> post;                    //插入帖子数据
    private List<DocListData> doctor;                     //医生数据
    private List<HosListData> hospital;                   //医院数据

    public PostContentText getText() {
        return text;
    }

    public void setText(PostContentText text) {
        this.text = text;
    }

    public PostContentImage getImage() {
        return image;
    }

    public void setImage(PostContentImage image) {
        this.image = image;
    }

    public PostContentVideo getVideo() {
        return video;
    }

    public void setVideo(PostContentVideo video) {
        this.video = video;
    }

    public List<DiaryTaoData> getTao() {
        return tao;
    }

    public void setTao(List<DiaryTaoData> tao) {
        this.tao = tao;
    }

    public List<BBsListData550> getPost() {
        return post;
    }

    public void setPost(List<BBsListData550> post) {
        this.post = post;
    }

    public List<DocListData> getDoctor() {
        return doctor;
    }

    public void setDoctor(List<DocListData> doctor) {
        this.doctor = doctor;
    }

    public List<HosListData> getHospital() {
        return hospital;
    }

    public void setHospital(List<HosListData> hospital) {
        this.hospital = hospital;
    }
}
