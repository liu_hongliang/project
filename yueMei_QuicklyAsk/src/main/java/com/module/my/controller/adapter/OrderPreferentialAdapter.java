package com.module.my.controller.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.my.view.view.RoundBackgroundColorSpan;
import com.module.shopping.model.bean.YouHuiCoupons;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;

/**
 * 下单优惠劵列表适配器
 * Created by 裴成浩 on 2018/3/21.
 */

public class OrderPreferentialAdapter extends BaseAdapter {
    private String TAG = "OrderPreferential";
    private Context mContext;
    private ArrayList<YouHuiCoupons> mYouhuiData;
    private YouHuiCoupons youHuiCoupons;
    private LayoutInflater mInflater;

    public OrderPreferentialAdapter(Context context, ArrayList<YouHuiCoupons> mYouhuiData, YouHuiCoupons youHuiCoupons) {
        this.mContext = context;
        this.mYouhuiData = mYouhuiData;
        this.youHuiCoupons = youHuiCoupons;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mYouhuiData.size();
    }

    @Override
    public Object getItem(int position) {
        return mYouhuiData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_order_preferential, null);
            viewHolder = new ViewHolder();

            viewHolder.couponsHigh =  convertView.findViewById(R.id.ll_coupons_high);
            viewHolder.money =  convertView.findViewById(R.id.tv_order_money);
            viewHolder.mankeyong =  convertView.findViewById(R.id.tv_order_mankeyong);
            viewHolder.couponsDivider =  convertView.findViewById(R.id.iv_coupons_high);
            viewHolder.couponsRight =  convertView.findViewById(R.id.ll_coupons_high_right);
            viewHolder.title =  convertView.findViewById(R.id.order_preferential_title);
            viewHolder.conditions =  convertView.findViewById(R.id.order_preferential_conditions);
            viewHolder.date =  convertView.findViewById(R.id.order_preferential_date);
            viewHolder.check =  convertView.findViewById(R.id.order_preferential_check);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        YouHuiCoupons mYouHui = mYouhuiData.get(position);

        //设置优惠标识
        setYouhuiFlag(viewHolder,mYouHui);

        viewHolder.money.setText(mYouHui.getMoney() + "");
        viewHolder.mankeyong.setText(mYouHui.getMankeyong());
        viewHolder.conditions.setText(mYouHui.getShiyongtiaojian());
        viewHolder.date.setText(mYouHui.getTime());

        if(mYouHui.getCard_id().equals(youHuiCoupons.getCard_id())){
            viewHolder.check.setBackground(ContextCompat.getDrawable(mContext,R.drawable.raido_dui3x));
        }else{
            viewHolder.check.setBackground(ContextCompat.getDrawable(mContext,R.drawable.raido_cuo3x));
        }

        int height1 = viewHolder.couponsHigh.getHeight();
        int height2 = viewHolder.couponsDivider.getHeight();
        int height3 = viewHolder.couponsRight.getHeight();

        int measuredHeight1 = viewHolder.couponsHigh.getMeasuredHeight();
        int measuredHeight2 = viewHolder.couponsDivider.getMeasuredHeight();
        int measuredHeight3 = viewHolder.couponsRight.getMeasuredHeight();

        Log.e(TAG, "height1 === " + height1);
        Log.e(TAG, "height2 === " + height2);
        Log.e(TAG, "height3 === " + height3);

        Log.e(TAG, "measuredHeight1 === " + measuredHeight1);
        Log.e(TAG, "measuredHeight2 === " + measuredHeight2);
        Log.e(TAG, "measuredHeight3 === " + measuredHeight3);

        return convertView;
    }



    /**
     * 设置优惠券前边的标识
     * @param viewHolder
     * @param mYouHui
     */
    private void setYouhuiFlag(ViewHolder viewHolder, YouHuiCoupons mYouHui) {

        String typeTitle = "";

       switch (mYouHui.getCouponsType()) {
           case "1" :
               typeTitle = "订金劵";
               break;
           case "2" :
               typeTitle = "尾款劵";
               break;
           case "3" :
               typeTitle = "尾款红包";
               break;
       }

        SpannableString spanString = new SpannableString(typeTitle + mYouHui.getTitle());
        RoundBackgroundColorSpan colorSpan = new RoundBackgroundColorSpan(Color.parseColor("#FF5c77"), Color.parseColor("#FFFFFF"), Utils.dip2px(3),Utils.dip2px(15));
        RelativeSizeSpan sizeSpan = new RelativeSizeSpan(0.76f);                                            //字体大小
        spanString.setSpan(colorSpan, 0, typeTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spanString.setSpan(sizeSpan, 0, typeTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        viewHolder.title.setText(spanString);

    }

    public void setYouHuiCoupons(YouHuiCoupons youHuiCoupons) {
        this.youHuiCoupons = youHuiCoupons;
    }

    static class ViewHolder {
        public LinearLayout couponsHigh;
        public TextView money;
        public TextView mankeyong;
        public ImageView couponsDivider;
        public LinearLayout couponsRight;
        public TextView title;
        public TextView conditions;
        public TextView date;
        public ImageView check;
    }
}
