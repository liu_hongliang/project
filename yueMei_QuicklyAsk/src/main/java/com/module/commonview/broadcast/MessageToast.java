package com.module.commonview.broadcast;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.MyApplication;
import com.module.commonview.activity.ChatActivity;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class MessageToast implements View.OnTouchListener {
    public static final String TAG = "MessageToast";
    private Activity mActivity;
    private static MessageToast mInstance;
    private Toast mToast;
    private final int SHOW = 1;
    private final int HIDE = 0;
    private Object mTN;
    private Method mShow;
    private Method mHide;
    private Field mViewFeild;
    private long durationTime = 5 * 1000;
    private View view;
    private final static int ANIM_DURATION = 600;
    private float downY;
    private float downX;
    private int mWindowsWight;

    public static MessageToast getInstance() {
        if (mInstance == null) {
            mInstance = new MessageToast();
        }
        return mInstance;
    }

    public void init(Activity activity) {
        mActivity = activity;
    }

    public void createToast(String title, String content, String hosimg, final String clientid, final String hosid, final String pos) {
        if (mActivity == null) {
            return;
        }
        //加載layout下的布局
        view = LayoutInflater.from(mActivity).inflate(R.layout.notifycation_message, null);
        view.setOnTouchListener(this);
        LinearLayout contaier = view.findViewById(R.id.toast_container);
        TextView tvTitle = view.findViewById(R.id.notify_title);
        TextView tvContent = view.findViewById(R.id.notify_content);
        ImageView tvImage = view.findViewById(R.id.notify_img);
        tvTitle.setText(title);
        tvContent.setText(content);
        Glide.with(MyApplication.getContext())
                .load(hosimg)
                .transform(new GlideCircleTransform(mActivity))
                .placeholder(R.color._f6)
                .error(R.color._f6).
                into(tvImage);
        mWindowsWight = Cfg.loadInt(mActivity, FinalConstant.WINDOWS_W, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((mWindowsWight-Utils.dip2px(20)), ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置TextView的宽度为 屏幕宽度
        contaier.setLayoutParams(layoutParams);
        mToast = new Toast(mActivity);
        mToast.setView(view);
        mToast.setDuration(Toast.LENGTH_LONG);
        mToast.setGravity(Gravity.TOP, 0, 0);

        reflectToast();
        reflectEnableClick();

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"onClick");
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("hos_id", hosid);
                hashMap.put("event_others", pos);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.PUSH, "click"), hashMap);
                handler.sendEmptyMessage(HIDE);
                hide();
                Intent intent = new Intent(mActivity, ChatActivity.class);
                intent.putExtra("directId", clientid);
                intent.putExtra("objId", "0");
                intent.putExtra("objType", "0");
                mActivity.startActivity(intent);
            }
        });
        if (mShow != null && mHide != null) {
            handler.sendEmptyMessage(SHOW);
        } else {
            mToast.show();
        }
    }

    private void reflectToast() {
        Field field = null;
        try {

            field = mToast.getClass().getDeclaredField("mTN");
            field.setAccessible(true);
            mTN = field.get(mToast);
            mShow = mTN.getClass().getDeclaredMethod("show");
            mHide = mTN.getClass().getDeclaredMethod("hide");
            mViewFeild = mTN.getClass().getDeclaredField("mNextView");
            mViewFeild.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e1) {
            e1.printStackTrace();
        }
    }
    private void animDismiss() {


        ObjectAnimator a = ObjectAnimator.ofFloat(view, "translationY", 0, -700);
        a.setDuration(ANIM_DURATION);
        a.start();

        a.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                hide();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 反射字段
     *
     * @param object    要反射的对象
     * @param fieldName 要反射的字段名称
     */
    private static Object getField(Object object, String fieldName)
            throws NoSuchFieldException, IllegalAccessException {
        Field field = object.getClass().getDeclaredField(fieldName);
        if (field != null) {
            field.setAccessible(true);
            return field.get(object);
        }
        return null;
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SHOW:
                    handler.sendEmptyMessageDelayed(HIDE, durationTime);
                    show();
                    break;
                case HIDE:
                    animDismiss();
                    break;
            }
        }
    };


    private void reflectEnableClick() {
        Log.e("MessageToast","reflectEnableClick ====");
        try {
            Object mTN;
            mTN = getField(mToast, "mTN");
            if (mTN != null) {
                Object mParams = getField(mTN, "mParams");
                if (mParams != null && mParams instanceof WindowManager.LayoutParams) {

                    WindowManager.LayoutParams params = (WindowManager.LayoutParams) mParams;
                    //显示与隐藏动画
//                    params.windowAnimations = R.style.ClickToast;
                    //Toast可点击
                    params.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                            | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

                    params.width = mWindowsWight - Utils.dip2px(20); //设置Toast宽度为屏幕宽度
                    params.height = WindowManager.LayoutParams.WRAP_CONTENT; //设置高度
                    Log.e("MessageToast","reflectEnableClick ====");

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"e ===="+e.toString());
        }
    }


    public void show() {
        try {
            //android4.0以上就要以下处理
            if (Build.VERSION.SDK_INT > 16) {
                Field mNextViewField = mTN.getClass().getDeclaredField("mNextView");
                mNextViewField.setAccessible(true);
                LayoutInflater inflate = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = mToast.getView();
                mNextViewField.set(mTN, v);
                Method method = mTN.getClass().getDeclaredMethod("show", (Class<?>) null);
                method.invoke(mTN, (Object) null);
            }
            mShow.invoke(mTN, (Object) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hide() {
        try {
            mHide.invoke(mTN, (Object) null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downY = event.getRawY();
                downX = event.getRawX();
                Log.e(TAG, "downY ==" + downY);

                break;
            case MotionEvent.ACTION_MOVE:
                float currentY = event.getRawY();
                if ((downY - currentY) >= 10) {
                    animDismiss();
                }
                break;
            case MotionEvent.ACTION_UP:
                //检测移动的距离，如果很微小可以认为是点击事件
                if (Math.abs(event.getRawX() - downX) < 10 && Math.abs(event.getRawY() - downY) < 10) {
                    try {
                        Field field = View.class.getDeclaredField("mListenerInfo");
                        field.setAccessible(true);
                        Object object = field.get(view);
                        field = object.getClass().getDeclaredField("mOnClickListener");
                        field.setAccessible(true);
                        object = field.get(object);
                        if (object != null && object instanceof View.OnClickListener) {
                            ((View.OnClickListener) object).onClick(view);
                        }
                    } catch (Exception e) {

                    }
                    return false;
                } else {
                    Log.e(TAG, "button已移动 ==");
                }
                break;

        }
        return true;
    }
}
