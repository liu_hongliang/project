package com.quicklyask.entity;


import com.module.doctor.model.bean.DocListData;

import java.util.List;

public class SearchResultData4 {
	private List<DocListData> list;

	public List<DocListData> getList() {
		return list;
	}

	public void setList(List<DocListData> list) {
		this.list = list;
	}

}
