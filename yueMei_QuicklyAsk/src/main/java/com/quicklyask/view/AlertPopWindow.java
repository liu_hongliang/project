package com.quicklyask.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.module.MainTableActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.community.model.bean.ZhuanTi;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.home.controller.activity.ZhuanTiWebActivity;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;

/**
 * Created by dwb on 16/7/1.
 */
public class AlertPopWindow extends PopupWindow {

    private ImageView alertIv;
    private RelativeLayout alerCloseRly;

    public AlertPopWindow(final Context mContext, final ZhuanTi alerZt) {
        final View view = View.inflate(mContext, R.layout.pop_adert, null);
        view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();



        alertIv = view.findViewById(R.id.pop_aler_iv);
        alerCloseRly = view.findViewById(R.id.pop_aler_close_rly);

        Glide.with(mContext).load(alerZt.getImg()).into(alertIv);


        alertIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> event_params = (HashMap<String, String>) Cfg.getHashMapData(mContext, "ad_event_params");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLEALERT, "yes"), event_params);
                String type = alerZt.getType();
                if ("1".equals(type)) {// 帖子
                    String id = alerZt.get_id();
                    String url = alerZt.getLink();

                    Intent it2 = new Intent();
                    it2.putExtra("url", url);
                    it2.putExtra("qid", id);
                    it2.setClass(mContext, DiariesAndPostsActivity.class);
                    mContext.startActivity(it2);
                } else if ("418".equals(type)) {// 淘整形
                    String id = alerZt.get_id();

                    Intent it1 = new Intent();
                    it1.putExtra("id", id);
                    it1.putExtra("source", "0");
                    it1.putExtra("objid", "0");
                    it1.setClass(mContext, TaoDetailActivity.class);
                    mContext.startActivity(it1);
                } else if ("999".equals(type)) {
                    String url = alerZt.getLink();
                    String shareTitle = alerZt.getTitle();
                    String sharePic = alerZt.getImg();

                    Intent it1 = new Intent();
                    it1.setClass(mContext, SlidePicTitieWebActivity.class);
                    it1.putExtra("shareTitle", shareTitle);
                    it1.putExtra("sharePic", sharePic);
                    it1.putExtra("url", url);
                    mContext.startActivity(it1);
                } else if ("1000".equals(type)) {// 专题
                    String url = alerZt.getLink();
                    String title = alerZt.getTitle();
                    String ztid = alerZt.get_id();

                    Intent it1 = new Intent();
                    it1.setClass(mContext, ZhuanTiWebActivity.class);
                    it1.putExtra("url", url);
                    it1.putExtra("title", title);
                    it1.putExtra("ztid", ztid);

                    mContext.startActivity(it1);
                } else if ("5701".equals(type)) {// 医生
                    String id = alerZt.get_id();
                    String docname = alerZt.getTitle();

                    Intent it = new Intent();
                    it.setClass(mContext, DoctorDetailsActivity592.class);
                    it.putExtra("docId", id);
                    it.putExtra("docName", docname);
                    it.putExtra("partId", "");
                    mContext.startActivity(it);
                } else if ("511".equals(type)) {// 医院
                    String hosid = alerZt.get_id();

                    Intent it = new Intent();
                    it.setClass(mContext, HosDetailActivity.class);
                    it.putExtra("hosid", hosid);
                    mContext.startActivity(it);
                } else {
                    if (!TextUtils.isEmpty(alerZt.getLink())) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(alerZt.getLink(), "0", "0");
                    } else {
                        MainTableActivity.tancengUrl = "";
                    }
                }

                // 保存当前时间
                int cur = (int) System.currentTimeMillis();
                Cfg.saveInt(mContext, "time_alert", cur);
                Cfg.saveStr(mContext, "alert_close", "1");
                dismiss();
            }
        });


        alerCloseRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                HashMap<String, String> event_params = (HashMap<String, String>) Cfg.getHashMapData(mContext, "ad_event_params");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLEALERT, "no"), event_params);
                MainTableActivity.tancengUrl = "";
                // 保存当前时间
                int cur = (int) System.currentTimeMillis();
                Cfg.saveInt(mContext, "time_alert", cur);
                Cfg.saveStr(mContext, "alert_close", "1");
                dismiss();

            }
        });
    }


    public void setPopListener(PopDissmissListener popDissmissListener) {
        mPopListener = popDissmissListener;
    }

    PopDissmissListener mPopListener;

    public interface PopDissmissListener{
        void onPopListener(AlertPopWindow alertPopWindow);
    }


}
