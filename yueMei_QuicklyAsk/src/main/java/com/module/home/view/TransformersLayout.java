package com.module.home.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;


/**
 * @Author: Zaaach
 * @Date: 2019/11/22
 * @Email: zaaach@aliyun.com
 * @Description:
 */
public class TransformersLayout<T> extends LinearLayout {
    private static final String TAG = TransformersLayout.class.getSimpleName();

    /** 默认每页5列 */
    private static final int DEFAULT_SPAN_COUNT = 5;
    /** 默认每页2行 */
    private static final int DEFAULT_LINES      = 2;
    /** 滚动条默认宽度 */
    private static final int DEFAULT_SCROLL_BAR_WIDTH  = 36;//dp
    /** 滚动条默认高度 */
    private static final int DEFAULT_SCROLL_BAR_HEIGHT = 3;//dp
    private static final int DEFAULT_TRACK_COLOR = Color.parseColor("#f0f0f0");
    private static final int DEFAULT_THUMB_COLOR = Color.parseColor("#ffc107");
    private int totalPage = 0; // 总页数
    private int currentPage = 1; // 当前页
    private float slideDistance = 0; // 滑动的距离
    private float scrollX = 0; // X轴当前的位置
    private int shortestDistance; // 超过此距离的滑动才有效
    private int spanCount;
    private int lines;
    private float scrollBarRadius;
    private int scrollBarTrackColor;
    private int scrollBarThumbColor;
    private int scrollBarTopMargin;
    private int scrollBarWidth;
    private int scrollBarHeight;
    /*
     * 0: 停止滚动且手指移开; 1: 开始滚动; 2: 手指做了抛的动作（手指离开屏幕前，用力滑了一下）
     */
    private int scrollState = 0; // 滚动状态
    private OnTransformersItemClickListener onTransformersItemClickListener;

    private RecyclerView recyclerView;
//    private RecyclerViewScrollBar scrollBar;
    private PageIndicatorView mIndicatorView;
    private OnTransformersScrollListener onScrollListener;

    private TransformersAdapter<T> transformersAdapter;
    private GridLayoutManager layoutManager;
    private Parcelable savedState;//保存的滚动状态
//    private boolean attached;

    public TransformersLayout(Context context) {
        this(context, null);
    }

    public TransformersLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TransformersLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseAttrs(context, attrs);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TransformersLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        parseAttrs(context, attrs);
        init(context);
    }

    private void parseAttrs(Context context, AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TransformersLayout);
        spanCount = array.getInteger(R.styleable.TransformersLayout_tl_spanCount, DEFAULT_SPAN_COUNT);
        lines = array.getInteger(R.styleable.TransformersLayout_tl_lines, DEFAULT_LINES);
        scrollBarRadius = array.getDimensionPixelSize(R.styleable.TransformersLayout_tl_scrollbarRadius, -1);
        scrollBarTrackColor = array.getColor(R.styleable.TransformersLayout_tl_scrollbarTrackColor, DEFAULT_TRACK_COLOR);
        scrollBarThumbColor = array.getColor(R.styleable.TransformersLayout_tl_scrollbarThumbColor, DEFAULT_THUMB_COLOR);
        scrollBarTopMargin = array.getDimensionPixelSize(R.styleable.TransformersLayout_tl_scrollbarMarginTop, 0);
        scrollBarWidth = array.getDimensionPixelSize(R.styleable.TransformersLayout_tl_scrollbarWidth, dp2px(DEFAULT_SCROLL_BAR_WIDTH));
        scrollBarHeight = array.getDimensionPixelSize(R.styleable.TransformersLayout_tl_scrollbarHeight, dp2px(DEFAULT_SCROLL_BAR_HEIGHT));
        array.recycle();

        if (scrollBarRadius < 0){
            scrollBarRadius = dp2px(DEFAULT_SCROLL_BAR_HEIGHT) / 2f;
        }
        if (spanCount <= 0){
            spanCount = DEFAULT_SPAN_COUNT;
        }
        if (lines <= 0){
            lines = DEFAULT_LINES;
        }
    }

    private void init(final Context context) {
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);
        setLayoutParams(new LayoutParams(-1, -2));
        recyclerView = new RecyclerView(context);
        recyclerView.setLayoutParams(new LayoutParams(-1, -2));
        recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int state) {
                switch (state) {
                    case 2:
                        scrollState = 2;
                        break;
                    case 1:
                        scrollState = 1;
                        break;
                    case 0:
                        if (slideDistance == 0) {
                            break;
                        }
                        scrollState = 0;
                        if (slideDistance < 0) { // 上页
                            currentPage = (int) Math.ceil(scrollX / getWidth());
                            Log.e("TransformersLayout","上页 -----"+currentPage);
                            if (currentPage * getWidth() - scrollX < shortestDistance) {
                                currentPage += 1;
                                Log.e("TransformersLayout","上页 ====="+currentPage);
                            }
                            if (scrollX == 0.0f){
                                currentPage =1;
                            }
                            Log.e("TransformersLayout","上页 &&&&&&&"+currentPage);
                        } else { // 下页
                            currentPage = (int) Math.ceil(scrollX / getWidth()) + 1;
                            Log.e("TransformersLayout","下页 -----"+currentPage);

                            if (currentPage <= totalPage) {
                                if (scrollX - (currentPage - 2) * getWidth() < shortestDistance) {
                                    // 如果这一页滑出距离不足，则定位到前一页
                                    currentPage -= 1;
                                    Log.e("TransformersLayout","下页 ====="+currentPage);
                                }
                            } else {
                                currentPage = totalPage;
                            }
                            Log.e("TransformersLayout","下页 &&&&&&&"+currentPage);
                        }
                        // 执行自动滚动
                        recyclerView.smoothScrollBy((int) ((currentPage - 1) * getWidth() - scrollX), 0);
                        // 修改指示器选中项
                        mIndicatorView.setSelectedPage(currentPage - 1);
                        slideDistance = 0;
                        break;
                }
                super.onScrollStateChanged(recyclerView, state);
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                scrollX += dx;
                if (scrollState == 1) {
                    slideDistance += dx;
                }
                super.onScrolled(recyclerView, dx, dy);
            }

        });

        RecyclerView.ItemAnimator itemAnimator = recyclerView.getItemAnimator();
        if (itemAnimator != null){
            itemAnimator.setChangeDuration(0);
        }

        setupRecyclerView();
        transformersAdapter = new TransformersAdapter<>(context, recyclerView);
        recyclerView.setAdapter(transformersAdapter);
//        scrollBar = new RecyclerViewScrollBar(context);
        mIndicatorView = new PageIndicatorView(context);
//        setupScrollBar();
        ViewGroup.MarginLayoutParams layoutParams = (MarginLayoutParams) recyclerView.getLayoutParams();
        layoutParams.leftMargin = Utils.dip2px(5);
        layoutParams.rightMargin = Utils.dip2px(5);
        recyclerView.setLayoutParams(layoutParams);
        addView(recyclerView);
//        addView(scrollBar);
        addView(mIndicatorView);

    }

    private void setupRecyclerView() {
        layoutManager = new GridLayoutManager(getContext(), lines, GridLayoutManager.HORIZONTAL, false){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView.setLayoutManager(layoutManager);
    }

//    private void setupScrollBar() {
//        LayoutParams params = new LayoutParams(scrollBarWidth, scrollBarHeight);
//        params.topMargin = scrollBarTopMargin;
//        scrollBar.setLayoutParams(params);
//        scrollBar.setTrackColor(scrollBarTrackColor)
//                .setThumbColor(scrollBarThumbColor)
//                .setRadius(scrollBarRadius)
//                .applyChange();
//    }

    public TransformersLayout<T> addOnTransformersItemClickListener(OnTransformersItemClickListener listener){
        this.onTransformersItemClickListener = listener;
        return this;
    }

    public void load(List<T> data, TransformersHolderCreator<T> creator){
        transformersAdapter.setOnTransformersItemClickListener(onTransformersItemClickListener);
        transformersAdapter.setHolderCreator(creator);
        transformersAdapter.setSpanCount(spanCount);
        transformersAdapter.setData(data);
        update();
//        toggleScrollBar(data);
//        if (scrollBar.getVisibility() == VISIBLE) {
//            scrollBar.attachRecyclerView(recyclerView);
//        }
    }

    public TransformersLayout<T> apply(@Nullable TransformersOptions options){
        if (options != null){
            spanCount = options.spanCount <= 0 ? spanCount : options.spanCount;
            int newLines = options.lines <= 0 ? lines : options.lines;
            scrollBarWidth = options.scrollBarWidth <= 0 ? scrollBarWidth : options.scrollBarWidth;
            scrollBarHeight = options.scrollBarHeight <= 0 ? scrollBarHeight : options.scrollBarHeight;
            scrollBarRadius = options.scrollBarRadius < 0 ? scrollBarHeight/2f : options.scrollBarRadius;
            scrollBarTopMargin = options.scrollBarTopMargin <= 0 ? scrollBarTopMargin : options.scrollBarTopMargin;

//            Log.e(TAG, "trackColor = " + options.scrollBarTrackColor);
//            Log.e(TAG, "thumbColor = " + options.scrollBarThumbColor);
//            Log.e(TAG, "radius = " + options.scrollBarRadius);
            scrollBarTrackColor = options.scrollBarTrackColor == 0 ? scrollBarTrackColor : options.scrollBarTrackColor;
            scrollBarThumbColor = options.scrollBarThumbColor == 0 ? scrollBarThumbColor : options.scrollBarThumbColor;

            if (newLines != lines){
                layoutManager.setSpanCount(newLines);
            }
//            setupScrollBar();
        }
        return this;
    }

    /**
     * 恢复滚动状态
     */
    @Override
    protected void onAttachedToWindow() {
//        Log.e(TAG, "----------onAttachedToWindow()");
        super.onAttachedToWindow();
//        attached = true;
        if (savedState != null) {
            layoutManager.onRestoreInstanceState(savedState);
        }
        savedState = null;
    }

    /**
     * 保存滚动状态
     */
    @Override
    protected void onDetachedFromWindow() {
//        Log.e(TAG, "----------onDetachedFromWindow()");
        super.onDetachedFromWindow();
//        attached = false;
        savedState = layoutManager.onSaveInstanceState();
    }

    public void notifyDataChanged(List<T> data){
//        if (transformersAdapter != null){
//            transformersAdapter.setData(data);
//            scrollToStart();
//        }
//        toggleScrollBar(data);
//        //数据发生改变时重新计算滚动比例
//        if (scrollBar.getVisibility() == VISIBLE) {
//            scrollBar.computeScrollScale();
//        }
    }

//    public void scrollToStart(){
//        scrollToStart(true);
//    }

//    public void scrollToStart(boolean smooth){
//        scrollBar.setScrollBySelf(true);
//        if (recyclerView != null) {
//            if (recyclerView.computeHorizontalScrollOffset() > 0) {
////                Log.e(TAG, "----------scrollToStart()");
//                if (smooth) {
//                    recyclerView.smoothScrollToPosition(0);
//                } else {
//                    recyclerView.scrollToPosition(0);
//                }
//            }
//        }
//    }

//    public TransformersLayout<T> addOnTransformersScrollListener(OnTransformersScrollListener listener){
//        this.onScrollListener = listener;
//        if (scrollBar != null) {
//            scrollBar.setOnTransformersScrollListener(onScrollListener);
//        }
//        return this;
//    }

    /**
     * 不足一页时隐藏滚动条
     */
//    private void toggleScrollBar(List<T> data) {
//        if (spanCount * lines >= data.size()){
//            scrollBar.setVisibility(GONE);
//        }else {
//            scrollBar.setVisibility(VISIBLE);
//        }
//    }

    private int dp2px(float dp){
        return (int) (getContext().getResources().getDisplayMetrics().density * dp + 0.5f);
    }


    // 更新页码指示器和相关数据
    private void update() {
        // 计算总页数
        int temp = ((int) Math.ceil(transformersAdapter.getItemCount() / (double) (DEFAULT_LINES * DEFAULT_SPAN_COUNT)));
        if (temp != totalPage) {
            mIndicatorView.initIndicator(temp);
            // 页码减少且当前页为最后一页
            if (temp < totalPage && currentPage == totalPage) {
                currentPage = temp;
                // 执行滚动
                recyclerView.smoothScrollBy(-getWidth(), 0);
            }
            mIndicatorView.setSelectedPage(currentPage - 1);
            totalPage = temp;
        }

        // 当页面为1时不显示指示器
        if (totalPage > 1 ){
            mIndicatorView.setVisibility(VISIBLE);
        }else {
            mIndicatorView.setVisibility(GONE);
        }
    }
}
