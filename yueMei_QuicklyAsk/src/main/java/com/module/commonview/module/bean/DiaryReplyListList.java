package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/7.
 */
public class DiaryReplyListList implements Parcelable {
    private DiaryReplyListUserdata userdata;
    private String content;
    private String id;
    private DiaryReplyLisListTao tao;

    public DiaryReplyListList() {
    }

    protected DiaryReplyListList(Parcel in) {
        userdata = in.readParcelable(DiaryReplyListUserdata.class.getClassLoader());
        content = in.readString();
        id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(userdata, flags);
        dest.writeString(content);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryReplyListList> CREATOR = new Creator<DiaryReplyListList>() {
        @Override
        public DiaryReplyListList createFromParcel(Parcel in) {
            return new DiaryReplyListList(in);
        }

        @Override
        public DiaryReplyListList[] newArray(int size) {
            return new DiaryReplyListList[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DiaryReplyListUserdata getUserdata() {
        return userdata;
    }

    public void setUserdata(DiaryReplyListUserdata userdata) {
        this.userdata = userdata;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public DiaryReplyLisListTao getTao() {
        return tao;
    }

    public void setTao(DiaryReplyLisListTao tao) {
        this.tao = tao;
    }
}
