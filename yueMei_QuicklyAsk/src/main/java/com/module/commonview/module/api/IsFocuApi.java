package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.bean.IsFocuData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * 判断是否关注
 * Created by 裴成浩 on 2018/4/18.
 */

public class IsFocuApi implements BaseCallBackApi {
    private String TAG = "IsFocuApi";

//    https://sjapp.yuemei.com/V641/usernew/isfollowing/market/APP_STORE/uid/71661/objid/3345/type/3/act/houfei/device/test/appkey/test/city/%E5%8C%97%E4%BA%AC/imei/111/

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "isfollowing", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData === " + mData.toString());
                try {
                    IsFocuData isFocuData = JSONUtil.TransformSingleBean(mData.data, IsFocuData.class);
                    listener.onSuccess(isFocuData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
