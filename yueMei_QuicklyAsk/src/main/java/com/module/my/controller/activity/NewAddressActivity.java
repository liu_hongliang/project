package com.module.my.controller.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.base.view.ui.YMEditText;
import com.module.commonview.view.CommonTopBar;
import com.module.my.model.api.DeladdressApi;
import com.module.my.model.api.GetaddressApi;
import com.module.my.model.api.InitCityDataApi;
import com.module.my.model.api.SaveaddressApi;
import com.module.my.model.bean.City2List;
import com.module.my.model.bean.GetaddressData;
import com.module.my.model.bean.Province2ListData;
import com.module.my.model.bean.SaveaddressData;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyask.activity.R;
import com.quicklyask.entity.ProvinceBean;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.YueMeiDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 新增地址页面
 *
 * @author 裴成浩
 */
public class NewAddressActivity extends YMBaseActivity {

    @BindView(R.id.new_addresss_top)
    CommonTopBar mTop;                                      //标题
    @BindView(R.id.new_addresss_prompt)
    TextView mPrompt;                                      //提示

    @BindView(R.id.new_addresss_contact)
    YMEditText mContact;                                  //联系人
    @BindView(R.id.new_addresss_mobile_phone)
    YMEditText mMobilePhone;                              //手机号
    @BindView(R.id.new_addresss_city_selection)
    TextView mCitySelection;                              //地区
    @BindView(R.id.new_addresss_city_choose)
    ImageView mCityChoose;                                //地区选择按钮
    @BindView(R.id.new_addresss_address_choose)
    YMEditText mAddressChoose;                            //详细地址

    @BindView(R.id.new_addresss_delete_addresss_click)
    LinearLayout mDeleteAddresss;                           //删除地址点击

    @BindView(R.id.new_addresss_submit)
    Button mSubmit;                                         //提交按钮

    private String TAG = "NewAddressActivity";

    private OptionsPickerView mCityOptions;            //地区选择

    ArrayList optionsCityItems = new ArrayList();
    ArrayList<ArrayList<String>> optionsCityItems2 = new ArrayList<>();
    private List<Province2ListData> mCityDatas = new ArrayList<>();

    private final String PROMPT_TEXT1 = "我们将按照以下收货人信息发送奖品，请认真填写";
    private final String PROMPT_TEXT2 = "若地址有改动请拨打客服电话 ";
    private String mFlag = "add";              //add：新增地址，edit：编辑地址，look：查看地址

    private SaveaddressApi saveaddressApi;          //保存地址
    private GetaddressApi getaddressApi;            //获取地址
    private DeladdressApi deladdressApi;            //删除地址
    private String addressId;                       //地址id
    private String provinceId;                         //省id
    private String cityId;                             //市id
    private YueMeiDialog yueMeiDialog;
    private YueMeiDialog yueMeiDialog1;
    private String mTel;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_new_address;
    }

    @Override
    protected void initView() {
        mFlag = getIntent().getStringExtra("flag");
        switch (mFlag) {
            case "add":
                mTop.setCenterText("新增地址");
                mPrompt.setText(PROMPT_TEXT1);

                mDeleteAddresss.setVisibility(View.GONE);

                mSubmit.setVisibility(View.VISIBLE);
                mCityChoose.setVisibility(View.VISIBLE);
                break;
            case "edit":
                addressId = getIntent().getStringExtra("address_id");
                mTop.setCenterText("编辑地址");
                mPrompt.setText(PROMPT_TEXT1);

                mDeleteAddresss.setVisibility(View.VISIBLE);

                mSubmit.setVisibility(View.VISIBLE);
                mCityChoose.setVisibility(View.VISIBLE);
                break;
            case "look":
                addressId = getIntent().getStringExtra("address_id");
                mTop.setCenterText("查看地址");

                mDeleteAddresss.setVisibility(View.GONE);                           //删除按钮

                mSubmit.setVisibility(View.GONE);                                   //保存按钮显示和隐藏
                mCityChoose.setVisibility(View.GONE);                               //选择地区按钮
                break;
        }

        Log.e(TAG, "mFlag === " + mFlag);
        Log.e(TAG, "1111111 -----> addressId === " + addressId);

        mContact.setEditTextState(false);
        mMobilePhone.setEditTextState(false);
        mAddressChoose.setEditTextState(false);

        //联系人
        mContact.setClickCallBack(new YMEditText.ClickCallBack() {
            @Override
            public void onContentEditorClick(View v) {
                if (uneditableText()) {
                    mContact.setEditTextState(true);
                }
            }

            @Override
            public void onReleaseClick(int length) {
                submitIsClickable();
            }
        });

        //手机号
        mMobilePhone.setClickCallBack(new YMEditText.ClickCallBack() {
            @Override
            public void onContentEditorClick(View v) {
                if (uneditableText()) {
                    mMobilePhone.setEditTextState(true);
                }
            }

            @Override
            public void onReleaseClick(int length) {
                submitIsClickable();
            }
        });

        //详细地址
        mAddressChoose.setClickCallBack(new YMEditText.ClickCallBack() {
            @Override
            public void onContentEditorClick(View v) {
                if (uneditableText()) {
                    mAddressChoose.setEditTextState(true);
                }
            }

            @Override
            public void onReleaseClick(int length) {
                submitIsClickable();
            }
        });

    }

    @Override
    protected void initData() {
        yueMeiDialog = new YueMeiDialog(this, "确定要删除吗？", "确定", "取消");
        yueMeiDialog.setCanceledOnTouchOutside(false);
        yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
            @Override
            public void leftBtnClick() {
                yueMeiDialog.dismiss();
                //如果城市加载完毕了
                if (mCityDatas.size() > 0) {
                    deladdressApi.addData("address_id", addressId);
                    deladdressApi.getCallBack(mContext, deladdressApi.getDeladdressHashMap(), new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData data) {
                            mFunctionManager.showShort(data.message);
                            onBackPressed();
                        }
                    });
                }
            }

            @Override
            public void rightBtnClick() {
                yueMeiDialog.dismiss();
            }
        });

        yueMeiDialog1 = new YueMeiDialog(mContext);
        yueMeiDialog1.setmLeftMsg("拨打");
        yueMeiDialog1.setmRightMsg("取消");
        yueMeiDialog1.setCanceledOnTouchOutside(false);
        yueMeiDialog1.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
            @Override
            public void leftBtnClick() {
                yueMeiDialog1.dismiss();
                mFunctionManager.showShort("正在拨打中·····");
                //版本判断
                if (Build.VERSION.SDK_INT >= 23) {

                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CALL_PHONE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {        //判断权限是否开启
                            phoneCall();
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                            mFunctionManager.showShort("没有电话权限");
                        }
                    });
                } else {
                    phoneCall();
                }
            }

            @Override
            public void rightBtnClick() {
                yueMeiDialog1.dismiss();
            }
        });

        initCityData();
    }

    /**
     * 删除地址按钮点击
     */
    @OnClick(R.id.new_addresss_delete_addresss_click)
    public void onAddAddresssClicked() {
        yueMeiDialog.show();
    }

    /**
     * 城市选择按钮点击
     */
    @OnClick(R.id.new_addresss_city_selection_click)
    public void onCitySelectionClicked() {

        if (mCityOptions != null && uneditableText()) {

            Utils.hideSoftKeyboard(mContext);

            mCityOptions.show();
        }
    }

    /**
     * 提交按钮点击
     */
    @OnClick(R.id.new_addresss_submit)
    public void onSubmitClicked() {
        //如果城市加载完毕了
        if (mCityDatas.size() > 0) {
            saveaddressApi.addData("address_id", TextUtils.isEmpty(addressId) ? "0" : addressId);
            saveaddressApi.addData("name", mContact.getContent());           //联系人
            Log.e(TAG, "provinceId === " + provinceId);
            Log.e(TAG, "cityId === " + cityId);
            saveaddressApi.addData("province_id", provinceId + "");             //省id
            saveaddressApi.addData("city_id", cityId + "");                     //市id
            saveaddressApi.addData("address", mAddressChoose.getContent());     //详细地址
            saveaddressApi.addData("phone", mMobilePhone.getContent());         //手机号
            saveaddressApi.getCallBack(mContext, saveaddressApi.getSaveaddressHashMap(), new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData data) {
                    try {
                        mFunctionManager.showShort(data.message);
                        if ("1".equals(data.code)) {
                            SaveaddressData saveaddressData = JSONUtil.TransformSingleBean(data.data, SaveaddressData.class);
                            String addressId = saveaddressData.getAddress_id();
                            Log.e(TAG, "addressId === " + addressId);
                            onBackPressed();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * 初始化城市列表
     */
    private void initCityData() {
        new InitCityDataApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mCityDatas = JSONUtil.TransformCity2(serverData.data);

                mCityOptions = new OptionsPickerView(mContext);

                int it1 = 0;
                int it2 = 0;

                for (int i = 0; i < mCityDatas.size(); i++) {
                    optionsCityItems.add(new ProvinceBean(i, mCityDatas.get(i).getName(), "", ""));
                    ArrayList<String> options2Items_00 = new ArrayList<>();
                    for (int j = 0; j < mCityDatas.get(i).getList().size(); j++) {
                        options2Items_00.add(mCityDatas.get(i).getList().get(j).getName());
                    }
                    optionsCityItems2.add(options2Items_00);
                }

                mCityOptions.setPicker(optionsCityItems, optionsCityItems2, true);
                mCityOptions.setTitle("选择城市");
                mCityOptions.setCyclic(false, false, true);

                mCityOptions.setSelectOptions(it1, it2, 1);
                mCityOptions.setCancelable(true);
                mCityOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onOptionsSelect(int options1, int option2, int options3) {

                        String name1 = mCityDatas.get(options1).getName();
                        String name2 = mCityDatas.get(options1).getList().get(option2).getName();

                        provinceId = mCityDatas.get(options1).get_id();
                        cityId = mCityDatas.get(options1).getList().get(option2).get_id();

                        mCitySelection.setText(name1 + name2);

                        submitIsClickable();
                    }
                });

                //地址操作设置
                switch (mFlag) {
                    case "add":
                        saveaddressApi = new SaveaddressApi();
                        break;
                    case "edit":
                        saveaddressApi = new SaveaddressApi();
                        getaddressApi = new GetaddressApi();
                        deladdressApi = new DeladdressApi();

                        getaddressData();
                        break;
                    case "look":
                        getaddressApi = new GetaddressApi();
                        getaddressData();
                        break;
                }
            }
        });
    }

    /**
     * 获取地址信息
     */
    private void getaddressData() {
        getaddressApi.addData("address_id", addressId);
        getaddressApi.getCallBack(mContext, getaddressApi.getGetaddressHashMap(), new BaseCallBackListener<GetaddressData>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(GetaddressData data) {
                mTel = data.getTel();
                setPrompt();

                mContact.setText(data.getName());                       //联系人
                mMobilePhone.setText(data.getPhone());                  //手机号
                mAddressChoose.setText(data.getAddress());              //详细地址

                //地区
                provinceId = data.getProvince();
                cityId = data.getCity();
                Log.e(TAG, "provinceId == " + provinceId);
                Log.e(TAG, "cityId == " + cityId);
                mCitySelection.setText(getRegionName(provinceId, cityId));
            }
        });
    }

    /**
     * 提交是否可以点击
     */
    private void submitIsClickable() {
        if (mContact.getLength() > 0 && mMobilePhone.getLength() > 0 && mAddressChoose.getLength() > 0 && getCityContent().length() > 0) {
            mSubmit.setEnabled(true);
            mSubmit.setBackgroundResource(R.drawable.renzheng_background);
        } else {
            mSubmit.setEnabled(false);
            mSubmit.setBackgroundResource(R.drawable.shoukuan_save_background);
        }
    }

    /**
     * 获取城市内容
     *
     * @return
     */
    private String getCityContent() {
        return mCitySelection.getText().toString().trim();
    }

    /**
     * 文本框不可编辑状态
     *
     * @return true：文本框可编辑，false；文本框不可编辑
     */
    private boolean uneditableText() {
        switch (mFlag) {
            case "add":
                return true;
            case "edit":
                return true;
            case "look":
                return false;
            default:
                return true;
        }
    }

    private String getRegionName(String provinceId, String cityId) {

        String results = "";
        for (int i = 0; i < mCityDatas.size(); i++) {
            Province2ListData data = mCityDatas.get(i);
            if (provinceId.equals(data.get_id())) {
                results = results + data.getName();
                List<City2List> list = data.getList();
                for (int j = 0; j < list.size(); j++) {
                    City2List city2List = list.get(j);
                    if (cityId.equals(city2List.get_id())) {
                        results = results + city2List.getName();
                    }
                }
                break;
            }
        }
        return results;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(101, intent);
        super.onBackPressed();
    }


    /**
     * 设置电话富文本
     */
    private void setPrompt() {
        yueMeiDialog1.setmMessage("拨打" + mTel + "？");

        SpannableString spannableString = new SpannableString(PROMPT_TEXT2 + mTel);

        //这个一定要记得设置，不然点击不生效
        mPrompt.setMovementMethod(LinkMovementMethod.getInstance());
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                yueMeiDialog1.show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Utils.getLocalColor(mContext, R.color.light_blue));
                ds.setUnderlineText(false); //是否设置下划线
            }
        }, PROMPT_TEXT2.length(), spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        //只有是查看地址时才显示
        switch (mFlag) {
            case "look":
                mPrompt.setText(spannableString);          //提示文案
                break;
        }

    }

    /**
     * 打电话
     */
    @SuppressLint("MissingPermission")
    private void phoneCall() {
        mFunctionManager.showShort("正在拨打中·····");
        Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mTel));
        startActivity(it);
    }
}
