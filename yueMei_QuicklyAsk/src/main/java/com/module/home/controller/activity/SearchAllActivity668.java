package com.module.home.controller.activity;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.community.model.bean.HistorySearchWords;
import com.module.community.model.bean.SearchAboutData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.SearchApi;
import com.module.home.controller.adapter.SearchKeywordsAdapter;
import com.module.home.fragment.SearchHospitalResultsFragment;
import com.module.home.fragment.SearchInitFragment;
import com.module.home.fragment.SearchResultsFragment;
import com.module.home.model.bean.HotWordsData;
import com.module.home.model.bean.SearchEntry;
import com.module.home.view.SearchTitleView;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import org.kymjs.kjframe.KJDB;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

public class SearchAllActivity668 extends YMBaseActivity {

    @BindView(R.id.activity_search_top)
    SearchTitleView mTop;
    @BindView(R.id.activity_search_fragment)
    FrameLayout mFragment;
    @BindView(R.id.search_keywords_recycler)
    public RecyclerView mRecycler;

    @BindView(R.id.community_coordinator_latoyt)
    CoordinatorLayout mCoordinatorLatoyt;
    @BindView(R.id.community_appbar_latoyt)
    AppBarLayout mAppbarLatoyt;
    private String TAG = "SearchAllActivity668";
    private SearchInitFragment searchInitFragment;                          //初始化页面
    private SearchApi mSearchApi;                                           //搜索联想词
    private SearchKeywordsAdapter mKeyAdater;
    private KJDB mKjdb;
    private String mKeys;
    private String mType;
    private String hospital_id;
    private int mPosition;
    private boolean isResults;

    //传参key值
    public static final String KEYS = "keys";
    public static final String TYPE = "type";
    public static final String HOSPITAL_ID = "hospital_id";
    public static final String POSITION = "position";
    public static final String RESULTS = "results";
    public static final String TARGET = "target";
    private SearchEntry mSearchEntry;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_search_all668;
    }

    @Override
    protected void initView() {
        String key = getIntent().getStringExtra(KEYS);
        Log.e(TAG,"key ===="+key);
        if (!TextUtils.isEmpty(key)) {
            mKeys = Utils.unicodeDecode(key);
        } else {
            mKeys = "";
        }
        mType = getIntent().getStringExtra(TYPE);
        hospital_id = getIntent().getStringExtra(HOSPITAL_ID);
        mPosition = getIntent().getIntExtra(POSITION, 0);
        isResults = getIntent().getBooleanExtra(RESULTS, false);
        mSearchEntry = getIntent().getParcelableExtra(TARGET);
        if (mSearchEntry != null){
            HashMap<String, String> eventParams = mSearchEntry.getEvent_params();
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_HOT_WORD_EXPOSURE),eventParams,new ActivityTypeData("73"));
        }
        //如果id是0，改为""
        if ("0".equals(hospital_id)) {
            hospital_id = "";
        }
        Log.e(TAG, "mKeys == " + mKeys);
        Log.e(TAG, "mType == " + mType);
        Log.e(TAG, "hospital_id == " + hospital_id);
        Log.e(TAG, "mPosition == " + mPosition);
        Log.e(TAG, "isResults == " + isResults);

        //设置背景联动
        mAppbarLatoyt.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

            }
        });
    }

    @Override
    protected void initData() {
        mSearchApi = new SearchApi();

        mKjdb = KJDB.create(mContext, "yuemeiwords");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(linearLayoutManager);

        //直接展示到结果页
        if (isResults) {
            mTop.setContent(mKeys);
            setResultsFragment(mKeys, mPosition);
        } else {
            if (!TextUtils.isEmpty(mKeys)) {
                mTop.setHint(mKeys);
            }
            Log.e(TAG,"键盘弹窗来了哈哈哈哈");
//            Utils.showSoftInputFromWindow(mContext,mTop.getEditText());
            setInitFragment();
        }

        //头部回调
        mTop.setOnEventClickListener(new SearchTitleView.OnEventClickListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus, String key) {
                if (hasFocus && !TextUtils.isEmpty(key)) {
                    //如果存在联想词：显示联想词页面
                    loadKeywordsData(key);
                } else {
                    mRecycler.setVisibility(View.GONE);
                }
            }

            @Override
            public void onKeywordsClick(String key) {
                if (!TextUtils.isEmpty(key)) {
                    loadKeywordsData(key);
                } else {
                    mRecycler.setVisibility(View.GONE);
                    if (searchInitFragment == null) {
                        setInitFragment();
                    } else {
                        setActivityFragment(R.id.activity_search_fragment, searchInitFragment);
                    }
                }
            }

            @Override
            public void onSearchClick(View v, String text) {
                if (mSearchEntry != null){
                    HashMap<String, String> eventParams = mSearchEntry.getEvent_params();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_HOT_WORD_TO),eventParams,new ActivityTypeData("73"));
                    String keyType = mSearchEntry.getKey_type();
                    if ("2".equals(keyType)){
                        String link = mSearchEntry.getLink();
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(link);
                    }else {
                        setResultsFragment(text);
                    }
                }else {
                    if (!TextUtils.isEmpty(text)){
                        setResultsFragment(text);
                    }
                }

                //隐藏软键盘
                Utils.hideSoftKeyboard(mContext);
            }

            @Override
            public void onCancelClick(View v) {
                onBackPressed();
            }
        });
    }

    /**
     * 设置初始化布局
     */
    private void setInitFragment() {
        //默认布局
        searchInitFragment = SearchInitFragment.newInstance(mType, hospital_id);
        setActivityFragment(R.id.activity_search_fragment, searchInitFragment);
        //初始化页面点击回调
        searchInitFragment.setOnEventClickListener(new SearchInitFragment.OnEventClickListener() {

            /**
             * 搜索历史点击
             * @param v
             * @param key
             */
            @Override
            public void onHistoryClick(View v, String key) {
                mTop.setContent(key);
                setResultsFragment(key);
            }

            /**
             * 热门搜索点击
             * @param v
             * @param key
             */
            @Override
            public void onHotClick(View v, String key) {
                mTop.setContent(key);
                setResultsFragment(key);
            }

        });
    }

    /**
     * 获取联想关键词
     */
    private void loadKeywordsData(final String key) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("key", Utils.unicodeEncode(key));

        mSearchApi.getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                ArrayList<SearchAboutData> searchAboutDatas = JSONUtil.TransformSearchAboutData(serverData.data);

                //是否有联想词
                if (searchAboutDatas != null && searchAboutDatas.size() > 0 && !TextUtils.isEmpty(mTop.getEditStr())) {
                    //如果存在联想词：显示联想词页面
                    mRecycler.setVisibility(View.VISIBLE);

                    if (mKeyAdater == null) {
                        mKeyAdater = new SearchKeywordsAdapter(mContext, searchAboutDatas);
                        mRecycler.setAdapter(mKeyAdater);
                        mKeyAdater.setOnEventClickListener(new SearchKeywordsAdapter.OnEventClickListener() {
                            @Override
                            public void onItemViewClick(View v, String keys, HashMap<String, String> event_params) {
                                Log.e(TAG, "keys == " + keys);
                                //隐藏软键盘
                                Utils.hideSoftKeyboard(mContext);
                                YmStatistics.getInstance().tongjiApp(event_params);
                                mTop.setContent(keys);
                                setResultsFragment(keys);

                            }
                        });
                    } else {
                        mKeyAdater.replaceData(searchAboutDatas);
                    }

                } else {
                    //如果不存在联想词：显示初始化页面
                    mRecycler.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 设置结果页面
     *
     * @param key
     */
    private void setResultsFragment(String key) {
        setResultsFragment(key, 1);
    }

    /**
     * 设置结果页，有默认选中项
     *
     * @param key
     * @param pos
     */
    private void setResultsFragment(String key, int pos) {
        saveRecord(key);

        if (!TextUtils.isEmpty(hospital_id)) {
            SearchHospitalResultsFragment searchHospitalResultsFragment = SearchHospitalResultsFragment.newInstance(key, hospital_id);
            setActivityFragment(R.id.activity_search_fragment, searchHospitalResultsFragment);
        } else {
            SearchResultsFragment searchResultsFragment = SearchResultsFragment.newInstance(key, pos);
            searchResultsFragment.setOnEventClickListener(new SearchResultsFragment.OnEventClickListener() {

                @Override
                public void onSearchKeyClick(String key) {
                    mTop.setContent(key);
                    setResultsFragment(key);
                }
            });
            setActivityFragment(R.id.activity_search_fragment, searchResultsFragment);
        }
    }


    /**
     * 保存搜索记录
     *
     * @param key
     */
    private void saveRecord(String key) {
        List<HistorySearchWords> datas = mKjdb.findAllByWhere(HistorySearchWords.class, "hwords='" + key + "'");
        if (datas != null && datas.size() > 0) {
            mKjdb.deleteByWhere(HistorySearchWords.class, "hwords='" + key + "'");
        }
        HistorySearchWords hs = new HistorySearchWords();
        hs.setHwords(key);
        mKjdb.save(hs);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

}
