package com.module.other.module.bean;

public class RewardAlertData {

    private String motionType; // 动效类型 1: 价格递增 2: 静态展示
    private String rewardTitle;//奖励类型名称: 颜值币/现金红包/优惠券
    private String rewardUnit;//奖励单位: +/￥
    private String rewardMoney;//奖励金额
    private String rewardNoticeDesc;// 底部说明

    public String getMotionType() {
        return motionType;
    }

    public void setMotionType(String motionType) {
        this.motionType = motionType;
    }

    public String getRewardTitle() {
        return rewardTitle;
    }

    public void setRewardTitle(String rewardTitle) {
        this.rewardTitle = rewardTitle;
    }

    public String getRewardUnit() {
        return rewardUnit;
    }

    public void setRewardUnit(String rewardUnit) {
        this.rewardUnit = rewardUnit;
    }

    public String getRewardMoney() {
        return rewardMoney;
    }

    public void setRewardMoney(String rewardMoney) {
        this.rewardMoney = rewardMoney;
    }

    public String getRewardNoticeDesc() {
        return rewardNoticeDesc;
    }

    public void setRewardNoticeDesc(String rewardNoticeDesc) {
        this.rewardNoticeDesc = rewardNoticeDesc;
    }
}
