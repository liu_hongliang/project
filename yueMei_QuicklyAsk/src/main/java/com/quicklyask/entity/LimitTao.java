package com.quicklyask.entity;


import com.module.commonview.module.bean.LimitTaoData;

import java.util.List;

public class LimitTao {
	private String code;
	private String message;
	private List<LimitTaoData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<LimitTaoData> getData() {
		return data;
	}

	public void setData(List<LimitTaoData> data) {
		this.data = data;
	}

}
