package com.module.my.controller.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.view.CommonTopBar;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.home.controller.activity.ZhuanTiWebActivity;
import com.module.home.view.LoadingProgress;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.util.WriteNoteManager;
import com.quicklyask.view.BaoxianPopWindow;
import com.quicklyask.view.EditExitDialog;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.net.URLDecoder;
import java.util.HashMap;

//import com.quicklyask.util.HuanXinManager;
//import com.quicklyask.util.RongIMManager;

/**
 * Created by dwb on 17/1/16.
 */
public class WebPaiMingActivity extends BaseActivity {

    private final String TAG = "WebPaiMingActivity";

    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.basic_web_top)
    private CommonTopBar mTop;// 返回

    private WebView docDetWeb;

    private Activity mContex;

    public JSONObject obj_http;

    private String url;
    private String title;
    private String city;
    private String uid;

    private int bTheight;
    private int windowsH;
    private int statusBarHeight;
    private int windowsW;

    @BindView(id = R.id.title_bar_rly)
    private RelativeLayout biaoView;

    @BindView(id = R.id.all_content)
    private LinearLayout contentLy;

    private BaoxianPopWindow baoxianPop;
    private PageJumpManager pageJumpManager;
    private LoadingProgress mDialog;
    private String mTyUrl;


    @Override
    public void setRootView() {
        setContentView(R.layout.acty_peifu_basic_web);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = WebPaiMingActivity.this;

        mDialog = new LoadingProgress(mContex);
        pageJumpManager = new PageJumpManager(mContex);

        windowsH = Cfg.loadInt(mContex, FinalConstant.WINDOWS_H, 0);
        windowsW = Cfg.loadInt(mContex, FinalConstant.WINDOWS_W, 0);

        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        biaoView.measure(w, h);
        bTheight = biaoView.getMeasuredHeight();

        Rect rectangle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        statusBarHeight = rectangle.top;


        Intent it = getIntent();
        url = it.getStringExtra("link");
        title = it.getStringExtra("title");
        city = it.getStringExtra("city");

        url = FinalConstant.baseUrl + FinalConstant.VER + url;

        mTop.setCenterText(title);

        initWebview();

        LodUrl1(url);

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(new MyWebViewClientMessage());
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) docDetWeb.getLayoutParams();
        linearParams.height = windowsH - bTheight - statusBarHeight * 2 - 60;
        linearParams.weight = windowsW;
        docDetWeb.setLayoutParams(linearParams);


        contentWeb.addView(docDetWeb);
    }

    protected void OnReceiveData(String str) {
//        scollwebView.onRefreshComplete();
    }

    public void webReload() {
        if (docDetWeb != null) {
            mDialog.startLoading();
            docDetWeb.reload();
        }
    }

    /**
     * 处理webview里面的按钮
     *
     * @param urlStr
     */
    public void showWebDetail(String urlStr) {
//        Log.e("AAAAAAAAAAAAAA", urlStr);
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;
            obj_http = obj;

            if (obj.getString("type").equals("1")) {// 医生详情页
                try {
                    String id = obj.getString("id");
                    // String docname = URLDecoder.decode(
                    // obj.getString("docname"), "utf-8");

                    Intent it = new Intent();
                    it.setClass(mContex, DoctorDetailsActivity592.class);
                    it.putExtra("docId", id);
                    it.putExtra("docName", "");
                    it.putExtra("partId", "");
                    startActivity(it);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (obj.getString("type").equals("6")) {// 问答详情
                String link = obj.getString("link");
                String qid = obj.getString("id");

                Intent it = new Intent();
                it.setClass(mContex, DiariesAndPostsActivity.class);
                it.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER
                        + link);
                it.putExtra("qid", qid);
                startActivity(it);
            }

            if (obj.getString("type").equals("999")) {// 悦美
                String link = obj.getString("link");
                Intent it1 = new Intent();
                it1.setClass(mContex, SlidePicTitieWebActivity.class);
                it1.putExtra("url", link);
                it1.putExtra("shareTitle", "0");
                it1.putExtra("sharePic", "0");
                startActivity(it1);
            }

            if (obj.getString("type").equals("511")) {// 医院详情
                try {

                    String hosid = obj.getString("hosid");

                    Intent it = new Intent();
                    it.setClass(mContex, HosDetailActivity.class);
                    it.putExtra("hosid", hosid);

                    startActivity(it);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            if (obj.getString("type").equals("5981")) {// 融云
                String kid = obj.getString("id");

                String uid = Utils.getUid();
                if (Utils.isLogin()) {
//                    RongIMManager.getInstance(mContex, null, "").chatAndPrivate(
//                            kid, "在线咨询", "", "0");


                } else {
                    showDialogExitEdit();
                }
            }

            if (obj.getString("type").equals("5982")) {// 美洽

//                HuanXinManager.getInstance(mContex).chatHxkefu(mContex, "kefuchannelimid_861836", "悦美客服", "", "", "", "", "");
            }

            if (obj.getString("type").equals("5983")) {// 私信
                String kid = obj.getString("id");
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(kid)
                        .setObjId("0")
                        .setObjType("0")
                        .setYmClass("0")
                        .setYmId("0")
                        .build();
                pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
            }

            if (obj.getString("type").equals("431")) {// 个人信息
                try {
                    String id = obj.getString("id");
                    Intent it = new Intent();
                    it.setClass(mContex, PersonCenterActivity641.class);
                    it.putExtra("id", id);
                    startActivity(it);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (obj.getString("type").equals("1000")) {// 1000专题
                try {
                    String url = obj.getString("link");
                    String title = URLDecoder.decode(
                            obj.getString("title"), "utf-8");
                    String ztid = obj.getString("id");

                    Intent it1 = new Intent();
                    it1.setClass(
                            mContex,
                            ZhuanTiWebActivity.class);
                    it1.putExtra("url", url);
                    it1.putExtra("title", title);
                    it1.putExtra("ztid", ztid);
                    startActivity(it1);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            if (obj.getString("type").equals("418")) {//淘整形
                try {

                    String id = obj.getString("id");
                    Intent it = new Intent();
                    it.putExtra("id", id);
                    it.putExtra("source", "10");
                    it.putExtra("objid", "0");
                    it.setClass(mContex, TaoDetailActivity.class);
                    startActivity(it);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            if (obj.getString("type").equals("432")) {// 写日记

                String uid = Utils.getUid();

                if (Utils.isLogin()) {
                    if (Utils.isBind()){
                        WriteNoteManager.getInstance(mContex).ifAlert(null);
                    }else {
                        Utils.jumpBindingPhone(mContex);

                    }

                } else {
                    Utils.jumpLogin(mContex);
                }

            }

            if (obj.getString("type").equals("6156")) {// 排行榜弹窗
                String boardtype = obj.getString("boardtype");
                String urlss = FinalConstant.PAIHANGBANG_GUIZE;
                HashMap<String, Object> urlMap = new HashMap<>();
                urlMap.put("boardtype", boardtype);
                baoxianPop = new BaoxianPopWindow(mContex, urlss, urlMap);
                baoxianPop.showAtLocation(contentLy, Gravity.CENTER, 0, 0);

            }

            if (obj.getString("type").equals("6061")) {//观看直播
                try {

                    String uid = Utils.getUid();
                    String fake_token = Cfg.loadStr(mContex, "hj_token", "");

                    if (Utils.isLogin()) {
                            if (Utils.isBind()){
                                if (fake_token.length() > 0) {
                                    String relatedId = obj.getString("relateid");
                                    String type = obj.getString("flag");
                                    //					String sn=obj.getString("sn");
                                    //					String channel=obj.getString("channel");
                                    //					String usign=obj.getString("usign");
                                    //					String image=obj.getString("image");

                                    Bundle bundle = new Bundle();
                                    //					bundle.putString("type",type);
                                    //					bundle.putInt("type",Integer.parseInt(type));
                                    //					bundle.putString("sn",sn);
                                    //					bundle.putString("liveId",relatedId);
                                    //					bundle.putString("replayId",relatedId);
                                    //					bundle.putString("background",image);
                                    //					bundle.putString("channel",channel);
                                    //					bundle.putString("usign",usign);

                                    bundle.putInt("type", Integer.parseInt(type));
                                    bundle.putString("sn", "");
                                    bundle.putString("liveId", relatedId);
                                    bundle.putString("replayId", relatedId);
                                    bundle.putString("background", "");
                                    bundle.putString("channel", "");
                                    bundle.putString("usign", "");

//                            HJSDK.watchLive(WebPaiMingActivity.this, uid, fake_token, bundle);
                                }
                            }else {
                                Utils.jumpBindingPhone(mContex);

                            }


                    } else {
                        Utils.jumpLogin(mContex);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    Button cancelBt;
    Button trueBt11;
    TextView titleTv;

    void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContex,
                R.style.mystyle, R.layout.dialog_edit_exit);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        titleTv = editDialog
                .findViewById(R.id.dialog_exit_content_tv);
        titleTv.setText("亲，您还没有登录哦~");

        cancelBt = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt.setText("取消");
        cancelBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();
            }
        });

        trueBt11 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt11.setText("马上登录");
        trueBt11.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();

                Utils.jumpLogin(mContex);

            }
        });

    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        mDialog.startLoading();

        mTyUrl = urlstr;

        // Log.e(TAG, "url=" + tyUrl);

        docDetWeb.loadUrl(mTyUrl);
    }

    public class MyWebViewClientMessage extends WebViewClient {
        @SuppressWarnings("deprecation")
        public void onExceededDatabaseQuota(String url,
                                            String databaseIdentifier, long currentQuota,
                                            long estimatedSize, long totalUsedQuota,
                                            WebStorage.QuotaUpdater quotaUpdater) {
            quotaUpdater.updateQuota(estimatedSize * 2);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("type")) {
                showWebDetail(url);
            } else if (url.startsWith(FinalConstant.baseUrl + FinalConstant.VER + "/rank/rank/")) {
                docDetWeb.loadUrl(url);
            } else if (url.startsWith(FinalConstant.baseUrl + FinalConstant.VER + "/rank/choosecity/")) {
                docDetWeb.loadUrl(url);
            } else {
                WebUrlTypeUtil.getInstance(mContex).urlToApp(url, "0", "0", mTyUrl);
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public void toProjectDetailActivity(String projectId) {

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            OnReceiveData("");
            mDialog.stopLoading();
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedSslError(WebView view,
                                       SslErrorHandler handler, SslError error) {
            handler.proceed();
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);

        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }


//    /**
//     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
//     */
//    public static int dip2px(Context context, float dpValue) {
//        final float scale = context.getResources().getDisplayMetrics().density;
//        return (int) (dpValue * scale + 0.5f);
//    }
}