package com.quicklyask.entity;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/4/18
 */
public class SearchResultBoard {
    private String id;
    private String title;
    private String postName;
    private String postVal;
    private SearchResultBoardImage image;
    private HashMap<String,String> event_params;
    private boolean isSelected = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostVal() {
        return postVal;
    }

    public void setPostVal(String postVal) {
        this.postVal = postVal;
    }

    public SearchResultBoardImage getImage() {
        return image;
    }

    public void setImage(SearchResultBoardImage image) {
        this.image = image;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
