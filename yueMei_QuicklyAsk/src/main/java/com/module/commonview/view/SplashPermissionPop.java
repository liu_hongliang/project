package com.module.commonview.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;

import com.module.commonview.adapter.PermissionAdapter;
import com.module.commonview.module.bean.PermsissionData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

public class SplashPermissionPop extends PopupWindow {


    public SplashPermissionPop (Context context, List<PermsissionData> list){
        final View view = View.inflate(context, R.layout.splash_permission_pop, null);
        setWidth(Utils.dip2px(300));
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setFocusable(false);
        setOutsideTouchable(false);
        setContentView(view);
        update();
        RecyclerView recyclerView = view.findViewById(R.id.permission_list);
        Button btn = view.findViewById(R.id.permission_btn);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        PermissionAdapter permissionAdapter = new PermissionAdapter(R.layout.permission_list_item, list);
        recyclerView.setAdapter(permissionAdapter);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPermissionCallBack.onPermissionCallBack();
            }
        });
    }

    PermissionCallBack mPermissionCallBack;

    public void setPermissionCallBack(PermissionCallBack permissionCallBack) {
        mPermissionCallBack = permissionCallBack;
    }

    public interface PermissionCallBack{
        void onPermissionCallBack();
    }
}
