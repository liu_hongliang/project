package com.module.home.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.home.model.bean.HomeData623;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * Created by 裴成浩 on 2017/9/29.
 */

public class HomeApi implements BaseCallBackApi {
    private String TAG = "HomeApi";
    private String mHomeData;

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.HOME_NEW, "home", maps, new ServerCallback() {

            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData === " + mData.toString());
                if ("1".equals(mData.code)) {
                    mHomeData = mData.data;
                    HomeData623 home = null;
                    try {
                        home = JSONUtil.TransformSingleBean(mData.data, HomeData623.class);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                    listener.onSuccess(home);
                }
            }
        });
    }

    public String getmHomeData() {
        return mHomeData;
    }

    public void setmHomeData(String mHomeData) {
        this.mHomeData = mHomeData;
    }
}
