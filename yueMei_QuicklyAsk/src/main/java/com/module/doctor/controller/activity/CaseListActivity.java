package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.doctor.model.api.CaseDetailApi;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.view.ElasticScrollView;
import com.quicklyask.view.ElasticScrollView.OnRefreshListener;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 案例列表
 *
 * @author Rubin
 *
 */
public class CaseListActivity extends BaseActivity {

	private final String TAG = "CaseListActivity";

	@BindView(id = R.id.case_lis_web_det_scrollview1, click = true)
	private ElasticScrollView scollwebView;
	@BindView(id = R.id.case_lis_linearlayout, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.case_list_web_back, click = true)
	private RelativeLayout back;// 返回

	private WebView docDetWeb;

	private CaseListActivity mContex;

	private String link;
	public JSONObject obj_http;

	private String caseId;
	private String caseHosId;
	private BaseWebViewClientMessage baseWebViewClientMessage;
	private LoadingProgress mDialog;

	@Override
	public void setRootView() {
		setContentView(R.layout.web_acty_caselist);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = CaseListActivity.this;

		Intent it0 = getIntent();
		link = it0.getStringExtra("link");

		scollwebView.GetLinearLayout(contentWeb);

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
			@Override
			public void otherJump(String urlStr) {
				showWebDetail(urlStr);
			}
		});
		initWebview();
		scollwebView.setonRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				webReload();
			}
		});
		LodUrl(link);

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						CaseListActivity.this.finish();
					}
				});

		mDialog = new LoadingProgress(mContex);
	}

	@TargetApi(Build.VERSION_CODES.ECLAIR)
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	protected void OnReceiveData(String str) {
		scollwebView.onRefreshComplete();
	}

	public void webReload() {
		if (docDetWeb != null) {
			mDialog.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 处理webview里面的按钮
	 *
	 * @param urlStr
	 */
	public void showWebDetail(String urlStr) {
		// Log.e(TAG, urlStr);

		ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
		try {
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			if (obj.getString("type").equals("1")) {// 案例详情
				String id = obj.getString("id");
				caseId = id;
				String hosid = obj.getString("hosid");
				caseHosId = hosid;
				Map<String,Object> maps=new HashMap<>();
				maps.put("id",id);
				new CaseDetailApi().getCallBack(mContex, maps, new BaseCallBackListener<String>() {
					@Override
					public void onSuccess(String s) {
						Intent it = new Intent();
						it.setClass(mContex, CaseFinalPageActivity.class);
						it.putExtra("casefinaljson", s);
						it.putExtra("id", caseId);
						it.putExtra("hosid", caseHosId);
						startActivity(it);
					}
				});
			}

			if (obj.getString("type").equals("6")) {// 问答详情
				String link = obj.getString("link");
				String qid = obj.getString("id");

				Intent it = new Intent();
				it.setClass(mContex, DiariesAndPostsActivity.class);
				it.putExtra("url", link);
				it.putExtra("qid", qid);
				startActivity(it);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 加载web
	 */
	public void LodUrl(String link) {
		mDialog.startLoading();

		WebSignData addressAndHead = SignUtils.getAddressAndHead(link);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.case_list_web_back:
			// finish();
			onBackPressed();
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}