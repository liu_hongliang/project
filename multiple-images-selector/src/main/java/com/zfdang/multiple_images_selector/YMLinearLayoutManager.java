package com.zfdang.multiple_images_selector;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * 文 件 名: YMLinearLayoutManager
 * 创 建 人: 原成昊
 * 创建日期: 2019-12-23 19:42
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class YMLinearLayoutManager extends LinearLayoutManager {
    public YMLinearLayoutManager(Context context) {
        super(context);
    }

    public YMLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public YMLinearLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            //这里捕获之前的数组越界问题...
            super.onLayoutChildren(recycler, state);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

    }
}

