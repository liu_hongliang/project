package com.quicklyask.entity;


import com.module.home.model.bean.SearchTitleDataData;
import com.module.taodetail.model.bean.HomeTaoData;

import java.util.List;

public class SearchResultData1 {

    private String total;                               //搜索结果数
    private List<HomeTaoData> list;
    private SearchTitleDataData data;                    //推荐数据
    private String desc;                                //通过优惠券搜索返回字段
    private String type;                                //通过优惠券搜索返回字段
    private List<SearchResultMethod> search_method;     //方法
    private List<SearchResultBoard> screen_board;       //活动
    private List<HomeTaoData> recomend_list;            //推荐淘列表

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<HomeTaoData> getList() {
        return list;
    }

    public void setList(List<HomeTaoData> list) {
        this.list = list;
    }

    public SearchTitleDataData getData() {
        return data;
    }

    public void setData(SearchTitleDataData data) {
        this.data = data;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<SearchResultMethod> getSearch_method() {
        return search_method;
    }

    public void setSearch_method(List<SearchResultMethod> search_method) {
        this.search_method = search_method;
    }

    public List<SearchResultBoard> getScreen_board() {
        return screen_board;
    }

    public void setScreen_board(List<SearchResultBoard> screen_board) {
        this.screen_board = screen_board;
    }

    public List<HomeTaoData> getRecomend_list() {
        return recomend_list;
    }

    public void setRecomend_list(List<HomeTaoData> recomend_list) {
        this.recomend_list = recomend_list;
    }
}
