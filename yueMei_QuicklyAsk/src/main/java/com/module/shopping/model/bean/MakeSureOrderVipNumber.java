package com.module.shopping.model.DingjinCouponsRank;

/**
 * Created by 裴成浩 on 2018/12/12
 */
public class MakeSureOrderVipNumber {
    private String card_id;
    private String lowest_consumption;
    private String title;
    private String money;
    private String couponsType;
    private String mankeyong;
    private String shiyongtiaojian;
    private String time;

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getLowest_consumption() {
        return lowest_consumption;
    }

    public void setLowest_consumption(String lowest_consumption) {
        this.lowest_consumption = lowest_consumption;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCouponsType() {
        return couponsType;
    }

    public void setCouponsType(String couponsType) {
        this.couponsType = couponsType;
    }

    public String getMankeyong() {
        return mankeyong;
    }

    public void setMankeyong(String mankeyong) {
        this.mankeyong = mankeyong;
    }

    public String getShiyongtiaojian() {
        return shiyongtiaojian;
    }

    public void setShiyongtiaojian(String shiyongtiaojian) {
        this.shiyongtiaojian = shiyongtiaojian;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
