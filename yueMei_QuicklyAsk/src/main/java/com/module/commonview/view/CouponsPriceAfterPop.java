package com.module.commonview.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.util.Cfg;

public class CouponsPriceAfterPop extends PopupWindow {

    public CouponsPriceAfterPop(Context context, String content) {
        final View view = View.inflate(context, R.layout.coupons_price_after_pop, null);
        setAnimationStyle(R.style.AnimBottom);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        int windowH = Cfg.loadInt(context, FinalConstant.WINDOWS_H, 0);
        setHeight((int) (windowH * 0.55));
        setFocusable(true);
        setContentView(view);
        update();
        TextView mSkuProjectContent = view.findViewById(R.id.coupons_price_after_pos_content);

        LinearLayout mSkuprojectClose = view.findViewById(R.id.sku_project_close);
        Button mSkuprojectBtn = view.findViewById(R.id.sku_project_btn);
        mSkuProjectContent.setText(content);

        mSkuprojectClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mSkuprojectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
