package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/6/5
 */
public class SearchResultDoctor implements Parcelable {
    private String comparedTitle;                       //对比咨询列表title
    private ComparedBean comparedA;
    private ComparedBean comparedB;

    public String getComparedTitle() {
        return comparedTitle;
    }

    public void setComparedTitle(String comparedTitle) {
        this.comparedTitle = comparedTitle;
    }

    public ComparedBean getComparedA() {
        return comparedA;
    }

    public void setComparedA(ComparedBean comparedA) {
        this.comparedA = comparedA;
    }

    public ComparedBean getComparedB() {
        return comparedB;
    }

    public void setComparedB(ComparedBean comparedB) {
        this.comparedB = comparedB;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.comparedTitle);
        dest.writeParcelable(this.comparedA, flags);
        dest.writeParcelable(this.comparedB, flags);
    }

    protected SearchResultDoctor(Parcel in) {
        this.comparedTitle = in.readString();
        this.comparedA = in.readParcelable(ComparedBean.class.getClassLoader());
        this.comparedB = in.readParcelable(ComparedBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<SearchResultDoctor> CREATOR = new Parcelable.Creator<SearchResultDoctor>() {
        @Override
        public SearchResultDoctor createFromParcel(Parcel source) {
            return new SearchResultDoctor(source);
        }

        @Override
        public SearchResultDoctor[] newArray(int size) {
            return new SearchResultDoctor[size];
        }
    };
}
