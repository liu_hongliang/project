package com.module.home.model.bean;

import com.module.community.model.bean.BBsListData550;

import java.util.HashMap;
import java.util.List;

public class ShareListNewData {
    private String showType;
    private List<BBsListData550> list;
    private HashMap<String,String> show_control;

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }

    public List<BBsListData550> getList() {
        return list;
    }

    public void setList(List<BBsListData550> list) {
        this.list = list;
    }

    public HashMap<String, String> getShow_control() {
        return show_control;
    }

    public void setShow_control(HashMap<String, String> show_control) {
        this.show_control = show_control;
    }
}
