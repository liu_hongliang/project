package com.module.home.model.bean;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2017/7/31.
 */

public class HotBean {
    private String bmsid;
    private String url;
    private String img;
    private String lable;
    private String title;
    private String price;
    private String price_x;
    private String desc;
    private String member_price;
    private HashMap<String,String>event_params;

    public String getBmsid() {
        return bmsid;
    }

    public void setBmsid(String bmsid) {
        this.bmsid = bmsid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_x() {
        return price_x;
    }

    public void setPrice_x(String price_x) {
        this.price_x = price_x;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }

    public HashMap<String,String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String,String> event_params) {
        this.event_params = event_params;
    }
}
