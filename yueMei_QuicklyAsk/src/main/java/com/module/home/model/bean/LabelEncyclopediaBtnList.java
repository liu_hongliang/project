package com.module.home.model.bean;

import java.util.HashMap;

/**
 * @author 裴成浩
 * @data 2019/11/7
 */
public class LabelEncyclopediaBtnList {
    private String btn_icon;
    private String btn_title;
    private String btn_url;
    private HashMap<String,String> event_params;

    public String getBtn_icon() {
        return btn_icon;
    }

    public void setBtn_icon(String btn_icon) {
        this.btn_icon = btn_icon;
    }

    public String getBtn_title() {
        return btn_title;
    }

    public void setBtn_title(String btn_title) {
        this.btn_title = btn_title;
    }

    public String getBtn_url() {
        return btn_url;
    }

    public void setBtn_url(String btn_url) {
        this.btn_url = btn_url;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
