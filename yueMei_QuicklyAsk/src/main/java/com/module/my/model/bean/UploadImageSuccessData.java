package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2018/8/9.
 */
public class UploadImageSuccessData {

    private int width;                  //压缩后的图片宽度
    private int height;                 //压缩后的图片高度
    private boolean isContinuous;           //是否是连续的
    private String ImageUrl;                //图片网络路径

    public UploadImageSuccessData(boolean isContinuous) {
        this.isContinuous = isContinuous;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isContinuous() {
        return isContinuous;
    }

    public void setContinuous(boolean continuous) {
        isContinuous = continuous;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }
}
