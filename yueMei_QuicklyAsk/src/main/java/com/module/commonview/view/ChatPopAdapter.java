package com.module.commonview.view;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.commonview.module.bean.MessageBean;
import com.quicklyask.activity.R;

import java.util.List;

public class ChatPopAdapter extends BaseQuickAdapter <MessageBean.CouponsBean,BaseViewHolder>{
    public ChatPopAdapter(int layoutResId, @Nullable List<MessageBean.CouponsBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MessageBean.CouponsBean item) {
        helper.setText(R.id.pop_redpack_money,item.getMoney())
                .setText(R.id.pop_redpack_lowest_consumption,"满"+item.getLowest_consumption()+"元可用")
                .setText(R.id.pop_redpack_end_time,"有效期至："+item.getEnd_time());
    }
}
