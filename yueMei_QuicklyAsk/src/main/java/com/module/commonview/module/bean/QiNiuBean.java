package com.module.commonview.module.bean;

public class QiNiuBean {

    /**
     * qiniu_token : AqmHERRCZbJ194cJTPZmPCZn6VupDT6un2h7JlqD:y-JBga9p2N39cqYO4-uuyIjb64s=:eyJzY29wZSI6InltLXVwbG9hZCIsImRlYWRsaW5lIjoxNTMxOTE0MTY4fQ==
     * ym_token :
     */

    private String qiniu_token;
    private String ym_token;

    public String getQiniu_token() {
        return qiniu_token;
    }

    public void setQiniu_token(String qiniu_token) {
        this.qiniu_token = qiniu_token;
    }

    public String getYm_token() {
        return ym_token;
    }

    public void setYm_token(String ym_token) {
        this.ym_token = ym_token;
    }
}
