package com.module.my.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.Expression;
import com.quicklyack.emoji.Expressions;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 文字编辑
 */
public class WriteFontActivity extends YMBaseActivity {

    private final String TAG = "WriteFontActivity";

    @BindView(R.id.write_font_top)
    CommonTopBar mTop;
    @BindView(R.id.write_exp_content_et)
    EditText postContent;   // 提交的内容控件

    @BindView(R.id.rootview_lyly)
    LinearLayout rootview;
    @BindView(R.id.biaoqing_shuru_content_ly1)
    LinearLayout biaoqingContentLy;
    @BindView(R.id.biaoqing_ly_content)
    LinearLayout biaoqingBtRly;

    @BindView(R.id.write_number_zi_tv)
    TextView zinumberTv;

    @BindView(R.id.colse_biaoqingjian_bt)
    ImageButton closeImBt;

    // 表情
    private ViewPager viewPager;
    private ArrayList<GridView> grids;
    private int[] expressionImages;
    private String[] expressionImageNames;
    private int[] expressionImages1;
    private String[] expressionImageNames1;
    private int[] expressionImages2;
    private String[] expressionImageNames2;
    private GridView gView1;
    private GridView gView2;
    private GridView gView3;
    private ImageView page0;
    private ImageView page1;
    private ImageView page2;

    private static final int FONT = 4;

    @Override
    protected int getLayoutId() {
        return R.layout.acty_write_font;
    }

    @Override
    protected void initView() {
        Intent it = getIntent();
        String tt = it.getStringExtra("text_str");
        if (tt.length() > 0) {
            try {
                SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(tt, mContext, Utils.dip2px(12));

                postContent.setText(stringBuilder);
                postContent.setSelection(postContent.getText().length());
                mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        page0 = findViewById(R.id.page0_select);
        page1 = findViewById(R.id.page1_select);
        page2 = findViewById(R.id.page2_select);
        // 引入表情
        expressionImages = Expressions.expressionImgs;
        expressionImageNames = Expressions.expressionImgNames;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;
        expressionImages2 = Expressions.expressionImgs2;
        expressionImageNames2 = Expressions.expressionImgNames2;
        // 创建ViewPager
        viewPager = findViewById(R.id.viewpager);
        initViewPager();

        postContent.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                String textStr = postContent.getText().toString().trim();

                if (textStr.length() >= 20) {
                    mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
                } else {
                    mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2, int count) {
                if (s.length() > 0) {
                    zinumberTv.setText("已输入" + s.length() + "个字符");
                } else {
                    zinumberTv.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {

            }
        });

        rootview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // 比较Activity根布局与当前布局的大小

                Rect r = new Rect();
                //获取当前界面可视部分
                getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                //获取屏幕的高度
                int screenHeight = getWindow().getDecorView().getRootView().getHeight();
                //此处就是用来获取键盘的高度的， 在键盘没有弹出的时候 此高度为0 键盘弹出的时候为一个正数
                int heightDifference = screenHeight - r.bottom;
                Log.e(TAG, "Size: " + heightDifference);

                //如果等于0说明键盘隐藏
                if (heightDifference > 0) {
                    biaoqingBtRly.setVisibility(View.VISIBLE);
                } else {
                    biaoqingBtRly.setVisibility(View.GONE);
                }

            }
        });

        biaoqingBtRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                biaoqingContentLy.setVisibility(View.VISIBLE);
            }
        });

        closeImBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });

        postContent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
            }
        });

        //取消点击事件
        mTop.setLeftTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //完成点击事件
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                String texStr = postContent.getText().toString();
                Intent it = new Intent();
                it.putExtra("text_str", texStr);
                it.setClass(mContext, WriteNoteActivity.class);
                setResult(FONT, it);
                finish();
            }
        });
    }

    @Override
    protected void initData() {

    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    private void initViewPager() {
        LayoutInflater inflater = LayoutInflater.from(this);
        grids = new ArrayList<>();
        gView1 = (GridView) inflater.inflate(R.layout.grid1, null);

        List<Map<String, Object>> listItems = new ArrayList<>();
        // 生成28个表情
        for (int i = 0; i < 28; i++) {
            Map<String, Object> listItem = new HashMap<>();
            listItem.put("image", expressionImages[i]);
            listItems.add(listItem);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                if (arg2 == 27) {
                    // 动作按下
                    int action = KeyEvent.ACTION_DOWN;
                    // code:删除，其他code也可以，例如 code = 0
                    int code = KeyEvent.KEYCODE_DEL;
                    KeyEvent event = new KeyEvent(action, code);
                    postContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                } else {
                    Bitmap bitmap = null;
                    bitmap = BitmapFactory.decodeResource(getResources(), expressionImages[arg2 % expressionImages.length]);

                    bitmap = zoomImage(bitmap, 47, 47);
                    ImageSpan imageSpan = new ImageSpan(mContext, bitmap);

                    SpannableString spannableString = new SpannableString(expressionImageNames[arg2].substring(1, expressionImageNames[arg2].length() - 1));

                    spannableString.setSpan(imageSpan, 0, expressionImageNames[arg2].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    // 编辑框设置数据
                    //postContent.append(spannableString);
                    int index = postContent.getSelectionStart();//获取光标所在位置

                    Editable edit = postContent.getEditableText();//获取EditText的文字
                    if (index < 0 || index >= edit.length()) {
                        edit.append(spannableString);
                    } else {
                        edit.insert(index, spannableString);//光标所在位置插入文字
                    }
                }
            }
        });
        grids.add(gView1);

        gView2 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView2);
        gView3 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView3);

        // 填充ViewPager的数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return grids.size();
            }

            @Override
            public void destroyItem(View container, int position, Object object) {
                ((ViewPager) container).removeView(grids.get(position));
            }

            @Override
            public Object instantiateItem(View container, int position) {
                ((ViewPager) container).addView(grids.get(position));
                return grids.get(position);
            }
        };

        viewPager.setAdapter(mPagerAdapter);
        //viewPager.setAdapter();

        viewPager.setOnPageChangeListener(new GuidePageChangeListener());
    }

    //** 指引页面改监听器 */
    class GuidePageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    page2.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    break;
                case 1:
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    page2.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));

                    List<Map<String, Object>> listItems = new ArrayList<>();

                    // 生成28个表情
                    for (int i = 0; i < 28; i++) {
                        Map<String, Object> listItem = new HashMap<>();
                        listItem.put("image", expressionImages1[i]);
                        listItems.add(listItem);
                    }

                    SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

                    gView2.setAdapter(simpleAdapter);
                    // 表情点击
                    gView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                postContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {

                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(getResources(), expressionImages1[pos % expressionImages1.length]);
                                bitmap = zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mContext, bitmap);
                                SpannableString spannableString = new SpannableString(expressionImageNames1[pos].substring(1, expressionImageNames1[pos].length() - 1));

                                spannableString.setSpan(imageSpan, 0, expressionImageNames1[pos].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                                int index = postContent.getSelectionStart();//获取光标所在位置

                                Editable edit = postContent.getEditableText();//获取EditText的文字
                                if (index < 0 || index >= edit.length()) {
                                    edit.append(spannableString);
                                } else {
                                    edit.insert(index, spannableString);//光标所在位置插入文字
                                }
                            }
                        }
                    });
                    break;
                case 2:
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    page2.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));

                    List<Map<String, Object>> listItems2 = new ArrayList<Map<String, Object>>();

                    // 生成28个表情
                    for (int i = 0; i < 15; i++) {
                        Map<String, Object> listItem = new HashMap<String, Object>();
                        listItem.put("image", expressionImages2[i]);
                        listItems2.add(listItem);
                    }

                    SimpleAdapter simpleAdapter2 = new SimpleAdapter(mContext, listItems2, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

                    gView3.setAdapter(simpleAdapter2);
                    // 表情点击
                    gView3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                postContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {

                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(getResources(), expressionImages2[pos % expressionImages2.length]);
                                bitmap = zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mContext, bitmap);
                                SpannableString spannableString = new SpannableString(expressionImageNames2[pos].substring(1, expressionImageNames2[pos].length() - 1));

                                spannableString.setSpan(imageSpan, 0, expressionImageNames2[pos].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                // 编辑框设置数据
                                int index = postContent.getSelectionStart();//获取光标所在位置

                                Editable edit = postContent.getEditableText();//获取EditText的文字
                                if (index < 0 || index >= edit.length()) {
                                    edit.append(spannableString);
                                } else {
                                    edit.insert(index, spannableString);//光标所在位置插入文字
                                }
                            }
                        }
                    });
                    break;
            }
        }
    }

    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width, (int) height, matrix, true);
        return bitmap;
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
