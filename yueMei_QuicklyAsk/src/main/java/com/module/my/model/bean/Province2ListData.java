package com.module.my.model.bean;

import java.util.List;

public class Province2ListData {

	private String _id;
	private String name;
	private List<City2List> list;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<City2List> getList() {
		return list;
	}

	public void setList(List<City2List> list) {
		this.list = list;
	}

}
