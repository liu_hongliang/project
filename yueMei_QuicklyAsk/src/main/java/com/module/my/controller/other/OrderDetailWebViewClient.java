package com.module.my.controller.other;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.format.Time;
import android.util.Log;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.MapHospitalWebActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.my.model.api.ChangeTimeApi;
import com.module.my.view.orderpay.OrderDetailActivity;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;

import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mirko.android.datetimepicker.date.DatePickerDialog;



/**
 * Created by 裴成浩 on 2018/1/22.
 */

public class OrderDetailWebViewClient implements BaseWebViewClientCallback {
    public static final String TAG="OrderDetailWebViewClien";
    private final Intent intent;
    private final OrderDetailActivity mActivity;
    private final OrderDetailWebViewClient orderDetailWebViewClient;
    private String uid;
    private String time_order_id;

    private final Calendar mCalendar = Calendar.getInstance();

    private int day = mCalendar.get(Calendar.DAY_OF_MONTH);

    private int month = mCalendar.get(Calendar.MONTH);

    private int year = mCalendar.get(Calendar.YEAR);
    private String dateStr = "";
    private String mPhoneFen="";
    private String mPhone400;

    public OrderDetailWebViewClient(OrderDetailActivity activity) {
        this.mActivity = activity;
        orderDetailWebViewClient = OrderDetailWebViewClient.this;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;
        String mType = obj.getString("type");
        mPhone400 = obj.getString("tel");
        if (mPhone400.contains(",")){
            String[] split = mPhone400.split(",");
            mPhone400 =split[0];
            mPhoneFen = split[1];
        }
        switch (mType) {
            case "6":// 问答详情

                String link = obj.getString("link");
                String qid = obj.getString("id");

                Intent it = new Intent();
                it.setClass(mActivity, DiariesAndPostsActivity.class);
                it.putExtra("url", link);
                it.putExtra("qid", qid);
                mActivity.startActivity(it);
                break;
            case "443"://更改预约时间

                time_order_id = obj.getString("order_id");
                // CHANGE_TIME 更改预约时间
                String tag = "";
                datePickerDialog.show(mActivity.getFragmentManager(), tag);

                break;
            case "535": //打电话
                ViewInject.toast("正在拨打中·····");
                //版本判断
                if (Build.VERSION.SDK_INT >= 23) {

                    Acp.getInstance(mActivity).request(new AcpOptions.Builder()
                                    .setPermissions(Manifest.permission.CALL_PHONE)
                                    .build(),
                            new AcpListener() {
                                @Override
                                public void onGranted() {        //判断权限是否开启
                                    phoneCall();
                                }

                                @Override
                                public void onDenied(List<String> permissions) {
                                    ViewInject.toast("没有电话权限");
                                }
                            });
                } else {
                    phoneCall();
                }
                break;
            case "541": //医院位置

                String hosid541 = obj.getString("hosid");
                intent.setClass(mActivity, MapHospitalWebActivity.class);
                intent.putExtra("hosid", hosid541);
                mActivity.startActivity(intent);

                break;

            case "6079"://融云私信
                if (null != mActivity.groupRIMdata1) {
//                    RongIMManager.getInstance(mActivity, mActivity.groupRIMdata1, "").chatAndGroup("");
                }
                break;
        }
    }

    final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePickerDialog datePickerDialog,
                                      int year, int month, int day) {
                    // dateStr = year + "-" + (month + 1) + "-" + day;
                    // changeTime(time_order_id, dateStr);

                    if (year >= orderDetailWebViewClient.year) {
                        if (year == orderDetailWebViewClient.year) {
                            if (month >= orderDetailWebViewClient.month) {
                                if (month == orderDetailWebViewClient.month) {
                                    if (day >= orderDetailWebViewClient.day) {
                                        dateStr = year + "-" + (month + 1)
                                                + "-" + day;
                                        changeTime(time_order_id, dateStr);
                                    } else {
                                        dateStr = "";
                                        ViewInject.toast("预约时间无效");
                                    }
                                } else {
                                    dateStr = year + "-" + (month + 1) + "-"
                                            + day;
                                    changeTime(time_order_id, dateStr);
                                }
                            } else {
                                dateStr = "";
                                ViewInject.toast("预约时间无效");
                            }
                        } else {
                            dateStr = year + "-" + (month + 1) + "-" + day;
                            changeTime(time_order_id, dateStr);
                        }
                    } else {
                        dateStr = "";
                        ViewInject.toast("预约时间无效");
                    }
                }
            }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
            mCalendar.get(Calendar.DAY_OF_MONTH));

    void changeTime(String order_id, String data) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("arrive_time", data);
        maps.put("order_id", order_id);
        maps.put("uid", uid);
        new ChangeTimeApi().getCallBack(mActivity, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    ViewInject.toast("修改成功");
                    // MyToast.makeTextToast(mContex, "修改成功",
                    // 1000).show();
                    mActivity.webReload();
                }
            }

        });

    }
    /**
     * 打电话
     */
    private void phoneCall() {

        Time t = new Time(); // or Time t=new Time("GMT+8"); 加上Time
        t.setToNow(); // 取得系统时间。
        int hour = t.hour; // 0-23
        int minute = t.minute;
        int second = t.second;
        Log.e(TAG, "hour === " + hour);
        if (hour > 9 && hour < 22) {
            ViewInject.toast("正在拨打中·····");
            if ("".equals(mPhoneFen)){
                Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mPhone400));
                mActivity.startActivity(it);
            }else {
                Intent it1 = new Intent(Intent.ACTION_CALL);
                it1.setData(Uri.fromParts("tel", mPhone400+",,"+mPhoneFen, null));//拼一个电话的Uri，拨打分机号 关键代码
                it1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(it1);
            }
        } else if (hour == 9 && minute >= 30) {
            ViewInject.toast("正在拨打中·····");
            if ("".equals(mPhoneFen)){
                Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mPhone400));
                mActivity.startActivity(it);
            }else {
                Intent it1 = new Intent(Intent.ACTION_CALL);
                it1.setData(Uri.fromParts("tel", mPhone400+",,"+mPhoneFen, null));//拼一个电话的Uri，拨打分机号 关键代码
                it1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(it1);
            }
        } else if (hour == 21 && minute <= 30) {
            ViewInject.toast("正在拨打中·····");
            if ("".equals(mPhoneFen)){
                Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mPhone400));
                mActivity.startActivity(it);
            }else {
                Intent it1 = new Intent(Intent.ACTION_CALL);
                it1.setData(Uri.fromParts("tel", mPhone400+",,"+mPhoneFen, null));//拼一个电话的Uri，拨打分机号 关键代码
                it1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(it1);
            }
        }
    }
}
