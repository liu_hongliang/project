package com.module.commonview.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.quicklyask.activity.R;

public class YanzhibiPopwindow extends PopupWindow {


    public YanzhibiPopwindow(Context context){
        final View view = View.inflate(context, R.layout.yanzhibi_pop, null);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setContentView(view);
        update();

        LinearLayout lowPriceKonw=view.findViewById(R.id.sku_low_price_konw);
        LinearLayout lowPriceClose=view.findViewById(R.id.low_price_close);

        lowPriceClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        lowPriceKonw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
