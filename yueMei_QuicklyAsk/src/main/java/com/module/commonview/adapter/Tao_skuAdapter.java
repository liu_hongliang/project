package com.module.commonview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.entity.Rel_Tao;

import java.util.ArrayList;
import java.util.List;

public class Tao_skuAdapter extends BaseAdapter {

	private final String TAG = "Tao_skuAdapter";

	private List<Rel_Tao> mGroupData = new ArrayList<Rel_Tao>();
	private Context mContext;
	private LayoutInflater inflater;
	private Rel_Tao groupData;
	ViewHolder viewHolder;
	private String taoid;

	public Tao_skuAdapter(Context mContext, List<Rel_Tao> mGroupData,
			String taoid) {
		this.mContext = mContext;
		this.mGroupData = mGroupData;
		this.taoid = taoid;
		inflater = LayoutInflater.from(mContext);
	}

	static class ViewHolder {
		public TextView groupNameTV;
		public ImageView taoskuIv;
	}

	@Override
	public int getCount() {
		return mGroupData.size();
	}

	@Override
	public Object getItem(int position) {
		return mGroupData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_tao_sku, null);
			viewHolder = new ViewHolder();
			viewHolder.groupNameTV = convertView
					.findViewById(R.id.home_pop_part_name_tv);
			viewHolder.taoskuIv = convertView
					.findViewById(R.id.tao_sku_gou);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		groupData = mGroupData.get(position);

		viewHolder.groupNameTV.setText(groupData.getSpe_name());

		if (groupData.getTao_id().equals(taoid)) {
			viewHolder.taoskuIv.setVisibility(View.VISIBLE);
			viewHolder.groupNameTV.setTextColor(mContext.getResources()
					.getColor(R.color.red_ff4965));
			viewHolder.groupNameTV
					.setBackgroundResource(R.drawable.shape_ff5c77_6px);
		} else {
			viewHolder.taoskuIv.setVisibility(View.GONE);
			if (groupData.getStatus().equals("4")) {
				viewHolder.groupNameTV.setTextColor(mContext.getResources()
						.getColor(R.color._3));
				viewHolder.groupNameTV
						.setBackgroundResource(R.drawable.shape_cccccc_6px);
			} else {
				viewHolder.groupNameTV.setTextColor(mContext.getResources()
						.getColor(R.color.ddd));
				viewHolder.groupNameTV
						.setBackgroundResource(R.drawable.shape_e5e5e5_6px);
			}
		}

		return convertView;
	}

	public void add(List<Rel_Tao> infos) {
		mGroupData.addAll(infos);
	}
}
