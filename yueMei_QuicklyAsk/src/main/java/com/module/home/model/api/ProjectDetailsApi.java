package com.module.home.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.home.model.bean.ProjectDetailsData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 裴成浩 on 2019/2/26
 */
public class ProjectDetailsApi implements BaseCallBackApi {
    private String TAG = "ProjectDetailsApi";
    private HashMap<String, Object> mProjectDetailsHashMap;  //传值容器

    public ProjectDetailsApi() {
        mProjectDetailsHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.CHANNEL, "channel", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, mData.toString());
                if ("1".equals(mData.code)) {
                    ProjectDetailsData projectDetailsData = JSONUtil.TransformSingleBean(mData.data, ProjectDetailsData.class);
                    listener.onSuccess(projectDetailsData);
                }
            }
        });
    }

    public HashMap<String, Object> getProjectDetailsHashMap() {
        return mProjectDetailsHashMap;
    }

    public void addData(String key, String value) {
        mProjectDetailsHashMap.put(key, value);
    }
}
