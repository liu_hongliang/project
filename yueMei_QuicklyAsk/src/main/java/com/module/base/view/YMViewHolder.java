package com.module.base.view;

import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseViewHolder;

/**
 * Created by 裴成浩 on 2019/3/12
 */
public class YMViewHolder extends RecyclerView.ViewHolder {

    private final SparseArray<View> views;      //获取到所有View的集合
    private YMRecyclerViewAdapter adapter;

    public YMViewHolder(View view) {
        super(view);
        this.views = new SparseArray<>();
    }

    public YMViewHolder setAdapter(YMRecyclerViewAdapter adapter) {
        this.adapter = adapter;
        return this;
    }

    /**
     * 将设置TextView的文本。
     *
     * @param viewId 视图id
     * @param value  要放入文本视图中的文本
     * @return YMViewHolder对象
     */
    public YMViewHolder setText(@IdRes int viewId, CharSequence value) {
        TextView view = getView(viewId);
        view.setText(value);
        return this;
    }

    public YMViewHolder setText(@IdRes int viewId, @StringRes int strId) {
        TextView view = getView(viewId);
        view.setText(strId);
        return this;
    }


    @SuppressWarnings("unchecked")
    public <T extends View> T getView(@IdRes int viewId) {
        View view = views.get(viewId);
        if (view == null) {
            view = itemView.findViewById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 将视图可见性设置为可见(true)或消失(false)
     *
     * @param viewId
     * @param visible
     * @return
     */
    public YMViewHolder setGone(@IdRes int viewId, boolean visible) {
        View view = getView(viewId);
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
        return this;
    }

    /**
     * 将视图可见性设置为可见(true)或不可见(false)。
     *
     * @param viewId
     * @param visible
     * @return
     */
    public YMViewHolder setInVisible(@IdRes int viewId, boolean visible) {
        View view = getView(viewId);
        view.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
        return this;
    }

    /**
     * 设置View的点击监听
     *
     * @param viewId
     * @param listener
     * @return
     */
    @Deprecated
    public YMViewHolder setOnClickListener(@IdRes int viewId, View.OnClickListener listener) {
        View view = getView(viewId);
        view.setOnClickListener(listener);
        return this;
    }


}
