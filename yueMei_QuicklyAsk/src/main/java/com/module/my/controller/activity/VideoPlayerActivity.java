package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.BaikeFourApi;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.commonview.module.api.IsCollectApi;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.GetVideoInformationApi;
import com.module.my.model.bean.VideoPlay;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.broadcast.NetWorkChangeBroadcastReceiver;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.NetworkStatus;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.PauseHandler;
import com.quicklyask.view.VideoView;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 视频播放页
 *
 * @author 裴成浩
 */
public class VideoPlayerActivity extends BaseActivity {
    private String TAG = "VideoPlayerActivity";

    @BindView(id = R.id.rl_video_content)
    private RelativeLayout mVideoContent;

    @BindView(id = R.id.rl_video_head)
    private RelativeLayout mHead;

    @BindView(id = R.id.ll_video_control_panel)
    private LinearLayout mbottom;

    @BindView(id = R.id.iv_video_watermark)
    private ImageView mWatermake;

    @BindView(id = R.id.vv_viodeo_player)
    private VideoView mVideoView;

    @BindView(id = R.id.shopping_bags)
    private RelativeLayout shoppingBags;
    @BindView(id = R.id.video_pause_click)
    private RelativeLayout pauseClick;
    @BindView(id = R.id.video_pause)
    private ImageView videoPause;
    @BindView(id = R.id.ll_again_play)
    private LinearLayout againPlay;

    @BindView(id = R.id.iv_again_play)
    private ImageView videoPlay;

    @BindView(id = R.id.ll_interrupt_signal)
    private LinearLayout mSignal;
    @BindView(id = R.id.video_max)
    private RelativeLayout videoMax;
    @BindView(id = R.id.seekbar_video)
    private SeekBar seekbar;

    @BindView(id = R.id.tv_start_time)
    private TextView startTime;
    @BindView(id = R.id.tv_end_time)
    private TextView endTime;

    @BindView(id = R.id.iv_video_collection)
    private ImageView mCollection;
    @BindView(id = R.id.iv_video_share)
    private ImageView mShare;
    @BindView(id = R.id.iv_video_clos)
    private ImageView mClos;
    @BindView(id = R.id.ll_continue_play)
    private LinearLayout continue_play_show;
    @BindView(id = R.id.tv_continue_play_yd)
    private TextView continuePlay;

    @BindView(id = R.id.vido_sku_pager)
    private ViewPager skuPager;
    @BindView(id = R.id.ll_num_layout)
    private LinearLayout mNumLayout;

    @BindView(id = R.id.rl_sku_pager)
    private RelativeLayout skuLL;

    @BindView(id = R.id.iv_shop_sku_clos)
    private ImageView shopSkuClos;

    @BindView(id = R.id.stop_sku)
    private LinearLayout stopSku;

    //视频重新播放时的sku
    @BindView(id = R.id.stop_video_container1)
    private LinearLayout stopContainer1;
    @BindView(id = R.id.stop_video_container2)
    private LinearLayout stopContainer2;

    @BindView(id = R.id.ll_popsku_show)
    private LinearLayout popskuShow;

    @BindView(id = R.id.iv_tan_sku_img)
    private ImageView tanSkuImg;
    @BindView(id = R.id.tv_tan_sku_title)
    private TextView tanSkuTitle;
    @BindView(id = R.id.tv_tan_sku_hos)
    private TextView tanSkuHos;
    @BindView(id = R.id.tv_tan_sku_price)
    private TextView tanSkuPrice;
    @BindView(id = R.id.tv_video_error)
    private TextView videoError;

    private String videoPath;
    private boolean fullScreenMax = false;     //是否可以全屏,默认是不可以的
    private boolean stretch_flag = true;       //默认是竖屏的
    private int mWidth;
    private int mHeight;

    private int posS;
    private boolean videoURL = false;       //是否是网络视频（默认为本地视频）
    private int duration;
    private SensorManager sm;
    private Sensor sensor;
    private OrientationSensorListener listener;
    private SensorManager sm1;
    private Sensor sensor1;
    private OrientationSensorListener2 listener1;
    private boolean sensor_flag = true;

    private VideoPlayerActivity mContext = VideoPlayerActivity.this;
    private String shareTitle = "";
    private String shareContent = "";
    private String shareUrl = "";
    private String shareImgUrl = "";
    private boolean ifcollect = false;
    private final int BACK = 689;
    private String mainQid = "";
    private String uid;
    private int currentPosition;
    private int typePosition;
    private BroadcastReceiver netReceiver;
    private EditExitDialog editDialog;
    private Button mPreSelectedBt;
    private List<VideoPlay.DataBean.TaoBean> skuTao = new ArrayList<>();

    // 分享
    private static final int SHOW_TIME = 1000;
    private final int PROGRESS = 1;
    private final int SOMEHOW_SCREEN = 888;
    private final int SKU_SHOW = 666;
    private final int SKU_DIMSS = 667;
    private final int HAVE_ACCELEROMETER = 668;
    private boolean skuShowComplete = false;            //sku三次是否已经执行完毕。
    private boolean playEnd = true;     //结束是true，其他状态为flase

    private boolean isPlaying;
    private LayoutInflater mInflater;
    private int windowsWight;
    private boolean showLayer = false;      //是否在大于5s时显示购物袋
    private boolean isRestart = false;      //是否是重新启动的

    @SuppressLint("HandlerLeak")
    private PauseHandler mHandler = new PauseHandler() {
        @Override
        protected boolean storeMessage(Message message) {
            //视频是显示隐藏sku消息时,消息不暂停发送
            return message.what == SKU_SHOW || message.what == SKU_DIMSS;
        }

        @Override
        protected void processMessage(Message msg) {
            switch (msg.what) {
                case PROGRESS:

                    //1.得到当前进度
                    currentPosition = mVideoView.getCurrentPosition();
//                    Log.e(TAG, "currentPosition === " + currentPosition);
                    //视频时间大于5,且有sku关联,那么开始弹出购物袋。这个消息只发送一次(就是在重新播放和首次播放时)
                    if (skuTao != null && skuTao.size() > 0 && currentPosition >= 5000 && showLayer) {
                        if (againPlay.getVisibility() != View.VISIBLE && stopSku.getVisibility() != View.VISIBLE && skuLL.getVisibility() != View.VISIBLE) {
                            if (!isRestart) {            //不是重新启动的
                                Message msgS = Message.obtain();
                                msgS.arg1 = 0;
                                msgS.what = SKU_SHOW;
                                mHandler.sendMessage(msgS);
                                mHandler.resume();
                                showLayer = false;
                            }
                        }
                    }

                    //保存当前的进度（在视频中断后，重新播放时使用）
                    if (currentPosition != 0) {
                        typePosition = currentPosition;
                    }
                    //视频进度的更新
                    seekbar.setProgress(currentPosition);
                    //设置时间跟新
                    startTime.setText(msConversion(currentPosition));
                    //循环发消息
                    removeMessages(PROGRESS);

                    if (currentPosition <= duration) {                                 //没有播放完成
                        //继续发送消息
                        sendEmptyMessageDelayed(PROGRESS, 100);
                    }

                    break;
                case SOMEHOW_SCREEN:
                    int orientation = msg.arg1;
                    if (orientation > 45 && orientation < 135) {

                    } else if (orientation > 135 && orientation < 225) {

                    } else if (orientation > 225 && orientation < 315) {
                        Log.e(TAG, "切换成横屏");
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        sensor_flag = false;
                        stretch_flag = false;

                    } else if ((orientation > 315 && orientation < 360) || (orientation > 0 && orientation < 45)) {
                        Log.e(TAG, "切换成竖屏");
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        sensor_flag = true;
                        stretch_flag = true;
                    }

                    break;

                case SKU_SHOW:              //sku显示
                    Log.e(TAG, "sku显示");

                    posS = msg.arg1;
                    VideoPlay.DataBean.TaoBean taoB = skuTao.get(posS);
//                    Picasso.with(mContext).load(taoB.getImg()).placeholder(R.drawable.radius_gray80).error(R.drawable.radius_gray80).into(tanSkuImg);
                    Glide.with(mContext).load(taoB.getImg()).placeholder(R.drawable.radius_gray80).error(R.drawable.radius_gray80).into(tanSkuImg);
                    tanSkuTitle.setText(taoB.getTitle());
                    tanSkuHos.setText(taoB.getHos_name());
                    tanSkuPrice.setText("￥" + taoB.getPrice_discount());

                    popskuShow.setVisibility(View.VISIBLE);

                    Message msgS = Message.obtain();
                    msgS.arg1 = posS;
                    msgS.what = SKU_DIMSS;
                    mHandler.sendMessageDelayed(msgS, 3000);

                    break;
                case SKU_DIMSS:        //sku隐藏
                    Log.e(TAG, "sku隐藏");
                    popskuShow.setVisibility(View.GONE);
                    int posD = msg.arg1;

                    posD++;
                    if (posD != skuTao.size()) {
                        Message msgD = Message.obtain();
                        msgD.arg1 = posD;
                        msgD.what = SKU_SHOW;
                        mHandler.sendMessageDelayed(msgD, 2000);
                    } else {
                        skuShowComplete = true;
                    }

                    break;
                case HAVE_ACCELEROMETER:
                    haveAccelerometer = true;
                    break;
            }
        }
    };
    private int videoWidth;
    private int videoHeight;
    private ShareWechat shareWechat;
    private LoadingProgress mDialog;
    private String isProgram;
    private boolean haveAccelerometer = true;       //是否有重力感应
    private boolean is_network_video;          //flase,本地视频，true网路视频
    private int progress;
    public static final int RETURN_VIDEO_PROGRESS = 100;

    @Override
    public void setRootView() {
        setContentView(R.layout.activity_video_player);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //去掉头信息
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;

        mDialog = new LoadingProgress(mContext);

        HorizontalPortraitSwitching();

        uid = Utils.getUid();

        mInflater = LayoutInflater.from(mContext);

        initView();
    }

    /**
     * 布局设置
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        //获取传过来的本地视频路径
        Intent intent = getIntent();
        is_network_video = intent.getBooleanExtra("is_network_video", false);
        videoPath = intent.getStringExtra("selectNum");
        Log.e("shipinurl", "urlStr 55555 === " + videoPath);
        shareTitle = intent.getStringExtra("shareTitle");
        shareContent = intent.getStringExtra("shareContent");
        shareUrl = intent.getStringExtra("shareUrl");
        shareImgUrl = intent.getStringExtra("shareImgUrl");
        mainQid = intent.getStringExtra("objid");
        isProgram = intent.getStringExtra("is_program");
        progress = intent.getIntExtra("progress", 0);

        if (is_network_video) {
            mWatermake.setVisibility(View.GONE);
        } else {
            mWatermake.setVisibility(View.VISIBLE);
        }

        if (isProgram == null) {
            isProgram = "0";
        }

        Log.e(TAG, "isProgram === " + isProgram);
        if ("1".equals(isProgram)) {
            Bundle bundle = intent.getExtras();
            shareWechat = new ShareWechat();
            shareWechat.setTitle(bundle.getString("wechatTitle"));
            shareWechat.setDescription(bundle.getString("wechatDescription"));
            shareWechat.setPath(bundle.getString("wechatPath"));
            shareWechat.setThumbImage(bundle.getString("wechatThumbImage"));
            shareWechat.setUserName(bundle.getString("wechatUserName"));
            shareWechat.setWebpageUrl(bundle.getString("wechatWebpageUrl"));
        }
        //判断是否收藏
        initIfCollect();

        //开始播放
        Log.e(TAG, "mainQid === " + mainQid);
        if (mainQid != null && !"0".equals(mainQid) && !"".equals(mainQid)) {
            videoURL = true;

            getVideoInformation();

            mDialog.startLoading();
        } else {
            mDialog.startLoading();

            videoURL = false;
            mCollection.setVisibility(View.GONE);
            mShare.setVisibility(View.GONE);

            //开始播放视频
            videoPlayer();
        }

        //购物袋点击事件
        shoppingBags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (skuTao.size() != 0) {     //有sku
                    if (skuLL.getVisibility() == View.GONE) {
                        //暂停
                        mVideoView.pause();
                        videoPause.setBackgroundResource(R.drawable.video_play_2x);     //按钮状态--播放

                        skuLL.setVisibility(View.VISIBLE);          //viewPager购物袋显示
                        popskuShow.setVisibility(View.GONE);        //单个弹出sku隐藏

                        mHandler.removeMessages(PROGRESS);      //暂停视频（视频进度消息暂停）
                        mHandler.pause();           //sku弹层消息暂停
                    }
                } else {
                    Toast.makeText(VideoPlayerActivity.this, "暂无sku", Toast.LENGTH_SHORT).show();
                }

            }
        });

        //视频开始播放监听
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                Log.e("aaaaa", "1111");
                mDialog.stopLoading();
                playEnd = false;
                videoWidth = mediaPlayer.getVideoWidth();
                videoHeight = mediaPlayer.getVideoHeight();

                //设置视频横竖屏幕
                fullScreenMax = setVideoRelation();
                //获取视频时长
                duration = mediaPlayer.getDuration();
                //进度条设置
                seekbar.setMax(duration);
                //进度条监听
                seekbar.setOnSeekBarChangeListener(new VideoOnSeekBarChangeListener());
                //设置视频总进度
                endTime.setText(msConversion(duration));

                showLayer = true;
                //进度条更新
                mHandler.sendEmptyMessage(PROGRESS);

                //购物袋显示
                if (skuTao != null && skuTao.size() != 0) {     //有sku。
                    shoppingBags.setVisibility(View.VISIBLE);
                }


            }
        });

        //播放结束回调
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.e("aaaaa", "22222");
                playEnd = true;
                isRestart = false;
                startTime.setText(msConversion(duration));
                //按钮状态--播放
                videoPause.setBackgroundResource(R.drawable.video_play_2x);
                againPlay.setVisibility(View.VISIBLE);
                //视频结束后显示的sku
                setEndSku();
                mVideoContent.setBackgroundColor(Color.parseColor("#000000"));

                mHead.setVisibility(View.VISIBLE);        //头部隐藏
                mbottom.setVisibility(View.VISIBLE);      //底部隐藏
                shoppingBags.setVisibility(View.INVISIBLE); //购物袋隐藏
                skuLL.setVisibility(View.GONE);             //sku购物袋详情隐藏
                popskuShow.setVisibility(View.GONE);        //单独谈层的sku隐藏
                mHandler.removeMessages(SKU_SHOW);           //移除sku谈层显示消息
                mHandler.removeMessages(SKU_DIMSS);          //移除sku谈层隐藏消息
            }
        });

        //视频播放出错
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                videoError.setVisibility(View.VISIBLE);

                mHead.setVisibility(View.INVISIBLE);        //头部隐藏
                mbottom.setVisibility(View.INVISIBLE);      //底部隐藏
                shoppingBags.setVisibility(View.INVISIBLE); //购物袋隐藏
                skuLL.setVisibility(View.GONE);             //sku购物袋详情隐藏
                popskuShow.setVisibility(View.GONE);        //单独谈层的sku隐藏
                againPlay.setVisibility(View.GONE);          //重新播放按钮隐藏
                stopSku.setVisibility(View.GONE);               //stop后显示的sku隐藏


                return false;
            }
        });

        //播放/暂停 点击事件
        pauseClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setStartAndPause();
                againPlay.setVisibility(View.GONE);             //重新播放按钮隐藏
                stopSku.setVisibility(View.GONE);               //结束后的sku 隐藏
            }
        });

        //全屏播放
        videoMax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                haveAccelerometer = false;
                mHandler.sendEmptyMessageDelayed(HAVE_ACCELEROMETER, 1500);
                if (stretch_flag) {
                    //切换成横屏
                    VideoPlayerActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    //切换成竖屏
                    VideoPlayerActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }

                mVideoView.invalidate();
            }
        });

        //重新播放
        videoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoPlayer();
                againPlay.setVisibility(View.GONE);
                stopSku.setVisibility(View.GONE);

                //按钮状态--播放
                videoPause.setBackgroundResource(R.drawable.video_pause_2x);
            }
        });


        popskuShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jumpPageSKU(skuTao.get(posS).get_id(), "1", "0");
            }
        });


        //收藏
        mCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.isFastDoubleClick()) {
                    return;
                }

                uid = Utils.getUid();

                if (Utils.isLogin()) {
                    if (ifcollect) {
                        deleCollectHttp();
                    } else {
                        collectHttp();
                    }
                } else {
                    Intent it3 = new Intent();
                    it3.setClass(getApplicationContext(), LoginBackActivity.class);
                    startActivityForResult(it3, BACK);

                }

            }
        });

        //分享
        mShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }

                if (null != shareContent && null != shareUrl && null != shareImgUrl && shareContent.length() > 0) {
                    if (TextUtils.isEmpty(shareTitle)) {
                        shareTitle = shareContent;
                    }
                    setShare();
                }
            }
        });

        //关闭
        mClos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //mVideoContent 点击后头部和尾部 显示和隐藏
        mVideoContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mHead.getVisibility() == View.VISIBLE && mbottom.getVisibility() == View.VISIBLE) {
                    mHead.setVisibility(View.INVISIBLE);
                    mbottom.setVisibility(View.INVISIBLE);
                    shoppingBags.setVisibility(View.INVISIBLE);
                } else {
                    mHead.setVisibility(View.VISIBLE);
                    mbottom.setVisibility(View.VISIBLE);
                    if (videoURL && !playEnd) {         //网络视频 && 不是播放结束的状态
                        if (skuTao != null && skuTao.size() != 0) {     //有sku。
                            shoppingBags.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });

        //不向下分发
        mVideoContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return false;
            }
        });

        //网络中断
        mSignal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkStatus.isNetworkConnected(mContext)) {
                    mVideoView.start();
                    mVideoView.seekTo(typePosition);
                    videoPause.setBackgroundResource(R.drawable.video_pause_2x);
                    mSignal.setVisibility(View.GONE);

                    mHandler.sendEmptyMessage(PROGRESS);
                }
            }
        });

        //移动网络下点击继续播放
        continuePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mVideoView.start();
                mVideoView.seekTo(typePosition);
                videoPause.setBackgroundResource(R.drawable.video_pause_2x);
                continue_play_show.setVisibility(View.GONE);

                initReceiver();
                mHandler.sendEmptyMessage(PROGRESS);
            }
        });

    }

    private void setStartAndPause() {
        if (mVideoView.isPlaying()) {
            //暂停
            mVideoView.pause();
            //按钮状态--播放
            videoPause.setBackgroundResource(R.drawable.video_play_2x);

            mHandler.removeMessages(PROGRESS);
        } else {

            //播放
            mVideoView.start();
            //按钮-暂停
            videoPause.setBackgroundResource(R.drawable.video_pause_2x);

            mHandler.sendEmptyMessage(PROGRESS);

            //购物袋显示
            if (skuTao != null && skuTao.size() != 0) {     //有sku。
                shoppingBags.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * 视频结束后sku数据添加
     */
    private void setEndSku() {
        //如果有sku，判断有几个显示几个
        if (skuTao != null && skuTao.size() > 0) {
            stopContainer1.removeAllViews();
            stopContainer2.removeAllViews();

            stopSku.setVisibility(View.VISIBLE);
            if (skuTao.size() == 1) {
                setViewPagerView(stopContainer1, skuTao.get(0));

                //主要是占位。
                setViewPagerView(stopContainer1, skuTao.get(0));
                View at = stopContainer1.getChildAt(1);
                at.setVisibility(View.INVISIBLE);
                if (!stretch_flag) {                 //横屏
                    //主要是占位。
                    setViewPagerView(stopContainer1, skuTao.get(0));
                    View at1 = stopContainer1.getChildAt(2);
                    at1.setVisibility(View.INVISIBLE);
                }

            } else if (skuTao.size() == 2) {

                setViewPagerView(stopContainer1, skuTao.get(0));
                setViewPagerView(stopContainer1, skuTao.get(1));

            } else if (skuTao.size() == 3) {
                setViewPagerView(stopContainer1, skuTao.get(0));
                setViewPagerView(stopContainer1, skuTao.get(1));

                if (stretch_flag) {                 //竖屏
                    setViewPagerView(stopContainer2, skuTao.get(2));
                    stopContainer2.setVisibility(View.VISIBLE);

                    //主要是占位。
                    setViewPagerView(stopContainer2, skuTao.get(2));
                    View at = stopContainer2.getChildAt(1);
                    at.setVisibility(View.INVISIBLE);

                } else {                            //横屏
                    setViewPagerView(stopContainer1, skuTao.get(2));
                    stopContainer2.setVisibility(View.GONE);
                }

            }
        }
    }


    /**
     * 跳转到SKU页面
     *
     * @param id
     * @param source
     * @param objid
     */
    private void jumpPageSKU(String id, String source, String objid) {
        Intent it = new Intent();
        it.putExtra("id", id);
        it.putExtra("source", source);
        it.putExtra("objid", objid);
        it.setClass(mContext, TaoDetailActivity.class);
        startActivity(it);
    }

    /**
     * 获取网络视频信息
     */
    private void getVideoInformation() {
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("uid", uid);

        keyValues.put("id", mainQid);

        new GetVideoInformationApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    Log.e(TAG, "serverData.data == " + serverData.data);

                    try {
                        VideoPlay.DataBean dataBean = JSONUtil.TransformVideoPlay(serverData.data);
                        skuTao = dataBean.getTao();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (skuTao == null || skuTao.size() == 0) {     //无sku，购物袋隐藏
                        shoppingBags.setVisibility(View.INVISIBLE);
                    } else {
                        shoppingBags.setVisibility(View.VISIBLE);

                        //开启sku关联
                        setSkuShoppingBags();
                    }

                    //开启网络监听
                    initReceiver();

                    //开始播放视频
                    videoPlayer();
                }
            }

        });

    }

    /**
     * 设置sku购物袋
     */
    private void setSkuShoppingBags() {
        mNumLayout.removeAllViews();
        skuPager.setAdapter(new SkuPagerAdapter(skuTao));

        if (stretch_flag) {                             //竖屏的
            int page = skuTao.size() == 3 ? 2 : 1;
            if (page == 2) {                          //如果是两页，显示白点
                for (int i = 0; i < page; i++) {
                    Button bt = new Button(mContext);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DensityUtil.dip2px(10), DensityUtil.dip2px(10));
                    params.setMargins(DensityUtil.dip2px(5), 0, DensityUtil.dip2px(5), 0);            //设置圆点的宽高和左边距
                    bt.setLayoutParams(params);
                    if (i == 0) {
                        bt.setBackgroundResource(R.drawable.home_page_dot_select);
                        mPreSelectedBt = bt;
                    } else {
                        bt.setBackgroundResource(R.drawable.icon_dot_normal);
                    }
                    mNumLayout.addView(bt);
                }

            }
        }

        skuPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (mPreSelectedBt != null) {
                    mPreSelectedBt.setBackgroundResource(R.drawable.icon_dot_normal);
                }

                Button currentBt = (Button) mNumLayout.getChildAt(position);
                currentBt.setBackgroundResource(R.drawable.home_page_dot_select);
                mPreSelectedBt = currentBt;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //购物袋关闭按钮
        shopSkuClos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (skuLL.getVisibility() == View.VISIBLE) {
                    //播放
                    mVideoView.start();
                    //按钮-暂停
                    videoPause.setBackgroundResource(R.drawable.video_pause_2x);

                    skuLL.setVisibility(View.GONE);

                    //视频时间大于5,且有sku关联
                    Log.e(TAG, "currentPosition == " + currentPosition);
                    Log.e(TAG, "skuTao.size() == " + skuTao.size());
                    Log.e(TAG, "skuShowComplete == " + skuShowComplete);
                    if (currentPosition >= 5000 && skuTao.size() > 0) {
                        if (skuShowComplete) {
                            Log.e(TAG, "popskuShow --- > GONE");
                            popskuShow.setVisibility(View.GONE);        //单个弹出sku隐藏
                        } else {
                            Log.e(TAG, "popskuShow --- > VISIBLE");
                            popskuShow.setVisibility(View.VISIBLE);        //单个弹出sku显示
                        }
                    }

                    mHandler.sendEmptyMessageDelayed(PROGRESS, 1);     //开始播放（播放进度消息开启）
                    mHandler.resume();      //sku弹层消息开启
                }
            }
        });
    }

    /**
     * SKU显示的适配器
     */
    class SkuPagerAdapter extends PagerAdapter {

        private final List<VideoPlay.DataBean.TaoBean> mTao;
        private final LayoutInflater mInflater;

        public SkuPagerAdapter(List<VideoPlay.DataBean.TaoBean> tao) {
            this.mTao = tao;
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            if (stretch_flag) {                             //竖屏的
                return mTao.size() == 3 ? 2 : 1;
            } else {
                return 1;
            }
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = mInflater.inflate(R.layout.video_sku_show, null, false);
            LinearLayout showContainer = view.findViewById(R.id.video_sku_show);
            showContainer.removeAllViews();

            if (stretch_flag) {                             //竖屏的
                if (position == 0) {                        //第一页
                    if (mTao.size() == 1) {                 //如果只有一个sku
                        setViewPagerView(showContainer, skuTao.get(0));

                        //占位
                        mInflater.inflate(R.layout.item_video_stop_sku, showContainer);
                        showContainer.getChildAt(1).setVisibility(View.INVISIBLE);
                    }
                    if (mTao.size() >= 2) {                 //如果有两个及以上sku
                        setViewPagerView(showContainer, skuTao.get(0));
                        setViewPagerView(showContainer, skuTao.get(1));
                    }
                } else {                                    //第二页

                    setViewPagerView(showContainer, skuTao.get(2));

                    //占位
                    mInflater.inflate(R.layout.item_video_stop_sku, showContainer);
                    showContainer.getChildAt(1).setVisibility(View.INVISIBLE);
                }
            } else {                                     //横屏
                if (mTao.size() == 1) {

                    setViewPagerView(showContainer, skuTao.get(0));
                    //占位
                    mInflater.inflate(R.layout.item_video_stop_sku, showContainer);
                    showContainer.getChildAt(1).setVisibility(View.INVISIBLE);
                    //占位
                    mInflater.inflate(R.layout.item_video_stop_sku, showContainer);
                    showContainer.getChildAt(2).setVisibility(View.INVISIBLE);
                }

                if (mTao.size() == 2) {
                    setViewPagerView(showContainer, skuTao.get(0));
                    setViewPagerView(showContainer, skuTao.get(1));
                    //占位
                    mInflater.inflate(R.layout.item_video_stop_sku, showContainer);
                    showContainer.getChildAt(2).setVisibility(View.INVISIBLE);
                }

                if (mTao.size() == 3) {
                    setViewPagerView(showContainer, skuTao.get(0));
                    setViewPagerView(showContainer, skuTao.get(1));
                    setViewPagerView(showContainer, skuTao.get(2));
                }
            }

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        }

    }

    /**
     * sku的item
     *
     * @param showContainer :容器
     * @param mTao：数据
     */
    private void setViewPagerView(LinearLayout showContainer, final VideoPlay.DataBean.TaoBean mTao) {
        View view = mInflater.inflate(R.layout.item_video_stop_sku, null);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        lp.setMargins(Utils.dip2px(mContext, 5), 0, Utils.dip2px(mContext, 5), 0);
        view.setLayoutParams(lp);

        ImageView imgSKU = view.findViewById(R.id.iv_video_img);
        TextView titkeSKU = view.findViewById(R.id.iv_video_title);
        TextView hosSKU = view.findViewById(R.id.iv_video_hos);
        TextView priceSKU = view.findViewById(R.id.iv_video_price);

//        Picasso.with(mContext).load(mTao.getImg()).placeholder(R.drawable.radius_gray80).error(R.drawable.radius_gray80).into(imgSKU);
        Glide.with(mContext).load(mTao.getImg()).placeholder(R.drawable.radius_gray80).error(R.drawable.radius_gray80).into(imgSKU);
        titkeSKU.setText(mTao.getTitle());
        hosSKU.setText(mTao.getHos_name());
        priceSKU.setText("￥" + mTao.getPrice_discount());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jumpPageSKU(mTao.get_id(), "1", "0");
            }
        });

        showContainer.addView(view);
    }

    /**
     * 开始播放视频
     */
    private void videoPlayer() {
        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEOCLICK, "1"));

        Uri uri = Uri.parse(videoPath);
        //默认隐藏videoView自带的进度条和暂停按钮
        MediaController mc = new MediaController(this);
        mc.setVisibility(View.GONE);
        mVideoView.setMediaController(mc);
        mVideoView.setVideoURI(uri);

        if (progress != 0) {
            mVideoView.seekTo(progress);
            progress = 0;
        }
        mVideoView.start();
        mVideoView.requestFocus();
    }


    /**
     * 把毫秒转化为 mm:ss格式
     *
     * @return
     */
    private String msConversion(int ms) {
        String results;

        int seconds = ms / 1000;            //多少秒
        int points = seconds / 60;          //多少分
        int remaMs = seconds % 60;          //总数 - 分钟 = 剩余秒数

        if (points == 0) {        //时间不超过一分钟
            String s = (seconds < 10) ? "0" + seconds : seconds + "";
            results = "00:" + s;
        } else {             //时间超过一分钟
            String s = (points < 10) ? "0" + points : points + "";

            String s1 = (remaMs < 10) ? "0" + remaMs : remaMs + "";
            results = s + ":" + s1;
        }

        return results;
    }

    /**
     * 判断视频横竖屏
     */
    private boolean setVideoRelation() {

        videoWidth = DensityUtil.dip2px(videoWidth);             //dp转换为px
        videoHeight = DensityUtil.dip2px(videoHeight);

        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {        //切换成横屏
            if (videoWidth > videoHeight) {                       //视频宽》高的
                mWidth = windowsWight;
                mHeight = mWidth * videoHeight / videoWidth;
                mVideoView.setVideoSize(mWidth, mHeight);

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                mVideoView.setLayoutParams(layoutParams);
                return true;
            } else {

                mHeight = windowsWight;
                mWidth = mHeight * videoWidth / videoHeight;

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(mWidth, mHeight);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT);
                mVideoView.setLayoutParams(lp);
                return false;
            }

        } else if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {      //切换成竖屏
            Log.e(TAG, "竖屏");

            if (videoWidth > videoHeight) {                       //视频宽》高的
                mWidth = windowsWight;
                mHeight = mWidth * videoHeight / videoWidth;
                mVideoView.setVideoSize(mWidth, mHeight);

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(mWidth, mHeight);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT);
                mVideoView.setLayoutParams(lp);
                return true;
            } else {
                mHeight = windowsWight;
                mWidth = mHeight * videoWidth / videoHeight;

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(videoWidth, videoHeight);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT);
                mVideoView.setLayoutParams(lp);
                return false;
            }
        } else {
            return true;
        }
    }

    class VideoOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        /**
         * 当SeekBar 改变的时候回调这个方法
         *
         * @param seekBar
         * @param progress
         * @param fromUser 自动false,用户操作true
         */
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            if (fromUser) {     //只响应用户操作
                mVideoView.seekTo(progress);
            }

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    /**
     * 横竖屏切换
     */
    private void HorizontalPortraitSwitching() {
        //注册重力感应器  屏幕旋转
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        listener = new OrientationSensorListener();


        //根据  旋转之后 点击 符合之后 激活sm
        sm1 = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor1 = sm1.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        listener1 = new OrientationSensorListener2();
        sm1.registerListener(listener1, sensor1, SensorManager.SENSOR_DELAY_UI);
    }

    /**
     * 在屏幕发生旋转变化时
     *
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {        //切换成横屏
            Log.e(TAG, "横屏");

            stretch_flag = false;//改变全屏
            if (playEnd) {
                setEndSku();        //停止时sku的样式改变
            }
            setSkuShoppingBags();   //暂停时sku样式的变化
            if (fullScreenMax) {                       //视频宽》高的
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                mVideoView.setLayoutParams(layoutParams);
            } else {
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(mWidth, mHeight);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT);
                mVideoView.setLayoutParams(lp);
            }

        } else if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {      //切换成竖屏
            Log.e(TAG, "竖屏");

            stretch_flag = true;//窗口的标记
            if (playEnd) {
                setEndSku();        //停止时sku的样式改变
            }
            setSkuShoppingBags();   //暂停时sku样式的变化

            if (fullScreenMax) {                       //视频宽》高的

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(mWidth, mHeight);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT);
                mVideoView.setLayoutParams(lp);

            } else {

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(videoWidth, videoHeight);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT);
                mVideoView.setLayoutParams(lp);

            }

        }
    }


    /**
     * 重力感应监听者
     */
    public class OrientationSensorListener implements SensorEventListener {
        private static final int _DATA_X = 0;
        private static final int _DATA_Y = 1;
        private static final int _DATA_Z = 2;

        public static final int ORIENTATION_UNKNOWN = -1;


        public OrientationSensorListener() {
        }

        public void onAccuracyChanged(Sensor arg0, int arg1) {
        }

        public void onSensorChanged(SensorEvent event) {
            if (sensor_flag != stretch_flag) {           //只有两个不相同才开始监听行为
                float[] values = event.values;
                int orientation = ORIENTATION_UNKNOWN;
                float X = -values[_DATA_X];
                float Y = -values[_DATA_Y];
                float Z = -values[_DATA_Z];
                float magnitude = X * X + Y * Y;
                // 不要相信角度如果大小和y值相比是很小的
                if (magnitude * 4 >= Z * Z) {
                    //屏幕旋转时
                    float OneEightyOverPi = 57.29577957855f;
                    float angle = (float) Math.atan2(-Y, X) * OneEightyOverPi;
                    orientation = 90 - Math.round(angle);
                    // normalize to 0 - 359 range
                    while (orientation >= 360) {
                        orientation -= 360;
                    }
                    while (orientation < 0) {
                        orientation += 360;
                    }
                }
                if (mHandler != null && haveAccelerometer) {
                    mHandler.obtainMessage(SOMEHOW_SCREEN, orientation, 0).sendToTarget();
                }

            }
        }
    }

    //网络状态监听广播
    private void initReceiver() {
        netReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(TAG, "aaaaaaaaaa");
                if (intent.getIntExtra(NetWorkChangeBroadcastReceiver.NET_TYPE, 0) == 0) {       //判断当前网络状态.0表示无网络连接,1表示有
                    Log.e(TAG, "bbbbbbb");
                    mVideoView.pause();
                    videoPause.setBackgroundResource(R.drawable.video_play_2x);
                    mHandler.removeMessages(PROGRESS);

                    mSignal.setVisibility(View.VISIBLE);
                } else if (intent.getIntExtra(NetWorkChangeBroadcastReceiver.NET_TYPE, 0) == 2) {      //处于移动网络
                    Log.e(TAG, "ccccc");
                    mVideoView.pause();
                    videoPause.setBackgroundResource(R.drawable.video_play_2x);
                    mHandler.removeMessages(PROGRESS);

                    mSignal.setVisibility(View.GONE);
                    if (editDialog == null || !editDialog.isShowing()) {
                        showDialogExitEdit3();
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter(NetWorkChangeBroadcastReceiver.NET_CHANGE);
        mContext.registerReceiver(netReceiver, filter);
    }

    /**
     * 当前不是wifi网络提示
     */
    void showDialogExitEdit3() {
        editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_phone_zhuan);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
        titleTv77.setText("非wifi提示");

        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("您当前处于非wifi下，确定继续浏览该视频？");

        titleTv88.setHeight(50);

        //确定
        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText("继续播放");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mVideoView.start();
                mVideoView.seekTo(typePosition);
                videoPause.setBackgroundResource(R.drawable.video_pause_2x);

                mHandler.sendEmptyMessage(PROGRESS);

                if (netReceiver != null) {
                    mContext.unregisterReceiver(netReceiver);
                }

                editDialog.dismiss();
            }
        });

        //取消
        Button cancelBt99 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt99.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();

                continue_play_show.setVisibility(View.VISIBLE);

                if (netReceiver != null) {
                    mContext.unregisterReceiver(netReceiver);
                }
            }
        });
    }


    public class OrientationSensorListener2 implements SensorEventListener {
        private static final int _DATA_X = 0;
        private static final int _DATA_Y = 1;
        private static final int _DATA_Z = 2;

        public static final int ORIENTATION_UNKNOWN = -1;

        public void onAccuracyChanged(Sensor arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        public void onSensorChanged(SensorEvent event) {

            float[] values = event.values;

            int orientation = ORIENTATION_UNKNOWN;
            float X = -values[_DATA_X];
            float Y = -values[_DATA_Y];
            float Z = -values[_DATA_Z];

            /**
             * 这一段据说是 android源码里面拿出来的计算 屏幕旋转的 不懂 先留着 万一以后懂了呢
             */
            float magnitude = X * X + Y * Y;
            // Don't trust the angle if the magnitude is small compared to the y value
            if (magnitude * 4 >= Z * Z) {
                //屏幕旋转时
                float OneEightyOverPi = 57.29577957855f;
                float angle = (float) Math.atan2(-Y, X) * OneEightyOverPi;
                orientation = 90 - Math.round(angle);
                // normalize to 0 - 359 range
                while (orientation >= 360) {
                    orientation -= 360;
                }
                while (orientation < 0) {
                    orientation += 360;
                }
            }

            if (orientation > 225 && orientation < 315) {  //横屏
                sensor_flag = false;
            } else if ((orientation > 315 && orientation < 360) || (orientation > 0 && orientation < 45)) {  //竖屏
                sensor_flag = true;
            }

            if (stretch_flag == sensor_flag) {  //点击变成横屏  屏幕 也转横屏 激活
                sm.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI);
            }
        }
    }

    /**
     * 设置分享
     */
    private void setShare() {
        BaseShareView baseShareView = new BaseShareView(mContext);
        baseShareView.setShareContent(shareTitle);
        if ("1".equals(isProgram)) {
            baseShareView.ShareAction(shareWechat);
        } else {
            baseShareView.ShareAction();
        }

        baseShareView.getShareBoardlistener().setSinaText("#整形#" + shareContent + shareUrl + "分享自@悦美整形APP").setSinaThumb(new UMImage(mContext, shareImgUrl)).setSmsText(shareContent + "，" + shareUrl).setTencentUrl(shareUrl).setTencentTitle(shareContent).setTencentThumb(new UMImage(mContext, shareImgUrl)).setTencentDescription(shareContent).setTencentText(shareContent).getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {

                if (!platform.equals(SHARE_MEDIA.SMS)) {
                    Toast.makeText(mContext, " 分享成功啦", Toast.LENGTH_SHORT).show();
                    uid = Utils.getUid();
                    if (Utils.isLogin()) {
                        sumitHttpCode("9");
                    }
                }
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

            }
        });
    }


    /**
     * 获取颜值币
     *
     * @param flag
     */
    void sumitHttpCode(final String flag) {
        SumitHttpAip sumitHttpAip = new SumitHttpAip();
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", flag);
        maps.put("uid", uid);
        sumitHttpAip.getCallBack(mContext, maps, new BaseCallBackListener<JFJY1Data>() {
            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                if (jfjyData != null) {
                    String jifenNu = jfjyData.getIntegral();
                    String jyNu = jfjyData.getExperience();

                    Log.e("TAG", "jifenNu == " + jifenNu);
                    Log.e("TAG", "jyNu == " + jyNu);
                    if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                        MyToast.makeTexttext4Toast(mContext, jifenNu, jyNu, SHOW_TIME).show();
                    } else {
                        if (!jifenNu.equals("0")) {
                            MyToast.makeTexttext2Toast(mContext, jifenNu, SHOW_TIME).show();
                        } else {
                            if (!jyNu.equals("0")) {
                                MyToast.makeTexttext3Toast(mContext, jyNu, SHOW_TIME).show();
                            }
                        }
                    }
                }
            }
        });

    }

    /**
     * 判断是否收藏
     */
    void initIfCollect() {

        if (Utils.isLogin() && null != mainQid) {
            Map<String, Object> maps = new HashMap<>();
            maps.put("uid", uid);
            maps.put("objid", mainQid);
            maps.put("type", "2");
            new IsCollectApi().getCallBack(mContext, maps, new BaseCallBackListener<String>() {
                @Override
                public void onSuccess(String s) {
                    if (s.equals("1")) {
                        ifcollect = true;
                        mCollection.setImageResource(R.drawable.video_collection_yes);
                    } else {
                        ifcollect = false;
                        mCollection.setImageResource(R.drawable.video_collection_no);
                    }
                }
            });

        }

    }


    /**
     * 收藏接口
     */
    void collectHttp() {
        BaikeFourApi baikeFourApi = new BaikeFourApi();
        Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        params.put("objid", mainQid);
        params.put("type", "2");
        baikeFourApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                ifcollect = true;
                mCollection.setImageResource(R.drawable.video_collection_yes);
                if (!serverData.isOtherCode) {
                    MyToast.makeImgAndTextToast(mContext, getResources().getDrawable(R.drawable.tips_smile), "收藏成功", SHOW_TIME).show();

                }
            }
        });
    }


    /**
     * 取消收藏
     */
    void deleCollectHttp() {
        CancelCollectApi cancelCollectApi = new CancelCollectApi();
        Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        params.put("objid", mainQid);
        params.put("type", "2");
        cancelCollectApi.getCallBack(mContext, params, new BaseCallBackListener() {
            @Override
            public void onSuccess(Object o) {
                ifcollect = false;
                mCollection.setImageResource(R.drawable.video_collection_no);
                MyToast.makeImgAndTextToast(mContext, getResources().getDrawable(R.drawable.tips_smile), "取消收藏", SHOW_TIME).show();

            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        isPlaying = mVideoView.isPlaying();
        mHandler.removeMessages(PROGRESS);                                      //销毁视频进度消息
        mVideoView.pause();

        typePosition = mVideoView.getCurrentPosition();
        Log.e(TAG, "isPlaying111 === " + isPlaying);
        Log.e(TAG, "typePosition111 === " + typePosition);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        isRestart = true;
        if (mainQid != null && mainQid.length() > 0) {                  //如果是网络视频
            if (NetworkStatus.isNetworkConnected(mContext)) {                     //如果有网络
                Log.e(TAG, "isPlaying333 === " + isPlaying);
                Log.e(TAG, "typePosition333 === " + typePosition);
                if (isPlaying) {
                    mVideoView.start();
                    videoPause.setBackgroundResource(R.drawable.video_pause_2x);
                    mVideoView.seekTo(typePosition);
                    mHandler.sendEmptyMessage(PROGRESS);
                } else {
                    mVideoView.pause();
                    videoPause.setBackgroundResource(R.drawable.video_play_2x);
                    mVideoView.seekTo(typePosition);
                }

                mSignal.setVisibility(View.GONE);


            } else {                                                                //没有网络

                mSignal.setVisibility(View.VISIBLE);

            }
        } else {                                                         //否则为本地视频
            mVideoView.start();
            videoPause.setBackgroundResource(R.drawable.video_pause_2x);
            mVideoView.seekTo(typePosition);
            mHandler.sendEmptyMessage(PROGRESS);
            mSignal.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (netReceiver != null) {
            mContext.unregisterReceiver(netReceiver);               //注销广播
        }
        mHandler.removeMessages(PROGRESS);
        mHandler.removeMessages(SOMEHOW_SCREEN);
        mHandler.removeMessages(SKU_SHOW);
        mHandler.removeMessages(SKU_DIMSS);
        mHandler.removeMessages(HAVE_ACCELEROMETER);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("current_position", mVideoView.getCurrentPosition());
        setResult(RETURN_VIDEO_PROGRESS, intent);
        super.onBackPressed();
    }

}
