package com.quicklyask.entity;

import java.util.List;

public class SlidePicQid {

	private String code;

	private String message;

	private List<SlidePicQidData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<SlidePicQidData> getData() {
		return data;
	}

	public void setData(List<SlidePicQidData> data) {
		this.data = data;
	}


}
