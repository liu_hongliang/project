package com.module.community.controller.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.recyclerlodemore.LoadMoreRecyclerView;
import com.module.base.view.YMBaseFragment;
import com.module.community.controller.adapter.CommunityFragmentLabelAdapter;
import com.module.community.controller.api.CommunityFragmentApi;
import com.module.community.model.bean.HomeCommunityTagData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.adapter.ProjectAnswerAdapter;
import com.module.home.model.bean.QuestionListData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

/**
 * 社区问答列表模版
 * Created by 裴成浩 on 2018/7/10.
 */
public class CommunityAnswerFragment extends YMBaseFragment {
    @BindView(R.id.community_answer_refresh)
    SmartRefreshLayout mPullRefresh;
    @BindView(R.id.community_answer_label_recycler)
    RecyclerView mLabelRecycler;
    @BindView(R.id.community_answer_list_recycler)
    LoadMoreRecyclerView mAnswerList;
    private ProjectAnswerAdapter mProjectAnswerAdapter;
    private HomeCommunityTagData mData;
    private CommunityFragmentApi staggeredApi;
    private String TAG = "CommunityAnswerFragment";
    private ArrayList<QuestionListData> mAnswerData;
    private int mPage = 1;
    private CommunityFragmentLabelAdapter mCommunityFragmentAdapter;

    public static CommunityAnswerFragment newInstance(HomeCommunityTagData data) {
        CommunityAnswerFragment fragment = new CommunityAnswerFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mData = getArguments().getParcelable("data");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_answer_community;
    }

    @Override
    protected void initView(View view) {

        if (mData.getTag().size() > 0) {
            mLabelRecycler.setVisibility(View.VISIBLE);
            mCommunityFragmentAdapter = new CommunityFragmentLabelAdapter(mContext, mData.getTag());
            mLabelRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            ((DefaultItemAnimator) mLabelRecycler.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果
            mLabelRecycler.setAdapter(mCommunityFragmentAdapter);

            mCommunityFragmentAdapter.setOnItemCallBackListener(new CommunityFragmentLabelAdapter.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    mPage = 1;
                    loadingData(true);
                }
            });
        } else {
            mLabelRecycler.setVisibility(View.GONE);
        }

        mPullRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mPage = 1;
                loadingData(true);
            }
        });

        //加载更多
        mAnswerList.setLoadMoreListener(new LoadMoreRecyclerView.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e(TAG, "onLoadMore........");
                loadingData(false);
            }
        });

    }

    @Override
    protected void initData(View view) {
        staggeredApi = new CommunityFragmentApi(mData.getController(), mData.getAction());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mAnswerList.setLayoutManager(linearLayoutManager);

        loadingData(true);
    }

    /**
     * 加载数据
     */
    private void loadingData(final boolean isRefresh) {
        staggeredApi.addData("page", mPage + "");
        staggeredApi.addData("id", mData.getId() + "");
        if (mCommunityFragmentAdapter != null && mCommunityFragmentAdapter.getmData().size() > 0) {
            Log.e(TAG, "二级筛选id === " + mCommunityFragmentAdapter.getmData().get(mCommunityFragmentAdapter.findSelected()).getPartId());
            staggeredApi.addData("partId", mCommunityFragmentAdapter.getmData().get(mCommunityFragmentAdapter.findSelected()).getPartId() + "");
        }
        staggeredApi.getCallBack(mContext, staggeredApi.getHashMap(), new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String json) {
                Log.e(TAG, "json === " + json);
                mPullRefresh.finishRefresh();
                mPage++;

                mAnswerData = JSONUtil.jsonToArrayList(json, QuestionListData.class);
                Log.e(TAG, "mAnswerData == " + mAnswerData.size());

                if (isRefresh && mProjectAnswerAdapter != null) {
                    mProjectAnswerAdapter.clearData();
                }

                setRecyclerData(mAnswerData);
            }
        });
    }


    /**
     * 设置列表数据
     *
     * @param questionList
     */
    private void setRecyclerData(ArrayList<QuestionListData> questionList) {
        if (mProjectAnswerAdapter == null) {
            mAnswerList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            mProjectAnswerAdapter = new ProjectAnswerAdapter(mContext, questionList,"");
            mAnswerList.setAdapter(mProjectAnswerAdapter);
            //item点击事件
            mProjectAnswerAdapter.setOnItemClickListener(new ProjectAnswerAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int pos, QuestionListData data) {
                    String jumpUrl = data.getJumpUrl();
                    HashMap<String, String> event_params = mProjectAnswerAdapter.getDatas().get(pos).getEvent_params();
                    if (!TextUtils.isEmpty(jumpUrl)) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBSHOMECLICK, "bbs|list_" + mData.getTongjiid() + "|" + (pos + 1) + "|" + FinalConstant1.MARKET + "|" + FinalConstant1.DEVICE), event_params);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(jumpUrl);
                    }
                }
            });
        } else {
            mProjectAnswerAdapter.addData2(mAnswerData);
        }

        if (questionList.size() < 15) {
            mAnswerList.setNoMore(true);
        } else {
            mAnswerList.loadMoreComplete();
        }

    }
}
