package com.quicklyask.view;

/**
 * @author harvic
 * @address blog.csdn.net/harvic880925
 * @date 2014-12-17
 */

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TimerTextView extends AppCompatTextView implements Runnable {

    private String copywriting;
    private long[] times;

    public TimerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private long mday, mhour, mmin, msecond;// 天，小时，分钟，秒

    private boolean run = false; // 是否启动了

    public void setTimes(long[] times) {
        this.times = times;
        mday = times[0];
        mhour = times[1];
        mmin = times[2];
        msecond = times[3];
    }

    public long[] getTimes() {
        return times;
    }

    public void setCopywriting(String copywriting) {
        this.copywriting = copywriting;
    }

    /**
     * 倒计时计算
     */
    private void ComputeTime() {
        msecond--;
        if (msecond < 0) {
            mmin--;
            msecond = 59;
            if (mmin < 0) {
                mmin = 59;
                mhour--;
                if (mhour < 0) {
                    // 倒计时结束，一天有24个小时
                    mhour = 23;
                    mday--;
                }
            }

        }

    }


    public boolean isRun() {
        return run;
    }

    public void beginRun() {
        this.run = true;
        run();
    }

    public void stopRun() {
        this.run = false;
    }

    @Override
    public void run() {
        // 标示已经启动
        if (run) {
            ComputeTime();

            // String mdayStr;
            String mhourStr = null;
            String mminStr = null;
            String msecondStr = null;

            if (msecond < 10) {
                msecondStr = "0" + msecond;
            } else {
                msecondStr = "" + msecond;
            }
            if (mmin < 10) {
                mminStr = "0" + mmin;
            } else {
                mminStr = "" + mmin;
            }
            if (mhour < 10) {
                mhourStr = "0" + mhour;
            } else {
                mhourStr = "" + mhour;
            }

            String strTime;
            if (mday == 0) {
                strTime = copywriting + mhourStr + ":" + mminStr + ":" + msecondStr;
            } else {
                strTime = copywriting + mday + "天  " + mhourStr + ":" + mminStr + ":" + msecondStr;
            }

            this.setText(strTime);

            times[0] = mday;
            times[1] = mhour;
            times[2] = mmin;
            times[3] = msecond;
            if (mday == 0 && mhour == 0 && mmin == 0 && msecond == 0) {
                stopRun();
                if (timeCallBack != null){
                    timeCallBack.timeCallBack();
                }
            } else {
                postDelayed(this, 1000);
            }

        } else {
            removeCallbacks(this);
        }
    }

    public void setTimeCallBack(TimeCallBack timeCallBack) {
        this.timeCallBack = timeCallBack;
    }

    private TimeCallBack timeCallBack;
    public interface TimeCallBack{
        void timeCallBack();
    }

}
