package com.module.doctor.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.module.base.api.BaseCallBackListener;
import com.module.doctor.controller.adapter.ProjectHlistAdpter;
import com.module.doctor.model.api.LodGroupDataApi;
import com.module.doctor.model.bean.GroupDiscData;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.activity.interfaces.Project2ListSelectListener;
import com.quicklyask.util.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProjectDetailListFragment extends ListFragment {

    public static final String TAG = "ProjectDetailList";
    private String pid;
    private String z_pid;
    private List<GroupDiscData> lvGroupData = new ArrayList<>();
    private Handler mHandler;
    private ListView mlist;
    private ProjectHlistAdpter discAdapter;
    private GroupDiscData itemData = new GroupDiscData();
    public static int mmPosition;

    // 提供给外的接口
    private Project2ListSelectListener menuViewOnSelectListener;

    private static ProjectDetailListFragment instance = null;
    private LoadingProgress mDialog;

    // 单例模式
    public static ProjectDetailListFragment getInstance() {
        if (instance == null) {
            instance = new ProjectDetailListFragment();
        }
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ffragment_list, null);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
        super.onActivityCreated(savedInstanceState);
        mlist = getListView();
        mDialog = new LoadingProgress(getActivity());
        if (isAdded()) {
            pid = getArguments().getString("id");
            z_pid = getArguments().getString("z_id");
        }
        mHandler = getHandler();
        lodGroupData();

        mlist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                itemData = lvGroupData.get(pos);

                if (menuViewOnSelectListener != null) {
                    menuViewOnSelectListener.getValue(itemData);
                }
            }

        });
    }

    public void setCascadingMenuViewOnSelectListener(
            Project2ListSelectListener onSelectListener) {
        menuViewOnSelectListener = onSelectListener;
    }

    void lodGroupData() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                HashMap<String, Object> maps = new HashMap<>();
                maps.put("id", pid);
                maps.put("type", "3");
                new LodGroupDataApi().getCallBack(getActivity(), maps, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData serverData) {
                        if ("1".equals(serverData.code)) {
                            lvGroupData = JSONUtil.jsonToArrayList(serverData.data, GroupDiscData.class);
                            Message message = mHandler.obtainMessage(1);
                            message.sendToTarget();
                        }
                    }
                });

            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler(Looper.getMainLooper()) {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (null != lvGroupData && lvGroupData.size() > 0) {
                            mDialog.stopLoading();

                            String ids = "0";
                            for (int i = 0; i < lvGroupData.size(); i++) {

                                ids = lvGroupData.get(i).get_id();

                                if (z_pid.equals(ids)) {
                                    mmPosition = i;
                                    break;
                                } else {
                                    mmPosition = -1;
                                }
                            }

                            try {
                                if (null != getActivity()) {
                                    discAdapter = new ProjectHlistAdpter(getActivity(), lvGroupData);
                                    mlist.setAdapter(discAdapter);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            mDialog.stopLoading();
                        }
                        break;
                }

            }
        };
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }
}
