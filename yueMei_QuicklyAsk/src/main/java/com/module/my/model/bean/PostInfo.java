package com.module.my.model.bean;

public class PostInfo {

	private String code;
	private String message;

	private PostInfoData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PostInfoData getData() {
		return data;
	}

	public void setData(PostInfoData data) {
		this.data = data;
	}

}
