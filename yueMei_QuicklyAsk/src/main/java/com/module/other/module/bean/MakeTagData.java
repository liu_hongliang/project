package com.module.other.module.bean;

import java.util.List;

/**
 * Created by 裴成浩 on 2017/6/30.
 */

public class MakeTagData {
    private String id;
    private String name;
    private String level;
    private String channel_title;
    private String img;
    private List<MakeTagListData> list;
    private boolean isSelected = false;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<MakeTagListData> getList() {
        return list;
    }

    public void setList(List<MakeTagListData> list) {
        this.list = list;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getChannel_title() {
        return channel_title;
    }

    public void setChannel_title(String channel_title) {
        this.channel_title = channel_title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
