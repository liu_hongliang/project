package com.module.commonview.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.commonview.adapter.ButtomDialogAdapter;
import com.module.commonview.module.bean.ButtomDialogBean;
import com.module.commonview.module.bean.ButtomDialogEnum;
import com.module.commonview.view.share.MyShareBoardlistener;
import com.quicklyask.activity.R;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义分享控制面板2
 * Created by 裴成浩 on 2018/5/16.
 */
public class ButtomDialogView extends Dialog {

    private Context context;
    private MyShareBoardlistener myShareBoardlistener;
    private boolean iscancelable;                                           //控制点击dialog外部是否dismiss
    private boolean isBackCancelable;                                       //控制返回键是否dismiss
    private List<ButtomDialogBean> buttomDialogBeans;   //数据
    private LinearLayout dialogTip;

    private ButtomDialogView(Context context, Builder builder){
        super(context, R.style.MyDialog1);
        this.context = builder.context;
        this.myShareBoardlistener = builder.myShareBoardlistener;
        this.iscancelable = builder.iscancelable;
        this.isBackCancelable = builder.isBackCancelable;
        this.buttomDialogBeans = builder.buttomDialogBeans;
        initData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    private void initView() {
        View view = LayoutInflater.from(context).inflate(R.layout.pop_button_dialog2, null, false);

        setContentView(view);           //这行一定要写在前面
        onClickListener(view);

        setCancelable(iscancelable);    //点击外部不可dismiss
        setCanceledOnTouchOutside(isBackCancelable);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
    }

    /**
     * 初始化数据
     */
    private void initData() {
        if(buttomDialogBeans.size() == 0){
            ButtomDialogBean buttomDialogBean1 = new ButtomDialogBean(ButtomDialogEnum.HUABAO, R.drawable.umeng_socialize_huabao, "悦美画报");
            ButtomDialogBean buttomDialogBean2 = new ButtomDialogBean(ButtomDialogEnum.WEIXIN, R.drawable.umeng_socialize_wechat, "微信好友");
            ButtomDialogBean buttomDialogBean3 = new ButtomDialogBean(ButtomDialogEnum.WEIXIN_CIRCLE, R.drawable.umeng_socialize_wxcircle, "微信朋友圈");
            ButtomDialogBean buttomDialogBean4 = new ButtomDialogBean(ButtomDialogEnum.WEIXIN_FAVORITE, R.drawable.umeng_socialize_fav, "微信收藏");
            ButtomDialogBean buttomDialogBean5 = new ButtomDialogBean(ButtomDialogEnum.SINA, R.drawable.umeng_socialize_sina, "微博");
            ButtomDialogBean buttomDialogBean6 = new ButtomDialogBean(ButtomDialogEnum.SMS, R.drawable.umeng_socialize_sms, "短信");
            ButtomDialogBean buttomDialogBean7 = new ButtomDialogBean(ButtomDialogEnum.QQ, R.drawable.umeng_socialize_qq, "QQ");
            ButtomDialogBean buttomDialogBean8 = new ButtomDialogBean(ButtomDialogEnum.QZONE, R.drawable.umeng_socialize_qzone, "QQ空间");
            buttomDialogBeans.add(buttomDialogBean1);
            buttomDialogBeans.add(buttomDialogBean2);
            buttomDialogBeans.add(buttomDialogBean3);
            buttomDialogBeans.add(buttomDialogBean4);
            buttomDialogBeans.add(buttomDialogBean5);
            buttomDialogBeans.add(buttomDialogBean6);
            buttomDialogBeans.add(buttomDialogBean7);
            buttomDialogBeans.add(buttomDialogBean8);
        }
    }

    /**
     * 监听设置
     *
     * @param view
     */
    private void onClickListener(View view) {
        GridView mGridView = view.findViewById(R.id.button_dialog_gridview);
        TextView buttonDismiss = view.findViewById(R.id.ll_button_dismiss2);
        dialogTip = view.findViewById(R.id.dialog_button_tip2);

        ButtomDialogAdapter buttomDialogAdapter = new ButtomDialogAdapter(context,buttomDialogBeans);
        mGridView.setAdapter(buttomDialogAdapter);

        //跳转设置
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setShareJump(buttomDialogBeans.get(position).getButtomDialogEnum());
                downDialog();
            }
        });

        //关闭
        buttonDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
            }
        });
    }

    /**
     * 分享跳转的设置
     */
    private void setShareJump(ButtomDialogEnum buttomDialogEnum) {
        switch (buttomDialogEnum) {
            case HUABAO:
                myShareBoardlistener.illustratedShare();
                break;
            case WEIXIN:
                myShareBoardlistener.weixinShare(SHARE_MEDIA.WEIXIN);
                break;
            case WEIXIN_CIRCLE:
                myShareBoardlistener.tencentShare(SHARE_MEDIA.WEIXIN_CIRCLE, MyShareBoardlistener.WEI_XIN_FLAG);
                break;
            case WEIXIN_FAVORITE:
                myShareBoardlistener.tencentShare(SHARE_MEDIA.WEIXIN_FAVORITE, MyShareBoardlistener.WEI_XIN_FLAG);
                break;
            case SINA:
                myShareBoardlistener.sinaShare(SHARE_MEDIA.SINA);
                break;
            case SMS:
                myShareBoardlistener.smsShare(SHARE_MEDIA.SMS);
                break;
            case QQ:
                myShareBoardlistener.tencentShare(SHARE_MEDIA.QQ, MyShareBoardlistener.QQ_FLAG);
                break;
            case QZONE:
                myShareBoardlistener.tencentShare(SHARE_MEDIA.QZONE, MyShareBoardlistener.QQ_FLAG);
                break;
        }
    }

    /**
     * 关闭
     */
    private void downDialog() {
        setDialogTipGone();
        dismiss();
    }

    /**
     * 显示提示
     */
    public void setDialogTipVisible() {
        dialogTip.setVisibility(View.VISIBLE);
    }

    /**
     * 隐藏提示
     */
    public void setDialogTipGone() {
        dialogTip.setVisibility(View.GONE);
    }

    public List<ButtomDialogBean> getButtomDialogBeans() {
        return buttomDialogBeans;
    }

    public static class Builder {

        private final Context context;
        private MyShareBoardlistener myShareBoardlistener;
        private boolean iscancelable;
        private boolean isBackCancelable;
        private List<ButtomDialogBean> buttomDialogBeans = new ArrayList<>();

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setShareBoardListener(MyShareBoardlistener myShareBoardlistener) {
            this.myShareBoardlistener = myShareBoardlistener;
            return this;
        }

        public Builder setIscancelable(boolean iscancelable){
            this.iscancelable = iscancelable;
            return this;
        }

        public Builder setIsBackCancelable(boolean isBackCancelable){
            this.isBackCancelable = isBackCancelable;
            return this;
        }

        public Builder setButtomDialogBeans(List<ButtomDialogBean> buttomDialogBeans){
            this.buttomDialogBeans = buttomDialogBeans;
            return this;
        }

        public ButtomDialogView build() {
            return new ButtomDialogView(context,this);
        }
    }


}
