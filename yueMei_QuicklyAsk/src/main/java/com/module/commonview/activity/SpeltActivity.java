package com.module.commonview.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.SpeltShareApi;
import com.module.commonview.module.api.TaoDataApi;
import com.module.commonview.module.bean.SerMap;
import com.module.commonview.module.bean.SpeltShareData;
import com.module.commonview.module.bean.YuDingData;
import com.module.commonview.module.other.SpeltWebViewClient;
import com.module.commonview.view.SkuOrdingPopwindow;
import com.module.commonview.view.TaoDetailShowPopup;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.my.model.bean.CouponsStatusData;
import com.module.my.view.view.OrderSuccessfulDialog;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMMin;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 悦美拼团
 */
public class SpeltActivity extends BaseActivity {

    @BindView(id = R.id.spelt_share_rly)
    public RelativeLayout mSpeltShare;
    @BindView(id = R.id.ll_spell_group)
    public LinearLayout mSpellGroup;
    @BindView(id = R.id.spelt_back)
    private RelativeLayout speltBack;
    @BindView(id = R.id.spelt_receptacle)
    private LinearLayout contentWeb;

    private String TAG = "SpeltActivity";
    private SpeltActivity mContext;
    private WebView directWeb;
    private String url;
    private String source;
    private String objid;
    private String groupId;
    private String skuId;
    private int num;//用户点的第几个拼团
    public Animation mScalOutAnimation;
    public Animation mScalInAnimation1;
    private Animation mScalInAnimation2;
    private String orderId;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private Map<String, Object> urlParame = new HashMap<>();
    private HashMap<String, String> shareParame = new HashMap<>();
    private TaoDetailBean mTaoDetailBean;
    public SkuOrdingPopwindow mSkuOrdingPopwindow;
    private BaseNetWorkCallBackApi getRecommendCouponsStatus;

    @Override
    public void setRootView() {
        setContentView(R.layout.activity_spelt);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getRecommendCouponsStatus = new BaseNetWorkCallBackApi(FinalConstant1.COUPONS, "getRecommendCouponsStatus");
        initView();
        LodUrl(url);
        loadShareData();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        LodUrl(url);
    }

    private void initView() {
        mContext = SpeltActivity.this;
        groupId = getIntent().getStringExtra("group_id");
        Log.e(TAG, "groupId------>" + groupId);
        skuId = getIntent().getStringExtra("sku_id");
        num = getIntent().getIntExtra("num", 0);

        String mType = getIntent().getStringExtra("type");
        if ("0".equals(mType)) {      //未下单的
            source = getIntent().getStringExtra("source");
            objid = getIntent().getStringExtra("objid");

            //得到数据集
            Bundle bundle = getIntent().getExtras();
            //获得自定义类
            SerMap serializableMap = (SerMap) bundle.get("serMap");

        } else if ("1".equals(mType)) {    //已下单的(不是从下单成功页跳转的)

        } else if ("2".equals(mType)) {    //已下单的(从下单成功页跳转的)
            orderId = getIntent().getStringExtra("order_id");
            Log.e(TAG, "orderId === " + orderId);
        }


        url = FinalConstant.SPELT_URL;
        if (!TextUtils.isEmpty(orderId)) {
            urlParame.put("group_id", groupId);
            urlParame.put("order_id", orderId);
        } else {
            urlParame.put("group_id", groupId);
        }

        speltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //分享
        mSpeltShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shareParame.size() > 0) {       //说明请求成功了
                    setShare(shareParame);
                }
            }
        });

        init();
        if ("0".equals(mType)) {
            initYudingData();
        }

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContext);

        initWebview();

        loadingDialogData();
    }

    /**
     * 加载弹层数据
     */
    private void loadingDialogData() {
        getRecommendCouponsStatus.addData("mainOrderID", orderId);
        getRecommendCouponsStatus.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    CouponsStatusData couponsStatusData = JSONUtil.TransformSingleBean(data.data, CouponsStatusData.class);
                    Log.e(TAG, "data === " + data.toString());

                    if ("1".equals(couponsStatusData.getShowStatus())) {
                        OrderSuccessfulDialog orderSuccessfulDialog = new OrderSuccessfulDialog(mContext, couponsStatusData.getAlertWebUrl());
                        orderSuccessfulDialog.show();
                    }
                }
            }
        });
    }

    /**
     * 初始化
     */
    private void init() {

        // 动画初始化
        mScalInAnimation1 = AnimationUtils.loadAnimation(mContext, R.anim.root_in);
        mScalInAnimation2 = AnimationUtils.loadAnimation(mContext, R.anim.root_in2);
        mScalOutAnimation = AnimationUtils.loadAnimation(mContext, R.anim.root_out);


        // 注册事件回调
        mScalInAnimation1.setAnimationListener(new ScalInAnimation1());
    }

    /**
     * 缩小动画的回调
     */
    public class ScalInAnimation1 implements Animation.AnimationListener {

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mSpellGroup.startAnimation(mScalInAnimation2);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }

    /**
     * webView配置
     */
    private void initWebview() {
        directWeb = new WebView(mContext);
        contentWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        contentWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示

        directWeb.getSettings().setUseWideViewPort(true);
        directWeb.getSettings().setJavaScriptEnabled(true);             //启用js
        directWeb.getSettings().setBlockNetworkImage(false);//解决图片不显示
        directWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        directWeb.getSettings().setLoadsImagesAutomatically(true);    //支持自动加载图片
        directWeb.getSettings().setUseWideViewPort(true);
        directWeb.getSettings().setLoadWithOverviewMode(true);
        directWeb.getSettings().setSaveFormData(true);    //设置webview保存表单数据
        directWeb.getSettings().setSavePassword(true);    //设置webview保存密码
        directWeb.getSettings().supportMultipleWindows();
        directWeb.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        directWeb.getSettings().setSupportZoom(true);
        directWeb.getSettings().setDefaultTextEncodingName("UTF-8"); // 设置默认的显示编码
        directWeb.getSettings().setNeedInitialFocus(true);

        directWeb.setWebViewClient(baseWebViewClientMessage);
        directWeb.setWebChromeClient(new WebChromeClient());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            directWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        directWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        contentWeb.addView(directWeb);
    }


    /**
     * 加载web
     */
    public void LodUrl(String urlstr) {
        Log.e(TAG, "urlstr === " + urlstr);
        Log.e(TAG, "groupId === " + groupId);
        baseWebViewClientMessage.startLoading();

        WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr, urlParame);
        if (null != directWeb) {
            directWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }

    }

    void initYudingData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", skuId);
        maps.put("source", source);
        maps.put("objid", objid);
        new TaoDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        mTaoDetailBean = JSONUtil.TransformSingleBean(serverData.data, TaoDetailBean.class);
                        mSkuOrdingPopwindow = new SkuOrdingPopwindow(mContext, "2", mTaoDetailBean, source, objid, "0", "0");
                        baseWebViewClientMessage.setBaseWebViewClientCallback(new SpeltWebViewClient(mContext, num, mTaoDetailBean, source, objid));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    /**
     * 请求分享数据
     */
    private void loadShareData() {
        SpeltShareApi speltShareApi = new SpeltShareApi();
        Map<String, Object> maps = new HashMap<>();
        Log.e(TAG, "groupId === " + groupId);
        maps.put("group_id", groupId);
        speltShareApi.getCallBack(mContext, maps, new BaseCallBackListener<SpeltShareData>() {

            @Override
            public void onSuccess(SpeltShareData shareData) {
                String path = shareData.getPath();
                String description = shareData.getDescription();
                String thumbImage = shareData.getThumbImage();
                String title = shareData.getTitle();
                String userName = shareData.getUserName();
                String webpageUrl = shareData.getWebpageUrl();

                Log.e(TAG, "path === " + path);
                Log.e(TAG, "description === " + description);
                Log.e(TAG, "thumbImage === " + thumbImage);
                Log.e(TAG, "title === " + title);
                Log.e(TAG, "userName === " + userName);
                Log.e(TAG, "webpageUrl === " + webpageUrl);

                shareParame.put("title", title);
                shareParame.put("description", description);
                shareParame.put("webpageUrl", webpageUrl);
                shareParame.put("path", path);
                shareParame.put("thumbImage", thumbImage);
                shareParame.put("userName", userName);

            }
        });
    }

    /**
     * 分享设置
     *
     * @param hashMap
     */
    private void setShare(HashMap<String, String> hashMap) {
        MyUMShareListener myUMShareListener = new MyUMShareListener(mContext);
        boolean installWeiXin = UMShareAPI.get(mContext).isInstall(mContext, SHARE_MEDIA.WEIXIN);

        if (!installWeiXin) {     //微信不存在
            Toast.makeText(mContext, "请先安装微信", Toast.LENGTH_SHORT).show();
            return;
        } else {
            UMMin umMin = new UMMin(hashMap.get("webpageUrl"));
            umMin.setThumb(new UMImage(mContext, hashMap.get("thumbImage")));
            umMin.setTitle(hashMap.get("title"));
            umMin.setDescription(hashMap.get("description"));
            umMin.setPath(hashMap.get("path"));
            umMin.setUserName(hashMap.get("userName"));
            new ShareAction(mContext)
                    .withMedia(umMin)
                    .setPlatform(SHARE_MEDIA.WEIXIN)
                    .setCallback(myUMShareListener)
                    .share();
        }

        myUMShareListener.setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {
                Toast.makeText(mContext, "分享成功", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {
                Toast.makeText(mContext, "分享失败", Toast.LENGTH_LONG).show();
            }
        });
    }


}
