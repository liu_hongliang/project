package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2019/5/14
 */
public class QuestionListReplyData implements Parcelable {
    private String userName;
    private String noticeText;
    private String userImg;
    private String replyContent;

    protected QuestionListReplyData(Parcel in) {
        userName = in.readString();
        noticeText = in.readString();
        userImg = in.readString();
        replyContent = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(noticeText);
        dest.writeString(userImg);
        dest.writeString(replyContent);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionListReplyData> CREATOR = new Creator<QuestionListReplyData>() {
        @Override
        public QuestionListReplyData createFromParcel(Parcel in) {
            return new QuestionListReplyData(in);
        }

        @Override
        public QuestionListReplyData[] newArray(int size) {
            return new QuestionListReplyData[size];
        }
    };

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNoticeText() {
        return noticeText;
    }

    public void setNoticeText(String noticeText) {
        this.noticeText = noticeText;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }
}
