package com.module.commonview.view.webclient;

import android.webkit.WebView;

/**
 * Created by 裴成浩 on 2018/1/22.
 */

public interface WebViewTypeOutside {
    void typeOutside(WebView view, String url);
}
