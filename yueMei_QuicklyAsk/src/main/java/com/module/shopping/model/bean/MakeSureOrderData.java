package com.module.shopping.model.bean;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/11/27.
 */
public class MakeSureOrderData {
    private String loginphone;
    private List<MakeSureOrderSkuData> data;
    private MakeSureOrderTaomember taomember;
    private MakeSureOrderDepositCoupons depositCoupons;
    private String userWalletBalance;

    public String getLoginphone() {
        return loginphone;
    }

    public void setLoginphone(String loginphone) {
        this.loginphone = loginphone;
    }

    public List<MakeSureOrderSkuData> getData() {
        return data;
    }

    public void setData(List<MakeSureOrderSkuData> data) {
        this.data = data;
    }

    public MakeSureOrderTaomember getTaomember() {
        return taomember;
    }

    public void setTaomember(MakeSureOrderTaomember taomember) {
        this.taomember = taomember;
    }

    public MakeSureOrderDepositCoupons getDepositCoupons() {
        return depositCoupons;
    }

    public void setDepositCoupons(MakeSureOrderDepositCoupons depositCoupons) {
        this.depositCoupons = depositCoupons;
    }

    public String getUserWalletBalance() {
        return userWalletBalance;
    }

    public void setUserWalletBalance(String userWalletBalance) {
        this.userWalletBalance = userWalletBalance;
    }
}
