package com.module.commonview.fragment;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.module.base.view.YMBaseFragment;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CommentFragment extends YMBaseFragment {
    public static final String TAG = "CommentFragment";
    @BindView(R.id.comment_webview)
    WebView mCommentWebview;
    Unbinder unbinder;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    StringBuilder baseUrl;
    HashMap<String, Object> maps = new HashMap<>();
    @Override
    protected int getLayoutId() {
        return R.layout.comment_fragemnt;
    }


    @Override
    protected void initView(View view) {
        ViewGroup.MarginLayoutParams layoutParams= (ViewGroup.MarginLayoutParams) mCommentWebview.getLayoutParams();
        layoutParams.topMargin = Utils.dip2px(50) + statusbarHeight;
        mCommentWebview.setLayoutParams(layoutParams);
        mDialog.startLoading();
        initWebView();

    }

    @Override
    protected void initData(View view) {

    }
    
    public void initWebView(){

        Bundle arguments = getArguments();
        String url = arguments.getString("url");
        String[] urls = url.split("/");

        baseUrl = new StringBuilder(FinalConstant.baseUrl + FinalConstant.VER+"/");
        for (int i = 1; i <urls.length ; i++) {
            if (i==1){
                baseUrl.append(urls[1]+"/");
            }else if (i == 2){
                baseUrl.append(urls[2]+"/");
            }else {
                maps.put(urls[i],urls[i+1]);
                i++;
            }

        }

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContext);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCommentWebview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        mCommentWebview.getSettings().setUseWideViewPort(true);    //设置webview推荐使用的窗口，使html界面自适应屏幕
        mCommentWebview.getSettings().setSupportZoom(true);
        mCommentWebview.getSettings().setLoadWithOverviewMode(true);  //设置webview加载的页面的模式
        mCommentWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
        mCommentWebview.getSettings().setAllowFileAccess(true); // 允许访问文件
        mCommentWebview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        mCommentWebview.getSettings().setJavaScriptEnabled(true);
        mCommentWebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mCommentWebview.setWebViewClient(baseWebViewClientMessage);
        mCommentWebview.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    mDialog.stopLoading();
                }
                super.onProgressChanged(view, newProgress);
            }
        });
        mCommentWebview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        mCommentWebview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        LodUrl1(baseUrl.toString(),maps);
    }


    public void LodUrl1(String url, Map<String, Object> map) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, map);
        if (null !=mCommentWebview ) {
            mCommentWebview.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

        }
    }


    public void refreshData(){
        LodUrl1(baseUrl.toString(),maps);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
