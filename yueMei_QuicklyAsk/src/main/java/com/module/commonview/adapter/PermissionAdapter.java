package com.module.commonview.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.commonview.module.bean.PermsissionData;
import com.quicklyask.activity.R;

import java.util.List;

public class PermissionAdapter extends BaseQuickAdapter<PermsissionData, BaseViewHolder> {
    public PermissionAdapter(int layoutResId, @Nullable List<PermsissionData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PermsissionData item) {
        Glide.with(mContext).load(item.getImg()).into((ImageView) helper.getView(R.id.permission_img));
        helper.setText(R.id.permission_name,item.getName())
                .setText(R.id.permission_desc,item.getDesc());
    }
}
