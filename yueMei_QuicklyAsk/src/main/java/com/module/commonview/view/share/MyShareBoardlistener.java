package com.module.commonview.view.share;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.module.commonview.activity.IllustratedActivity;
import com.module.commonview.module.bean.SharePictorial;
import com.module.commonview.module.bean.ShareWechat;
import com.module.community.model.bean.ShareDetailPictorial;
import com.quicklyask.util.ExternalStorage;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMMin;
import com.umeng.socialize.media.UMWeb;
import com.umeng.socialize.shareboard.SnsPlatform;
import com.umeng.socialize.utils.ShareBoardlistener;


/**
 * 分享平台
 * Created by 裴成浩 on 2018/1/4.
 */

public class MyShareBoardlistener implements ShareBoardlistener {

    public static final String QQ_FLAG = "0";
    public static final String WEI_XIN_FLAG = "1";
    private String TAG = "MyShareBoardlistener";
    //    private boolean isProgram = false;                  //分享给微信好友是否是小程序，默认分享的不是小程序。  false不是小程序，true是小程序
    private MyUMShareListener umShareListener;
    private Activity mActivity;
    private ShareWechat mWechat;
    private SharePictorial mSharePictorial;                  //SKU画报数据
    private ShareDetailPictorial mShareDetailPictorial;     //日记本数据
    private String sinaText;
    private String smsText;
    private String tencentUrl;
    private String tencentTitle;
    private UMImage tencentThumb;
    private String tencentDescription;
    private String tencentText;
    private UMImage sinaThumb;
    private boolean installSina;        //微博是否安装
    private boolean installWeiXin;      //微信是否安装
    private boolean installQQ;          //qq是否安装
    private Bitmap UMIimage;            //分享的全是图片信息

    /**
     * 普通分享
     *
     * @param activity
     */
    public MyShareBoardlistener(Activity activity) {
        initData(activity, null);
    }

    /**
     * 普通分享
     *
     * @param activity
     * @param wechat
     */
    public MyShareBoardlistener(Activity activity, ShareWechat wechat) {
        initData(activity, wechat);
    }

    /**
     * SKU画报
     *
     * @param activity
     * @param wechat
     * @param sharePictorial
     */
    public MyShareBoardlistener(Activity activity, ShareWechat wechat, SharePictorial sharePictorial) {
        this.mSharePictorial = sharePictorial;
        initData(activity, wechat);
    }

    /**
     * 日记画报
     *
     * @param activity
     * @param wechat
     * @param shareDetailPictorial
     */
    public MyShareBoardlistener(Activity activity, ShareWechat wechat, ShareDetailPictorial shareDetailPictorial) {
        this.mShareDetailPictorial = shareDetailPictorial;
        initData(activity, wechat);
    }

    /**
     * 初始化数据
     *
     * @param activity
     * @param wechat
     */
    private void initData(Activity activity, ShareWechat wechat) {
        this.mActivity = activity;
        this.mWechat = wechat;

        UMShareAPI umShareAPI = UMShareAPI.get(mActivity);
        installSina = umShareAPI.isInstall(mActivity, SHARE_MEDIA.SINA);
        installWeiXin = umShareAPI.isInstall(mActivity, SHARE_MEDIA.WEIXIN);
        installQQ = umShareAPI.isInstall(mActivity, SHARE_MEDIA.QQ);

        umShareListener = new MyUMShareListener(mActivity);
    }


    /**
     * 微博分享文案
     *
     * @param sinaText
     * @return
     */
    public MyShareBoardlistener setSinaText(String sinaText) {
        this.sinaText = sinaText;
        return this;
    }

    /**
     * 微博分享缩略图
     *
     * @param sinaThumb
     * @return
     */
    public MyShareBoardlistener setSinaThumb(UMImage sinaThumb) {
        this.sinaThumb = sinaThumb;
        return this;
    }

    /**
     * 分享图片
     *
     * @param UMIimage
     */
    public MyShareBoardlistener setUMIimage(Bitmap UMIimage) {
        this.UMIimage = UMIimage;
        return this;
    }

    /**
     * 短信分享文案
     *
     * @param smsText
     * @return
     */
    public MyShareBoardlistener setSmsText(String smsText) {
        this.smsText = smsText;
        return this;
    }

    /**
     * 腾讯分享跳转连接
     *
     * @param tencentUrl
     * @return
     */
    public MyShareBoardlistener setTencentUrl(String tencentUrl) {
        this.tencentUrl = tencentUrl;
        return this;
    }

    /**
     * 腾讯分享标题
     *
     * @param tencentTitle
     * @return
     */
    public MyShareBoardlistener setTencentTitle(String tencentTitle) {
        this.tencentTitle = tencentTitle;
        return this;
    }

    /**
     * 腾讯分享缩略图
     *
     * @param tencentThumb
     * @return
     */
    public MyShareBoardlistener setTencentThumb(UMImage tencentThumb) {
        this.tencentThumb = tencentThumb;
        return this;
    }

    /**
     * 腾讯分享描述
     *
     * @param tencentDescription
     * @return
     */
    public MyShareBoardlistener setTencentDescription(String tencentDescription) {
        this.tencentDescription = tencentDescription;
        return this;
    }

    /**
     * 设置腾讯分享内容
     *
     * @param tencentText
     * @return
     */
    public MyShareBoardlistener setTencentText(String tencentText) {
        this.tencentText = tencentText;
        return this;
    }

    /**
     * 设置微信小程序数据
     *
     * @param wechat
     * @return
     */
    public MyShareBoardlistener setWechatData(ShareWechat wechat) {
        this.mWechat = wechat;
        return this;
    }

    /**
     * 设置SKU画报数据
     *
     * @param pictorial
     * @return
     */
    public MyShareBoardlistener setPictorialData(SharePictorial pictorial) {
        this.mSharePictorial = pictorial;
        return this;
    }

    /**
     * 设置日记本画报数据
     *
     * @param shareDetailPictorial
     * @return
     */
    public MyShareBoardlistener setPictorialData(ShareDetailPictorial shareDetailPictorial) {
        this.mShareDetailPictorial = shareDetailPictorial;
        return this;
    }

    @Override
    public void onclick(SnsPlatform snsPlatform, SHARE_MEDIA share_media) {
        if (share_media != null) {

            if (share_media.equals(SHARE_MEDIA.SINA)) {

                sinaShare(share_media);

            } else if (share_media.equals(SHARE_MEDIA.SMS)) {

                smsShare(share_media);

            } else if (share_media.equals(SHARE_MEDIA.QQ) || share_media.equals(SHARE_MEDIA.QZONE)) {     //qq或者qq空间

                tencentShare(share_media, QQ_FLAG);

            } else if (share_media.equals(SHARE_MEDIA.WEIXIN_CIRCLE) || share_media.equals(SHARE_MEDIA.WEIXIN_FAVORITE)) {       //微信朋友圈,或者微信收藏

                tencentShare(share_media, WEI_XIN_FLAG);

            } else if (share_media.equals(SHARE_MEDIA.WEIXIN)) {       //微信好友
                weixinShare(share_media);
            }

        } else {                                                            //自定义按钮
            if (snsPlatform.mKeyword.equals("umeng_sharebutton_huabao")) {       //SKU画报
                illustratedShare();
            }
        }
    }

    /**
     * 新浪分享
     *
     * @param share_media
     */
    public void sinaShare(SHARE_MEDIA share_media) {

        if (installSina) {
            if (UMIimage != null) {
                setUMIimage(share_media);
            } else {
                new ShareAction(mActivity).setPlatform(share_media).setCallback(umShareListener).withText(sinaText).withMedia(sinaThumb).share();
            }

        } else {
            Toast.makeText(mActivity, "请先安装微博", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 分享短息
     *
     * @param share_media
     */
    public void smsShare(SHARE_MEDIA share_media) {
        new ShareAction(mActivity).setPlatform(share_media).setCallback(umShareListener).withText(smsText).share();
    }

    /**
     * 腾讯分享（微信好友[非小程序]、微信收藏、朋友圈、qq好友、QQ空间）
     *
     * @param share_media
     * @param location:分享到的是qq还是微信。0:qq,1：微信
     */
    public void tencentShare(SHARE_MEDIA share_media, String location) {

        switch (location) {
            case "0":
                if (!installQQ) {     //qq不存在
                    Toast.makeText(mActivity, "请先安装qq", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
            case "1":
                if (!installWeiXin) {     //微信不存在
                    Toast.makeText(mActivity, "请先安装微信", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
        }

//        Log.e(TAG, "tencentUrl === " + tencentUrl);
//        Log.e(TAG, "tencentTitle === " + tencentTitle);
//        Log.e(TAG, "tencentThumb === " + tencentThumb);
//        Log.e(TAG, "tencentDescription === " + tencentDescription);
//        Log.e(TAG, "tencentText === " + tencentText);
//        Log.e(TAG, "umShareListener === " + umShareListener);

        if (UMIimage != null) {
            setUMIimage(share_media);
        } else {
            UMWeb shareWeb = new UMWeb(tencentUrl);
            shareWeb.setTitle(tencentTitle);
            shareWeb.setThumb(tencentThumb);
            shareWeb.setDescription(tencentDescription);

            Log.e(TAG, "mActivity == " + mActivity);
            Log.e(TAG, "tencentText == " + tencentText);
            Log.e(TAG, "shareWeb == " + shareWeb);
            Log.e(TAG, "share_media == " + share_media);
            Log.e(TAG, "umShareListener == " + umShareListener);
            new ShareAction(mActivity).withText(tencentText).withMedia(shareWeb).setPlatform(share_media).setCallback(umShareListener).share();
        }
    }

    /**
     * 微信好友
     *
     * @param share_media
     */
    public void weixinShare(SHARE_MEDIA share_media) {
        if (!installWeiXin) {     //微信不存在
            Toast.makeText(mActivity, "请先安装微信", Toast.LENGTH_SHORT).show();
        } else {
            if (mWechat != null) {                  //分享到微信是小程序
                UMMin umMin = new UMMin(mWechat.getWebpageUrl());
                umMin.setThumb(new UMImage(mActivity, mWechat.getThumbImage()));
                umMin.setTitle(mWechat.getTitle());
                umMin.setDescription(mWechat.getDescription());
                umMin.setPath(mWechat.getPath());
                umMin.setUserName(mWechat.getUserName());
                new ShareAction(mActivity).withMedia(umMin).setPlatform(share_media).setCallback(umShareListener).share();
            } else {                          //分享到微信的是消息
                tencentShare(share_media, WEI_XIN_FLAG);
            }
        }
    }

    /**
     * 分享图片
     */
    private void setUMIimage(SHARE_MEDIA share_media) {
        UMImage image1 = new UMImage(mActivity, UMIimage);          //本地文件
        ExternalStorage.saveImageToGallery(mActivity, UMIimage);    //保存到本地图库中

        new ShareAction(mActivity).withMedia(image1).setPlatform(share_media).setCallback(umShareListener).share();
    }

    /**
     * 自定义画报
     */
    public void illustratedShare() {
//        buttomDialogView.huabaoVisible();
        if (mSharePictorial != null) {
            Intent intent = new Intent(mActivity, IllustratedActivity.class);
            intent.putExtra("falg", "0");
            intent.putExtra("data", mSharePictorial);
            mActivity.startActivity(intent);
        } else if (mShareDetailPictorial != null) {                        //日记画报
            Log.e(TAG, "mShareDetailPictorial === " + mShareDetailPictorial.getUser_name());
            Intent intent = new Intent(mActivity, IllustratedActivity.class);
            intent.putExtra("falg", "1");
            intent.putExtra("data1", mShareDetailPictorial);
            mActivity.startActivity(intent);
        } else {
            Toast.makeText(mActivity, "该帖子或项目暂不能生成画报", Toast.LENGTH_SHORT).show();
//            buttomDialogView.huabaoGone();
        }
    }

    public MyUMShareListener getUmShareListener() {
        return umShareListener;
    }
}
