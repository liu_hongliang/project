package com.module.my.model.bean;

/**
 * Created by dwb on 16/9/12.
 */
public class BaoXian {

    private String code;
    private String message;
    private BaoXianData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BaoXianData getData() {
        return data;
    }

    public void setData(BaoXianData data) {
        this.data = data;
    }
}
