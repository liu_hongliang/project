package com.module.home.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.commonview.view.FocusButton2;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.my.model.bean.MyFansData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.MyToast;

import java.util.List;

/**
 * 用户列表
 * Created by 裴成浩 on 2019/9/5
 */
public class ProjectUserAdapter extends RecyclerView.Adapter<ProjectUserAdapter.ViewHolder> {

    private String TAG = "ProjectUserAdapter";
    private Activity mContext;
    private List<MyFansData> mDatas;
    private LayoutInflater mInflater;
    private final FocusAndCancelApi mFocusAndCancelApi;

    public ProjectUserAdapter(Activity context, List<MyFansData> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mFocusAndCancelApi = new FocusAndCancelApi();
        mInflater = LayoutInflater.from(mContext);
        Log.e(TAG, "mDatas === " + mDatas.size());
    }

    @NonNull
    @Override
    public ProjectUserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.item_project_user_fragment, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProjectUserAdapter.ViewHolder viewHolder, final int position) {
        MyFansData fansData = mDatas.get(position);
        Log.e(TAG, "fansData.getImg() === " + fansData.getImg());
        Log.e(TAG, "fansData.getName() === " + fansData.getName());
        Glide.with(mContext).load(fansData.getImg())
                .transform(new GlideCircleTransform(mContext))
                .into(viewHolder.ficusImg);

        viewHolder.ficusName.setText(fansData.getName());
        viewHolder.ficusDesc.setText(fansData.getDesc());

        switch (fansData.getV()) {
            case "0":          //无
                viewHolder.ficusV.setVisibility(View.GONE);
                break;
            case "10":          //红色--官方
                viewHolder.ficusV.setVisibility(View.VISIBLE);
                viewHolder.ficusV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.official_bottom));
                break;
            case "12":          //蓝色--认证
                viewHolder.ficusV.setVisibility(View.VISIBLE);
                viewHolder.ficusV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.renzheng_bottom));
                break;
            case "13":          //蓝色--认证
                viewHolder.ficusV.setVisibility(View.VISIBLE);
                viewHolder.ficusV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.renzheng_bottom));
                break;
            case "11":          //达人
                viewHolder.ficusV.setVisibility(View.VISIBLE);
                viewHolder.ficusV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.talent_bottom));
                break;
        }

        //关注样式
        final String eachFollowing = fansData.getEach_following();
        switch (eachFollowing) {
            case "0":               //未关注
                viewHolder.eachFollowing.setFocusType(FocusButton2.FocusType.NOT_FOCUS);
                break;
            case "1":               //已关注
                viewHolder.eachFollowing.setFocusType(FocusButton2.FocusType.HAS_FOCUS);
                break;
            case "2":               //互相关注
                viewHolder.eachFollowing.setFocusType(FocusButton2.FocusType.EACH_FOCUS);
                break;
        }

        //关注按钮点击
        viewHolder.eachFollowing.setFocusClickListener(fansData.getEach_following(), new FocusButton2.ClickCallBack() {
            @Override
            public void onClick(View v) {
                MyFansData myFansData = mDatas.get(position);
                focusAndCancel(viewHolder.eachFollowing, myFansData);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ficusImg;
        ImageView ficusV;
        TextView ficusName;
        TextView ficusDesc;
        FocusButton2 eachFollowing;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ficusImg = itemView.findViewById(R.id.item_project_user_img);
            ficusV = itemView.findViewById(R.id.item_project_user_v);
            ficusName = itemView.findViewById(R.id.item_project_user_name);
            ficusDesc = itemView.findViewById(R.id.item_project_user_desc);
            eachFollowing = itemView.findViewById(R.id.item_project_user_focus);

            //item点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = mDatas.get(getLayoutPosition()).getUrl();
                    if (!TextUtils.isEmpty(url)) {
                        YmStatistics.getInstance().tongjiApp(mDatas.get(getLayoutPosition()).getEvent_params());
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                }
            });
        }
    }

    /**
     * 关注和取消关注
     *
     * @param mFocus     ：关注按钮
     * @param myFansData ：关注数据
     */
    private void focusAndCancel(final FocusButton2 mFocus, final MyFansData myFansData) {
        mFocusAndCancelApi.addData("objid", myFansData.getObj_id());
        mFocusAndCancelApi.addData("type", myFansData.getObj_type());
        mFocusAndCancelApi.getCallBack(mContext, mFocusAndCancelApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        FocusAndCancelData mFocusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (mFocusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                myFansData.setEach_following("0");
                                mFocus.setFocusType(FocusButton2.FocusType.NOT_FOCUS);
                                MyToast.makeTextToast1(mContext, "取关成功", MyToast.SHOW_TIME).show();
                                break;
                            case "1":          //已关注
                                myFansData.setEach_following("1");
                                mFocus.setFocusType(FocusButton2.FocusType.HAS_FOCUS);
                                MyToast.makeTextToast1(mContext, serverData.message, MyToast.SHOW_TIME).show();
                                break;
                            case "2":          //互相关注
                                myFansData.setEach_following("2");
                                mFocus.setFocusType(FocusButton2.FocusType.EACH_FOCUS);
                                MyToast.makeTextToast1(mContext, serverData.message, MyToast.SHOW_TIME).show();
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 添加加载更多的数据
     *
     * @return
     */
    public void addData(List<MyFansData> datas) {
        int size = mDatas.size();
        mDatas.addAll(datas);
        notifyItemRangeInserted(size, mDatas.size() - size);
    }
}
