/**
 *
 */
package com.module.commonview.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.net.URLDecoder;
import java.util.HashMap;

/**
 * 百科二级页面
 *
 * @author Robin
 */
public class BaikeTwoActivity extends BaseActivity {

    private final String TAG = "BaikeTwoActivity";

    private LinearLayout contentWeb;

    private WebView docDetWeb;

    private BaikeTwoActivity mContex;

    public JSONObject obj_http;

    private BaseWebViewClientMessage baseWebViewClientMessage;
    private CommonTopBar baikeTowTop;


    @Override
    public void setRootView() {
        setContentView(R.layout.acty_zhifu_help1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = BaikeTwoActivity.this;

        Intent it = getIntent();
        String titlename = it.getStringExtra("name");
        String url = it.getStringExtra("url");

        Log.e(TAG, "titlename === " + titlename);

        baikeTowTop = findViewById(R.id.baike_tow_top);
        contentWeb = findViewById(R.id.wan_beautifu_linearlayout3);

        baikeTowTop.setCenterText(titlename + "小档案");

        baikeTowTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex,"11","0");
        baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String urlStr) {
                showWebDetail(urlStr);
            }
        });

        initWebview();
        LodUrl1(url);
        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout.setOnSildingFinishListener(new OnSildingFinishListener() {

            @Override
            public void onSildingFinish() {
                BaikeTwoActivity.this.finish();
            }
        });
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    @SuppressLint({"SetJavaScriptEnabled", "InlinedApi"})
    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.setHorizontalScrollBarEnabled(false);//水平不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直不显示
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        contentWeb.addView(docDetWeb);
    }


    /**
     * 处理webview里面的按钮
     *
     * @param urlStr
     */
    public void showWebDetail(String urlStr) {
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;
            obj_http = obj;

            if (obj.getString("type").equals("1")) {// 问答详情
                String link = obj.getString("link");
                String qid = obj.getString("id");
                link = FinalConstant.baseUrl + FinalConstant.VER + link;
                Intent it = new Intent();
                it.setClass(mContex, DiariesAndPostsActivity.class);
                it.putExtra("url", link);
                it.putExtra("qid", qid);
                startActivity(it);
            }

            // type:eq:5902:and:link:eq:/baike/twopart/id/79/:and:name:eq:XX
            if (obj.getString("type").equals("5902")) {// 跳百科二级
                String link = obj.getString("link");
                String url = FinalConstant.baseUrl + FinalConstant.VER + link;
                String name = URLDecoder.decode(obj.getString("name"), "utf-8");
                Intent it = new Intent();
                it.putExtra("url", url);
                it.putExtra("name", name);
                it.setClass(mContex, BaikeTwoActivity.class);
                startActivity(it);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加载web
     */
    public void LodUrl1(String url) {

        Log.e(TAG, "url === " + url);

        String[] strs = url.split("/");
        Log.e(TAG, "strs4 === " + strs[4]);
        Log.e(TAG, "strs5 === " + strs[5]);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(strs[6],strs[7]);

        String urls = FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_URL + FinalConstant1.SYMBOL2 + FinalConstant1 .YUEMEI_VER + FinalConstant1.SYMBOL2 + strs[4] + FinalConstant1.SYMBOL2 + strs[5] + FinalConstant1.SYMBOL2;
        Log.e(TAG, "urls === " + urls);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(urls,hashMap);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
