package com.module.taodetail.controller.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.taodetail.model.bean.TaoZtItemTao;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2017/8/4.
 */

public class TaoWonderfulAdapter extends RecyclerView.Adapter<TaoWonderfulAdapter.ViewHolder> {

    private final List<TaoZtItemTao> mTaoData;
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final int mParentPos;

    public TaoWonderfulAdapter(Context context, List<TaoZtItemTao> taoData, int parentPos) {
        this.mContext = context;
        this.mTaoData = taoData;
        this.mParentPos = parentPos;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_item_tao_wonderfu, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        View view = holder.itemView;
        ViewGroup.LayoutParams params = view.getLayoutParams();
        if (position == 0) {                                //第一个
            params.width = Utils.dip2px(mContext, 110);
            view.setPadding(Utils.dip2px(mContext, 10), 0, 0, 0);
        } else if (position == getItemCount() - 1) {        //最后一个
            params.width = Utils.dip2px(mContext, 110);
            view.setPadding(0, 0, Utils.dip2px(mContext, 10), 0);
        } else {                                            //其他
            params.width = Utils.dip2px(mContext, 110);
        }
        view.setLayoutParams(params);

        if (position == getItemCount() - 1) {        //是最后一个
            holder.itemWonderfuImg.setVisibility(View.INVISIBLE);
            holder.itemWonderfuMore.setVisibility(View.VISIBLE);
            holder.itemWonderfuShow.setVisibility(View.INVISIBLE);
        } else {

            TaoZtItemTao itemTao = mTaoData.get(position);

            Glide.with(mContext)
                    .load(itemTao.getImg())
                    .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                    .placeholder(R.drawable.radius_gray80)
                    .error(R.drawable.radius_gray80)
                    .into(holder.itemWonderfuImg);


            holder.itemWonderfuTitle.setText(itemTao.getTitle());

            holder.itemWonderfuPrice.setText(itemTao.getPrice_discount());
            String member_price = itemTao.getMember_price();
            int i = Integer.parseInt(member_price);
            if (i >= 0) {
                holder.itemPlusVibisity.setVisibility(View.VISIBLE);
                holder.itemPlusPrice.setText("¥" + member_price);
            } else {
                holder.itemPlusVibisity.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mTaoData.size() == 0 ? 0 : mTaoData.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView itemWonderfuImg;
        public LinearLayout itemWonderfuMore;
        public TextView itemWonderfuTitle;
        public TextView itemWonderfuPrice;
        public LinearLayout itemWonderfuShow;
        public LinearLayout itemPlusVibisity;
        public TextView itemPlusPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            itemWonderfuImg = itemView.findViewById(R.id.item_wonderfu_img);
            itemWonderfuMore = itemView.findViewById(R.id.item_wonderfu_more);
            itemWonderfuTitle = itemView.findViewById(R.id.item_wonderfu_title);
            itemWonderfuPrice = itemView.findViewById(R.id.item_wonderfu_price);
            itemWonderfuShow = itemView.findViewById(R.id.item_wonderfu_show);
            itemPlusVibisity = itemView.findViewById(R.id.tao_plus_vibility);
            itemPlusPrice = itemView.findViewById(R.id.tao_plus_price);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String _id = "0";
                    if (mTaoData.size() > 0) {
                        _id = mTaoData.get(getLayoutPosition()).get_id();
                    }
//                    HashMap<String, String> hashMap = new HashMap<>();
//                    hashMap.put("id", _id);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_ZT_TAO, (mParentPos + 1) + "-" + (getLayoutPosition() + 1), "tao"), mTaoData.get(getLayoutPosition()).getEvent_params());
                    Intent it2 = new Intent();
                    it2.putExtra("id", _id);
                    it2.putExtra("source", "49");
                    it2.putExtra("objid", "0");
                    it2.setClass(mContext, TaoDetailActivity.class);
                    mContext.startActivity(it2);
                }
            });

            itemWonderfuMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickMoreListener != null) {
                        onItemClickMoreListener.onItemClickMore(mTaoData.size());
                    }
                }
            });
        }
    }

    //查看更多点击事件回调
    public interface OnItemClickMoreListener {
        void onItemClickMore(int pos);
    }

    private OnItemClickMoreListener onItemClickMoreListener;

    public void setOnItemClickMoreListener(OnItemClickMoreListener onItemClickMoreListener) {
        this.onItemClickMoreListener = onItemClickMoreListener;
    }


}