package com.module.shopping.model.bean;

/**
 * 购物车删除操作实体类
 * Created by 裴成浩 on 2018/12/4
 */
public class CartDeleteData {
    private String hos_id;             //医院id
    private String sku_id;             //sku id
    private String cart_id;            //购物车id

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }

    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }
}
