package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.view.MyElasticScrollView.OnRefreshListener1;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.SystemTool;

/**
 * 赔付声明
 *
 * @author lenovo17
 */
public class TaoCompensateWebActivity extends BaseActivity {

    private final String TAG = "TaoCompensateWebActivity";

    @BindView(id = R.id.wan_beautifu_web_det_scrollview3, click = true)
    private MyElasticScrollView scollwebView;
    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.wan_beautiful_web_back, click = true)
    private RelativeLayout back;// 返回

    @BindView(id = R.id.tao_web_share_rly111, click = true)
    private RelativeLayout shareBt;// 分享

    private WebView docDetWeb;

    private TaoCompensateWebActivity mContex;

    public JSONObject obj_http;

    // 分享
    private String shareUrl = "";
    private LinearLayout nowifiLy;
    private Button refreshBt;
    private BaseWebViewClientMessage baseWebViewClientMessage;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_tao_compensate_web);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = TaoCompensateWebActivity.this;
        nowifiLy = findViewById(R.id.no_wifi_ly1);
        refreshBt = findViewById(R.id.refresh_bt);
        scollwebView.GetLinearLayout(contentWeb);

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        initWebview();
        LodUrl1(FinalConstant.COMPENSTATE);

        scollwebView.setonRefreshListener(new OnRefreshListener1() {

            @Override
            public void onRefresh() {
                webReload();
            }
        });

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        TaoCompensateWebActivity.this.finish();
                    }
                });
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);

        if (SystemTool.checkNet(mContex)) {
            nowifiLy.setVisibility(View.GONE);
        } else {
            nowifiLy.setVisibility(View.VISIBLE);
        }

        refreshBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                baseWebViewClientMessage.startLoading();
                new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (SystemTool.checkNet(mContex)) {
                            nowifiLy.setVisibility(View.GONE);
                            baseWebViewClientMessage.stopLoading();
                            LodUrl1(FinalConstant.COMPENSTATE);
                        } else {
                            baseWebViewClientMessage.stopLoading();
                            nowifiLy.setVisibility(View.VISIBLE);
                            ViewInject.toast("网络连接失败");
                        }
                    }
                }.start();
            }
        });

    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    public Dialog dialog = null;


    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        contentWeb.addView(docDetWeb);
    }

    protected void OnReceiveData(String str) {
        scollwebView.onRefreshComplete();
    }

    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        baseWebViewClientMessage.startLoading();
        WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

    }

    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.wan_beautiful_web_back:
                // finish();
                onBackPressed();
                break;
            case R.id.tao_web_share_rly111://分享
                setShare();
                break;
        }
    }

    /**
     * 设置分享
     */
    private void setShare() {
        String sinaText = "悦美推出整形先行赔付保障：整形失败悦美先行赔付。点击查询：http://m.yuemei.com/tao/compensate/ 分享自@悦美整形APP";
        String smsText = "悦美推出整形先行赔付保障：整形失败悦美先行赔付。 http://m.yuemei.com/tao/compensate/";
        BaseShareView baseShareView = new BaseShareView(mContex);
        baseShareView
                .setShareContent("悦美先行赔付保障")
                .ShareAction();

        baseShareView.getShareBoardlistener()
                .setSinaText(sinaText)
                .setSinaThumb(new UMImage(mContex, R.drawable.weibo_share_peifu_xiao))
                .setSmsText(smsText)
                .setTencentUrl(shareUrl)
                .setTencentTitle("悦美先行赔付保障")
                .setTencentThumb(new UMImage(mContex, R.drawable.weibo_share_peifu))
                .setTencentDescription("悦美推出整形先行赔付保障：整形失败悦美先行赔付。")
                .setTencentText("悦美推出整形先行赔付保障：整形失败悦美先行赔付。")
                .getUmShareListener()
                .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                    @Override
                    public void onShareResultClick(SHARE_MEDIA platform) {

                        if (!platform.equals(SHARE_MEDIA.SMS)) {
                            Toast.makeText(mContex, " 分享成功啦",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}