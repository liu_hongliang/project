package com.module;

import android.os.Bundle;

import com.module.base.view.YMBaseActivity;
import com.quicklyask.activity.R;

public class InterestingActivity extends YMBaseActivity {

//    public static final String TAG = "InterestingActivity";
//    @BindView(R.id.interest_rl)
//    RelativeLayout mIntestRel;
//    @BindView(R.id.select_tv_next)
//    TextView mSelectTvNext;
//    @BindView(R.id.bubblepicker)
//    BubblePicker mBubble;
//    @BindView(R.id.select_btn_ok)
//    Button mSelectBtnOk;
//    @BindView(R.id.select_rl_next)
//    RelativeLayout mSelectRlNext;
//
//    private List<String> mSelectList = new ArrayList<>();
//    private ArrayList<PickerItem> mPickerItems = new ArrayList<>();
//    private Context mContext;
//    private ArrayList<TagBean> mTagList;
//    private ArrayList<BudgetBean> mBudgetList;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_interesting;

    }

    @Override
    protected void initView() {
        mContext = InterestingActivity.this;
    }

    @Override
    protected void initData() {
//        if (mTagList != null){
//            mTagList.clear();
//        }
//        if (mBudgetList != null){
//            mBudgetList.clear();
//        }
//        mTagList = getIntent().getParcelableArrayListExtra("taglist");
//        mBudgetList = getIntent().getParcelableArrayListExtra("budget");
//        Log.e(TAG,"mBudgetList"+mBudgetList.toString());
//        for (int i = 0; i <mTagList.size() ; i++) {
//            PickerItem pickerItem = new PickerItem();
//            pickerItem.setTitle(mTagList.get(i).getName());
//            pickerItem.setGradient(new BubbleGradient(ContextCompat.getColor(mContext,R.color.interent), ContextCompat.getColor(mContext,R.color.interent), BubbleGradient.VERTICAL));
//            pickerItem.setTextColor(ContextCompat.getColor(mContext, android.R.color.white));
//            pickerItem.setBackgroundImage(ContextCompat.getDrawable(mContext,R.drawable.interestballbg));
//            mPickerItems.add(pickerItem);
//        }
//        mBubble.setItems(mPickerItems);
//        mBubble.setBubbleSize(20);
//        mBubble.setMaxSelectedCount(6);
//        mBubble.setListener(new BubblePickerListener() {
//            @Override
//            public void onBubbleSelected(@NotNull PickerItem item) {
//
//                for (int i = 0; i < mTagList.size(); i++) {
//                    if (item.getTitle().equals(mTagList.get(i).getName())) {
//                        mSelectList.add(mTagList.get(i).getId() + "");
//                        HashMap event_params = mTagList.get(i).getEvent_params();
//                        YmStatistics.getInstance().tongjiApp(event_params);
//                    }
//                }
//                Log.e(TAG, mSelectList.toString());
//                setButtonState();
//            }
//
//            @Override
//            public void onBubbleDeselected(@NotNull PickerItem item) {
//                for (int i = 0; i < mTagList.size(); i++) {
//                    if (item.getTitle().equals(mTagList.get(i).getName())) {
//                        mSelectList.remove(mTagList.get(i).getId() + "");
//                    }
//                }
//                Log.e(TAG, mSelectList.toString());
//                setButtonState();
//            }
//        });
//        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COLD_START_INTEREST, "liulan"));
//

    }

//    private void setButtonState() {
//        if (mSelectList.size() < 2) {
//            mSelectBtnOk.setBackgroundResource(R.drawable.select_interest_unselect);
//            mSelectBtnOk.setText("请选择至少2个兴趣");
//            mSelectBtnOk.setEnabled(false);
//        } else {
//            mSelectBtnOk.setBackgroundResource(R.drawable.select_interest_shap);
//            mSelectBtnOk.setText("OK，选好啦");
//            mSelectBtnOk.setEnabled(true);
//        }
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

//    @OnClick({R.id.select_tv_next, R.id.select_btn_ok,R.id.select_rl_next})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.select_tv_next:
//                if (Utils.isFastDoubleClick()) return;
//                Intent intent = new Intent(InterestingActivity.this, BudgetActivity.class);
//                intent.putParcelableArrayListExtra("budget", mBudgetList);
//                startActivity(intent);
//                finish();
//                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COLD_START_INTEREST, "jump"));
//                break;
//            case R.id.select_btn_ok:
//                if (Utils.isFastDoubleClick()) return;
//                String data = StringUtils.strip(mSelectList.toString(), "[]");
//                Log.e(TAG, "data===" + data.trim());
//                HashMap<String, Object> maps = new HashMap<>();
//                maps.put("tag_ids", data.trim());
//                maps.put("type", "1");
//                new CollectUserDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
//                    @Override
//                    public void onSuccess(ServerData serverData) {
//                        if ("1".equals(serverData.code)) {
//                            Intent intent1 = new Intent(mContext, BudgetActivity.class);
//                            ArrayList<String> tagName = new ArrayList<>();
//                            List<PickerItem> selectedItems = mBubble.getSelectedItems();
//                            for (int i = 0; i < selectedItems.size(); i++) {
//                                tagName.add(selectedItems.get(i).getTitle());
//                            }
//                            intent1.putStringArrayListExtra("tagname", tagName);
//                            intent1.putParcelableArrayListExtra("budget", mBudgetList);
//                            startActivity(intent1);
//                            finish();
//                        } else {
//                            Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COLD_START_INTEREST, "ok"));
//                break;
//            case R.id.select_rl_next:
//                if (Utils.isFastDoubleClick()) return;
//                Intent intent2 = new Intent(InterestingActivity.this, BudgetActivity.class);
//                intent2.putParcelableArrayListExtra("budget", mBudgetList);
//                startActivity(intent2);
//                finish();
//                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COLD_START_INTEREST, "jump"));
//                break;
//        }
//    }


    @Override
    protected void onResume() {
        super.onResume();
//        mBubble.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        mBubble.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
