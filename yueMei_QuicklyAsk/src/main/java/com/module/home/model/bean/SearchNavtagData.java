package com.module.home.model.bean;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/9/10
 */
public class SearchNavtagData {
    private String id;
    private String p_id;
    private String cate_name;
    private HashMap<String,String> event_params;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getCate_name() {
        return cate_name;
    }

    public void setCate_name(String cate_name) {
        this.cate_name = cate_name;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
