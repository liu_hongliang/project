package com.module.home.model.bean;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2017/7/31.
 */

public class HomeData623{
    private List<HuangDeng1> huandeng;
    private HomeNav nav;
    private List<CardBean> kapian;
    private List<LanMu2Bean> lanmu2;
    private List<LanMu3Bean> lanmu3;
    private List<List<HuangDeng1>> metro_top;
    private List<List<HuangDeng1>> metro_buttom;
    private List<HuangDeng1> zt;
    private List<HuangDeng1> tuij;
    private List<HotBean> hot;
    private List<BbsBean> bbsdata;
    private List<BoardBean> board;
    private ArrayList<Tuijshare> tuijshare;
    private Floating floating;
    private HomePlus plus;
    private HomeAskEntry ask_entry;
    private NewZt new_zt;
    private Coupos coupons;
    private String showType;
    private HomeButtomFloat buttomFloat;
    private List<NewZtCouponsBean> newZtCoupons;


    public List<HuangDeng1> getHuandeng() {
        return huandeng;
    }

    public void setHuandeng(List<HuangDeng1> huandeng) {
        this.huandeng = huandeng;
    }

    public HomeNav getHomeNav() {
        return nav;
    }

    public void setHomeNav(HomeNav nav) {
        this.nav = nav;
    }

    public List<CardBean> getKapian() {
        return kapian;
    }

    public void setKapian(List<CardBean> kapian) {
        this.kapian = kapian;
    }

    public List<LanMu2Bean> getLanmu2() {
        return lanmu2;
    }

    public void setLanmu2(List<LanMu2Bean> lanmu2) {
        this.lanmu2 = lanmu2;
    }

    public List<LanMu3Bean> getLanmu3() {
        return lanmu3;
    }

    public void setLanmu3(List<LanMu3Bean> lanmu3) {
        this.lanmu3 = lanmu3;
    }

    public List<List<HuangDeng1>> getMetro_top() {
        return metro_top;
    }

    public void setMetro_top(List<List<HuangDeng1>> metro_top) {
        this.metro_top = metro_top;
    }

    public List<List<HuangDeng1>> getMetro_buttom() {
        return metro_buttom;
    }

    public void setMetro_buttom(List<List<HuangDeng1>> metro_buttom) {
        this.metro_buttom = metro_buttom;
    }

    public List<HuangDeng1> getZt() {
        return zt;
    }

    public void setZt(List<HuangDeng1> zt) {
        this.zt = zt;
    }

    public List<HuangDeng1> getTuij() {
        return tuij;
    }

    public void setTuij(List<HuangDeng1> tuij) {
        this.tuij = tuij;
    }

    public List<HotBean> getHot() {
        return hot;
    }

    public void setHot(List<HotBean> hot) {
        this.hot = hot;
    }

    public List<BbsBean> getBbsdata() {
        return bbsdata;
    }

    public void setBbsdata(List<BbsBean> bbsdata) {
        this.bbsdata = bbsdata;
    }

    public List<BoardBean> getBoard() {
        return board;
    }

    public void setBoard(List<BoardBean> board) {
        this.board = board;
    }

    public ArrayList<Tuijshare> getTuijshare() {
        return tuijshare;
    }

    public void setTuijshare(ArrayList<Tuijshare> tuijshare) {
        this.tuijshare = tuijshare;
    }

    public Floating getFloating() {
        return floating;
    }

    public void setFloating(Floating floating) {
        this.floating = floating;
    }

    public HomeNav getNav() {
        return nav;
    }

    public void setNav(HomeNav nav) {
        this.nav = nav;
    }

    public HomePlus getPlus() {
        return plus;
    }

    public void setPlus(HomePlus plus) {
        this.plus = plus;
    }

    public NewZt getNew_zt() {
        return new_zt;
    }

    public void setNew_zt(NewZt new_zt) {
        this.new_zt = new_zt;
    }

    public HomeAskEntry getAsk_entry() {
        return ask_entry;
    }

    public void setAsk_entry(HomeAskEntry ask_entry) {
        this.ask_entry = ask_entry;
    }

    public Coupos getCoupons() {
        return coupons;
    }

    public void setCoupons(Coupos coupons) {
        this.coupons = coupons;
    }

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }

    public HomeButtomFloat getButtomFloat() {
        return buttomFloat;
    }

    public void setButtomFloat(HomeButtomFloat buttomFloat) {
        this.buttomFloat = buttomFloat;
    }

    public List<NewZtCouponsBean> getNewZtCoupons() {
        return newZtCoupons;
    }

    public void setNewZtCoupons(List<NewZtCouponsBean> newZtCoupons) {
        this.newZtCoupons = newZtCoupons;
    }
}
