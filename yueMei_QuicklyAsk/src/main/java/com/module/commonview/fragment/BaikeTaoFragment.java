package com.module.commonview.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.OtherListApi;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.commonview.view.ScreenTitleView;
import com.module.commonview.view.SortScreenPopwin;
import com.module.community.controller.adapter.TaoListAdapter;
import com.module.community.model.bean.TaoListDataType;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.FilterDataApi;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 百科四级页面相关商品
 * Created by 裴成浩 on 2019/5/5
 */
public class BaikeTaoFragment extends YMBaseFragment {
    @BindView(R.id.baike_tao_screen)
    ScreenTitleView mScreen;
    @BindView(R.id.baike_tao_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.baike_tao_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.baike_tao_not_view)
    LinearLayout mNotView;

    private OtherListApi otherListApi;
    private String TAG = "BaikeTaoFragment";
    private int mPage = 1;
    private String latitude;
    private String longitude;
    private BaseCityPopwindows cityPop;
    private BaseSortPopupwindows sortPop;
    private SortScreenPopwin scerPop;
    private String mKey;
    private String mId;
    private String mParentId;

    private ArrayList<ProjcetList> kindStr = new ArrayList<>();      //筛选集合
    private String mSort = "1";                                 //排序的选中id
    private TaoListAdapter mProjectSkuAdapter;

    public static BaikeTaoFragment newInstance(String key, String id, String parentId) {
        BaikeTaoFragment fragment = new BaikeTaoFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("id", id);
        bundle.putString("parentId", parentId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_baike_tao;
    }

    @Override
    protected void initView(View view) {
        mKey = getArguments().getString("key");
        mId = getArguments().getString("id");
        mParentId = getArguments().getString("parentId");
        latitude = mFunctionManager.loadStr(FinalConstant.DW_LATITUDE, "0");
        longitude = mFunctionManager.loadStr(FinalConstant.DW_LONGITUDE, "0");

        mScreen.initView(true);
        mScreen.setCityTitle(Utils.getCity());
        mScreen.setOnEventClickListener(new ScreenTitleView.OnEventClickListener() {
            @Override
            public void onCityClick() {
                if (cityPop != null) {
                    if (cityPop.isShowing()) {
                        cityPop.dismiss();
                    } else {
                        cityPop.showPop();
                    }
                    mScreen.initCityView(cityPop.isShowing());
                }
            }

            @Override
            public void onSortClick() {
                if (sortPop != null) {
                    if (sortPop.isShowing()) {
                        sortPop.dismiss();
                    } else {
                        sortPop.showPop();
                    }
                    mScreen.initSortView(sortPop.isShowing());
                }
            }

            @Override
            public void onKindClick() {
                if (scerPop != null) {
                    if (scerPop.isShowing()) {
                        scerPop.dismiss();
                    } else {
                        scerPop.showPop();
                    }
                    mScreen.initKindView(scerPop.isShowing(), isScerPopSelected());
                }
            }
        });

        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });

    }

    @Override
    protected void initData(View view) {
        otherListApi = new OtherListApi();
        cityPop = new BaseCityPopwindows(mContext, mScreen);
        setSortData();
        loadKindData();
        loadingData();

        //城市回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                mScreen.setCityTitle(city);
                if (cityPop != null) {
                    cityPop.dismiss();
                    mScreen.initCityView(cityPop.isShowing());
                }

                refresh();
            }
        });

        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initCityView(false);
            }
        });
    }

    /**
     * 获取筛选的数据
     */
    private void loadKindData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("partId", "0");
        new FilterDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ArrayList<ProjcetData>>() {
            @Override
            public void onSuccess(ArrayList<ProjcetData> sxsxData) {

                scerPop = new SortScreenPopwin(mContext, mScreen, sxsxData);

                scerPop.setOnButtonClickListener(new SortScreenPopwin.OnButtonClickListener() {
                    @Override
                    public void onResetListener(View view) {
                        if (scerPop != null) {
                            kindStr.clear();
                            scerPop.resetData();
                        }
                    }

                    @Override
                    public void onSureListener(View view, ArrayList data) {
                        if (scerPop != null) {
                            kindStr = scerPop.getSelectedData();
                            refresh();
                            scerPop.dismiss();
                        }
                    }
                });

                //筛选关闭
                scerPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        mScreen.initKindView(false, isScerPopSelected());
                    }
                });
            }

        });
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        otherListApi.getHashMap().clear();
        otherListApi.addData("key", mKey);               //筛选词
        otherListApi.addData("page", mPage + "");        //页码
        otherListApi.addData("id", mId);                 //四级id
        otherListApi.addData("parentId", mParentId);     //二级id
        otherListApi.addData("sort", mSort);              //排序
        //筛选
        for (ProjcetList data : kindStr) {
            Log.e(TAG, "data.getPostName() == " + data.getPostName());
            Log.e(TAG, "data.getPostVal() == " + data.getPostVal());
            otherListApi.addData(data.getPostName(), data.getPostVal());
        }
        otherListApi.addData("flag", "3");              //解析模型
        otherListApi.addData("lon", longitude);
        otherListApi.addData("lat", latitude);

        otherListApi.getCallBack(mContext, otherListApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                List<HomeTaoData> lvHotIssueData = JSONUtil.jsonToArrayList(serverData.data, HomeTaoData.class);
                setNotDataView(lvHotIssueData);
                mPage++;
                //刷新隐藏
                if (mRefresh != null) {
                    mRefresh.finishRefresh();
                    if (lvHotIssueData.size() == 0) {
                        mRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mRefresh.finishLoadMore();
                    }

                    if (mProjectSkuAdapter == null) {
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                        mRecycler.setLayoutManager(linearLayoutManager);

                        mProjectSkuAdapter = new TaoListAdapter(mContext, lvHotIssueData);
                        mRecycler.setAdapter(mProjectSkuAdapter);

                        mProjectSkuAdapter.setOnItemClickListener(new TaoListAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(TaoListDataType taoData, int pos) {
                                HomeTaoData data = taoData.getTao();
                                HashMap<String, String> event_params = data.getEvent_params();
                                event_params.put("id", mId);
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BAIKE_TAOLIST_CLICK, (pos + 1) + ""), event_params, new ActivityTypeData("124"));
                                String id = data.get_id();
                                Intent it1 = new Intent(mContext, TaoDetailActivity.class);
                                it1.putExtra("id", id);
                                it1.putExtra("source", "96");
                                it1.putExtra("objid", "0");
                                mContext.startActivity(it1);
                            }

                        });
                    } else {
                        mProjectSkuAdapter.addData(lvHotIssueData);
                    }
                }

            }
        });
    }

    /**
     * 排序数据
     */
    private void setSortData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("3");
        a2.setName("价格从低到高");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("4");
        a4.setName("销量最高");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("5");
        a5.setName("日记最多");
        TaoPopItemData a6 = new TaoPopItemData();
        a6.set_id("7");
        a6.setName("离我最近");

        List<TaoPopItemData> lvSortData = new ArrayList<>();
        lvSortData.add(a1);
        lvSortData.add(a4);
        lvSortData.add(a5);
        lvSortData.add(a2);
        lvSortData.add(a6);
        sortPop = new BaseSortPopupwindows(mContext, mScreen, lvSortData);

        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                if (sortPop != null) {
                    sortPop.dismiss();
                    mSort = sortId;
                    mScreen.initSortView(sortPop.isShowing());
                    mScreen.setSortTitle(sortName);
                }
                refresh();
            }
        });

        sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initSortView(false);
            }
        });
    }

    /**
     * 设置没有数据的view
     * @param lvBBslistData
     */
    private void setNotDataView(List<HomeTaoData> lvBBslistData) {
        if(mPage == 1 && lvBBslistData.size()==0){
            mNotView.setVisibility(View.VISIBLE);
            mRefresh.setVisibility(View.GONE);
        }else{
            mNotView.setVisibility(View.GONE);
            mRefresh.setVisibility(View.VISIBLE);
        }
    }


    /**
     * 筛选中的数据是否有选中项
     *
     * @return ：true:有 false:无
     */
    private boolean isScerPopSelected() {
        return kindStr.size() != 0;
    }

    /**
     * 刷新
     */
    private void refresh() {
        mProjectSkuAdapter = null;
        mPage = 1;
        loadingData();
    }

}
