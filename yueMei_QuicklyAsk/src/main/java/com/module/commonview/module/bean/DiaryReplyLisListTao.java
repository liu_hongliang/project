package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/7.
 */
public class DiaryReplyLisListTao implements Parcelable {
    private String title;
    private String price_discount;
    private String url;

    public DiaryReplyLisListTao(){

    }

    protected DiaryReplyLisListTao(Parcel in) {
        title = in.readString();
        price_discount = in.readString();
        url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(price_discount);
        dest.writeString(url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryReplyLisListTao> CREATOR = new Creator<DiaryReplyLisListTao>() {
        @Override
        public DiaryReplyLisListTao createFromParcel(Parcel in) {
            return new DiaryReplyLisListTao(in);
        }

        @Override
        public DiaryReplyLisListTao[] newArray(int size) {
            return new DiaryReplyLisListTao[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
