package com.module.commonview.view.webclient;

import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;

/**
 * Created by 裴成浩 on 2018/1/23.
 */

public interface WebViewOnReceivedSslErrorClient {

    void onReceivedSslErrorClient(WebView view, SslErrorHandler handler, SslError error);
}
