package com.module.commonview.view;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

public class TaoDetailPop extends PopupWindow {
    LinearLayout mSkuClickShare;
    ImageView mSkuCollectIv;
    TextView mSkuCollectTxt;
    LinearLayout mSkuClickCollect;
    TextView mSkuMessageNmu;
    LinearLayout mSkuClickMessage;
    LinearLayout mSkuCliclHome;

    private Activity mActivity;
    private String mMessageNum;

    public TaoDetailPop(Activity activity,String messageNum) {
        this.mActivity=activity;
        this.mMessageNum=messageNum;
        initView();
    }

    private void initView() {
        final View view = View.inflate(mActivity, R.layout.sku_pop, null);
        setWidth(Utils.dip2px(133));
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        setContentView(view);
        setFocusable(true);//获取焦点
        setOutsideTouchable(true);//获取外部触摸事件
        setTouchable(true);
        update();
        mSkuClickShare= view.findViewById(R.id.sku_click_share);
//        mSkuCollectIv= view.findViewById(R.id.sku_collect_iv);
//        mSkuCollectTxt= view.findViewById(R.id.sku_collect_txt);
//        mSkuClickCollect= view.findViewById(R.id.sku_click_collect);
        mSkuMessageNmu= view.findViewById(R.id.sku_message_nmu);
        mSkuClickMessage= view.findViewById(R.id.sku_click_message);
        mSkuCliclHome= view.findViewById(R.id.sku_clicl_home);
        if (!TextUtils.isEmpty(mMessageNum) && !"0".equals(mMessageNum)){
            mSkuMessageNmu.setVisibility(View.VISIBLE);
            mSkuMessageNmu.setText(mMessageNum);
        }else {
            mSkuMessageNmu.setVisibility(View.GONE);
        }
        mSkuClickShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnPopItemClickListener.share(v);
            }
        });
//        mSkuClickCollect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mOnPopItemClickListener.collect(v);
//            }
//        });
        mSkuClickMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnPopItemClickListener.message(v);
            }
        });
        mSkuCliclHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnPopItemClickListener.home(v);
            }
        });

    }

//    public void setCollectTextAndImg(boolean isCollect){
//        if (isCollect){
//            mSkuCollectIv.setBackgroundResource(R.drawable.tao_collect_red);
//            mSkuCollectTxt.setText("已收藏");
//        }else {
//            mSkuCollectIv.setBackgroundResource(R.drawable.tao_collect_hui);
//            mSkuCollectTxt.setText("收藏");
//        }
//    }

    private OnPopItemClickListener mOnPopItemClickListener;

    public void setOnPopItemClickListener(OnPopItemClickListener onPopItemClickListener) {
        mOnPopItemClickListener = onPopItemClickListener;
    }

    public interface OnPopItemClickListener{
        void share(View view);
//        void collect(View view);
        void message(View view);
        void home(View view);
    }


}
