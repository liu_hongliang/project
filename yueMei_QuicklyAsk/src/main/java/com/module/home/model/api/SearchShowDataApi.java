package com.module.home.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.home.model.bean.SearchXSData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * Created by Administrator on 2018/2/8.
 */

public class SearchShowDataApi implements BaseCallBackApi  {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.HOME,"searchk", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.d("HomeActivity623",mData.toString());
                try {
                    SearchXSData searchXSData = JSONUtil.TransformSingleBean(mData.data, SearchXSData.class);
                    listener.onSuccess(searchXSData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
