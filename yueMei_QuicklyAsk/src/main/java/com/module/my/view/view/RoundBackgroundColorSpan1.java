package com.module.my.view.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.style.ReplacementSpan;

import com.quicklyask.util.Utils;

/**
 * 搜索页面提示语后边标签
 * Created by 裴成浩 on 2019/5/28
 */
public class RoundBackgroundColorSpan1 extends ReplacementSpan {

    private int mSize;
    private int bgColor;
    private int mRadius;
    private int mTextColor;
    private final int mTopAndBottomPadding;

    /**
     * @param color               背景颜色
     * @param textColor           文字颜色
     * @param radius              圆角半径(左右内边距)
     * @param topAndBottomPadding 上下内边距
     */
    public RoundBackgroundColorSpan1(int color, int textColor, int radius, int topAndBottomPadding) {
        bgColor = color;
        mTextColor = textColor;
        mRadius = radius;
        mTopAndBottomPadding = topAndBottomPadding;
    }

    @Override
    public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
        mSize = (int) (paint.measureText(text, start, end) + Utils.dip2px(1) * mRadius);
        return mSize;
    }

    /**
     * @param canvas ：画布
     * @param text   ：要绘制的文字
     * @param start
     * @param end
     * @param x      :当前所在坐标X
     * @param top
     * @param y      ：当前所在坐标Y
     * @param bottom
     * @param paint  ：画笔
     */
    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        paint.setColor(bgColor);//设置背景颜色
        paint.setAntiAlias(true);// 设置画笔的锯齿效果

        //文字高度  顶线-底线
        float centHeight = paint.descent() - paint.ascent();

        RectF oval = new RectF(x, y - (mTopAndBottomPadding * 2), x + mSize, y + mTopAndBottomPadding);
        canvas.drawRoundRect(oval, mRadius, mRadius, paint);
        paint.setColor(mTextColor);

        canvas.drawText(text, start, end, x + mRadius, y, paint);
    }

}
