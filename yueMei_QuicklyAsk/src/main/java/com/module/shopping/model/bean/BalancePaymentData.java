package com.module.shopping.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2018/12/14
 */
public class BalancePaymentData implements Parcelable {
    private int hosPos;
    private YouHuiCoupons coupons;                     //选中的尾款红包
    private ArrayList<YouHuiCoupons> wk_coupons;            //尾款红包列表

    public BalancePaymentData() {
    }

    protected BalancePaymentData(Parcel in) {
        hosPos = in.readInt();
        coupons = in.readParcelable(YouHuiCoupons.class.getClassLoader());
        wk_coupons = in.createTypedArrayList(YouHuiCoupons.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(hosPos);
        dest.writeParcelable(coupons, flags);
        dest.writeTypedList(wk_coupons);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BalancePaymentData> CREATOR = new Creator<BalancePaymentData>() {
        @Override
        public BalancePaymentData createFromParcel(Parcel in) {
            return new BalancePaymentData(in);
        }

        @Override
        public BalancePaymentData[] newArray(int size) {
            return new BalancePaymentData[size];
        }
    };

    public int getHosPos() {
        return hosPos;
    }

    public void setHosPos(int hosPos) {
        this.hosPos = hosPos;
    }

    public YouHuiCoupons getCoupons() {
        return coupons;
    }

    public void setCoupons(YouHuiCoupons coupons) {
        this.coupons = coupons;
    }

    public ArrayList<YouHuiCoupons> getWk_coupons() {
        return wk_coupons;
    }

    public void setWk_coupons(ArrayList<YouHuiCoupons> wk_coupons) {
        this.wk_coupons = wk_coupons;
    }
}
