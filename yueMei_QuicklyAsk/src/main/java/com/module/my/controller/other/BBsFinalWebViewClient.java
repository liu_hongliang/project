package com.module.my.controller.other;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.DeleteBBsApi;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.my.controller.activity.BBsFinalWebActivity;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

/**
 * Created by 裴成浩 on 2018/1/23.
 */

public class BBsFinalWebViewClient implements BaseWebViewClientCallback {

    private BBsFinalWebActivity mActivity;
    private Intent intent;
    private String uid;
    private String qid;
    private String cid;
    private String askorshare = "";
    private String sumbitContent1;
    private static final int SHOW_TIME = 1000;

    public BBsFinalWebViewClient(BBsFinalWebActivity mActivity) {
        this.mActivity = mActivity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }
    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 医生详情页

                String id = obj.getString("id");
                String docname = URLDecoder.decode(
                        obj.getString("docname"), "utf-8");

                intent.setClass(mActivity, DoctorDetailsActivity592.class);
                intent.putExtra("docId", id);
                intent.putExtra("docName", docname);
                intent.putExtra("partId", "");
                mActivity.startActivity(intent);

                break;
            case "2":   // 回复问题
                sethuifu(obj);
                break;

            case "414": // 删除楼中帖 接口
                String id414 = obj.getString("id");
                String reply414 = obj.getString("reply");
                String userid414 = obj.getString("userid");
                deleAnswerDilogR(id414, userid414, reply414);
                break;

            case "5441":// 回复问题
                if (Utils.isLogin()) {
                    if (Utils.isBind()){
                        try {
                            cid = obj.getString("cid");
                            qid = obj.getString("qid");
                            askorshare = obj.getString("askorshare");

                            String docname5441 = URLDecoder.decode(
                                    obj.getString("docname"), "utf-8");
                            String inputStr = "回复 " + docname5441 + "： ";

                            // 弹出输入框
                            mActivity.inputTxtRly.setVisibility(View.VISIBLE);

                            mActivity.inputTxtRly.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View arg0) {

                                }
                            });
                            mActivity.inputContentEt.setText(inputStr);
                            mActivity.inputContentEt.setSelection(mActivity.inputContentEt.getText()
                                    .length());
                            mActivity.inputContentEt.requestFocus();
                            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(0,
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                            // 取消
                            mActivity.cancelRly.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View arg0) {
                                    mActivity.inputTxtRly.setVisibility(View.GONE);
                                    View view = mActivity.getWindow().peekDecorView();
                                    if (view != null) {
                                        InputMethodManager inputmanger = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                                        inputmanger.hideSoftInputFromWindow(
                                                view.getWindowToken(), 0);
                                    }

                                    mActivity.biaoqingContentLy.setVisibility(View.GONE);
                                }
                            });
                            // 提交
                            mActivity.sumbitBt.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View arg0) {
                                    if (Utils.isFastDoubleClick()) {
                                        return;
                                    }
                                    sumbitContent1 = mActivity.inputContentEt.getText()
                                            .toString().trim();

                                    Matcher matcher1 = mActivity.emoji
                                            .matcher(sumbitContent1);
                                    if (matcher1.find()) {
                                        Toast.makeText(mActivity,
                                                "暂不支持表情输入", Toast.LENGTH_SHORT)
                                                .show();
                                    } else {

                                        if (sumbitContent1.length() > 0) {
                                            if (sumbitContent1.length() > 1) {
                                                mActivity.baseWebViewClientMessage.startLoading();

                                                mActivity.inputTxtRly
                                                        .setVisibility(View.GONE);
                                                mActivity.biaoqingContentLy
                                                        .setVisibility(View.GONE);
                                                sumbitHttp();
                                                mActivity.inputContentEt.setText("");
                                            } else {
                                                ViewInject.toast("亲，内容至少要大于1个字哟！");
                                            }
                                        } else {
                                            ViewInject.toast("内容不能为空");
                                        }
                                    }
                                }
                            });

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }else {
                        Utils.jumpBindingPhone(mActivity);

                    }

                } else {
                    Utils.jumpLogin(mActivity);
                }
                break;
        }
    }

    private void sethuifu(JSONObject obj) throws JSONException {
        if (Utils.isLogin()) {
            if (Utils.isBind()){
                try {
                    cid = obj.getString("cid");
                    qid = obj.getString("qid");
                    askorshare = obj.getString("askorshare");

                    String docname = URLDecoder.decode(
                            obj.getString("docname"), "utf-8");
                    String inputStr = "@" + docname + ":";

                    // 弹出输入框
                    mActivity.inputTxtRly.setVisibility(View.VISIBLE);
                    mActivity.inputContentEt.setSelection(mActivity.inputContentEt.getText().length());
                    mActivity.inputContentEt.requestFocus();
                    InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(0,
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    mActivity.inputTxtRly.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                        }
                    });
                    mActivity.inputContentEt.setText("");
                    // 取消
                    mActivity.cancelRly.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            mActivity.inputContentEt.setText("");
                            mActivity.inputTxtRly.setVisibility(View.GONE);
                            View view = mActivity.getWindow().peekDecorView();
                            if (view != null) {
                                InputMethodManager inputmanger = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                                inputmanger.hideSoftInputFromWindow(
                                        view.getWindowToken(), 0);
                            }
                        }
                    });
                    // 提交
                    mActivity.sumbitBt.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            sumbitContent1 = mActivity.inputContentEt.getText()
                                    .toString().trim();

                            Matcher matcher = mActivity.emoji.matcher(sumbitContent1);

                            if (matcher.find()) {
                                Toast.makeText(mActivity,
                                        "暂不支持表情输入", Toast.LENGTH_SHORT)
                                        .show();
                            } else {

                                if (sumbitContent1.length() > 0) {
                                    if (sumbitContent1.length() > 1) {
                                        mActivity.baseWebViewClientMessage.startLoading();
                                        mActivity.inputTxtRly
                                                .setVisibility(View.GONE);
                                        sumbitHttp();

                                        View view = mActivity.getWindow()
                                                .peekDecorView();
                                        if (view != null) {
                                            InputMethodManager inputmanger = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                                            inputmanger
                                                    .hideSoftInputFromWindow(
                                                            view.getWindowToken(),
                                                            0);
                                        }
                                    } else {
                                        ViewInject.toast("亲，内容至少要大于1个字哟！");
                                    }
                                } else {
                                    ViewInject.toast("内容不能为空");
                                }
                            }
                        }
                    });

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }else {
                Utils.jumpBindingPhone(mActivity);

            }

        } else {
            Utils.jumpLogin(mActivity);
        }
    }

    public void deleAnswerDilogR(final String aid, final String uid,
                                 final String reply) {
        AlertDialog.Builder ad = new AlertDialog.Builder(
                mActivity);
        ad.setTitle("提示");
        ad.setMessage("确定删除吗?");
        ad.setPositiveButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                // 不退出不用执行任何操作
            }
        });

        ad.setNegativeButton("是", new DialogInterface.OnClickListener() {// 退出按钮
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                deleteBBs(aid, reply);
            }
        });

        ad.show();// 显示对话框
    }

    /**
     * 删除帖子 或 回复
     *
     * @param id
     */
    void deleteBBs(String id,String replystr) {

        Map<String,Object> maps=new HashMap<>();
        maps.put("id",id);
        maps.put("reply",replystr);
        new DeleteBBsApi().getCallBack(mActivity, maps, new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String message) {
                mActivity.webReload();
            }
        });
    }

    /**
     * 回复
     */
    void sumbitHttp() {
        Map<String,Object> params = new HashMap<>();
        params.put("uid", uid);
        params.put("userid", "0");
        params.put("qid", qid);
        params.put("cid", cid);
        params.put("type", "1");
        params.put("askorshare", askorshare);
        params.put("content", sumbitContent1);
        params.put("visibility", "0");
        SumitHttpAip sumitHttpAip = new SumitHttpAip();
        sumitHttpAip.getCallBack(mActivity, params, new BaseCallBackListener() {
            @Override
            public void onSuccess(Object o) {
                sumitHttpCode("7");
                mActivity.inputContentEt.setText("");
                mActivity.webReload();
            }
        });
    }

    void sumitHttpCode(final String flag) {
        SumitHttpAip sumitHttpAip = new SumitHttpAip();
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag",flag);
        maps.put("uid",uid);
        sumitHttpAip.getCallBack(mActivity, maps, new BaseCallBackListener<JFJY1Data>() {
            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                if (jfjyData != null){
                    String jifenNu = jfjyData.getIntegral();
                    String jyNu = jfjyData.getExperience();

                    Log.e("TAG", "jifenNu == " + jifenNu);
                    Log.e("TAG", "jyNu == " + jyNu);
                    if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                        MyToast.makeTexttext4Toast(mActivity, jifenNu, jyNu,
                                SHOW_TIME).show();
                    } else {
                        if (!jifenNu.equals("0")) {
                            MyToast.makeTexttext2Toast(mActivity, jifenNu,
                                    SHOW_TIME).show();
                        } else {
                            if (!jyNu.equals("0")) {
                                MyToast.makeTexttext3Toast(mActivity, jyNu,
                                        SHOW_TIME).show();
                            }
                        }
                    }
                }
            }

        });
    }

}
