package com.module.my.view.view;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.module.my.model.bean.UploadImageSuccessData;
import com.quicklyask.entity.WriteVideoResult;

/**
 * Created by 裴成浩 on 2018/8/6.
 */
public class PostingAndNoteHandler extends Handler {

    private String TAG = "PostingAndNoteHandler";

    public static final int JIN_DU = 2;         //上传进度更新
    public static final int WANCHENG_S_C = 3;   //上传完成（成功）
    public static final int SHIBAI_S_C = 4;     //上传失败
    public static final int VIDEO_PROGRESS = 7;     //视频上传进度
    public static final int VIDEO_SUCCESS = 8;     //视频上传成功
    public static final int VIDEO_FAILURE = 9;     //视频上传失败

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what) {
            case JIN_DU:                    //图片上传进度更新
                int progress = msg.arg1;

                onClickListener.onProgressClick(false, progress);

                break;
            case WANCHENG_S_C:              //图片上传上传完成（成功）

                UploadImageSuccessData imgUrl = (UploadImageSuccessData) msg.obj;

                onClickListener.onSuccessClick(false, null, imgUrl);

                break;
            case SHIBAI_S_C:                //图片上传上传失败

                onClickListener.onFailureClick(false);

                break;

            case VIDEO_PROGRESS:            //视频上传进度
                int videoprog = msg.arg1;

                Log.e(TAG, "videoprog === " + videoprog);

                onClickListener.onProgressClick(true, videoprog);

                break;
            case VIDEO_SUCCESS:             //视频上传成功
                WriteVideoResult writeVideoResult = (WriteVideoResult) msg.obj;
                onClickListener.onSuccessClick(true, writeVideoResult, null);
                break;
            case VIDEO_FAILURE:             //视频上传失败

                onClickListener.onFailureClick(true);

                break;
        }
    }


    private OnClickListener onClickListener;

    public interface OnClickListener {
        void onProgressClick(boolean isVideo, int progress);                                                      //上传进度

        void onSuccessClick(boolean isVideo, WriteVideoResult writeVideoResult, UploadImageSuccessData imgUrl);  //成功回调

        void onFailureClick(boolean isVideo);                                                                   //失败回调
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

}
