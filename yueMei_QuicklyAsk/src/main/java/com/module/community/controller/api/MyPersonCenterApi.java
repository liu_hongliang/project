package com.module.community.controller.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.community.model.bean.BBsListData550;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.List;
import java.util.Map;

/**
 * 我的帖子接口
 * Created by 裴成浩 on 2018/3/13.
 */

public class MyPersonCenterApi implements BaseCallBackApi {
    private String TAG = "MyPersonCenterApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "mypost", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e("MyPersonCenterApi",mData.toString());
                if("1".equals(mData.code)){
                    List<BBsListData550> docData = JSONUtil.jsonToArrayList(mData.data,BBsListData550.class);
                    listener.onSuccess(docData);
                }
            }
        });
    }
}