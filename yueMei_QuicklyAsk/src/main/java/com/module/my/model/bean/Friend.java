package com.module.my.model.bean;

import org.kymjs.aframe.database.annotate.Id;

/**
 * 
 * @author AM 模拟你的好友信息 bean
 * 
 */
public class Friend {

	@Id()
	private int id;

	private String userId;

	private String userName;

	private String portraitUri;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the portraitUri
	 */
	public String getPortraitUri() {
		return portraitUri;
	}

	/**
	 * @param portraitUri
	 *            the portraitUri to set
	 */
	public void setPortraitUri(String portraitUri) {
		this.portraitUri = portraitUri;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Friend [id=" + id + ", userId=" + userId + ", userName="
				+ userName + ", portraitUri=" + portraitUri + "]";
	}

}
