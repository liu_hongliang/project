package com.module.home.model.bean;

public class HospitalTop {

    /**
     * level : 1
     * desc : 北京面部轮廓月销TOP1医院
     * bilateral_desc : 北京面部轮廓热销榜
     */

    private int level;
    private String desc;
    private String bilateral_desc;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getBilateral_desc() {
        return bilateral_desc;
    }

    public void setBilateral_desc(String bilateral_desc) {
        this.bilateral_desc = bilateral_desc;
    }
}
