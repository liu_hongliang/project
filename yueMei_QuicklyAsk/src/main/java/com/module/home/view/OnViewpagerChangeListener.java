package com.module.home.view;

public interface OnViewpagerChangeListener {

    void onChange(int currentPage);
}
