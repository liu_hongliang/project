package com.quicklyask.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class YueMeiDialog extends Dialog implements android.view.View.OnClickListener {

    /**
     * 上下文对象
     **/
    Context context;
    /**
     * 左边按钮
     **/
    private Button mLeftBtn;
    /**
     * 右边按钮
     **/
    private Button mRightBtn;
    private String mMessage;
    private String mLeftMsg;
    private String mRightMsg;
    private TextView mTip;


    public YueMeiDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public YueMeiDialog(Context context, String message, String leftMsg, String rightMsg) {
        super(context, R.style.mystyle);
        this.context = context;
        this.mMessage = message;
        this.mLeftMsg = leftMsg;
        this.mRightMsg = rightMsg;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 指定布局
        this.setContentView(R.layout.yueme_dialog);

        // 根据id在布局中找到控件对象
        mTip = findViewById(R.id.yuemei_tip);
        mLeftBtn = findViewById(R.id.yuemei_left);
        mRightBtn = findViewById(R.id.yuemei_right);

        //设置内容
        mTip.setText(mMessage);
        mLeftBtn.setText(mLeftMsg);
        mRightBtn.setText(mRightMsg);

        // 为按钮绑定点击事件监听器
        mLeftBtn.setOnClickListener(this);
        mRightBtn.setOnClickListener(this);

        setCanceledOnTouchOutside(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.yuemei_left:
                if (mBtnClickListener != null) {
                    mBtnClickListener.leftBtnClick();
                }
                break;
            case R.id.yuemei_right:
                if (mBtnClickListener != null) {
                    mBtnClickListener.rightBtnClick();
                }
                break;
        }
    }

    public void setBtnClickListener(BtnClickListener btnClickListener) {
        mBtnClickListener = btnClickListener;
    }

    private BtnClickListener mBtnClickListener;

    public interface BtnClickListener {
        void leftBtnClick();

        void rightBtnClick();
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public void setmLeftMsg(String mLeftMsg) {
        this.mLeftMsg = mLeftMsg;
    }

    public void setmRightMsg(String mRightMsg) {
        this.mRightMsg = mRightMsg;
    }
}
