package com.quicklyask.entity;

public class DiscTop1 {
	private String code;
	private String message;
	private DiscTop1Data data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public DiscTop1Data getData() {
		return data;
	}

	public void setData(DiscTop1Data data) {
		this.data = data;
	}

}
