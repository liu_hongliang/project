package com.quicklyask.entity;

import com.module.my.model.bean.PostingVideoCover;

/**
 * 上传视频的json串
 * Created by 裴成浩 on 2018/8/15.
 */
public class UploadVideoJson {
    private PostingVideoCover cover;
    private String file_size;
    private String mimetype;
    private String url;
    private String video_time;

    public PostingVideoCover getCover() {
        return cover;
    }

    public void setCover(PostingVideoCover cover) {
        this.cover = cover;
    }

    public String getFile_size() {
        return file_size;
    }

    public void setFile_size(String file_size) {
        this.file_size = file_size;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVideo_time() {
        return video_time;
    }

    public void setVideo_time(String video_time) {
        this.video_time = video_time;
    }
}
