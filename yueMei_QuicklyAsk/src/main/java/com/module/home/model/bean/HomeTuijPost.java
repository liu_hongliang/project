package com.module.home.model.bean;

import java.util.List;

/**
 * Created by 裴成浩 on 2017/8/17.
 */

public class HomeTuijPost {
    private String code;
    private String message;
    private List<TuijPost> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TuijPost> getData() {
        return data;
    }

    public void setData(List<TuijPost> data) {
        this.data = data;
    }
}
