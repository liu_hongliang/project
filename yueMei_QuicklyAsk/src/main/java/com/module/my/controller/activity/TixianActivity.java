package com.module.my.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.my.controller.other.ActivityCollector;
import com.module.my.controller.other.EditInputFilter;
import com.module.my.model.api.TixianApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.view.WalletSecurityCodePop;
import com.quicklyask.view.YueMeiDialog2;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TixianActivity extends YMBaseActivity {
    public static final String TAG = "TixianActivity";
    private Context mContext;
    @BindView(R.id.tixian_content)
    LinearLayout mTixianContent;
    @BindView(R.id.tixian_message)
    TextView mTixianMessage;
    @BindView(R.id.wallet_tixian_title)
    CommonTopBar mWalletTixianTitle;
    @BindView(R.id.tixian_account_txt)
    TextView mTixianAccountTxt;
    @BindView(R.id.ymwallet_account_txt)
    TextView mYmwalletAccountTxt;
    @BindView(R.id.tixian_shoukuan_rly)
    RelativeLayout mTixianShoukuanRly;
    @BindView(R.id.tixian_money_txt)
    TextView mTixianMoneyTxt;
    @BindView(R.id.tixian_money_et)
    EditText mTixianMoneyEt;
    @BindView(R.id.tixian_clear)
    ImageView mTixianClear;
    @BindView(R.id.tixian_tip_txt)
    TextView mTixianTipTxt;
    @BindView(R.id.tixian_ask_iv)
    ImageView mTixianAskIv;
    @BindView(R.id.tixian_shuoming_ll)
    LinearLayout mTixianShuomingLl;
    @BindView(R.id.tixian_all_txt)
    TextView mTixianAllTxt;
    @BindView(R.id.tixian_tip2)
    TextView mTixianTip2;
    @BindView(R.id.tixian_btn)
    Button mTixianBtn;
    private String mOnExtractBalance;
    private String mRealName;
    private String mProceedsAccount;
    private float mMonthMoney;
    private int mMonthMaxMoney;
    private int mDayMaxLimit;
    private int mWithdrawNum;
    private float mWithdrawMonthNum;
    private int mEveryMinMoney;
    private int mAccount_type;
    private float mExtrachBalance;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_tixian;
    }

    @Override
    protected void initView() {
        mContext = TixianActivity.this;
        InputFilter[] filters = {new EditInputFilter()};
        mTixianMoneyEt.setFilters(filters);
        Intent intent = getIntent();
        mOnExtractBalance = intent.getStringExtra("on_extract_balance");
        mMonthMoney = intent.getFloatExtra("month_money", 0);//用户本月可提现金额
        mMonthMaxMoney = intent.getIntExtra("month_max_money", 0);//每月最大可提现金额
        mDayMaxLimit = intent.getIntExtra("day_max_limit", 0);//每天最大可提现次数
        mWithdrawNum = intent.getIntExtra("withdrawNum", 0);//用户当天申请提现次数
        mWithdrawMonthNum = intent.getFloatExtra("withdrawMonthNum", 0);//实名用户当月的提现总金额
        mRealName = intent.getStringExtra("real_name");
        mProceedsAccount = intent.getStringExtra("proceeds_account");
        mAccount_type = intent.getIntExtra("account_type", -1);
        mEveryMinMoney = intent.getIntExtra("every_min_money", 0);//每次提现最低金额
        mTixianAccountTxt.setText(mRealName);
        mYmwalletAccountTxt.setText(mProceedsAccount);
        //a.超过可提现金额，提示“金额已超过可提现金额”，提现按钮不可点
        //b.输入金额不足50元，提示“单次满50元才可申请提现”，提现按钮不可点
        //c.普通用户输入金额大于800元，提示“每月累计提现金额最高800元”
        //该账号可提现余额>800-该实名认证人本月累计提现，本月可提现金额=800-该实名认证人本月累计提现
        //本月可提现余额<=800-该实名认证人本月累计提现，本月可提现金额=该账号可提现余额

        mTixianTipTxt.setText("当前可提现金额" + mMonthMoney + "元");
        switch (mAccount_type) {
            case 0:
                mTixianMessage.setText("提现至支付宝");
                break;
            case 1:
                mTixianMessage.setText("提现至银行卡");
                break;
        }
        mTixianMoneyEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s)) {
                    mTixianClear.setVisibility(View.VISIBLE);
                    float money = Float.parseFloat(s + "");
                    if (money > mMonthMaxMoney) {
                        mTixianTipTxt.setTextColor(mFunctionManager.getLocalColor(R.color.red));
                        mTixianTipTxt.setText("每月累计提现金额最高" + mMonthMaxMoney + "元");
                        return;
                    } else {
                        mTixianTipTxt.setTextColor(mFunctionManager.getLocalColor(R.color._bb));
                        mTixianTipTxt.setText("当前可提现金额" + mMonthMoney + "元");
                    }
                    if (money > mMonthMoney) {
                        mTixianTipTxt.setTextColor(mFunctionManager.getLocalColor(R.color.red));
                        mTixianTipTxt.setText("金额已超过可提现金额");
                        return;
                    } else {
                        mTixianTipTxt.setTextColor(mFunctionManager.getLocalColor(R.color._bb));
                        mTixianTipTxt.setText("当前可提现金额" + mMonthMoney + "元");
                    }
                    if (money < mEveryMinMoney) {
                        mTixianTipTxt.setTextColor(mFunctionManager.getLocalColor(R.color.red));
                        mTixianTipTxt.setText("单次满" + mEveryMinMoney + "元才可申请提现");
                        return;
                    } else {
                        mTixianTipTxt.setTextColor(mFunctionManager.getLocalColor(R.color._bb));
                        mTixianTipTxt.setText("当前可提现金额" + mMonthMoney + "元");
                    }
                    mTixianBtn.setEnabled(true);
                    mTixianBtn.setBackgroundResource(R.drawable.renzheng_background);

                } else {
                    mTixianTipTxt.setTextColor(mFunctionManager.getLocalColor(R.color._bb));
                    mTixianTipTxt.setText("当前可提现金额" + mMonthMoney + "元");
                    mTixianClear.setVisibility(View.GONE);
                    mTixianBtn.setEnabled(false);
                    mTixianBtn.setBackgroundResource(R.drawable.shoukuan_save_background);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mWalletTixianTitle.setBottomLineVisibility(View.GONE);
        mWalletTixianTitle.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                //客服跳转
                Intent it = new Intent();
                it.setClass(mContext, OnlineKefuWebActivity.class);
                it.putExtra("link", "/service/zxzx/");
                it.putExtra("title", "在线客服");
                startActivity(it);
            }
        });


    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.tixian_clear, R.id.tixian_shoukuan_rly, R.id.tixian_shuoming_ll, R.id.tixian_all_txt, R.id.tixian_btn, R.id.tixian_tip2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tixian_clear:
                mTixianMoneyEt.setText("");
                break;
            case R.id.tixian_shoukuan_rly:
                Intent intent = new Intent(this, AccountShoukuanActivity.class);
                intent.putExtra("sk_name", mRealName);
                intent.putExtra("sk_account", mProceedsAccount);
                intent.putExtra("sk_account_type", mAccount_type);
                startActivity(intent);
                break;
            case R.id.tixian_shuoming_ll:
                Intent intent3 = new Intent(this, WalletMingxiActivity.class);
                intent3.putExtra("jump_type", "3");//1,钱包明细   2，钱包说明  3，提现说明
                intent3.putExtra("title", "说明");
                startActivity(intent3);
                break;
            case R.id.tixian_all_txt:
                mTixianMoneyEt.setText(mMonthMoney + "");
                mTixianMoneyEt.setSelection(mTixianMoneyEt.getText().length());
                break;
            case R.id.tixian_btn:  //提现按钮
                String content = mTixianMoneyEt.getText().toString().trim();
                final double money = Double.parseDouble(content);
                if (mWithdrawNum >= mDayMaxLimit) {  //判断当前用户提现是否大于mDayMaxLimit次
                    final YueMeiDialog2 yueMeiDialog2 = new YueMeiDialog2(mContext, "每天最多可提现" + mDayMaxLimit + "次，请明天再来", "确定");
                    yueMeiDialog2.setCanceledOnTouchOutside(false);
                    yueMeiDialog2.show();
                    yueMeiDialog2.setBtnClickListener(new YueMeiDialog2.BtnClickListener2() {
                        @Override
                        public void BtnClick() {
                            yueMeiDialog2.dismiss();
                        }
                    });
                    return;
                }

                if (mWithdrawMonthNum + money > mMonthMaxMoney) { //判断当前用户 当月累计提现金额+本次提现金额 是否大于每月最大可提现金额
                    final YueMeiDialog2 yueMeiDialog2 = new YueMeiDialog2(mContext, "您当月提现已满" + mMonthMaxMoney + "元上限（此前累计提现金额" + mWithdrawMonthNum + "元，同用户多个收款账号共享此额度），请调整提现金额或下月再操作提现。", "确定");
                    yueMeiDialog2.setCanceledOnTouchOutside(false);
                    yueMeiDialog2.show();
                    yueMeiDialog2.setBtnClickListener(new YueMeiDialog2.BtnClickListener2() {
                        @Override
                        public void BtnClick() {
                            yueMeiDialog2.dismiss();
                        }
                    });
                    return;
                }

                WalletSecurityCodePop securityCodePop = new WalletSecurityCodePop(mContext);
                securityCodePop.showAtLocation(mTixianContent, Gravity.BOTTOM, 0, 0);
                securityCodePop.setOnTureClickListener(new WalletSecurityCodePop.OnTrueClickListener() {
                    @Override
                    public void onTrueClick(String codes) {
                        HashMap<String, Object> maps = new HashMap<>();
                        maps.put("applyMoney", (money * 100) + "");
                        maps.put("code", codes);
                        new TixianApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData serverData) {
                                if ("1".equals(serverData.code)) {
                                    Intent intent1 = new Intent(mContext, WalletResultAcitvity.class);
                                    ActivityCollector.addActivity(TixianActivity.this);
                                    startActivity(intent1);
                                } else {
                                    mFunctionManager.showShort(serverData.message);
                                }
                            }
                        });
                    }
                });

                break;
            case R.id.tixian_tip2:
                Intent intent1 = new Intent(TixianActivity.this, WalletMingxiActivity.class);
                intent1.putExtra("jump_type", "4");
                intent1.putExtra("title", "提现协议");
                startActivity(intent1);
                break;
        }
    }
}
