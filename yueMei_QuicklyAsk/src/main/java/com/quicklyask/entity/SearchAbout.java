/**
 * 
 */
package com.quicklyask.entity;

import com.module.community.model.bean.SearchAboutData;

import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class SearchAbout {

	private String code;
	private String message;
	private List<SearchAboutData> data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public List<SearchAboutData> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<SearchAboutData> data) {
		this.data = data;
	}


}
