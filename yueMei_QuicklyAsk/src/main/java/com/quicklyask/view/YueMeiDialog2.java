package com.quicklyask.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class YueMeiDialog2 extends Dialog implements android.view.View.OnClickListener{
    /** 上下文对象 **/
    Context context;
    /** 中间按钮 **/
    private Button mButton;
    private String mMessage;
    private String mButtonMsg;
    private TextView mTip;


    public YueMeiDialog2(@NonNull Context context) {
        super(context);
        this.context = context;
    }


    public YueMeiDialog2(Context context, String message,String buttonMsg) {
        super(context, R.style.mystyle);
        this.context = context;
        this.mMessage=message;
        this.mButtonMsg=buttonMsg;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 指定布局
        this.setContentView(R.layout.yuemei_dialog2);
        // 根据id在布局中找到控件对象
        mTip = findViewById(R.id.yuemei_tip);
        mButton =  findViewById(R.id.yuemei_button);
        // 设置按钮的文本颜色
        //设置内容
        mTip.setText(mMessage);
        mButton.setText(mButtonMsg);
        // 为按钮绑定点击事件监听器
        mButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.yuemei_button:
                mBtnClickListener2.BtnClick();
                break;
        }
    }

    public void setBtnClickListener(YueMeiDialog2.BtnClickListener2 btnClickListener2) {
        mBtnClickListener2 = btnClickListener2;
    }

    private YueMeiDialog2.BtnClickListener2 mBtnClickListener2;

    public interface BtnClickListener2{
        void BtnClick();
    }
}
