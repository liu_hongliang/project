/**
 * 
 */
package com.module.doctor.model.bean;

import com.module.commonview.module.bean.ShareWechat;

/**
 * 医院分享
 * 
 * @author Robin
 * 
 */
public class HosShareData {
	private String _id;
	private String hos_name;
	private String address;
	private String img;
	private String url;
	private String doc_name;
	private ShareWechat Wechat;

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the hos_name
	 */
	public String getHos_name() {
		return hos_name;
	}

	/**
	 * @param hos_name
	 *            the hos_name to set
	 */
	public void setHos_name(String hos_name) {
		this.hos_name = hos_name;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the img
	 */
	public String getImg() {
		return img;
	}

	/**
	 * @param img
	 *            the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}


	public ShareWechat getWechat() {
		return Wechat;
	}

	public void setWechat(ShareWechat wechat) {
		Wechat = wechat;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}
}
