package com.quicklyask.entity;

import java.util.List;

/**
 * 大家都聊
 * 
 * @author Rubin
 * 
 */
public class HotIssue {
	private String code;
	private String message;
	private List<HotIssueData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<HotIssueData> getData() {
		return data;
	}

	public void setData(List<HotIssueData> data) {
		this.data = data;
	}

}
