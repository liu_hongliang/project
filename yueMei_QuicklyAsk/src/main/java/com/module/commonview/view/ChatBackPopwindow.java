package com.module.commonview.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.ChatBackBean;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebData;
import com.module.community.web.WebUtil;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;
import java.util.List;

public class ChatBackPopwindow extends PopupWindow {

    public ChatBackPopwindow (final Activity mContext, final String taoId , ChatBackBean chatBackBean){
        final View view = View.inflate(mContext, R.layout.chat_compar, null);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setContentView(view);
        update();

        LinearLayout chatComparBack=view.findViewById(R.id.chat_compar_back);
        LinearLayout doctorClick1=view.findViewById(R.id.doctor_click1);
        LinearLayout doctorClick2=view.findViewById(R.id.doctor_click2);
        TextView chatComparTitle=view.findViewById(R.id.chat_compar_title);
        TextView chatComparOhter=view.findViewById(R.id.chat_compar_ohter);
        ImageView chatComparDoctorImg1=view.findViewById(R.id.chat_compar_doctor_img1);
        ImageView chatComparDoctorImg2=view.findViewById(R.id.chat_compar_doctor_img2);


        TextView chatComparDoctorTag1=view.findViewById(R.id.chat_compar_doctor_tag1);
        TextView chatComparDoctorTag2=view.findViewById(R.id.chat_compar_doctor_tag2);

        TextView chatComparDoctorName1=view.findViewById(R.id.chat_compar_doctor_name1);
        TextView chatComparDoctorName2=view.findViewById(R.id.chat_compar_doctor_name2);

        TextView chatComparDoctorLevel1=view.findViewById(R.id.chat_compar_doctor_level1);
        TextView chatComparDoctorLevel2=view.findViewById(R.id.chat_compar_doctor_level2);

        TextView chatComparDoctorHos1=view.findViewById(R.id.chat_compar_doctor_hospital1);
        TextView chatComparDoctorHos2=view.findViewById(R.id.chat_compar_doctor_hospital2);

        TextView chatComparDoctorScore1=view.findViewById(R.id.chat_compar_doctor_score1);
        TextView chatComparDoctorScore2=view.findViewById(R.id.chat_compar_doctor_score2);

        TextView chatComparDoctorBooking1=view.findViewById(R.id.chat_compar_doctor_booking1);
        TextView chatComparDoctorBooking2=view.findViewById(R.id.chat_compar_doctor_booking2);

        RelativeLayout chatComparAskDoctor1=view.findViewById(R.id.chat_compar_askdoctor1);
        RelativeLayout chatComparAskDoctor2=view.findViewById(R.id.chat_compar_askdoctor2);

        chatComparOhter.setText(chatBackBean.getComparedTitle());
        final List<ChatBackBean.ComparedDoctorsBean> comparedDoctors = chatBackBean.getComparedDoctors();
        Glide.with(mContext).load(comparedDoctors.get(0).getDoctorsImg()).transform(new GlideCircleTransform(mContext)).into(chatComparDoctorImg1);
        Glide.with(mContext).load(comparedDoctors.get(1).getDoctorsImg()).transform(new GlideCircleTransform(mContext)).into(chatComparDoctorImg2);

        chatComparDoctorName1.setText(comparedDoctors.get(0).getDoctorsName());
        chatComparDoctorName2.setText(comparedDoctors.get(1).getDoctorsName());

        chatComparDoctorTag1.setText(comparedDoctors.get(0).getDoctorsTag());
        chatComparDoctorTag2.setText(comparedDoctors.get(1).getDoctorsTag());
        setTagBackground(chatComparDoctorTag1,comparedDoctors.get(0).getDoctorsTagID());
        setTagBackground(chatComparDoctorTag2,comparedDoctors.get(1).getDoctorsTagID());

        chatComparDoctorLevel1.setText(comparedDoctors.get(0).getDoctorsTitle());
        chatComparDoctorLevel2.setText(comparedDoctors.get(1).getDoctorsTitle());

        chatComparDoctorHos1.setText(comparedDoctors.get(0).getHospitalName());
        chatComparDoctorHos2.setText(comparedDoctors.get(1).getHospitalName());

        chatComparDoctorScore1.setText("口碑："+comparedDoctors.get(0).getDiary_pf());
        chatComparDoctorScore2.setText("口碑："+comparedDoctors.get(1).getDiary_pf());

        chatComparDoctorBooking1.setText(comparedDoctors.get(0).getSku_order_num()+"人预订");
        chatComparDoctorBooking2.setText(comparedDoctors.get(1).getSku_order_num()+"人预订");

        chatComparBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mContext.finish();
            }
        });


        chatComparAskDoctor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id",taoId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COMPARE_TO_CHAT, "left"),hashMap,new ActivityTypeData("63"));
                ChatBackBean.ComparedDoctorsBean comparedDoctorsBean = comparedDoctors.get(0);
                ChatBackBean.ComparedDoctorsBean.TypeControlParamsBean typeControlParams = comparedDoctorsBean.getTypeControlParams();
                WebData webData = new WebData(comparedDoctorsBean.getJumpUrl());
                webData.setShowTitle("0".equals(typeControlParams.getIsHide()));
                webData.setShowRefresh("1".equals(typeControlParams.getIsRefresh()));
                WebUtil.getInstance().startWebActivity(mContext, webData);
            }
        });
        chatComparAskDoctor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id",taoId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COMPARE_TO_CHAT, "right"),hashMap,new ActivityTypeData("63"));
                ChatBackBean.ComparedDoctorsBean comparedDoctorsBean = comparedDoctors.get(1);
                ChatBackBean.ComparedDoctorsBean.TypeControlParamsBean typeControlParams = comparedDoctorsBean.getTypeControlParams();
                WebData webData = new WebData(comparedDoctorsBean.getJumpUrl());
                webData.setShowTitle("0".equals(typeControlParams.getIsHide()));
                webData.setShowRefresh("1".equals(typeControlParams.getIsRefresh()));
                WebUtil.getInstance().startWebActivity(mContext, webData);
            }
        });

        doctorClick1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id",taoId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COMPARE_TO_CHAT, "left"),hashMap,new ActivityTypeData("63"));
                ChatBackBean.ComparedDoctorsBean comparedDoctorsBean = comparedDoctors.get(0);
                ChatBackBean.ComparedDoctorsBean.TypeControlParamsBean typeControlParams = comparedDoctorsBean.getTypeControlParams();
                WebData webData = new WebData(comparedDoctorsBean.getJumpUrl());
                webData.setShowTitle("0".equals(typeControlParams.getIsHide()));
                webData.setShowRefresh("1".equals(typeControlParams.getIsRefresh()));
                WebUtil.getInstance().startWebActivity(mContext, webData);
            }
        });
        doctorClick2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id",taoId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COMPARE_TO_CHAT, "right"),hashMap,new ActivityTypeData("63"));
                ChatBackBean.ComparedDoctorsBean comparedDoctorsBean = comparedDoctors.get(1);
                ChatBackBean.ComparedDoctorsBean.TypeControlParamsBean typeControlParams = comparedDoctorsBean.getTypeControlParams();
                WebData webData = new WebData(comparedDoctorsBean.getJumpUrl());
                webData.setShowTitle("0".equals(typeControlParams.getIsHide()));
                webData.setShowRefresh("1".equals(typeControlParams.getIsRefresh()));
                WebUtil.getInstance().startWebActivity(mContext, webData);

            }
        });

    }

    /**
     * 设置标签背景
     *
     * @param tag
     * @param tag_id
     */
    private void setTagBackground(TextView tag, String tag_id) {
        switch (tag_id) {
            case "1" :
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag1);
                break;
            case "2" :
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag2);
                break;
            case "3" :
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag3);
                break;
            case "4" :
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag4);
                break;
            case "5" :
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag5);
                break;
            case "6" :
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag6);
                break;
            case "7" :
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag7);
                break;
            case "8" :
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag8);
                break;
        }
    }





}
