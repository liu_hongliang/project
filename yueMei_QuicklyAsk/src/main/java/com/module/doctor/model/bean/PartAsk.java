/**
 * 
 */
package com.module.doctor.model.bean;

import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class PartAsk {
	private String code;
	private String message;

	private List<PartAskData> data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public List<PartAskData> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<PartAskData> data) {
		this.data = data;
	}

}
