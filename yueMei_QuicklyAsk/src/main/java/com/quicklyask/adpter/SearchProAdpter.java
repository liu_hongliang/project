package com.quicklyask.adpter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.entity.ProjectHot;

public class SearchProAdpter extends BaseAdapter {

	private final String TAG = "SearchProAdpter";

	private List<ProjectHot> mProjectHot = new ArrayList<ProjectHot>();
	private Context mContext;
	private LayoutInflater inflater;
	private ProjectHot ProjectHot;
	ViewHolder viewHolder;

	public SearchProAdpter(Context mContext, List<ProjectHot> mProjectHot) {
		this.mContext = mContext;
		this.mProjectHot = mProjectHot;
		inflater = LayoutInflater.from(mContext);

		// Log.e(TAG, "ProjectHot==" + mProjectHot);
	}

	static class ViewHolder {
		public TextView mPart1NameTV;
		public RelativeLayout mRly;
	}

	@Override
	public int getCount() {
		return mProjectHot.size();
	}

	@Override
	public Object getItem(int position) {
		return mProjectHot.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_tao_pop, null);
			viewHolder = new ViewHolder();

			viewHolder.mPart1NameTV = convertView
					.findViewById(R.id.pop_tao_item_name_tv);
			viewHolder.mRly = convertView
					.findViewById(R.id.top_city_rly);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		ProjectHot = mProjectHot.get(position);
		// String cityid = ProjectHot.get_id();

		if (ProjectHot.get_id().equals("0")) {
			viewHolder.mPart1NameTV.setText(ProjectHot.getTwotitle());
		} else {
			viewHolder.mPart1NameTV.setText(ProjectHot.getTitle());
		}

		// viewHolder.mPart1NameTV.setTextSize(16);
		// viewHolder.mPart1NameTV.setTextColor(mContext.getResources().getColor(
		// R.color.gary_person));
		// viewHolder.mRly.setBackgroundResource(R.color.login_co);

		return convertView;
	}

	public void add(List<ProjectHot> infos) {
		mProjectHot.addAll(infos);
	}
}
