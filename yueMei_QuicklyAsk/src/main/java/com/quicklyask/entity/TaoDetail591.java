/**
 * 
 */
package com.quicklyask.entity;

/**
 * @author lenovo17
 * 
 */
public class TaoDetail591 {

	private String code;

	private String message;

	private TaoDetailData639 data;


	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public TaoDetailData639 getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(TaoDetailData639 data) {
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TaoDetail591 [code=" + code + ", message=" + message
				+ ", data=" + data + "]";
	}
}
