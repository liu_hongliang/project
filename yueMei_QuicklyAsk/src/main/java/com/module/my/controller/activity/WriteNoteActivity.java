package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.QiNiuTokenApi;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.commonview.module.bean.QiNiuBean;
import com.module.commonview.utils.StatisticalManage;
import com.module.commonview.view.CommonTopBar;
import com.module.home.controller.activity.SurgeryAfterTimeActivity;
import com.module.home.view.LoadingProgress;
import com.module.my.controller.adapter.ImageUploadAdapter2;
import com.module.my.model.api.PostTextQueApi;
import com.module.my.model.bean.ForumTextData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.QiNiuConfigration;
import com.module.other.netWork.netWork.QiNuConfig;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.quicklyack.constant.FinalConstant;
import com.quicklyack.emoji.Expressions;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.entity.WriteResultData;
import com.quicklyask.entity.WriteVideoResult;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.MyUploadImage;
import com.quicklyask.util.MyUploadImage2;
import com.quicklyask.util.Utils;
import com.quicklyask.util.VideoUploadUpyun;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.ProcessImageView;
import com.quicklyask.view.WritePicPopWindow;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.xinlan.imageeditlibrary.editimage.EditImageActivity;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.common.util.DensityUtil;
import org.xutils.image.ImageOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.quicklyask.activity.R.id.v_fengexian;


/**
 * 编辑日记
 * <p>
 * Created by dwb on 16/3/23.
 */
public class WriteNoteActivity extends BaseActivity {

    private final String TAG = "WriteNoteActivity";

    private WriteNoteActivity mContext;

    private GridView gridview;

    private ImageUploadAdapter2 adapter;
    private float dp;


    @BindView(id = R.id.write_note_top)
    private CommonTopBar mTop;
    @BindView(id = R.id.write_exp_content_et)
    private EditText postContent;// 提交的内容控件
    @BindView(id = R.id.write_exp_title_et)
    private EditText writeExptitleEt;
    @BindView(id = R.id.tv_title_num)
    private TextView tvTitleNum;
    @BindView(id = R.id.ll_riji_title)
    private LinearLayout llRijiTitle;

    //术后天数选择
    @BindView(id = R.id.ll_after_time)
    private LinearLayout afterTime;
    @BindView(id = R.id.tv_after_time)
    private TextView afterDay;


    private String ratN1 = "0";
    private String ratN2 = "0";
    private String ratN3 = "0";

    @BindView(id = R.id.write_question_tips_photo_rly)
    private RelativeLayout tipsPhoto;

    @BindView(id = R.id.rootview_lyly)
    private LinearLayout rootview;
    @BindView(id = R.id.biaoqing_shuru_content_ly1)
    private LinearLayout biaoqingContentLy;
    @BindView(id = R.id.biaoqing_ly_content)
    private LinearLayout biaoqingBtRly;

    @BindView(id = R.id.write_number_zi_tv)
    private TextView zinumberTv;

    @BindView(id = R.id.colse_biaoqingjian_bt)
    private ImageButton closeImBt;
    // 表情
    private ViewPager viewPager;
    private ArrayList<GridView> grids;
    private int[] expressionImages;
    private String[] expressionImageNames;
    private int[] expressionImages1;
    private String[] expressionImageNames1;
    private int[] expressionImages2;
    private String[] expressionImageNames2;
    private GridView gView1;
    private GridView gView2;
    private GridView gView3;
    private LinearLayout page_select;
    private ImageView page0;
    private ImageView page1;
    private ImageView page2;
    private TextView modification;
    private LinearLayout click_consumer_certificate;

    /*
     * @BindView(id = R.id.write_exp_photo_num) private TextView photoNumTv;//
     * 照片张数
     */
    private String uid;
    private String content = "";// 提交的内容
    private String visibility = "0";
    private String hosname = "0";// 医院名称
    private String docname = "0";// 医生名字
    private String fee = "0";// 费用
    private String cateid = "0";// 提问的板块id
    private String userid = "0";
    private String hosid = "0";
    private String taoid = "0";
    private String server_id = "0";

    private String sharetime = "";// 手术时间

    private String noteId = "0";//日记本id
    private String noteTitle = "";

    @BindView(id = R.id.child_scroll)

    Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

    private final Calendar mCalendar = Calendar.getInstance();

    private int day = mCalendar.get(Calendar.DAY_OF_MONTH);

    private int month = mCalendar.get(Calendar.MONTH) + 1;

    private int year = mCalendar.get(Calendar.YEAR);

    private String dateStr = "";

    private String wType;
    @BindView(id = R.id.pingfen_content_ly)
    private LinearLayout pingfenLy;// 评分容器
    // 评分系统
    @BindView(id = R.id.room_ratingbar1)
    private RatingBar rating1;
    @BindView(id = R.id.room_ratingbar2)
    private RatingBar rating2;
    @BindView(id = R.id.room_ratingbar3)
    private RatingBar rating3;

    // 手术之前的照片
    @BindView(id = R.id.tv_surgery_before)
    private LinearLayout surgeryBefore;
    @BindView(id = R.id.add_iv1)
    private ProcessImageView addIv1;
    private boolean isIv1 = false;
    private String pic_id1 = "0";
    @BindView(id = R.id.add_iv1_bt1)
    private Button addIvBt1;
    @BindView(id = R.id.add_iv2)
    private ProcessImageView addIv2;
    private boolean isIv2 = false;
    private String pic_id2 = "0";
    @BindView(id = R.id.add_iv1_bt2)
    private Button addIvBt2;
    @BindView(id = R.id.add_iv3)
    private ProcessImageView addIv3;
    private boolean isIv3 = false;
    private String pic_id3 = "0";
    @BindView(id = R.id.add_iv1_bt3)
    private Button addIvBt3;
    @BindView(id = R.id.add_iv4)
    private ProcessImageView addIv4;
    private boolean isIv4 = false;
    private String pic_id4 = "0";
    @BindView(id = R.id.add_iv1_bt4)
    private Button addIvBt4;


    @BindView(id = R.id.rating_tv1)
    private TextView ratingTv1;
    @BindView(id = R.id.rating_tv2)
    private TextView ratingTv2;
    @BindView(id = R.id.rating_tv3)
    private TextView ratingTv3;

    @BindView(id = R.id.add_note_ly)
    private LinearLayout addNoteLy;//添加到日记本
    @BindView(id = R.id.note_select_list_iv)
    private ImageView mIv;
    @BindView(id = R.id.note_select_title_tv)
    private TextView mTitleTv;
    @BindView(id = R.id.note_select_page_tv)
    private TextView mPageTv;
    @BindView(id = R.id.note_select_subtitle)
    private TextView mSubTitleTv;

    //视频上传列表
    @BindView(id = R.id.ll_note_video)
    private LinearLayout noteVideo;
    @BindView(id = R.id.rl_note_video)
    private RelativeLayout replaceVideo;
    @BindView(id = R.id.piv_note_video)
    private ProcessImageView thumVideo;
    @BindView(id = R.id.rl_note_picture)
    private RelativeLayout pictureVideo;
    @BindView(id = R.id.rl_video_del)
    private RelativeLayout delVideo;

    //视频封面
    @BindView(id = R.id.iv_note_cover)
    private ImageView noteCover;
    @BindView(id = R.id.fl_editor_cover)
    private FrameLayout editorCover;
    @BindView(id = R.id.tv_cover_title)
    private TextView titleCover;
    @BindView(id = R.id.ll_shuqian_container)
    private LinearLayout shuqian;

    //术前照片
    @BindView(id = R.id.rl_sq_zm)
    private RelativeLayout sq_zm;
    @BindView(id = R.id.rl_sq_cm45)
    private RelativeLayout sq_cm45;
    @BindView(id = R.id.rl_sq_cm90)
    private RelativeLayout sq_cm90;
    @BindView(id = R.id.rl_sq_qt)
    private RelativeLayout sq_qt;

    @BindView(id = R.id.tv_sqzm)
    private TextView sqzmTex;
    @BindView(id = R.id.tv_sqcm45)
    private TextView sqcm45Tex;
    @BindView(id = R.id.tv_sqcm90)
    private TextView sqcm90Tex;
    @BindView(id = R.id.tv_sqqt)
    private TextView sqqtTex;
    @BindView(id = v_fengexian)
    private View vFengexian;

    private String mAddIv = "";
    private String mTitle = "";
    private String mFtitle = "";
    private String pageStr = "";

    ImageOptions imageOptions;
    ImageOptions imageOptions1;


    private ArrayList<String> mResults = new ArrayList<>();
    private ArrayList<String> mResults2 = new ArrayList<>();
    private static final int REQUEST_CODE = 732;
    public static final int ACTION_REQUEST_EDITIMAGE = 9;
    private static final int FONT = 4;
    private static final int VIDEO_REQUEST_CODE = 76;

    private static final int EDITOR_COVER = 79;

    public static final int POSTOPERATIVE_TIME = 666;
    public static final int FROM_GALLERY = 777;

    @BindView(id = R.id.tuppppppppppp_fly)
    private FrameLayout tuPicFly;

    private WritePicPopWindow wpicPop;

    private String addtype = "1";
    private HashMap<String, ProcessImageView> processImages;
    private boolean notClick = false;
    private String iv_type = "1";
    private JSONObject[] drr_ = new JSONObject[4];
    private String selectNum = "";
    private WriteVideoResult videoResult;
    private File errorFile = null;
    private HashMap<String, String> errorDrrNum = new HashMap<>();            //术前照片上传失败的
    private HashMap<String, String> beforeMap = new HashMap<>();
    private HashMap<String, String> beforeKey = new HashMap<>();
    private JSONObject videoCoverUrl;       //视频封面上传后的网络存储路径
    private String imgCoverPath = "";        //视频封面的本地存储路径
    private boolean videoCoverState = true;     //视频封面是否上传成功
    private String videoDuration = "";
    private String textStr;
    private String titleStr;
    private String mImgCover = "";          //封面图
    private String mZhipaths;   //到贴纸页传的路径
    private int shCover = 1;
    private int coverImgWidth;
    private int coverImgHeight;
    private long surgeryafterdays;          //日记添加术后Y天
    private String beforeMaxDay;
    private long postoTime;
    private PageJumpManager pageJumpManager;
    private LoadingProgress mDialog;
    private boolean consumer_certificate = false;
    private ArrayList<String> mListExtra;
    private Uri mOutputUri;
    private String mConsumer_certificate;
    private int[] mImageWidthHeight;
    private String mKey;
    private Gson mGson;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_write_note);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = WriteNoteActivity.this;

        mGson = new Gson();

        pageJumpManager = new PageJumpManager(mContext);

        mDialog = new LoadingProgress(mContext);

        imageOptions = new ImageOptions.Builder().setImageScaleType(ImageView.ScaleType.FIT_XY).setLoadingDrawableId(R.drawable.radius_gray80).setFailureDrawableId(R.drawable.radius_gray80).build();

        imageOptions1 = new ImageOptions.Builder().setRadius(6).setImageScaleType(ImageView.ScaleType.FIT_XY).setLoadingDrawableId(R.drawable.radius_gray80).setFailureDrawableId(R.drawable.radius_gray80).build();

        page_select = findViewById(R.id.page_select);
        page0 = findViewById(R.id.page0_select);
        page1 = findViewById(R.id.page1_select);
        page2 = findViewById(R.id.page2_select);
        modification = findViewById(R.id.modification);
        click_consumer_certificate = findViewById(R.id.click_consumer_certificate);
        // 引入表情
        expressionImages = Expressions.expressionImgs;
        expressionImageNames = Expressions.expressionImgNames;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;
        expressionImages2 = Expressions.expressionImgs2;
        expressionImageNames2 = Expressions.expressionImgNames2;
        // 创建ViewPager
        viewPager = findViewById(R.id.viewpager);
        initViewPager();

        initView();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        postContent.setFocusable(false);
        postContent.setFocusableInTouchMode(false);
        postContent.requestFocus();
        //术前照片高度设置
        ViewGroup.LayoutParams params = shuqian.getLayoutParams();

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenWidth = dm.widthPixels; // 屏幕宽（px，如：px）
        Log.e(TAG, "screenWidth == " + screenWidth);
        int height = (screenWidth - DensityUtil.dip2px(15) * 2 - DensityUtil.dip2px(11) * 3) / 4;
        params.height = height;                             //设置高度
        shuqian.setLayoutParams(params);

        //正面宽高
        ViewGroup.LayoutParams l1 = sq_zm.getLayoutParams();
        l1.width = height;
        l1.height = height;
        sq_zm.setLayoutParams(l1);

        ViewGroup.LayoutParams l11 = addIv1.getLayoutParams();
        l11.width = height - DensityUtil.dip2px(8);
        l11.height = height - DensityUtil.dip2px(8);
        addIv1.setLayoutParams(l11);


        //侧面45宽高
        ViewGroup.LayoutParams l2 = sq_cm45.getLayoutParams();
        l2.width = height;
        l2.height = height;
        sq_cm45.setLayoutParams(l2);

        ViewGroup.LayoutParams l22 = addIv2.getLayoutParams();
        l22.width = height - DensityUtil.dip2px(8);
        l22.height = height - DensityUtil.dip2px(8);
        addIv2.setLayoutParams(l22);

        //侧面90宽高
        ViewGroup.LayoutParams l3 = sq_cm90.getLayoutParams();
        l3.width = height;
        l3.height = height;
        sq_cm90.setLayoutParams(l3);

        ViewGroup.LayoutParams l33 = addIv3.getLayoutParams();
        l33.width = height - DensityUtil.dip2px(8);
        l33.height = height - DensityUtil.dip2px(8);
        addIv3.setLayoutParams(l33);

        //其他宽高
        ViewGroup.LayoutParams l4 = sq_qt.getLayoutParams();
        l4.width = height;
        l4.height = height;
        sq_qt.setLayoutParams(l4);

        ViewGroup.LayoutParams l44 = addIv4.getLayoutParams();
        l44.width = height - DensityUtil.dip2px(8);
        l44.height = height - DensityUtil.dip2px(8);
        addIv4.setLayoutParams(l44);

        postContent.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2, int count) {
                if (s.length() > 0) {
                    zinumberTv.setText("已输入" + s.length() + "个字符");
                } else {
                    zinumberTv.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                textStr = postContent.getText().toString().trim();
                titleStr = writeExptitleEt.getText().toString().trim();

                Log.e("6666", "mAddIv === " + mAddIv);
                Log.e("6666", "wType === " + wType);
                Log.e("6666", "textStr.length() === " + textStr.length());
                Log.e("6666", "titleStr.length() === " + titleStr.length());
                if (mAddIv != null && mAddIv.length() > 0) {
                    if ("3".equals(wType) || "1".equals(wType)) {
                        if (textStr.length() >= 20) {
                            mTop.getTv_right().setClickable(true);
                            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
                        } else {
                            mTop.getTv_right().setClickable(false);
                            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
                        }
                    } else {

                        if (textStr.length() >= 20 && titleStr.length() >= 4) {
                            mTop.getTv_right().setClickable(true);
                            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
                        } else {
                            mTop.getTv_right().setClickable(false);
                            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
                        }

                    }
                } else {
                    if ("3".equals(wType) || "1".equals(wType)) {
                        if (textStr.length() >= 20) {
                            mTop.getTv_right().setClickable(true);
                            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
                        } else {
                            mTop.getTv_right().setClickable(false);
                            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
                        }
                    } else {

                        if (textStr.length() >= 20 && titleStr.length() >= 4) {
                            mTop.getTv_right().setClickable(true);
                            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
                        } else {
                            mTop.getTv_right().setClickable(false);
                            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
                        }
                    }
                }

            }

        });

        click_consumer_certificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notClick) {
                    Toast.makeText(mContext, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    if (Build.VERSION.SDK_INT >= 23) {

                        Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                            @Override
                            public void onGranted() {
                                toXIangce();
                            }

                            @Override
                            public void onDenied(List<String> permissions) {
//                                    Toast.makeText(mContext, "没有权限", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        toXIangce();
                    }
                }
            }
        });

        /**
         * 标题的监听
         */
        writeExptitleEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textStr = postContent.getText().toString().trim();
                titleStr = writeExptitleEt.getText().toString().trim();
                if (textStr.length() >= 20 && titleStr.length() >= 4) {
                    mTop.getTv_right().setClickable(true);
                    mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
                } else {
                    mTop.getTv_right().setClickable(false);
                    mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
                }

                tvTitleNum.setText(writeExptitleEt.getText().toString().length() + "/18");
            }
        });


        rootview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // 比较Activity根布局与当前布局的大小
                int heightDiff = rootview.getRootView().getHeight() - rootview.getHeight();
                if (heightDiff > 300) {
                    // 大小超过100时，一般为显示虚拟键盘事件
                    biaoqingBtRly.setVisibility(View.VISIBLE);
                } else {
                    // 大小小于100时，为不显示虚拟键盘或虚拟键盘隐藏
                    biaoqingBtRly.setVisibility(View.GONE);
                }
            }
        });

        biaoqingBtRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                biaoqingContentLy.setVisibility(View.VISIBLE);
            }
        });

        closeImBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });

        postContent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (notClick) {
                    Toast.makeText(mContext, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    String as = postContent.getText().toString();
                    Intent it = new Intent();
                    it.putExtra("text_str", as);
                    it.setClass(mContext, WriteFontActivity.class);
                    startActivityForResult(it, FONT);
                }
            }
        });

        rating1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 1) {
                    ratingTv1.setText("差");
                    ratN1 = 1 + "";
                } else if (rating == 2) {
                    ratingTv1.setText("不满意");
                    ratN1 = 2 + "";
                } else if (rating == 3) {
                    ratingTv1.setText("一般");
                    ratN1 = 3 + "";
                } else if (rating == 4) {
                    ratingTv1.setText("比较满意");
                    ratN1 = 4 + "";
                } else if (rating == 5) {
                    ratingTv1.setText("非常满意");
                    ratN1 = 5 + "";
                } else {
                    ratingTv1.setText("");
                    ratN1 = 0 + "";
                }
            }
        });

        rating2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 1) {
                    ratingTv2.setText("差");
                    ratN2 = 1 + "";
                } else if (rating == 2) {
                    ratingTv2.setText("不满意");
                    ratN2 = 2 + "";
                } else if (rating == 3) {
                    ratingTv2.setText("一般");
                    ratN2 = 3 + "";
                } else if (rating == 4) {
                    ratingTv2.setText("比较满意");
                    ratN2 = 4 + "";
                } else if (rating == 5) {
                    ratingTv2.setText("非常满意");
                    ratN2 = 5 + "";
                } else {
                    ratingTv2.setText("");
                    ratN2 = 0 + "";
                }
            }
        });

        rating3.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 1) {
                    ratingTv3.setText("差");
                    ratN3 = 1 + "";
                } else if (rating == 2) {
                    ratingTv3.setText("不满意");
                    ratN3 = 2 + "";
                } else if (rating == 3) {
                    ratingTv3.setText("一般");
                    ratN3 = 3 + "";
                } else if (rating == 4) {
                    ratingTv3.setText("比较满意");
                    ratN3 = 4 + "";
                } else if (rating == 5) {
                    ratingTv3.setText("非常满意");
                    ratN3 = 5 + "";
                } else {
                    ratingTv3.setText("");
                    ratN3 = 0 + "";
                }
            }
        });


        /**
         * 添加照片一
         */
        addIv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"1".equals(errorDrrNum.get("1"))) {
                    if (drr_[0] != null && drr_[0].length() > 0) {
                        if (shCover != 1) {
                            addIv1.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);

                            if (shCover == 2) {
                                addIv2.startHua(ProcessImageView.WANC_HENG, 100);
                            } else if (shCover == 3) {
                                addIv3.startHua(ProcessImageView.WANC_HENG, 100);
                            } else if (shCover == 4) {
                                addIv4.startHua(ProcessImageView.WANC_HENG, 100);
                            }

                            shCover = 1;
                        }
                    } else {
                        View view = getWindow().peekDecorView();
                        if (view != null) {
                            InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        new PopupWindows(mContext, rootview);
                        iv_type = "1";
                    }

                } else {
                    String filePath = beforeMap.get("1");
                    mKey = beforeMap.get("1");
                    MyUploadImage2.getMyUploadImage(mContext, 1, mHandler, filePath, addIv1).uploadImage(mKey);
                }
            }
        });

        /**
         * 添加照片二
         */
        addIv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"2".equals(errorDrrNum.get("2"))) {
                    if (drr_[1] != null && drr_[1].length() > 0) {
                        if (shCover != 2) {
                            addIv2.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);

                            if (shCover == 1) {
                                addIv1.startHua(ProcessImageView.WANC_HENG, 100);
                            } else if (shCover == 3) {
                                addIv3.startHua(ProcessImageView.WANC_HENG, 100);
                            } else if (shCover == 4) {
                                addIv4.startHua(ProcessImageView.WANC_HENG, 100);
                            }

                            shCover = 2;
                        }
                    } else {
                        View view = getWindow().peekDecorView();
                        if (view != null) {
                            InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        new PopupWindows(mContext, rootview);
                        iv_type = "2";
                    }
                } else {
                    String filePath = beforeMap.get("2");
                    mKey = beforeMap.get("2");
                    MyUploadImage2.getMyUploadImage(mContext, 2, mHandler, filePath, addIv2).uploadImage(mKey);
                }
            }
        });

        /**
         * 添加照片三
         */
        addIv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"3".equals(errorDrrNum.get("3"))) {
                    if (drr_[2] != null && drr_[2].length() > 0) {
                        if (shCover != 3) {
                            addIv3.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);

                            if (shCover == 1) {
                                addIv1.startHua(ProcessImageView.WANC_HENG, 100);
                            } else if (shCover == 2) {
                                addIv2.startHua(ProcessImageView.WANC_HENG, 100);
                            } else if (shCover == 4) {
                                addIv4.startHua(ProcessImageView.WANC_HENG, 100);
                            }

                            shCover = 3;
                        }
                    } else {
                        View view = getWindow().peekDecorView();
                        if (view != null) {
                            InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        new PopupWindows(mContext, rootview);
                        iv_type = "3";
                    }
                } else {
                    String filePath = beforeMap.get("3");
                    mKey = beforeMap.get("3");
                    MyUploadImage2.getMyUploadImage(mContext, 3, mHandler, filePath, addIv3).uploadImage(mKey);
                }
            }
        });

        /**
         * 添加照片四
         */
        addIv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"4".equals(errorDrrNum.get("4"))) {
                    if (drr_[3] != null && drr_[3].length() > 0) {
                        if (shCover != 4) {
                            addIv4.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);

                            if (shCover == 1) {
                                addIv1.startHua(ProcessImageView.WANC_HENG, 100);
                            } else if (shCover == 2) {
                                addIv2.startHua(ProcessImageView.WANC_HENG, 100);
                            } else if (shCover == 3) {
                                addIv3.startHua(ProcessImageView.WANC_HENG, 100);
                            }

                            shCover = 4;
                        }
                    } else {
                        View view = getWindow().peekDecorView();
                        if (view != null) {
                            InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        new PopupWindows(mContext, rootview);
                        iv_type = "4";
                    }
                } else {
                    String filePath = beforeMap.get("4");
                    mKey = beforeMap.get("4");
                    MyUploadImage2.getMyUploadImage(mContext, 4, mHandler, filePath, addIv4).uploadImage(mKey);
                }
            }
        });

        /**
         * 删除照片一
         */
        addIvBt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIvBt1.setVisibility(View.GONE);
                addIv1.setImageDrawable(null);
                if (!pic_id1.equals("0")) {
                    isIv1 = true;
                }
                errorDrrNum.remove("1");
                beforeMap.remove("1");
                drr_[0] = null;

                //删除后设置封面
                if (shCover == 1) {
                    for (int i = 0; i < drr_.length; i++) {
                        if (drr_[i] != null && drr_[i].length() > 0) {
                            shCover = i + 1;

                            break;
                        }
                    }
                }

                if (shCover == 1) {
                    addIv1.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 2) {
                    addIv2.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 3) {
                    addIv3.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 4) {
                    addIv4.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                }
            }
        });

        /**
         * 删除照片二
         */
        addIvBt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIvBt2.setVisibility(View.GONE);
                addIv2.setImageDrawable(null);
                if (!pic_id2.equals("0")) {
                    isIv2 = true;
                }
                errorDrrNum.remove("2");
                beforeMap.remove("2");
                drr_[1] = null;

                //删除后设置封面
                if (shCover == 1) {
                    for (int i = 0; i < drr_.length; i++) {
                        if (drr_[i] != null && drr_[i].length() > 0) {
                            shCover = i + 1;
                            break;
                        }
                    }
                }

                if (shCover == 1) {
                    addIv1.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 2) {
                    addIv2.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 3) {
                    addIv3.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 4) {
                    addIv4.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                }
            }
        });

        /**
         * 删除照片三
         */
        addIvBt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIvBt3.setVisibility(View.GONE);
                addIv3.setImageDrawable(null);
                if (!pic_id3.equals("0")) {
                    isIv3 = true;
                }
                errorDrrNum.remove("3");
                beforeMap.remove("3");
                drr_[2] = null;

                //删除后设置封面
                if (shCover == 1) {
                    for (int i = 0; i < drr_.length; i++) {
                        if (drr_[i] != null && drr_[i].length() > 0) {
                            shCover = i + 1;
                            break;
                        }
                    }
                }


                if (shCover == 1) {
                    addIv1.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 2) {
                    addIv2.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 3) {
                    addIv3.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 4) {
                    addIv4.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                }
            }
        });

        /**
         * 删除照片四
         */
        addIvBt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIvBt4.setVisibility(View.GONE);
                addIv4.setImageDrawable(null);
                if (!pic_id4.equals("0")) {
                    isIv4 = true;
                }
                errorDrrNum.remove("4");
                beforeMap.remove("4");
                drr_[3] = null;

                //删除后设置封面
                if (shCover == 1) {
                    for (int i = 0; i < drr_.length; i++) {
                        if (drr_[i] != null && drr_[i].length() > 0) {
                            shCover = i + 1;
                            break;
                        }
                    }
                }

                if (shCover == 1) {
                    addIv1.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 2) {
                    addIv2.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 3) {
                    addIv3.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                } else if (shCover == 4) {
                    addIv4.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                }
            }
        });

        writeExptitleEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notClick) {
                    Toast.makeText(mContext, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //编辑封面
        editorCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectNum != null && selectNum.length() > 0) {
                    if (videoCoverState) {
                        Intent intent = new Intent(mContext, SetCoverActivity.class);
                        intent.putExtra("selectNum", selectNum);            //本地视频存储路径
                        intent.putExtra("imgCoverPath", imgCoverPath);      //本地封面存储路径
                        startActivityForResult(intent, EDITOR_COVER);
                    } else {
                        uploadCoverImage(imgCoverPath);      //上传失败后重新上传
                    }

                } else {
                    Toast.makeText(mContext, "请先选择视频", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //术后时间选择
        afterTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SurgeryAfterTimeActivity.class);
                intent.putExtra("afterDay", surgeryafterdays);
                intent.putExtra("beforemaxday", beforeMaxDay);
                startActivityForResult(intent, POSTOPERATIVE_TIME);
                overridePendingTransition(R.anim.activity_left_to_right_open, 0);
            }
        });
    }


    /**
     * 编辑内容是否获取焦点
     *
     * @param focusable
     */
    private void setContentSelected(boolean focusable) {
        postContent.setFocusable(false);
        postContent.setFocusableInTouchMode(false);
        postContent.requestFocus();

        writeExptitleEt.setFocusable(focusable);
        writeExptitleEt.setFocusableInTouchMode(focusable);
        writeExptitleEt.requestFocus();
    }


    private static final int IMAGE_REQUEST_CODE = 0;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int RESIZE_REQUEST_CODE = 2;

    /**
     * 弹出照相 图库选择
     *
     * @author lenovo17
     */
    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.item_popupwindows, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.FILL_PARENT);
            setHeight(ViewGroup.LayoutParams.FILL_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            // setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            Button bt1 = view.findViewById(R.id.item_popupwindows_camera);
            Button bt2 = view.findViewById(R.id.item_popupwindows_Photo);
            Button bt3 = view.findViewById(R.id.item_popupwindows_cancel);
            // 拍照
            bt1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    photo();
                    dismiss();
                }
            });
            // 点击 之外 消失
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {

                    int height = view.findViewById(R.id.ll_popup).getTop();
                    int y = (int) event.getY();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (y < height) {
                            dismiss();
                        }
                    }
                    return true;
                }
            });
            // 相册
            bt2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent galleryIntent = new Intent();
                    galleryIntent.setType("image/*");
                    galleryIntent.setAction("android.intent.action.PICK");
                    galleryIntent.addCategory("android.intent.category.DEFAULT");
                    startActivityForResult(galleryIntent, IMAGE_REQUEST_CODE);
                    dismiss();
                }
            });
            bt3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    public void photo() {
        try {
            Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String pathS = Environment.getExternalStorageDirectory().toString() + "/AyuemeiImage";
            File path1 = new File(pathS);
            if (!path1.exists()) {
                path1.mkdirs();
            }
            File file = new File(path1, "yuemei" + ".JPEG");
            openCameraIntent.addCategory("android.intent.category.DEFAULT");
            Uri uri = uriFromFile(WriteNoteActivity.this, file);
            openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(openCameraIntent, CAMERA_REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Uri uriFromFile(Activity activity, File file) {
        Uri fileUri;
        //7.0以上进行适配
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String p = activity.getPackageName() + ".FileProvider";
            fileUri = FileProvider.getUriForFile(activity, p, file);
        } else {
            fileUri = Uri.fromFile(file);
        }
        return fileUri;
    }

    @Override
    protected void onStart() {
        super.onStart();
        uid = Utils.getUid();
    }

    void initView() {
        Intent it = getIntent();
        if (it != null) {
            cateid = it.getStringExtra("cateid");
            userid = it.getStringExtra("userid");
            docname = it.getStringExtra("docname");
            hosname = it.getStringExtra("hosname");
            hosid = it.getStringExtra("hosid");
            fee = it.getStringExtra("fee");
            taoid = it.getStringExtra("taoid");
            server_id = it.getStringExtra("server_id");
            wType = it.getStringExtra("type");                      //这个参数用来判断是不是第一次
            sharetime = it.getStringExtra("sharetime");
            noteId = it.getStringExtra("noteid");
            noteTitle = it.getStringExtra("notetitle");
            beforeMaxDay = it.getStringExtra("beforemaxday");
            mConsumer_certificate = it.getStringExtra("consumer_certificate");
            if ("1".equals(mConsumer_certificate)) {
                vFengexian.setVisibility(View.VISIBLE);
                click_consumer_certificate.setVisibility(View.VISIBLE);
            } else {
                vFengexian.setVisibility(View.GONE);
                click_consumer_certificate.setVisibility(View.GONE);
            }
            Log.e(TAG, "cateid === " + cateid);
            Log.e(TAG, "userid === " + userid);
            Log.e(TAG, "docname === " + docname);
            Log.e(TAG, "hosname === " + hosname);
            Log.e(TAG, "hosid === " + hosid);
            Log.e(TAG, "fee === " + fee);
            Log.e(TAG, "taoid === " + taoid);
            Log.e(TAG, "server_id === " + server_id);
            Log.e(TAG, "wType === " + wType);
            Log.e(TAG, "sharetime === " + sharetime);
            Log.e(TAG, "noteId === " + noteId);
            Log.e(TAG, "noteTitle === " + noteTitle);
            Log.e(TAG, "beforeMaxDay === " + beforeMaxDay);

            if (beforeMaxDay == null || "".equals(beforeMaxDay)) {
                beforeMaxDay = "0";
            }

            Log.e(TAG, "beforeMaxDay == " + beforeMaxDay);

            mAddIv = it.getStringExtra("noteiv");                       //笔记本图片路径
            mTitle = it.getStringExtra("notetitle_");
            mFtitle = it.getStringExtra("notefutitle");
            pageStr = it.getStringExtra("page");

            addtype = it.getStringExtra("addtype");


            if (null != hosname && hosname.length() > 0) {

            } else {
                hosname = "0";
            }
            if (null != docname && docname.length() > 0) {

            } else {
                docname = "0";
            }
            if (fee == null || fee.length() == 0) {

                fee = "0";
            }
            if (null != noteId && noteId.length() > 0) {

            } else {
                noteId = "0";
            }
        }

        if (sharetime != null && sharetime.length() > 0) {
            dateStr = sharetime;
        } else {
            dateStr = "2016-03-23";
            sharetime = year + "-" + month + "-" + day;
        }
        Log.e(TAG, "sharetime == " + sharetime);
        Log.e(TAG, "year == " + year);
        Log.e(TAG, "month == " + month);
        Log.e(TAG, "day == " + day);
        surgeryafterdays = Utils.getDaySub(sharetime, year + "-" + month + "-" + day);

        if (surgeryafterdays >= Integer.parseInt(beforeMaxDay)) {
            postoTime = surgeryafterdays;
        } else {
            postoTime = Integer.parseInt(beforeMaxDay);
        }

        if (postoTime == 0) {
            afterDay.setText("手术当天");
        } else {
            afterDay.setText("术后第" + postoTime + "天");

        }
        Log.e(TAG, "wType666 == " + wType);
        Log.e(TAG, "mAddIv666 == " + mAddIv);
        if ("1".equals(wType)) {
            vFengexian.setVisibility(View.GONE);               //分割线
            llRijiTitle.setVisibility(View.GONE);               //标题
            surgeryBefore.setVisibility(View.GONE);             //术前照片
            pingfenLy.setVisibility(View.GONE);                 //评分
            addNoteLy.setVisibility(View.VISIBLE);              //日记本

            if (null != mAddIv && mAddIv.length() > 0) {         //日记本图片不为空
                Glide.with(mContext).load(mAddIv).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).into(mIv);

                mTitleTv.setText(mTitle);
                if (wType.equals("1")) {
                    mPageTv.setText("更新至第" + pageStr + "页");
                    mSubTitleTv.setText(mFtitle + "最后编辑");
                } else {
                    mPageTv.setText(mFtitle);
                }

            } else {
                addNoteLy.setVisibility(View.GONE);
            }

        } else if ("3".equals(wType)) {
            llRijiTitle.setVisibility(View.GONE);
            surgeryBefore.setVisibility(View.GONE);
            pingfenLy.setVisibility(View.GONE);
            addNoteLy.setVisibility(View.GONE);
        } else {
            llRijiTitle.setVisibility(View.VISIBLE);
            surgeryBefore.setVisibility(View.VISIBLE);
            pingfenLy.setVisibility(View.VISIBLE);
            addNoteLy.setVisibility(View.GONE);
        }

        dp = getResources().getDimension(R.dimen.dp);
        gridview = findViewById(R.id.noScrollgridview3);
        gridview.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gridviewInit();

        //新增视频提示
        new CountDownTimer(350, 100) {      // 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                if (Utils.isValidContext(mContext)) {

                    String ispop = Cfg.loadStr(mContext, "popvideo", "");
                    if (ispop.equals("1")) {

                    } else {
                        wpicPop = new WritePicPopWindow(mContext, R.drawable.video_word_gray2x);
                        int[] location = new int[2];
                        tuPicFly.getLocationOnScreen(location);
                        int x = location[0];
                        int y = location[1];
                        wpicPop.showAtLocation(tuPicFly, Gravity.NO_GRAVITY, 0, y - tuPicFly.getHeight() / 2 - DensityUtil.dip2px(10));

                        Cfg.saveStr(mContext, "popvideo", "1");
                    }
                }

            }
        }.start();

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (postContent.getText().toString().trim().length() > 0 || mResults.size() > 0) {
                    showDialogExitEdit();
                } else {
                    finish();
                }
            }
        });

        //提交
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (notClick) {
                    if (selectNum.length() > 0) {
                        Toast.makeText(mContext, "视频正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    uid = Utils.getUid();
                    if (Utils.isLogin()) {

                        if (dateStr.length() > 1) {
                            content = postContent.getText().toString().trim();
                            if (null == mAddIv || mAddIv.length() == 0) {
                                noteTitle = writeExptitleEt.getText().toString().trim();
                            }

                            Matcher matcherTitle = emoji.matcher(noteTitle);
                            Matcher matcher = emoji.matcher(content);

                            if (matcher.find() || matcherTitle.find()) {        //日记本标题或者内容有第三方表情输入
                                Toast.makeText(mContext, "提交失败，请去掉非悦美表情或其他特殊字符后在尝试提交", Toast.LENGTH_SHORT).show();
                            } else {

                                if (content.length() > 0 && !"".equals(content)) {
                                    if (content.length() > 19) {
                                        Log.e("NNNN", "noteTitle === " + noteTitle);

                                        if (null == mAddIv || mAddIv.length() == 0) {

                                            if ("3".equals(wType) || "1".equals(wType)) {
                                                tijiaoData();
                                            } else {
                                                if (noteTitle.length() >= 1) {
                                                    if ("1".equals(mConsumer_certificate)) {
                                                        if (consumer_certificate) {
                                                            tijiaoData();
                                                        } else {
                                                            showDialogExitEdit3("为了确保您所做的项目真实有效，请上传您的消费凭证", "我知道了");
                                                        }
                                                    } else {
                                                        tijiaoData();
                                                    }

                                                } else {
                                                    ViewInject.toast("亲，标题至少要大于1个字哟！");
                                                }
                                            }

                                        } else {
                                            tijiaoData();
                                        }

                                    } else {
                                        ViewInject.toast("亲，内容至少要大于20个字哟！");
                                    }
                                } else {
                                    ViewInject.toast("内容不能为空！");
                                }
                            }
                        } else {
                            ViewInject.toast("请选择手术时间！");
                        }
                    } else {
                        Utils.jumpLogin(mContext);
                    }
                }
            }
        });
    }

    private void tijiaoData() {
        if (wType.equals("2")) {
            if (!ratN1.equals("0") && !ratN2.equals("0") && !ratN3.equals("0")) {
                mTop.getTv_right().setClickable(false);
                mDialog.startLoading();
                try {
                    postTextQue();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                ViewInject.toast("服务评价不能为空！");
            }
        } else {
            mTop.getTv_right().setClickable(false);
            mDialog.startLoading();
            try {
                postTextQue();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    Button cancelBt;
    Button trueBt;

    void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        cancelBt = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
        trueBt = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();
                finish();
            }
        });

    }

    public static final int START_S_C = 1;      //上传开始
    public static final int JIN_DU = 2;         //上传进度更新
    public static final int WANCHENG_S_C = 3;   //上传完成（成功）
    public static final int SHIBAI_S_C = 4;     //上传失败
    public static final int WANCHENG_C_X = 5;     //重新上传成功（包括贴纸修改上传、上传失败重新上传）
    public static final int SHIBAI_C_X = 6;     //重新上传失败（包括贴纸修改上传、上传失败重新上传）
    public static final int VIDEO_PROGRESS = 7;     //视频上传进度
    public static final int VIDEO_SUCCESS = 8;     //视频上传成功
    public static final int VIDEO_FAILURE = 9;     //视频上传失败
    public static final int IMAG_BEFORE_PROGRESS = 10;     //术前照片进度
    public static final int IMAG_BEFORE_SUCCESS = 11;     //术前照片上传成功
    public static final int IMAG_BEFORE_FAILURE = 12;     //术前照片上传失败
    public static final int CONSUMER_CERTIFICATE_START = 13;     //消费凭证开始上传
    public static final int CONSUMER_CERTIFICATE_LODING = 14;     //消费凭证上传中
    public static final int CONSUMER_CERTIFICATE_SUCCESS = 15;     //消费凭证上传完成

    HashMap<String, Object> mSameData = new HashMap<>();            //图片上传完成后返回的图片地址集合。key是本地存储路径，vle是服务器连接
    HashMap<String, String> mErrorImg = new HashMap<>();            //图片上传失败后保存的一个集合。key：本地存储路径，vle：本地存储路径
    HashMap<String, Object> mVoucher = new HashMap<>();            //消费凭证上传成功后的集合。key：本地存储路径，vle：本地存储路径
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @TargetApi(Build.VERSION_CODES.KITKAT)
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case START_S_C:             //开始上传（包括图片返回上传、贴纸修改的上传、重新上传）

                    String lianXu = (String) msg.obj;

                    if ("0".equals(lianXu)) {

                        postFileQue(msg.arg1, true);

                    } else if ("1".equals(lianXu)) {

                        postFileQue(msg.arg1, false);

                    } else {

                    }

                    break;
                case JIN_DU:               //上传进度

                    int progress = msg.arg1;
                    ProcessImageView mImgs = (ProcessImageView) msg.obj;
                    mImgs.startHua(ProcessImageView.SAHNG_CHUAN_ZHONG, progress);             //开始画图

                    break;
                case WANCHENG_S_C:  //完成上传（成功）
                    Log.e("TAG", "1111");
                    int pos = msg.arg1;
                    JSONObject jsonObject = setJson();
                    mSameData.put(mResults.get(pos), jsonObject);
                    ProcessImageView imgView = processImages.get(mResults.get(pos));
                    imgView.startHua(ProcessImageView.WANC_HENG, 100);      //设置图片UI
                    if (pos != mResults.size() - 1) {     //不是最后一个,继续上传
                        mySendMessage(pos + 1, "0", START_S_C);
                    } else {
                        notClick = false;           //设置按钮可以点击
                        setContentSelected(true);
                    }
                    adapter.notifyDataSetChanged();
                    break;

                case SHIBAI_S_C:  //上传失败
                    int posSb = msg.arg1;
                    mErrorImg.put(mResults.get(posSb), mResults.get(posSb));      //上传失败后设置失败图片的路径

                    ProcessImageView imgViewSb = processImages.get(mResults.get(posSb));
                    imgViewSb.startHua(ProcessImageView.SHI_BAI, 0);             //设置图片UI

                    if (posSb != mResults.size() - 1) {     //不是最后一个,继续上传
                        mySendMessage(posSb + 1, "0", START_S_C);
                    } else {
                        notClick = false;       //设置按钮可以点击
                        setContentSelected(true);
                    }
                    adapter.notifyDataSetChanged();
                    break;

                case WANCHENG_C_X:                 //重新上传后成功（包括贴纸返回，图片重新上传）

                    int pos1 = msg.arg1;
                    JSONObject jsonObject1 = setJson();
                    mSameData.put(mResults.get(pos1), jsonObject1);
                    if (mErrorImg.get(mResults.get(pos1)) != null) {
                        mErrorImg.remove(mResults.get(pos1));               //如果这个是上传失败重新上传的图片，那么删除失败集合的数据
                    }

                    ProcessImageView imgView1 = processImages.get(mResults.get(pos1));
                    imgView1.startHua(ProcessImageView.WANC_HENG, 100);              //设置图片UI

                    notClick = false;
                    setContentSelected(true);
                    adapter.notifyDataSetChanged();
                    break;

                case SHIBAI_C_X:               //重新上传后失败（包括贴纸返回，图片重新上传）

                    int posSb1 = msg.arg1;
                    mErrorImg.put(mResults.get(posSb1), mResults.get(posSb1));      //上传失败后设置失败图片的路径

                    ProcessImageView imgViewSb1 = processImages.get(mResults.get(posSb1));
                    imgViewSb1.startHua(ProcessImageView.SHI_BAI, 0);             //设置图片UI

                    notClick = false;
                    setContentSelected(true);
                    break;

                case VIDEO_PROGRESS:               //视频上传进度

                    int prog = msg.arg1;
                    Log.e(TAG, "prog == " + prog);
                    thumVideo.startHua(ProcessImageView.SAHNG_CHUAN_ZHONG, prog, true);
                    if (prog == 100) {
                        notClick = false;
                        setContentSelected(true);
                    }

                    break;
                case VIDEO_SUCCESS:                 //视频上传成功

                    videoResult = (WriteVideoResult) msg.obj;

                    thumVideo.startHua(ProcessImageView.WANC_HENG, 100, true);

                    errorFile = null;

                    notClick = false;
                    setContentSelected(true);
                    break;

                case VIDEO_FAILURE:                 //视频上传失败

                    errorFile = (File) msg.obj;
                    Log.e("GGG", "errorFile == " + errorFile);
                    thumVideo.startHua(ProcessImageView.SHI_BAI, 0, true);

                    notClick = false;
                    setContentSelected(true);
                    break;

                case IMAG_BEFORE_PROGRESS:               //术前上传进度
                    int progress1 = msg.arg1;
                    ProcessImageView mImgs1 = (ProcessImageView) msg.obj;
                    mImgs1.startHua(ProcessImageView.SAHNG_CHUAN_ZHONG, progress1);             //开始画图
                    break;
                case IMAG_BEFORE_SUCCESS:                 //术前上传成功

                    int drrNum = msg.arg1;
                    JSONObject jsonObject3 = setJson();
                    drr_[drrNum - 1] = jsonObject3;
                    errorDrrNum.remove(drrNum + "");

                    if (drrNum == 1) {
                        if (shCover == 1) {
                            addIv1.startHua(ProcessImageView.WANC_HENG, 100, false, true, false);
                        } else {
                            addIv1.startHua(ProcessImageView.WANC_HENG, 100);
                        }
                    } else if (drrNum == 2) {
                        addIv2.startHua(ProcessImageView.WANC_HENG, 100);
                    } else if (drrNum == 3) {
                        addIv3.startHua(ProcessImageView.WANC_HENG, 100);
                    } else if (drrNum == 4) {
                        addIv4.startHua(ProcessImageView.WANC_HENG, 100);
                    }

                    break;

                case IMAG_BEFORE_FAILURE:                 //术前上传失败
                    int drrNum1 = msg.arg1;
                    errorDrrNum.put(drrNum1 + "", drrNum1 + "");

                    if (drrNum1 == 1) {
                        if (shCover == 1) {
                            addIv1.startHua(ProcessImageView.SHI_BAI, 0, false, true, false);
                        } else {
                            addIv1.startHua(ProcessImageView.SHI_BAI, 0);
                        }
                    } else if (drrNum1 == 2) {
                        addIv2.startHua(ProcessImageView.SHI_BAI, 0);
                    } else if (drrNum1 == 3) {
                        addIv3.startHua(ProcessImageView.SHI_BAI, 0);
                    } else if (drrNum1 == 4) {
                        addIv4.startHua(ProcessImageView.SHI_BAI, 0);
                    }
                    break;
                case CONSUMER_CERTIFICATE_START:    //消费凭证开始上传
                    upLoadFile();
                    click_consumer_certificate.setClickable(false);
                    break;
                case CONSUMER_CERTIFICATE_LODING:  //消费凭证上传中
                    modification.setText("上传中");
                    click_consumer_certificate.setClickable(false);
                    break;
                case CONSUMER_CERTIFICATE_SUCCESS:  //消费凭证上传成功
                    click_consumer_certificate.setClickable(true);
                    modification.setText("修改");
                    ViewInject.toast("消费凭证上传成功！ ");
                    consumer_certificate = true;
                    JSONObject jsonObject2 = setJson();
                    mVoucher.put(mResults2.get(0), jsonObject2);
                    break;
            }
        }
    };

    private JSONObject setJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("width", mImageWidthHeight[0]);
            jsonObject.put("height", mImageWidthHeight[1]);
            jsonObject.put("img", mKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     * 初始化
     */
    public void gridviewInit() {

        adapter = new ImageUploadAdapter2(this, mResults, mSameData, mErrorImg, mImgCover);

        setGridViewSize();

        gridview.setAdapter(adapter);

        //删除回调
        adapter.setOnItemDeleteClickListener(new ImageUploadAdapter2.onItemDeleteListener() {
            @Override
            public void onDeleteClick(int i) {
                if (notClick) {
                    Toast.makeText(mContext, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    if (mErrorImg.get(mResults.get(i)) != null) {
                        mErrorImg.remove(mResults.get(i));
                    }

                    if (mSameData.get(mResults.get(i)) != null) {
                        mSameData.remove(mResults.get(i));
                    }

                    mResults.remove(i);

                    if (mResults.size() > 0) {
                        mImgCover = mResults.get(0);
                    } else {
                        mImgCover = "";
                    }

                    gridviewInit();
                }
            }
        });
        //上传照片监听
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, final int pos, long arg3) {
                if (notClick) {
                    Toast.makeText(mContext, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    Utils.hideSoftKeyboard(mContext);

                    biaoqingContentLy.setVisibility(View.GONE);

                    //版本判断
                    if (Build.VERSION.SDK_INT >= 23) {

                        Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                            @Override
                            public void onGranted() {
                                toXIangce(pos);
                            }

                            @Override
                            public void onDenied(List<String> permissions) {
//                                    Toast.makeText(mContext, "没有权限", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        toXIangce(pos);
                    }

                }
            }

        });

    }

    /**
     * 设置gritView的宽高
     */
    private void setGridViewSize() {
        int size = 0;
        if (mResults.size() < 9) {
            size = mResults.size() + 2;
        } else {
            size = mResults.size() + 1;
        }

        if (mResults.size() == 0) {
            tipsPhoto.setVisibility(View.VISIBLE);
        } else {
            tipsPhoto.setVisibility(View.GONE);
        }

        ViewGroup.LayoutParams params = gridview.getLayoutParams();
        int sizeType = size > 4 ? size > 8 ? 3 : 2 : 1;
        int height = (sizeType * (int) (dp * 9.4f)) + (sizeType == 3 ? 12 : sizeType == 3 ? 30 : sizeType == 2 ? 20 : 10);
        params.height = height;                             //设置高度
        gridview.setLayoutParams(params);
        gridview.setColumnWidth((int) (dp * 9.4f));         //设置列宽
    }

    void toXIangce() {
        Log.e(TAG, "mResults2 === " + mResults2.size());
        // 启动多个图片选择器
        Intent intent = new Intent(WriteNoteActivity.this, ImagesSelectorActivity.class);
        // 要选择的图像的最大数量
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
        // 将显示的最小尺寸图像;用于过滤微小的图像(主要是图标)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 50000);
        // 显示摄像机或不
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
        // 将当前选定的图像作为初始值传递
        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults2);
        // 开始选择器
        startActivityForResult(intent, FROM_GALLERY);

    }

    void toXIangce(int pos) {
        if (pos == mResults.size()) {       //视频
            if (mResults.size() > 0) {
                showDialogExitEdit3("一篇日记内容只能选择视频或图片一种形式", "我知道了");
            } else {
                if (errorFile != null) {                              //如果是上传失败的
                    gridviewVideoInit(selectNum);       //视频上传
                } else {
                    Intent intent = new Intent(mContext, SelectVideoActivity.class);
                    intent.putExtra("selectNum", selectNum);
                    startActivityForResult(intent, VIDEO_REQUEST_CODE);
                    overridePendingTransition(R.anim.activity_open, 0);
                }
            }
        } else if (pos == mResults.size() + 1) {
            String sdcardState = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(sdcardState)) {

                pageJumpManager.jumpToImagesSelectorActivity(REQUEST_CODE, 9, 50000, true, mResults);

            } else {
                Toast.makeText(getApplicationContext(), "sdcard已拔出，不能选择照片", Toast.LENGTH_SHORT).show();
            }

        } else {
            mZhipaths = mResults.get(pos);

            if (mErrorImg.get(mZhipaths) != null) {          //是上传失败的图片
                Log.e("TAG", "是上传失败的图片");
                mySendMessage(pos, "1", START_S_C);        //发送不连续上传的消息

            } else {
                Intent it = new Intent(mContext, EditImageActivity.class);
                it.putExtra(EditImageActivity.FILE_PATH, mZhipaths);
                it.putExtra(EditImageActivity.EXTRA_COVER, "1");
                File outputFile = FileUtils.getEmptyFile("yuemei" + System.currentTimeMillis() + ".jpg");
                it.putExtra(EditImageActivity.EXTRA_OUTPUT, outputFile.getAbsolutePath());
                it.putExtra("pos", pos + "");
                if (mImgCover.equals(mZhipaths)) {
                    it.putExtra("isCover", true);
                } else {
                    it.putExtra("isCover", false);
                }
                startActivityForResult(it, ACTION_REQUEST_EDITIMAGE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String pathS = Environment.getExternalStorageDirectory().toString() + "/AyuemeiImage";
        File path1 = new File(pathS);
        if (!path1.exists()) {
            path1.mkdirs();
        }
        File imageCrop = new File(path1, "yuemei_crop_" + System.currentTimeMillis() + ".JPEG");
        switch (requestCode) {
            case FONT:
                if (data != null) {
                    postContent.setFocusable(true);
                    postContent.setFocusableInTouchMode(true);
                    postContent.requestFocus();
                    String ss = data.getStringExtra("text_str");
                    if (ss != null) {
                        postContent.setText(ss);
                        postContent.setSelection(textStr.length()); //将光标移至文字末尾
                    }
                }
                break;
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (null != data) {
                        mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);

                        if (mResults != null && mResults.size() > 0) {
                            if (mImgCover == null || "".equals(mImgCover)) {
                                mImgCover = mResults.get(0);
                            }
                        }

                        gridviewInit();

                        pictureReorder();

                        if (mResults.size() > 0) {
                            new CountDownTimer(350, 100) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {

                                    if (Utils.isValidContext(mContext)) {

                                        String ispop = Cfg.loadStr(mContext, "poppop", "");
                                        if (ispop.equals("1")) {

                                        } else {
                                            wpicPop = new WritePicPopWindow(mContext);


                                            if (Build.VERSION.SDK_INT > 24) {

                                                int[] location = new int[2];
                                                tuPicFly.getLocationOnScreen(location);
                                                int x = location[0];
                                                int y = location[1];
                                                Log.e("xxxxxx", "7.0以上x : " + x + ", 7.0以上y : " + y);
                                                wpicPop.showAtLocation(tuPicFly, Gravity.NO_GRAVITY, 0, y - tuPicFly.getHeight() / 2 - DensityUtil.dip2px(10));

                                            } else {
                                                // 适配 android 7.0
                                                int[] location = new int[2];
                                                tuPicFly.getLocationOnScreen(location);
                                                int x = location[0];
                                                int y = location[1];
                                                Log.e("xxxxxx", "7.0  7.0以下x : " + x + ", 7.0y : " + y);
                                                wpicPop.showAtLocation(tuPicFly, Gravity.NO_GRAVITY, 0, y - tuPicFly.getHeight() / 2 - DensityUtil.dip2px(10));
                                            }

                                            Cfg.saveStr(mContext, "poppop", "1");
                                        }
                                    }

                                }
                            }.start();
                        }
                    }
                }
                break;
            case ACTION_REQUEST_EDITIMAGE://添加贴纸后的回调
                if (null != data) {
                    String newFilePath = data.getStringExtra("save_file_path");
                    String poss = data.getStringExtra("pos");
                    String dele = data.getStringExtra("dele");
                    boolean isCover = data.getBooleanExtra("isCover", false);               //当前这个

                    Log.e(TAG, "newFilePath === " + newFilePath);
                    Log.e(TAG, "poss === " + poss);
                    Log.e(TAG, "dele === " + dele);
                    Log.e(TAG, "isCover === " + isCover);

                    if (dele.equals("0")) {
                        mResults.set(Integer.parseInt(poss), newFilePath);
                        if (isCover) {
                            mImgCover = newFilePath;
                        } else {
                            if (mImgCover.equals(newFilePath)) {
                                mImgCover = mResults.get(0);
                            }
                        }

                        gridviewInit();
                        if (!mZhipaths.equals(newFilePath)) {
                            mSameData.remove(mResults.get(Integer.parseInt(poss)));         //删除成功后的图片集合(不管是删除还是修改都是要删除的)
                            mySendMessage(Integer.parseInt(poss), "1", START_S_C);
                        }
                    } else {
                        mSameData.remove(mResults.get(Integer.parseInt(poss)));         //删除成功后的图片集合(不管是删除还是修改都是要删除的)
                        mResults.remove(Integer.parseInt(poss));                        //删除集合中的数据

                        Log.e(TAG, "mResults == " + mResults.size());
                        Log.e(TAG, "mResults == " + mResults);
                        if (isCover && mResults.size() > 0) {
                            mImgCover = mResults.get(0);
                        }

                        gridviewInit();
                    }
                }
                break;
            case IMAGE_REQUEST_CODE:                    //相册照片的选择
                if (data != null) {
                    resizeImage(data.getData(), imageCrop);
                }
                break;
            case CAMERA_REQUEST_CODE:                   //也是拍照后的选择
                if (resultCode == -1) {
                    File file = new File(path1, "yuemei" + ".JPEG");
                    Uri filtUri = uriFromFile(WriteNoteActivity.this, file);
                    resizeImage(filtUri, imageCrop);
                }
                break;

            case RESIZE_REQUEST_CODE:                       //调整大小后的选择（这个真正执行的地方）
                if (data != null && resultCode == -1) {
                    showResizeImage(data);
                }
                break;

            case VIDEO_REQUEST_CODE:                //视频的回调

                String selectType = "";
                if (data != null) {
                    selectType = data.getStringExtra("selectNum");
                }

                if (selectType != null && selectType.length() > 0) {
                    videoDuration = data.getStringExtra("duration");
                    Log.e(TAG, "videoDuration111 == " + videoDuration);
                    selectNum = selectType;
                    editorCover.setVisibility(View.VISIBLE);

                    noteVideo.setVisibility(View.VISIBLE);
                    gridview.setVisibility(View.GONE);

                    gridviewVideoInit(selectNum);       //视频上传
                }

                break;

            case EDITOR_COVER:              //编辑视频封面
                imgCoverPath = data.getStringExtra("imgPath");

                ViewGroup.LayoutParams l = noteCover.getLayoutParams();
                l.width = coverImgWidth;
                l.height = coverImgHeight;
                noteCover.setLayoutParams(l);

                Glide.with(mContext).load(imgCoverPath).into(noteCover);

                uploadCoverImage(imgCoverPath);      //自己选择的封面上传
                break;
            case POSTOPERATIVE_TIME:              //术后时间选择
                String afterTiem = data.getStringExtra("tiem");
                if (afterTiem != null && !"".equals(afterTiem)) {
                    postoTime = Integer.parseInt(afterTiem);
                    if (postoTime == 0) {
                        afterDay.setText("手术当天");
                    } else {
                        afterDay.setText("术后第" + postoTime + "天");
                    }
                }
                break;
            case FROM_GALLERY:
                if (resultCode == RESULT_OK) {
                    if (null != data) {
                        mResults2 = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                        if (mResults2 != null && mResults2.size() > 0) {
                            Message msg = Message.obtain();
                            msg.what = CONSUMER_CERTIFICATE_START;
                            mHandler.sendMessage(msg);

                        }
                    }
                }
                break;

        }
    }

    void upLoadFile() {
        // 压缩图片
        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);// 建立这个路径的文件或文件夹
        if (!path1.exists()) {// 如果不存在，就建立文件夹
            path1.mkdirs();
        }
        File file = new File(path1, "yuemei_" + System.currentTimeMillis() + ".jpg");
        String desPath = file.getPath();
        FileUtils.compressPicture(mResults2.get(0), desPath);
        mImageWidthHeight = FileUtils.getImageWidthHeight(desPath);
        mKey = QiNuConfig.getKey();
        MyUploadImage.getMyUploadImage(mContext, mHandler, desPath).upConsumerCertificate(mKey);
    }
    //这个是拍照选择

    public void resizeImage(Uri uri, File outputFile) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        setIntentDataAndType(WriteNoteActivity.this, intent, "image/*", uri, true);
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        // 不启用人脸识别
        intent.putExtra("noFaceDetection", false);
        //return-data为true时，直接返回bitmap，可能会很占内存，不建议，小米等个别机型会出异常！！！
        //所以适配小米等个别机型，裁切后的图片，不能直接使用data返回，应使用uri指向
        //裁切后保存的URI，不属于我们向外共享的，所以可以使用fill://类型的URI
        mOutputUri = Uri.fromFile(outputFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputUri);
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(intent, RESIZE_REQUEST_CODE);
    }

    public void setIntentDataAndType(Context context, Intent intent, String type, Uri fileUri, boolean writeAble) {
        //7.0以上进行适配
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setDataAndType(fileUri, type);
            //临时赋予读写Uri的权限
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (writeAble) {
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
        } else {
            intent.setDataAndType(fileUri, type);
        }
    }

    /**
     * 视频的适配器
     *
     * @param path:视频本地路径
     */
    private void gridviewVideoInit(final String path) {

        tipsPhoto.setVisibility(View.GONE);
        setContentSelected(false);
        notClick = true;

        Glide.with(mContext).load(Uri.fromFile(new File(path))).into(thumVideo);

        thumVideo.startHua(ProcessImageView.WANC_HENG, 0);

        //上传视频到又拍云
        VideoUploadUpyun.getVideoUploadUpyun(mContext, mHandler).uploadVideo(selectNum);

        //上传视频封面到悦美服务器（获取默认第一帧的图片）
        savePicture(path);

        //重新选择视频
        replaceVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notClick) {
                    Toast.makeText(mContext, "视频上传中请稍后再试...", Toast.LENGTH_SHORT).show();
                } else {
                    if (errorFile != null && errorFile.length() > 0) {
                        setContentSelected(false);
                        notClick = true;
                        VideoUploadUpyun.getVideoUploadUpyun(mContext, mHandler).uploadVideo(selectNum);  //重新上传
                    } else {
                        Intent intent = new Intent(mContext, VideoPlayerActivity.class);
                        intent.putExtra("selectNum", selectNum);
                        Log.e(TAG, "videoDuration === " + videoDuration);
                        intent.putExtra("duration", videoDuration);
                        startActivity(intent);
                    }
                }
            }
        });

        //图片点击按钮
        pictureVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notClick) {
                    Toast.makeText(mContext, "视频上传中请稍后再试...", Toast.LENGTH_SHORT).show();
                } else {
                    showDialogExitEdit3("一篇日记内容只能选择视频或图片一种形式", "我知道了");
                }
            }
        });

        //删除当前图片
        delVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notClick) {
                    Toast.makeText(mContext, "视频上传中请稍后再试...", Toast.LENGTH_SHORT).show();
                } else {
                    videoResult = null;
                    tipsPhoto.setVisibility(View.VISIBLE);
                    selectNum = "";
                    errorFile = null;
                    noteVideo.setVisibility(View.GONE);
                    gridview.setVisibility(View.VISIBLE);
                    editorCover.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 上传视频封面图
     */
    private void uploadCoverImage(String path) {
        String qiniutoken = Cfg.loadStr(mContext, FinalConstant.QINIUTOKEN, "");
        titleCover.setText("上传封面中");
        UploadManager uploadManager = QiNiuConfigration.getInstance().init();
        File file = new File(path);
        mImageWidthHeight = FileUtils.getImageWidthHeight(path);
        mKey = QiNuConfig.getKey();
        uploadManager.put(file, mKey, qiniutoken, new UpCompletionHandler() {
            @Override
            public void complete(String key, ResponseInfo info, JSONObject response) {
                if (info.isOK()) {
                    Log.e(TAG, "Upload Success");
                    JSONObject jsonObject = setJson();
                    titleCover.setText("编辑视频封面");
                    videoCoverState = true;
                    videoCoverUrl = jsonObject;
                } else {
                    try {
                        if (response != null) {
                            String error = response.getString("error");
                            if ("expired token".equals(error)) {
                                new QiNiuTokenApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData serverData) {
                                        if ("1".equals(serverData.code)) {
                                            try {
                                                QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                                                Cfg.saveStr(mContext, FinalConstant.QINIUTOKEN, qiNiuBean.getQiniu_token());
                                                uploadCoverImage(imgCoverPath);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            } else {
                                Log.e(TAG, "Upload Fail");
                                titleCover.setText("上传失败，点击重试");
                                videoCoverState = false;
                            }
                        } else {
                            Log.e(TAG, "Upload Fail");
                            titleCover.setText("上传失败，点击重试");
                            videoCoverState = false;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                Log.e(TAG, "key==" + key + "info==" + info + "response==" + response);
            }
        }, null);

    }

    /**
     * 保存默认封面图图片
     *
     * @param path:视频路径
     * @return 第一帧图片保存路径
     */
    private void savePicture(String path) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(path);
        Bitmap bitmap = retriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);    //获取第一帧的数据

        int imgWidth = bitmap.getWidth();
        int imgHeight = bitmap.getHeight();

        //术后封面图宽高比设置
        coverImgHeight = DensityUtil.dip2px(99);
        coverImgWidth = imgWidth * coverImgHeight / imgHeight;

        ViewGroup.LayoutParams l = noteCover.getLayoutParams();
        l.width = coverImgWidth;
        l.height = coverImgHeight;
        noteCover.setLayoutParams(l);

        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);   // 建立这个路径的文件或文件夹
        if (!path1.exists()) {      // 如果不存在，就建立文件夹
            path1.mkdirs();
        }

        String pathPic = pathS + "/yuemei_" + System.currentTimeMillis() + ".jpg";

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(pathPic);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.close();

            imgCoverPath = pathPic;

//            Picasso.with(mContext)                               //视频封面
//                    .load(new File(imgCoverPath)).into(noteCover);
            Glide.with(mContext)                               //视频封面
                    .load(new File(imgCoverPath))
                    .into(noteCover);
            uploadCoverImage(imgCoverPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //术前照片选择
    private void showResizeImage(Intent data) {
        if (data != null) {
            Bitmap photo = null;
            try {
                photo = BitmapFactory.decodeStream(getContentResolver().openInputStream(mOutputUri));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            final String picName = "yuemei" + System.currentTimeMillis();
            FileUtils.saveBitmap(photo, picName);// 保存图片
            String filePath = FileUtils.SDPATH + picName + ".jpg";
            Drawable drawable = new BitmapDrawable(photo);
            mImageWidthHeight = FileUtils.getImageWidthHeight(filePath);
            mKey = QiNuConfig.getKey();
            if (iv_type.equals("1")) {
                addIv1.setImageDrawable(drawable);
                addIvBt1.setVisibility(View.VISIBLE);
                beforeMap.put("1", filePath);
                beforeKey.put("1", mKey);
                // 压缩图片
                MyUploadImage2.getMyUploadImage(mContext, 1, mHandler, filePath, addIv1).uploadImage(mKey);
            } else if (iv_type.equals("2")) {
                addIv2.setImageDrawable(drawable);
                addIvBt2.setVisibility(View.VISIBLE);
                beforeMap.put("2", filePath);
                beforeKey.put("2", mKey);
                MyUploadImage2.getMyUploadImage(mContext, 2, mHandler, filePath, addIv2).uploadImage(mKey);
            } else if (iv_type.equals("3")) {
                addIv3.setImageDrawable(drawable);
                addIvBt3.setVisibility(View.VISIBLE);
                beforeMap.put("3", filePath);
                beforeKey.put("3", mKey);
                MyUploadImage2.getMyUploadImage(mContext, 3, mHandler, filePath, addIv3).uploadImage(mKey);
            } else if (iv_type.equals("4")) {
                addIv4.setImageDrawable(drawable);
                addIvBt4.setVisibility(View.VISIBLE);
                beforeMap.put("4", filePath);
                beforeKey.put("4", mKey);
                MyUploadImage2.getMyUploadImage(mContext, 4, mHandler, filePath, addIv4).uploadImage(mKey);
            }

        }
    }

    /**
     * 图片重新排序
     */
    private void pictureReorder() {
        int pos = 0;
        if (mSameData.size() == 0 && mErrorImg.size() == 0) {     //如果是第一次添加图片
            mySendMessage(pos, "0", START_S_C);
        } else {
            for (int i = 0; i < mResults.size(); i++) {
                if (mSameData.get(mResults.get(i)) == null && mErrorImg.get(mResults.get(i)) == null) {       //说明是新增的图片
                    pos = i;
                    break;
                }
            }
            mySendMessage(pos, "0", START_S_C);
        }

    }

    /**
     * 发送图片上传的消息
     *
     * @param arg：第几个位置
     * @param lianXu：是否连续上传：0连续上传，1不连续上传
     * @param state：状态
     */
    private void mySendMessage(int arg, String lianXu, int state) {
        notClick = true;
        setContentSelected(false);
        Message msg = Message.obtain();
        msg.arg1 = arg;
        msg.obj = lianXu;
        msg.what = state;
        mHandler.sendMessage(msg);
    }

    /**
     * 上传图片地址和文字
     */
    void postFileQue(int pos, boolean isLianXu) {
        // 压缩图片
        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);// 建立这个路径的文件或文件夹
        if (!path1.exists()) {// 如果不存在，就建立文件夹
            path1.mkdirs();
        }
        File file = new File(path1, "yuemei_" + pos + System.currentTimeMillis() + ".jpg");
        String desPath = file.getPath();
        FileUtils.compressPicture(mResults.get(pos), desPath);
        // 压缩图片
        mImageWidthHeight = FileUtils.getImageWidthHeight(desPath);
        processImages = adapter.getProcessImage();
        ProcessImageView image = processImages.get(mResults.get(pos));
        mKey = QiNuConfig.getKey();
        MyUploadImage.getMyUploadImage(mContext, pos, mHandler, desPath, image, isLianXu).uploadImage(mKey);
    }

    /**
     * 上传图片文件
     */
    void postTextQue() {
        mTop.getTv_right().setEnabled(false);
        //内容图片存储路径转换为json串
        ArrayList<Object> typeData = new ArrayList<>();
        for (int i = 0; i < mResults.size(); i++) {
            Log.e(TAG, "mResults" + mResults.get(i));
            if (mSameData.get(mResults.get(i)) != null) {
                Log.e(TAG, "mSameData" + mSameData.get(mResults.get(i)));
                typeData.add(mSameData.get(mResults.get(i)));
            }
        }
        Log.e(TAG, "image===---" + typeData.toString());
        //术前照片存储路径转化为json串
        Map<String, Object> beforeData = new HashMap<>();
        for (int j = 0; j < 4; j++) {
            if (null != drr_[j] && drr_[j].length() > 0) {
                beforeData.put((j + 1) + "", drr_[j]);
                Log.d(TAG, "beforeData:==>" + (j + 1) + drr_[j]);
            }
        }
        String beforeImageUrls = "";
        try {
            beforeImageUrls = JSONUtil.toJSONString(beforeData);
            Log.e(TAG, "beforeimage===---" + beforeImageUrls);
        } catch (Exception e) {
            e.printStackTrace();
        }


        //对比图Json串
        Map<String, Object> contrastData = new HashMap<>();
        if (drr_[shCover - 1] != null) {
            contrastData.put("before", drr_[shCover - 1]);        //术前封面
            Log.d(TAG, "contrastData:==>before--" + drr_[shCover - 1].toString());
        }

        if (mResults.size() > 0) {
            if (mSameData.get(mResults.get(0)) != null) {
                contrastData.put("after", mSameData.get(mResults.get(0)));        //术后封面
                Log.d(TAG, "contrastData:==>after--" + mSameData.get(mResults.get(0)));
            }
        }
        String contrastImageUrls = "";
        try {
            contrastImageUrls = JSONUtil.toJSONString(contrastData);
            Log.e(TAG, "compare===---" + contrastImageUrls);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //视频参数转化为json串
        String videoUrls = "";
        if (videoResult != null) {
            if (videoCoverUrl != null) {

                Map<String, Object> videoData = new HashMap<>();
                Log.e(TAG, "videoResult.getFile_size() == " + videoResult.getFile_size());
                Log.e(TAG, "videoResult.getMimetype() == " + videoResult.getMimetype());
                Log.e(TAG, "videoResult.getUrl() == " + videoResult.getUrl());
                Log.e(TAG, "(Integer.parseInt(videoDuration) / 1000) == " + (Integer.parseInt(videoDuration) / 1000));
                Log.e(TAG, "videoCoverUrl == " + videoCoverUrl);
                videoData.put("file_size", videoResult.getFile_size() + "");
                videoData.put("mimetype", videoResult.getMimetype());
                videoData.put("url", videoResult.getUrl());
                videoData.put("video_time", (Integer.parseInt(videoDuration) / 1000) + "");
                videoData.put("cover", videoCoverUrl);

                try {
                    videoUrls = JSONUtil.toJSONString(videoData);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "videoUrls == " + videoUrls);

            } else {
                mDialog.stopLoading();
                mTop.getTv_right().setClickable(true);
                showDialogExitEdit3("上传视频封面失败，请重试", "确定");
                return;
            }
        }

        content = postContent.getText().toString().trim();
        Map<String, Object> maps = new HashMap<>();
        maps.put("cateid", cateid);          //标签Id
        Log.e(TAG, "mGson.toJson(new ForumTextData(content)) === " + mGson.toJson(new ForumTextData(content)));
        maps.put("content", mGson.toJson(new ForumTextData(content)));        //内容（必填）
        maps.put("visibility", visibility);  //
        maps.put("hosname", hosname);        //医院名称
        maps.put("docname", docname);        //医生名称
        maps.put("fee", fee);                //订单金额
        maps.put("hosid", hosid);            //医院id
        maps.put("userid", userid);          //医生user_id
        maps.put("taoid", taoid);            //SkuId
        maps.put("server_id", server_id);    //代表订单号
        maps.put("sharetime", dateStr);      //消费时间
        maps.put("surgeryafterdays", postoTime + "");      //日记添加术后Y天
        maps.put("service", ratN1);          //服务评分
        maps.put("effect", ratN2);           //效果评分
        maps.put("pf_doctor", ratN3);        //医生评分
        maps.put("image", typeData.toString());        //内容 图片上传
        maps.put("beforeimage", beforeImageUrls);        //术前图片上传
        maps.put("compare", contrastImageUrls);        //术前图片上传

        if (mVoucher.size() > 0 && mResults2.size() > 0) {
            maps.put("consumer_voucher", mVoucher.get(mResults2.get(0)).toString()); //消费凭证上传
        }

        if (!TextUtils.isEmpty(videoUrls)) {
            maps.put("video", videoUrls);        //内容视频上传
        }
        maps.put("title", noteTitle);//日记本标题
        maps.put("id", noteId);//日记本id
        maps.put("cover", "");//视频封面图
        new PostTextQueApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mDialog.stopLoading();
                mTop.getTv_right().setEnabled(true);
                if ("1".equals(serverData.code)) {
                    //打点统计写日记成功
                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);    //获取年
                    int month = cal.get(Calendar.MONTH) + 1;   //获取月份，0表示1月份
                    int day = cal.get(Calendar.DAY_OF_MONTH);    //获取当前天数

                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("Time", year + month + day + "");
                    StatisticalManage.getInstance().growingIO("Keep_diary", hashMap);


                    StatService.onEvent(mContext, "003", "日记贴", 1);

                    WriteResultData data = null;
                    try {
                        data = JSONUtil.TransformSingleBean(serverData.data, WriteResultData.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    MyToast.makeImgToast(mContext, getResources().getDrawable(R.drawable.tips_submit_success2x), 1000).show();
                    String ifoneLogin = data.getOnelogin();
                    String url = data.getAppmurl();
                    String qid = data.get_id();
                    String appmurl = data.getAppmurl();

                    String isFirst2 = Cfg.loadStr(mContext, FinalConstant.ISFIRST2, "");
                    if (isFirst2.equals("1")) {

                        if (null != addtype && addtype.equals("2")) {
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                            finish();
                        } else {
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                            finish();
                        }


                    } else {
                        if (ifoneLogin.equals("1")) {// 第一次登录发帖
                            Intent it = new Intent();
                            it.setClass(mContext, SubmitSuccess1Activity.class);
                            it.putExtra("url", url);
                            it.putExtra("qid", qid);
                            startActivity(it);
                            Cfg.saveStr(mContext, FinalConstant.ISFIRST2, "1");
                            finish();
                        } else {

                            if (null != addtype && addtype.equals("2")) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                                finish();
                            } else {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                                finish();
                            }
                        }
                    }

                    sumitHttpCode("4");

                } else {
                    ViewInject.toast(serverData.message);
                    mTop.getTv_right().setClickable(true);

                    StatService.onEvent(mContext, "017", "写日记", 1);
                    TCAgent.onEvent(mContext, "帖子提交失败", "日记帖0");
                }
            }
        });
    }

    /**
     * 清除缓存的图片
     */
    protected void onDestroy() {
        super.onDestroy();
    }

    void sumitHttpCode(final String flag) {
        SumitHttpAip sumitHttpAip = new SumitHttpAip();
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", flag);
        maps.put("uid", uid);
        sumitHttpAip.getCallBack(mContext, maps, new BaseCallBackListener<JFJY1Data>() {
            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                if (jfjyData != null) {
                    String jifenNu = jfjyData.getIntegral();
                    String jyNu = jfjyData.getExperience();

                    if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                        MyToast.makeTexttext4Toast(mContext, jifenNu, jyNu, 1000).show();
                    } else {
                        if (!jifenNu.equals("0")) {
                            MyToast.makeTexttext2Toast(mContext, jifenNu, 1000).show();
                        } else {
                            if (!jyNu.equals("0")) {
                                MyToast.makeTexttext3Toast(mContext, jyNu, 1000).show();
                            }
                        }
                    }
                }
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (postContent.getText().toString().length() > 0 || mResults.size() > 0) {
                showDialogExitEdit();
            } else {
                finish();
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    private void initViewPager() {
        LayoutInflater inflater = LayoutInflater.from(this);
        grids = new ArrayList<>();
        gView1 = (GridView) inflater.inflate(R.layout.grid1, null);

        List<Map<String, Object>> listItems = new ArrayList<>();
        // 生成28个表情
        for (int i = 0; i < 28; i++) {
            Map<String, Object> listItem = new HashMap<>();
            listItem.put("image", expressionImages[i]);
            listItems.add(listItem);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                if (arg2 == 27) {
                    // 动作按下
                    int action = KeyEvent.ACTION_DOWN;
                    // code:删除，其他code也可以，例如 code = 0
                    int code = KeyEvent.KEYCODE_DEL;
                    KeyEvent event = new KeyEvent(action, code);
                    postContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                } else {
                    Bitmap bitmap = null;
                    bitmap = BitmapFactory.decodeResource(getResources(), expressionImages[arg2 % expressionImages.length]);

                    bitmap = zoomImage(bitmap, 47, 47);
                    ImageSpan imageSpan = new ImageSpan(mContext, bitmap);

                    SpannableString spannableString = new SpannableString(expressionImageNames[arg2].substring(1, expressionImageNames[arg2].length() - 1));

                    spannableString.setSpan(imageSpan, 0, expressionImageNames[arg2].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


                    // 编辑框设置数据
//                    postContent.append(spannableString);
                    int index = postContent.getSelectionStart();//获取光标所在位置

                    Editable edit = postContent.getEditableText();//获取EditText的文字
                    if (index < 0 || index >= edit.length()) {
                        edit.append(spannableString);
                    } else {
                        edit.insert(index, spannableString);//光标所在位置插入文字
                    }
                }
            }
        });
        grids.add(gView1);

        gView2 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView2);
        gView3 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView3);

        // 填充ViewPager的数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return grids.size();
            }

            @Override
            public void destroyItem(View container, int position, Object object) {
                ((ViewPager) container).removeView(grids.get(position));
            }

            @Override
            public Object instantiateItem(View container, int position) {
                ((ViewPager) container).addView(grids.get(position));
                return grids.get(position);
            }
        };

        viewPager.setAdapter(mPagerAdapter);

        viewPager.setOnPageChangeListener(new GuidePageChangeListener());
    }

    // ** 指引页面改监听器 */
    class GuidePageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    page2.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    break;
                case 1:
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    page2.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));

                    List<Map<String, Object>> listItems = new ArrayList<>();

                    // 生成28个表情
                    for (int i = 0; i < 28; i++) {
                        Map<String, Object> listItem = new HashMap<>();
                        listItem.put("image", expressionImages1[i]);
                        listItems.add(listItem);
                    }

                    SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

                    gView2.setAdapter(simpleAdapter);
                    // 表情点击
                    gView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                postContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {

                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(getResources(), expressionImages1[pos % expressionImages1.length]);
                                bitmap = zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mContext, bitmap);
                                SpannableString spannableString = new SpannableString(expressionImageNames1[pos].substring(1, expressionImageNames1[pos].length() - 1));

                                spannableString.setSpan(imageSpan, 0, expressionImageNames1[pos].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                // 编辑框设置数据

                                int index = postContent.getSelectionStart();//获取光标所在位置

                                Editable edit = postContent.getEditableText();//获取EditText的文字
                                if (index < 0 || index >= edit.length()) {
                                    edit.append(spannableString);
                                } else {
                                    edit.insert(index, spannableString);//光标所在位置插入文字
                                }
                            }
                        }
                    });
                    break;
                case 2:
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    page2.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));

                    List<Map<String, Object>> listItems2 = new ArrayList<>();

                    // 生成28个表情
                    for (int i = 0; i < 15; i++) {
                        Map<String, Object> listItem = new HashMap<>();
                        listItem.put("image", expressionImages2[i]);
                        listItems2.add(listItem);
                    }

                    SimpleAdapter simpleAdapter2 = new SimpleAdapter(mContext, listItems2, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

                    gView3.setAdapter(simpleAdapter2);
                    // 表情点击
                    gView3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                postContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {

                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(getResources(), expressionImages2[pos % expressionImages2.length]);
                                bitmap = zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mContext, bitmap);
                                SpannableString spannableString = new SpannableString(expressionImageNames2[pos].substring(1, expressionImageNames2[pos].length() - 1));

                                spannableString.setSpan(imageSpan, 0, expressionImageNames2[pos].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                // 编辑框设置数据

                                int index = postContent.getSelectionStart();//获取光标所在位置

                                Editable edit = postContent.getEditableText();//获取EditText的文字
                                if (index < 0 || index >= edit.length()) {
                                    edit.append(spannableString);
                                } else {
                                    edit.insert(index, spannableString);//光标所在位置插入文字
                                }
                            }
                        }
                    });
                    break;
            }
        }

    }

    //dialog提示
    private void showDialogExitEdit3(String title, String content) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dilog_newuser_yizhuce);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(title);

        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(content);
        cancelBt88.setTextColor(Color.parseColor("#ffa5cc"));
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }


    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width, (int) height, matrix, true);
        return bitmap;
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
