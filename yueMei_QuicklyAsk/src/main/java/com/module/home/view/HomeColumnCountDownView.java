package com.module.home.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

/**
 * Created by 裴成浩 on 2019/9/23
 */
public class HomeColumnCountDownView extends LinearLayout implements Runnable {

    private long[] times;//当前时间
    private long mhour, mmin, msecond;// 天，小时，分钟，秒

    private boolean run = false; // 是否启动了
    private TextView mTimeTextView;

    public HomeColumnCountDownView(Context context) {
        this(context, null);
    }

    public HomeColumnCountDownView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeColumnCountDownView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.home_column_count_down_view, this, true);
        mTimeTextView = view.findViewById(R.id.home_column_count_down_time);
    }

    /**
     * 设置倒计时时间
     *
     * @param times ：时：分：秒
     */
    public void setTimes(long[] times) {
        this.times = times;
        mhour = times[0];
        mmin = times[1];
        msecond = times[2];
    }

    /**
     * 倒计时计算
     */
    private void ComputeTime() {
        msecond--;
        if (msecond < 0) {
            mmin--;
            msecond = 59;
            if (mmin < 0) {
                mmin = 59;
                mhour--;
                if (mhour < 0) {
                    // 倒计时结束，一天有24个小时
                    mhour = 23;
                }
            }
        }
    }

    public boolean isRun() {
        return run;
    }

    public void beginRun() {
        this.run = true;
        run();
    }

    public void stopRun() {
        this.run = false;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void run() {
        // 标示已经启动
        if (run) {
            ComputeTime();

            String mhourStr = null;
            String mminStr = null;
            String msecondStr = null;

            if (msecond < 10) {
                msecondStr = "0" + msecond;
            } else {
                msecondStr = "" + msecond;
            }
            if (mmin < 10) {
                mminStr = "0" + mmin;
            } else {
                mminStr = "" + mmin;
            }
            if (mhour < 10) {
                mhourStr = "0" + mhour;
            } else {
                mhourStr = "" + mhour;
            }

            mTimeTextView.setText(mhourStr + ":" + mminStr + ":" + msecondStr);

            times[0] = mhour;
            times[1] = mmin;
            times[2] = msecond;
            if (mhour == 0 && mmin == 0 && msecond == 0) {
                stopRun();
            } else {
                postDelayed(this, 1000);
            }

        } else {
            removeCallbacks(this);
        }
    }
}