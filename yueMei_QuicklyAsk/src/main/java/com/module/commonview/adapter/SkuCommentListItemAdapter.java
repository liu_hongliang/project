package com.module.commonview.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.entity.Pic;
import com.quicklyask.util.Utils;

import java.util.List;

public class SkuCommentListItemAdapter extends BaseQuickAdapter<Pic,BaseViewHolder> {

    public SkuCommentListItemAdapter(int layoutResId, @Nullable List<Pic> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Pic item) {
        Glide.with(mContext)
                .load(item.getImg())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.ALL))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into((ImageView) helper.getView(R.id.sku_comment_list_list_img));
        if (getData().size() == 3){
            if (!TextUtils.isEmpty(item.getHave_pic())){
                helper.setGone(R.id.sku_comment_list_list_num,true);
                helper.setText(R.id.sku_comment_list_list_num,item.getHave_pic());
            }else {
                helper.setGone(R.id.sku_comment_list_list_num,false);
            }
        }else {
            helper.setGone(R.id.sku_comment_list_list_num,false);
        }
        }
}
