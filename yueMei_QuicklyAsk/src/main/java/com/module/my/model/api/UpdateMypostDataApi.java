package com.module.my.model.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.PostInfoData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * Created by Administrator on 2017/10/27.
 */

public class UpdateMypostDataApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.FORUM, "getpostinfo", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if ("1".equals(mData.code)){
                    try {
                        PostInfoData postInfoData = JSONUtil.TransformSingleBean(mData.data, PostInfoData.class);
                        listener.onSuccess(postInfoData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
