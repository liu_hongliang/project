package com.module.my.model.bean;

/**
 * 扫描关联的订单列表
 * 
 * @author dwb
 * 
 */
public class SaoOrderListData {

	private String doc_name;
	private String price;
	private String order_id;
	private String status;

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
