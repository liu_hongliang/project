package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.activity.ChannelFourActivity;
import com.module.home.controller.activity.ChannelPartsActivity;
import com.module.home.controller.activity.ChannelTwoActivity;
import com.module.home.controller.adapter.ProjectDetailsManualAdapter;
import com.module.home.model.bean.ChannelLabelData;
import com.module.home.model.bean.ChannelLabelListData;
import com.module.home.model.bean.ManualPosition;
import com.module.home.model.bean.ManualPositionBtnBoard;
import com.module.home.model.bean.ManualPositionTag;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.home.model.bean.ProjectDetailsData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 部位页面
 *
 * @author 裴成浩
 * @data 2019/11/4
 */
public class ChannelPartsScrollFrament extends YMBaseFragment {

    @BindView(R.id.fragment_channel_parts_list)
    RecyclerView mFragmentRec;                                     //手工位设置
    private BaseNetWorkCallBackApi mPartAllLabelApi;
    private ArrayList<ManualPosition> mListDatas;
    private ProjectDetailsBean detailsBean;
    private String homeSource;
    private ChannelLabelData channelLabelData;
    private ProjectDetailsManualAdapter mProjectDetailsManualAdapter;

    public static ChannelPartsScrollFrament newInstance(ProjectDetailsData data, ProjectDetailsBean detailsBean) {
        ChannelPartsScrollFrament channelPartsScrollFrament = new ChannelPartsScrollFrament();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putParcelable("detailsBean", detailsBean);
        channelPartsScrollFrament.setArguments(bundle);
        return channelPartsScrollFrament;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_channel_parts_top;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            ProjectDetailsData datas = getArguments().getParcelable("data");
            detailsBean = getArguments().getParcelable("detailsBean");
            if (datas != null) {
                mListDatas = datas.getManualPosition();
            }
        }
        if (mListDatas == null) {
            mListDatas = new ArrayList<>();
        }

        if (mContext instanceof ChannelPartsActivity) {
            homeSource = ((ChannelPartsActivity) mContext).homeSource;
        }

        Log.e(TAG, "mListDatas == " + mListDatas.size());
    }

    @Override
    protected void initData(View view) {
        mPartAllLabelApi = new BaseNetWorkCallBackApi(FinalConstant1.CHANNEL, "getPartAllLabel");

        if (detailsBean != null && getActivity() != null) {
            loadLabelData();
        }
    }

    /**
     * 加载标签数据
     */
    private void loadLabelData() {
        String twoLabelId = detailsBean.getTwoLabelId();
        String fourLabelId = detailsBean.getFourLabelId();


        Log.e(TAG, "twoLabelId == " + twoLabelId);
        Log.e(TAG, "fourLabelId == " + fourLabelId);

        Log.e(TAG, "twoLabelId == " + twoLabelId);
        Log.e(TAG, "fourLabelId == " + fourLabelId);
        if (!TextUtils.isEmpty(fourLabelId)) {
            mPartAllLabelApi.addData("labelID", fourLabelId);
        } else if (!TextUtils.isEmpty(twoLabelId)) {
            mPartAllLabelApi.addData("labelID", twoLabelId);
        }
        mPartAllLabelApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "serverData  = " + serverData.toString());
                if ("1".equals(serverData.code)) {
                    channelLabelData = JSONUtil.TransformSingleBean(serverData.data, ChannelLabelData.class);
                    List<ChannelLabelListData> channelLabelListDatas = channelLabelData.getBoard();
                    for (int i = channelLabelListDatas.size() - 1; i >= 0; i--) {
                        ChannelLabelListData channelLabelListData = channelLabelListDatas.get(i);
                        ManualPositionTag listOne = channelLabelListData.getModule_178_184();
                        ManualPositionTag listTwo = channelLabelListData.getModule_187();
                        ManualPositionTag listThree = channelLabelListData.getModule_181();

                        ManualPosition manualPosition = new ManualPosition();
                        if (listOne != null) {
                            manualPosition.setListOne(listOne);
                        }
                        if (listTwo != null) {
                            manualPosition.setListTwo(listTwo);
                        }
                        if (listThree != null) {
                            manualPosition.setListThree(listThree);
                        }
                        mListDatas.add(0, manualPosition);
                    }
                }
                if (getActivity() != null) {
                    setLabelData();
                }
            }
        });
    }

    /**
     * 设置标签数据
     */
    private void setLabelData() {
        if (mListDatas != null && mListDatas.size() != 0) {
            mFragmentRec.setVisibility(View.VISIBLE);
            ScrollLayoutManager scrollLinear = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            mFragmentRec.setLayoutManager(scrollLinear);
            if (mProjectDetailsManualAdapter == null){
                if (channelLabelData != null){
                    HashMap<String, String> moreEventParams = channelLabelData.getMore_event_params();
                    mProjectDetailsManualAdapter = new ProjectDetailsManualAdapter(mContext, mListDatas, 0, moreEventParams);
                    mFragmentRec.setAdapter(mProjectDetailsManualAdapter);

                    mProjectDetailsManualAdapter.setOnItemClickListener(new ProjectDetailsManualAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position, ManualPositionBtnBoard data) {
                            YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                            if (!TextUtils.isEmpty(data.getUrl())) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(data.getUrl());
                            } else {
                                switch (data.getLevel()) {
                                    case "2":
                                        //部位
                                        Intent intent2 = new Intent(mContext, ChannelPartsActivity.class);
                                        intent2.putExtra("id", data.getId());
                                        intent2.putExtra("title", data.getChannel_title());
                                        intent2.putExtra("home_source", homeSource);
                                        mContext.startActivity(intent2);
                                        break;
                                    case "3":
                                        //二级
                                        Intent intent3 = new Intent(mContext, ChannelTwoActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString(ChannelTwoActivity.TITLE, data.getChannel_title());
                                        bundle.putString(ChannelTwoActivity.TWO_ID, data.getId());
                                        bundle.putString(ChannelTwoActivity.SELECTED_ID, data.getOne_id());
                                        bundle.putString(ChannelTwoActivity.HOME_SOURCE, homeSource);
                                        intent3.putExtra("data", bundle);
                                        mContext.startActivity(intent3);
                                        break;
                                    case "4":
                                        //四级
                                        Intent intent4 = new Intent(mContext, ChannelFourActivity.class);
                                        intent4.putExtra("id", data.getId());
                                        intent4.putExtra("title", data.getChannel_title());
                                        intent4.putExtra("home_source", homeSource);
                                        mContext.startActivity(intent4);
                                        break;
                                }
                            }
                        }
                    });
                }

            }else {
                mProjectDetailsManualAdapter.refreshData(mListDatas);
            }

        } else {
            mFragmentRec.setVisibility(View.GONE);
        }
    }

    public void setData(ProjectDetailsData projectDetailsData, ProjectDetailsBean detailsBean) {
        if (projectDetailsData != null) {
            this.mListDatas = projectDetailsData.getManualPosition();
        }
        this.detailsBean = detailsBean;
        loadLabelData();

    }
}
