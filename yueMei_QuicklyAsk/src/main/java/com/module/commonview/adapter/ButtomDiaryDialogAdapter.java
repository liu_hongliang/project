package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatDataBean;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.module.bean.PostListData;
import com.module.commonview.view.DiaryProductDetailsDialog;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/11.
 */
public class ButtomDiaryDialogAdapter extends BaseAdapter {

    private Context mContext;
    private List<DiaryTaoData> mDiaryTaoDatas;

    public ButtomDiaryDialogAdapter(Context context, List<DiaryTaoData> diaryTaoDatas) {
        this.mContext = context;
        this.mDiaryTaoDatas = diaryTaoDatas;
    }

    @Override
    public int getCount() {
        return mDiaryTaoDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDiaryTaoDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_buttom_diary_dialog, null);
            holder = new ViewHolder();

            holder.img = convertView.findViewById(R.id.item_buttom_diary_img);
            holder.title = convertView.findViewById(R.id.item_buttom_diary_title);
            holder.price = convertView.findViewById(R.id.item_buttom_diary_goods_price);
            holder.unit = convertView.findViewById(R.id.item_buttom_diary_goods_unit);
//            holder.originalPrice = convertView.findViewById(R.id.item_buttom_diary_goods_original_price);
            holder.plusVisibity = convertView.findViewById(R.id.tao_plus_vibility);
            holder.plusPrice = convertView.findViewById(R.id.tao_plus_price);
            holder.tv_consultation = convertView.findViewById(R.id.tv_consultation);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DiaryTaoData taoData = mDiaryTaoDatas.get(position);

        Glide.with(mContext).load(taoData.getList_cover_image()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).into(holder.img);
        holder.title.setText(taoData.getTitle());               //文案
        holder.price.setText(taoData.getPrice_discount());      //价格
        holder.unit.setText(taoData.getFee_scale());            //单位

        String member_price = taoData.getMember_price();
        int i = Integer.parseInt(member_price);
        if (i >= 0) {
            holder.plusVisibity.setVisibility(View.VISIBLE);
            holder.plusPrice.setText("¥" + member_price);
//            holder.originalPrice.setVisibility(View.GONE);
        } else {
            holder.plusVisibity.setVisibility(View.GONE);
//            holder.originalPrice.setVisibility(View.VISIBLE);
//            holder.originalPrice.setText("￥" + taoData.getPrice());       //原价
//            holder.originalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        }

        if("3".equals(mDiaryTaoDatas.get(position).getIs_rongyun())){
            holder.tv_consultation.setVisibility(View.VISIBLE);
        }else{
            holder.tv_consultation.setVisibility(View.INVISIBLE);
        }

        //咨询按钮点击回调
        holder.tv_consultation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemChatCallListener != null) {
                    itemChatCallListener.onItemClick(v, position);
                }
            }
        });
        return convertView;
    }


    class ViewHolder {
        public ImageView img;
        public TextView title;
        public TextView price;
        public TextView unit;
        //        public TextView originalPrice;
        public LinearLayout plusVisibity;
        public TextView plusPrice;
        public TextView tv_consultation;
    }


    //item点击回调
    private ItemChatCallListener itemChatCallListener;

    public interface ItemChatCallListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemChatCallListener(ItemChatCallListener itemChatCallListener) {
        this.itemChatCallListener = itemChatCallListener;
    }
}
