package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.CommonTopBar;
import com.module.home.controller.adapter.ProjectHotztAdapter;
import com.module.home.model.api.LodHotIssueData;
import com.module.home.model.bean.ProjectHotztData;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.DropDownListView2;
import com.quicklyask.view.DropDownListView2.OnDropDownListener2;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 热门专题更多/特价专场
 * 
 * @author dwb
 * 
 */
public class ProjectMoreZTActivity extends BaseActivity {

	private final String TAG = "ProjectMoreZTActivity";
	private ProjectMoreZTActivity mContext;
	private Activity mActy;
	// List
	@BindView(id = R.id.my_doc_collect_list_view)
	private DropDownListView2 homeList;
	private int mCurPage = 1;
	private Handler mHandler;

	private List<ProjectHotztData> lvHotIssueData = new ArrayList<ProjectHotztData>();
	private List<ProjectHotztData> lvHotIssueMoreData = new ArrayList<ProjectHotztData>();
	private ProjectHotztAdapter hotAdpter;

	private String docid;

	@BindView(id = R.id.project_more_top)
	private CommonTopBar mTop;// 返回

	@BindView(id = R.id.my_collect_doc_tv_nodata)
	private LinearLayout nodataTv;

	private LoadingProgress dialog;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_project_more_zhuanti);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = ProjectMoreZTActivity.this;
		mTop.setCenterText("特价专场");

		dialog = new LoadingProgress(mContext);


		initList();
		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						ProjectMoreZTActivity.this.finish();
					}
				});
	}

	public void onResume() {
		super.onResume();

		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	void initList() {

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		mHandler = getHandler();
		dialog.startLoading();
		lodHotIssueData(true);

		homeList.setOnDropDownListener(new OnDropDownListener2() {

			@Override
			public void onDropDown() {
				lvHotIssueData = null;
				lvHotIssueMoreData = null;
				mCurPage = 1;
				dialog.startLoading();
				lodHotIssueData(true);
				homeList.setHasMore(true);
			}
		});

		homeList.setOnBottomListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lodHotIssueData(false);
			}
		});

		homeList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adpter, View v, int pos,
					long arg3) {
				if (lvHotIssueData.size() < 9) {
					String url = lvHotIssueData.get(pos).getUrl();
					String title = lvHotIssueData.get(pos).getTitle();
					String ztid = lvHotIssueData.get(pos).get_id();

					Utils.ztrecordHttp(mContext,"TopicList"+ztid);

					Intent it1 = new Intent();
					it1.setClass(mContext, ZhuanTiWebActivity.class);
					it1.putExtra("url", url);
					it1.putExtra("title", title);
					it1.putExtra("ztid", ztid);
					startActivity(it1);
				} else {
					String url = lvHotIssueData.get(pos - 1).getUrl();
					String title = lvHotIssueData.get(pos - 1).getTitle();
					String ztid = lvHotIssueData.get(pos - 1).get_id();

					Utils.ztrecordHttp(mContext,"TopicList"+ztid);

					Intent it1 = new Intent();
					it1.setClass(mContext, ZhuanTiWebActivity.class);
					it1.putExtra("url", url);
					it1.putExtra("title", title);
					it1.putExtra("ztid", ztid);
					startActivity(it1);
				}
			}
		});
	}

	void lodHotIssueData(final boolean isDonwn) {

		new Thread(new Runnable() {
			@Override
			public void run() {
				HashMap<String,Object> maps=new HashMap<>();
				maps.put("page",mCurPage+"");
				if (isDonwn){
					if (mCurPage ==1){
						new LodHotIssueData().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
							@Override
							public void onSuccess(ServerData s) {
								lvHotIssueData=JSONUtil.jsonToArrayList(s.data,ProjectHotztData.class);
								Message message = mHandler.obtainMessage(1);
								message.sendToTarget();
							}
						});
					}
				}else {
					mCurPage++;
					maps.put("page",mCurPage+"");
					new LodHotIssueData().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
						@Override
						public void onSuccess(ServerData s) {
							lvHotIssueMoreData=JSONUtil.jsonToArrayList(s.data,ProjectHotztData.class);
							Message message = mHandler.obtainMessage(1);
							message.sendToTarget();
						}
					});
				}
			}
		}).start();

	}

	@SuppressLint("HandlerLeak")
	private Handler getHandler() {
		return new Handler() {
			@SuppressLint({ "NewApi", "SimpleDateFormat" })
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					if (null != lvHotIssueData && lvHotIssueData.size() > 0) {
						nodataTv.setVisibility(View.GONE);

						if (lvHotIssueData.size() < 9) {
							homeList.setDropDownStyle(false);
							homeList.setOnBottomStyle(false);
						} else {
							homeList.setDropDownStyle(true);
							homeList.setOnBottomStyle(true);
						}

						dialog.stopLoading();
						hotAdpter = new ProjectHotztAdapter(mContext,
								lvHotIssueData);

						homeList.setAdapter(hotAdpter);

						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"MM-dd HH:mm:ss");

						homeList.onDropDownComplete(getString(R.string.update_at)
								+ dateFormat.format(new Date()));

						homeList.onBottomComplete();
					} else {
						dialog.stopLoading();
						// ViewInject.toast("数据加载完毕");
						nodataTv.setVisibility(View.VISIBLE);
					}
					break;
				case 2:
					if (null != lvHotIssueMoreData
							&& lvHotIssueMoreData.size() > 0) {
						hotAdpter.add(lvHotIssueMoreData);
						hotAdpter.notifyDataSetChanged();
						homeList.onBottomComplete();
					} else {
						homeList.setHasMore(false);
						homeList.setShowFooterWhenNoMore(true);
						homeList.onBottomComplete();
					}
					break;
				}

			}
		};
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
