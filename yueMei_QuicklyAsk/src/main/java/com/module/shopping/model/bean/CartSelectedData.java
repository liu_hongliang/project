package com.module.shopping.model.bean;

/**
 * Created by 裴成浩 on 2018/12/3
 */
public class CartSelectedData {
    private String cart_id;         //flag传0时需要参数
    private String hos_id;          //flag传0时需要参数
    private String flag;            //0 单条购物车 1用户所有购物车 2医院sku
    private String select;          //1选中 0不选中
    private int hosPos;             //当前医院下标

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public int getHosPos() {
        return hosPos;
    }

    public void setHosPos(int hosPos) {
        this.hosPos = hosPos;
    }
}
