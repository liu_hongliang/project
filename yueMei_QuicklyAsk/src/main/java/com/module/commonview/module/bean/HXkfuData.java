package com.module.commonview.module.bean;

/**
 * Created by dwb on 17/4/27.
 */

public class HXkfuData {

    private String id;
    private String img;
    private String name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
