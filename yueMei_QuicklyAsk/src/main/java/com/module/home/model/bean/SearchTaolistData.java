package com.module.home.model.bean;

import java.util.HashMap;

public class SearchTaolistData {

    /**
     * taoTopNum : 1
     * id : 96586
     * title : 注射微整
     * bilateral_title : 注射微整
     * img : https://p24.yuemei.com/tao/2019/0709/200_200/jt190709094750_632c65.jpg
     * price : 780
     * app_url : https://m.yuemei.com/tao/96586/
     * coupons :
     * totalPv : 567
     * labelID : 184
     */

    private String taoTopNum;
    private String id;
    private String title;
    private String bilateral_title;
    private String img;
    private int price;
    private String app_url;
    private String coupons;
    private String totalPv;
    private String labelID;
    private String diaryImg;
    private HashMap<String,String> event_params;
    private int img_resouce;

    public String getTaoTopNum() {
        return taoTopNum;
    }

    public void setTaoTopNum(String taoTopNum) {
        this.taoTopNum = taoTopNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBilateral_title() {
        return bilateral_title;
    }

    public void setBilateral_title(String bilateral_title) {
        this.bilateral_title = bilateral_title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getApp_url() {
        return app_url;
    }

    public void setApp_url(String app_url) {
        this.app_url = app_url;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }

    public String getTotalPv() {
        return totalPv;
    }

    public void setTotalPv(String totalPv) {
        this.totalPv = totalPv;
    }

    public String getLabelID() {
        return labelID;
    }

    public void setLabelID(String labelID) {
        this.labelID = labelID;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public int getImg_resouce() {
        return img_resouce;
    }

    public void setImg_resouce(int img_resouce) {
        this.img_resouce = img_resouce;
    }

    public void setDiaryImg(String diaryImg) {
        this.diaryImg = diaryImg;
    }

    public String getDiaryImg() {
        return diaryImg;
    }
}
