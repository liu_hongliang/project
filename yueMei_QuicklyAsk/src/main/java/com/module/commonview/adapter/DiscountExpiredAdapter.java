package com.module.commonview.adapter;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.quicklyask.activity.R;


/**
 * 文 件 名: DiscountExpiredAdapter
 * 创 建 人: 原成昊
 * 创建日期: 2020-01-17 16:51
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class DiscountExpiredAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    //用于退出 Activity,避免 Countdown，造成资源浪费。
    private SparseArray<CountDownTimer> countDownCounters;

    public DiscountExpiredAdapter(Context context) {
        this.mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.countDownCounters = new SparseArray<>();

    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_discount_expired, null);
            viewHolder = new ViewHolder();

            CountDownTimer countDownTimer = countDownCounters.get(viewHolder.tv_hours.hashCode());
            if (countDownTimer != null) {
                //将复用的倒计时清除
                countDownTimer.cancel();
            }
            long timer = 0;
            //LogUtils.e("当前日期时间戳:"+ System.currentTimeMillis());
//            timer = MyFormatUtils.dateAddOneDay(orderBeans.get(position).odtime) - System.currentTimeMillis();

            //expirationTime 与系统时间做比较，timer 小于零，则此时倒计时已经结束。
            if (timer > 0) {
                countDownTimer = new CountDownTimer(timer, 1000) {
                    public void onTick(long millisUntilFinished) {
                        viewHolder.tv_hours.setText("");
                        viewHolder.tv_minute.setText("");
                        viewHolder.tv_second.setText("");

//                        holder.bt_order_pay.setText("付款"+MyFormatUtils.getCountTimeByLong(millisUntilFinished));

                    }

                    public void onFinish() {
//                        holder.bt_order_pay.setText("付款取消");
                        viewHolder.tv_hours.setText("00");
                        viewHolder.tv_minute.setText("00");
                        viewHolder.tv_second.setText("00");

                    }
                }.start();
                //将此 countDownTimer 放入list.
                countDownCounters.put(viewHolder.tv_hours.hashCode(), countDownTimer);
            } else {
                //倒计时取消
            }


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        return convertView;
    }


    public class ViewHolder {
        TextView tv_price;
        TextView tv_full_reduction;
        TextView tv_hours;
        TextView tv_minute;
        TextView tv_second;
    }

    /**
     * 清空当前 CountTimeDown 资源
     */
    public void cancelAllTimers() {
        if (countDownCounters == null) {
            return;
        }
        for (int i = 0, length = countDownCounters.size(); i < length; i++) {
            CountDownTimer cdt = countDownCounters.get(countDownCounters.keyAt(i));
            if (cdt != null) {
                cdt.cancel();
            }
        }
    }


}
