package com.module.home.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.home.model.api.AddInsCodeApi;
import com.quicklyask.activity.R;
import com.quicklyask.entity.JFJY1;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 补填邀请码
 *
 * @author Rubin
 */
public class AddInsCodeActivity extends BaseActivity {

    private final String TAG = "AddInsCodeActivity";

    @BindView(id = R.id.butian_bt, click = true)
    private Button inviBt;
    @BindView(id = R.id.set_back, click = true)
    private RelativeLayout back;
    @BindView(id = R.id.butian_et)
    private EditText codeEt;

    private Context mContex;

    private String uid;
    private String flag;

    private boolean isCanNext = false;
    private static final int SHOW_TIME = 1000;
    private BaseCallBackApi codeApi;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_butian_code);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = AddInsCodeActivity.this;
        uid = Utils.getUid();
        Intent it = getIntent();
        flag = it.getStringExtra("flag");

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        AddInsCodeActivity.this.finish();
                    }
                });

        inviBt.setPressed(true);
        inviBt.setClickable(false);
        codeEt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                String textStr = codeEt.getText().toString();

                if (textStr.length() != 0 && !isCanNext) {
                    isCanNext = true;
                    inviBt.setPressed(false);
                    inviBt.setClickable(true);
                }
                if (textStr.length() == 0) {
                    isCanNext = false;
                    inviBt.setPressed(true);
                    inviBt.setClickable(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

            }

        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.set_back:
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                onBackPressed();
                break;
            case R.id.butian_bt:
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                sumbitCodeHttp();
                break;
        }
    }

    void sumbitCodeHttp() {
        codeApi = new AddInsCodeApi();

        uid = Utils.getUid();
        String code1 = codeEt.getText().toString();

        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", flag);
        maps.put("uid", uid);
        maps.put("code", code1);

        codeApi.getCallBack(mContex, maps, new BaseCallBackListener<JFJY1>() {
            @Override
            public void onSuccess(JFJY1 jfjy) {
                Log.d("lhl666666",jfjy.toString());
                JFJY1Data jfjyData = jfjy.getData();
                String jifenNu = jfjyData.getIntegral();
                String jyNu = jfjyData.getExperience();

                if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                    MyToast.makeTexttext4Toast(mContex, jifenNu, jyNu,
                            SHOW_TIME).show();
                } else {
                    if (!jifenNu.equals("0")) {
                        MyToast.makeTexttext2Toast(mContex, jifenNu,
                                SHOW_TIME).show();
                    } else {
                        if (!jyNu.equals("0")) {
                            MyToast.makeTexttext3Toast(mContex, jyNu,
                                    SHOW_TIME).show();

                        }
                    }
                }
                finish();
            }
        });

    }
//	void sumbitCodeHttp() {
//		uid = Cfg.loadStr(mContex, FinalConstant.UID, "");
//
//		String code1 = codeEt.getText().toString();
//		KJHttp kjh = new KJHttp();
//		kjh.get(FinalConstant.WRITE_CODE + flag + "/uid/" + uid + "/code/"
//				+ code1 + "/" + Utils.getTokenStr(), new StringCallBack() {
//
//			@Override
//			public void onSuccess(String json) {
//
//				// Log.e(TAG, json);
//				if (null != json && json.length() > 0) {
//					String code = JSONUtil
//							.resolveJson(json, FinalConstant.CODE);
//					String message = JSONUtil.resolveJson(json,
//							FinalConstant.MESSAGE);
//					if (code.equals("1")) {
//						// MyToast.makeImgAndTextToast(mContex,
//						// getResources().getDrawable(R.drawable.tips_smile),
//						// "领取成功", SHOW_TIME).show();
//						JFJY1 jfjy = new JFJY1();
//						jfjy = JSONUtil.TransformJFJY1Data(json);
//						JFJY1Data jfjyData = new JFJY1Data();
//						jfjyData = jfjy.getData();
//						String jifenNu = jfjyData.getIntegral();
//						String jyNu = jfjyData.getExperience();
//
//						// MyToast.makeTexttext3Toast(mContext, jyNu,
//						// SHOW_TIME);
//						if (!jifenNu.equals("0") && !jyNu.equals("0")) {
//							MyToast.makeTexttext4Toast(mContex, jifenNu, jyNu,
//									SHOW_TIME).show();
//						} else {
//							if (!jifenNu.equals("0")) {
//								MyToast.makeTexttext2Toast(mContex, jifenNu,
//										SHOW_TIME).show();
//							} else {
//								if (!jyNu.equals("0")) {
//									MyToast.makeTexttext3Toast(mContex, jyNu,
//											SHOW_TIME).show();
//
//								}
//							}
//						}
//						finish();
//					} else {
//						MyToast.makeTextToast(mContex, message, 1000).show();
//					}
//				}
//
//			}
//
//		});
//	}

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
