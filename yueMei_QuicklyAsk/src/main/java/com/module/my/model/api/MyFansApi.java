package com.module.my.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.MyFansData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.List;
import java.util.Map;

/**
 * 我的粉丝获取接口
 * Created by 裴成浩 on 2018/4/19.
 */

public class MyFansApi implements BaseCallBackApi {
    private String TAG = "MyFansApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "followingme", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if("1".equals(mData.code)){
                    Log.e(TAG, "mData === " + mData.data);
                    List<MyFansData> fansDatas = JSONUtil.jsonToArrayList(mData.data,MyFansData.class);
                    listener.onSuccess(fansDatas);
                }
            }
        });
    }
}