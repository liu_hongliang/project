package com.module.shopping.controller.adapter;

import android.util.Log;

import com.module.shopping.view.SlideLayout;

/**
 * Created by 裴成浩 on 2018/11/29
 */
public class ShoppingOnStateChangeListener implements SlideLayout.OnStateChangeListener {

    private String TAG = "ShoppingOnStateChangeListener";
    private SlideLayout slideLayout = null;

    @Override
    public void onOpen(SlideLayout layout) {
        Log.e(TAG, "onOpen...");
        slideLayout = layout;
    }

    @Override
    public void onMove(SlideLayout layout) {
        Log.e(TAG, "onMove...");
//        if (slideLayout != null) {
//            slideLayout.closeMenu();
//        }
        //如果点击的不是当前item
        if (slideLayout != null && slideLayout != layout) {
            slideLayout.closeMenu();
        }
    }

    @Override
    public void onClose(SlideLayout layout) {
        Log.e(TAG, "onClose...");
        if (slideLayout == layout) {
            slideLayout = null;
        }
    }

    public SlideLayout getSlideLayout() {
        return slideLayout;
    }
}
