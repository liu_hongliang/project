package com.module.my.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.util.JSONUtil;

import java.util.ArrayList;
import java.util.Map;

/**
 * 收藏的淘整形
 * Created by 裴成浩 on 2018/3/13.
 */

public class MyCollectTaoApi implements BaseCallBackApi {
    private String TAG = "MyCollectTaoApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "mycollecttao", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData === " + mData.toString());
                if("1".equals(mData.code)){
                    ArrayList<HomeTaoData> docData = JSONUtil.jsonToArrayList(mData.data,HomeTaoData.class);
                    listener.onSuccess(docData);
                }
            }
        });
    }
}
