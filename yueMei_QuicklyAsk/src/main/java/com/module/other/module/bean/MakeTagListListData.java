package com.module.other.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/2/20
 */
public class MakeTagListListData implements Parcelable {
    private String id;
    private String name;
    private String level;
    private HashMap<String, String> event_params;

    public MakeTagListListData(String id) {
        this.id = id;
    }

    public MakeTagListListData(String id, String name) {
        this.id = id;
        this.name = name;
    }

    protected MakeTagListListData(Parcel in) {
        id = in.readString();
        name = in.readString();
        level = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(level);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MakeTagListListData> CREATOR = new Creator<MakeTagListListData>() {
        @Override
        public MakeTagListListData createFromParcel(Parcel in) {
            return new MakeTagListListData(in);
        }

        @Override
        public MakeTagListListData[] newArray(int size) {
            return new MakeTagListListData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
