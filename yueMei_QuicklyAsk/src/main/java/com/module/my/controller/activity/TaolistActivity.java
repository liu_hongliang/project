package com.module.my.controller.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.commonview.view.ScreenTitleView;
import com.module.community.controller.adapter.TaoListAdapter;
import com.module.community.model.bean.TaoListDataType;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.home.model.bean.SearchResultTaoData;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 淘列表页
 */
public class TaolistActivity extends YMBaseActivity {

    private final String TAG = "TaolistActivity";

    @BindView(R.id.tao_list_back)
    RelativeLayout back;
    @BindView(R.id.search_tao_click)
    RelativeLayout sousuoClick;
    @BindView(R.id.tv_tao_list_title)
    EditText inputEdit1;
    @BindView(R.id.taolist_share_rly)
    RelativeLayout taolistShare;
    @BindView(R.id.instruction_screen)
    ScreenTitleView mScreen;
    @BindView(R.id.tv_instruction)
    TextView mInstruction;
    @BindView(R.id.tao_list_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.tao_list_recycle)
    RecyclerView mRecycle;
    @BindView(R.id.my_collect_post_tv_nodata1)
    LinearLayout mNotData;

    private BaseCityPopwindows cityPop;         //城市筛选
    private BaseSortPopupwindows sortPop;       //智能排序数据

    private BaseNetWorkCallBackApi workCallBackApi;
    private TaoListAdapter mProjectSkuAdapter;
    private int mPage = 1;
    private String mSort = "1";     //智能排序
    private String mCouponsId;
    private String mKey = "";


    @Override
    protected int getLayoutId() {
        return R.layout.activity_taolist;
    }

    @Override
    protected void initView() {

        mCouponsId = getIntent().getStringExtra("coupons_id");

        Log.e(TAG, "mCouponsId == " + mCouponsId);

        //初始化筛选
        mScreen.initView(false, false);
        mScreen.setCityTitle(Utils.getCity());
        mScreen.setOnEventClickListener(new ScreenTitleView.OnEventClickListener1() {
            @Override
            public void onCityClick() {
                if (cityPop != null) {
                    if (cityPop.isShowing()) {
                        cityPop.dismiss();
                    } else {
                        cityPop.showPop();
                    }
                    mScreen.initCityView(cityPop.isShowing());
                }
            }

            @Override
            public void onSortClick() {
                if (sortPop != null) {
                    if (sortPop.isShowing()) {
                        sortPop.dismiss();
                    } else {
                        sortPop.showPop();
                    }
                    mScreen.initSortView(sortPop.isShowing());
                }
            }
        });
    }

    @Override
    protected void initData() {
        workCallBackApi = new BaseNetWorkCallBackApi(FinalConstant1.HOME, "index613");
        cityPop = new BaseCityPopwindows(mContext, mScreen);        //城市筛选

        setPopData();           //智能排序
        listeningCallback();
        lodHotIssueData();
    }


    /**
     * 设置筛选的数据
     */
    private void setPopData() {
        ArrayList<TaoPopItemData> lvSortData = new ArrayList<>();//智能排序数据

        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("3");
        a2.setName("价格从低到高");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("4");
        a4.setName("销量最高");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("5");
        a5.setName("日记最多");
        TaoPopItemData a6 = new TaoPopItemData();
        a6.set_id("7");
        a6.setName("离我最近");

        lvSortData.add(a1);
        lvSortData.add(a2);
        lvSortData.add(a4);
        lvSortData.add(a5);
        lvSortData.add(a6);

        sortPop = new BaseSortPopupwindows(mContext, mScreen, lvSortData);

        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                if (sortPop != null) {
                    sortPop.dismiss();
                    mSort = sortId;
                    mScreen.initSortView(sortPop.isShowing());
                    mScreen.setSortTitle(sortName);
                }
                reshData();
            }
        });

        sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initSortView(false);
            }
        });
    }


    private void listeningCallback() {
        //返回的监听
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        //搜索框点击监听
        sousuoClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Utils.showSoftInputFromWindow(mContext, inputEdit1);
            }
        });

        //搜索点击
        taolistShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboardFromWindow(mContext, inputEdit1);
                mKey = inputEdit1.getText().toString();
                reshData();
            }
        });

        //加载更多和刷新
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                lodHotIssueData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                reshData();
            }
        });

        //城市回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                mScreen.setCityTitle(city);
                if (cityPop != null) {
                    cityPop.dismiss();
                    mScreen.initCityView(cityPop.isShowing());
                }

                reshData();
            }
        });

        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initCityView(false);
            }
        });
    }

    /**
     * 数据加载
     */
    private void lodHotIssueData() {
        workCallBackApi.getHashMap().clear();
        if (!TextUtils.isEmpty(mKey)) {
            try {
                workCallBackApi.addData("key", URLEncoder.encode(mKey, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                workCallBackApi.addData("key", mKey);
            }
        }
        workCallBackApi.addData("type", "4");
        workCallBackApi.addData("high", "1");
        workCallBackApi.addData("page", mPage + "");
        workCallBackApi.addData("sort", mSort);
        workCallBackApi.addData("coupon_id", mCouponsId);

        workCallBackApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    mPage++;
                    SearchResultTaoData lvHotIssueData = JSONUtil.TransformSingleBean(data.data, SearchResultTaoData.class);
                    List<HomeTaoData> mList = lvHotIssueData.getList();

                    mRefresh.finishRefresh();
                    if (mList.size() == 0) {
                        mRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mRefresh.finishLoadMore();
                    }

                    if (mProjectSkuAdapter == null) {
                        if (mList.size() != 0) {
                            mRefresh.setVisibility(View.VISIBLE);
                            mNotData.setVisibility(View.GONE);
                            setRecyclerData(mList);

                        } else {
                            mRefresh.setVisibility(View.GONE);
                            mNotData.setVisibility(View.VISIBLE);
                        }

                        if (!TextUtils.isEmpty(lvHotIssueData.getDesc())){
                            mInstruction.setVisibility(View.VISIBLE);
                            mInstruction.setText(lvHotIssueData.getDesc());
                        }else {
                            mInstruction.setVisibility(View.GONE);
                        }

                    } else {
                        mProjectSkuAdapter.addData(mList);
                    }
                }
            }
        });
    }


    /**
     * 设置列表数据
     */
    private void setRecyclerData(List<HomeTaoData> list) {
        mRecycle.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(1), Utils.getLocalColor(mContext, R.color.subscribe_item_drag_bg)));
        mRecycle.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mProjectSkuAdapter = new TaoListAdapter(mContext, list);
        mRecycle.setAdapter(mProjectSkuAdapter);

        mProjectSkuAdapter.setOnItemClickListener(new TaoListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TaoListDataType taoData, int pos) {
                HomeTaoData  data = taoData.getTao();
                if (TextUtils.isEmpty(data.getTuijianTitle())) {
                    Intent it1 = new Intent(mContext, TaoDetailActivity.class);
                    it1.putExtra("id", data.get_id());
                    it1.putExtra("source", "2");
                    it1.putExtra("objid", "0");
                    startActivity(it1);
                }
            }
        });
    }

    /**
     * 刷新数据
     */
    private void reshData() {
        mProjectSkuAdapter = null;
        mPage = 1;
        lodHotIssueData();
    }
}
