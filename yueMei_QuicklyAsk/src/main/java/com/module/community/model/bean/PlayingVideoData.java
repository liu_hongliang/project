package com.module.community.model.bean;

import com.module.community.controller.adapter.VideoListAdapter;

/**
 * 正在播放中的视频
 * Created by 裴成浩 on 2019/5/10
 */
public class PlayingVideoData {
    private VideoListData data;                         //当前视频数据
    private boolean isShowTao = true;                   //是否显示淘详情View，默认显示
    private boolean isUploadLookVideo = false;          //是否上传了看过视频，默认内有上传
    private VideoListAdapter.ViewHolder viewHolder;     //当前的ViewHolder


    public VideoListData getData() {
        return data;
    }

    public void setData(VideoListData data) {
        this.data = data;
    }

    public boolean isShowTao() {
        return isShowTao;
    }

    public void setShowTao(boolean showTao) {
        isShowTao = showTao;
    }

    public boolean isUploadLookVideo() {
        return isUploadLookVideo;
    }

    public void setUploadLookVideo(boolean uploadLookVideo) {
        isUploadLookVideo = uploadLookVideo;
    }

    public VideoListAdapter.ViewHolder getViewHolder() {
        return viewHolder;
    }

    public void setViewHolder(VideoListAdapter.ViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
}
