package com.module.commonview.broadcast;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.MyApplication;
import com.module.commonview.activity.ChatActivity;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;

import java.lang.reflect.Field;
import java.util.HashMap;


public class TopWindowUtils {

    private static PopupWindow popupWindow;

    public static void show(final Activity activity,String title, String content, String hosimg, final String clientid, final String hosid, final String pos) {
        if (popupWindow == null) {
            popupWindow = new PopupWindow();
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout linearLayout = new LinearLayout(activity);
            View viewContent = inflater.inflate(R.layout.notifycation_message, linearLayout);
            popupWindow.setContentView(viewContent);
            popupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            popupWindow.setAnimationStyle(R.style.PopupTopAnim);
        }
        popupWindow.showAtLocation(activity.getWindow().getDecorView(), Gravity.TOP, 0, 0);

        LinearLayout contaier = popupWindow.getContentView().findViewById(R.id.toast_container);
        TextView tvTitle = popupWindow.getContentView().findViewById(R.id.notify_title);
        TextView tvContent = popupWindow.getContentView().findViewById(R.id.notify_content);
        ImageView tvImage = popupWindow.getContentView().findViewById(R.id.notify_img);
        tvTitle.setText(title);
        tvContent.setText(content);
        Glide.with(activity)
                .load(hosimg)
                .transform(new GlideCircleTransform(activity))
                .placeholder(R.color._f6)
                .error(R.color._f6).
                into(tvImage);
        popupWindow.getContentView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("hos_id", hosid);
                hashMap.put("event_others", pos);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.PUSH, "click"), hashMap);
                popupWindow.dismiss();
                Intent intent = new Intent(activity, ChatActivity.class);
                intent.putExtra("directId", clientid);
                intent.putExtra("objId", "0");
                intent.putExtra("objType", "0");
                activity.startActivity(intent);
            }
        });
//        popupWindow.getContentView().setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                 float downY = 0;
//                 float downX = 0;
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        downY = event.getRawY();
//                        downX = event.getRawX();
//
//                        break;
//                    case MotionEvent.ACTION_MOVE:
//                        float currentY = event.getRawY();
//                        if ((downY - currentY) >= 10) {
//                            ObjectAnimator a = ObjectAnimator.ofFloat(popupWindow.getContentView(), "translationY", 0, -700);
//                            a.setDuration(600);
//                            a.start();
//
//                            a.addListener(new Animator.AnimatorListener() {
//
//                                @Override
//                                public void onAnimationStart(Animator animation) {
//
//                                }
//
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    popupWindow.dismiss();
//                                }
//
//                                @Override
//                                public void onAnimationCancel(Animator animation) {
//
//                                }
//
//                                @Override
//                                public void onAnimationRepeat(Animator animation) {
//
//                                }
//                            });
//                        }
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        //检测移动的距离，如果很微小可以认为是点击事件
//                        if (Math.abs(event.getRawX() - downX) < 10 && Math.abs(event.getRawY() - downY) < 10) {
//                            try {
//                                Field field = View.class.getDeclaredField("mListenerInfo");
//                                field.setAccessible(true);
//                                Object object = field.get(popupWindow.getContentView());
//                                field = object.getClass().getDeclaredField("mOnClickListener");
//                                field.setAccessible(true);
//                                object = field.get(object);
//                                if (object != null && object instanceof View.OnClickListener) {
//                                    ((View.OnClickListener) object).onClick(popupWindow.getContentView());
//                                }
//                            } catch (Exception e) {
//
//                            }
//                            return false;
//                        } else {
//                        }
//                        break;
//
//                }
//                return true;
//            }
//        });


        handler.removeMessages(1);
        handler.sendEmptyMessageDelayed(1, 5000);
    }

    @SuppressLint("HandlerLeak")
    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            popupWindow.dismiss();
        }
    };





}
