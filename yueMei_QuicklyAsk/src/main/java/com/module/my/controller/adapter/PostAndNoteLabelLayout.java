package com.module.my.controller.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.other.module.bean.MakeTagListListData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2018/8/10.
 */
public class PostAndNoteLabelLayout {

    private Activity mContext;
    private FlowLayout mLabel;
    private ArrayList<MakeTagListListData> mDatas;
    private ViewGroup.MarginLayoutParams lp;
    public static final String TEXT1 = "添加标签";
    private String TAG = "PostAndNoteLabelLayout";

    public PostAndNoteLabelLayout(Activity context, FlowLayout label) {
        this.mContext = context;
        this.mLabel = label;
        this.mDatas = new ArrayList<>();
        this.mDatas.add(new MakeTagListListData("0", PostAndNoteLabelLayout.TEXT1));
        initView();
    }

    /**
     * 初始化标签
     */
    private void initView() {
        mLabel.removeAllViews();
        lp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(Utils.dip2px(10), Utils.dip2px(10), Utils.dip2px(10), Utils.dip2px(10));
        for (int i = 0; i < mDatas.size(); i++) {
            MakeTagListListData data = mDatas.get(i);

            View view = View.inflate(mContext, R.layout.item_make_top, null);
            LinearLayout mMakeTop = view.findViewById(R.id.item_make_top);
            TextView mTitle = view.findViewById(R.id.tv_xiang_mu);
            RelativeLayout mDelete = view.findViewById(R.id.rl_top_delete);
            ImageView mDeleteImage = view.findViewById(R.id.rl_top_delete_image);

            mMakeTop.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_bian_yuanjiao_f6f6f6));
            mTitle.setText(data.getName());

            if (isButton(data.getId())) {
                mTitle.setTextColor(Utils.getLocalColor(mContext, R.color._99));
                mDeleteImage.setImageResource(R.drawable.add_label_img);
            } else {
                mTitle.setTextColor(Utils.getLocalColor(mContext, R.color._33));
                mDeleteImage.setImageResource(R.drawable.delete_label_img);
            }

            mLabel.addView(view, i, lp);

            view.setTag(i);
            view.setOnClickListener(new onClickView());

            mDelete.setTag(i);
            mDelete.setOnClickListener(new onDeleteClickView());
        }
    }

    /**
     * 点击按钮回调
     */
    class onClickView implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Log.e(TAG, "mDatas.size() == " + mDatas.size());
            if (mDatas.size() == 0){
                mDatas.add(new MakeTagListListData("0", PostAndNoteLabelLayout.TEXT1));
            }
            int selected = (int) v.getTag();
            Log.e(TAG, "selected == " + selected);
            for (int i = 0; i < mDatas.size(); i++) {
                if (i == selected) {
                    if (isButton(mDatas.get(i).getId()) && clickCallBack != null) {
                        clickCallBack.onClick(v, i);
                    }
                }
            }
        }
    }

    /**
     * 删除按钮点击
     */
    class onDeleteClickView implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int selected = (int) v.getTag();
            for (int i = 0; i < mDatas.size(); i++) {
                if (i == selected) {
                    if (!isButton(mDatas.get(i).getId()) && clickCallBack != null) {
                        clickCallBack.onDeleteClick(v, i);
                    }
                }
            }
        }
    }

    /**
     * 重新赋值
     *
     * @param datas
     */
    public void addDatas(ArrayList<MakeTagListListData> datas) {
        mDatas = datas;
        //如果不到两个标签
        if (mDatas.size() < 3) {
            mDatas.add(new MakeTagListListData("0", PostAndNoteLabelLayout.TEXT1));
        }
        initView();
    }

    /**
     * 删除一条数据
     *
     * @param pos
     */
    public void deleteData(int pos) {
        mDatas.remove(pos);

        //如果添加按钮不存在
        if (!haveButton()) {
            mDatas.add(mDatas.size(), new MakeTagListListData("0", TEXT1));
        }

        initView();
    }


    /**
     * 判断添加按钮是否存在
     */
    private boolean haveButton() {
        for (MakeTagListListData data : mDatas) {
            if (isButton(data.getId())) {
                return true;
            }
        }
        return false;
    }


    /**
     * 获取添加按钮下标
     *
     * @return
     */
    private int buttonPos() {
        for (int i = 0; i < mDatas.size(); i++) {
            MakeTagListListData data = mDatas.get(i);
            if (isButton(data.getId())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 判断是否是选择标签
     *
     * @param tag_id :标签id
     * @return
     */
    private boolean isButton(String tag_id) {
        return "0".equals(tag_id);
    }

    /**
     * 剔除添加标签按钮
     *
     * @return
     */
    public ArrayList<MakeTagListListData> getmDatas() {
        for (MakeTagListListData data : mDatas) {
            if (isButton(data.getId())) {
                mDatas.remove(data);
            }
        }
        return mDatas;
    }

    private ClickCallBack clickCallBack;

    public interface ClickCallBack {
        void onClick(View v, int pos);

        void onDeleteClick(View v, int pos);
    }

    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }
}
