/**
 * 
 */
package com.quicklyask.entity;

/**
 * @author lenovo17
 * 
 */
public class Advert {

	private String code;
	private String message;
	private AdertAdv data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public AdertAdv getData() {
		return data;
	}

	public void setData(AdertAdv data) {
		this.data = data;
	}
}
