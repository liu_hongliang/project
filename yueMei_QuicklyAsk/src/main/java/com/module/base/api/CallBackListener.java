package com.module.base.api;

public interface CallBackListener <T>{

    void onSuccess(T t);

    void onError(T t);
}
