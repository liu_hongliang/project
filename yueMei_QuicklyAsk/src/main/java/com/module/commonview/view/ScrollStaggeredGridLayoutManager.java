package com.module.commonview.view;

import android.support.v7.widget.StaggeredGridLayoutManager;

/**
 * Created by 裴成浩 on 2019/6/29
 */
public class ScrollStaggeredGridLayoutManager extends StaggeredGridLayoutManager {
    private boolean isScrollEnable = true;

    public ScrollStaggeredGridLayoutManager(int spanCount, int orientation) {
        super(spanCount, orientation);
    }

    @Override
    public boolean canScrollVertically() {
        return isScrollEnable && super.canScrollVertically();
    }

    /**
     * 设置是否可以垂直滑动
     *
     * @param isEnable
     */
    public void setScrollEnable(boolean isEnable) {
        this.isScrollEnable = isEnable;
    }
}
