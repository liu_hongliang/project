package com.module.shopping.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.bean.PayOrderData;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.view.MyToast;

import java.util.HashMap;
import java.util.Map;

/**
 * 支付订单的联网请求
 * Created by 裴成浩 on 2018/3/8.
 */

public class PayOrderApi implements BaseCallBackApi {
    private String TAG = "PayOrderApi";

    private HashMap<String, Object> mHashMap;  //传值容器

    public PayOrderApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(final Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.ORDER, "saveorder", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData == " + mData.toString());
                if ("1".equals(mData.code)) {
                    try {
                        PayOrderData payOrderData = JSONUtil.TransformSingleBean(mData.data, PayOrderData.class);
                        listener.onSuccess(payOrderData);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "e == " + e.toString());
                    }
                } else {
                    Log.e(TAG, "mData.message == " + mData.message);
                    MyToast.makeTextToast2(context, mData.message, MyToast.SHOW_TIME).show();
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
