package com.module.shopping.model.bean;

/**
 * Created by 裴成浩 on 2018/12/19
 */
public class PayOrderTaoSku {
    private String tao_id;
    private String extension_user;
    private String number;
    private String is_insure;
    private String postStr;

    public String getTao_id() {
        return tao_id;
    }

    public void setTao_id(String tao_id) {
        this.tao_id = tao_id;
    }

    public String getExtension_user() {
        return extension_user;
    }

    public void setExtension_user(String extension_user) {
        this.extension_user = extension_user;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIs_insure() {
        return is_insure;
    }

    public void setIs_insure(String is_insure) {
        this.is_insure = is_insure;
    }

    public String getPostStr() {
        return postStr;
    }

    public void setPostStr(String postStr) {
        this.postStr = postStr;
    }
}
