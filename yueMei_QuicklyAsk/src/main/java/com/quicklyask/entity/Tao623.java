package com.quicklyask.entity;

/**
 * Created by 裴成浩 on 17/8/3.
 */
public class Tao623 {

    private String code;
    private String message;
    private TaoData623 data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TaoData623 getData() {
        return data;
    }

    public void setData(TaoData623 data) {
        this.data = data;
    }
}
