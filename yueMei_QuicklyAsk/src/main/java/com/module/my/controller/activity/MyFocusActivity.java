package com.module.my.controller.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.module.my.controller.adapter.MyFocusPagerAdapter;
import com.module.my.view.fragment.MyFocusFragment;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的关注
 */
public class MyFocusActivity extends FragmentActivity {

    private TabLayout mTabSegment;
    private ViewPager mViewPager;

    private String TAG = "MyFocusActivity";
    private MyFocusActivity mContext;
    private MyFocusPagerAdapter mPagerAdapter;
    private List<Fragment> mFragmentList = new ArrayList<>();
    private List<String> mPageTitleList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_focus);

        mContext = MyFocusActivity.this;
        initView();
    }

    private void initView() {
        mTabSegment = findViewById(R.id.my_focus_tab_segment);
        mViewPager = findViewById(R.id.my_focus_view_pager);

        mPageTitleList.add("用户");
        mPageTitleList.add("医生");
        mPageTitleList.add("医院");
        mPageTitleList.add("圈子");

        mFragmentList.add(MyFocusFragment.newInstance("6"));
        mFragmentList.add(MyFocusFragment.newInstance("1"));
        mFragmentList.add(MyFocusFragment.newInstance("3"));
        mFragmentList.add(MyFocusFragment.newInstance("0"));

        mPagerAdapter = new MyFocusPagerAdapter(mContext, getSupportFragmentManager(), mFragmentList, mPageTitleList);
        mViewPager.setAdapter(mPagerAdapter);
        mTabSegment.setupWithViewPager(mViewPager);

        mTabSegment.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


}
