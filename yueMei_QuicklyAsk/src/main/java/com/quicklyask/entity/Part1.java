package com.quicklyask.entity;

import com.module.home.model.bean.Part1Data;

import java.util.List;

public class Part1 {
	private String code;
	private String message;
	private List<Part1Data> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Part1Data> getData() {
		return data;
	}

	public void setData(List<Part1Data> data) {
		this.data = data;
	}

}
