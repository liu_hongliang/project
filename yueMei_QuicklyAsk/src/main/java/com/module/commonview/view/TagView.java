package com.module.commonview.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class TagView extends LinearLayout {
    private Context mContext;
    private ImageView tagImageView;
    private TextView tagTextView;

    public TagView(Context context) {
        this(context,null);
    }

    public TagView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public TagView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext=context;
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.tag_view, this, true);
        tagImageView=view.findViewById(R.id.tag_iv);
        tagTextView=view.findViewById(R.id.tag_tv);
    }

    public void setContent(String contnet){
        tagTextView.setText(contnet);
    }
}
