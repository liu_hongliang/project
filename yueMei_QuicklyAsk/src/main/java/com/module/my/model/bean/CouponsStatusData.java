package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2019/8/22
 */
public class CouponsStatusData {
    private String showStatus;
    private String alertWebUrl;

    public String getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(String showStatus) {
        this.showStatus = showStatus;
    }

    public String getAlertWebUrl() {
        return alertWebUrl;
    }

    public void setAlertWebUrl(String alertWebUrl) {
        this.alertWebUrl = alertWebUrl;
    }
}
