package com.module.home.model.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.Map;

/**
 * Created by 裴成浩 on 2017/12/19.
 */

public class XiaoxiChatListAPI implements BaseCallBackApi {

    private String TAG = "XiaoxiChatListAPI";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.CHAT, "chatlist", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                listener.onSuccess(mData);

            }
        });
    }
}
