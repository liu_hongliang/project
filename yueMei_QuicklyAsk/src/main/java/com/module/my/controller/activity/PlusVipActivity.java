package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.cookie.store.CookieStore;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.module.base.view.YMBaseWebViewActivity;
import com.module.commonview.view.CommentDialogView;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Cookie;
import okhttp3.HttpUrl;
import okhttp3.Response;

/**
 * 会员详情页面
 * Created by 裴成浩 on 2018/9/11.
 */
public class PlusVipActivity extends YMBaseWebViewActivity {

    @BindView(R.id.plus_vip_top)
    CommonTopBar mTop;                              //头标题
    @BindView(R.id.plus_vip_container)
    FrameLayout webViewContainer;                   //webView容器
    private String TAG = "PlusVipActivity";
    private String mUrl;
    private BaseWebViewClientMessage clientManager;

    private String domain;
    private long expiresAt;
    private String name;
    private String path;
    private String value;
    private String replace;
    private CommentDialogView mCommentDialogView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_plus_vip;
    }

    @SuppressLint("AddJavascriptInterface")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void initView() {
        super.initView();

        mUrl = FinalConstant.PLUS_VIP_URL;
        webViewContainer.addView(mWebView);
        Log.e(TAG, "mWebView === " + mWebView);

        loadUrl(mUrl);
    }

    @Override
    protected void initData() {
        mCommentDialogView = new CommentDialogView(mContext);
        /**
         * 公共跳转外的跳转
         */
        clientManager = new BaseWebViewClientMessage(mContext);
        clientManager.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String url) {
                try {
                    showWebDetail(url);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "e === " + e.toString());
                }
            }
        });
    }

    @Override
    protected boolean ymShouldOverrideUrlLoading(WebView view, String url) {
        WebView.HitTestResult hitTestResult = view.getHitTestResult();
        int hitType = hitTestResult.getType();
        Log.e(TAG, "hitTestResult == " + hitTestResult);
        Log.e(TAG, "hitType == " + hitType);
        Log.e(TAG, "url == " + url);

        if (hitType != WebView.HitTestResult.UNKNOWN_TYPE) {
            if (url.startsWith("type")) {
                clientManager.showWebDetail(url);
            } else {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
            }
            return true;
        }
        return super.ymShouldOverrideUrlLoading(view, url);
    }

    @Override
    protected void onYmReceivedTitle(WebView view, String title) {
        super.onYmReceivedTitle(view, title);
        if (!TextUtils.isEmpty(title)) {
            mTop.setCenterText(title);
        } else {
            mTop.setCenterText("会员首页");
        }
    }

    private void showWebDetail(String urlStr) throws Exception {
        Log.e(TAG, "urlStr == " + urlStr);
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "6341":            //	问题解答
                String question_id6341 = obj.getString("question_id");
                setCommentsCallback(question_id6341);
                break;
            case "6343":            // 当前页面跳转
                String link = obj.getString("link");
                String flag6343 = obj.getString("flag");
                String url6343 = FinalConstant.baseUrl + FinalConstant.VER + link;
                Map<String, Object> keyValues6343 = new HashMap<>();
                keyValues6343.put("flag", flag6343);
                loadUrl(url6343, keyValues6343);

                break;
            case "6481":            //实名认证回调
//                showShort("实名认证回调");
                break;
            case "6482":            //	钱包明细当前页面跳转
                String link6482 = obj.getString("link");
                String url6482 = FinalConstant.baseUrl + FinalConstant.VER + link6482;
                String[] split6482 = link6482.split("/");
                HashMap<String, Object> mSingStr = new HashMap<>();
                mSingStr.put("flag", split6482[4]);
                loadUrl(url6482, mSingStr);
                break;
        }
    }

    /**
     * 评论回调
     */
    public void setCommentsCallback(final String questionId) {
        if (Utils.isLoginAndBind(mContext)) {
            mCommentDialogView.showDialog();

            //评论提交完成回调
            mCommentDialogView.setOnSubmitClickListener(new CommentDialogView.OnSubmitClickListener() {
                @Override
                public void onSubmitClick(String content) {
                    postFileQue(content, questionId);
                }
            });
        }
    }


    /**
     * 上传解答
     *
     * @param content
     */
    private void postFileQue(String content, String questionId) {
        mDialog.startLoading();

        Map<String, Object> maps = new HashMap<>();
        maps.put("id", questionId);
        maps.put("content", content);


        HttpParams httpParams = SignUtils.buildHttpParam5(maps);

        HttpHeaders headers = SignUtils.buildHttpHeaders(maps);


        CookieStore cookieStore = OkGo.getInstance().getCookieJar().getCookieStore();

        HttpUrl httpUrl = new HttpUrl.Builder().scheme("http").host("sjapp.yuemei.com").build();

        List<Cookie> cookies = cookieStore.loadCookie(httpUrl);
        Log.e(TAG, "cookies == " + cookies);


        for (Cookie cookie : cookies) {
            domain = cookie.domain();
            expiresAt = cookie.expiresAt();
            name = cookie.name();
            path = cookie.path();
            value = cookie.value();

            Log.e(TAG, "domain == " + domain);
            Log.e(TAG, "expiresAt == " + expiresAt);
            Log.e(TAG, "name == " + name);
            Log.e(TAG, "path == " + path);
            Log.e(TAG, "value == " + value);
        }

        Log.e(TAG, "replace == " + replace);
        cookieStore.removeCookie(httpUrl);

        Cookie yuemeiinfo = new Cookie.Builder().name("yuemeiinfo").value(replace).domain(domain).expiresAt(expiresAt).path(path).build();


        cookieStore.saveCookie(httpUrl, yuemeiinfo);

        List<Cookie> cookies1 = cookieStore.loadCookie(httpUrl);
        android.util.Log.e(TAG, "cookies1 == " + cookies1);

        OkGo.post(FinalConstant.ANSWER_MESSAGE).cacheMode(CacheMode.DEFAULT).params(httpParams).headers(headers).execute(new StringCallback() {
            @Override
            public void onSuccess(String result, Call call, Response response) {
                android.util.Log.e(TAG, "result === " + result);
                mDialog.stopLoading();
                String code = JSONUtil.resolveJson(result, FinalConstant.CODE);
                String message = JSONUtil.resolveJson(result, FinalConstant.MESSAGE);
                if ("1".equals(code)) {
                    ViewInject.toast(message);
                    loadUrl(mUrl);
                } else {
                    ViewInject.toast(message);
                }

            }

            @Override
            public void onError(Call call, Response response, Exception e) {
                super.onError(call, response, e);
                Log.e(TAG, "call === " + call);
                Log.e(TAG, "response === " + response);
                Log.e(TAG, "e === " + e);
            }
        });
    }
}
