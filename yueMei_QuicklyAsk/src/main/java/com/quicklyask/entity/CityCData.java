package com.quicklyask.entity;


import com.module.doctor.model.bean.MainCityData;

import org.kymjs.aframe.database.annotate.Id;

import java.util.List;

/**
 * Created by dwb on 16/11/25.
 */
public class CityCData {

    @Id()
    private int id;
    private String letter;
    private List<MainCityData> data;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public List<MainCityData> getData() {
        return data;
    }

    public void setData(List<MainCityData> data) {
        this.data = data;
    }
}
