package com.module.my.model.bean;

public class GetCode {
	private String code;
	private String message;
	private GetCodeData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public GetCodeData getData() {
		return data;
	}

	public void setData(GetCodeData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetCode [code=" + code + ", message=" + message + ", data="
				+ data + "]";
	}

}
