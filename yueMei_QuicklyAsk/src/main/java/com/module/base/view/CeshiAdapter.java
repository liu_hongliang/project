package com.module.base.view;


import android.support.annotation.Nullable;

import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/26
 */
public class CeshiAdapter extends YMRecyclerViewAdapter<String, YMViewHolder> {

    public CeshiAdapter(@Nullable List<String> data) {
        super(data);
    }

    @Override
    protected int findResById() {
        return R.layout.activity_ceshi_view;
    }

    @Override
    protected void convert(YMViewHolder helper, String item) {
        int position = helper.getAdapterPosition();
        helper.setText(R.id.item_ceshi_text, item);
    }
}
