package com.module.my.controller.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.my.model.api.PostTextQueApi;
import com.module.my.model.bean.ForumShareData;
import com.module.my.view.fragment.PostingAndNoteFragment;
import com.module.other.module.api.LoadPartCityListApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.WriteResultData;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;

import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * 重构的报名发帖
 *
 * @author 裴成浩
 */
public class EnrolPageActivity647 extends YMBaseActivity {

    @BindView(R.id.post_apply_top)
    CommonTopBar mTop;                          //标题

    //模版
    @BindView(R.id.ll_enrol_moban)
    LinearLayout mMoban;
    @BindView(R.id.enrol_name)
    EditText mMobanNameEt;                      //姓名

    @BindView(R.id.enrol_gender_select)
    RadioGroup mGgenderSelect;                  //性别
    @BindView(R.id.enrol_gender_girl)
    RadioButton mGgenderSelectGirl;             //性别1
    @BindView(R.id.enrol_gender_boy)
    RadioButton mGgenderSelectBoy;              //性别2

    @BindView(R.id.enrol_age)
    EditText mEnrolAge;                         //年龄
    @BindView(R.id.enrol_phone)
    EditText mEnrolPhone;                       //电话
    @BindView(R.id.enrol_weixin)
    EditText mEnrolWeixin;                      //微信号
    @BindView(R.id.enrol_projects)
    EditText mEnrolProjects;                    //申请项目

    public static final int ENROL_CODE = 10;

    private String TAG = "EnrolPageActivity647";
    private PostingAndNoteFragment postingAndNoteFragment;
    private PostTextQueApi postTextQueApi;
    private LoadPartCityListApi loadPartCityListApi;

    private String cateid;
    private String sex;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_enrol_page647;
    }

    @Override
    protected void initView() {

        cateid = getIntent().getStringExtra("cateid");

        //设置发布默认是不可点击的
        mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
        mTop.getTv_right().setEnabled(false);

        postingAndNoteFragment = PostingAndNoteFragment.newInstance(4);
        setActivityFragment(R.id.write_suibian_post_view, postingAndNoteFragment);

        postingAndNoteFragment.setOnClickListener(new PostingAndNoteFragment.OnClickListener() {
            @Override
            public void onButtonStateListener(boolean isClick) {
                if (isClick) {
                    mTop.getTv_right().setEnabled(true);
                    mTop.setRightTextColor((Utils.getLocalColor(mContext, R.color.title_red_new)));
                } else {
                    mTop.getTv_right().setEnabled(false);
                    mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
                }
            }
        });

        //性别选择
        mGgenderSelect.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.enrol_gender_girl:
                        sex = "2";
                        break;
                    case R.id.enrol_gender_boy:
                        sex = "1";
                        break;
                }
            }
        });

        //关闭按钮点击
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                showDialogExitEdit();
            }
        });

        //发布按钮点击
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (postingAndNoteFragment.uploadingPrompt()) {

                    ForumShareData forumShareData = postingAndNoteFragment.setReleaseData();
                    if (!TextUtils.isEmpty(forumShareData.getCover_photo())) {
                        if (!TextUtils.isEmpty(sex)) {
                            uploadData(forumShareData);
                        } else {
                            mFunctionManager.showShort("请选择性别");
                        }
                    } else {
                        mFunctionManager.showShort("请至少添加一张图片或一个视频");
                    }
                }
            }
        });

    }

    @Override
    protected void initData() {
        postTextQueApi = new PostTextQueApi();
        loadPartCityListApi = new LoadPartCityListApi();

    }

    /**
     * 上传数据
     */
    private void uploadData(ForumShareData forumShareData) {
        mDialog.startLoading();
        Log.e(TAG, "forumShareData.getTitle() === " + forumShareData.getTitle());
        Log.e(TAG, "forumShareData.getCateid() === " + forumShareData.getCateid());
        Log.e(TAG, "forumShareData.getContent() === " + forumShareData.getContent());
        Log.e(TAG, "forumShareData.getVisibility() === " + forumShareData.getVisibility());
        Log.e(TAG, "forumShareData.getAskorshare() === " + forumShareData.getAskorshare());
        Log.e(TAG, "forumShareData.getImage() === " + forumShareData.getImage());
        Log.e(TAG, "forumShareData.getVideo() === " + forumShareData.getVideo());
        Log.e(TAG, "forumShareData.getCover_photo() === " + forumShareData.getCover_photo());

        postTextQueApi.addData("title", forumShareData.getTitle());                                 //日记本标题
        postTextQueApi.addData("content", forumShareData.getContent());                             //内容
        postTextQueApi.addData("visibility", forumShareData.getVisibility());                       //图片/视频仅医生可见  默认是   （1）
        postTextQueApi.addData("askorshare", forumShareData.getAskorshare());                       //标识

        //判断标签串是否是空的
        if (!TextUtils.isEmpty(forumShareData.getCateid())) {
            postTextQueApi.addData("cateid", forumShareData.getCateid());                               //标签串
        }

        //判断是上传图片还是上传视频
        if (!TextUtils.isEmpty(forumShareData.getVideo())) {
            postTextQueApi.addData("video", forumShareData.getVideo());                                 //视频json
            postTextQueApi.addData("cover_photo", forumShareData.getCover_photo());                     //封面图
        } else if (!TextUtils.isEmpty(forumShareData.getImage())) {
            postTextQueApi.addData("image", forumShareData.getImage());                                 //图片json
            postTextQueApi.addData("cover_photo", forumShareData.getCover_photo());                     //封面图
        }

        postTextQueApi.getCallBack(mContext, postTextQueApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (!TextUtils.isEmpty(serverData.data)) {
                    if ("1".equals(serverData.code)) {
                        WriteResultData data = JSONUtil.TransformWriteResult(serverData.data);
                        mobanSubmit(data.get_id());
                    } else {
                        mDialog.stopLoading();
                        mFunctionManager.showShort(serverData.message);
                    }
                } else {
                    mDialog.stopLoading();
                    mFunctionManager.showShort("提交失败,请检查网络连接是否正常");
                }
            }
        });
    }

    /**
     * 模版数据提交
     */
    public void mobanSubmit(String post_id) {
        String username = mMobanNameEt.getText().toString();
        String phone = mEnrolPhone.getText().toString();
        String weixin = mEnrolWeixin.getText().toString();
        String age = mEnrolAge.getText().toString();
        String projects = mEnrolProjects.getText().toString();

        Log.e(TAG, "id === " + cateid);
        Log.e(TAG, "username === " + username);
        Log.e(TAG, "phone === " + phone);
        Log.e(TAG, "weixin === " + weixin);
        Log.e(TAG, "age === " + age);
        Log.e(TAG, "projects === " + projects);
        Log.e(TAG, "sex === " + sex);

        if ("".equals(username) || "".equals(phone)) {
            ViewInject.toast("姓名或手机号不能为空");
            mDialog.stopLoading();
            return;
        }

        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("id", cateid);
        keyValues.put("username", username);    //姓名
        keyValues.put("phone", phone);          //电话
        if (!TextUtils.isEmpty(sex)) {
            keyValues.put("sex", sex);              //性别
        } else {
            //默认女
            keyValues.put("sex", "2");              //性别
        }
        keyValues.put("weixin", weixin);        //微信号
        keyValues.put("item", projects);        //项目
        keyValues.put("age", age);              //年龄
        keyValues.put("post_id", post_id);           //报名同时发帖的Id

        loadPartCityListApi.getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "result == " + serverData.data);
                Log.e(TAG, "code == " + serverData.code);
                mDialog.stopLoading();
                if ("1".equals(serverData.code)) {
                    Intent intent = new Intent();
                    intent.setClass(mContext, DiariesAndPostsActivity.class);
                    intent.putExtra("textMessage", serverData.message);
                    setResult(ENROL_CODE, intent);
                    finish();
                } else {
                    mFunctionManager.showShort(serverData.message);
                }
            }
        });
    }


    /**
     * dialog提示，是否退出此次编辑
     */
    private void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_dianhuazixun);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText("确定退出此次编辑吗？");

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editDialog.dismiss();
                onBackPressed();
            }
        });
    }

}
