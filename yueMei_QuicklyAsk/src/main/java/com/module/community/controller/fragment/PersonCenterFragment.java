package com.module.community.controller.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.loadmore.LoadMoreListView;
import com.module.base.refresh.loadmore.LoadMoreListener;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.controller.api.MyPersonCenterApi;
import com.module.community.model.bean.BBsListData550;
import com.module.my.model.api.MyDiaryApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户信息Fragment 日记
 * Created by 裴成浩 on 2018/5/02.
 */
public class PersonCenterFragment extends Fragment {

    private Activity mActivity;
    private String myId;
    private int mPos = 0;
    private LoadMoreListView mLlistView;
    private Map<String, Object> myPersonCenterMap = new HashMap<>();
    private int mPage = 1;
    private String TAG = "PersonCenterFragment";
    private MyDiaryApi myDiaryApi;
    private MyPersonCenterApi myPersonCenterApi;
    private boolean isNoMore = false;
    private BBsListAdapter bbsListAdapter;
    private List<BBsListData550> mDocListDatas;
    private TextView personNotText;
    private View headerView;
    private LinearLayout personFragmentView;

    public static PersonCenterFragment newInstance(String myId, int pos) {
        PersonCenterFragment fragment = new PersonCenterFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", myId);
        bundle.putInt("pos", pos);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDiaryApi = new MyDiaryApi();
        myPersonCenterApi = new MyPersonCenterApi();
        mActivity = getActivity();
        myId = getArguments().getString("id");
        mPos = getArguments().getInt("pos");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.person_fragment_list_view, container, false);
        personFragmentView = view.findViewById(R.id.person_fragment_view);
        mLlistView = view.findViewById(R.id.person_frag_list);


        headerView = LayoutInflater.from(mActivity).inflate(R.layout.person_fragment_list_header, null);
        personNotText = headerView.findViewById(R.id.person_not_text);

        initView();
        switch (mPos) {
            case 0:
                personNotText.setText("暂无日记");
                loadData0();
                break;
            case 1:
                personNotText.setText("暂无帖子");
                loadData1();
                break;
        }

        return view;
    }

    private void initView() {
        mLlistView.setLoadMoreListener(new LoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isNoMore) {
                    switch (mPos) {
                        case 0:
                            loadData0();
                            break;
                        case 1:
                            loadData1();
                            break;
                    }
                } else {
                    mLlistView.loadMoreEnd();
                }
            }
        });

        mLlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<BBsListData550> lvBBslistData = bbsListAdapter.getmHotIssues();
                Log.e(TAG, "position == " + position);
                Log.e(TAG, "mDocListDatas.size() == " + lvBBslistData.size());
                if (position != lvBBslistData.size()) {
                    String appmurl = lvBBslistData.get(position).getAppmurl();
                    WebUrlTypeUtil.getInstance(mActivity).urlToApp(appmurl, "0", "0");
                }
            }
        });
    }

    private void loadData0() {
        myPersonCenterMap.put("id", myId);
        myPersonCenterMap.put("page", mPage + "");
        Log.e(TAG, "myId11 == " + myId);
        Log.e(TAG, "mPage11 == " + mPage);
        myDiaryApi.getCallBack(mActivity, myPersonCenterMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    mPage++;
                    mDocListDatas = JSONUtil.jsonToArrayList(serverData.data, BBsListData550.class);

                    Log.e(TAG, "mDocListDatas---a1 === " + mDocListDatas);
                    Log.e(TAG, "mDocListDatas---a2 === " + mDocListDatas.toString());
                    Log.e(TAG, "mDocListDatas---a3 === " + mDocListDatas.size());

                    setAdapter();
                }
            }
        });
    }

    private void loadData1() {
        myPersonCenterMap.put("id", myId);
        myPersonCenterMap.put("page", mPage + "");
        Log.e(TAG, "myId222== " + myId);
        Log.e(TAG, "mPage222 == " + mPage);
        myPersonCenterApi.getCallBack(mActivity, myPersonCenterMap, new BaseCallBackListener<List<BBsListData550>>() {

            @Override
            public void onSuccess(List<BBsListData550> docListDatas) {
                mPage++;
                mDocListDatas = docListDatas;
                Log.e(TAG, "mDocListDatas---b1 === " + mDocListDatas);
                Log.e(TAG, "mDocListDatas---b2 === " + mDocListDatas.toString());
                Log.e(TAG, "mDocListDatas---b3 === " + mDocListDatas.size());
                setAdapter();
            }
        });
    }

    /**
     * 设置适配器
     */
    private void setAdapter() {
        if (bbsListAdapter == null) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mLlistView.getLayoutParams();
            if (mDocListDatas.size() != 0) {
                mLlistView.removeHeaderView(headerView);
                layoutParams.setMargins(0, 0,0,0);
            } else {
                mLlistView.addHeaderView(headerView);
                layoutParams.setMargins(0, Utils.dip2px(mActivity,100),0,0);
            }

            mLlistView.setLayoutParams(layoutParams);

            bbsListAdapter = new BBsListAdapter(mActivity, mDocListDatas);
            mLlistView.setAdapter(bbsListAdapter);

        } else {
            if (mDocListDatas.size() != 0) {
                bbsListAdapter.add(mDocListDatas);
                bbsListAdapter.notifyDataSetChanged();
            } else {
                isNoMore = true;
            }
        }

        if (isNoMore) {
            mLlistView.loadMoreEnd();
        } else {
            mLlistView.loadMoreComplete();
        }
    }

}
