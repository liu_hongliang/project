package com.module.other.netWork.netWork;

/**
 * @author 裴成浩
 * @data 2019/10/29
 */
public class ErrorResult {
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
