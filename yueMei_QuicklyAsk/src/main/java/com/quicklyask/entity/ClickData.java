package com.quicklyask.entity;

import java.util.HashMap;

public class ClickData {
    private String url;
    private HashMap<String,String> event_params;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
