package com.module.commonview.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.commonview.module.bean.MenuDoctorsData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;

import java.util.List;

public class ChatItemDoctorAdapter extends BaseQuickAdapter<MenuDoctorsData, BaseViewHolder> {
    public ChatItemDoctorAdapter(int layoutResId, @Nullable List<MenuDoctorsData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MenuDoctorsData item) {
        Glide.with(mContext)
                .load(item.getImg())
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .into((ImageView) helper.getView(R.id.chat_list_item_img));
        helper.setText(R.id.chat_list_item_doctorname,item.getTitle())
                .setText(R.id.chat_list_item_doctorconent,item.getOffice())
                .setText(R.id.chat_list_item_office,item.getDesc());
        String doctors_good_board = item.getDoctors_good_board();
        if (!TextUtils.isEmpty(doctors_good_board)){
            helper.setGone(R.id.chat_list_item_good_board,true);
            helper.setText(R.id.chat_list_item_good_board,doctors_good_board);
        }else {
            helper.setGone(R.id.chat_list_item_good_board,false);
        }


        if (helper.getLayoutPosition() == getData().size()-1){
            helper.setGone(R.id.chat_list_item_doctor_line,false);
        }else {
            helper.setGone(R.id.chat_list_item_doctor_line,true);
        }

    }
}
