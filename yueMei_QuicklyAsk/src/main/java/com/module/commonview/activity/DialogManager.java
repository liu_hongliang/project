package com.module.commonview.activity;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

public class DialogManager {
    private Dialog mDialog;

    private LinearLayout mBackground;
    private ImageView mIcon;
    private TextView mMessageText;


    private Context mContext;

    public DialogManager(Context context){
        mContext = context;
    }

    public void showRecordingDialog(){
        mDialog = new Dialog(mContext, R.style.Theme_AudioDialog);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view=inflater.inflate(R.layout.dialog_recorder,null);
        mDialog.setContentView(view);
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        params.width = Utils.dip2px(155);
        params.height = Utils.dip2px(155);
        mDialog.getWindow().setAttributes(params);
        mBackground = view.findViewById(R.id.dialog_background);
        mIcon = view.findViewById(R.id.dialge_recorder_img);
        mMessageText = view.findViewById(R.id.dialge_recorder_text);
        Glide.with(mContext).load(R.drawable.chat_voice).asGif().into(mIcon);
        mDialog.show();
    }

    //正在播放时的状态
    public void recording() {
        if (mDialog != null&&mDialog.isShowing()) {
            mBackground.setBackground(ContextCompat.getDrawable(mContext,R.drawable.dialog_recording));
//            mIcon.setVisibility(View.VISIBLE);
//            mVoice.setVisibility(View.VISIBLE);
//            mLable.setVisibility(View.VISIBLE);

//            mIcon.setImageResource(R.drawable.recorder);
            mMessageText.setText("松手发送，上滑取消");
        }
    }
    //想要取消
    public void wantToCancel(){
        if (mDialog != null&&mDialog.isShowing()) {
            mBackground.setBackground(ContextCompat.getDrawable(mContext,R.drawable.dialog_cancel));
//            mIcon.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(R.drawable.chat_cancel_send).into(mIcon);
//            mVoice.setVisibility(View.GONE);
//            mLable.setVisibility(View.VISIBLE);

//            mIcon.setImageResource(R.drawable.cancel);
            mMessageText.setText("松手取消发送");
        }
    }
    //录音时间太短
    public void tooShort() {
        if (mDialog != null&&mDialog.isShowing()) {
//            mIcon.setVisibility(View.VISIBLE);
//            mVoice.setVisibility(View.GONE);
//            mLable.setVisibility(View.VISIBLE);
//             mIcon.setImageResource(R.drawable.voice_to_short);
            mBackground.setBackground(ContextCompat.getDrawable(mContext,R.drawable.dialog_cancel));
            Glide.with(mContext).load(R.drawable.chat_cancel_send).into(mIcon);
            mMessageText.setText("录音时间过短");
        }
    }
    //关闭dialog
    public void dimissDialog(){
        if (mDialog != null&&mDialog.isShowing()) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    /**
     * 通过level更新voice上的图片
     *
     * @param level
     */
    public void updateVoiceLevel(int level){
        if (mDialog != null&&mDialog.isShowing()) {

//            int resId = mContext.getResources().getIdentifier("v" + level, "drawable", mContext.getPackageName());
//            mVoice.setImageResource(resId);
        }
    }
}
