package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2019/9/3
 */
public class TaoListDoctors {
    private String comparedTitle;                                   //对比咨询列表title
    private TaoListDoctorsCompared compared;                        //医生数据

    public String getComparedTitle() {
        return comparedTitle;
    }

    public void setComparedTitle(String comparedTitle) {
        this.comparedTitle = comparedTitle;
    }

    public TaoListDoctorsCompared getCompared() {
        return compared;
    }

    public void setCompared(TaoListDoctorsCompared compared) {
        this.compared = compared;
    }
}
