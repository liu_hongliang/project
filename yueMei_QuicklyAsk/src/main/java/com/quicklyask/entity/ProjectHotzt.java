package com.quicklyask.entity;

import com.module.home.model.bean.ProjectHotztData;

import java.util.List;

public class ProjectHotzt {

	private String code;
	private String message;

	private List<ProjectHotztData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<ProjectHotztData> getData() {
		return data;
	}

	public void setData(List<ProjectHotztData> data) {
		this.data = data;
	}

}
