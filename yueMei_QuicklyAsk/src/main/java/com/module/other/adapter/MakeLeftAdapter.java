package com.module.other.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.other.adapter.MakeLeftAdapter.ViewHolder;
import com.module.other.module.bean.MakeTagData;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by 裴成浩 on 2017/6/30.
 */

public class MakeLeftAdapter extends RecyclerView.Adapter<ViewHolder> {

    private boolean isSelected;
    private int highly;                 //传过来的是dp
    private LayoutInflater inflater;
    private Context mContext;
    private List<MakeTagData> mData;
    private int mPos;

    public MakeLeftAdapter(Context context, List<MakeTagData> data, int pos, int highly) {
        this(context, data, pos, true, highly);
    }

    public MakeLeftAdapter(Context context, List<MakeTagData> data, int pos, boolean isSelected, int highly) {
        this.mContext = context;
        this.mData = data;
        this.mPos = pos;
        this.isSelected = isSelected;
        this.highly = highly;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_make_left, parent, false);
        return new ViewHolder(itemView);        //把这个布局传到ViewHolder中
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == mPos && isSelected) {
            holder.vSelected.setVisibility(View.VISIBLE);
            holder.tvXiangmu.setTextSize(15);
            holder.tvXiangmu.setTextColor(Color.parseColor("#333333"));
            holder.tvXiangmu.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.classIfication.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.vSelected.setVisibility(View.GONE);
            holder.tvXiangmu.setTextSize(13);
            holder.tvXiangmu.setTextColor(Color.parseColor("#333333"));
            holder.tvXiangmu.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            holder.classIfication.setBackgroundColor(Color.parseColor("#F5F5F5"));
        }
        holder.tvXiangmu.setText(mData.get(position).getName());
    }

    public void setmPos(int mPos) {
        this.isSelected = true;
        this.mPos = mPos;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvXiangmu;
        private LinearLayout classIfication;
        private View vSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            vSelected = itemView.findViewById(R.id.v_selected);
            tvXiangmu = itemView.findViewById(R.id.tv_xiangmu);
            classIfication = itemView.findViewById(R.id.ll_class_ification);

            ViewGroup.LayoutParams layoutParams = tvXiangmu.getLayoutParams();
            layoutParams.height = highly;
            tvXiangmu.setLayoutParams(layoutParams);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

        }
    }

    /**
     * item的监听器
     */
    public interface OnItemClickListener {

        /**
         * 当点击某条的时候回调该方法
         *
         * @param view 被点击的视图
         * @param pos  点击的是哪个
         */
        void onItemClick(View view, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}