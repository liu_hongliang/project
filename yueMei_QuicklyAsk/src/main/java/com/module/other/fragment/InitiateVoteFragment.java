package com.module.other.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.module.api.TaoPKApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.event.MsgEvent;
import com.module.event.VoteMsgEvent;
import com.module.other.activity.InitiateVoteActivity;
import com.module.other.activity.SearchProjectActivity;
import com.module.other.adapter.InitiateVoteBottomAdapter;
import com.module.other.module.bean.SearchTaoDate;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.zfdang.multiple_images_selector.YMLinearLayoutManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.kymjs.aframe.utils.SystemTool;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.module.other.activity.InitiateVoteActivity.selectList1;
import static com.module.other.activity.InitiateVoteActivity.selectList2;

/**
 * 发起投票底部Fragment
 */
public class InitiateVoteFragment extends YMBaseFragment {
    @BindView(R.id.tv1_empty)
    TextView tvTypeEmpty;
    @BindView(R.id.tv2_empty)
    TextView tvSearchEmpty;

    Unbinder unbinder;
    private String TAG = "ProjectContrastFragment";
    @BindView(R.id.refresh)
    SmartRefreshLayout refresh;
    @BindView(R.id.ll_empty)
    LinearLayout ll_empty;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.YMLoadMore)
    YMLoadMore ymLoadMore;
    private InitiateVoteBottomAdapter initiateVoteBottomAdapter;
    private TaoPKApi taoPKApi;
    private String mType;
    private SearchTaoDate mSearchTaoData;
    private List<SearchTaoDate.TaoListBean> tao_list;
    private int mDataPage = 1;
    private String mType2;

    public static InitiateVoteFragment newInstance(String type) {
        InitiateVoteFragment fragment = new InitiateVoteFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(MsgEvent msgEvent) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (getArguments() != null) {
            mType = getArguments().getString("type");
        }
    }


    /**
     * 判断是否有网络
     */
    private void isCheckNet() {
        //有网络
        if (SystemTool.checkNet(mContext)) {
            loadData();
        } else {
            Toast.makeText(mContext, "请检查网络", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadData() {
        taoPKApi = new TaoPKApi();
        taoPKApi.getHashMap().clear();
        taoPKApi.addData("type", Integer.parseInt(mType) + 1 + "");
        taoPKApi.addData("page", mDataPage + "");
        taoPKApi.getCallBack(mContext, taoPKApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (getActivity() != null) {
                    if (serverData != null && "1".equals(serverData.code)) {
                        mSearchTaoData = JSONUtil.TransformSingleBean(serverData.data, SearchTaoDate.class);
                        tao_list = mSearchTaoData.getTao_list();
                        if (null != refresh) {
                            refresh.finishRefresh();
                        }
                        if (tao_list.size() == 0) {
                            refresh.finishLoadMoreWithNoMoreData();
                        } else {
                            refresh.finishLoadMore();
                        }
                        if (initiateVoteBottomAdapter == null) {
                            if (tao_list.size() != 0) {
                                rv.setVisibility(View.VISIBLE);
                                ll_empty.setVisibility(View.GONE);
                            } else {
                                rv.setVisibility(View.GONE);
                                ll_empty.setVisibility(View.VISIBLE);
                                ymLoadMore.setVisibility(View.GONE);
                            }
                        }
                        setAdapter();
                    } else {
                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    /**
     * 刷新数据
     */
    private void reshData() {
        mDataPage = 1;
        initiateVoteBottomAdapter = null;
        isCheckNet();
    }

    private void setAdapter() {
        if (initiateVoteBottomAdapter == null) {
            initiateVoteBottomAdapter = new InitiateVoteBottomAdapter(mContext, tao_list);
            YMLinearLayoutManager linearLayoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            rv.setLayoutManager(linearLayoutManager);
            rv.setAdapter(initiateVoteBottomAdapter);
        } else {
            //1可以继续加载 0没有更多了
            if (mSearchTaoData.getIs_has_more().equals("1")) {
                initiateVoteBottomAdapter.addList(tao_list);
            }
        }
        //item点击回调
        initiateVoteBottomAdapter.setOnItemClickListener(new InitiateVoteBottomAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (!TextUtils.isEmpty(initiateVoteBottomAdapter.getmDatas().get(pos).getId())) {
                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
                    intent.putExtra("id", initiateVoteBottomAdapter.getmDatas().get(pos).getId());
                    intent.putExtra("source", "0");
                    intent.putExtra("objid", "0");
                    mContext.startActivity(intent);
                }

            }
        });

        initiateVoteBottomAdapter.setOnItemAddClickListener(new InitiateVoteBottomAdapter.OnItemAddClickListener() {
            @Override
            public void onItemAddClick(View view, int pos) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (mType.equals("2")) {
                    mType2 = "1";
                } else if (mType.equals("3")) {
                    mType2 = "2";
                } else if (mType.equals("1")) {
                    mType2 = "3";
                }
//                发起投票--SKU添加点击
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("tao_id", initiateVoteBottomAdapter.getmDatas().get(pos).get_id());
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VOTE_TAO_ADD, pos + 1 + "", mType2), hashMap, new ActivityTypeData("170"));
                if (!selectList1.contains(initiateVoteBottomAdapter.getmDatas().get(pos).getId().trim())) {
                    if (selectList1 != null && selectList1.size() == 5) {
                        Toast.makeText(mContext, "不能添加", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    selectList1.add(initiateVoteBottomAdapter.getmDatas().get(pos).getId().trim());
                    selectList2.add(initiateVoteBottomAdapter.getmDatas().get(pos));
                    EventBus.getDefault().post(new VoteMsgEvent(1, selectList1));
                    EventBus.getDefault().post(new VoteMsgEvent(2, selectList2));
                } else {
                    Toast.makeText(mContext, "不能重复添加", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_vote;
    }

    @Override
    protected void initView(View view) {
        if (mType.equals("2")) {
            tvTypeEmpty.setText("您的购物车为空，搜索");
        } else if (mType.equals("3")) {
            tvTypeEmpty.setText("您的收藏为空，搜索");
        } else if (mType.equals("1")) {
            tvTypeEmpty.setText("您的足迹为空，搜索");
        }
        tvSearchEmpty.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvSearchEmpty.getPaint().setAntiAlias(true);
        tvSearchEmpty.setTextColor(Color.parseColor("#FF527F"));
        setMultiOnClickListener(ll_empty, tvSearchEmpty);
        refresh.setEnableRefresh(false);
//        //加载更多和刷新
//        refresh.setEnableFooterFollowWhenLoadFinished(true);

        //上拉加载更多
        refresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                mDataPage++;
                isCheckNet();
            }
        });
    }

    @Override
    protected void initData(View view) {
        reshData();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ll_empty:
                isCheckNet();
                break;
            case R.id.tv2_empty:
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Intent intent = new Intent(InitiateVoteFragment.this.getContext(), SearchProjectActivity.class);
                intent.putExtra("from", "InitiateVoteActivity");
                intent.putStringArrayListExtra("taoIdList", selectList1);
                intent.putExtra("taoData", selectList2);
                startActivityForResult(intent, 1000);
                break;
            default:
                break;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
//        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
//        getFocus();
    }

    private void getFocus() {
        getView().setFocusable(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                    // 监听到返回按钮点击事件
                    InitiateVoteFragment.this.getActivity().finish();
                    return true;// 未处理
                }
                return false;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
