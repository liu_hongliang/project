package com.module.commonview.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SaleRecTaoBean;

import java.util.List;

public class SellOutAdapter extends BaseQuickAdapter<SaleRecTaoBean,BaseViewHolder> {
    private Context mContext;
    public SellOutAdapter(Context context,int layoutResId, @Nullable List<SaleRecTaoBean> data) {
        super(layoutResId, data);
        mContext=context;
    }

    @Override
    protected void convert(BaseViewHolder helper, SaleRecTaoBean item) {
        Glide.with(mContext).load(item.getImg()).into((ImageView) helper.getView(R.id.sell_out_item_img));
        helper.setText(R.id.sell_out_item_title,item.getTitle());
        helper.setText(R.id.sell_out_item_price,item.getPrice_discount());
        TextView view = helper.getView(R.id.sell_out_item_hosprice);
        view.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);//添加横划线
        view.getPaint().setAntiAlias(true);  //抗锯齿
        helper.setText(R.id.sell_out_item_hosprice,"¥"+item.getPrice());
        helper.setText(R.id.sell_out_item_ording,item.getNumber()+"人预订");
    }
}
