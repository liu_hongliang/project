package com.module.my.controller.activity;

import com.module.my.controller.other.FlexibleScrollView;

/**
 * Created by Administrator on 2018/5/2.
 */

public interface ScrollViewListener {
    void onScrollChanged(FlexibleScrollView scrollView, int x, int y, int oldx, int oldy);
}
