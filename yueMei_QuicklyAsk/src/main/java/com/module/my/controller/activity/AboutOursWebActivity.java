package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

/**
 * 关于我们 悦美
 *
 * @author Rubin
 *
 */
public class AboutOursWebActivity extends Activity {

	private final String TAG = "AboutOursWebActivity";

	private LinearLayout contentWeb;

	private RelativeLayout back;// 返回

	private WebView bbsDetWeb;

	private AboutOursWebActivity mContex;
	public JSONObject obj_http;

	private BaseWebViewClientMessage baseWebViewClientMessage;
	private LoadingProgress mDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web_acty_aboutours);
		mContex = AboutOursWebActivity.this;

		mDialog = new LoadingProgress(mContex);
		Intent it = getIntent();
		String link = it.getStringExtra("url");
		String url = FinalConstant.baseUrl + FinalConstant.VER + link;

		contentWeb = findViewById(R.id.about_linearlayout);
		back = findViewById(R.id.about_web_back);

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);

		initWebview();

		LodUrl(url);
		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						AboutOursWebActivity.this.finish();
					}
				});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				onBackPressed();
			}
		});
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	public void initWebview() {
		bbsDetWeb = new WebView(mContex);
		bbsDetWeb.getSettings().setUseWideViewPort(true);
		bbsDetWeb.setWebViewClient(baseWebViewClientMessage);
		bbsDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(bbsDetWeb);
	}
	/**
	 * 加载web
	 */
	public void LodUrl(String url) {
		mDialog.startLoading();
		WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
		if (null != bbsDetWeb) {
			bbsDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
