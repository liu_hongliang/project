package com.module.other.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.api.InitLoadCityApi;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.bean.IsFocuData;
import com.module.commonview.view.CommonTopBar;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.model.bean.BBsListData550;
import com.module.community.model.bean.UserClickData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.adapter.DiaryListAdapter;
import com.module.doctor.model.bean.GroupDiscData;
import com.module.doctor.model.bean.MainCityData;
import com.module.doctor.model.bean.TaoPopItemIvData;
import com.module.doctor.view.ProjectDetailListFragment;
import com.module.home.model.bean.ProjectDetailsListData;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.MyDiaryApi;
import com.module.other.adapter.DiaryCityAdapter;
import com.module.other.adapter.MyAdapter9;
import com.module.other.api.HomeDiarySXApi;
import com.module.other.module.api.LoadCityListApi;
import com.module.other.module.bean.CityPartData;
import com.module.other.module.bean.IdName;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.activity.interfaces.Project2ListSelectListener;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.DropDownListView;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import simplecache.ACache;

/**
 * 精选日记
 */
public class HomeDiarySXActivity extends YMBaseActivity {

    @BindView(R.id.tab_doc_city_rly)
    RelativeLayout docCityRly;
    @BindView(R.id.tab_doc_list_city_tv)
    TextView cityTv;
    @BindView(R.id.home_diary_top)
    CommonTopBar mTop;
    @BindView(R.id.content)
    LinearLayout contentLy;
    @BindView(R.id.my_doc_list_view)
    DropDownListView mlist;

    @BindView(R.id.diary_select_show)
    LinearLayout mDiary_select_show;
    @BindView(R.id.project_part_pop_rly1)
    RelativeLayout partRly;
    @BindView(R.id.project_part_pop_tv)
    TextView partTv;
    @BindView(R.id.sort_content)
    LinearLayout mSort_content;

    @BindView(R.id.tab_recommend_click)
    LinearLayout mTab_recommend_click;
    @BindView(R.id.tab_recommend_bg)
    LinearLayout mTab_recommend_bg;
    @BindView(R.id.tab_recommend_tv)
    TextView mTab_recommend_tv;
    @BindView(R.id.tab_popularity_click)
    LinearLayout mTab_popularity_click;
    @BindView(R.id.tab_popularity_bg)
    LinearLayout mTab_popularity_bg;
    @BindView(R.id.tab_popularity_tv)
    TextView mTab_popularity_tv;

    @BindView(R.id.tab_hot_click)
    LinearLayout mTab_hot_click;
    @BindView(R.id.tab_hot_bg)
    LinearLayout mTab_hot_bg;
    @BindView(R.id.tab_hot_tv)
    TextView mTab_hot_tv;
    @BindView(R.id.tab_new_click)
    LinearLayout mTab_new_click;

    @BindView(R.id.tab_new_bg)
    LinearLayout mTab_new_bg;
    @BindView(R.id.tab_new_tv)
    TextView mTab_new_tv;
    @BindView(R.id.ly_content_ly1)
    RelativeLayout otherRly;
    @BindView(R.id.part_search_ly)
    LinearLayout partSearchLy;
    @BindView(R.id.pop_project_listview)
    ListView partlist;
    @BindView(R.id.my_collect_post_tv_nodata)
    LinearLayout nodataTv;  // 数据为空时候的显示

    private final String TAG = "HomeDiarySXActivity";
    private int windowsWight;

    private int mCurPage = 1;
    private Handler mHandler;
    private List<BBsListData550> lvBBslistData = new ArrayList<>();
    private List<BBsListData550> lvBBslistMoreData = new ArrayList<>();
    private DiaryListAdapter bbsListAdapter;

    private String partId = "0";
    private String p_partId = "0";
    private String partName;// 部位名
    public static int mPosition;
    private MyAdapter9 adapter;
    private ProjectDetailListFragment mFragment;

    private List<TaoPopItemIvData> lvGroupData = new ArrayList<>();

    private List<IdName> citys = new ArrayList<>();
    private List<IdName> parts = new ArrayList<>();

    private String cityId = "0";
    private String sortId = "1";

    private int cru = 0;
    PartPopupwindows partPop;
    DiaryCityAdapter homepartAdater;
    GridView cityPartlist;


    private String curCity = "";
    private String city = "";
    private boolean isCity = false;

    private List<MainCityData> citylist = new ArrayList<>();

    private ACache mCache;

    private LoadingProgress progress;
    private HomeDiarySXApi homeDiarySXApi;
    private HashMap<String, Object> HomeDiarySXMap = new HashMap<>();
    private String mFlag;                   //1代表从我的日记跳过来的
    private int mTempPos = -1;

    private ProjectDetailsListData screenData;


    @Override
    protected int getLayoutId() {
        return R.layout.acty_home_diary_sx;
    }

    @Override
    protected void initView() {
        mCache = ACache.get(this);
        mFlag = getIntent().getStringExtra("flag");
        screenData = getIntent().getParcelableExtra("screen");
        if(screenData != null){
            partId = screenData.getId();
            partName = screenData.getName();
            partTv.setText(partName);
        }

        homeDiarySXApi = new HomeDiarySXApi();

        progress = new LoadingProgress(mContext);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;

        if ("1".equals(mFlag)) {  //1代表从我的日记跳过来的
            mTop.setCenterText("我的日记");
            mDiary_select_show.setVisibility(View.GONE);
            mSort_content.setVisibility(View.GONE);

        }
        docCityRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.tongjiApp(mContext, "sharelist_city", "post", "post", "");

                if (citys.size() > 0) {
                    partPop.showAtLocation(contentLy, Gravity.CENTER, 0, 0);
                }
            }
        });

        otherRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                partSearchLy.setVisibility(View.GONE);
            }
        });

        partRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (partSearchLy.getVisibility() == View.GONE) {
                    Utils.tongjiApp(mContext, "sharelist_tag", "post", "post", "");
                    mSort_content.setVisibility(View.GONE);
                    partSearchLy.setVisibility(View.VISIBLE);
                } else {
                    mSort_content.setVisibility(View.VISIBLE);
                    partSearchLy.setVisibility(View.GONE);
                }

                if (null != partPop && partPop.isShowing()) {
                    partPop.dismiss();
                }
            }
        });
        //推荐
        mTab_recommend_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackground(1);
            }
        });
        //人气
        mTab_popularity_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackground(2);
            }
        });
        //最热
        mTab_hot_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackground(3);
            }
        });
        //最新
        mTab_new_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackground(4);
            }
        });

    }

    @Override
    protected void initData() {
        initloadCity();
    }


    @Override
    protected void onResume() {
        super.onResume();

        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);

        curCity = Cfg.loadStr(mContext, FinalConstant.DWCITY, "");

        if (curCity.length() > 0) {

            if (curCity.equals("失败")) {
                city = "全国";
                loadCityList();

            } else if (curCity.equals(city)) {

            } else {
                city = curCity;
                loadCityList();
            }
        } else {
            city = "全国";
            loadCityList();
        }

    }

    void setBackground(int id) {
        sortId = id + "";
        if (1 == id) {
            mTab_recommend_tv.setTextColor(getResources().getColor(R.color.tab_tag));
            mTab_recommend_bg.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab));
        } else {
            mTab_recommend_tv.setTextColor(getResources().getColor(R.color._33));
            mTab_recommend_bg.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab2));
        }
        if (2 == id) {
            mTab_popularity_tv.setTextColor(getResources().getColor(R.color.tab_tag));
            mTab_popularity_bg.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab));
        } else {
            mTab_popularity_tv.setTextColor(getResources().getColor(R.color._33));
            mTab_popularity_bg.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab2));
        }
        if (3 == id) {
            mTab_hot_tv.setTextColor(getResources().getColor(R.color.tab_tag));
            mTab_hot_bg.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab));
        } else {
            mTab_hot_tv.setTextColor(getResources().getColor(R.color._33));
            mTab_hot_bg.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab2));
        }
        if (4 == id) {
            mTab_new_tv.setTextColor(getResources().getColor(R.color.tab_tag));
            mTab_new_bg.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab));
        } else {
            mTab_new_tv.setTextColor(getResources().getColor(R.color._33));
            mTab_new_bg.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab2));
        }
        onreshData();
    }


    void loadCityList() {

        Log.e(TAG, "bbbb");
        if (!SystemTool.checkNet(mContext)) {
            Log.e(TAG, "ccccc");
            initList();
            return;
        }

        Map<String, Object> maps = new HashMap<>();
        maps.put("cityId", cityId);
        new LoadCityListApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.d(TAG, "--------->" + serverData.data);
                if (null != serverData.data) {
                    try {
                        CityPartData cityPartData = JSONUtil.TransformSingleBean(serverData.data, CityPartData.class);
                        citys = cityPartData.getCity();
                        int sizecity = citys.size();
                        for (int i = 0; i < sizecity; i++) {

                            if (city.equals(citys.get(i).getName())) {
                                cityId = citys.get(i).getId();
                                cru = i;
                                isCity = true;
                                break;
                            } else {
                                cru = 0;
                                isCity = false;
                            }
                        }

                        if (isCity) {
                            cityTv.setText(city);
                        } else {
                            cityTv.setText("全国");
                        }

                        partPop = new PartPopupwindows();
                        LoadPartCityList();
                        initList();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    void LoadPartCityList() {

        mPosition = 0;
        if (null != lvGroupData && lvGroupData.size() > 0) {
            lvGroupData.clear();
        }
        Map<String, Object> maps = new HashMap<>();
        maps.put("cityId", cityId);
        new LoadCityListApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (null != serverData) {
                    try {
                        CityPartData cityPartData = JSONUtil.TransformSingleBean(serverData.data, CityPartData.class);
                        parts = cityPartData.getTag();
                        int sizes = parts.size();
                        for (int i = 0; i < sizes; i++) {
                            TaoPopItemIvData itm = new TaoPopItemIvData();
                            itm.set_id(parts.get(i).getId());
                            itm.setName(parts.get(i).getName());
                            lvGroupData.add(itm);
                        }

                        for (int i = 0; i < lvGroupData.size(); i++) {
                            if (p_partId.equals(lvGroupData.get(i).get_id())) {
                                mPosition = i;
                            }
                        }

                        adapter = new MyAdapter9(HomeDiarySXActivity.this, lvGroupData);
                        partlist.setAdapter(adapter);

                        // 创建MyFragment对象
                        mFragment = new ProjectDetailListFragment();
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        mFragment.setCascadingMenuViewOnSelectListener(new NMProject2ListSelectListener());
                        fragmentTransaction.replace(R.id.pop_fragment_container2, mFragment);
                        // 通过bundle传值给MyFragment
                        Bundle bundle = new Bundle();
                        bundle.putString("id", lvGroupData.get(mPosition).get_id());
                        bundle.putString("z_id", partId);
                        mFragment.setArguments(bundle);
                        fragmentTransaction.commitAllowingStateLoss();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        partlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                // 拿到当前位置
                mPosition = pos;
                TaoPopItemIvData taoPopItemIvData = lvGroupData.get(pos);
                p_partId = taoPopItemIvData.get_id();
                adapter = new MyAdapter9(HomeDiarySXActivity.this, lvGroupData);
                partlist.setAdapter(adapter);
                partlist.setSelection(pos);
                // 即使刷新adapter
                adapter.notifyDataSetChanged();
                mFragment = new ProjectDetailListFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                mFragment.setCascadingMenuViewOnSelectListener(new NMProject2ListSelectListener());
                fragmentTransaction.replace(R.id.pop_fragment_container2, mFragment);
                // 通过bundle传值给MyFragment
                Bundle bundle = new Bundle();
                bundle.putString("id", lvGroupData.get(pos).get_id());
                bundle.putString("z_id", partId);
                mFragment.setArguments(bundle);
                fragmentTransaction.commitAllowingStateLoss();
            }
        });
    }

    // 级联菜单选择回调接口
    class NMProject2ListSelectListener implements Project2ListSelectListener {

        @Override
        public void getValue(GroupDiscData projectItem) {
            mSort_content.setVisibility(View.VISIBLE);
            partSearchLy.setVisibility(View.GONE);
            partName = projectItem.getCate_name();
            partId = projectItem.get_id();
            partTv.setText(partName);
            onreshData();
        }
    }

    void onreshData() {
        lvBBslistData = null;
        lvBBslistMoreData = null;
        mCurPage = 1;
        progress.startLoading();
        lodHotIssueData(true);
        mlist.setHasMore(true);
    }

    public class PartPopupwindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PartPopupwindows() {

            final View view = View.inflate(mContext, R.layout.pop_diary_city, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            RelativeLayout closeRly = view.findViewById(R.id.write_que_back);

            closeRly.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            cityPartlist = view.findViewById(R.id.group_grid_list1);
            ViewGroup.LayoutParams params0 = cityPartlist.getLayoutParams();
            params0.width = windowsWight;
            cityPartlist.setLayoutParams(params0);

            homepartAdater = new DiaryCityAdapter(HomeDiarySXActivity.this, citys, cru);
            cityPartlist.setAdapter(homepartAdater);

            cityPartlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                    cru = pos;
                    cityId = citys.get(pos).getId();
                    cityTv.setText(citys.get(pos).getName());

                    homepartAdater = new DiaryCityAdapter(mContext, citys, cru);
                    cityPartlist.setAdapter(homepartAdater);

                    curCity = citys.get(pos).getName();
                    dismiss();
                    onreshData();

                    if (null != citylist) {
                        int num = citylist.size();
                        for (int i = 0; i < num; i++) {

                            if (citys.get(pos).getName().equals("全国")) {
                                Cfg.saveStr(HomeDiarySXActivity.this.mContext, FinalConstant.DWCITY, citys.get(pos).getName());
                            }

                            if (citys.get(pos).getName().equals(citylist.get(i).getName())) {
                                Cfg.saveStr(HomeDiarySXActivity.this.mContext, FinalConstant.DWCITY, citys.get(pos).getName());
                            }
                        }
                    }
                }
            });

        }
    }

    void initList() {
        Log.e(TAG, "aaaa");
        mHandler = getHandler();
        progress.startLoading();
        lodHotIssueData(true);

        mlist.setOnDropDownListener(new DropDownListView.OnDropDownListener() {

            @Override
            public void onDropDown() {
                lvBBslistData = null;
                lvBBslistMoreData = null;
                mCurPage = 1;
                progress.startLoading();
                lodHotIssueData(true);
                mlist.setHasMore(true);
            }
        });

        mlist.setOnBottomListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "======");
                lodHotIssueData(false);
            }
        });

        mlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos, long arg3) {

                if (null != lvBBslistData && lvBBslistData.size() > 0) {
                    mTempPos = pos;
                    String url = lvBBslistData.get(pos).getUrl();
                    String qid = lvBBslistData.get(pos).getQ_id();
                    String appmurl = lvBBslistData.get(pos).getAppmurl();
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");

//                    HashMap<String, String> hashMap = new HashMap<>();
//                    hashMap.put("id", qid);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.FEATURED_DIARY, (pos + 1) + ""), lvBBslistData.get(pos).getEvent_params());
                }
            }
        });
    }

    void lodHotIssueData(final boolean isDonwn) {
        List<BBsListData550> replylistDatas = getCacheData();
        if (replylistDatas != null && replylistDatas.size() > 0) {
            Message msg = null;
            if (isDonwn) {
                if (mCurPage == 1) {
                    lvBBslistData = replylistDatas;
                    msg = mHandler.obtainMessage(1);
                    msg.sendToTarget();
                }
            } else {
                mCurPage++;
                lvBBslistMoreData = replylistDatas;
                msg = mHandler.obtainMessage(2);
                msg.sendToTarget();
            }
        } else {
            if ("1".equals(mFlag)) {  //1代表从我的日记跳过来的
                if (isDonwn) {
                    if (mCurPage == 1) {
                        HashMap<String, Object> maps = new HashMap<>();
                        maps.put("id", Utils.getUid());
                        maps.put("page", mCurPage + "");
                        new MyDiaryApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData serverData) {
                                if ("1".equals(serverData.code)) {
                                    List<BBsListData550> bBsListData550s = JSONUtil.jsonToArrayList(serverData.data, BBsListData550.class);
                                    lvBBslistData = bBsListData550s;
                                    Message msg = mHandler.obtainMessage(1);
                                    msg.sendToTarget();


                                }
                            }
                        });
                    }
                } else {
                    mCurPage++;
                    HashMap<String, Object> maps = new HashMap<>();
                    maps.put("id", Utils.getUid());
                    maps.put("page", mCurPage + "");
                    new MyDiaryApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData) {
                            if ("1".equals(serverData.code)) {
                                List<BBsListData550> bBsListData550s = JSONUtil.jsonToArrayList(serverData.data, BBsListData550.class);
                                lvBBslistMoreData = bBsListData550s;
                                Message msg = mHandler.obtainMessage(2);
                                msg.sendToTarget();


                            }
                        }
                    });
                }

            } else {   //精选日记接口
                if (isDonwn) {
                    if (mCurPage == 1) {
                        HomeDiarySXMap.put("page", mCurPage + "");
                        HomeDiarySXMap.put("cateid", partId);
                        HomeDiarySXMap.put("cityId", cityId);
                        HomeDiarySXMap.put("sort", sortId);
                        Log.e(TAG, "partId == " + partId);
                        homeDiarySXApi.getCallBack(mContext, HomeDiarySXMap, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData serverData) {
                                mCache.put("diary_json" + partId + "_" + mCurPage, serverData.data, 60 * 60 * 24);
                                if ("1".equals(serverData.code)) {
                                    List<BBsListData550> bBsListData550s = JSONUtil.jsonToArrayList(serverData.data, BBsListData550.class);
                                    lvBBslistData = bBsListData550s;
                                    Message msg = mHandler.obtainMessage(1);
                                    msg.sendToTarget();
                                }
                            }
                        });
                    }
                } else {
                    mCurPage++;
                    HomeDiarySXMap.put("page", mCurPage + "");
                    HomeDiarySXMap.put("cateid", partId);
                    HomeDiarySXMap.put("cityId", cityId);
                    HomeDiarySXMap.put("sort", sortId);
                    Log.e(TAG, "partId == " + partId);
                    homeDiarySXApi.getCallBack(mContext, HomeDiarySXMap, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData) {
                            mCache.put("diary_json" + partId + "_" + mCurPage, serverData.data, 60 * 60 * 24);
                            if ("1".equals(serverData.code)) {
                                List<BBsListData550> bBsListData550s = JSONUtil.jsonToArrayList(serverData.data, BBsListData550.class);
                                lvBBslistMoreData = bBsListData550s;
                                Message msg = mHandler.obtainMessage(2);
                                msg.sendToTarget();
                            }
                        }
                    });
                }
            }
        }
    }

    /**
     * 获取到缓存的数据
     *
     * @return
     */
    private List<BBsListData550> getCacheData() {
        ACache mCache;
        mCache = ACache.get(mContext);

        if (!SystemTool.checkNet(mContext)) {
            String homejson = mCache.getAsString("diary_json" + partId + "_" + mCurPage);
            if (homejson != null) {
                List<BBsListData550> hotData = JSONUtil.jsonToArrayList(homejson, BBsListData550.class);
                return hotData;
            }
        }

        return null;
    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:

                        if (null != lvBBslistData && lvBBslistData.size() > 0) {

                            nodataTv.setVisibility(View.GONE);
                            mlist.setVisibility(View.VISIBLE);

                            progress.stopLoading();

                            bbsListAdapter = new DiaryListAdapter(HomeDiarySXActivity.this, lvBBslistData, mFlag);

                            mlist.setAdapter(bbsListAdapter);
                            mlist.onDropDownComplete();
                            mlist.onBottomComplete();

                            bbsListAdapter.setOnItemPersonClickListener(new DiaryListAdapter.OnItemPersonClickListener() {
                                @Override
                                public void onItemPersonClick(String id, int pos) {
                                    mTempPos = pos;
                                    Intent intent = new Intent(mContext, PersonCenterActivity641.class);
                                    intent.putExtra("id", id);
                                    startActivityForResult(intent, 18);
                                    UserClickData userClickData = lvBBslistData.get(pos).getUserClickData();
                                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.FEATURED_SHARELIST_USER_CLICK,(pos + 1)+""),userClickData.getEvent_params(),new ActivityTypeData("18"));
                                }
                            });

                        } else {
                            progress.stopLoading();
                            nodataTv.setVisibility(View.VISIBLE);
                            mlist.setVisibility(View.GONE);
                        }
                        break;
                    case 2:
                        if (null != lvBBslistMoreData && lvBBslistMoreData.size() > 0) {

                            bbsListAdapter.add(lvBBslistMoreData);
                            bbsListAdapter.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {
                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }

    void initloadCity() {
        new InitLoadCityApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                Log.d(TAG, "---------->" + s.data);
                if ("1".equals(s.code)) {
                    citylist = JSONUtil.jsonToArrayList(s.data, MainCityData.class);
                    Log.e(TAG, "citylist == " + citylist.toString());
                    Log.e(TAG, "citylist个数 == " + citylist.size());
                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            if (mTempPos >= 0) {
                isFocu(lvBBslistData.get(mTempPos).getUser_id());
            }
        }
    }

    /**
     * 判断是否关注
     */
    private void isFocu(String id) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new IsFocuApi().getCallBack(mContext, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                bbsListAdapter.setEachFollowing(mTempPos, isFocuData.getFolowing());
                mTempPos = -1;
            }
        });
    }

}