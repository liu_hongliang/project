package com.module.other.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.util.JSONUtil;

import java.util.List;
import java.util.Map;

/**
 * 限时淘 与 最新上架列表
 * Created by 裴成浩 on 2018/3/13.
 */

public class HotIssueApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.BOARD, "taolist", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if("1".equals(mData.code)){
                    List<HomeTaoData> homeTaoDatas = JSONUtil.jsonToArrayList(mData.data, HomeTaoData.class);
                    listener.onSuccess(homeTaoDatas);
                }
            }
        });
    }
}
