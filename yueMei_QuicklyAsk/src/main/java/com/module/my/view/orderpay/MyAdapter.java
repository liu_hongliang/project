package com.module.my.view.orderpay;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.KeyBoardUtils;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> implements View.OnClickListener {

    /**
     *  * checkbox的Hashmap集合，一条数据对应一个checkbox状态
     * <p>
     *  
     */
    private final HashMap<Integer, Boolean> map;

    //接口实例
    private RecyclerViewOnItemClickListener onItemClickListener;
    /**
     *  *数据源
     *  
     */
    private List<String> list;
    private Context mContext;
    private List<String>message=new ArrayList<>();

    public MyAdapter(Context context,List<String> datas,int position) {
        mContext=context;
        map = new HashMap<>();
        list = datas;
        for (int i = 0; i < datas.size(); i++) {
            //Checkbox初始状态置为false
            if (i==position){
                map.put(i, true);
            }else {
                map.put(i, false);
            }
        }
    }


    /**
     *  * 全选，
     *  
     */
    public void selectAll() {
        Set<Map.Entry<Integer, Boolean>> entries = map.entrySet();
        boolean shouldall = false;
        for (Map.Entry<Integer, Boolean> entry : entries) {
            Boolean value = entry.getValue();
            if (!value) {
                shouldall = true;
                break;
            }
        }


        for (Map.Entry<Integer, Boolean> entry : entries) {
            entry.setValue(shouldall);
        }
        notifyDataSetChanged();
    }


    /**
     *  * 反选
     *  
     */
    public void neverAll() {
        Set<Map.Entry<Integer, Boolean>> entries = map.entrySet();
        for (Map.Entry<Integer, Boolean> entry : entries) {
            entry.setValue(!entry.getValue());
        }
        notifyDataSetChanged();
    }


    /**
     *  * 单选
     *  * @param postion
     *  
     */
    public void singleSelect(int postion) {
        Set<Map.Entry<Integer, Boolean>> entries = map.entrySet();
        for (Map.Entry<Integer, Boolean> entry : entries) {
            entry.setValue(false);
        }
        map.put(postion, true);
        notifyDataSetChanged();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.reason_item, parent, false);
        inflate.setOnClickListener(this);
        return new MyViewHolder(inflate);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ReasonActivity reasonActivity= (ReasonActivity) mContext;
        holder.txt.setText(list.get(position));
        //从map集合获取状态
        holder.checkBox.setChecked(map.get(position));

        holder.itemView.setTag(position);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.put(position, !map.get(position));
                //刷新适配器
                notifyDataSetChanged();
                //单选
                singleSelect(position);
                if (position==list.size()-1){
                    reasonActivity.mMessageVisorgone.setVisibility(View.VISIBLE);
                    KeyBoardUtils.showKeyBoard(reasonActivity, reasonActivity.mPersonOhterEdt);
                    reasonActivity.mScrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            reasonActivity.mScrollView.scrollTo(0, Utils.dip2px(290));
                        }
                    });
                }else {
                    reasonActivity.mMessageVisorgone.setVisibility(View.GONE);
                }


            }
        });
    }


    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    @Override
    public void onClick(View v) {
        if (onItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            onItemClickListener.onItemClickListener(v, (Integer) v.getTag());
        }
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        private TextView txt;
        private CheckBox checkBox;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            txt = itemView.findViewById(R.id.person_reason_tv);
            checkBox = itemView.findViewById(R.id.cancel_reson_check);
        }
    }

    //设置点击事件
    public void setRecyclerViewOnItemClickListener(RecyclerViewOnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    //接口回调设置点击事件
    public interface RecyclerViewOnItemClickListener {
        //点击事件
        void onItemClickListener(View view, int position);

    }

    //返回集合给MainActivity
    public Map<Integer, Boolean> getMap() {
        return map;
    }

    public void setRecyclerCallback(RecyclerCallback recyclerCallback) {
        mRecyclerCallback = recyclerCallback;
    }

    public RecyclerCallback mRecyclerCallback;
    public interface RecyclerCallback{
        void getEditTest(EditText editText);
    }
}
