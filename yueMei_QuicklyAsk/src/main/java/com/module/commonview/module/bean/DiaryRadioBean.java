package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryRadioBean implements Parcelable {
    /**
     * radio_id : 0
     * radio_name : 全部 7
     */

    private String radio_id;
    private String radio_name;
    private String share_num;
    private boolean isSelected = false;
    private HashMap<String,String> event_params;

    protected DiaryRadioBean(Parcel in) {
        radio_id = in.readString();
        radio_name = in.readString();
        share_num = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(radio_id);
        dest.writeString(radio_name);
        dest.writeString(share_num);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryRadioBean> CREATOR = new Creator<DiaryRadioBean>() {
        @Override
        public DiaryRadioBean createFromParcel(Parcel in) {
            return new DiaryRadioBean(in);
        }

        @Override
        public DiaryRadioBean[] newArray(int size) {
            return new DiaryRadioBean[size];
        }
    };

    public String getRadio_id() {
        return radio_id;
    }

    public void setRadio_id(String radio_id) {
        this.radio_id = radio_id;
    }

    public String getRadio_name() {
        return radio_name;
    }

    public void setRadio_name(String radio_name) {
        this.radio_name = radio_name;
    }

    public String getShare_num() {
        return share_num;
    }

    public void setShare_num(String share_num) {
        this.share_num = share_num;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
