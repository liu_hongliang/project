package com.module.community.controller.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.home.model.bean.SearchTaolistData;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

public class ItemAdapter extends BaseQuickAdapter<SearchTaolistData, BaseViewHolder> {
    public ItemAdapter(int layoutResId, @Nullable List<SearchTaolistData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SearchTaolistData item) {
        Glide.with(mContext).load(item.getImg())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.TOP))
                .into((ImageView) helper.getView(R.id.item_img));
        Glide.with(mContext).load(item.getImg_resouce()).into((ImageView) helper.getView(R.id.item_tagimg));
        helper.setText(R.id.item_title,item.getTitle())
                .setText(R.id.item_txt_price,"¥"+item.getPrice());
        String coupons = item.getCoupons();
        if (!TextUtils.isEmpty(coupons)){
            helper.setGone(R.id.item_coupons,true);
            helper.setText(R.id.item_coupons,coupons);
        }else {
            helper.setGone(R.id.item_coupons,false);
        }


    }
}
