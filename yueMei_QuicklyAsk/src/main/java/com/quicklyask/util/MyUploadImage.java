package com.quicklyask.util;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.QiNiuTokenApi;
import com.module.commonview.module.bean.QiNiuBean;
import com.module.other.netWork.netWork.QiNiuConfigration;
import com.module.other.netWork.netWork.ServerData;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;
import com.quicklyask.view.ProcessImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import static com.module.commonview.view.DiaryCommentDialogView.IMAG_FAILURE;
import static com.module.commonview.view.DiaryCommentDialogView.IMAG_PROGRESS;
import static com.module.commonview.view.DiaryCommentDialogView.IMAG_SUCCESS;
import static com.module.my.controller.activity.WriteNoteActivity.CONSUMER_CERTIFICATE_LODING;
import static com.module.my.controller.activity.WriteNoteActivity.CONSUMER_CERTIFICATE_SUCCESS;

/**
 * Created by 裴成浩 on 2017/6/12.
 */

public class MyUploadImage {
    public static final String TAG = "MyUploadImage";
    private File uploadFile;
    private int mAnInt;
    private Activity mActivity;
    private Handler mHandler;
    private ProcessImageView mImgs;
    private boolean isLianXu;
    private boolean isEnd;

    public static MyUploadImage getMyUploadImage(Activity activity, int anInt, Handler handler, String mFilePath, ProcessImageView imgs, boolean isLianXu) {
        return new MyUploadImage(activity, anInt, handler, mFilePath, imgs, isLianXu);
    }

    public static MyUploadImage getMyUploadImage(Activity activity, Handler handler, String mFilePath) {
        return new MyUploadImage(activity, handler, mFilePath);
    }

    public static MyUploadImage getMyUploadImage(Activity activity, Handler handler, String mFilePath, Boolean isEnd) {
        return new MyUploadImage(activity, handler, mFilePath, isEnd);
    }

    private MyUploadImage(Activity activity, int anInt, Handler handler, String mFilePath, ProcessImageView imgs, boolean isLianXu) {
        this.mActivity = activity;
        this.mAnInt = anInt;
        this.mHandler = handler;
        this.mImgs = imgs;
        this.isLianXu = isLianXu;
        uploadFile = new File(mFilePath);
    }

    private MyUploadImage(Activity activity, Handler handler, String mFilePath) {
        this.mActivity = activity;
        this.mHandler = handler;
        uploadFile = new File(mFilePath);
    }

    private MyUploadImage(Activity activity, Handler handler, String mFilePath, Boolean isEnd) {
        this.mActivity = activity;
        this.mHandler = handler;
        uploadFile = new File(mFilePath);
        this.isEnd = isEnd;
    }


    public void uploadImage(final String qiNiuKey) {
        String qiniutoken = Cfg.loadStr(mActivity, "qiniutoken", "");
        UploadManager uploadManager = QiNiuConfigration.getInstance().init();
        uploadManager.put(uploadFile, qiNiuKey, qiniutoken, new UpCompletionHandler() {
            @Override
            public void complete(String key, ResponseInfo info, JSONObject response) {

                if (isLianXu) {             //上传完成后继续上传后一个
                    if (info.isOK()) {
                        Log.e(TAG, "Upload Success");
                        Log.e(TAG, "上传的 --- 3333333");
                        sendMessage(3);
                    } else {
                        try {
                            Log.e(TAG, " response=== " + response);
                            if (response != null) {
                                String error = response.getString("error");
                                if ("expired token".equals(error)) {
                                    new QiNiuTokenApi().getCallBack(mActivity, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                                        @Override
                                        public void onSuccess(ServerData serverData) {
                                            if ("1".equals(serverData.code)) {
                                                try {
                                                    QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                                                    Cfg.saveStr(mActivity, "qiniutoken", qiNiuBean.getQiniu_token());
                                                    uploadImage(qiNiuKey);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    Log.e(TAG, "上传的 --- 444444 == " + mAnInt);
                                    sendMessage(4);
                                }
                            } else {
                                sendMessage(4);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                } else {                     //上传完成后不继续上传后一个

                    if (info.isOK()) {
                        Log.e(TAG, "上传的 --- 5555555");
                        sendMessage(5);
                    } else {
                        try {
                            if (response != null) {
                                String error = response.getString("error");
                                if ("expired token".equals(error)) {
                                    new QiNiuTokenApi().getCallBack(mActivity, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                                        @Override
                                        public void onSuccess(ServerData serverData) {
                                            if ("1".equals(serverData.code)) {
                                                try {
                                                    QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                                                    Cfg.saveStr(mActivity, "qiniutoken", qiNiuBean.getQiniu_token());
                                                    uploadImage(qiNiuKey);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    Log.e(TAG, "上传的 --- 6666666");
                                    sendMessage(6);
                                }
                            } else {
                                sendMessage(6);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
                Log.e(TAG, key + ",\r\n " + info + ",\r\n " + response);
            }
        }, new UploadOptions(null, null, false, new UpProgressHandler() {
            @Override
            public void progress(String key, double percent) {
                Log.e(TAG, "progress===" + (int) (percent * 100));
                Message obtain = Message.obtain();
                obtain.arg1 = (int) (percent * 100);
                obtain.obj = mImgs;
                obtain.what = 2;
                mHandler.sendMessage(obtain);
            }
        }, null));

    }

    /**
     * 上传消费凭证
     *
     * @param qiNiuKey
     */
    public void upConsumerCertificate(final String qiNiuKey) {
        String qiniutoken = Cfg.loadStr(mActivity, "qiniutoken", "");
        UploadManager uploadManager = QiNiuConfigration.getInstance().init();
        uploadManager.put(uploadFile, qiNiuKey, qiniutoken, new UpCompletionHandler() {
            @Override
            public void complete(String key, ResponseInfo info, JSONObject response) {

                if (info.isOK()) {
                    Log.e(TAG, "Upload Success");
                    Message obtain = Message.obtain();
                    obtain.what = CONSUMER_CERTIFICATE_SUCCESS;
                    mHandler.sendMessage(obtain);
                } else {
                    try {
                        if (response != null) {
                            String error = response.getString("error");
                            if ("expired token".equals(error)) {
                                new QiNiuTokenApi().getCallBack(mActivity, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData serverData) {
                                        if ("1".equals(serverData.code)) {
                                            try {
                                                QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                                                Cfg.saveStr(mActivity, "qiniutoken", qiNiuBean.getQiniu_token());
                                                upConsumerCertificate(qiNiuKey);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            } else {
                                Log.e(TAG, "上传的 --- 444444 == " + mAnInt);
                            }
                        } else {
                            Log.e(TAG, "上传的 --- 444444 == " + mAnInt);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                Log.e(TAG, "key==" + key + "info==" + info + "response==" + response);
            }
        }, new UploadOptions(null, null, false, new UpProgressHandler() {
            @Override
            public void progress(String key, double percent) {
                Message obtain = Message.obtain();
                obtain.what = CONSUMER_CERTIFICATE_LODING;
                mHandler.sendMessage(obtain);
            }
        }, null));

    }

    /**
     * 评论列表上传图片
     *
     * @param qiNiuKey
     */
    public void upCommentsList(final String qiNiuKey, final int[] imageWidthHeight) {
        String qiniutoken = Cfg.loadStr(mActivity, "qiniutoken", "");
        UploadManager uploadManager = QiNiuConfigration.getInstance().init();
        uploadManager.put(uploadFile, qiNiuKey, qiniutoken, new UpCompletionHandler() {
            @Override
            public void complete(final String key, ResponseInfo info, JSONObject response) {

                if (info.isOK()) {
                    Log.e(TAG, "Upload Success");
                    Message obtain = Message.obtain();
                    obtain.what = IMAG_SUCCESS;
                    obtain.obj = qiNiuKey;
                    obtain.arg1 = imageWidthHeight[0];
                    obtain.arg2 = imageWidthHeight[1];
                    mHandler.sendMessage(obtain);
                } else {
                    try {
                        if (response != null) {
                            String error = response.getString("error");
                            if ("expired token".equals(error)) {
                                new QiNiuTokenApi().getCallBack(mActivity, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData serverData) {
                                        if ("1".equals(serverData.code)) {
                                            try {
                                                QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                                                Cfg.saveStr(mActivity, "qiniutoken", qiNiuBean.getQiniu_token());
                                                upCommentsList(qiNiuKey,imageWidthHeight);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            } else {
                                Log.e(TAG, "Upload Fail");
                                Message obtain = Message.obtain();
                                obtain.what = IMAG_FAILURE;
                                mHandler.sendMessage(obtain);
                            }
                        } else {
                            Log.e(TAG, "Upload Fail");
                            Message obtain = Message.obtain();
                            obtain.what = IMAG_FAILURE;
                            mHandler.sendMessage(obtain);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                Log.e(TAG, "key==" + key + "info==" + info + "response==" + response);
            }
        }, new UploadOptions(null, null, false, new UpProgressHandler() {
            @Override
            public void progress(String key, double percent) {
                Message obtain = Message.obtain();
                obtain.what = IMAG_PROGRESS;
                mHandler.sendMessage(obtain);
            }
        }, null));

    }

    private void sendMessage(int what) {
        Message msg = Message.obtain();
        msg.arg1 = mAnInt;
        msg.what = what;
        mHandler.sendMessage(msg);
    }
}
