package com.module.commonview.module.bean;

public class TaoShare {
	private String code;
	private String message;
	private TaoShareData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TaoShareData getData() {
		return data;
	}

	public void setData(TaoShareData data) {
		this.data = data;
	}

}
