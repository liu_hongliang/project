package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseProjectPopupwindows2;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.adapter.DoctorListAdpter;
import com.module.doctor.controller.api.DoctorListApi;
import com.module.doctor.model.api.PartDataApi;
import com.module.doctor.model.bean.DocListData;
import com.module.doctor.model.bean.PartAskData;
import com.module.home.model.bean.ProjectDetailsListData;
import com.module.home.view.LoadingProgress;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.view.DropDownListView;
import com.quicklyask.view.DropDownListView.OnDropDownListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.utils.SystemTool;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 医生列表页
 *
 * @author Rubin
 */
public class TabDocListFragActivity extends FragmentActivity {

    private final String TAG = "TabDocListFragActivity";

    private TabDocListFragActivity mContex;

    // List
    private DropDownListView mlist;
    private int mCurPage = 1;
    private Handler mHandler;
    private List<DocListData> lvHotIssueData = new ArrayList<>();
    private List<DocListData> lvHotIssueMoreData = new ArrayList<>();
    private DoctorListAdpter hotAdpter;
    private String mCity = "全国";

    private LinearLayout nodataTv;// 数据为空时候的显示

    private RelativeLayout cityRly;
    private RelativeLayout partRly;
    private RelativeLayout sortRly;
    private RelativeLayout kindRly;
    private TextView cityTv;
    private TextView partTv;
    private TextView sortTv;
    private TextView kindTv;
    private ImageView cityIv;
    private ImageView partIv;
    private ImageView sortIv;
    private ImageView kindIv;


    private List<TaoPopItemData> lvSortData = new ArrayList<>();
    private List<TaoPopItemData> lvKindData = new ArrayList<>();
    private String sortStr = "1";// 筛选
    private String kindStr = "0";

    private BaseCityPopwindows cityPop;
    private BaseProjectPopupwindows2 projectPop;
    private BaseSortPopupwindows sortPop;
    private BaseSortPopupwindows kindPop;

    private String partId = "0";

    public static int mPosition;

    private LinearLayout nowifiLy;
    private Button refreshBt;
    private List<PartAskData> lvGroupData;
    private LoadingProgress mDialog;
    private DoctorListApi mDctorListApi;
    private HashMap<String, Object> lodHotIssueDatMap = new HashMap<>();
    private ProjectDetailsListData screen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acty_tab_doc_select);
        mContex = TabDocListFragActivity.this;
        mDialog = new LoadingProgress(mContex);
        mDctorListApi = new DoctorListApi();

        findView();
        initList();
        setListner();

        screen = getIntent().getParcelableExtra("screen");
        if (screen != null) {
            partId = screen.getId();
            partTv.setText(screen.getName());
        }

        mCity = Cfg.loadStr(mContex, FinalConstant.DWCITY, "");
        if (mCity.length() > 0) {
            if (mCity.equals("失败")) {
                mCity = "全国";
            } else {
            }
        } else {
            mCity = "全国";
        }
        cityTv.setText(mCity);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);

        if (SystemTool.checkNet(mContex)) {
            nowifiLy.setVisibility(View.GONE);
        } else {
            nowifiLy.setVisibility(View.VISIBLE);
        }

        refreshBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (SystemTool.checkNet(mContex)) {
                            nowifiLy.setVisibility(View.GONE);
                            initList();
                            setListner();
                        } else {
                            nowifiLy.setVisibility(View.VISIBLE);
                            ViewInject.toast(getResources().getString(
                                    R.string.no_wifi_tips));
                        }
                    }
                }.start();
            }
        });
    }

    void findView() {
        mlist = findViewById(R.id.my_doc_list_view);


        partRly = findViewById(R.id.project_part_pop_rly1);
        sortRly = findViewById(R.id.project_sort_pop_rly);
        kindRly = findViewById(R.id.project_kind_pop_rly);
        cityRly = findViewById(R.id.project_diqu_pop_rly);
        partTv = findViewById(R.id.project_part_pop_tv);
        sortTv = findViewById(R.id.project_sort_pop_tv);
        kindTv = findViewById(R.id.project_kind_pop_tv);
        cityTv = findViewById(R.id.project_diqu_pop_tv);
        cityIv = findViewById(R.id.project_diqu_pop_iv);
        partIv = findViewById(R.id.project_part_pop_iv);
        sortIv = findViewById(R.id.project_sort_pop_iv);
        kindIv = findViewById(R.id.project_kind_pop_iv);

        nodataTv = findViewById(R.id.my_collect_post_tv_nodata);

        nowifiLy = findViewById(R.id.no_wifi_ly1);
        refreshBt = findViewById(R.id.refresh_bt);

        setPopData();

        projectPop = new BaseProjectPopupwindows2(mContex, partRly, lvGroupData);
        cityPop = new BaseCityPopwindows(mContex, partRly);
        sortPop = new BaseSortPopupwindows(mContex, partRly, lvSortData);
        kindPop = new BaseSortPopupwindows(mContex, partRly, lvKindData);

        //城市点击回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {

                Log.e(TAG, "city == " + city);
                mCity = city;

                cityTv.setText(mCity);
                Cfg.saveStr(mContex, FinalConstant.DWCITY, mCity);

                onreshData();

                cityPop.dismiss();
                initpop();
            }
        });

        //智能排序点击回调
        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                sortPop.setAdapter(pos);
                sortStr = lvSortData.get(pos).get_id();
                sortTv.setText(lvSortData.get(pos).getName());
                sortPop.dismiss();
                initpop();
                onreshData();
            }
        });

        //筛选点击回调
        kindPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                kindPop.setAdapter(pos);
                kindStr = lvKindData.get(pos).get_id();
                kindTv.setText(lvKindData.get(pos).getName());
                kindPop.dismiss();
                initpop();
                onreshData();
            }
        });

        loadPartList();
    }

    /**
     * 设置智能排序和筛选所需要的数据
     */
    private void setPopData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("2");
        a2.setName("服务优先");
        TaoPopItemData a3 = new TaoPopItemData();
        a3.set_id("3");
        a3.setName("案例优先");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("4");
        a4.setName("热心优先");
        lvSortData.add(a1);
        lvSortData.add(a2);
        lvSortData.add(a3);
        lvSortData.add(a4);

        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("0");
        a5.setName("不限职称");
        TaoPopItemData a6 = new TaoPopItemData();
        a6.set_id("1");
        a6.setName("住院医师");
        TaoPopItemData a7 = new TaoPopItemData();
        a7.set_id("2");
        a7.setName("主治医师");
        TaoPopItemData a8 = new TaoPopItemData();
        a8.set_id("3");
        a8.setName("副主任医师");
        TaoPopItemData a9 = new TaoPopItemData();
        a9.set_id("4");
        a9.setName("主任医师");
        lvKindData.add(a5);
        lvKindData.add(a6);
        lvKindData.add(a7);
        lvKindData.add(a8);
        lvKindData.add(a9);
    }

    /**
     * 获取项目的接口
     */
    void loadPartList() {
        mPosition = 0;
        new PartDataApi().getCallBack(mContex, new HashMap<String, Object>(), new BaseCallBackListener<List<PartAskData>>() {
            @Override
            public void onSuccess(List<PartAskData> partAskData) {
                lvGroupData = partAskData;
                projectPop.setmData(lvGroupData);
                projectPop.setLeftView();
                projectPop.setRightView(projectPop.getmOnePos());

                projectPop.setOnItemSelectedClickListener(new BaseProjectPopupwindows2.OnItemSelectedClickListener() {
                    @Override
                    public void onItemSelectedClick(String id, String name) {
                        Log.e(TAG, "id == " + id);
                        Log.e(TAG, "name == " + name);
                        partId = id;
                        if (!TextUtils.isEmpty(name)) {
                            partTv.setText(name);
                        }
                        initpop();
                        onreshData();
                    }
                });

                projectPop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss() {
                        initpop();
                    }
                });
            }

        });
    }

    void setListner() {

        kindPop.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        sortPop.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        cityPop.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        projectPop.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                initpop();
            }
        });


        kindRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (kindPop.isShowing()) {
                    kindPop.dismiss();
                } else {
                    kindPop.showPop();
                }
                initpop();
            }
        });

        sortRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (sortPop.isShowing()) {
                    sortPop.dismiss();
                } else {
                    sortPop.showPop();
                }
                initpop();
            }
        });


        cityRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (cityPop.isShowing()) {
                    cityPop.dismiss();
                } else {
                    cityPop.showPop();
                }
                initpop();
            }
        });

        partRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (projectPop.isShowing()) {
                    projectPop.dismiss();
                } else {
                    projectPop.showPop();
                }
                initpop();
            }
        });
    }

    void initpop() {
        if (projectPop.isShowing()) {
            partTv.setTextColor(Color.parseColor("#E95165"));
            partIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            partTv.setTextColor(Color.parseColor("#414141"));
            partIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
        if (sortPop.isShowing()) {
            sortTv.setTextColor(Color.parseColor("#E95165"));
            sortIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            sortTv.setTextColor(Color.parseColor("#414141"));
            sortIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
        if (kindPop.isShowing()) {
            kindTv.setTextColor(Color.parseColor("#E95165"));
            kindIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            kindTv.setTextColor(Color.parseColor("#414141"));
            kindIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
        if (cityPop.isShowing()) {
            cityTv.setTextColor(Color.parseColor("#E95165"));
            cityIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            cityTv.setTextColor(Color.parseColor("#414141"));
            cityIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
    }

    void onreshData() {
        lvHotIssueData = null;
        lvHotIssueMoreData = null;
        mCurPage = 1;
        lodHotIssueData(true);
        mlist.setHasMore(true);
    }

    void initList() {

        mHandler = getHandler();
        lodHotIssueData(true);

        mlist.setOnDropDownListener(new OnDropDownListener() {

            @Override
            public void onDropDown() {
                lvHotIssueData = null;
                lvHotIssueMoreData = null;
                mCurPage = 1;
                lodHotIssueData(true);
                mlist.setHasMore(true);
            }
        });

        mlist.setOnBottomListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                lodHotIssueData(false);
            }
        });

        mlist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos,
                                    long arg3) {

                if (null != lvHotIssueData && lvHotIssueData.size() > 0) {

                    String docId = lvHotIssueData.get(pos).getUser_id();
                    String docName = lvHotIssueData.get(pos).getUsername();

                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "doctor|list_" + mCity + "_" + partId + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, lvHotIssueData.get(pos).getBmsid(), "1"), lvHotIssueData.get(pos).getEvent_params());
                    Intent it0 = new Intent();
                    it0.putExtra("docId", docId);
                    it0.putExtra("docName", docName);
                    it0.putExtra("partId", partId);
                    it0.setClass(mContex, DoctorDetailsActivity592.class);
                    startActivity(it0);
                }
            }
        });
    }

    /**
     * 列表数据联网请求
     *
     * @param isDonwn
     */
    private void lodHotIssueData(final boolean isDonwn) {
        mDialog.startLoading();
        Log.e(TAG, "cateid == " + partId);
        Log.e(TAG, "page == " + mCurPage);
        Log.e(TAG, "sort == " + sortStr);
        Log.e(TAG, "kind == " + kindStr);

        lodHotIssueDatMap.clear();
        lodHotIssueDatMap.put("cateid", partId);
        lodHotIssueDatMap.put("page", mCurPage + "");
        lodHotIssueDatMap.put("sort", sortStr);
        lodHotIssueDatMap.put("kind", kindStr);

        mDctorListApi.getCallBack(mContex, lodHotIssueDatMap, new BaseCallBackListener<List<DocListData>>() {
            @Override
            public void onSuccess(List<DocListData> docListData) {
                Log.e(TAG, "docListData == " + docListData.size());
                mDialog.stopLoading();
                Message msg;
                if (isDonwn) {
                    if (mCurPage == 1) {
                        lvHotIssueData = docListData;

                        msg = mHandler.obtainMessage(1);
                        msg.sendToTarget();
                    }
                } else {
                    lvHotIssueMoreData = docListData;
                    msg = mHandler.obtainMessage(2);
                    msg.sendToTarget();
                }

                mCurPage++;
            }
        });
    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {

        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (lvHotIssueData != null && lvHotIssueData.size() > 0) {

                            nodataTv.setVisibility(View.GONE);
                            mlist.setVisibility(View.VISIBLE);


                            hotAdpter = new DoctorListAdpter(TabDocListFragActivity.this, lvHotIssueData);
                            mlist.setAdapter(hotAdpter);

                            SimpleDateFormat dateFormat = new SimpleDateFormat(
                                    "MM-dd HH:mm:ss");

                            mlist.onDropDownComplete(getString(R.string.update_at)
                                    + dateFormat.format(new Date()));
                            mlist.onBottomComplete();
                        } else {
                            nodataTv.setVisibility(View.VISIBLE);
                            mlist.setVisibility(View.GONE);
                        }
                        break;
                    case 2:
                        if (lvHotIssueMoreData != null
                                && lvHotIssueMoreData.size() > 0) {
                            hotAdpter.add(lvHotIssueMoreData);
                            hotAdpter.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {
                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
