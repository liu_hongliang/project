package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/7.
 */
public class DiaryReplyListPic implements Parcelable {
    private String yuan;
    private String width;
    private String height;

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public DiaryReplyListPic(String yuan) {
        this.yuan = yuan;
    }


    protected DiaryReplyListPic(Parcel in) {
        yuan = in.readString();
    }

    public static final Creator<DiaryReplyListPic> CREATOR = new Creator<DiaryReplyListPic>() {
        @Override
        public DiaryReplyListPic createFromParcel(Parcel in) {
            return new DiaryReplyListPic(in);
        }

        @Override
        public DiaryReplyListPic[] newArray(int size) {
            return new DiaryReplyListPic[size];
        }
    };

    public String getYuan() {
        return yuan;
    }

    public void setYuan(String yuan) {
        this.yuan = yuan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(yuan);
    }
}
