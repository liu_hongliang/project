package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2018/8/15.
 */
public class DiaryTagList implements Parcelable {
    private String id;
    private String url;
    private String name;
    private String jump_type;
    private HashMap<String,String> event_params;
    private String tag_type;

    public DiaryTagList(String id, String url, String name) {
        this.id = id;
        this.url = url;
        this.name = name;
    }

    protected DiaryTagList(Parcel in) {
        id = in.readString();
        url = in.readString();
        name = in.readString();
        jump_type = in.readString();
        tag_type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(url);
        dest.writeString(name);
        dest.writeString(jump_type);
        dest.writeString(tag_type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryTagList> CREATOR = new Creator<DiaryTagList>() {
        @Override
        public DiaryTagList createFromParcel(Parcel in) {
            return new DiaryTagList(in);
        }

        @Override
        public DiaryTagList[] newArray(int size) {
            return new DiaryTagList[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJump_type() {
        return jump_type;
    }

    public void setJump_type(String jump_type) {
        this.jump_type = jump_type;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public String getTag_type() {
        return tag_type;
    }

    public void setTag_type(String tag_type) {
        this.tag_type = tag_type;
    }
}
