package com.module.commonview.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.module.commonview.module.bean.UploadBean;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.util.List;

public class UploadImgView extends RelativeLayout {

    private TextView uploadName;
    private ImageView uploadImage;
    private LinearLayout uploadCover;
    private LinearLayout uploadClick;
    private Button uploadDelete;
    private Activity mContext;
    public static final int FROM_GALLERY = 777;

    public UploadImgView(Activity context) {
        this(context,null);
    }

    public UploadImgView(Activity context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public UploadImgView(Activity context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);

    }

    private void initView(Activity context) {
        mContext=context;
        View view = LayoutInflater.from(context).inflate(R.layout.uplod_imgview, this, true);
        RelativeLayout uploadContainer=view.findViewById(R.id.upload_container);
        uploadClick = view.findViewById(R.id.upload_click);
        uploadName = view.findViewById(R.id.upload_name);
        uploadImage = view.findViewById(R.id.upload_photo_img);
        uploadDelete = view.findViewById(R.id.upload_delete_btn);
        uploadCover = view.findViewById(R.id.upload_bottom_cover);

        int windowW = Cfg.loadInt(context, FinalConstant.WINDOWS_W, 0);
        ViewGroup.LayoutParams layoutParams = uploadContainer.getLayoutParams();
        int weitht=(windowW-Utils.dip2px(54))/3;
        layoutParams.width=weitht;
        layoutParams.height=weitht+Utils.dip2px(4);
        uploadContainer.setLayoutParams(layoutParams);

        uploadClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUploadListener != null){
                    mUploadListener.onUploadListener();
                }
                if (Build.VERSION.SDK_INT >= 23) {

                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            toXIangce(1);

                        }

                        @Override
                        public void onDenied(List<String> permissions) {
//                                    Toast.makeText(mContext, "没有权限", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    toXIangce(1);
                }

            }
        });
        uploadImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUploadListener != null){
                    mUploadListener.onImgListener();
                }

            }
        });
        uploadDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUploadListener != null){
                    mUploadListener.onBtnListener();
                }

            }
        });
    }

    public void setData(UploadBean uploadBean){
        String img = uploadBean.getImg();
        if (!TextUtils.isEmpty(img)){
            uploadClick.setVisibility(GONE);
            uploadImage.setVisibility(VISIBLE);
            uploadName.setText(uploadBean.getName());
            Glide.with(mContext)
                    .load(img)
                    .transform(new CenterCrop(mContext),new GlideRoundTransform(mContext, Utils.dip2px(6)))
                    .placeholder(R.drawable.home_other_placeholder)
                    .error(R.drawable.home_other_placeholder)
                    .into(uploadImage);
            if (uploadBean.isMyself()){
                uploadClick.setClickable(true);
                uploadDelete.setVisibility(VISIBLE);
            }else {
                uploadClick.setClickable(false);
                uploadDelete.setVisibility(GONE);
            }
            if (uploadBean.isIs_cover())uploadCover.setVisibility(VISIBLE);
        }else {
            uploadClick.setVisibility(VISIBLE);
            uploadName.setText(uploadBean.getName());
        }


    }




    public void setmUploadListener(UploadListener mUploadListener) {
        this.mUploadListener = mUploadListener;
    }

    private UploadListener mUploadListener;
    public interface UploadListener{
        void onUploadListener();
        void onImgListener();
        void onBtnListener();
    }

    void toXIangce(int pos) {
        // 启动多个图片选择器
        Intent intent = new Intent(mContext, ImagesSelectorActivity.class);
        // 要选择的图像的最大数量
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
        // 将显示的最小尺寸图像;用于过滤微小的图像(主要是图标)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 50000);
        // 显示摄像机或不
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
        intent.putExtra("pos", pos);
        // 开始选择器
        mContext.startActivityForResult(intent, FROM_GALLERY);

    }
}
