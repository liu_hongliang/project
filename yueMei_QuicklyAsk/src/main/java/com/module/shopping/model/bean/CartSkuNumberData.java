package com.module.shopping.model.bean;

/**
 * Created by 裴成浩 on 2019/2/15
 */
public class CartSkuNumberData {
    private String cart_number;

    public String getCart_number() {
        return cart_number;
    }

    public void setCart_number(String cart_number) {
        this.cart_number = cart_number;
    }
}
