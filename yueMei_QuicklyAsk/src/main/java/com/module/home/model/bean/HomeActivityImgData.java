package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2019/9/20
 */
public class HomeActivityImgData {

    private String img;
    private String desc;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
