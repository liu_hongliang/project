package com.module.my.view.orderpay;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.utils.StatisticalManage;
import com.module.commonview.view.CommonTopBar;
import com.module.my.controller.activity.CreditActivity;
import com.module.my.controller.activity.MyOrdersActivity;
import com.module.my.controller.activity.PlusVipActivity;
import com.module.my.model.api.JiFenStroeApi;
import com.module.my.model.bean.CouponsStatusData;
import com.module.my.view.view.OrderSuccessfulDialog;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.DataUrl;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * 支付状态1 3 支付成功 下单成功
 *
 * @author 裴成浩
 */
public class OrderZhiFuStatus1Activity extends YMBaseActivity {

    private final String TAG = "OrderZhiFuStatus";
    @BindView(R.id.order_zhifu_top)
    CommonTopBar zhifuTop;
    @BindView(R.id.order_status_date_tv)
    TextView zhifuTv;
    @BindView(R.id.zhifu_suess_id_tv)
    TextView serverIdTv;// 服务码
    @BindView(R.id.zhifu_suess_orderid_tv)
    TextView orderIdTv;// 订单号
    @BindView(R.id.server_serverid_ly)
    LinearLayout ifserverIdLy;
    @BindView(R.id.zhifu_success_miaoshu_tv)
    TextView zhifuSuccessTv;
    @BindView(R.id.zhifu_suecc_chakan_fuwuma_rly)
    RelativeLayout chakanwufumaRly;// 查看服务码
    @BindView(R.id.zhifu_tips_1)
    TextView zhifuTips1;
    @BindView(R.id.zhifu_tips_2)
    TextView zhifuTips2;
    @BindView(R.id.finish_bt)
    Button finishBt;
    @BindView(R.id.zhifu_choujiang_click)
    LinearLayout mChoujiangClick;
    @BindView(R.id.zhifu_yanzhi_click)
    LinearLayout mYanzhiClick;
    @BindView(R.id.zhifu_suess_text_code)
    TextView zhifuCodeTv;
    @BindView(R.id.zhifu_success_jifen_tv)
    TextView jifenTv;//颜值币展示
    @BindView(R.id.zhifu_success_fenqi_tv)
    TextView fenqiTipsTv;//颜值币展示
    @BindView(R.id.zhifu_choujiang)
    ImageView bannerJiang;    //抽奖banner

    private String payType;
    private String server_id;
    private String order_id;
    private String pay_type;// 到院支付2 其他1
    private String sku_type;
    private String is_repayment;
    private String is_repayment_mimo;
    private String price;
    private BaseNetWorkCallBackApi getRecommendCouponsStatus;


    @Override
    protected int getLayoutId() {
        return R.layout.acty_zhifu_sueccful;
    }

    @Override
    protected void initView() {

        Bundle data = getIntent().getBundleExtra("data");
        payType = data.getString("payType");
        pay_type = data.getString("pay_type");
        server_id = data.getString("server_id");
        order_id = data.getString("order_id");
        sku_type = data.getString("sku_type");
        is_repayment = data.getString("is_repayment");
        is_repayment_mimo = data.getString("is_repayment_mimo");
        price = data.getString("price");

        Log.e(TAG, "payType == " + payType);
        Log.e(TAG, "pay_type == " + pay_type);
        Log.e(TAG, "server_id == " + server_id);
        Log.e(TAG, "order_id == " + order_id);
        Log.e(TAG, "sku_type == " + sku_type);
        Log.e(TAG, "is_repayment == " + is_repayment);
        Log.e(TAG, "is_repayment_mimo == " + is_repayment_mimo);
        Log.e(TAG, "price == " + price);
        String ngmv = mFunctionManager.loadStr(order_id + FinalConstant.GMV, "0");
        String quantity = mFunctionManager.loadStr(order_id + "quantity", "0");
        Log.e(TAG, "gmv == " + ngmv);
        Log.e(TAG, "quantity == " + quantity);

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("ngmv", ngmv);
        hashMap.put("quantity", quantity);
        hashMap.put("Order_number", order_id);
        StatisticalManage.getInstance().growingIO("Payment_orders", hashMap);

        StatService.onEvent(OrderZhiFuStatus1Activity.this, "002", "支付成功", 1);

        serverIdTv.setText(server_id);
        orderIdTv.setText(order_id);

        if (null != server_id) {
            if (!"0".equals(server_id) && server_id.length() > 0) {
                zhifuCodeTv.setText("查看订单及服务码");
            } else {
                zhifuCodeTv.setText("查看订单及服务码");
            }
        } else {
            zhifuCodeTv.setText("查看订单及服务码");
        }

        if ("1".equals(payType)) {
            ifserverIdLy.setVisibility(View.GONE);
            zhifuTop.setCenterText("预约成功");
            zhifuTv.setText("预约成功");
        } else if ("2".equals(payType)) {

            if ("1".equals(pay_type)) {
                ifserverIdLy.setVisibility(View.VISIBLE);
                zhifuTv.setText("支付成功");
                zhifuTop.setCenterText("支付成功");
            } else {
                ifserverIdLy.setVisibility(View.VISIBLE);
                zhifuTv.setText("下单成功");
                zhifuTop.setCenterText("下单成功");
            }

        } else if ("3".equals(payType) || "4".equals(payType)) {
            if ("2".equals(pay_type)) {
                ifserverIdLy.setVisibility(View.VISIBLE);
                zhifuTv.setText("下单成功");
                zhifuTop.setCenterText("下单成功");
            } else {
                ifserverIdLy.setVisibility(View.VISIBLE);
                zhifuTv.setText("预订成功");
                zhifuTop.setCenterText("预订成功");
            }
        } else if ("55".equals(payType)) {
            ifserverIdLy.setVisibility(View.VISIBLE);
            zhifuTv.setText("下单成功");
            zhifuTop.setCenterText("下单成功");
        }


        if ("2".equals(sku_type)) {
            zhifuSuccessTv.setText("预订成功，医生助理会在1个工作日内联系您确认确认面诊时间与手术事宜，请保持手机的畅通");
            zhifuCodeTv.setText("查看订单详情");
            zhifuTips1.setText("与医生助理确认面诊时间与到院事宜");
            zhifuTips2.setText("预约时间内到院消费，补齐余款");
        }

        jifenTv.setText("成功到院消费并验证服务码后，将获得相应颜值币");

        if (null != is_repayment && "1".equals(is_repayment)) {
            fenqiTipsTv.setVisibility(View.VISIBLE);
            fenqiTipsTv.setText(Html.fromHtml("因信用卡账期限制，请" + "<font color=\"#ff5c77\">" + "24" + "</font>" + "天内，根据收到的短信致电医院预约，到院验证服务码使用。逾期订单自动过期、分期取消。"));
        } else {
            fenqiTipsTv.setVisibility(View.GONE);
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHH");    //设置日期格式
        long curtime = Long.parseLong(df.format(new Date()));
        Log.e(TAG, "curtime == " + curtime);
        if (curtime >= 2017090510 && curtime <= 2017091309) {
            if (Integer.parseInt(price) >= 399) {
                bannerJiang.setVisibility(View.VISIBLE);
                bannerJiang.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = "https://m.yuemei.com/tao_zt/2000.html";

                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                    }
                });
            } else {
                bannerJiang.setVisibility(View.GONE);
            }
        }

        setMultiOnClickListener(chakanwufumaRly, finishBt, mChoujiangClick, mYanzhiClick);
    }

    @Override
    protected void initData() {
        getRecommendCouponsStatus = new BaseNetWorkCallBackApi(FinalConstant1.COUPONS, "getRecommendCouponsStatus");
        loadingDialogData();
    }

    /**
     * 加载弹层数据
     */
    private void loadingDialogData() {
        if (TextUtils.isEmpty(order_id)){
            return;
        }
        Log.e(TAG, "order_id == " + order_id);
        getRecommendCouponsStatus.addData("mainOrderID", order_id);
        getRecommendCouponsStatus.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    CouponsStatusData couponsStatusData = JSONUtil.TransformSingleBean(data.data, CouponsStatusData.class);
                    Log.e(TAG, "data === " + data.toString());

                    if("1".equals(couponsStatusData.getShowStatus())){
                        OrderSuccessfulDialog orderSuccessfulDialog = new OrderSuccessfulDialog(mContext, couponsStatusData.getAlertWebUrl());
                        orderSuccessfulDialog.show();
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.zhifu_suecc_chakan_fuwuma_rly:
                startActivity(new Intent(mContext, MyOrdersActivity.class));
                break;
            case R.id.finish_bt:
                onBackPressed();
                break;
            case R.id.zhifu_choujiang_click:
                Intent intent = new Intent(mContext, PlusVipActivity.class);
                startActivity(intent);
                break;
            case R.id.zhifu_yanzhi_click:
                toJifen();
                break;
        }
    }

    private void toJifen() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("act", "autologin");
        new JiFenStroeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (null != serverData) {
                    Log.d(TAG, "url===>" + serverData.code + "---" + serverData.data);
                    try {
                        DataUrl dataUrl = JSONUtil.TransformSingleBean(serverData.data, DataUrl.class);
                        String url = dataUrl.getUrl();
                        Intent intent = new Intent();
                        intent.setClass(mContext, CreditActivity.class);
                        intent.putExtra("navColor", "#fafafa");    //配置导航条的背景颜色，请用#ffffff长格式。
                        intent.putExtra("titleColor", "#636a76");    //配置导航条标题的颜色，请用#ffffff长格式。
                        intent.putExtra("url", url);    //配置自动登陆地址，每次需服务端动态生成。
                        startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Cfg.saveInt(mContext, order_id + "sl", 0);
        Cfg.saveInt(mContext, order_id + "ymj", 0);
    }
}
