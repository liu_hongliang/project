package com.module.other.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.PageJumpManager;
import com.module.commonview.view.CommonTopBar;
import com.module.other.adapter.MakeLeftAdapter;
import com.module.other.adapter.MakeTopAdapter;
import com.module.other.adapter.RightListAdapter;
import com.module.other.fragment.MakeNewNoteFragment;
import com.module.other.module.api.GetTagDataApi;
import com.module.other.module.bean.MakeTagData;
import com.module.other.module.bean.MakeTagListListData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 日记本详情页
 *
 * @author 裴成浩
 */
public class MakeNewNoteActivity2 extends YMBaseActivity {

    private static final int LABEL_SEARCH = 1;
    @BindView(R.id.rv_left_mu)
    RecyclerView mLeftRecy;
    @BindView(R.id.lv_right_mu)
    ListView rightList;
    @BindView(R.id.rv_list_zhong)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_top_wenan)
    TextView tvTopWenan;
    @BindView(R.id.make_note_top)
    CommonTopBar mTop;
    @BindView(R.id.search_rly_click)
    RelativeLayout searchRlyClick;

    public ArrayList<MakeTagListListData> topData = new ArrayList<>();
    private int mPos = 0;
    private MakeLeftAdapter leftListAdapter;
    private MakeTopAdapter makeTopAdapter;
    public String TAG = "MakeNewNoteActivity2";
    private MakeNewNoteActivity2 mContext = MakeNewNoteActivity2.this;
    private List<MakeTagData> mData;
    private RightListAdapter rightListAdapter;


    //时间选择器
    private PageJumpManager pageJumpManager;
    private String mToDay;              //当前日期，例如：1999-9-25 格式
    private String surgeryDate;         //手术日期，例如：1999-9-25 格式
    private boolean isNoteNext;         //判断下一步是否是跳转到写日记，默认是

    @Override
    protected int getLayoutId() {
        return R.layout.activity_make_new_note2;
    }


    @Override
    protected void initView() {
        pageJumpManager = new PageJumpManager(mContext);

        topData = getIntent().getParcelableArrayListExtra("datas");
        Log.e(TAG, "111topData == " + topData);
        if (topData != null) {
            isNoteNext = false;
        } else {
            isNoteNext = true;
            topData = new ArrayList<>();
        }

        Log.e(TAG, "222topData == " + topData);
        Log.e(TAG, "isNoteNext == " + isNoteNext);


        mTop.setRightTextVisibility(View.VISIBLE);
        if (isNoteNext) {
            MakeNewNoteFragment makeNewNoteFragment = MakeNewNoteFragment.newInstance();
            setActivityFragment(R.id.make_note_fragment, makeNewNoteFragment);
            makeNewNoteFragment.setOnTimeSelectListener(new MakeNewNoteFragment.OnTimeSelectListener() {
                @Override
                public void onTimeSelect(String data) {
                    surgeryDate = data;
                }
            });

            mTop.setCenterText("日记项目类型");
            mTop.setRightText("下一步");

        } else {
            mTop.setCenterText("选择项目类型");
            mTop.setRightText("确认");
            setTopRecyvlerView();
        }

        mToDay = Calendar.getInstance().get(Calendar.YEAR) + "-" + (Calendar.getInstance().get(Calendar.MONTH) + 1) + "-" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        surgeryDate = Calendar.getInstance().get(Calendar.YEAR) + "-" + Calendar.getInstance().get(Calendar.MONTH) + 1 + "-" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                setNextData();
            }
        });

        //搜索的点击事件
        searchRlyClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MakeNewNoteActivity2.this, TagSearchActivity.class);
                startActivityForResult(intent, LABEL_SEARCH);
            }
        });


    }


    @Override
    protected void initData() {
        getTagData();
    }

    /**
     * 下一步/确认的点击
     */
    private void setNextData() {
        if (isNoteNext) {
            long surgeryafterdays = Utils.getDaySub(surgeryDate, mToDay);
            Log.e(TAG, "surgeryafterdays == " + surgeryafterdays);
            if (surgeryafterdays < 9999) {
                Bundle data = getIntent().getBundleExtra("data");
                String cateid = data.getString("cateid");

                for (int i = 0; i < topData.size(); i++) {
                    if (i == 0) {
                        cateid = topData.get(i).getId();
                    } else {
                        cateid = cateid + "," + topData.get(i).getId();
                    }
                }
                HashMap<String, String> mMap = new LinkedHashMap<>();
                mMap.put("cateid", cateid);
                mMap.put("userid", data.getString("userid"));
                mMap.put("hosid", data.getString("hosid"));
                mMap.put("hosname", data.getString("hosname"));
                mMap.put("docname", data.getString("docname"));
                mMap.put("fee", data.getString("fee"));
                mMap.put("taoid", data.getString("taoid"));
                mMap.put("server_id", data.getString("server_id"));
                mMap.put("sharetime", mToDay);
                mMap.put("type", "2");
                mMap.put("noteid", "");
                mMap.put("notetitle", "");
                mMap.put("beforemaxday", "0");
                mMap.put("consumer_certificate", "1");
                mMap.put("noteiv", "");
                mMap.put("notetitle_", "");
                mMap.put("notefutitle", "");
                mMap.put("page", "");

                pageJumpManager.jumpToWriteNoteActivity(PageJumpManager.DEFAULT_LOGO, mMap);

                onBackPressed();
            } else {
                showDialogExitEdit("术后时间不能大于9999天");
            }

        } else {
            ArrayList<MakeTagListListData> mDatas = new ArrayList<>();

            for (MakeTagListListData data : topData) {
                mDatas.add(new MakeTagListListData(data.getId(), data.getName()));
            }

            Intent intent = new Intent();
            intent.putParcelableArrayListExtra("datas", mDatas);
            setResult(100, intent);
            onBackPressed();
        }
    }

    /**
     * 获取标签的个数
     */
    void getTagData() {
        Map<String, Object> keyValues = new HashMap<>();
        new GetTagDataApi().getCallBack(mContext, keyValues, new BaseCallBackListener<List<MakeTagData>>() {
            @Override
            public void onSuccess(List<MakeTagData> serverData) {

                mData = serverData;

                setLeftView();
                setRightView(0);
            }

        });
    }

    /**
     * 设置头部选中的数据
     */
    public void setTopRecyvlerView() {
        if (topData.size() != 0) {        //上边的文案是否显示
            tvTopWenan.setVisibility(View.GONE);
        } else {
            tvTopWenan.setVisibility(View.VISIBLE);
        }

        //设置布局管理器
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MakeNewNoteActivity2.this, LinearLayoutManager.HORIZONTAL, false));
        ((DefaultItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果
        //设置适配器
        makeTopAdapter = new MakeTopAdapter(mContext, topData);
        mRecyclerView.setAdapter(makeTopAdapter);

        //删除按钮
        makeTopAdapter.setOnItemDeleteClickListener(new MakeTopAdapter.onItemDeleteListener() {
            @Override
            public void onDeleteClick(int pos) {
                topData.remove(pos);
                setTopRecyvlerView();
                setRightView(mPos);
            }
        });
    }

    /**
     * 设置左边listView的数据
     */
    private void setLeftView() {
        //设置布局管理器
        mLeftRecy.setLayoutManager(new LinearLayoutManager(MakeNewNoteActivity2.this, LinearLayoutManager.VERTICAL, false));
        ((DefaultItemAnimator) mLeftRecy.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果

        //设置适配器
        leftListAdapter = new MakeLeftAdapter(mContext, mData, mPos, Utils.dip2px(mContext, 50));
        mLeftRecy.setAdapter(leftListAdapter);

        // 设置点击某条的监听
        leftListAdapter.setOnItemClickListener(new MakeLeftAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                if (mPos != pos) {
                    int typePos = mPos;         //之前选中的

                    mPos = pos;

                    leftListAdapter.setmPos(mPos);
                    leftListAdapter.notifyItemChanged(mPos);

                    leftListAdapter.notifyItemChanged(typePos);

                    setRightView(pos);
                }
            }
        });

    }

    /**
     * 设置右边listView的数据
     *
     * @param pos
     */
    private void setRightView(int pos) {
        rightListAdapter = new RightListAdapter(mContext, mData.get(pos).getList());
        rightList.setAdapter(rightListAdapter);
    }


    /**
     * 搜索回调
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LABEL_SEARCH) {
            if (data != null) {
                String id = data.getStringExtra("id");
                String name = data.getStringExtra("name");
                if (topData.size() == 3) {
                    Toast.makeText(MakeNewNoteActivity2.this, "最多可以选择3个项目", Toast.LENGTH_SHORT).show();
                } else {
                    if (id.length() > 0 && name.length() > 0) {
                        MakeTagListListData bean = new MakeTagListListData(id, name);
                        topData.add(bean);
                        setTopRecyvlerView();
                        setRightView(mPos);
                    }
                }
            }
        }
    }

    private void showDialogExitEdit(String content) {
        final EditExitDialog editDialog = new EditExitDialog(mContext,
                R.style.mystyle, R.layout.dilog_newuser_yizhuce1);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView trueText = editDialog.findViewById(R.id.dialog_exit_content_tv);
        trueText.setText(content);
        Button trueBt = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt.setText("确定");
        trueBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });

    }
}

