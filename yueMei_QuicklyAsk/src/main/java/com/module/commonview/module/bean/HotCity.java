/**
 * 
 */
package com.module.commonview.module.bean;

import com.module.doctor.model.bean.CityDocDataitem;

import java.util.List;

/**
 * 热门城市
 * 
 * @author Robin
 * 
 */
public class HotCity {

	private String code;
	private String message;
	private List<CityDocDataitem> data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public List<CityDocDataitem> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<CityDocDataitem> data) {
		this.data = data;
	}

}
