/**
 * 
 */
package com.module.community.model.bean;

import java.util.HashMap;

/**
 * @author lenovo17
 * 
 */
public class ZhuanTi {


	private String type;
	private String link;
	private String title;
	private String img;
	private String img_new;
	private String appmurl;
	private String _id;
	private String flag;
	private HashMap<String,String> event_params;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getImg_new() {
		return img_new;
	}

	public void setImg_new(String img_new) {
		this.img_new = img_new;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getAppmurl() {
		return appmurl;
	}

	public void setAppmurl(String appmurl) {
		this.appmurl = appmurl;
	}

	public HashMap<String,String> getEvent_params() {
		return event_params;
	}

	public void setEvent_params(HashMap<String,String> event_params) {
		this.event_params = event_params;
	}
}
