package com.quicklyask.entity;

import java.util.List;

/**
 * Created by dwb on 16/11/25.
 */
public class CityC {

    private String code;
    private String message;
    private List<CityCData> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CityCData> getData() {
        return data;
    }

    public void setData(List<CityCData> data) {
        this.data = data;
    }
}
