package com.module.home.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.base.view.FunctionManager;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.ProjectFocuMapData;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/4/3
 */
public class ProjectDetailsMetrosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mContext;
    private final List<List<ProjectFocuMapData>> mDatas;
    private final int mPos;
    private final LayoutInflater mInflater;
    private final FunctionManager mFunctionManager;

    private final int ITEM_TYPE_ONE = 1;
    private final int ITEM_TYPE_TOW = 2;
    private final int ITEM_TYPE_FOUR = 4;
    private final int windowsWight;
    private String TAG = "ProjectDetailsMetrosAdapter";

    public ProjectDetailsMetrosAdapter(Activity context, List<List<ProjectFocuMapData>> datas, int pos) {
        this.mContext = context;
        this.mDatas = datas;
        this.mPos = pos;
        mInflater = LayoutInflater.from(context);
        mFunctionManager = new FunctionManager(mContext);

        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @Override
    public int getItemViewType(int position) {
        switch (mDatas.get(position).size()) {
            case 1:
                return ITEM_TYPE_ONE;
            case 2:
                return ITEM_TYPE_TOW;
            case 4:
            default:
                return ITEM_TYPE_FOUR;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_ONE:
                return new ViewHolder1(mInflater.inflate(R.layout.item_project_details_metros_one, parent, false));
            case ITEM_TYPE_TOW:
                return new ViewHolder2(mInflater.inflate(R.layout.item_project_details_metros_two, parent, false));
            case ITEM_TYPE_FOUR:
            default:
                return new ViewHolder2(mInflater.inflate(R.layout.item_project_details_metros_four, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder1) {
            setImgView1((ViewHolder1) holder, position);
        } else if (holder instanceof ViewHolder2) {
            setImgView2((ViewHolder2) holder, position);
        } else if (holder instanceof ViewHolder4) {
            setImgView4((ViewHolder4) holder, position);

        }
    }

    private void setImgView1(final ViewHolder1 holder, final int position) {
        List<ProjectFocuMapData> metroTopData = mDatas.get(position);

        Log.e(TAG, "1111metroTopData.get(0).getImg() == " + metroTopData.get(0).getImg());
        Glide.with(mContext).load(metroTopData.get(0).getImg())
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                int intrinsicWidth = resource.getIntrinsicWidth();
                int intrinsicHeight = resource.getIntrinsicHeight();

                int mImageHeight = (windowsWight * intrinsicHeight) / intrinsicWidth;
                ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.merroTopOne.getLayoutParams();
                layoutParams.height = mImageHeight;
                holder.merroTopOne.setLayoutParams(layoutParams);

                holder.merroTopOne.setImageDrawable(resource);
            }
        });
    }

    private void setImgView2(final ViewHolder2 holder, int position) {
        List<ProjectFocuMapData> metroTopData = mDatas.get(position);
        final ImageView[] merroTopTwos = holder.merroTopTwo;

        for (int i = 0; i < merroTopTwos.length; i++) {

            final ImageView merroTopTwo = merroTopTwos[i];

            Log.e(TAG, "2222metroTopData.get(" + i + ").getImg() == " + metroTopData.get(i).getImg());

            Glide.with(mContext).load(metroTopData.get(i).getImg())
                    .placeholder(R.drawable.home_other_placeholder)
                    .error(R.drawable.home_other_placeholder).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    int intrinsicWidth = resource.getIntrinsicWidth();
                    int intrinsicHeight = resource.getIntrinsicHeight();

                    int mImageHeight = ((windowsWight / 2) * intrinsicHeight) / intrinsicWidth;
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) merroTopTwo.getLayoutParams();
                    layoutParams.height = mImageHeight;
                    merroTopTwo.setLayoutParams(layoutParams);

                    merroTopTwo.setImageDrawable(resource);
                }
            });
        }

        //判断是否有分割线
        if ("1".equals(metroTopData.get(0).getMetro_line())) {
            holder.merroTowDivder.setVisibility(View.VISIBLE);
        } else {
            holder.merroTowDivder.setVisibility(View.GONE);
        }
    }


    private void setImgView4(ViewHolder4 holder, int position) {
        List<ProjectFocuMapData> metroTopData = mDatas.get(position);
        ImageView[] merroTopFours = holder.merroTopFour;
        View[] merroFourDivder = holder.merroFourDivder;

        for (int i = 0; i < merroTopFours.length; i++) {

            final ImageView merroTopFour = merroTopFours[i];

            Log.e(TAG, "4444metroTopData.get(" + i + ").getImg() == " + metroTopData.get(i).getImg());

            Glide.with(mContext).load(metroTopData.get(i).getImg())
                    .placeholder(R.drawable.home_other_placeholder)
                    .error(R.drawable.home_other_placeholder).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    int intrinsicWidth = resource.getIntrinsicWidth();
                    int intrinsicHeight = resource.getIntrinsicHeight();

                    int mImageHeight = ((windowsWight / 4) * intrinsicHeight) / intrinsicWidth;
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) merroTopFour.getLayoutParams();
                    layoutParams.height = mImageHeight;
                    merroTopFour.setLayoutParams(layoutParams);

                    merroTopFour.setImageDrawable(resource);
                }
            });

        }

        //判断是否有分割线
        for (int i = 0; i < merroFourDivder.length; i++) {
            if ("1".equals(metroTopData.get(i).getMetro_line())) {
                holder.merroFourDivder[i].setVisibility(View.VISIBLE);
            } else {
                holder.merroFourDivder[i].setVisibility(View.GONE);
            }
        }
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder {

        private ImageView merroTopOne;

        public ViewHolder1(@NonNull View itemView) {
            super(itemView);
            merroTopOne = itemView.findViewById(R.id.item_details_metros_one);

            merroTopOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<ProjectFocuMapData> metroTopData = mDatas.get(getLayoutPosition());
                    String url = metroTopData.get(0).getUrl();
                    HashMap<String, String> event_params = metroTopData.get(0).getEvent_params();
                    if (metroTopData.size() > 0 && !TextUtils.isEmpty(url)) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_METRO, (mPos + 1) + "-" + (getLayoutPosition() + 1)),event_params);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                }
            });
        }
    }

    public class ViewHolder2 extends RecyclerView.ViewHolder {

        private ImageView[] merroTopTwo = new ImageView[2];
        private final View merroTowDivder;

        public ViewHolder2(@NonNull View itemView) {
            super(itemView);
            merroTopTwo[0] = itemView.findViewById(R.id.item_details_metros_two1);
            merroTopTwo[1] = itemView.findViewById(R.id.item_details_metros_two2);
            merroTowDivder = itemView.findViewById(R.id.item_details_metros_two_divider);

            for (int i = 0; i < merroTopTwo.length; i++) {
                final int finalI = i;
                merroTopTwo[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        List<ProjectFocuMapData> metroTopData = mDatas.get(getLayoutPosition());
                        String url = metroTopData.get(finalI).getUrl();
                        if (metroTopData.size() > 0 && !TextUtils.isEmpty(url)) {
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_METRO, (mPos + 1) + "-" + (getLayoutPosition() + 1)));
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                        }
                    }
                });
            }
        }
    }

    public class ViewHolder4 extends RecyclerView.ViewHolder {
        private ImageView[] merroTopFour = new ImageView[4];
        private View[] merroFourDivder = new View[3];

        public ViewHolder4(@NonNull View itemView) {
            super(itemView);

            merroTopFour[0] = itemView.findViewById(R.id.item_details_metros_four1);
            merroTopFour[1] = itemView.findViewById(R.id.item_details_metros_four2);
            merroTopFour[2] = itemView.findViewById(R.id.item_details_metros_four3);
            merroTopFour[3] = itemView.findViewById(R.id.item_details_metros_four4);
            merroFourDivder[0] = itemView.findViewById(R.id.item_details_metros_two_divider1);
            merroFourDivder[1] = itemView.findViewById(R.id.item_details_metros_two_divider2);
            merroFourDivder[2] = itemView.findViewById(R.id.item_details_metros_two_divider3);

            for (int i = 0; i < merroTopFour.length; i++) {
                final int finalI = i;
                merroTopFour[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        List<ProjectFocuMapData> metroTopData = mDatas.get(getLayoutPosition());
                        String url = metroTopData.get(finalI).getUrl();
                        if (metroTopData.size() > 0 && !TextUtils.isEmpty(url)) {
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_METRO, (mPos + 1) + "-" + (getLayoutPosition() + 1)));
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                        }
                    }
                });
            }
        }
    }
}
