package com.module.my.model.bean;

/**
 * 视频封面数据
 * Created by 裴成浩 on 2018/8/15.
 */
public class PostingVideoCover {
    private String img;
    private int width;
    private int height;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
