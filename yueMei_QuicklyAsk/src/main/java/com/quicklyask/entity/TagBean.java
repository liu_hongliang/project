package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class TagBean implements Parcelable {


    /**
     * id : 78
     * name : 眼部
     * img :
     * event_params : {"event_name":"cold_start_interest","type":0,"event_pos":1}
     */

    private int id;
    private String name;
    private String img;
    private HashMap<String,String> event_params;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public HashMap<String,String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String,String> event_params) {
        this.event_params = event_params;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.img);
        dest.writeSerializable(this.event_params);
    }

    public TagBean() {
    }

    protected TagBean(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.img = in.readString();
        this.event_params = (HashMap) in.readSerializable();
    }

    public static final Parcelable.Creator<TagBean> CREATOR = new Parcelable.Creator<TagBean>() {
        @Override
        public TagBean createFromParcel(Parcel source) {
            return new TagBean(source);
        }

        @Override
        public TagBean[] newArray(int size) {
            return new TagBean[size];
        }
    };
}
