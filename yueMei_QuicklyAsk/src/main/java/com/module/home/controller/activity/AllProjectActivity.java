package com.module.home.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.home.controller.adapter.AllProjectLeftAdapter;
import com.module.home.controller.adapter.AllProjectRightAdapter;
import com.module.home.model.api.ChannelAllpartApi;
import com.module.other.module.bean.MakeTagData;
import com.module.other.module.bean.MakeTagListData;
import com.quicklyask.activity.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 全部项目
 *
 * @author 裴成浩
 */
public class AllProjectActivity extends YMBaseActivity {

    @BindView(R.id.all_project_left_rl)
    RecyclerView mLeftRecy;
    @BindView(R.id.all_project_right_lv)
    RecyclerView rightRecy;
    private List<MakeTagData> mData;

    private AllProjectLeftAdapter leftListAdapter;
    private AllProjectRightAdapter rightListAdapter;


    private String homeSource;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_all_project;
    }

    @Override
    protected void initView() {
        homeSource = getIntent().getStringExtra("home_source");
    }

    @Override
    protected void initData() {
        getTagData();
    }

    /**
     * 获取标签的个数
     */
    void getTagData() {
        Map<String, Object> keyValues = new HashMap<>();
        new ChannelAllpartApi().getCallBack(mContext, keyValues, new BaseCallBackListener<List<MakeTagData>>() {
            @Override
            public void onSuccess(List<MakeTagData> serverData) {
                mData = serverData;
                setLeftView();
                setRightView(0);
            }
        });
    }

    /**
     * 设置左边listView的数据
     */
    private void setLeftView() {
        //设置布局管理器
        mLeftRecy.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        if (mLeftRecy.getItemAnimator() != null) {
            ((DefaultItemAnimator) mLeftRecy.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果
        }

        //设置适配器
        leftListAdapter = new AllProjectLeftAdapter(mContext, mData);
        mLeftRecy.setAdapter(leftListAdapter);

        // 设置点击某条的监听
        leftListAdapter.setOnItemClickListener(new AllProjectLeftAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                setRightView(pos);
            }
        });

    }

    /**
     * 设置右边listView的数据
     *
     * @param pos
     */
    private void setRightView(int pos) {
        List<MakeTagListData> list = mData.get(pos).getList();

        if (rightListAdapter == null) {
            //设置布局管理器
            rightRecy.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            //设置适配器
            Log.e(TAG, "mData.get(pos).getId() == " + mData.get(pos).getId());
            rightListAdapter = new AllProjectRightAdapter(mContext, list,mData.get(pos).getId());
            rightRecy.setAdapter(rightListAdapter);

            rightListAdapter.setItemClickListener(new AllProjectRightAdapter.ItemClickListener() {
                @Override
                public void onItemClick(String level, String id, String name, String selected_id,HashMap<String, String> params) {
                    Log.e(TAG, "level === " + level);
                    switch (level) {
                        case "2":
                            //部位
                            Intent intent2 = new Intent(mContext, ChannelPartsActivity.class);
                            intent2.putExtra("id", id);
                            intent2.putExtra("title", name);
                            intent2.putExtra("home_source", homeSource);
                            startActivity(intent2);
                            break;
                        case "3":
                            //二级
                            Intent intent3 = new Intent(mContext, ChannelTwoActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(ChannelTwoActivity.TWO_ID, id);
                            bundle.putString(ChannelTwoActivity.TITLE, name);
                            bundle.putString(ChannelTwoActivity.SELECTED_ID, selected_id);
                            bundle.putString(ChannelTwoActivity.HOME_SOURCE, homeSource);
                            intent3.putExtra("data", bundle);
                            startActivity(intent3);
                            break;
                        case "4":
                            //四级
                            Intent intent4 = new Intent(mContext, ChannelFourActivity.class);
                            intent4.putExtra("id", id);
                            intent4.putExtra("title", name);
                            intent4.putExtra("home_source", homeSource);
                            startActivity(intent4);
                            break;
                    }
                }

            });
        } else {
            rightListAdapter.setData(list,mData.get(pos).getId());
        }
    }

}
