package com.module.my.model.bean;

import android.support.annotation.NonNull;

import java.util.List;

public class ProjcetData {

//    private List<ProjcetSXitem> kind;
//    private List<ProjcetSXitem> method;

    private int id;
    private String name;
    private List<ProjcetList> list;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProjcetList> getList() {
        return list;
    }

    public void setList(List<ProjcetList> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
