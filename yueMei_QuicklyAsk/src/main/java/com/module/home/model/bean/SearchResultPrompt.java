package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2019/5/24
 */
public class SearchResultPrompt {
    private String search_key;
    private String tips;

    public SearchResultPrompt(String search_key, String tips) {
        this.search_key = search_key;
        this.tips = tips;
    }

    public String getSearch_key() {
        return search_key;
    }

    public void setSearch_key(String search_key) {
        this.search_key = search_key;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }
}
