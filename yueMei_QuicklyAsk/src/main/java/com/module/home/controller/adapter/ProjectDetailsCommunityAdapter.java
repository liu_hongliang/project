package com.module.home.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.BbsBean;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;
import java.util.List;

/**
 * 频道首页教你避坑适配器
 * Created by 裴成浩 on 2019/4/2
 */
public class ProjectDetailsCommunityAdapter extends RecyclerView.Adapter<ProjectDetailsCommunityAdapter.ViewHolder> {

    private final Context mContext;
    private List<BbsBean> mDatas;
    private final FunctionManager mFunctionManager;

    public ProjectDetailsCommunityAdapter(Context context, List<BbsBean> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mFunctionManager = new FunctionManager(mContext);
    }

    @NonNull
    @Override
    public ProjectDetailsCommunityAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_project_details_community, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) viewHolder.mCommunity.getLayoutParams();
        if (position == 0) {
            params.leftMargin = Utils.dip2px(20);
            params.rightMargin = Utils.dip2px(5);
        } else if (position == mDatas.size() - 1) {
            params.leftMargin = Utils.dip2px(5);
            params.rightMargin = Utils.dip2px(20);
        } else {
            params.leftMargin = Utils.dip2px(5);
            params.rightMargin = Utils.dip2px(5);
        }
        viewHolder.mCommunity.setLayoutParams(params);

        BbsBean data = mDatas.get(position);
        mFunctionManager.setPlaceholderImageSrc(viewHolder.mCommunityImage, data.getImg());
        viewHolder.mCommunityTitle.setText(data.getTitle());
        viewHolder.mCommunityContent.setText(data.getDesc());
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView mCommunity;
        ImageView mCommunityImage;
        TextView mCommunityTitle;
        TextView mCommunityContent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mCommunity = itemView.findViewById(R.id.item_project_details_community);
            mCommunityImage = itemView.findViewById(R.id.item_project_details_community_img);
            mCommunityTitle = itemView.findViewById(R.id.item_project_details_community_title);
            mCommunityContent = itemView.findViewById(R.id.item_project_details_community_content);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BbsBean data = mDatas.get(getLayoutPosition());
                    String url = data.getUrl();
                    HashMap<String, String> event_params = data.getEvent_params();
                    if (!TextUtils.isEmpty(url)) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_BBS, (getLayoutPosition() + 1) + ""),event_params);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                }
            });
        }
    }
}
