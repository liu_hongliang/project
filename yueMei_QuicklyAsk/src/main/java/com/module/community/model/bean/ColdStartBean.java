package com.module.community.model.bean;

import com.quicklyask.entity.BudgetBean;
import com.quicklyask.entity.TagBean;

import java.util.List;

public class ColdStartBean {
    private List<TagBean> tag;
    private List<BudgetBean> budget;

    public List<TagBean> getTag() {
        return tag;
    }

    public void setTag(List<TagBean> tag) {
        this.tag = tag;
    }

    public List<BudgetBean> getBudget() {
        return budget;
    }

    public void setBudget(List<BudgetBean> budget) {
        this.budget = budget;
    }
}
