package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/12.
 */
public class PhotoBrowsListData implements Parcelable {
    private PhotoUserData userdata;
    private String url;
    private PhotoBrowsListTaoData taoData;
    private String title;
    private String content;
    private String prev_id;
    private String next_id;
    private String diary_title;
    private String is_agree;
    private List<PhotoBrowsListPic> pic;

    protected PhotoBrowsListData(Parcel in) {
        userdata = in.readParcelable(PhotoUserData.class.getClassLoader());
        url = in.readString();
        taoData = in.readParcelable(PhotoBrowsListTaoData.class.getClassLoader());
        title = in.readString();
        content = in.readString();
        prev_id = in.readString();
        next_id = in.readString();
        diary_title = in.readString();
        is_agree = in.readString();
        pic = in.createTypedArrayList(PhotoBrowsListPic.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(userdata, flags);
        dest.writeString(url);
        dest.writeParcelable(taoData, flags);
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(prev_id);
        dest.writeString(next_id);
        dest.writeString(diary_title);
        dest.writeString(is_agree);
        dest.writeTypedList(pic);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PhotoBrowsListData> CREATOR = new Creator<PhotoBrowsListData>() {
        @Override
        public PhotoBrowsListData createFromParcel(Parcel in) {
            return new PhotoBrowsListData(in);
        }

        @Override
        public PhotoBrowsListData[] newArray(int size) {
            return new PhotoBrowsListData[size];
        }
    };

    public PhotoUserData getUserdata() {
        return userdata;
    }

    public void setUserdata(PhotoUserData userdata) {
        this.userdata = userdata;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public PhotoBrowsListTaoData getTaoData() {
        return taoData;
    }

    public void setTaoData(PhotoBrowsListTaoData taoData) {
        this.taoData = taoData;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPrev_id() {
        return prev_id;
    }

    public void setPrev_id(String prev_id) {
        this.prev_id = prev_id;
    }

    public String getNext_id() {
        return next_id;
    }

    public void setNext_id(String next_id) {
        this.next_id = next_id;
    }

    public String getDiary_title() {
        return diary_title;
    }

    public void setDiary_title(String diary_title) {
        this.diary_title = diary_title;
    }

    public String getIs_agree() {
        return is_agree;
    }

    public void setIs_agree(String is_agree) {
        this.is_agree = is_agree;
    }

    public List<PhotoBrowsListPic> getPic() {
        return pic;
    }

    public void setPic(List<PhotoBrowsListPic> pic) {
        this.pic = pic;
    }
}
