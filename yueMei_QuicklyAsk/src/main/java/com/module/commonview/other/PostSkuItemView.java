package com.module.commonview.other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.view.CenterAlignImageSpan;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/7/10
 */
public class PostSkuItemView {
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final String mDiaryId;
    private final FunctionManager mFunctionManager;
    private final String mTagTitle;
    private String TAG = "PostSkuItemView";

    public PostSkuItemView(Context context, String diaryId, String tagTitle) {
        this.mContext = context;
        this.mDiaryId = diaryId;
        this.mTagTitle = tagTitle;
        mInflater = LayoutInflater.from(mContext);
        mFunctionManager = new FunctionManager(mContext);
    }

    /**
     * 初始化下单商品
     */
    @SuppressLint("SetTextI18n")
    public List<View> initTaoData(List<HomeTaoData> diaryTaoDatas) {
        List<View> views = new ArrayList<>();

        for (int i = 0; i < diaryTaoDatas.size(); i++) {
            final HomeTaoData taoData = diaryTaoDatas.get(i);
            View view = mInflater.inflate(R.layout.item_post_ask_list_view, null);
            TextView tv_title = view.findViewById(R.id.tv_title);

            ImageView skuImg = view.findViewById(R.id.diary_list_goods_sku_img);
            TextView viewContent = view.findViewById(R.id.diary_list_goods_title);
            TextView price = view.findViewById(R.id.diary_list_goods_price);
            TextView originalPrice = view.findViewById(R.id.diary_list_goods_original_price);
            RelativeLayout plusPriceVibiable = view.findViewById(R.id.diary_list_plus_vibiable);
            TextView plusPrice = view.findViewById(R.id.plus_vip_price);
            TextView btn = view.findViewById(R.id.diary_list_goods_btn);

            LinearLayout ll_posts = view.findViewById(R.id.post_list_goods_doc_click);
            TextView post_title = view.findViewById(R.id.post_title);

//            ImageView portrait1 = view.findViewById(R.id.post_ask_list_portrait1);
//            ImageView portrait2 = view.findViewById(R.id.post_ask_list_portrait2);
//            ImageView portrait3 = view.findViewById(R.id.post_ask_list_portrait3);
//            TextView title = view.findViewById(R.id.post_ask_list_title);
//            ImageView img = view.findViewById(R.id.post_ask_list_img);
//            TextView describe = view.findViewById(R.id.post_ask_list_describe);
//            TextView hosName = view.findViewById(R.id.post_ask_list_hos);
//            TextView price = view.findViewById(R.id.post_ask_list_price);
//            TextView unit = view.findViewById(R.id.post_ask_list_unit);
//            LinearLayout vipPriceContainer = view.findViewById(R.id.post_ask_list_vip_price_container);
//            TextView vipPrice = view.findViewById(R.id.post_ask_list_vip_price);
//            TextView chatBtn = view.findViewById(R.id.post_ask_list_btn);
//
//            //马甲号头像设置
//            List<String> userImg = taoData.getUserImg();
//            mFunctionManager.setCircleImageSrc(portrait1, userImg.get(0));
//            mFunctionManager.setCircleImageSrc(portrait2, userImg.get(1));
//            mFunctionManager.setCircleImageSrc(portrait3, userImg.get(2));
//
//            //设置文案
//            title.setText(taoData.getRate() + "此商品解决" + mTagTitle + "问题");
//            //设置SKU图片
//            mFunctionManager.setRoundImageSrc(img, taoData.getImg(), Utils.dip2px(4));
//            //设置SKU描述
//            describe.setText(taoData.getTitle());
//            //SKU所在的医院
//            hosName.setText(taoData.getHos_name());
//            //设置SKU价格
//            price.setText(taoData.getPrice_discount());
//            //SKU单位
//            unit.setText(taoData.getFeeScale());
//
//            //判断是否是会员
//            String member_price = taoData.getMember_price();//plus会员价
//            if (Integer.parseInt(member_price) > 0) {
//                vipPriceContainer.setVisibility(View.VISIBLE);
//                vipPrice.setText("¥" + member_price);
//            } else {
//                vipPriceContainer.setVisibility(View.GONE);
//            }
//
//            //咨询点击
//            chatBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Utils.isLoginAndBind(mContext)) {
//                        ItemClick(taoData);
//                    }
//                }
//            });
//            if(!TextUtils.isEmpty(taoData.getRate()) && !TextUtils.isEmpty(mTagTitle)){
            if(!TextUtils.isEmpty(taoData.getRate())){
                tv_title.setText(taoData.getRate() + "此商品,解决" + mTagTitle + "问题");
            }else {
                tv_title.setVisibility(View.INVISIBLE);
            }

            //SKU图片优化
            mFunctionManager.setRoundImageSrc(skuImg, taoData.getImg(), Utils.dip2px(5));

            //SKU文案
            if ("1".equals(taoData.getImg66())) {
                SpannableString spanString = new SpannableString("大促" + taoData.getSubtitle());
                CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(mContext, Utils.zoomImage(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.cu_tips_2x), Utils.dip2px(15), Utils.dip2px(15)));
                spanString.setSpan(imageSpan, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                viewContent.setText(spanString);                   //文案
            } else {
                viewContent.setText(taoData.getSubtitle());
            }

            //咨询按钮显示与隐藏
            final String is_rongyun = taoData.getIs_rongyun();
            if ("3".equals(is_rongyun)) {
                btn.setVisibility(View.VISIBLE);
            } else {
                btn.setVisibility(View.GONE);
            }

            //咨询按钮点击
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemClick(taoData);
                    if(onEventClickListener != null){
                        onEventClickListener.onItemClick(v,taoData);
                    }
                }
            });

            String member_price = taoData.getMember_price();
            String oPrice = taoData.getPrice();
            String priceDiscount = taoData.getPrice_discount();
            int memberPrice = Integer.parseInt(member_price);
            int o_price = Integer.parseInt(oPrice);
            int price_discount = Integer.parseInt(priceDiscount);
            price.setText(priceDiscount);
            if (memberPrice >= 0) {
                plusPriceVibiable.setVisibility(View.VISIBLE);
                plusPrice.setText("¥" + member_price);
            } else {
                plusPriceVibiable.setVisibility(View.GONE);
                if (o_price >= 0 && o_price != price_discount) {
                    originalPrice.setVisibility(View.VISIBLE);
                    originalPrice.setText("¥" + o_price);
                    originalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线
                } else {
                    originalPrice.setVisibility(View.GONE);
                }
            }

            if(!TextUtils.isEmpty(taoData.getFanxian())){
                ll_posts.setVisibility(View.VISIBLE);
                mFunctionManager.setTextValue(view, R.id.post_title, taoData.getFanxian());           //说明
            }else{
                ll_posts.setVisibility(View.GONE);
            }

            views.add(view);
        }

        return views;
    }

    /**
     * 咨询点击
     *
     * @param taoData
     */
    private void ItemClick(HomeTaoData taoData) {
        final String hos_userid = taoData.getHos_userid();
        final String taoid = taoData.getId();
        final String title = taoData.getTitle();
        final String price_discount = taoData.getPrice_discount();
        final String member_price = taoData.getMember_price();
        final String list_cover_image = taoData.getImg();

        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                .setDirectId(hos_userid)
                .setObjId(taoid)
                .setObjType("2")
                .setTitle(title)
                .setPrice(price_discount)
                .setImg(list_cover_image)
                .setMemberPrice(member_price)
                .setYmClass("102")
                .setYmId(mDiaryId)
                .build();
        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
    }

    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onItemClick(View view, HomeTaoData taoData);
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}

