package com.module.commonview.module.bean;

public class IfTaoOrderData {
	private String paytype;

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	@Override
	public String toString() {
		return "IfTaoOrderData [paytype=" + paytype + "]";
	}

}
