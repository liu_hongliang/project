package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.baidu.mobstat.StatService;
import com.lzy.okgo.model.HttpParams;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.my.model.api.ModifyMyDataApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.bitmap.KJBitmap;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 提问题、提交体验提交成功 仅出现一次
 *
 * @author Rubin
 */
public class SubmitSuccess1Activity extends BaseActivity {

    private final String TAG = "SubmitSuccess1Activity";

    @BindView(id = R.id.sumbit1_head_iv, click = true)
    private ImageView headView;
    @BindView(id = R.id.sumbit1_name_et)
    private EditText nameEt;
    @BindView(id = R.id.submit1_bt, click = true)
    private Button sumbitBt;

    @BindView(id = R.id.submit_all_content)
    private LinearLayout allContent;// 整个界面容器

    private String nickName;
    private String imgUrl;
    private Context mContext;

    private String filePath;

    private KJBitmap kjb;

    private String qid;
    private String url;

    private ModifyMyDataApi modifyMyDataApi;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_submit_success1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = SubmitSuccess1Activity.this;
        kjb = KJBitmap.create();

        Intent it2 = getIntent();
        qid = it2.getStringExtra("qid");
        url = it2.getStringExtra("url");

        modifyMyDataApi = new ModifyMyDataApi();
        nickName = Cfg.loadStr(mContext, FinalConstant.UNAME, "");
        imgUrl = Cfg.loadStr(mContext, FinalConstant.UHEADIMG, "");
        nameEt.setText(nickName);
        if (imgUrl.length() > 0 && !"".equals(imgUrl)) {
            kjb.display(headView, imgUrl);
        }

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        SubmitSuccess1Activity.this.finish();
                    }
                });

    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.submit1_bt:// 提交
                saveData();
                break;

            case R.id.sumbit1_head_iv:// 拍照片
                new PopupWindows(SubmitSuccess1Activity.this, allContent);
                break;
        }
    }

    private static final int IMAGE_REQUEST_CODE = 0;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int RESIZE_REQUEST_CODE = 2;

    private static final String IMAGE_FILE_NAME = "header.jpg";

    public class PopupWindows extends PopupWindow {

        public PopupWindows(Context mContext, View parent) {

            final View view = View.inflate(mContext,
                    R.layout.item_popupwindows, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.fade_ins));

            setWidth(LayoutParams.FILL_PARENT);
            setHeight(LayoutParams.FILL_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            Button bt1 = view
                    .findViewById(R.id.item_popupwindows_camera);
            Button bt2 = view
                    .findViewById(R.id.item_popupwindows_Photo);
            Button bt3 = view
                    .findViewById(R.id.item_popupwindows_cancel);
            // 拍照
            bt1.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Intent cameraIntent = new Intent(
                            "android.media.action.IMAGE_CAPTURE");

                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            getImageUri());
                    cameraIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);

                    startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
                    dismiss();
                }
            });
            // 点击 之外 消失
            view.setOnTouchListener(new OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {

                    int height = view.findViewById(R.id.ll_popup).getTop();
                    int y = (int) event.getY();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (y < height) {
                            dismiss();
                        }
                    }
                    return true;
                }
            });
            // 相册
            bt2.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);

                    galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);

                    galleryIntent.setDataAndType(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            "image/*");

                    galleryIntent.setType("image/*");

                    startActivityForResult(galleryIntent, IMAGE_REQUEST_CODE);

                    dismiss();
                }
            });
            bt3.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IMAGE_REQUEST_CODE:
                if (data != null) {
                    resizeImage(data.getData());
                }
                break;
            case CAMERA_REQUEST_CODE:
                if (data != null) {
                    resizeImage(getImageUri());
                }
                break;
            case RESIZE_REQUEST_CODE:
                if (data != null) {
                    showResizeImage(data);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void resizeImage(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, RESIZE_REQUEST_CODE);
    }

    private Uri getImageUri() {
        return Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
                IMAGE_FILE_NAME));
    }

    private void showResizeImage(Intent data) {
        Bundle extras = data.getExtras();
        if (extras != null) {
            Bitmap photo = extras.getParcelable("data");
            final String picName = "yuemei" + System.currentTimeMillis();
            FileUtils.saveBitmap(photo, picName);// 保存图片
            filePath = FileUtils.SDPATH + picName + ".JPEG";

            Drawable drawable = new BitmapDrawable(photo);
            headView.setImageDrawable(drawable);
            // headIv.destroyDrawingCache();
            // Bitmap bit = BitmapFactory.decodeFile(filePath);
            // Log.e(TAG, "path==" + filePath + "/bit==" + bit);

            // headIv.setImageBitmap(bit);
        }
    }


    /**
     * 上传资料
     */
    Map<String, Object> maps = new HashMap<>();
    HttpParams params = new HttpParams();

    private void saveData() {

        String nickName = nameEt.getText().toString();

        maps.put("nickname", nickName);

        params.put("nickname", nickName);
        if (filePath != null) {
            params.put("avatar", new File(filePath));
        }

        modifyMyDataApi.getCallBack(mContext, maps, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                try {
                    if ("1".equals(serverData.code)) {
							ViewInject.toast(serverData.message);
							Intent it1 = new Intent();
							it1.putExtra("url", url);
							it1.putExtra("qid", qid);
							it1.setClass(mContext, DiariesAndPostsActivity.class);
							startActivity(it1);
							finish();
						} else {
							ViewInject.toast(serverData.message);
						}
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
