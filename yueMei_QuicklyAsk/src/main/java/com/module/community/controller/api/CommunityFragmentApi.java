package com.module.community.controller.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

import static com.module.other.netWork.netWork.EnumInterfaceType.POST;

/**
 * Created by 裴成浩 on 2018/7/11.
 */
public class CommunityFragmentApi implements BaseCallBackApi {
    private final String action;
    private final String controller;
    private String message;
    private String TAG = "CommunityFragmentApi";

    private HashMap<String, Object> hashMap;  //传值容器

    public CommunityFragmentApi(String controller,String action) {
        this.controller = controller;
        this.action = action;

        NetWork.getInstance().regist(FinalConstant1.HTTPS, FinalConstant1.BASE_URL, controller, action, POST);
        hashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, final Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(controller, action, maps, new ServerCallback() {

            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData.data === " + mData.data);
                message = mData.message;
                if ("1".equals(mData.code)) {
                    listener.onSuccess(mData.data);
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return hashMap;
    }

    public void addData(String key, String value) {
        hashMap.put(key, value);
    }

    public String getMessage() {
        return message;
    }
}