package com.module.shopping.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.view.NumberAddSubView;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.shopping.model.bean.CartDeleteData;
import com.module.shopping.model.bean.CartNumberData;
import com.module.shopping.model.bean.CartSelectedData;
import com.module.shopping.model.bean.ShopCarTaoData;
import com.module.shopping.model.bean.ShoppingCartSkuData;
import com.module.shopping.view.SlideLayout;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.Promotion;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;
import com.quicklyask.view.MyToast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/11/22.
 */
public class ShoppingCartSkuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ShoppingCartSkuData> mDatas;
    private ShoppingOnStateChangeListener mStateChangeListener;
    private LayoutInflater mInflater;
    private String mHosId;
    private String TAG = "ShoppingCartSkuAdapter";

    private final int ITEM_TYPE_FAILURE = 0;                //失效
    private final int ITEM_TYPE_NOT_FAILURE = 1;            //未失效

    public ShoppingCartSkuAdapter(Context context, String hosId, List<ShoppingCartSkuData> datas, ShoppingOnStateChangeListener stateChangeListener) {
        this.mContext = context;
        this.mHosId = hosId;
        this.mDatas = datas;
        this.mStateChangeListener = stateChangeListener;
        mInflater = LayoutInflater.from(mContext);
    }


    @Override
    public int getItemViewType(int position) {
        if ("0".equals(mHosId)) {
            return ITEM_TYPE_FAILURE;
        } else {
            return ITEM_TYPE_NOT_FAILURE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_FAILURE) {
            View itemView = mInflater.inflate(R.layout.shopping_cart_failure_sku_view, parent, false);
            ((SlideLayout) itemView).setOnStateChangeListener(mStateChangeListener);
            return new FailureViewHolder(itemView);
        } else {
            View itemView = mInflater.inflate(R.layout.shopping_cart_sku_view, parent, false);
            ((SlideLayout) itemView).setOnStateChangeListener(mStateChangeListener);
            return new SkuViewHolder(itemView);
        }
    }

    /**
     * 局部刷新
     *
     * @param viewHolder
     * @param position
     * @param payloads
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(viewHolder, position);
        } else {
            for (Object payload : payloads) {
                switch ((String) payload) {
                    case "selected_button":
                        ///选中按钮，且不是失效商品
                        if (viewHolder instanceof SkuViewHolder) {
                            SkuViewHolder holder = (SkuViewHolder) viewHolder;
                            Log.e(TAG, "mDatas.get(pos).getSelected()) pos == " + mDatas.get(position).getSelected());
                            if ("1".equals(mDatas.get(position).getSelected())) {
                                holder.skuHosRadio.setChecked(true);
                            } else {
                                holder.skuHosRadio.setChecked(false);
                            }
                        }
                        break;
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int pos) {
        if (viewHolder instanceof FailureViewHolder) {
            //失效
            setFailureViewHolder((FailureViewHolder) viewHolder, pos);
        } else if (viewHolder instanceof SkuViewHolder) {
            //sku
            setSkuViewHolder((SkuViewHolder) viewHolder, pos);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class SkuViewHolder extends RecyclerView.ViewHolder {
        private SlideLayout cartSkuView;        //布局父容器
        private LinearLayout skuContent;        //列表内容
        private TextView skuMenu;               //删除

        private CheckBox skuHosRadio;           //选中按钮

        private ImageView skuImg;               //SKU图片

        private TextView skuName;               //SKU名称
        private TextView skuDoc;                //sku医生
        private LinearLayout dropPriceClick;    //降价和库存显示
        private TextView dropPrice;             //比加入时下降
        private TextView skuInventory;          //库存
        private TextView skuPrice;              //价格
        private TextView skuUnit;               //单位
        private TextView vipPrice;              //会员价格
        private FlowLayout mFlowLayout;         //标签容器

        private TextView skuDeposit;            //订金价格
        private TextView hosBalace;             //到院支付
        private NumberAddSubView addsubView;    //加减组件

        public SkuViewHolder(@NonNull View itemView) {
            super(itemView);
            cartSkuView = itemView.findViewById(R.id.shopping_cart_sku_view);
            skuContent = itemView.findViewById(R.id.shopping_cart_sku_content);
            skuMenu = itemView.findViewById(R.id.shopping_cart_sku_menu);
            skuHosRadio = itemView.findViewById(R.id.shopping_cart_sku_hos_radio);
            skuImg = itemView.findViewById(R.id.shopping_cart_sku_img);
            skuName = itemView.findViewById(R.id.shopping_cart_sku_name);
            skuDoc = itemView.findViewById(R.id.shopping_cart_sku_doc);
            dropPriceClick = itemView.findViewById(R.id.shopping_cart_sku_drop_price_click);
            dropPrice = itemView.findViewById(R.id.shopping_cart_sku_drop_price);
            skuInventory = itemView.findViewById(R.id.shopping_cart_sku_inventory);
            skuPrice = itemView.findViewById(R.id.shopping_cart_sku_price);
            skuUnit = itemView.findViewById(R.id.shopping_cart_sku_unit);
            vipPrice = itemView.findViewById(R.id.shopping_cart_sku_vip_price);
            mFlowLayout = itemView.findViewById(R.id.shopping_cart_sku_preferential_flowLayout);
            skuDeposit = itemView.findViewById(R.id.shopping_cart_sku_deposit);
            hosBalace = itemView.findViewById(R.id.shopping_cart_sku_hos_balace);
            addsubView = itemView.findViewById(R.id.shopping_cart_sku_addsub_view);

            //点击事件
            skuContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cartSkuView.isOpen()) {
                        cartSkuView.closeMenu();
                    } else {
                        Intent it1 = new Intent();
                        it1.putExtra("id", mDatas.get(getLayoutPosition()).getTao_id());
                        it1.putExtra("source", "93");
                        it1.putExtra("objid", "0");
                        it1.setClass(mContext, TaoDetailActivity.class);
                        mContext.startActivity(it1);
                    }
                }
            });

            //sku选中监听
            skuHosRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.e(TAG, "aaa = " + isChecked);
                    if (buttonView.isPressed()) {
                        itemSelected(getLayoutPosition(), isChecked);
                    }
                }
            });

            //删除按钮点击
            skuMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {

                        CartDeleteData cartDeleteData = new CartDeleteData();
                        cartDeleteData.setCart_id(mDatas.get(getLayoutPosition()).getCart_id());
                        cartDeleteData.setSku_id(mDatas.get(getLayoutPosition()).getTao_id());

                        ArrayList<CartDeleteData> list = new ArrayList<>();
                        list.add(cartDeleteData);
                        onEventClickListener.onDeleteClick(list);
                    }
                }
            });

            //加减按钮
            addsubView.setOnButtonClickListenter(new NumberAddSubView.OnButtonClickListenter() {
                @Override
                public void onButtonAddClick(int value, boolean b) {
                    if (b) {
                        if (onEventClickListener != null) {
                            Log.e(TAG,"onButtonAddClick value ===  "+value);
                            Log.e(TAG,"position ===  "+getLayoutPosition());

                            ShoppingCartSkuData cartSkuData = mDatas.get(getLayoutPosition());
                            cartSkuData.setNumber(value + "");
                            CartNumberData cartNumberData = new CartNumberData();
                            cartNumberData.setCart_id(mDatas.get(getLayoutPosition()).getCart_id());
                            cartNumberData.setFlag("1");
                            cartNumberData.setSkuPos(getLayoutPosition());
                            ShoppingCartSkuData shoppingCartSkuData = mDatas.get(getLayoutPosition());
                            String number = shoppingCartSkuData.getNumber();
                            Log.e(TAG,"number ===  "+number);
                            onEventClickListener.onNumberClick(cartNumberData);
                        }
                    } else {
                        int kucun_number = Integer.parseInt(mDatas.get(getLayoutPosition()).getTao().getKucun_number());        //剩余库存
                        int number = Integer.parseInt(mDatas.get(getLayoutPosition()).getTao().getNumber());                    //最高限购

                        if (kucun_number >= number) {
                            MyToast.makeTextToast2(mContext, "不能再增加啦，最高限购" + number + "件", MyToast.SHOW_TIME).show();
                        } else {
                            MyToast.makeTextToast2(mContext, "不能再增加啦，剩余库存" + kucun_number + "件", MyToast.SHOW_TIME).show();
                        }
                    }
                }

                @Override
                public void onTextViewClick(int value) {
                    if (onEventClickListener != null) {
                        Log.e(TAG,"onTextViewClick value ===  "+value);
                        mDatas.get(getLayoutPosition()).setNumber(value + "");

                        CartNumberData cartNumberData = new CartNumberData();
                        cartNumberData.setCart_id(mDatas.get(getLayoutPosition()).getCart_id());
                        cartNumberData.setFlag("3");
                        cartNumberData.setSkuPos(getLayoutPosition());
                        cartNumberData.setValue(value);
                        onEventClickListener.onNumberClick(cartNumberData);
                    }
                }

                @Override
                public void onButtonSubClick(int value, boolean b) {
                    if (b) {
                        if (onEventClickListener != null) {

                            Log.e(TAG,"onButtonSubClick value ===  "+value);
                            mDatas.get(getLayoutPosition()).setNumber(value + "");

                            CartNumberData cartNumberData = new CartNumberData();
                            cartNumberData.setCart_id(mDatas.get(getLayoutPosition()).getCart_id());
                            cartNumberData.setFlag("2");
                            cartNumberData.setSkuPos(getLayoutPosition());
                            onEventClickListener.onNumberClick(cartNumberData);
                        }
                    } else {
                        int startNumber = Integer.parseInt(mDatas.get(getLayoutPosition()).getTao().getStart_number());
                        if (startNumber >= 1) {
                            MyToast.makeTextToast2(mContext, "不能再减少啦，最低" + startNumber + "件起预订", MyToast.SHOW_TIME).show();
                        } else {
                            MyToast.makeTextToast2(mContext, "不能再减少啦", MyToast.SHOW_TIME).show();
                        }
                    }
                }
            });
        }
    }

    public class FailureViewHolder extends RecyclerView.ViewHolder {
        private SlideLayout cartSkuView;        //布局父容器
        private LinearLayout skuContent;        //列表内容
        private TextView skuMenu;               //删除
        private ImageView skuImg;               //SKU图片
        private TextView skuNameFailure;        //失效SKU名称
        private TextView skuDescribeFailure;    //失效SKU描述

        public FailureViewHolder(@NonNull View itemView) {
            super(itemView);
            cartSkuView = itemView.findViewById(R.id.shopping_cart_failure_sku_view);
            skuContent = itemView.findViewById(R.id.shopping_cart_failure_sku_content);
            skuMenu = itemView.findViewById(R.id.shopping_cart_failure_sku_menu);
            skuImg = itemView.findViewById(R.id.shopping_cart_failure_sku_img);
            skuNameFailure = itemView.findViewById(R.id.shopping_cart_failure_sku_name);
            skuDescribeFailure = itemView.findViewById(R.id.shopping_cart_failure_sku_describe);

            //点击事件
            skuContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cartSkuView.isOpen()) {
                        cartSkuView.closeMenu();
                    } else {
                        Intent it1 = new Intent();
                        it1.putExtra("id", mDatas.get(getLayoutPosition()).getTao_id());
                        it1.putExtra("source", "0");
                        it1.putExtra("objid", "0");
                        it1.setClass(mContext, TaoDetailActivity.class);
                        mContext.startActivity(it1);
                    }
                }
            });

            //删除按钮点击
            Log.e(TAG, "skuMenu === " + skuMenu);
            skuMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        CartDeleteData cartDeleteData = new CartDeleteData();
                        cartDeleteData.setCart_id(mDatas.get(getLayoutPosition()).getCart_id());
                        cartDeleteData.setSku_id(mDatas.get(getLayoutPosition()).getTao_id());

                        ArrayList<CartDeleteData> list = new ArrayList<>();
                        list.add(cartDeleteData);
                        onEventClickListener.onDeleteClick(list);
                    }
                }
            });
        }
    }

    /**
     * 失效的SKU
     *
     * @param viewHolder
     * @param pos
     */
    private void setFailureViewHolder(FailureViewHolder viewHolder, int pos) {
        //布局边界
        if (pos == 0) {
            viewHolder.cartSkuView.setPadding(0, 0, 0, Utils.dip2px(11.5f));
        } else {
            viewHolder.cartSkuView.setPadding(0, Utils.dip2px(11.5f), 0, Utils.dip2px(11.5f));
        }

        ShoppingCartSkuData shoppingCartSkuData = mDatas.get(pos);
        ShopCarTaoData taoData = shoppingCartSkuData.getTao();
        //SKU图片
        Glide.with(mContext).load(taoData.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(4))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.skuImg);
        //SKU名称
        viewHolder.skuNameFailure.setText(taoData.getTitle());
        //SKU描述
        viewHolder.skuDescribeFailure.setText("商品已下架");
    }

    /**
     * SKU信息
     *
     * @param viewHolder
     * @param pos
     */
    @SuppressLint("SetTextI18n")
    private void setSkuViewHolder(SkuViewHolder viewHolder, int pos) {

        ShoppingCartSkuData shoppingCartSkuData = mDatas.get(pos);
        ShopCarTaoData taoData = shoppingCartSkuData.getTao();
        Log.e(TAG,"pos == "+pos);
        Log.e(TAG,"skuNumbr == "+shoppingCartSkuData.getNumber());
        int skuNumbr = Integer.parseInt(shoppingCartSkuData.getNumber());           //购买的数量
        int startSkuNumber = Integer.parseInt(taoData.getStart_number());           //最少购买数量
        int number = Integer.parseInt(taoData.getNumber());                         //最多限购数
        int kucunNumber = Integer.parseInt(taoData.getKucun_number());              //库存数

        float payDiscountPrice = Float.parseFloat(taoData.getPay_price_discount());   //订单价格
        float memberPrice = Float.parseFloat(taoData.getMember_price());              //会员价格
        float discountPrice = Float.parseFloat(taoData.getPrice_discount());          //售卖价格
        float dingjinPrice = Float.parseFloat(taoData.getPay_dingjin());              //订金价格

        //布局边界
        if (pos == 0) {
            viewHolder.cartSkuView.setPadding(0, 0, 0, Utils.dip2px(11.5f));
        } else {
            viewHolder.cartSkuView.setPadding(0, Utils.dip2px(11.5f), 0, Utils.dip2px(11.5f));
        }

        //SKU图片
        Glide.with(mContext).load(taoData.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(4))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.skuImg);
        //SKU描述
        viewHolder.skuName.setText(taoData.getTitle());
        //医生
        if (!TextUtils.isEmpty(taoData.getDoc_name())) {
            viewHolder.skuDoc.setVisibility(View.VISIBLE);
            viewHolder.skuDoc.setText(taoData.getDoc_name());
        } else {
            viewHolder.skuDoc.setVisibility(View.GONE);
        }

        //比加入时价格下降或者库存小于10时 降价和库存显示
        if (!TextUtils.isEmpty(taoData.getDepreciate()) || kucunNumber < 10) {
            viewHolder.dropPriceClick.setVisibility(View.VISIBLE);

            //比加入时下降
            if (!TextUtils.isEmpty(taoData.getDepreciate())) {
                viewHolder.dropPrice.setVisibility(View.VISIBLE);
                viewHolder.dropPrice.setText(taoData.getDepreciate());
            } else {
                viewHolder.dropPrice.setVisibility(View.INVISIBLE);
            }

            //库存
            if (!TextUtils.isEmpty(taoData.getRate())) {
                viewHolder.skuInventory.setVisibility(View.VISIBLE);
                viewHolder.skuInventory.setText(taoData.getRate());
            } else {
                viewHolder.skuInventory.setVisibility(View.INVISIBLE);
            }
        } else {
            viewHolder.dropPriceClick.setVisibility(View.GONE);
        }

        //售卖价格
        viewHolder.skuPrice.setText("¥" + ((int) discountPrice));
        //单位
        viewHolder.skuUnit.setText(taoData.getFeeScale());

        //有会员价格，且会员价格大于售卖价格
        if (memberPrice > 0 && memberPrice < discountPrice) {
            viewHolder.vipPrice.setVisibility(View.VISIBLE);
            viewHolder.vipPrice.setText("¥" + ((int) memberPrice));
        } else {
            viewHolder.vipPrice.setVisibility(View.GONE);
        }

        //第二件折扣设置
        setTagView(viewHolder.mFlowLayout, taoData.getPromotion());

        //加减组件设置

        viewHolder.addsubView.setValue(skuNumbr);
        viewHolder.addsubView.setMinValue(startSkuNumber);

        viewHolder.addsubView.setMaxValue(kucunNumber > number ? number : kucunNumber);

        //订金价格
        viewHolder.skuDeposit.setText("订金 ¥" + dingjinPrice);
        //到院支付
        Log.e(TAG, "payDiscountPrice == " + payDiscountPrice);
        Log.e(TAG, "dingjinPrice == " + dingjinPrice);

        float hosBalace = new BigDecimal(payDiscountPrice - dingjinPrice).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
        viewHolder.hosBalace.setText("到院支付 ¥" + hosBalace);

        //是否是选中状态
        Log.e(TAG, "mDatas.get(pos).getSelected() == " + mDatas.get(pos).getSelected());
        if ("1".equals(mDatas.get(pos).getSelected())) {
            viewHolder.skuHosRadio.setChecked(true);
        } else {
            viewHolder.skuHosRadio.setChecked(false);
        }
    }

    /**
     * 标签设置
     *
     * @param mFlowLayout
     * @param lists
     */
    private void setTagView(FlowLayout mFlowLayout, List<String> lists) {
        if (mFlowLayout.getChildCount() != lists.size()) {
            mFlowLayout.removeAllViews();
            for (int i = 0; i < lists.size(); i++) {
                String promotion = lists.get(i);
                TextView textView = new TextView(mContext);
                textView.setText(promotion);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                textView.setBackgroundResource(R.drawable.shopping_cart_sku_tab_background);
                textView.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                mFlowLayout.addView(textView);
            }
        }
    }

    /**
     * 全部选中,还是全部不选中
     *
     * @param isCheck
     */
    public void allSelected(boolean isCheck) {
        for (int i = 0; i < mDatas.size(); i++) {
            ShoppingCartSkuData data = mDatas.get(i);
            if (isCheck) {
                data.setSelected("1");
            } else {
                data.setSelected("0");
            }

            notifyItemChanged(i, "selected_button");
        }
    }

    /**
     * 某条item选中刷新
     *
     * @param pos
     * @param isCheck
     */
    private void itemSelected(int pos, boolean isCheck) {
        if (isCheck) {
            mDatas.get(pos).setSelected("1");
        } else {
            mDatas.get(pos).setSelected("0");
        }

        CartSelectedData cartSelectedData = new CartSelectedData();
        cartSelectedData.setCart_id(mDatas.get(pos).getCart_id());
        if (isAllCheck()) {
            cartSelectedData.setFlag("2");
            cartSelectedData.setHos_id(mDatas.get(pos).getHos_id());
        } else {
            cartSelectedData.setFlag("0");
        }

        if (isCheck) {
            cartSelectedData.setSelect("1");
        } else {
            cartSelectedData.setSelect("0");
        }

        if (onEventClickListener != null) {
            onEventClickListener.onSelectedClick(isAllCheck(), cartSelectedData);
        }
    }

    /**
     * 删除一条数据
     *
     * @param sku_id
     */
    public void deleteData(String sku_id) {
        for (int i = 0; i < mDatas.size(); i++) {
            ShoppingCartSkuData data = mDatas.get(i);
            if (data.getTao_id().equals(sku_id)) {
                Log.e(TAG, "删除第 == " + i + "个");
                mDatas.remove(i);             //删除数据源
                notifyItemRemoved(i);         //刷新被删除的地方
                break;
            }
        }


    }

    /**
     * 当前医院的SKU是否全部选中
     *
     * @return true:全部选中，false:全部未选中
     */
    public boolean isAllCheck() {
        boolean allCheck = true;        //默认全部都是选中状态
        for (ShoppingCartSkuData data : mDatas) {
            if ("0".equals(data.getSelected())) {
                allCheck = false;
                break;
            }
        }
        return allCheck;
    }

    /**
     * 是否有选中的SKU
     *
     * @return ：true：有选中的数据，false:全部是未选中的
     */
    public boolean isAllNotCheck() {
        boolean allCheck = false;                    //默认全部都是未选中状态
        for (ShoppingCartSkuData data : mDatas) {
            if ("1".equals(data.getSelected())) {
                //有选中的数据
                allCheck = true;
                break;
            }
        }
        return allCheck;
    }

    /**
     * 计算医院选中价格
     *
     * @return :当前医院选中的总价格
     */
    public float getHosSelectedPrice() {
        float hosAllPrice = 0;
        for (ShoppingCartSkuData data : mDatas) {
            if ("1".equals(data.getSelected())) {
                float price = Float.parseFloat(data.getTao().getPay_price_discount());
                hosAllPrice += price;
            }
        }
        return hosAllPrice;
    }

    /**
     * 获取全部数据
     *
     * @return
     */
    public List<ShoppingCartSkuData> getDatas() {
        return mDatas;
    }

    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onSelectedClick(boolean allCheck, CartSelectedData cartSelectedData);

        void onDeleteClick(List<CartDeleteData> data);

        void onNumberClick(CartNumberData data);
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
