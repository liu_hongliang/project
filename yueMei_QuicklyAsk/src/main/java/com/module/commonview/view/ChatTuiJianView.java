package com.module.commonview.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class ChatTuiJianView extends LinearLayout {

    private TextView mTextView;
    public ChatTuiJianView(Context context) {
        this(context,null);
    }

    public ChatTuiJianView(Context context,  @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ChatTuiJianView(Context context,  @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.chat_tuijian_view, this, true);
        mTextView=view.findViewById(R.id.chat_tv);
    }

    public void setText(String content){
        mTextView.setText(content);
    }
}
