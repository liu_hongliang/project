package com.module.doctor.controller.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.doctor.model.bean.DocListData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/10.
 */
public class DoctorListApi implements BaseCallBackApi {
    private String TAG = "DoctorListApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.DOCTOR, "list", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {

                if("1".equals(mData.code)){
                    ArrayList<DocListData> docData = JSONUtil.jsonToArrayList(mData.data,DocListData.class);
                    listener.onSuccess(docData);
                }
            }
        });
    }
}
