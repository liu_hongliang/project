package com.module.my.model.bean;

/**
 * Created by Administrator on 2018/4/19.
 */

public class Doparts {

    /**
     * id : 78
     * name : 眼部
     * checked : 0
     */

    private String id;
    private String name;
    private String checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
