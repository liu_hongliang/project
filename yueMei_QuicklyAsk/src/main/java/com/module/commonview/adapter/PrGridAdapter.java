package com.module.commonview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.PostoperativeRecoverBean;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.List;

public class PrGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener{
    private Context mContext;
    private List<PostoperativeRecoverBean.DataBean.PicBean> picData;
    private boolean mIsMyself;
    private GridViewHolder mGridViewHolder;

    public PrGridAdapter(Context context, List<PostoperativeRecoverBean.DataBean.PicBean> picData,Boolean isMyself) {
        mContext = context;
        this.picData = picData;
        mIsMyself=isMyself;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.postoperative_recoviry_item_item, parent, false);
        GridViewHolder gridViewHolder = new GridViewHolder(view);
        view.setOnClickListener(this);
        return gridViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PostoperativeRecoverBean.DataBean.PicBean picBean = picData.get(position);
        mGridViewHolder = (GridViewHolder) holder;

        Glide.with(mContext)
                .load(picBean.getImg())
                .transform(new GlideRoundTransform(mContext, Utils.dip2px(6)))
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .into(mGridViewHolder.mImageView);

        if ("1".equals(picBean.getIs_video())){
            mGridViewHolder.mImageViewVideo.setVisibility(View.VISIBLE);
        }
        if (picBean.isIs_cover()){
            mGridViewHolder.mAfterCover.setVisibility(View.VISIBLE);
        }else {
            mGridViewHolder.mAfterCover.setVisibility(View.GONE);
        }
        mGridViewHolder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return picData.size();
    }

    @Override
    public void onClick(View v) {
        if (mItemClickListener!=null){
            mItemClickListener.onItemClick((Integer) v.getTag());
        }
    }

    public class GridViewHolder extends RecyclerView.ViewHolder{
        FrameLayout mContainer;
        ImageView mImageView;
        ImageView mImageViewVideo;
        LinearLayout mAfterCover;
        public GridViewHolder(View itemView) {
            super(itemView);
            mContainer=itemView.findViewById(R.id.item_item_container);
            mImageView=itemView.findViewById(R.id.img_item_item);
            mImageViewVideo=itemView.findViewById(R.id.img_item_item_video);
            mAfterCover=itemView.findViewById(R.id.postoperative_before_cover);
            ViewGroup.LayoutParams layoutParams =  mContainer.getLayoutParams();
            int windowW = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
            int width=(windowW-Utils.dip2px(54))/3;
            layoutParams.width=width;
            layoutParams.height=width+4;
            mContainer.setLayoutParams(layoutParams);
        }
    }
    private OnItemClickListener mItemClickListener;
    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
