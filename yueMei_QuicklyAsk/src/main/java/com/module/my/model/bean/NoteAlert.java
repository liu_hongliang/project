package com.module.my.model.bean;

/**
 * Created by dwb on 16/3/24.
 */
public class NoteAlert {

    private String code;
    private String message;
    private NoteBookListData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NoteBookListData getData() {
        return data;
    }

    public void setData(NoteBookListData data) {
        this.data = data;
    }
}
