package com.module.commonview.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import com.module.base.refresh.refresh.MyPullRefresh;
import com.module.base.refresh.refresh.RefreshListener;
import com.module.base.view.YMBaseWebViewFragment;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.module.bean.VideoShareData;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.WebUrlTypeUtil;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;

/**
 * 百科四级页面项目介绍
 * Created by 裴成浩 on 2019/4/29
 */
public class BaikeWebFragment671 extends YMBaseWebViewFragment {

    @BindView(R.id.fragment_baike_introduce)
    MyPullRefresh mBaikeIntroduce;
    private String TAG = "BaikeWebFragment671";
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private String id;
    private String url;
    private VideoShareData videoShareData;

    public static BaikeWebFragment671 newInstance(String id, String fourUrl, VideoShareData data) {
        BaikeWebFragment671 fragment = new BaikeWebFragment671();
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("url", fourUrl);
        bundle.putParcelable("data", data);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_baike_introduce;
    }

    @Override
    protected void initView(View view) {
        id = getArguments().getString("id");
        url = getArguments().getString("url");
        videoShareData = getArguments().getParcelable("data");

        mBaikeIntroduce.addView(mWebView);

        baseWebViewClientMessage = new BaseWebViewClientMessage(getActivity(), "11", "0");
        baseWebViewClientMessage.setTypeOutside(true);
        baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String urlStr) {
                showWebDetail(urlStr);
            }
        });

        //刷新
        mBaikeIntroduce.setRefreshListener(new RefreshListener() {
            @Override
            public void onRefresh() {
                webReload();
            }
        });
    }


    @Override
    protected boolean ymShouldOverrideUrlLoading(WebView view, String url) {
        Log.e(TAG, "url === " + url);
        if (url.startsWith("type")) {
            baseWebViewClientMessage.showWebDetail(url);
        } else {
            if (url.contains("player.yuemei.com")) {
                if (videoShareData != null) {
                    WebUrlTypeUtil.getInstance(getActivity()).urlToApp(url, "11", id, videoShareData);
                }
            } else {
                WebUrlTypeUtil.getInstance(getActivity()).urlToApp(url, "11", id, url);
            }
        }
        return true;
    }

    @Override
    protected void onYmProgressChanged(WebView view, int newProgress) {
        if (newProgress == 100) {
            mDialog.stopLoading();
            if (mBaikeIntroduce != null) {
                mBaikeIntroduce.finishRefresh();
            }
        }
        super.onYmProgressChanged(view, newProgress);
    }


    @Override
    protected void initData(View view) {
        LodUrl();
    }

    public void webReload() {
        if (mWebView != null) {
            mDialog.startLoading();
            mWebView.reload();
        }
    }

    public void LodUrl() {
        mDialog.startLoading();

        Log.e(TAG, "url === " + url);
        String[] strs = url.split("/");
        Log.e(TAG, "strs4 === " + strs[4]);
        Log.e(TAG, "strs5 === " + strs[5]);
        Log.e(TAG, "strs6 === " + strs[6]);
        Log.e(TAG, "strs7 === " + strs[7]);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(strs[6], strs[7]);
        String urls = FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_URL + FinalConstant1.SYMBOL2 + FinalConstant1.YUEMEI_VER + FinalConstant1.SYMBOL2 + strs[4] + FinalConstant1.SYMBOL2 + strs[5] + FinalConstant1.SYMBOL2;
        Log.e(TAG, "urls === " + urls);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(urls, hashMap);

        mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }


    /**
     * 处理webview里面的按钮
     *
     * @param urlStr
     */
    public void showWebDetail(String urlStr) {
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;

            if (obj.getString("type").equals("1")) {// 问答详情
                String link = obj.getString("link");
                String qid = obj.getString("id");
                link = FinalConstant.baseUrl + FinalConstant.VER + link;
                Intent it = new Intent();
                it.setClass(getActivity(), DiariesAndPostsActivity.class);
                it.putExtra("url", link);
                it.putExtra("qid", qid);
                startActivity(it);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
