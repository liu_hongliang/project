package com.module.commonview.module.bean;

/**
 * 判断是否关注
 * Created by 裴成浩 on 2018/4/23.
 */

public class IsFocuData {
    private String folowing;

    public String getFolowing() {
        return folowing;
    }

    public void setFolowing(String folowing) {
        this.folowing = folowing;
    }
}
