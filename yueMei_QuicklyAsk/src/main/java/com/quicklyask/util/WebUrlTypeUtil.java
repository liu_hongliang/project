package com.quicklyask.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.DaiJinJuanApi;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.module.bean.VideoShareData;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.controller.activity.VideoListActivity;
import com.module.community.web.OtherWebviewActivity;
import com.module.community.web.WebData;
import com.module.community.web.WebUtil;
import com.module.community.web.WebViewUrlLoading;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.model.api.SaoPayApi;
import com.module.home.controller.activity.ProjectDetailsActivity;
import com.module.home.controller.activity.YueMingYiActivity;
import com.module.home.controller.activity.ZhuanTiWebActivity;
import com.module.home.model.bean.SaozfData;
import com.module.my.controller.activity.CreditActivity;
import com.module.my.controller.activity.MyDaijinjuanActivity;
import com.module.my.controller.activity.QuanziDetailActivity;
import com.module.my.controller.activity.VideoPlayerActivity;
import com.module.my.view.orderpay.BalancePaymentActivity;
import com.module.my.view.orderpay.OrderMethodActivity594;
import com.module.my.view.orderpay.OrderZhiFuStatus1Activity;
import com.module.other.activity.WebWebActivity;
import com.module.other.module.bean.SerializableMap;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.view.EditExitDialog;

import org.kymjs.aframe.ui.ViewInject;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dwb on 16/3/10.
 */
public class WebUrlTypeUtil {

    private PageJumpManager pageJumpManager;
    private Context mContext;

    private static WebUrlTypeUtil webUtils = null;
    private String TAG = "WebUrlTypeUtil";

    private WebUrlTypeUtil() {
    }

    private WebUrlTypeUtil(Context mContext) {
        this.mContext = mContext;
        pageJumpManager = new PageJumpManager((Activity) mContext);
    }

    public static WebUrlTypeUtil getInstance(Context mContext) {

        webUtils = new WebUrlTypeUtil(mContext);

        return webUtils;
    }

    /**
     * @param urlStr 1 首页
     *               2 搜索
     *               3 首页焦点图
     *               4 限时特价
     *               5 首页最新上架
     *               6 项目最新上架
     *               7 大专题（999）
     *               8 快速专题（1000）
     *               9 医生详情页
     *               10 帖子详情页
     *               11 百科详情页
     *               13 医院详情页
     *               14 医院服务
     *               15 订单详情页
     *               30 签到广告条
     *               31 签到抽奖
     *               32 签到抽奖记录
     *               33 网络安全法
     *               34 颜值精选的查看更多
     *               35 社区颜值精选（投稿奖）
     */

    //一般的web
    public void urlToApp(String urlStr) {
        urlToApp(urlStr, "0", "0");
    }

    //一般的web
    public void urlToApp(String urlStr, String source, String objid) {
        urlToApp(urlStr, null, "", source, objid, null);
    }

    public void urlToApp(String urlStr, String source, String objid, String referer) {
        urlToApp(urlStr, null, "", source, objid, null);
        Cfg.saveStr(mContext, "referer", referer);
    }

    //我要上精选web
    public void urlToApp(String urlStr, Map<String, String> singStr, String subtitle, String source, String objid) {
        urlToApp(urlStr, singStr, subtitle, source, objid, null);
    }

    //视频web
    public void urlToApp(String urlStr, String source, String objid, VideoShareData taoShareData) {
        urlToApp(urlStr, null, "", source, objid, taoShareData);
    }

    public void urlToApp(String urlStr, Map<String, String> singStr, String subtitle, String source, String objid, VideoShareData taoShareData) {
        Log.e("shipinurl", "urlStr 33333 === " + urlStr);

        String sid = "";
        String self_source = "0";

        if (urlStr.contains("m.yuemei.com/tao/")) {// 淘整形详情

            sid = getStringNum(urlStr);
            String uNmber = "0";
            if (urlStr.contains("?")) {
                uNmber = getTwogNum(urlStr);
            }
            self_source = uNmber.charAt(0) + "";
            if (TextUtils.isEmpty(source) || source.equals("0")) {
                source = self_source;
            }
            if (TextUtils.isEmpty(objid) || objid.equals("0")) {
                objid = uNmber;
            }

            Log.e(TAG, "objid == " + objid);
            Log.e(TAG, "source == " + source);
            Intent it1 = new Intent();
            it1.putExtra("id", sid);
            it1.putExtra("source", source);
            it1.putExtra("objid", objid);
            it1.putExtra("url", urlStr);
            it1.putExtra("u", uNmber);
            it1.putExtra("isAutoSend", "4");
            it1.setClass(mContext, TaoDetailActivity.class);
            ((Activity) mContext).startActivityForResult(it1, 13);

        } else if (urlStr.contains("m.yuemei.com/c/")) {// 日记贴详情
            sid = getStringNum(urlStr);

            String link = FinalConstant.baseUrl + FinalConstant.VER + "/forum/shareinfo/id/" + sid + "/";
            Intent it = new Intent();
            it.setClass(mContext, DiariesAndPostsActivity.class);
            it.putExtra("url", link);
            it.putExtra("qid", sid);
            it.putExtra("is_new", 1);
            ((Activity) mContext).startActivityForResult(it, 10);

        } else if (urlStr.contains("m.yuemei.com/article/")) {
            sid = getStringNum(urlStr);

            String link = FinalConstant.baseUrl + FinalConstant.VER + "/forum/postinfo/id/" + sid + "/";
            Intent it = new Intent();
            it.setClass(mContext, DiariesAndPostsActivity.class);
            it.putExtra("url", link);
            it.putExtra("qid", sid);
            ((Activity) mContext).startActivityForResult(it, 10);
        } else if (urlStr.contains("m.yuemei.com/p/")) {// 达人帖详情
            sid = getStringNum(urlStr);
            String link = FinalConstant.baseUrl + FinalConstant.VER + "/forum/postinfo/id/" + sid + "/";
            Intent it = new Intent();
            it.setClass(mContext, DiariesAndPostsActivity.class);
            it.putExtra("url", link);
            it.putExtra("qid", sid);
            ((Activity) mContext).startActivityForResult(it, 10);

        } else if (urlStr.contains("m.yuemei.com/ask/")) {// 问题贴详情
            sid = getStringNum(urlStr);
            String link = FinalConstant.baseUrl + FinalConstant.VER + "/forum/askinfo/id/" + sid + "/";
            Intent it = new Intent();
            it.setClass(mContext, DiariesAndPostsActivity.class);
            it.putExtra("url", link);
            it.putExtra("qid", sid);
            ((Activity) mContext).startActivityForResult(it, 10);

        } else if (urlStr.contains("m.yuemei.com/case/")) {// 医生案例
            sid = getStringNum(urlStr);
            String link = FinalConstant.baseUrl + FinalConstant.VER + "/forum/postinfo/id/" + sid + "/";
            Intent it = new Intent();
            it.setClass(mContext, DiariesAndPostsActivity.class);
            it.putExtra("url", link);
            it.putExtra("qid", sid);
            ((Activity) mContext).startActivityForResult(it, 10);

        } else if (urlStr.contains("m.yuemei.com/tao_zt/")) {// 专题详情
            //https://m.yuemei.com/tao_zt/5407.html?u=9100078

            sid = getId(urlStr);
            Map<String, String> paramsArr = getParamsArr(urlStr);
            Log.e(TAG,"paramsArr = "+ paramsArr.toString());
            String url = FinalConstant.ZHUANTI_DETAIL + sid + "/";
            Log.e(TAG, "专题详情url == " + url);
            if (mContext instanceof ProjectDetailsActivity) {
                ProjectDetailsActivity projectDetailsActivity = (ProjectDetailsActivity) this.mContext;
                url += "partid/" + projectDetailsActivity.oneLabelId + "/";
            }
            if (source.equals("10")) {
                Utils.ztrecordHttp(mContext, "BBsDetail" + objid);
            }
            for(Map.Entry<String, String> entry : paramsArr.entrySet()){
                String mapKey = entry.getKey();
                String mapValue = entry.getValue();
                System.out.println(mapKey+":"+mapValue);
                url += mapKey+"/";
                url += mapValue+"/";
            }
            Log.e(TAG,"zturl =="+url);
            Intent it1 = new Intent(mContext, ZhuanTiWebActivity.class);
            it1.putExtra("url", url);
            it1.putExtra("title", "0");
            it1.putExtra("ztid", sid);

            mContext.startActivity(it1);

        } else if (urlStr.contains("m.yuemei.com/hospital/")) {// 医院详情
            sid = getStringNum(urlStr);
            Intent it = new Intent(mContext, HosDetailActivity.class);
            it.putExtra("hosid", sid);
            mContext.startActivity(it);

        } else if (urlStr.contains("m.yuemei.com/dr/")) {// 医生详情
            sid = getStringNum(urlStr);
            String docname = "";
            Intent it = new Intent();
            it.setClass(mContext, DoctorDetailsActivity592.class);
            it.putExtra("docId", sid);
            it.putExtra("docName", docname);
            it.putExtra("partId", "");
            it.putExtra("isAutoSend", "3");
            mContext.startActivity(it);

        } else if (urlStr.contains("m.yuemei.com/mingyi/")) {       // 约名医
            Intent it = new Intent();
            it.putExtra("url", urlStr);
            it.setClass(mContext, YueMingYiActivity.class);
            mContext.startActivity(it);
        } else if (urlStr.contains("m.yuemei.com/fan/")) {          //热门圈子

            String aa = "";
            if (urlStr.contains("https")) {
                aa = urlStr.replace("https://m.yuemei.com/fan/", "");
            } else {
                aa = urlStr.replace("http://m.yuemei.com/fan/", "");
            }

            String bb = aa.replace("/", "");
            Intent it = new Intent();
            it.putExtra("url_name", bb);
            it.setClass(mContext, QuanziDetailActivity.class);
            mContext.startActivity(it);

        } else if (urlStr.contains("m.yuemei.com/shopingcart")) {//购物车

            mContext.startActivity(new Intent(mContext, ShoppingCartActivity.class));

        } else if (urlStr.contains("www.yuemei.com/api/duiba/duiba.php")) {//对吧界面

            String uid = "";
            uid = Utils.getUid();
            if (Utils.isLogin()) {
                if (Utils.isBind()) {
                    Intent intent = new Intent();
                    intent.setClass(mContext, CreditActivity.class);
                    intent.putExtra("navColor", "#fafafa");    //配置导航条的背景颜色，请用#ffffff长格式。
                    intent.putExtra("titleColor", "#636a76");    //配置导航条标题的颜色，请用#ffffff长格式。
                    intent.putExtra("url", urlStr + "&uid=" + uid);    //配置自动登陆地址，每次需服务端动态生成。
                    mContext.startActivity(intent);
                } else {
                    Utils.jumpBindingPhone(mContext);
                }

            } else {
                Utils.jumpLogin(mContext);
            }
        } else if (urlStr.contains("javascript:getcoupons")) {//点击领取优惠劵
            sid = getStringNum(urlStr);
            if (Utils.isLogin()) {
                if (Utils.isBind()) {
                    daijinjuanLingqu(sid);
                } else {
                    Utils.jumpBindingPhone(mContext);
                }

            } else {
                Utils.jumpLogin(mContext);
            }

        } else if (urlStr.contains("user.yuemei.com/marketing/pay/")) {//线下买单支付

            sid = getSegmentationUrl(urlStr, "id");

            String weikuan = getSegmentationUrl(urlStr, "weikuan");

            saoPay(sid, weikuan);

        } else if (urlStr.contains("user.yuemei.com/marketing/index/")) {//线下买单支付

            Log.e(TAG, "urlStr === " + urlStr);
            String url = urlStr + FinalConstant.MARKET;
            Log.e(TAG, "尾款结算页url === " + url);

            Intent mIntent = new Intent(mContext, BalancePaymentActivity.class);
            mIntent.putExtra("url", url);
            mIntent.putExtra("title", "0");

            mContext.startActivity(mIntent);

        } else if (urlStr.contains("user.yuemei.com/coupons/mmycoupons/")) {          //跳转到优惠劵页面
            try {
                Log.e(TAG, "urlStr == " + urlStr);
                Uri uri = Uri.parse(urlStr);
                String eventName = uri.getQueryParameter("event_name");
                String eventOthers = uri.getQueryParameter("event_others");

                Log.e(TAG, "eventName == " + eventName);
                Log.e(TAG, "eventOthers == " + eventOthers);
                Utils.tongjiApp(mContext, eventName, "0", eventOthers, "");
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "e == " + e.toString());
            }
            Log.e(TAG, "Utils.isLoginAndBind(mContext) == " + Utils.isLoginAndBind(mContext));
            if (Utils.isLoginAndBind(mContext)) {
                Log.e(TAG, "跳转.....");
                mContext.startActivity(new Intent(mContext, MyDaijinjuanActivity.class));
            }
        } else if (urlStr.contains("player.yuemei.com/") && urlStr.contains(".mp4")) {  //视频点击页面
            Log.e(TAG, "taoShareData === " + taoShareData);
            String shareTitle = "";
            String shareContent = "";
            String shareUrl = "";
            String shareImgUrl = "";
            ShareWechat wechat = null;
            if (taoShareData != null) {
                shareTitle = taoShareData.getTitle();
                shareContent = taoShareData.getContent();
                shareUrl = taoShareData.getUrl();
                shareImgUrl = taoShareData.getImg_weibo();
                wechat = taoShareData.getWechat();
            }

            if (NetworkStatus.isWifi(mContext)) {


                Log.e(TAG, "urlStr === " + urlStr);
                Log.e(TAG, "shareTitle === " + shareTitle);
                Log.e(TAG, "shareContent === " + shareContent);
                Log.e(TAG, "shareUrl === " + shareUrl);
                Log.e(TAG, "shareImgUrl === " + shareImgUrl);
                Log.e(TAG, "objid === " + objid);

                Intent intent = new Intent(mContext, VideoPlayerActivity.class);
                intent.putExtra("is_network_video", true);
                intent.putExtra("selectNum", urlStr);
                intent.putExtra("shareTitle", shareTitle);
                intent.putExtra("shareContent", shareContent);
                intent.putExtra("shareUrl", shareUrl);
                intent.putExtra("shareImgUrl", shareImgUrl);
                intent.putExtra("objid", objid);

                Log.e(TAG, "wechat222 == " + wechat);
                if (wechat != null) {
                    intent.putExtra("is_program", "1");

                    Bundle bundle = new Bundle();
                    bundle.putString("wechatTitle", wechat.getTitle());
                    bundle.putString("wechatDescription", wechat.getDescription());
                    bundle.putString("wechatPath", wechat.getPath());
                    bundle.putString("wechatThumbImage", wechat.getThumbImage());
                    bundle.putString("wechatUserName", wechat.getUserName());
                    bundle.putString("wechatWebpageUrl", wechat.getWebpageUrl());
                    intent.putExtras(bundle);
                } else {
                    intent.putExtra("is_program", "0");
                }

                mContext.startActivity(intent);
            } else {
                showDialogExitEdit3(urlStr, taoShareData, objid);
            }

        } else if (urlStr.contains("m.yuemei.com/video")) {                 //跳转到视频流页面

            String id = getSegmentationUrl(urlStr, "id");
            String flag = getSegmentationUrl(urlStr, "flag");

            Log.e(TAG, "id == " + id);
            Log.e(TAG, "flag == " + flag);
            Intent intent = new Intent(mContext, VideoListActivity.class);
            intent.putExtra("id", id);
            intent.putExtra("flag", flag);
            mContext.startActivity(intent);

        } else if (urlStr.contains("user.yuemei.com/cash/cash/")) {
            pageJumpManager.jumpToCashWebViewActivity(urlStr, "0");
        } else if (urlStr.contains("m.yuemei.com/u/home/") || urlStr.contains("m.yuemei.com/u/")) {

            Log.e(TAG, "urlStr === " + urlStr);
            sid = getStringNumOne(urlStr);
            Log.e(TAG, "sid === " + sid);
            Intent intent1 = new Intent(mContext, PersonCenterActivity641.class);
            intent1.putExtra("id", sid);
            mContext.startActivity(intent1);

        } else if (urlStr.contains("m.yuemei.com/doctor/")) {

            String docId = getStringNum(urlStr);
            Intent it = new Intent();
            it.setClass(mContext, DoctorDetailsActivity592.class);
            it.putExtra("docId", docId);
            it.putExtra("docName", "");
            it.putExtra("partId", "");
            mContext.startActivity(it);

        } else if (urlStr.contains("m.yuemei.com/app/cooperation.php")) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlStr));
            mContext.startActivity(intent);
        } else if (urlStr.contains("m.yuemei.com/taskcenter/")) {                               //任务中心
            WebData webDataTask = new WebData(FinalConstant.TASK);
            webDataTask.setShowTitle(true);
            webDataTask.setShowRefresh(true);
            WebUtil.getInstance().startWebActivity(mContext, webDataTask);
        } else {
            if (urlStr.startsWith("type")) {
                WebViewUrlLoading.getInstance().showWebDetail(mContext, urlStr);
            } else {
                WebUtil.getInstance().startWebActivity(mContext, urlStr);
//                Intent intent = new Intent(mContext, OtherWebviewActivity.class);
//                intent.putExtra("url",urlStr);
//                mContext.startActivity(intent);
            }
        }
    }


    /**
     * 当前不是wifi网络提示
     *
     * @param urlStr
     * @param taoShareData
     * @param objid
     */
    void showDialogExitEdit3(final String urlStr, final VideoShareData taoShareData, final String objid) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_phone_zhuan);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
        titleTv77.setText("非wifi提示");

        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("您当前处于非wifi下，确定继续浏览该视频？");

        titleTv88.setHeight(50);


        //确定
        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                String shareTitle = "";
                String shareContent = "";
                String shareUrl = "";
                String shareImgUrl = "";
                ShareWechat wechat = null;
                if (taoShareData != null) {
                    shareTitle = taoShareData.getTitle();
                    shareContent = taoShareData.getContent();
                    shareUrl = taoShareData.getUrl();
                    shareImgUrl = taoShareData.getImg_weibo();
                    wechat = taoShareData.getWechat();
                }

                Intent intent = new Intent(mContext, VideoPlayerActivity.class);
                intent.putExtra("is_network_video", true);
                intent.putExtra("selectNum", urlStr);
                intent.putExtra("shareTitle", shareTitle);
                intent.putExtra("shareContent", shareContent);
                intent.putExtra("shareUrl", shareUrl);
                intent.putExtra("shareImgUrl", shareImgUrl);
                intent.putExtra("objid", objid);


                Log.e(TAG, "wechat222 == " + wechat);
                if (wechat != null) {
                    intent.putExtra("is_program", "1");

                    Bundle bundle = new Bundle();
                    bundle.putString("wechatTitle", wechat.getTitle());
                    bundle.putString("wechatDescription", wechat.getDescription());
                    bundle.putString("wechatPath", wechat.getPath());
                    bundle.putString("wechatThumbImage", wechat.getThumbImage());
                    bundle.putString("wechatUserName", wechat.getUserName());
                    bundle.putString("wechatWebpageUrl", wechat.getWebpageUrl());
                    intent.putExtras(bundle);
                } else {
                    intent.putExtra("is_program", "0");
                }

                mContext.startActivity(intent);
                editDialog.dismiss();
            }
        });

        //取消
        Button cancelBt99 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt99.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    void saoPay(String orderid, final String weikuan) {
        Log.e(TAG, "orderid == " + orderid);
        String flag = "1";
        if ("1".equals(weikuan)) {
            flag = "3";
        }
        Map<String, Object> maps = new HashMap<>();
        maps.put("order_id", orderid);
        maps.put("flag", flag);
        new SaoPayApi().getCallBack(mContext, maps, new BaseCallBackListener<SaozfData>() {
            @Override
            public void onSuccess(SaozfData saozfData) {
                Log.e(TAG, "onSuccess ....");
                String price = saozfData.getPrice();
                String order_id = saozfData.getOrder_id();
                String title = saozfData.getTao_name();
                String taoid = saozfData.getTao_id();
                String is_insure = saozfData.getIs_insure();
                String is_repayment = saozfData.getIs_repayment();
                String is_repayment_mimo = saozfData.getIs_repayment_mimo();

                Bundle bundle = new Bundle();
                Intent intent = new Intent();
                if (Float.parseFloat(price) > 0) {
                    intent.setClass(mContext, OrderMethodActivity594.class);
                    bundle.putString("price", price);
                    bundle.putString("server_id", "0");
                    bundle.putString("order_id", order_id);
                    bundle.putString("order_time", "30m");
                    bundle.putString("type", "2");
                    bundle.putString("sku_type", "1");
                    bundle.putString("is_repayment", is_repayment);
                    bundle.putString("is_repayment_mimo", is_repayment_mimo);
                    bundle.putString("weikuan", weikuan);
                    bundle.putString("is_group", "0");
                    bundle.putString("group_id", "");
                } else {
                    intent.setClass(mContext, OrderZhiFuStatus1Activity.class);
                    bundle.putString("price", price);
                    bundle.putString("tao_title", title);
                    bundle.putString("server_id", "0");
                    bundle.putString("order_id", order_id);
                    bundle.putString("taoid", taoid);
                    bundle.putString("order_time", "30m");
                    bundle.putString("type", "2");
                    bundle.putString("sku_type", "1");
                    bundle.putString("is_repayment", is_repayment);
                    bundle.putString("is_repayment_mimo", is_repayment_mimo);
                    bundle.putString("weikuan", weikuan);
                    bundle.putString("number", "1");
                    bundle.putString("is_group", "0");
                    bundle.putString("group_id", "");
                }

                intent.putExtra("data", bundle);
                mContext.startActivity(intent);

                ((Activity) mContext).finish();
            }

        });
    }

    public String getTwogNum(String zifuchuang) {
        //https://m.yuemei.com/tao_zt/5407.html?u=9100078
        String aStr = zifuchuang;
        if (zifuchuang.contains("https")) {
            aStr = aStr.replace("https://", "");
        } else {
            aStr = aStr.replace("http://", "");
        }
        String[] aSz = aStr.split("/");
        if (aSz[3].contains("u=")) {
            String[] split = aSz[3].split("u=");
            return split[1];
        } else {
            return 0 + "";
        }
    }

    public String getTwogNum2(String zifuchuang) {
        //https://m.yuemei.com/tao_zt/5407.html?u=9100078
        //https://m.yuemei.com/tao_zt/11176.html?tab_pro_id=1102&tab_special_id=11197
        String aStr = zifuchuang;
        if (zifuchuang.contains("https")) {
            aStr = aStr.replace("https://", "");
        } else {
            aStr = aStr.replace("http://", "");
        }
        String[] aSz = aStr.split("/");
        if (aSz[2].contains("u=")) {
            String[] split = aSz[2].split("u=");
            return split[1];
        } else {
            return 0 + "";
        }
    }


    /**
     * 把链接里面 ？ = & 变成 /
     *
     * @param url
     * @return
     */
    private String changeUrl(String url) {
        //https://m.yuemei.com/tao_zt/11176.html?tab_pro_id=1102&tab_special_id=11197
        Map<String, String> paramsArr = getParamsArr(url);
        Log.e(TAG,"paramsArr = "+ paramsArr.toString());
        String changeUrl = "";

        Set<String> strings = paramsArr.keySet();



        if (url.contains("https")) {
            changeUrl = url.replace("https://", "");
        } else {
            changeUrl = url.replace("http://", "");
        }
        String[] splitUrl = changeUrl.split("/");
        for (int i = 0; i < splitUrl.length; i++) {
            if (splitUrl[i].contains(".html?")) {
                String str = splitUrl[i].replace(".html", "");
                String str1 = str.replace("?", "/");
                if (str1.contains("&")) {
                    String str2 = str1.replace("&", "/");
                    if (str2.contains("=")) {
                        String strResult = str2.replace("=", "/");
                        //11176/tab_pro_id//tab_special_id/11197
                        if (strResult.contains("//")) {
                            String finUrl = strResult.replace("//", "/");
                            return FinalConstant.ZHUANTI_DETAIL + finUrl;
                        }
                        return FinalConstant.ZHUANTI_DETAIL + strResult;
                    }
                }
            }
        }
        return null;
    }

    public Map<String,String> getParamsArr(String url){
        String ztid = "0";
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        if (url.contains("?")) {
            String[] urlArr = url.split("\\?");
            if(!TextUtils.isEmpty(urlArr[0])){
                ztid = getId(urlArr[0]);
                Log.e(TAG,"ztid = "+ztid);
            }
            if(!TextUtils.isEmpty(urlArr[1])){
                String[] paramsStrArr = urlArr[1].split("&");
                Log.e(TAG,"paramsStrArr = "+ Arrays.toString(paramsStrArr));
                int paramsStrArrLen = paramsStrArr.length;
                if(paramsStrArrLen>0){
                    for (int i = 0; i < paramsStrArrLen ; i++) {
                        if(!TextUtils.isEmpty(paramsStrArr[i])){
                            String[] paramsArr = paramsStrArr[i].split("=");
                            Log.e(TAG,"paramsArr = "+ Arrays.toString(paramsArr));
                            for (int j = 0; j < paramsArr.length; j++) {
                                stringStringHashMap.put(paramsArr[j],paramsArr[j+1]);
                                j++;
                            }
                        }
                    }
                }
            }
        }else {
            ztid = getId(url);
            Log.e(TAG,"ztid = "+ztid);
        }
        stringStringHashMap.put("ztid",ztid);
        return  stringStringHashMap;
    }


    public String getStringNum(String zifuchuang) {
        try {
            //https://m.yuemei.com/tao_zt/5407.html?u=9100078
            //https://m.yuemei.com/tao_zt/11176.html?tab_pro_id=1102&tab_special_id=11197
            String aStr = zifuchuang;
            if (zifuchuang.contains("https")) {
                aStr = aStr.replace("https://", "");
            } else {
                aStr = aStr.replace("http://", "");
            }
            String[] aSz = aStr.split("/");

            if (null != aSz[2] && aSz[2].length() > 0) {
                if (aSz[2].contains(".html")) {
                    String replace = aSz[2].replace(".html", "");
                    if (replace.contains("?u=")) { //5407?u=9100078
                        String[] split = replace.split("\\?u=");
                        return split[0];
                    } else {
                        return replace;
                    }
                } else {
                    return aSz[2];
                }

            } else {
                return 0 + "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    private String getId(String str) {
        String aStr = "";
        if (str.contains("https")) {
            aStr = str.replace("https://", "");
        } else {
            aStr = str.replace("http://", "");
        }
        String[] aSz = aStr.split("/");
        if (!TextUtils.isEmpty(aSz[2])) {
            String[] split = aSz[2].split("\\.");
            return split[0];
        }
        return null;
    }


    public String getStringNumOne(String zifuchuang) {
        String a = zifuchuang;
        String regEx = "[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(a);
        // System.out.println(m.replaceAll("").trim());
        String sss = m.replaceAll("").trim();

        return sss;
    }

    public String getSegmentationUrl(String url, String attribute) {
        String[] strs = url.split("/");
        int index = -1;
        String str = "0";
        for (int i = 0; i < strs.length; i++) {
            if (strs[i].equals(attribute)) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            str = strs[index + 1];
        }

        return str;
    }

    void daijinjuanLingqu(String jusnid) {
        String uid = Utils.getUid();
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("id", jusnid);
        new DaiJinJuanApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData != null) {
                    ViewInject.toast(serverData.message);
                } else {
                    ViewInject.toast(serverData.message);
                }
            }

        });

    }
}
