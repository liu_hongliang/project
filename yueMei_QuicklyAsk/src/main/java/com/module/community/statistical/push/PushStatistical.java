package com.module.community.statistical.push;

import android.text.TextUtils;
import android.util.Log;

import com.module.community.statistical.statistical.ExtrasBean;
import com.quicklyask.util.JSONUtil;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * push统计
 * Created by 裴成浩 on 2019/6/3
 */
@Aspect
public class PushStatistical {

    private String TAG = "PushStatistical";
    private HashMap<String, String> topHashMap = new LinkedHashMap<>();

    private List<String> mPaths = new ArrayList<>();                            //activity执行顺序列表
    private String extras;

    /**
     * 所有Activity的onCreate()统计
     *
     * @param joinPoint
     */
    @Before("execution(* android.app.Activity.onCreate(android.os.Bundle))")
    public void methodCreate(JoinPoint joinPoint) {
        String canonicalName = joinPoint.getThis().toString();
        Log.e(TAG, "onCreate --- > methodCreate --> canonicalName == " + canonicalName);
        //判断是否存在相同的
        for (String path : mPaths) {
            if (canonicalName.equals(path)) {
                return;
            }
        }

        mPaths.add(canonicalName);

        if (!TextUtils.isEmpty(extras)) {
            if (!topHashMap.containsKey(canonicalName)) {
                Log.e(TAG, "methodCreate --> topHashMap地址值 == " + topHashMap);
                topHashMap.put(canonicalName, extras);
            }
            extras = "";
        }

        Log.e(TAG, "methodCreate --- > topHashMap == " + topHashMap.toString());

    }

    /**
     * OpenClickActivity中的handleOpenClick方法获取参数
     *
     * @param joinPoint
     */
    @Before("execution(* com.module.OpenClickActivity.handleOpenClick(java.lang.String))")
    public void openClickCreate(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        extras = (String) args[0];
    }

    /**
     * 添加公共参数中的方法中加入push参数
     *
     * @param joinPoint
     */
    @Before("execution(* com.module.other.netWork.SignUtils.buildHttpParamMap(java.util.Map<java.lang.String,java.lang.Object>))")
    public void openSignUtils(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        Map<String, Object> map = (Map<String, Object>) args[0];

        Log.e(TAG, "openSignUtils  aaa === " + map.toString());
        if (topHashMap != null && topHashMap.size() > 0) {
            for (int i = mPaths.size() - 1; i >= 0; i--) {
                String path = mPaths.get(i);
                if (topHashMap.containsKey(path)) {
                    ExtrasBean extrasBean = JSONUtil.TransformSingleBean(topHashMap.get(path), ExtrasBean.class);
                    HashMap<String, String> params = extrasBean.getParams();

                    Log.e(TAG, "path == " + path);

                    if (params != null && params.size() > 0) {
                        Log.e(TAG, "params == " + params.toString());
                        Iterator<String> iter = params.keySet().iterator();
                        while (iter.hasNext()) {
                            String key = iter.next();
                            map.put(key, params.get(key));
                        }
                    }
                    break;
                }
            }
        }
    }

    @Before("execution(* android.app.Activity.onDestroy())")
    public void methodDestroy(JoinPoint joinPoint) {
        String canonicalName = joinPoint.getThis().toString();
        Log.e(TAG, "onDestroy --- > canonicalName == " + canonicalName);
        Log.e(TAG, "onDestroy --- > topHashMap == " + topHashMap.toString());
        if (!canonicalName.contains("com.module.OpenClickActivity")) {
            topHashMap.remove(canonicalName);
        }
    }
}
