package com.module.shopping.model.bean;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.module.commonview.module.bean.MessageBean;
import com.module.taodetail.model.bean.HomeTaoData;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/11/21.
 */
public class ShoppingCartData {
    private String hos_id;                          //医院id
    private String hos_name;                        //医院名称
    private String url;                             //跳转地址
    private ShoppingCartSkuCoupons coupons;         //优惠券信息
    private List<ShoppingCartSkuData> data;         //购物车数据

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ShoppingCartSkuCoupons getCoupons() {
        return coupons;
    }

    public void setCoupons(ShoppingCartSkuCoupons coupons) {
        this.coupons = coupons;
    }

    public List<ShoppingCartSkuData> getData() {
        return data;
    }

    public void setData(List<ShoppingCartSkuData> data) {
        this.data = data;
    }

}