package com.module.home.model.bean;

import java.util.HashMap;

public class HotWordsData {
	private String keywords;
	private String url;
	private String is_push;
	private HashMap<String,String> event_params;

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIs_push() {
		return is_push;
	}

	public void setIs_push(String is_push) {
		this.is_push = is_push;
	}

	public HashMap<String, String> getEvent_params() {
		return event_params;
	}

	public void setEvent_params(HashMap<String, String> event_params) {
		this.event_params = event_params;
	}
}
