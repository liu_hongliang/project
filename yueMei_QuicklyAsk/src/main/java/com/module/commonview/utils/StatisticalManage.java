package com.module.commonview.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 统计管理类
 * Created by 裴成浩 on 2018/3/26.
 */

public class StatisticalManage {

    private String TAG = "StatisticalManage";

    /**
     * 单例
     */
    private StatisticalManage() {
    }

    private static StatisticalManage statisticalManage = null;

    public synchronized static StatisticalManage getInstance() {
        if (statisticalManage == null) {
            statisticalManage = new StatisticalManage();
        }
        return statisticalManage;
    }


    /**
     * GrowingIO统计
     */
    public void growingIO(String identifier) {
        growingIO(identifier,new HashMap<String, String>());
    }
    public void growingIO(String identifier, Map<String, String> dates) {
//        try {
//            GrowingIO growingIO = GrowingIO.getInstance();
//            JSONObject props = new JSONObject();
//            Iterator<String> iter = dates.keySet().iterator();
//            while (iter.hasNext()) {
//                String key = iter.next();
//                String value = dates.get(key);
//                props.put(key, value);
//            }
//
//            growingIO.track(identifier, props);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }


}
