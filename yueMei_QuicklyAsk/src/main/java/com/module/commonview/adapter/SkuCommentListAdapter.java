package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.doctor.view.StaScoreBar;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AppraisalListBean;
import com.quicklyask.entity.Pic;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.List;

public class SkuCommentListAdapter extends BaseQuickAdapter<AppraisalListBean,BaseViewHolder> {
    public SkuCommentListAdapter(int layoutResId, @Nullable List<AppraisalListBean> data) {
        super(layoutResId, data);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void convert(final BaseViewHolder helper, final AppraisalListBean item) {

        Glide.with(mContext)
                .load(item.getUser_img())
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .into((ImageView) helper.getView(R.id.sku_comment_userimg));
        StaScoreBar staScoreBar=helper.getView(R.id.sku_comment_score);
        String rateSale = item.getRateSale();
        if (!TextUtils.isEmpty(rateSale)){
            int rate = Integer.parseInt(rateSale);
            staScoreBar.setProgressBar(rate);
        }
        helper.setText(R.id.sku_comment_name,item.getUser_name())
                .setText(R.id.sku_comment_time,item.getTime())
                .setText(R.id.sku_comemmt_title,item.getTitle())
                .setText(R.id.sku_comment_eye,item.getView_num());
        String answer_num = item.getAnswer_num();
        if (!TextUtils.isEmpty(answer_num) && !"0".equals(answer_num)){
            helper.setGone(R.id.sku_comment_talk_visorgone,true);
            helper.setText(R.id.sku_comment_talk,item.getAnswer_num());
        }else {
            helper.setGone(R.id.sku_comment_talk_visorgone,false);
        }

        RecyclerView commentList = helper.getView(R.id.sku_comemnt_list);
        List<Pic> itemPic = item.getPic();
        if (null != itemPic && itemPic.size() > 0){
            helper.setGone(R.id.sku_comemnt_list,true);
            ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            scrollLinearLayoutManager.setScrollEnable(false);
            commentList.setLayoutManager(scrollLinearLayoutManager);
            List<Pic> pic = item.getPic();
            for (int i = 0; i <pic.size() ; i++) {
                if (i == pic.size()-1){
                    pic.get(i).setHave_pic(item.getHave_pic());
                }
            }
            SkuCommentListItemAdapter skuCommentListItemAdapter = new SkuCommentListItemAdapter(R.layout.sku_comment_list_list, pic);
            commentList.setAdapter(skuCommentListItemAdapter);
            commentList.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return helper.itemView.onTouchEvent(event);
                }
            });
        }else {
            helper.setGone(R.id.sku_comemnt_list,false);
        }

//        if (helper.getLayoutPosition() == getData().size()-1){
//            helper.setGone(R.id.sku_comment_btn,true);
//        }else {
//            helper.setGone(R.id.sku_comment_btn,false);
//        }

        final LinearLayout headClick = helper.getView(R.id.sku_comment_usertitle_click);
        headClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemButtonClick.onItemHeadClick(v,helper.getLayoutPosition());
            }
        });
        Button bottomBtn=helper.getView(R.id.sku_comment_btn);
        bottomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemButtonClick.onItemButtonClik(v,helper.getLayoutPosition());
            }
        });

    }

    public void setmItemEventClick(ItemEventClick mItemButtonClick) {
        this.mItemButtonClick = mItemButtonClick;
    }

    ItemEventClick mItemButtonClick;

    public interface ItemEventClick{
        void onItemHeadClick(View v,int index);
        void onItemButtonClik(View v,int index);
    }

}
