package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.commonview.view.CommonTopBar;
import com.module.home.view.LoadingProgress;
import com.module.my.controller.adapter.ImageUploadAdapter;
import com.module.my.model.api.PostTextQueApi;
import com.module.other.netWork.netWork.QiNuConfig;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyack.emoji.Expressions;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.entity.WriteResultData;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.MyUploadImage;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.ProcessImageView;
import com.quicklyask.view.WritePicPopWindow;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.xinlan.imageeditlibrary.editimage.EditImageActivity;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.image.ImageOptions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WriteSuibianLiaoActivity extends BaseActivity {

    private String TAG = "WriteSuibianLiaoActivity";

    private WriteSuibianLiaoActivity mContext;

    private GridView gridview;
    private ImageUploadAdapter adapter;
    private float dp;

    @BindView(id = R.id.write_suibianliao_top)
    private CommonTopBar mTop;
    @BindView(id = R.id.write_que_content_et)
    private EditText postContent;

    @BindView(id = R.id.write_question_tips_photo_rly)
    private RelativeLayout tipsPhoto;

    private String cateid = "0";


    private String contentStr = "";

    private String uid;

    Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);


    // 表情
    private ViewPager viewPager;
    private ArrayList<GridView> grids;
    private int[] expressionImages;
    private String[] expressionImageNames;
    private int[] expressionImages1;
    private String[] expressionImageNames1;
    private GridView gView1;
    private GridView gView2;
    private LinearLayout page_select;
    private ImageView page0;
    private ImageView page1;

    @BindView(id = R.id.biaoqing_ly_content)
    private LinearLayout biaoqingRly;// 表情输入按钮
    @BindView(id = R.id.biaoqing_shuru_content_ly1)
    private LinearLayout biaoqingContentLy;
    @BindView(id = R.id.colse_biaoqingjian_bt)
    private ImageButton closeImBt;

    ImageOptions imageOptions;
    ImageOptions imageOptions1;

    @BindView(id = R.id.ll_bottom_container)
    private RelativeLayout llBottomContainer;
    @BindView(id = R.id.ll_suibian_context)
    private LinearLayout llSuibianContext;

    private ArrayList<String> mResults = new ArrayList<>();
    private static final int REQUEST_CODE = 732;
    public static final int ACTION_REQUEST_EDITIMAGE = 9;

    @BindView(id = R.id.tuppppppppppp_fly)
    private FrameLayout tuPicFly;

    private WritePicPopWindow wpicPop;
    private HashMap<String, ProcessImageView> processImages;
    private boolean notClick = false;
    private LoadingProgress mDialog;
    private int[] mImageWidthHeight;
    private String mKey;

    @SuppressLint("NewApi")
    @Override
    public void setRootView() {
        setContentView(R.layout.acty_write_suibianliao);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = WriteSuibianLiaoActivity.this;

        mDialog = new LoadingProgress(mContext);
        imageOptions = new ImageOptions.Builder().setImageScaleType(ImageView.ScaleType.FIT_XY).setLoadingDrawableId(R.drawable.radius_gray80).setFailureDrawableId(R.drawable.radius_gray80).build();

        imageOptions1 = new ImageOptions.Builder().setRadius(6).setImageScaleType(ImageView.ScaleType.FIT_XY).setLoadingDrawableId(R.drawable.radius_gray80).setFailureDrawableId(R.drawable.radius_gray80).build();

        page_select = findViewById(R.id.page_select);
        page0 = findViewById(R.id.page0_select);
        page1 = findViewById(R.id.page1_select);
        // 引入表情
        expressionImages = Expressions.expressionImgs;
        expressionImageNames = Expressions.expressionImgNames;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;
        // 创建ViewPager
        viewPager = findViewById(R.id.viewpager);
        initViewPager();
        Init();

        //关闭按钮
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (postContent.getText().toString().length() > 0 || mResults.size() > 0) {
                    showDialogExitEdit();
                } else {
                    finish();
                }
            }
        });

        mTop.getTv_right().setClickable(false);
        postContent.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                initBtIfClick();
            }

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2, int count) {
                initBtIfClick();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
                initBtIfClick();
            }

        });

        postContent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (notClick) {
                    Toast.makeText(WriteSuibianLiaoActivity.this, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    biaoqingContentLy.setVisibility(View.GONE);
                    biaoqingRly.setVisibility(View.VISIBLE);
                }
            }
        });

        llSuibianContext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notClick) {
                    Toast.makeText(WriteSuibianLiaoActivity.this, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    postContent.setFocusable(true);
                    postContent.setFocusableInTouchMode(true);
                    postContent.requestFocus();
                }
            }
        });


        biaoqingRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                biaoqingRly.setVisibility(View.GONE);
                biaoqingContentLy.setVisibility(View.VISIBLE);
            }
        });

        closeImBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
                biaoqingRly.setVisibility(View.VISIBLE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
    }

    void initBtIfClick() {
        if (TextUtils.isEmpty(postContent.getText())) {
            mTop.setRightTextColor(Utils.getLocalColor(mContext,R.color.gary));
            mTop.getTv_right().setClickable(false);
        } else {
            mTop.setRightTextColor(Utils.getLocalColor(mContext,R.color.title_red_new));
            mTop.getTv_right().setClickable(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        uid = Utils.getUid();
    }

    public void Init() {
        Intent it = getIntent();
        if (it != null) {
            cateid = it.getStringExtra("cateid");
        }

        dp = getResources().getDimension(R.dimen.dp);
        gridview = findViewById(R.id.noScrollgridview2);
        gridview.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gridviewInit();

        /**
         * 提交
         */
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (notClick) {
                    Toast.makeText(mContext, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    uid = Utils.getUid();

                    if (Utils.isLogin()) {
                        String content = postContent.getText().toString().trim();
                        contentStr = content;

                        Matcher matcher = emoji.matcher(contentStr);

                        if (matcher.find()) {
                            Toast.makeText(WriteSuibianLiaoActivity.this, "暂不支持表情输入", Toast.LENGTH_SHORT).show();
                        } else {
                            if (content.length() > 0 && !"".equals(content)) {
                                if (content.length() > 5) {
                                    mTop.setRightTextColor(Utils.getLocalColor(mContext,R.color.gary));
                                    mTop.getTv_right().setClickable(false);
                                    mDialog.startLoading();
                                    postTextQue();
                                } else {
                                    ViewInject.toast("亲，内容至少要大于5个字哟！");
                                }
                            } else {
                                ViewInject.toast("内容不能为空！");
                            }
                        }
                    } else {
                        Utils.jumpLogin(mContext);
                    }
                }
            }
        });

    }

    Button cancelBt;
    Button trueBt;

    void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        cancelBt = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
        trueBt = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
                finish();
            }
        });

    }


    public static final int START_S_C = 1;      //上传开始
    public static final int JIN_DU = 2;         //上传进度更逊
    public static final int WANCHENG_S_C = 3;   //上传完成（成功）
    public static final int SHIBAI_S_C = 4;     //上传失败
    public static final int WANCHENG_C_X = 5;     //重新上传成功（包括贴纸修改上传、上传失败重新上传）
    public static final int SHIBAI_C_X = 6;     //重新上传失败（包括贴纸修改上传、上传失败重新上传）

    HashMap<String, JSONObject> mSameData = new HashMap<>();            //图片上传完成后返回的图片地址集合。key是本地存储路径，vle是服务器连接
    HashMap<String, String> mErrorImg = new HashMap<>();            //图片上传失败后保存的一个集合。key：本地存储路径，vle：本地存储路径
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case START_S_C:             //开始上传（包括图片返回上传、贴纸修改的上传、重新上传）

                    String lianXu = (String) msg.obj;

                    if ("0".equals(lianXu)) {

                        postFileQue(msg.arg1, true);

                    } else if ("1".equals(lianXu)) {

                        postFileQue(msg.arg1, false);

                    } else {
                        Toast.makeText(WriteSuibianLiaoActivity.this, "没有设置是否连续上传", Toast.LENGTH_SHORT).show();
                    }

                    break;
                case JIN_DU:               //上传进度

                    int progress = msg.arg1;
                    ProcessImageView mImgs = (ProcessImageView) msg.obj;
                    mImgs.startHua(ProcessImageView.SAHNG_CHUAN_ZHONG, progress);             //开始画图

                    break;
                case WANCHENG_S_C:  //完成上传（成功）
                    Log.e("TAG", "1111");
                    int pos = msg.arg1;
                    JSONObject jsonObject = setJson();
                    mSameData.put(mResults.get(pos), jsonObject);            //设置要上传的集合值
                    ProcessImageView imgView = processImages.get(mResults.get(pos));
                    imgView.startHua(ProcessImageView.WANC_HENG, 100);      //设置图片UI

                    if (pos != mResults.size() - 1) {     //不是最后一个,继续上传
                        mySendMessage(pos + 1, "0", START_S_C);
                    } else {
                        notClick = false;           //设置按钮可以点击
                        setContentSelected(true);
                    }

                    adapter.notifyDataSetChanged();
                    break;

                case SHIBAI_S_C:  //上传失败
                    int posSb = msg.arg1;
                    mErrorImg.put(mResults.get(posSb), mResults.get(posSb));      //上传失败后设置失败图片的路径

                    Log.e("TAG", "连续上传，其中一个失败了999999999999");
                    ProcessImageView imgViewSb = processImages.get(mResults.get(posSb));
                    imgViewSb.startHua(ProcessImageView.SHI_BAI, 0);             //设置图片UI

                    if (posSb != mResults.size() - 1) {     //不是最后一个,继续上传
                        mySendMessage(posSb + 1, "0", START_S_C);
                    } else {
                        notClick = false;       //设置按钮可以点击
                        setContentSelected(true);
                    }
                    adapter.notifyDataSetChanged();
                    break;

                case WANCHENG_C_X:                 //重新上传后成功（包括贴纸返回，图片重新上传）

                    int pos1 = msg.arg1;
                    JSONObject jsonObject1 = setJson();
                    mSameData.put(mResults.get(pos1), jsonObject1);            //设置要上传的集合值

                    if (mErrorImg.get(mResults.get(pos1)) != null) {
                        mErrorImg.remove(mResults.get(pos1));               //如果这个是上传失败重新上传的图片，那么删除失败集合的数据
                    }

                    ProcessImageView imgView1 = processImages.get(mResults.get(pos1));
                    imgView1.startHua(ProcessImageView.WANC_HENG, 100);              //设置图片UI

                    notClick = false;
                    setContentSelected(true);
                    adapter.notifyDataSetChanged();
                    break;

                case SHIBAI_C_X:               //重新上传后失败（包括贴纸返回，图片重新上传）

                    int posSb1 = msg.arg1;
                    mErrorImg.put(mResults.get(posSb1), mResults.get(posSb1));      //上传失败后设置失败图片的路径

                    ProcessImageView imgViewSb1 = processImages.get(mResults.get(posSb1));
                    imgViewSb1.startHua(ProcessImageView.SHI_BAI, 0);             //设置图片UI

                    notClick = false;
                    setContentSelected(true);
                    break;
            }
        }
    };

    /**
     * 编辑内容是否获取焦点
     *
     * @param focusable
     */
    private void setContentSelected(boolean focusable) {
        postContent.setFocusable(focusable);
        postContent.setFocusableInTouchMode(focusable);
        postContent.requestFocus();
    }


    private JSONObject setJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("width", mImageWidthHeight[0]);
            jsonObject.put("height", mImageWidthHeight[1]);
            jsonObject.put("img", mKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     * 初始化图片列表形式
     */
    public void gridviewInit() {

        adapter = new ImageUploadAdapter(this, mResults, mSameData, mErrorImg);

        setGridViewSize();

        gridview.setAdapter(adapter);

        llBottomContainer.setBackgroundColor(Color.parseColor("#EEF1EFF5"));
        //删除回调
        adapter.setOnItemDeleteClickListener(new ImageUploadAdapter.onItemDeleteListener() {
            @Override
            public void onDeleteClick(int i) {
                if (notClick) {
                    Toast.makeText(WriteSuibianLiaoActivity.this, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    if (mErrorImg.get(mResults.get(i)) != null) {
                        mErrorImg.remove(mResults.get(i));
                    }

                    if (mSameData.get(mResults.get(i)) != null) {
                        mSameData.remove(mResults.get(i));
                    }

                    mResults.remove(i);

                    gridviewInit();
                }
            }
        });


        gridview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, final int pos, long arg3) {
                if (notClick) {
                    Toast.makeText(WriteSuibianLiaoActivity.this, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    Utils.hideSoftKeyboard(mContext);
                    biaoqingContentLy.setVisibility(View.GONE);
                    biaoqingRly.setVisibility(View.VISIBLE);

                    //版本判断
                    if (Build.VERSION.SDK_INT >= 23) {

                        Acp.getInstance(WriteSuibianLiaoActivity.this).request(new AcpOptions.Builder().setPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                            @Override
                            public void onGranted() {
                                toXIangce(pos);
                            }

                            @Override
                            public void onDenied(List<String> permissions) {
                            }
                        });
                    } else {
                        toXIangce(pos);
                    }
                }

            }

        });
    }

    /**
     * 设置gritView的宽高
     */
    private void setGridViewSize() {
        int size = 0;
        if (mResults.size() < 9) {
            size = mResults.size() + 1;
        } else {
            size = mResults.size();
        }

        if (mResults.size() == 0) {
            tipsPhoto.setVisibility(View.VISIBLE);
        } else {
            tipsPhoto.setVisibility(View.GONE);
        }

        LayoutParams params = gridview.getLayoutParams();
        int sizeType = size > 4 ? size > 8 ? 3 : 2 : 1;
        int height = (sizeType * (int) (dp * 9.4f)) + (sizeType == 3 ? 12 : sizeType == 3 ? 30 : sizeType == 2 ? 20 : 10);
        params.height = height;                             //设置高度
        gridview.setLayoutParams(params);
        gridview.setColumnWidth((int) (dp * 9.4f));         //设置列宽
    }


    void toXIangce(int pos) {
        if (pos == mResults.size()) {
            String sdcardState = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(sdcardState)) {

                // 启动多个图片选择器
                Intent intent = new Intent(WriteSuibianLiaoActivity.this, ImagesSelectorActivity.class);
                // 要选择的图像的最大数量
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 9);
                // 将显示的最小尺寸图像;用于过滤微小的图像(主要是图标)
                intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 50000);
                // 显示摄像机或不
                intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
                // 将当前选定的图像作为初始值传递
                intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
                // 开始选择器
                startActivityForResult(intent, REQUEST_CODE);

            } else {
                Toast.makeText(getApplicationContext(), "sdcard已拔出，不能选择照片", Toast.LENGTH_SHORT).show();
            }

        } else {

            String paths = mResults.get(pos);

            if (mErrorImg.get(paths) != null) {          //是上传失败的图片
                Log.e("TAG", "是上传失败的图片");
                mySendMessage(pos, "1", START_S_C);        //发送不连续上传的消息

            } else {
                Intent it = new Intent(WriteSuibianLiaoActivity.this, EditImageActivity.class);
                it.putExtra(EditImageActivity.FILE_PATH, paths);
                it.putExtra(EditImageActivity.EXTRA_COVER, "0");
                File outputFile = FileUtils.getEmptyFile("yuemei" + System.currentTimeMillis() + ".jpg");
                it.putExtra(EditImageActivity.EXTRA_OUTPUT, outputFile.getAbsolutePath());
                it.putExtra("pos", pos + "");
                startActivityForResult(it, ACTION_REQUEST_EDITIMAGE);
            }
        }
        initBtIfClick();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (null != data) {
                        mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);

                        gridviewInit();

                        pictureReorder();

                        if (mResults.size() > 0) {
                            new CountDownTimer(350, 100) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {

                                    if (Utils.isValidContext(mContext)) {

                                        String ispop = Cfg.loadStr(mContext, "poppop", "");
                                        if (ispop.equals("1")) {

                                        } else {
                                            wpicPop = new WritePicPopWindow(mContext);
//                                            wpicPop.showAsDropDown(tuPicFly, 0, -300);
                                            if (Build.VERSION.SDK_INT < 24) {
                                                wpicPop.showAsDropDown(tuPicFly, 0, -300);
                                            } else {
                                                // 适配 android 7.0
                                                int[] location = new int[2];
                                                tuPicFly.getLocationOnScreen(location);
                                                int x = location[0];
                                                int y = location[1];
                                                Log.e("xxxxxx", "x : " + x + ", y : " + y);
                                                wpicPop.showAtLocation(tuPicFly, Gravity.NO_GRAVITY, 0, y - tuPicFly.getHeight() / 2 - 20);
                                            }

                                            Cfg.saveStr(mContext, "poppop", "1");
                                        }
                                    }

                                }
                            }.start();
                        }

                    }
                }
                break;
            case ACTION_REQUEST_EDITIMAGE://
                if (null != data) {
                    String newFilePath = data.getStringExtra("save_file_path");
                    String poss = data.getStringExtra("pos");
                    String dele = data.getStringExtra("dele");

                    if (dele.equals("0")) {
                        Log.e("AAAAA", "newFilePath==" + newFilePath);
                        mResults.set(Integer.parseInt(poss), newFilePath);
                        gridviewInit();

                        mySendMessage(Integer.parseInt(poss), "1", START_S_C);
                    } else {
                        mResults.remove(Integer.parseInt(poss));
                        gridviewInit();
                    }
                }
                break;
        }
    }

    /**
     * 图片重新排序
     */
    private void pictureReorder() {
        int pos = 0;
        if (mSameData.size() == 0 && mErrorImg.size() == 0) {     //如果是第一次添加图片
            mySendMessage(pos, "0", START_S_C);
        } else {
            for (int i = 0; i < mResults.size(); i++) {
                if (mSameData.get(mResults.get(i)) == null && mErrorImg.get(mResults.get(i)) == null) {       //说明是新增的图片
                    pos = i;
                    break;
                }
            }
            mySendMessage(pos, "0", START_S_C);
        }

    }

    /**
     * 发送图片上传的消息
     *
     * @param arg：第几个位置
     * @param lianXu：是否连续上传：0连续上传，1不连续上传
     * @param state：状态
     */
    private void mySendMessage(int arg, String lianXu, int state) {
        notClick = true;
        setContentSelected(false);
        Message msg = Message.obtain();
        msg.arg1 = arg;
        msg.obj = lianXu;
        msg.what = state;
        mHandler.sendMessage(msg);
    }


    /**
     * 上传图片地址和文字
     */
    void postFileQue(int pos, boolean isLianXu) {
        mTop.getTv_right().setClickable(false);
        // 压缩图片
        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);// 建立这个路径的文件或文件夹
        if (!path1.exists()) {// 如果不存在，就建立文件夹
            path1.mkdirs();
        }
        File file = new File(path1, "yuemei_" + pos + System.currentTimeMillis() + ".JPEG");
        String desPath = file.getPath();
        FileUtils.compressPicture(mResults.get(pos), desPath);
        // 压缩图片
        processImages = adapter.getProcessImage();
        ProcessImageView image = processImages.get(mResults.get(pos));
        mImageWidthHeight = FileUtils.getImageWidthHeight(desPath);
        mKey = QiNuConfig.getKey();
        MyUploadImage.getMyUploadImage(WriteSuibianLiaoActivity.this, pos, mHandler, desPath, image, isLianXu).uploadImage(mKey);
    }


    /**
     * 上传图片文件
     */
    void postTextQue() {
        ArrayList<JSONObject> typeData = new ArrayList<>();

        for (int i = 0; i < mResults.size(); i++) {
            if (mSameData.get(mResults.get(i)) != null) {
                typeData.add(mSameData.get(mResults.get(i)));
            }
        }


        uid = Utils.getUid();
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("cateid", cateid);
        maps.put("content", contentStr);
        maps.put("visibility", "0");
        maps.put("hosname", "0");
        maps.put("docname", "0");
        maps.put("fee", "0");
        maps.put("askorshare", "4");
        maps.put("image", typeData.toString());
        String cityss = Cfg.loadStr(mContext, "city_dingwei", "");
        if (cityss.length() > 0) {
            maps.put("city", cityss);
        }
        new PostTextQueApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mDialog.stopLoading();
                if ("1".equals(serverData.code)) {
                    WriteResultData data = JSONUtil.TransformWriteResult(serverData.data);
                    MyToast.makeImgToast(mContext, getResources().getDrawable(R.drawable.tips_submit_success2x), 1000).show();

                    String ifoneLogin = data.getOnelogin();
                    String url = data.getAppmurl();
                    String qid = data.get_id();
                    String isFirst2 = Cfg.loadStr(mContext, FinalConstant.ISFIRST2, "");

                    StatService.onEvent(WriteSuibianLiaoActivity.this, "003", "随便聊聊", 1);

                    if (isFirst2.equals("1")) {
                        Intent it1 = new Intent();
                        it1.putExtra("url", url);
                        it1.putExtra("qid", qid);
                        it1.setClass(mContext, DiariesAndPostsActivity.class);
                        startActivity(it1);
                        finish();
                    } else {
                        if (ifoneLogin.equals("1")) {// 第一次登录发帖
                            Intent it = new Intent();
                            it.setClass(mContext, SubmitSuccess1Activity.class);
                            it.putExtra("url", url);
                            it.putExtra("qid", qid);
                            startActivity(it);
                            Cfg.saveStr(mContext, FinalConstant.ISFIRST2, "1");
                            finish();
                        } else {
                            Intent it1 = new Intent();
                            it1.putExtra("url", url);
                            it1.putExtra("qid", qid);
                            it1.setClass(mContext, DiariesAndPostsActivity.class);
                            startActivity(it1);
                            finish();
                        }
                    }
                    sumitHttpCode("30");
                } else {
                    StatService.onEvent(WriteSuibianLiaoActivity.this, "017", "随便聊聊帖", 1);
                    ViewInject.toast(serverData.message);
                }
            }
        });
    }


    void sumitHttpCode(final String flag) {
        SumitHttpAip sumitHttpAip = new SumitHttpAip();
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", flag);
        maps.put("uid", uid);
        sumitHttpAip.getCallBack(mContext, maps, new BaseCallBackListener<JFJY1Data>() {
            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                if (jfjyData != null) {
                    String jifenNu = jfjyData.getIntegral();
                    String jyNu = jfjyData.getExperience();

                    if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                        MyToast.makeTexttext4Toast(mContext, jifenNu, jyNu, 1000).show();
                    } else {
                        if (!jifenNu.equals("0")) {
                            MyToast.makeTexttext2Toast(mContext, jifenNu, 1000).show();
                        } else {
                            if (!jyNu.equals("0")) {
                                MyToast.makeTexttext3Toast(mContext, jyNu, 1000).show();
                            }
                        }
                    }
                }
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (postContent.getText().toString().length() > 0 || mResults.size() > 0) {
                showDialogExitEdit();
            } else {
                finish();
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    private void initViewPager() {
        LayoutInflater inflater = LayoutInflater.from(this);
        grids = new ArrayList<GridView>();
        gView1 = (GridView) inflater.inflate(R.layout.grid1, null);

        List<Map<String, Object>> listItems = new ArrayList<>();
        // 生成28个表情
        for (int i = 0; i < 28; i++) {
            Map<String, Object> listItem = new HashMap<>();
            listItem.put("image", expressionImages[i]);
            listItems.add(listItem);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                if (arg2 == 27) {
                    // 动作按下
                    int action = KeyEvent.ACTION_DOWN;
                    // code:删除，其他code也可以，例如 code = 0
                    int code = KeyEvent.KEYCODE_DEL;
                    KeyEvent event = new KeyEvent(action, code);
                    postContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                } else {
                    Bitmap bitmap = null;
                    bitmap = BitmapFactory.decodeResource(getResources(), expressionImages[arg2 % expressionImages.length]);

                    bitmap = zoomImage(bitmap, 47, 47);
                    ImageSpan imageSpan = new ImageSpan(mContext, bitmap);

                    SpannableString spannableString = new SpannableString(expressionImageNames[arg2].substring(1, expressionImageNames[arg2].length() - 1));

                    spannableString.setSpan(imageSpan, 0, expressionImageNames[arg2].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    // 编辑框设置数据
                    postContent.append(spannableString);

                    // System.out.println("edit的内容 = " + spannableString);
                }
            }
        });
        grids.add(gView1);

        gView2 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView2);

        // grids.add(gView3);
        // System.out.println("GridView的长度 = " + grids.size());

        // 填充ViewPager的数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return grids.size();
            }

            @Override
            public void destroyItem(View container, int position, Object object) {
                ((ViewPager) container).removeView(grids.get(position));
            }

            @Override
            public Object instantiateItem(View container, int position) {
                ((ViewPager) container).addView(grids.get(position));
                return grids.get(position);
            }
        };

        viewPager.setAdapter(mPagerAdapter);
        // viewPager.setAdapter();

        viewPager.setOnPageChangeListener(new GuidePageChangeListener());
    }

    // ** 指引页面改监听器 */
    class GuidePageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // System.out.println("页面滚动" + arg0);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // System.out.println("换页了" + arg0);
        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    break;
                case 1:
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));

                    List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();

                    // 生成28个表情
                    for (int i = 0; i < 28; i++) {
                        Map<String, Object> listItem = new HashMap<String, Object>();
                        listItem.put("image", expressionImages1[i]);
                        listItems.add(listItem);
                    }

                    SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

                    gView2.setAdapter(simpleAdapter);
                    // 表情点击
                    gView2.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                postContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {

                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(getResources(), expressionImages1[pos % expressionImages1.length]);
                                bitmap = zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mContext, bitmap);
                                SpannableString spannableString = new SpannableString(expressionImageNames1[pos].substring(1, expressionImageNames1[pos].length() - 1));

                                spannableString.setSpan(imageSpan, 0, expressionImageNames1[pos].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                // 编辑框设置数据
                                postContent.append(spannableString);

                                // System.out.println("edit的内容 = " +
                                // spannableString);
                            }
                        }
                    });
                    break;
            }
        }
    }

    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width, (int) height, matrix, true);
        return bitmap;
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
