package com.module.commonview.module.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 裴成浩 on 2019/5/5
 */
public class OtherListApi implements BaseCallBackApi {

    private final HashMap<String, Object> mHashMap;

    public OtherListApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.BAIKE, "otherList", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    listener.onSuccess(serverData);
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
