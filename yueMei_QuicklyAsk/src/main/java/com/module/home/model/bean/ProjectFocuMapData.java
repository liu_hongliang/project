package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/2/26
 */
public class ProjectFocuMapData implements Parcelable {
    private String qid;
    private String url;
    private String flag;
    private String img;
    private String img_new;
    private String title;
    private String type;
    private String h_w;
    private String metro_line;
    private HashMap<String,String> event_params;

    protected ProjectFocuMapData(Parcel in) {
        qid = in.readString();
        url = in.readString();
        flag = in.readString();
        img = in.readString();
        img_new = in.readString();
        title = in.readString();
        type = in.readString();
        h_w = in.readString();
        metro_line = in.readString();
        event_params= (HashMap) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(qid);
        dest.writeString(url);
        dest.writeString(flag);
        dest.writeString(img);
        dest.writeString(img_new);
        dest.writeString(title);
        dest.writeString(type);
        dest.writeString(h_w);
        dest.writeString(metro_line);
        dest.writeSerializable(this.event_params);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProjectFocuMapData> CREATOR = new Creator<ProjectFocuMapData>() {
        @Override
        public ProjectFocuMapData createFromParcel(Parcel in) {
            return new ProjectFocuMapData(in);
        }

        @Override
        public ProjectFocuMapData[] newArray(int size) {
            return new ProjectFocuMapData[size];
        }
    };

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_new() {
        return img_new;
    }

    public void setImg_new(String img_new) {
        this.img_new = img_new;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getH_w() {
        return h_w;
    }

    public void setH_w(String h_w) {
        this.h_w = h_w;
    }

    public String getMetro_line() {
        return metro_line;
    }

    public void setMetro_line(String metro_line) {
        this.metro_line = metro_line;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
