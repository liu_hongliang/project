package com.module.base.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform2;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;

/**
 * 公有函数管理器
 * Created by 裴成浩 on 2019/3/12
 */
public class FunctionManager implements ManagerCallback {

    private final Context mContext;
    private int[] screenSize;

    public FunctionManager(Context context) {
        this.mContext = context;
        if (mContext instanceof Activity) {
            DisplayMetrics metric = new DisplayMetrics();
            ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(metric);
            screenSize = new int[]{metric.widthPixels, metric.heightPixels};
        }
    }

    /**
     * 启动新的activity
     *
     * @param activity
     */
    @Override
    public void goToActivity(Class activity) {
        Intent intent = new Intent(mContext, activity);
        mContext.startActivity(intent);
    }

    @Override
    public void goToActivity(Class activity, Bundle bundle) {
        Intent intent = new Intent(mContext, activity);
        if (bundle != null && bundle.size() != 0) {
            intent.putExtra("data", bundle);
        }
        mContext.startActivity(intent);
    }

    /**
     * Toast显示
     *
     * @param msg
     */
    @Override
    public void showShort(String msg) {
        MyToast.makeTextToast2(mContext, msg, MyToast.SHOW_TIME).show();
    }

    /**
     * 初始化TextView
     *
     * @param v：布局
     * @param id：控件id
     * @param txt：要设置的值
     */
    @Override
    public void setTextValue(View v, int id, String txt) {
        ((TextView) v.findViewById(id)).setText(txt);
    }

    /**
     * 设置网络图片src
     *
     * @param imageView
     * @param url
     */
    @Override
    public void setImageSrc(ImageView imageView, String url) {
        Glide.with(mContext).load(url).into(imageView);
    }

    /**
     * 设置网络圆形图片src
     *
     * @param imageView
     * @param url
     */
    @Override
    public void setCircleImageSrc(ImageView imageView, String url) {
        Glide.with(mContext).load(url).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(imageView);
    }

    /**
     * 设置网络圆角图片src
     *
     * @param imageView
     * @param url
     * @param round     : 圆角的角度，dp
     */
    @Override
    public void setRoundImageSrc(ImageView imageView, String url, int round) {
        Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.SOURCE).transform(new GlideRoundTransform(mContext, round)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(imageView);
    }

    /**
     * 设置本地圆角图片src
     *
     * @param imageView
     * @param drawable
     * @param round     : 圆角的角度，dp
     */
    @Override
    public void setRoundImageSrc(ImageView imageView, int drawable, int round) {
        Glide.with(mContext).load(drawable).transform(new GlideRoundTransform(mContext, round)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(imageView);
    }


    /**
     * 设置网络图片src（带占位图的）
     *
     * @param imageView
     * @param url
     */
    @Override
    public void setPlaceholderImageSrc(ImageView imageView, String url) {
        Glide.with(mContext).load(url).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(imageView);
    }


    /**
     * 设置本地图片src
     *
     * @param imageView
     * @param src
     */
    @Override
    public void setImgSrc(ImageView imageView, int src) {
        imageView.setImageResource(src);
    }

    /**
     * 设置背景
     *
     * @param imageView
     * @param background
     */
    @Override
    public void setImgBackground(ImageView imageView, int background) {
        imageView.setBackgroundResource(background);
    }

    /**
     * 获取本地颜色
     *
     * @param color
     * @return
     */
    @Override
    public int getLocalColor(int color) {
        return Utils.getLocalColor(mContext, color);
    }

    /**
     * 获取本地图片
     *
     * @param drawable
     * @return
     */
    @Override
    public Drawable getLocalDrawable(int drawable) {
        return Utils.getLocalDrawable(mContext, drawable);
    }

    /**
     * 自定义颜色
     *
     * @param color
     * @return
     */
    @Override
    public int setCustomColor(String color) {
        return Utils.setCustomColor(color);
    }

    /**
     * 数据存储
     */
    private SharedPreferences mSharedPreferences;

    @Override
    public String loadStr(String key, String defStr) {
        if (mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        }
        return mSharedPreferences.getString(key, defStr);
    }

    @Override
    @SuppressLint("ApplySharedPref")
    public void saveStr(String key, String value) {
        if (mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        }
        mSharedPreferences.edit().putString(key, value).commit();
    }

    @Override
    public int loadInt(String key, int defVal) {
        if (mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        }
        return mSharedPreferences.getInt(key, defVal);
    }

    @Override
    public void saveInt(String key, int value) {

        if (mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        }
        mSharedPreferences.edit().putInt(key, value).apply();
    }

    @Override
    @SuppressLint("CommitPrefEdits")
    public void clear() {
        if (mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        }
        mSharedPreferences.edit().clear();
    }

    /**
     * 获取屏幕宽度
     *
     * @return
     */
    @Override
    public int getWindowWidth() {
        if (screenSize != null && screenSize.length >= 1) {
            return screenSize[0];
        } else {
            return 0;
        }
    }

    /**
     * 获取屏幕高度
     *
     * @return
     */
    @Override
    public int getWindowheight() {
        if (screenSize != null && screenSize.length >= 2) {
            return screenSize[1];
        } else {
            return 0;
        }
    }


}
