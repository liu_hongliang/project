package com.quicklyask.entity;

import com.module.community.model.bean.HuangDeng;

import java.util.List;

public class VersionJCData {

	private String url;
	private String ver;
	private String status;
	private String time;
	private List<HuangDeng> title;


	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<HuangDeng> getTitle() {
		return title;
	}

	public void setTitle(List<HuangDeng> title) {
		this.title = title;
	}
}
