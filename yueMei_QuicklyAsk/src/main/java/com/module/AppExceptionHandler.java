package com.module;

import android.os.Environment;
import android.util.Log;


import com.quicklyask.util.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 文 件 名: AppExceptionHandler
 * 创 建 人: 原成昊
 * 创建日期: 2019-12-11 15:45
 * 邮   箱: 188897876@qq.com
 * 修改备注：全局捕获异常类
 */

public class AppExceptionHandler implements Thread.UncaughtExceptionHandler {
    private static final String SDCARD_ROOT = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
    public static final String BASE_PATH = SDCARD_ROOT + "yuemei/";
    public static final String LOG_PATH = BASE_PATH + "log/";
    private Thread.UncaughtExceptionHandler defaultExceptionHandler;

    static class Holder {
        static AppExceptionHandler instance = new AppExceptionHandler();
    }

    private AppExceptionHandler() {
        defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    public static AppExceptionHandler getInstance() {
        return Holder.instance;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

        File logDir = new File(LOG_PATH);
        if (!logDir.exists()) {
            logDir.mkdirs();
        }

        StringBuilder sb = new StringBuilder();
        File logFile = new File(getLogPath());
        if (!logFile.exists()) {
            AnalyticsConfig config = AnalyticsConfig.getInstance();
            sb.append(config.toString());
        }

        FileInputStream fis = null;
        int numSize = 0;
        //限制最大2MB
        try {
            fis = new FileInputStream(logFile);
            numSize = fis.available();
            if (fis != null && numSize > 2 * 1024 * 1024) {
                return;
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }

                if (numSize > 2 * 1024 * 1024) {
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //追加方式写入文件
        PrintWriter writer = null;
        FileOutputStream outputStream = null;
        try {
            File file = this.createFile();
            outputStream = new FileOutputStream(file, true);//追加方式
            writer = new PrintWriter(outputStream);

            final int BLANK_LINE_BUFFER = 5;
            for (int i = 0; i < BLANK_LINE_BUFFER; i++) {
                sb.append("\n");
            }

            writer.append(sb.toString());
            ex.printStackTrace(writer);

        } catch (Exception e) {
            Log.e("ExceptionHandler",
                    "uncaughtException, error: " + e.toString());
        } finally {
            if (writer != null) {
                writer.close();
            }
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (this.defaultExceptionHandler != null) {
            this.defaultExceptionHandler.uncaughtException(thread, ex);
        }
    }

    private File createFile() throws IOException {
        File file = new File(getLogPath());
        file.createNewFile();
        return file;
    }

    public String getLogPath() {
        String bugLogFilePath = LOG_PATH
                + Utils.getUid() + ".log";
        return bugLogFilePath;
    }
}

