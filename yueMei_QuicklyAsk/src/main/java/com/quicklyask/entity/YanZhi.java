package com.quicklyask.entity;

/**
 * Created by 裴成浩 on 2017/7/17.
 */

public class YanZhi {
    private String title;
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
