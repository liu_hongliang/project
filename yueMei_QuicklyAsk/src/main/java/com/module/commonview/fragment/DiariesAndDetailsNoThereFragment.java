package com.module.commonview.fragment;

import android.view.View;

import com.module.base.view.YMBaseFragment;
import com.quicklyask.activity.R;

/**
 * Created by 裴成浩 on 2018/6/28.
 */
public class DiariesAndDetailsNoThereFragment extends YMBaseFragment {

    public static DiariesAndDetailsNoThereFragment newInstance() {
        DiariesAndDetailsNoThereFragment fragment = new DiariesAndDetailsNoThereFragment();
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diary_posts_no_there;
    }

    @Override
    protected void initView(View view) {

    }

    @Override
    protected void initData(View view) {

    }
}
