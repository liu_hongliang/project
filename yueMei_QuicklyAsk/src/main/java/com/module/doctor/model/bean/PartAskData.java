/**
 * 
 */
package com.module.doctor.model.bean;

import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class PartAskData {
	private String _id;
	private String name;
	private List<PartAskDataList> list;

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the list
	 */
	public List<PartAskDataList> getList() {
		return list;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(List<PartAskDataList> list) {
		this.list = list;
	}

}
