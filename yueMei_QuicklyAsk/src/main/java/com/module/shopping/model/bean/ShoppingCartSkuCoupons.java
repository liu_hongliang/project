package com.module.shopping.model.bean;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/12/3
 */
public class ShoppingCartSkuCoupons {
    private String title;       //领券
    private String url;         //	跳转地址
    private List<CartSkuCouponsData> data;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<CartSkuCouponsData> getData() {
        return data;
    }

    public void setData(List<CartSkuCouponsData> data) {
        this.data = data;
    }
}
