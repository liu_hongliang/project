package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.cn.demo.pinyin.AssortView;
import com.cn.demo.pinyin.PinyinAdapter;
import com.module.base.api.BaseCallBackListener;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.model.api.PartListApi;
import com.module.community.model.bean.BBsListData550;
import com.module.doctor.controller.adapter.HotCityAdapter;
import com.module.doctor.model.api.HotCityApi;
import com.module.doctor.model.bean.CityDocDataitem;
import com.module.doctor.model.bean.GroupDiscData;
import com.module.doctor.model.bean.TaoPopItemIvData;
import com.module.doctor.view.ProjectDetailListFragment;
import com.module.home.controller.adapter.MyAdapter7;
import com.module.home.view.LoadingProgress;
import com.module.other.api.LookRijiApi;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.activity.interfaces.Project2ListSelectListener;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.DropDownListView;
import com.quicklyask.view.MyGridView;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.ViewInject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 整形日记
 *
 * Created by dwb on 16/5/6.
 */
public class ZhengXingRijiActivity extends FragmentActivity {

    private final String TAG = "ZhengXingRijiActivity";

    private ZhengXingRijiActivity mContex;

    // List
    private DropDownListView mlist;
    private int mCurPage = 1;
    private Handler mHandler;
    private List<BBsListData550> lvHotIssueData = new ArrayList<BBsListData550>();
    private List<BBsListData550> lvHotIssueMoreData = new ArrayList<BBsListData550>();
    private BBsListAdapter hotAdpter;

    private LinearLayout nodataTv;// 数据为空时候的显示

    private RelativeLayout partRly;
    private RelativeLayout cityRly;
    private TextView partTv;
    private TextView cityTv;
    private ImageView partIv;
    private ImageView cityIv;

    private RelativeLayout otherRly;
    private LinearLayout partSearchLy;

    private List<TaoPopItemIvData> lvGroupData = new ArrayList<TaoPopItemIvData>();

    private String sortStr = "2";// 筛选

    private CityPopwindows cityPop;

    private String cityName;

    // 城市筛选
    private ExpandableListView eListView;
    private AssortView assortView;
    private PinyinAdapter pinAdapter;
    private List<String> names;
    private TextView cityLocTv;
    private RelativeLayout othserRly;
    private View headView;
    private String autoCity;

    private List<CityDocDataitem> hotcityList;
    private MyGridView hotGridlist;
    private HotCityAdapter hotcityAdapter;

    private String partId = "0";
    private String p_partId = "0";
    private String partName;// 部位名

    public static int mPosition;
    private MyAdapter7 adapter;
    private ListView partlist;
    private ProjectDetailListFragment mFragment;

    private TextView titleTv;
    private LinearLayout backRly;

    private String type="1";

    private String cityName1;
    private LoadingProgress mDialog;
    private LookRijiApi lookRijiApi;
    private HashMap<String, Object> lookRijiMap = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acty_zhengxing_riji);
        mContex = ZhengXingRijiActivity.this;
        cityName = "全国";
        lookRijiApi = new LookRijiApi();
        mDialog = new LoadingProgress(mContex);
        findView();

        cityName1 = Cfg.loadStr(getApplicationContext(), FinalConstant.DWCITY,
                "");
        if (cityName1.length() > 0) {

            if (cityName1.equals("失败")) {
                cityName = "全国";
                cityTv.setText(cityName);

            } else if (cityName1.equals(cityName)) {

            } else {
                cityName = cityName1;
                cityTv.setText(cityName);
            }

        } else {
            cityName = "全国";
            cityTv.setText("全国");
        }

        initList();
        setListner();
    }

    void findView() {
        titleTv=findViewById(R.id.title_name);
        backRly=findViewById(R.id.title_bar_rly);

        mlist =  findViewById(R.id.my_doc_list_view);

        partRly =  findViewById(R.id.project_part_pop_rly1);
        cityRly = findViewById(R.id.project_sort_pop_rly);
        partTv = findViewById(R.id.project_part_pop_tv);
        cityTv = findViewById(R.id.project_sort_pop_tv);
        partIv = findViewById(R.id.project_part_pop_iv);
        cityIv = findViewById(R.id.project_sort_pop_iv);

        otherRly = findViewById(R.id.ly_content_ly1);
        partSearchLy = findViewById(R.id.part_search_ly);
        partlist = findViewById(R.id.pop_project_listview);

        nodataTv = findViewById(R.id.my_collect_post_tv_nodata);

        cityPop = new CityPopwindows(mContex, partRly);

        titleTv.setText("整形日记");

        loadPartList();
    }

    void setListner() {


        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        cityRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                partSearchLy.setVisibility(View.GONE);
                if (cityPop.isShowing()) {
                    cityPop.dismiss();
                } else {
                    cityPop.showAsDropDown(partRly, 0, 0);
                }
                initpop();
            }
        });

        otherRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                partSearchLy.setVisibility(View.GONE);
                initpop();
            }
        });

        partRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                loadPartList();
                if (partSearchLy.getVisibility() == View.GONE) {
                    partSearchLy.setVisibility(View.VISIBLE);
                } else {
                    partSearchLy.setVisibility(View.GONE);
                }
                initpop();
            }
        });

        backRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    void initpop() {
        if (partSearchLy.getVisibility() == View.VISIBLE) {
            partTv.setTextColor(Color.parseColor("#E95165"));
            partIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            partTv.setTextColor(Color.parseColor("#414141"));
            partIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
        if (cityPop.isShowing()) {
            cityTv.setTextColor(Color.parseColor("#E95165"));
            cityIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            cityTv.setTextColor(Color.parseColor("#414141"));
            cityIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
    }

    void loadPartList() {
        mPosition = 0;
        Map<String,Object> maps=new HashMap<>();
        maps.put("flag","3");
        new PartListApi().getCallBack(mContex, maps, new BaseCallBackListener<List<TaoPopItemIvData>>() {
            @Override
            public void onSuccess(List<TaoPopItemIvData> taoPopItemIvData) {
                if (taoPopItemIvData != null){
                    lvGroupData=taoPopItemIvData;
                    for (int i = 0; i < lvGroupData.size(); i++) {
                        if (p_partId.equals(lvGroupData.get(i)
                                .get_id())) {
                            mPosition = i;
                        }
                    }
                    adapter = new MyAdapter7(ZhengXingRijiActivity.this,
                            lvGroupData);
                    partlist.setAdapter(adapter);

                    // 创建MyFragment对象
                    mFragment = new ProjectDetailListFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                            .beginTransaction();
                    mFragment
                            .setCascadingMenuViewOnSelectListener(new NMProject2ListSelectListener());
                    fragmentTransaction
                            .replace(R.id.pop_fragment_container2,
                                    mFragment);

                    // 通过bundle传值给MyFragment
                    Bundle bundle = new Bundle();
                    bundle.putString("id",
                            lvGroupData.get(mPosition).get_id());
                    bundle.putString("z_id", partId);
                    mFragment.setArguments(bundle);
                    fragmentTransaction.commitAllowingStateLoss();
                }else{
                    ViewInject.toast("请求错误");
                }
            }
        });

        partlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {
                // 拿到当前位置
                mPosition = pos;
                p_partId = lvGroupData.get(pos).get_id();
                adapter = new MyAdapter7(ZhengXingRijiActivity.this,
                        lvGroupData);
                partlist.setAdapter(adapter);
                partlist.setSelection(pos);
                // 即使刷新adapter
                adapter.notifyDataSetChanged();
                mFragment = new ProjectDetailListFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                        .beginTransaction();
                mFragment
                        .setCascadingMenuViewOnSelectListener(new NMProject2ListSelectListener());
                fragmentTransaction.replace(R.id.pop_fragment_container2,
                        mFragment);
                // 通过bundle传值给MyFragment
                Bundle bundle = new Bundle();
                bundle.putString("id", lvGroupData.get(pos).get_id());
                bundle.putString("z_id", partId);
                mFragment.setArguments(bundle);
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

    }

    // 级联菜单选择回调接口
    class NMProject2ListSelectListener implements Project2ListSelectListener {

        @Override
        public void getValue(GroupDiscData projectItem) {
            partSearchLy.setVisibility(View.GONE);
            partName = projectItem.getCate_name();
            partId = projectItem.get_id();
            partTv.setText(partName);
            initpop();
            onreshData();
        }
    }

    void onreshData() {
        lvHotIssueData = null;
        lvHotIssueMoreData = null;
        mCurPage = 1;
        mDialog.startLoading();
        lodHotIssueData(true);
        mlist.setHasMore(true);
    }



    void initList() {

        mHandler = getHandler();
        mDialog.startLoading();
        lodHotIssueData(true);

        mlist.setOnDropDownListener(new DropDownListView.OnDropDownListener() {

            @Override
            public void onDropDown() {
                lvHotIssueData = null;
                lvHotIssueMoreData = null;
                mCurPage = 1;
                mDialog.startLoading();
                lodHotIssueData(true);
                mlist.setHasMore(true);
            }
        });

        mlist.setOnBottomListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lodHotIssueData(false);
            }
        });

        mlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos,
                                    long arg3) {

                if (null != lvHotIssueData && lvHotIssueData.size() > 0) {
                    String url = lvHotIssueData.get(pos).getUrl();
                    String qid = lvHotIssueData.get(pos).getQ_id();
                    String appmurl = lvHotIssueData.get(pos).getAppmurl();
                    WebUrlTypeUtil.getInstance(mContex).urlToApp(appmurl, "0", "0");
                }
            }
        });
    }


    void lodHotIssueData(final boolean isDonwn) {

        lookRijiMap.put("cateid",partId);
        lookRijiMap.put("page",mCurPage+"");
        lookRijiMap.put("sort",sortStr);
        lookRijiMap.put("type",type);
        lookRijiApi.getCallBack(mContex, lookRijiMap, new BaseCallBackListener<List<BBsListData550>>() {
            @Override
            public void onSuccess(List<BBsListData550> docListDatas) {
                Message msg = null;
                if (isDonwn) {
                    if (mCurPage == 1) {
                        lvHotIssueData = docListDatas;

                        msg = mHandler.obtainMessage(1);
                        msg.sendToTarget();
                    }
                } else {
                    mCurPage++;
                    lvHotIssueMoreData = docListDatas;
                    msg = mHandler.obtainMessage(2);
                    msg.sendToTarget();
                }
            }
        });

    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({ "NewApi", "SimpleDateFormat" })
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:

                        if (null!=lvHotIssueData && lvHotIssueData.size() > 0) {

                            nodataTv.setVisibility(View.GONE);
                            mlist.setVisibility(View.VISIBLE);

                            mDialog.stopLoading();

                            hotAdpter = new BBsListAdapter(
                                    ZhengXingRijiActivity.this, lvHotIssueData);
                            mlist.setAdapter(hotAdpter);

                            SimpleDateFormat dateFormat = new SimpleDateFormat(
                                    "MM-dd HH:mm:ss");

                            mlist.onDropDownComplete(getString(R.string.update_at)
                                    + dateFormat.format(new Date()));
                            mlist.onBottomComplete();
                        } else {
                            mDialog.stopLoading();
                            nodataTv.setVisibility(View.VISIBLE);
                            mlist.setVisibility(View.GONE);
                        }
                        break;
                    case 2:
                        if (null!=lvHotIssueMoreData && lvHotIssueMoreData.size() > 0) {
                            hotAdpter.add(lvHotIssueMoreData);
                            hotAdpter.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {
                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }


    /**
     * 地区下拉选择
     *
     * @author Rubin
     *
     */
    public class CityPopwindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public CityPopwindows(Context mContext, View v) {

            final View view = View.inflate(mContext, R.layout.pop_city_diqu,
                    null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.fade_ins));

            eListView = view.findViewById(R.id.elist1);
            assortView = view.findViewById(R.id.assort1);
            othserRly = view.findViewById(R.id.ly_content_ly1);

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            initViewData();
            autoCity = Cfg.loadStr(mContext, FinalConstant.LOCATING_CITY, "失败");
            cityLocTv.setText(autoCity);

            othserRly.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    dismiss();
                    initpop();
                }
            });
        }
    }


    @SuppressLint("InflateParams")
    void initViewData() {
        names = new ArrayList<String>();
        names.add("鞍山");
        names.add("保定");
        names.add("北京");
        names.add("常州");
        names.add("成都");

        names.add("大连");
        names.add("大庆");
        names.add("丹东");
        names.add("东莞");

        names.add("佛山");
        names.add("福州");
        names.add("阜阳");

        names.add("广州");
        names.add("贵阳");

        names.add("哈尔滨");
        names.add("邯郸");
        names.add("杭州");
        names.add("合肥");
        names.add("衡水");
        names.add("呼和浩特");

        names.add("吉林");
        names.add("济南");
        names.add("佳木斯");
        names.add("金华");

        names.add("昆明");

        names.add("兰州");
        names.add("临沂");
        names.add("柳州");
        names.add("洛阳");

        names.add("绵阳");

        names.add("南昌");
        names.add("南京");
        names.add("南宁");
        names.add("宁波");

        names.add("普洱");

        names.add("齐齐哈尔");
        names.add("秦皇岛");
        names.add("青岛");

        names.add("厦门");
        names.add("上海");
        names.add("绍兴");
        names.add("深圳");
        names.add("沈阳");
        names.add("石家庄");
        names.add("苏州");

        names.add("台州");
        names.add("泰安");
        names.add("太原");
        names.add("唐山");
        names.add("天津");
        names.add("通化");

        names.add("威海");
        names.add("潍坊");
        names.add("温州");
        names.add("乌鲁木齐");
        names.add("无锡");
        names.add("武汉");

        names.add("西安");
        names.add("西宁");

        names.add("烟台");
        names.add("阳江");
        names.add("延吉");
        names.add("宜昌");

        names.add("长春");
        names.add("长沙");
        names.add("郑州");
        names.add("重庆");
        names.add("珠海");
        names.add("淄博");

        LayoutInflater mInflater = getLayoutInflater();
        headView = mInflater.inflate(R.layout.main_city_select__head_560, null);
        eListView.addHeaderView(headView);
        hotGridlist = headView.findViewById(R.id.group_grid_list1);

        loadHotCity();

        RelativeLayout cityAll = headView
                .findViewById(R.id.city_all_doc_rly);
        cityAll.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                cityName = "全国";
                cityTv.setText(cityName);
                Cfg.saveStr(ZhengXingRijiActivity.this.mContex,
                        FinalConstant.DWCITY, cityName);
                cityPop.dismiss();
                initpop();

                onreshData();

            }
        });

        RelativeLayout cityAntuo = headView
                .findViewById(R.id.city_auto_loaction_rly);
        cityLocTv = headView.findViewById(R.id.doc_city_select_tv);

        // if (dwCity.length() > 0) {
        // cityLocTv.setText(dwCity + "(自动定位)");
        // } else {
        // cityLocTv.setText("获取位置失败");
        // }

        cityAntuo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String city = autoCity;
                if (city.equals("失败")) {
                    cityName = "全国";
                } else {
                    cityName = city;
                }
                cityTv.setText(cityName);

                Cfg.saveStr(ZhengXingRijiActivity.this.mContex,
                        FinalConstant.DWCITY, cityName);

                onreshData();

                cityPop.dismiss();
                initpop();
            }
        });

        pinAdapter = new PinyinAdapter(mContex, names);
        eListView.setAdapter(pinAdapter);

        eListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView arg0, View arg1,
                                        int groupPosition, int childPosition, long arg4) {
                int tolal = 0;

                for (int j = 0; j < groupPosition; j++) {
                    int chidsize1 = pinAdapter.getChildrenCount(j);
                    tolal = tolal + chidsize1;
                }
                tolal = tolal + childPosition;

                String city;
                city = names.get(tolal);
                cityName = city;
                cityTv.setText(cityName);
                Cfg.saveStr(ZhengXingRijiActivity.this.mContex,
                        FinalConstant.DWCITY, cityName);

                onreshData();

                cityPop.dismiss();
                initpop();

                return true;
            }
        });

        // 展开所有
        for (int i = 0, length = pinAdapter.getGroupCount(); i < length; i++) {
            eListView.expandGroup(i);
        }

        // 字母按键回调
        assortView.setOnTouchAssortListener(new AssortView.OnTouchAssortListener() {

            View layoutView = LayoutInflater.from(mContex).inflate(
                    R.layout.alert_dialog_menu_layout, null);

            TextView text = (TextView) layoutView.findViewById(R.id.content);
            RelativeLayout alRly = (RelativeLayout) layoutView
                    .findViewById(R.id.pop_city_rly);

            public void onTouchAssortListener(String str) {
                int index = pinAdapter.getAssort().getHashList()
                        .indexOfKey(str);
                if (index != -1) {
                    eListView.setSelectedGroup(index);
                }
            }

            @Override
            public void onTouchAssortUP() {

            }
        });
    }

    void loadHotCity() {
        new HotCityApi().getCallBack(mContex, new HashMap<String, Object>(), new BaseCallBackListener<List<CityDocDataitem>>() {
            @Override
            public void onSuccess(List<CityDocDataitem> cityDocDataitem) {
                hotcityList = cityDocDataitem;

                hotcityAdapter = new HotCityAdapter(mContex,
                        hotcityList);
                hotGridlist.setAdapter(hotcityAdapter);
                hotGridlist
                        .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(
                                    AdapterView<?> arg0,
                                    View arg1, int pos,
                                    long arg3) {
                                cityName = hotcityList.get(pos)
                                        .getName();

                                cityTv.setText(cityName);
                                Cfg.saveStr(
                                        ZhengXingRijiActivity.this.mContex,
                                        FinalConstant.DWCITY,
                                        cityName);

                                onreshData();

                                cityPop.dismiss();
                                initpop();
                            }
                        });
            }

        });
    }


    /*
     *
     * @see android.support.v4.app.FragmentActivity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}