package com.module.community.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.community.model.bean.BBsListData550;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import org.kymjs.aframe.ui.ViewInject;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/7/16.
 */
public class CommunityPostAdapter extends RecyclerView.Adapter<CommunityPostAdapter.ViewHolder> {

    private final String TAG = "CommunityPostAdapter";

    private List<BBsListData550> mHotIssues;
    private Context mContext;
    private LayoutInflater inflater;

    private int windowsWight;

    public CommunityPostAdapter(Context mContext, List<BBsListData550> mHotIssues) {
        this.mContext = mContext;
        this.mHotIssues = mHotIssues;
        Log.e(TAG, "mHotIssues个数是 == " + mHotIssues.size());
        inflater = LayoutInflater.from(mContext);

        windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_bbs_550, null);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {
        BBsListData550 hotIsData = mHotIssues.get(position);

        viewHolder.mBBsHeadContainer.setPadding(Utils.dip2px(10), 0, Utils.dip2px(10), 0);

        String userImg = hotIsData.getUser_img();
        Log.e(TAG, "userImg == " + userImg);
        if (userImg != null && !"".equals(userImg)) {
            Glide.with(mContext).load(hotIsData.getUser_img()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).bitmapTransform(new GlideCircleTransform(mContext)).dontAnimate().into(viewHolder.mBBsHeadPic);

        }

        viewHolder.mBBsName.setText(hotIsData.getUser_name());
        viewHolder.mBBsTitle.setText(hotIsData.getTitle());

        viewHolder.mTime.setText(hotIsData.getTime());

        String isFx = hotIsData.getIs_fanxian();
        if (null != isFx && isFx.equals("1")) {
            viewHolder.mFxTipsLy.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mFxTipsLy.setVisibility(View.GONE);

        }

        if (null != hotIsData.getAnswer_num() && !hotIsData.getAnswer_num().equals("0")) {
            viewHolder.mCommentLy.setVisibility(View.VISIBLE);
            viewHolder.mCommentNum.setText(hotIsData.getAnswer_num());
        } else {
            viewHolder.mCommentLy.setVisibility(View.INVISIBLE);
        }

        if (null != hotIsData.getTalent() && !hotIsData.getTalent().equals("0")) {
            viewHolder.mYuemeiIv.setVisibility(View.VISIBLE);
            if ("10".equals(hotIsData.getTalent())) {
                ViewGroup.LayoutParams layoutParams = viewHolder.mYuemeiIv.getLayoutParams();
                layoutParams.width = Utils.dip2px(46);
                layoutParams.height = Utils.dip2px(16);
                viewHolder.mYuemeiIv.setLayoutParams(layoutParams);
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.yuemei_officia_listl);
            } else if ("11".equals(hotIsData.getTalent())) {
                ViewGroup.LayoutParams layoutParams = viewHolder.mYuemeiIv.getLayoutParams();
                layoutParams.width = Utils.dip2px(15);
                layoutParams.height = Utils.dip2px(15);
                viewHolder.mYuemeiIv.setLayoutParams(layoutParams);
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if ("12".equals(hotIsData.getTalent()) || "13".equals(hotIsData.getTalent())) {
                ViewGroup.LayoutParams layoutParams = viewHolder.mYuemeiIv.getLayoutParams();
                layoutParams.width = Utils.dip2px(46);
                layoutParams.height = Utils.dip2px(16);
                viewHolder.mYuemeiIv.setLayoutParams(layoutParams);
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.renzheng_list);
            }

        } else {
            viewHolder.mYuemeiIv.setVisibility(View.GONE);
        }

        if (null != hotIsData.getView_num() && !hotIsData.getView_num().equals("0")) {
            viewHolder.mSeenum.setText(hotIsData.getView_num());
            viewHolder.mSeeLy.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mSeeLy.setVisibility(View.INVISIBLE);
        }

        if (null != hotIsData.getSet_tid()) {
            if (hotIsData.getSet_tid().equals("1")) {
                viewHolder.mJingIv.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mJingIv.setVisibility(View.GONE);
            }
        }

        // 标签
        if (null != hotIsData.getTag()) {
            if (hotIsData.getTag().size() > 0) {
                viewHolder.tagLy.setVisibility(View.VISIBLE);
                viewHolder.mTagLy.setVisibility(View.VISIBLE);
                int tagsize = hotIsData.getTag().size();
                String tagStr = "";
                for (int i = 0; i < tagsize; i++) {
                    tagStr = hotIsData.getTag().get(i).getName() + "  " + tagStr;
                }
                viewHolder.mTag.setText(tagStr);
            } else {
                viewHolder.mTagLy.setVisibility(View.GONE);
                viewHolder.tagLy.setVisibility(View.GONE);
            }
        } else {
            viewHolder.mTagLy.setVisibility(View.GONE);
            viewHolder.tagLy.setVisibility(View.GONE);
        }
        // 标签


        if (null != hotIsData.getPrice() && !hotIsData.getPrice().equals("0") && hotIsData.getPrice().length() > 0) {
            viewHolder.tagLy.setVisibility(View.VISIBLE);
            viewHolder.mPrice.setText("（￥" + hotIsData.getPrice() + "）");
        } else {
            viewHolder.mPrice.setVisibility(View.GONE);

            if (null != hotIsData.getTag()) {
                if (hotIsData.getTag().size() > 0) {
                    viewHolder.tagLy.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tagLy.setVisibility(View.GONE);
                }
            } else {
                viewHolder.tagLy.setVisibility(View.GONE);
            }

        }

        // 图片

        if (null != hotIsData.getPic()) {

            if (hotIsData.getPic().size() > 0) {
                if (hotIsData.getPic().size() == 1) {
                    viewHolder.mDuozhangLy.setVisibility(View.GONE);
                    viewHolder.mSingalLy.setVisibility(View.VISIBLE);

                    String ww = hotIsData.getPic().get(0).getWidth();
                    String hh = hotIsData.getPic().get(0).getHeight();

                    if (null != ww && null != hh && !ww.equals(hh)) {

                        int www = Integer.parseInt(ww);
                        int hhh = Integer.parseInt(hh);

                        viewHolder.mPicLy.setVisibility(View.GONE);
                        viewHolder.mPicLy_.setVisibility(View.VISIBLE);

                        ViewGroup.LayoutParams params = viewHolder.mSingalPic_.getLayoutParams();
                        params.height = ((windowsWight - 75) * hhh / www);
                        viewHolder.mSingalPic_.setLayoutParams(params);

                        try {

                            Glide.with(mContext).load(hotIsData.getPic().get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).dontAnimate().into(viewHolder.mSingalPic_);

                        } catch (OutOfMemoryError e) {
                            ViewInject.toast("内存不足");
                        }

                    } else {

                        viewHolder.mPicLy.setVisibility(View.VISIBLE);
                        viewHolder.mPicLy_.setVisibility(View.GONE);
                        try {
                            Glide.with(mContext).load(hotIsData.getPic().get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).dontAnimate().into(viewHolder.mSingalPic);

                        } catch (OutOfMemoryError e) {
                            ViewInject.toast("内存不足");
                        }

                    }

                    //判断有没有视频
                    if ("1".equals(hotIsData.getIs_video())) {
                        viewHolder.mTagVideo.setVisibility(View.VISIBLE);
                        viewHolder.mTagVideo_.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.mTagVideo.setVisibility(View.GONE);
                        viewHolder.mTagVideo_.setVisibility(View.GONE);
                    }


                } else if (hotIsData.getPic().size() > 1) {

                    viewHolder.mDuozhangLy.setVisibility(View.VISIBLE);
                    viewHolder.mSingalLy.setVisibility(View.GONE);
                    int sizeI = hotIsData.getPic().size();

                    if (sizeI >= 9) {
                        sizeI = 9;
                    }

                    try {
                        for (int i = 0; i < sizeI; i++) {
                            viewHolder.mPic[i].setVisibility(View.VISIBLE);
                            Log.e(TAG, "hotIsData.getPic().get(i).getImg() === " + hotIsData.getPic().get(i).getImg());
                            Glide.with(mContext).load(hotIsData.getPic().get(i).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).dontAnimate().into(viewHolder.mPic[i]);
                        }
                    } catch (OutOfMemoryError e) {
                        ViewInject.toast("内存不足");
                    }

                    int sizeT = 9 - sizeI;
                    if (sizeT > 0) {
                        for (int i = 0; i < sizeT; i++) {
                            viewHolder.mPic[8 - i].setVisibility(View.GONE);
                        }
                    }
                }
            } else {
                viewHolder.mDuozhangLy.setVisibility(View.GONE);
                viewHolder.mSingalLy.setVisibility(View.GONE);
            }
            // 图片
            if (hotIsData.getPic().size() > 6) {
                viewHolder.mTu2.setVisibility(View.VISIBLE);
                viewHolder.mTu3.setVisibility(View.VISIBLE);
            } else if (3 < hotIsData.getPic().size() && hotIsData.getPic().size() <= 6) {
                viewHolder.mTu2.setVisibility(View.VISIBLE);
                viewHolder.mTu3.setVisibility(View.GONE);
            } else {
                viewHolder.mTu2.setVisibility(View.GONE);
                viewHolder.mTu3.setVisibility(View.GONE);
            }
        } else {
            viewHolder.mTu2.setVisibility(View.GONE);
            viewHolder.mTu3.setVisibility(View.GONE);
            viewHolder.mSingalLy.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mHotIssues.size();
    }

    public void clearData() {
        mHotIssues.clear();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mBBsHeadContainer;

        ImageView mBBsHeadPic;
        TextView mBBsName;
        TextView mBBsTitle;

        LinearLayout mSingalLy;
        ImageView mSingalPic;
        ImageView mSingalPic_;

        LinearLayout mDuozhangLy;
        ImageView mPic[] = new ImageView[9];

        LinearLayout mTagLy;
        TextView mTag;

        TextView mPrice;
        TextView mTime;
        TextView mCommentNum;

        ImageView mYuemeiIv;
        ImageView mDocRenIv;

        LinearLayout mTu2;
        LinearLayout mTu3;
        LinearLayout tagLy;

        LinearLayout mCommentLy;

        LinearLayout mSeeLy;

        TextView mSeenum;
        ImageView mJingIv;

        LinearLayout mFxTipsLy;

        RelativeLayout mPicLy;
        RelativeLayout mPicLy_;


        ImageView mTagVideo;
        ImageView mTagVideo_;


        public int flag;

        public ViewHolder(View itemView) {
            super(itemView);
            flag = getLayoutPosition();

            mBBsHeadContainer = itemView.findViewById(R.id.bbs_list_head_container);
            mBBsHeadPic = itemView.findViewById(R.id.bbs_list_head_image_iv);
            mBBsName = itemView.findViewById(R.id.bbs_list_name_tv);
            mBBsTitle = itemView.findViewById(R.id.bbs_list_title_tv);

            mDuozhangLy = itemView.findViewById(R.id.bbs_list_duozhang_ly);
            mPic[0] = itemView.findViewById(R.id.bbs_list_pic1);
            mPic[1] = itemView.findViewById(R.id.bbs_list_pic2);
            mPic[2] = itemView.findViewById(R.id.bbs_list_pic3);
            mPic[3] = itemView.findViewById(R.id.bbs_list_pic4);
            mPic[4] = itemView.findViewById(R.id.bbs_list_pic5);
            mPic[5] = itemView.findViewById(R.id.bbs_list_pic6);
            mPic[6] = itemView.findViewById(R.id.bbs_list_pic7);
            mPic[7] = itemView.findViewById(R.id.bbs_list_pic8);
            mPic[8] = itemView.findViewById(R.id.bbs_list_pic9);

            mSingalLy = itemView.findViewById(R.id.bbs_list_pic_danzhang_ly);
            mSingalPic = itemView.findViewById(R.id.bbs_list_pic_danzhang);
            mSingalPic_ = itemView.findViewById(R.id.bbs_list_pic_danzhang_);

            mTagLy = itemView.findViewById(R.id.bbs_list_tag_ly);
            mTag = itemView.findViewById(R.id.bbs_list_tag_tv);

            mPrice = itemView.findViewById(R.id.bbs_list_price_tv);
            mTime = itemView.findViewById(R.id.bbs_list_time_tv);
            mCommentNum = itemView.findViewById(R.id.bbs_list_comment_num_tv);

            mYuemeiIv = itemView.findViewById(R.id.bbs_list_yuemei_daren_iv);

            mTu2 = itemView.findViewById(R.id.bbs_list_h_ly2);
            mTu3 = itemView.findViewById(R.id.bbs_list_h_ly3);

            tagLy = itemView.findViewById(R.id.tag_ly_550);

            mCommentLy = itemView.findViewById(R.id.bbs_list_comment_ly);

            mSeenum = itemView.findViewById(R.id.bbs_list_see_num_tv);
            mJingIv = itemView.findViewById(R.id.bbs_list_jinghua_iv);
            mDocRenIv = itemView.findViewById(R.id.bbs_list_doc_daren_iv);

            mFxTipsLy = itemView.findViewById(R.id.note_fanxian_tips_ly);

            mSeeLy = itemView.findViewById(R.id.bbs_list_see_ly);


            mPicLy = itemView.findViewById(R.id.bbs_list_danzhang);
            mPicLy_ = itemView.findViewById(R.id.bbs_list_danzhang_);

            mTagVideo = itemView.findViewById(R.id.bbs_pic_tag);
            mTagVideo_ = itemView.findViewById(R.id.bbs_pic_tag_);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemCallBackListener != null){
                        itemCallBackListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

        }
    }

    public List<BBsListData550> getmData() {
        return mHotIssues;
    }

    public void addData(List<BBsListData550> data) {
        mHotIssues.addAll(data);
        notifyDataSetChanged();
    }

    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }
}