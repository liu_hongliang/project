package com.module.doctor.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.module.community.model.bean.BBsListData550;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dwb on 17/2/13.
 */

public class TeyaoFabiaoAdapter extends BaseAdapter {

    private final String TAG = "TeyaoFabiaoAdapter";

    private List<BBsListData550> mDoctorData = new ArrayList<BBsListData550>();
    private Context mContext;
    private LayoutInflater inflater;
    private BBsListData550 doctorData;
    ViewHolder viewHolder;


    public TeyaoFabiaoAdapter(Context mContext, List<BBsListData550> mDoctorData) {
        this.mContext = mContext;
        this.mDoctorData = mDoctorData;
        inflater = LayoutInflater.from(mContext);

    }

    static class ViewHolder {
        public TextView mTipsTv;
        public TextView mTitleTv;
        public TextView mLiulanTv;
    }

    @Override
    public int getCount() {
        return mDoctorData.size();
    }

    @Override
    public Object getItem(int position) {
        return mDoctorData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({ "NewApi", "InlinedApi" })
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_teyao_docfabiao, null);
            viewHolder = new ViewHolder();

            viewHolder.mTipsTv= convertView.findViewById(R.id.teyao_item_tips_tv);
            viewHolder.mTitleTv= convertView.findViewById(R.id.teyao_item_title_tv);
            viewHolder.mLiulanTv= convertView.findViewById(R.id.teyao_item_liulan_tv);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }



        doctorData = mDoctorData.get(position);


        if(doctorData.getAskorshare().endsWith("5")){
            viewHolder.mTipsTv.setText("案例");
        }else {
            viewHolder.mTipsTv.setText("观点");
        }


        viewHolder.mTitleTv.setText(doctorData.getTitle());

        viewHolder.mLiulanTv.setText(doctorData.getView_num()+"人浏览");


        convertView.setBackgroundResource(R.color.white);
        return convertView;
    }

    public void add(List<BBsListData550> infos) {
        mDoctorData.addAll(infos);
    }
}