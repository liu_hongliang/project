package com.quicklyask.entity;

import java.util.HashMap;

public class TaoQuickReply {
    private String show_content;
    private String send_content;
    private String send_QuickReply;
    private HashMap<String,String> event_params;

    public String getShow_content() {
        return show_content;
    }

    public void setShow_content(String show_content) {
        this.show_content = show_content;
    }

    public String getSend_content() {
        return send_content;
    }

    public void setSend_content(String send_content) {
        this.send_content = send_content;
    }

    public String getSend_QuickReply() {
        return send_QuickReply;
    }

    public void setSend_QuickReply(String send_QuickReply) {
        this.send_QuickReply = send_QuickReply;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
