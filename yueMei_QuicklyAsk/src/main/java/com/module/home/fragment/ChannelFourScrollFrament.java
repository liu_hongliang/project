package com.module.home.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseFragment;
import com.module.base.view.YMRecyclerViewAdapter;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebViewUrlLoading;
import com.module.home.controller.adapter.ChannelFourScrollAdapter;
import com.module.home.controller.adapter.ProjectBrandAdapter;
import com.module.home.model.bean.LabelEncyclopedia;
import com.module.home.model.bean.LabelEncyclopediaBtnList;
import com.module.home.model.bean.ProjectDetailsBrand;
import com.module.home.model.bean.ProjectDetailsData;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;

import butterknife.BindView;

/**
 * 设置四级滑动头部
 *
 * @author 裴成浩
 * @data 2019/11/1
 */
public class ChannelFourScrollFrament extends YMBaseFragment {

    @BindView(R.id.fragment_channel_four_top_click)
    FrameLayout mTopClick;
    @BindView(R.id.fragment_channel_four_top_image)
    ImageView mTopImage;
    @BindView(R.id.fragment_channel_four_top_title)
    TextView mTitle;
    @BindView(R.id.fragment_channel_four_top_desc)
    TextView mDesc;
    @BindView(R.id.fragment_channel_four_top_effective_time)
    TextView mEffectiveTime;
    @BindView(R.id.fragment_channel_four_top_reference_price)
    TextView mReferencePrice;

    @BindView(R.id.project_details_fragment_brand)
    LinearLayout mFragmentBrand;
    @BindView(R.id.project_details_fragment_brand_all)
    TextView mFragmentBrandAll;
    @BindView(R.id.project_details_fragment_brand_rec)
    RecyclerView mFragmentBrandRec;

    @BindView(R.id.fragment_channel_four_top_recycler)
    RecyclerView mRecycler;
    private LabelEncyclopedia data;
    private ProjectDetailsBrand brand;

    public static ChannelFourScrollFrament newInstance(ProjectDetailsData data) {
        ChannelFourScrollFrament channelFourScrollFrament = new ChannelFourScrollFrament();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        channelFourScrollFrament.setArguments(bundle);
        return channelFourScrollFrament;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_channel_four_top;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            ProjectDetailsData projectDetailsData = getArguments().getParcelable("data");
            if (projectDetailsData != null) {
                data = projectDetailsData.getLabelEncyclopedia();
                brand = projectDetailsData.getBrand();
            }
            setData(projectDetailsData);
        }
    }


    @Override
    protected void initData(View view) {

    }

    /**
     * 品牌馆
     *
     * @param brand
     */
    private void setBrand(final ProjectDetailsBrand brand) {
        Log.e(TAG, "brand.getBrand_list() == " + brand.getBrand_list().size());
        if (brand.getBrand_list().size() != 0) {
            mFragmentBrand.setVisibility(View.VISIBLE);
            mFragmentBrandRec.setVisibility(View.VISIBLE);

            if ("1".equals(brand.getIsShowAllBtn())) {
                mFragmentBrandAll.setVisibility(View.VISIBLE);
                mFragmentBrandAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String jumpUrl = brand.getJumpUrl();
                        if (!TextUtils.isEmpty(jumpUrl)) {
                            WebViewUrlLoading.getInstance().parserPagrms(jumpUrl);
                        }
                    }
                });
            } else {
                mFragmentBrandAll.setVisibility(View.GONE);
            }

            mFragmentBrandRec.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            ProjectBrandAdapter projectBrandAdapter = new ProjectBrandAdapter(mContext, brand.getBrand_list());
            mFragmentBrandRec.setAdapter(projectBrandAdapter);

        } else {
            mFragmentBrand.setVisibility(View.GONE);
            mFragmentBrandRec.setVisibility(View.GONE);
        }
    }


    /**
     * 设置数据
     * @param projectDetailsData
     */
    public void setData(ProjectDetailsData projectDetailsData) {
        if (projectDetailsData != null) {
            data = projectDetailsData.getLabelEncyclopedia();
            brand = projectDetailsData.getBrand();
        }

        if (data != null) {
            mTitle.setText(data.getTitle());
            mDesc.setText(data.getDesc());
            mEffectiveTime.setText(data.getEffective_time());
            mReferencePrice.setText(data.getReference_price());

            //设置背景图
            Log.e(TAG, "data.getBack_img() === " + data.getBack_img());
            if (!TextUtils.isEmpty(data.getBack_img())) {
                mFunctionManager.setImageSrc(mTopImage, data.getBack_img());
            }

            //点击事件
            mTopClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String btn_url = data.getUrl();
                    YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                    if (!TextUtils.isEmpty(btn_url)) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(btn_url);
                    }
                }
            });

            ScrollLayoutManager scrollLinear = new ScrollLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            scrollLinear.setScrollEnable(false);
            ChannelFourScrollAdapter channelFourScrollAdapter = new ChannelFourScrollAdapter(data.getBtn_list());
            mRecycler.setLayoutManager(scrollLinear);
            mRecycler.setAdapter(channelFourScrollAdapter);

            channelFourScrollAdapter.setOnItemClickListener(new YMRecyclerViewAdapter.OnItemClickListener<LabelEncyclopediaBtnList>() {
                @Override
                public void onItemClick(View view, int position, LabelEncyclopediaBtnList data) {
                    String btn_url = data.getBtn_url();
                    YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                    if (!TextUtils.isEmpty(btn_url)) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(btn_url);
                    }
                }
            });
        }

        //品牌馆
        if (brand != null) {
            setBrand(brand);
        }
    }
}
