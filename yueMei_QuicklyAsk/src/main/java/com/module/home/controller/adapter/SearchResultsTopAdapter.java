package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.doctor.view.MzRatingBar;
import com.module.home.model.bean.SearchTitleDataData;
import com.quicklyask.activity.R;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 裴成浩 on 2019/5/22
 */
public class SearchResultsTopAdapter extends RecyclerView.Adapter<SearchResultsTopAdapter.ViewHolder> {

    private final Activity mContext;
    private final List<SearchTitleDataData> mData;
    private final String mType;
    private final FunctionManager mFunctionManager;
    private final String mKey;
    private String TAG = "SearchResultsTopAdapter";

    public SearchResultsTopAdapter(Activity context, List<SearchTitleDataData> data, String type, String key) {
        this.mContext = context;
        this.mData = data;
        this.mType = type;
        this.mKey = key;
        //获取方法管理器
        mFunctionManager = new FunctionManager(mContext);
    }

    @NonNull
    @Override
    public SearchResultsTopAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_search_results_top, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SearchResultsTopAdapter.ViewHolder viewHolder, int i) {
        if (mData != null) {
            SearchTitleDataData sttdata = mData.get(i);
            //头像设置
            mFunctionManager.setCircleImageSrc(viewHolder.mImage, sttdata.getImg());

            //评分
            String rate = sttdata.getRate();
            Log.e(TAG, "rate == " + rate);
            if (!TextUtils.isEmpty(rate) && !"0".equals(rate)) {
                float num = Float.parseFloat(rate);
                num = num / 5 * 100;
                viewHolder.mRatingbar.setMax(100);
                viewHolder.mRatingbar.setProgress((int) num);
            } else {
                viewHolder.mScore.setVisibility(View.GONE);
            }

            //预定人数
            String sku_order_num = sttdata.getSku_order_num();
            Log.e(TAG, "sku_order_num == " + sku_order_num);
            if (!TextUtils.isEmpty(sku_order_num) && !"0".equals(sku_order_num)) {
                viewHolder.mReservation.setVisibility(View.VISIBLE);
                viewHolder.mReservation.setText(sku_order_num + "预订");
            } else {
                viewHolder.mReservation.setVisibility(View.GONE);
            }

            //服务人数
            String people = sttdata.getPeople();
            Log.e(TAG, "people == " + people);
            if (!TextUtils.isEmpty(people) && !"0".equals(people)) {
                viewHolder.mService.setVisibility(View.VISIBLE);
                viewHolder.mService.setText(people + "案例");
            } else {
                viewHolder.mService.setVisibility(View.GONE);
            }

            //横杠的显示隐藏
            if (viewHolder.mReservation.getVisibility() == View.VISIBLE && viewHolder.mService.getVisibility() == View.VISIBLE) {
                viewHolder.mMiddleLine.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mMiddleLine.setVisibility(View.GONE);
            }


            //医生/医院 区分
            if ("doctor".equals(mType)) {
                View docView = View.inflate(mContext, R.layout.item_search_results_top_doctor, viewHolder.mTopContainer);
                TextView docName = docView.findViewById(R.id.search_results_top_doc_name);
                TextView docLevel = docView.findViewById(R.id.search_results_top_doc_level);
                ImageView docImg = docView.findViewById(R.id.search_results_top_doc_img);

                //医生名称
                docName.setText(setNameString(sttdata.getName()));

                //职称
                docLevel.setText(sttdata.getTitle());
                if ("5".equals(sttdata.getTalent())) {
                    docImg.setBackgroundResource(R.drawable.user_tag_5_2x);
                } else if ("6".equals(sttdata.getTalent())) {
                    docImg.setBackgroundResource(R.drawable.user_tag_6_2x);
                }

            } else {
                View hosView = View.inflate(mContext, R.layout.item_search_results_top_hospital, viewHolder.mTopContainer);
                TextView hosName = hosView.findViewById(R.id.search_results_top_hos_name);
                ImageView hosImg = hosView.findViewById(R.id.search_results_top_hos_img);

                //医院名称
                hosName.setText(setNameString(sttdata.getName()));

                if ("1".equals(sttdata.getKind())) {
                    hosImg.setBackgroundResource(R.drawable.gongli_2x);
                } else {
                    hosImg.setBackgroundResource(R.drawable.minying_2x);
                }
            }

            //地址
            String hospital = sttdata.getHospital();
            Log.e(TAG, "hospital == " + hospital);
            if (!TextUtils.isEmpty(hospital)) {
                viewHolder.mAddress.setVisibility(View.VISIBLE);
                viewHolder.mAddress.setText(hospital);
            } else {
                viewHolder.mAddress.setVisibility(View.INVISIBLE);
            }

            //距离
            String distance = sttdata.getDistance();
            Log.e(TAG, "distance == " + distance);
            if (!TextUtils.isEmpty(distance)) {
                viewHolder.mDistance.setVisibility(View.VISIBLE);
                viewHolder.mDistance.setText(distance);
            } else {
                viewHolder.mDistance.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 返回
     *
     * @param name
     * @return
     */
    private SpannableString setNameString(String name) {
        SpannableString text;
        //名称设置
        Log.e(TAG, "name == " + name);
        Log.e(TAG, "mKey == " + mKey);
        if (!TextUtils.isEmpty(mKey)) {
            SpannableString s = new SpannableString(name);
            Pattern p = Pattern.compile(mKey);     //这里为关键字
            Matcher m = p.matcher(s);
            while (m.find()) {
                int start = m.start();
                int end = m.end();
                Log.e(TAG, "start == " + start);
                Log.e(TAG, "end == " + end);
                s.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            text = s;
        } else {
            text = new SpannableString(name);
        }

        return text;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImage;
        FrameLayout mTopContainer;
        //        TextView mName;
//        TextView mLevel;
//        ImageView mTitle;
        TextView mAddress;
        TextView mDistance;
        LinearLayout mScore;
        MzRatingBar mRatingbar;
        TextView mReservation;
        View mMiddleLine;
        TextView mService;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.search_results_top_image);
            mTopContainer = itemView.findViewById(R.id.search_results_top_container);
//            mName = itemView.findViewById(R.id.search_results_top_name);
//            mLevel = itemView.findViewById(R.id.search_results_top_level);
//            mTitle = itemView.findViewById(R.id.search_results_top_title);
            mAddress = itemView.findViewById(R.id.search_results_top_address);
            mDistance = itemView.findViewById(R.id.search_results_top_distance);
            mScore = itemView.findViewById(R.id.search_results_top_score);
            mRatingbar = itemView.findViewById(R.id.search_results_top_ratingbar);
            mReservation = itemView.findViewById(R.id.search_results_top_reservation);
            mMiddleLine = itemView.findViewById(R.id.search_results_top_middle_line);
            mService = itemView.findViewById(R.id.search_results_top_service);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(mData.get(getLayoutPosition()), getLayoutPosition());
                    }
                }
            });
        }
    }

    public interface OnEventClickListener {

        void onItemClick(SearchTitleDataData data, int position);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
