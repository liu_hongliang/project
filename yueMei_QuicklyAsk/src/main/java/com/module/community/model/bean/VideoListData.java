package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.commonview.module.bean.ChatDataBean;
import com.module.commonview.module.bean.DiaryTagList;
import com.module.commonview.module.bean.DiaryUserdataBean;
import com.module.commonview.module.bean.PostContentVideo;
import com.module.taodetail.model.bean.HomeTaoData;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/5/9
 */
public class VideoListData implements Parcelable {
    private String id;                                  //帖子Id
    private String p_id;                                //评论用到的
    private String askorshare;                          //评论用到的
    private HashMap<String, String> shareClickData;     //分享点击统计数据
    private DiaryUserdataBean userdata;                 //用户信息
    private List<HomeTaoData> taolist;                  //淘整形信息列表
    private String title;                               //标题
    private List<VideoTaoTimeData> videoTaoTime;        //视频时长对应淘整形id
    private List<DiaryTagList> tag;                     //标签
    private String appmurl;                             //跳转地址【走url识别】
    private int agree_num;                              //点赞数
    private String isAgree;                             //是否点赞 0:未点赞 1:已点赞
    private String answer_num;                          //评论数
    private int share_num;                              //分享数
    private BBsShareData shareData;                     //分享数据
    private ChatDataBean chatData;                      //咨询数据
    private PostContentVideo videoData;                 //视频数据
    private HashMap<String, String> event_params;       //分享点击
    private HashMap<String, String> postEventParams;    //点击帖子详情
    private HashMap<String,String> likeEventParams;
    private HashMap<String,String> shoppingCartEventParams;
    private HomeTaoData selected_tao;                   //选中的tao数据

    protected VideoListData(Parcel in) {
        id = in.readString();
        p_id = in.readString();
        askorshare = in.readString();
        userdata = in.readParcelable(DiaryUserdataBean.class.getClassLoader());
        taolist = in.createTypedArrayList(HomeTaoData.CREATOR);
        title = in.readString();
        tag = in.createTypedArrayList(DiaryTagList.CREATOR);
        appmurl = in.readString();
        agree_num = in.readInt();
        isAgree = in.readString();
        answer_num = in.readString();
        share_num = in.readInt();
        shareData = in.readParcelable(BBsShareData.class.getClassLoader());
        chatData = in.readParcelable(ChatDataBean.class.getClassLoader());
        videoData = in.readParcelable(PostContentVideo.class.getClassLoader());
        selected_tao = in.readParcelable(HomeTaoData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(p_id);
        dest.writeString(askorshare);
        dest.writeParcelable(userdata, flags);
        dest.writeTypedList(taolist);
        dest.writeString(title);
        dest.writeTypedList(tag);
        dest.writeString(appmurl);
        dest.writeInt(agree_num);
        dest.writeString(isAgree);
        dest.writeString(answer_num);
        dest.writeInt(share_num);
        dest.writeParcelable(shareData, flags);
        dest.writeParcelable(chatData, flags);
        dest.writeParcelable(videoData, flags);
        dest.writeParcelable(selected_tao, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoListData> CREATOR = new Creator<VideoListData>() {
        @Override
        public VideoListData createFromParcel(Parcel in) {
            return new VideoListData(in);
        }

        @Override
        public VideoListData[] newArray(int size) {
            return new VideoListData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getAskorshare() {
        return askorshare;
    }

    public void setAskorshare(String askorshare) {
        this.askorshare = askorshare;
    }

    public HashMap<String, String> getShareClickData() {
        return shareClickData;
    }

    public void setShareClickData(HashMap<String, String> shareClickData) {
        this.shareClickData = shareClickData;
    }

    public DiaryUserdataBean getUserdata() {
        return userdata;
    }

    public void setUserdata(DiaryUserdataBean userdata) {
        this.userdata = userdata;
    }

    public List<HomeTaoData> getTaolist() {
        return taolist;
    }

    public void setTaolist(List<HomeTaoData> taolist) {
        this.taolist = taolist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<VideoTaoTimeData> getVideoTaoTime() {
        return videoTaoTime;
    }

    public void setVideoTaoTime(List<VideoTaoTimeData> videoTaoTime) {
        this.videoTaoTime = videoTaoTime;
    }

    public List<DiaryTagList> getTag() {
        return tag;
    }

    public void setTag(List<DiaryTagList> tag) {
        this.tag = tag;
    }

    public String getAppmurl() {
        return appmurl;
    }

    public void setAppmurl(String appmurl) {
        this.appmurl = appmurl;
    }

    public int getAgree_num() {
        return agree_num;
    }

    public void setAgree_num(int agree_num) {
        this.agree_num = agree_num;
    }

    public String getIsAgree() {
        return isAgree;
    }

    public void setIsAgree(String isAgree) {
        this.isAgree = isAgree;
    }

    public String getAnswer_num() {
        return answer_num;
    }

    public void setAnswer_num(String answer_num) {
        this.answer_num = answer_num;
    }

    public int getShare_num() {
        return share_num;
    }

    public void setShare_num(int share_num) {
        this.share_num = share_num;
    }

    public BBsShareData getShareData() {
        return shareData;
    }

    public void setShareData(BBsShareData shareData) {
        this.shareData = shareData;
    }

    public ChatDataBean getChatData() {
        return chatData;
    }

    public void setChatData(ChatDataBean chatData) {
        this.chatData = chatData;
    }

    public PostContentVideo getVideoData() {
        return videoData;
    }

    public void setVideoData(PostContentVideo videoData) {
        this.videoData = videoData;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public HashMap<String, String> getPostEventParams() {
        return postEventParams;
    }

    public void setPostEventParams(HashMap<String, String> postEventParams) {
        this.postEventParams = postEventParams;
    }

    public HashMap<String, String> getLikeEventParams() {
        return likeEventParams;
    }

    public void setLikeEventParams(HashMap<String, String> likeEventParams) {
        this.likeEventParams = likeEventParams;
    }

    public HashMap<String, String> getShoppingCartEventParams() {
        return shoppingCartEventParams;
    }

    public void setShoppingCartEventParams(HashMap<String, String> shoppingCartEventParams) {
        this.shoppingCartEventParams = shoppingCartEventParams;
    }

    public HomeTaoData getSelected_tao() {
        return selected_tao;
    }

    public void setSelected_tao(HomeTaoData selected_tao) {
        this.selected_tao = selected_tao;
    }
}
