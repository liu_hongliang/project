package com.module.my.model.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.GetCodeData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * Created by Administrator on 2017/10/27.
 */

public class ShareDataApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "getinvitation", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if ("1".equals(mData.code)){
                    GetCodeData getCodeData = JSONUtil.TransformInvi(mData.data);
                    listener.onSuccess(getCodeData);
                }
            }
        });
    }
}
