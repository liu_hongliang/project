package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/12.
 */
public class PhotoBrowsListPic implements Parcelable{

    private String img;
    private String pic_id;
    private String images;
    private String width;
    private String height;
    private String weight;
    private String is_video;
    private String video_url;



    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getIs_video() {
        return is_video;
    }

    public void setIs_video(String is_video) {
        this.is_video = is_video;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getPic_id() {
        return pic_id;
    }

    public void setPic_id(String pic_id) {
        this.pic_id = pic_id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    protected PhotoBrowsListPic(Parcel in) {
        img = in.readString();
        width = in.readString();
        height = in.readString();
        is_video = in.readString();
        video_url = in.readString();
        pic_id = in.readString();
        images = in.readString();
        weight = in.readString();

    }
    public static final Creator<PhotoBrowsListPic>CREATOR = new Creator<PhotoBrowsListPic>(){

        @Override
        public PhotoBrowsListPic createFromParcel(Parcel source) {
            return new PhotoBrowsListPic(source);
        }

        @Override
        public PhotoBrowsListPic[] newArray(int size) {
            return new PhotoBrowsListPic[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
        dest.writeString(width);
        dest.writeString(height);
        dest.writeString(is_video);
        dest.writeString(video_url);
        dest.writeString(pic_id);
        dest.writeString(images);
        dest.writeString(weight);
    }
}
