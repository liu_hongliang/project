package com.module.commonview.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.module.bean.PostOtherpic;
import com.quicklyask.activity.R;
import com.quicklyask.view.YueMeiVideoView;

import butterknife.BindView;

/**
 * 视频播放器的模块
 * Created by 裴成浩 on 2018/8/16.
 */
public class PostVideoViewFragment extends YMBaseFragment {

    @BindView(R.id.fragment_diary_posts_video)
    public FrameLayout mVideoImage;                     //容器
    @BindView(R.id.diary_post_details_viodeo_player)
    public YueMeiVideoView mVideoView;                     //视频播放器
    @BindView(R.id.diary_details_image_player)
    public ImageView mImageView;                            //图片加载

    private String TAG = "PostVideoViewFragment";
    private final int VIDEO_CONTROL = 123;
    private int currentPosition;
    private String mDiaryId;
    private String mVideoUrl;
    private String mImageUrl;
    private String mIsVideo = "1";                       //默认是视频
    private int mWidth;                                 //后台返回的宽
    private int mHeight;                                //后台返回的高

    private final int PLAY_VIDEO = 10;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case PLAY_VIDEO:
                    if (mVideoView != null && mVideoView.getVisibility() == View.VISIBLE) {
                        mVideoView.videoStartPlayer();
                    }
                    break;
            }
        }
    };
    private int maxHeight;

    public static PostVideoViewFragment newInstance(PostOtherpic data, String diaryId) {
        PostVideoViewFragment fragment = new PostVideoViewFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("diaryId", diaryId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PostOtherpic mData = getArguments().getParcelable("data");

        mVideoUrl = mData.getVideo_url();
        mImageUrl = mData.getImg();
        mIsVideo = mData.getIs_video();
        mWidth = Integer.parseInt(mData.getWidth());
        mHeight = Integer.parseInt(mData.getHeight());

        mDiaryId = getArguments().getString("diaryId");

        maxHeight = (windowsHeight / 3) * 2;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diary_posts_video;
    }

    @Override
    protected void initView(View view) {

        if (videoOrImage()) {                   //如果是视频
            setVideoData();
        } else {
            setImageData();
        }

    }

    @Override
    protected void initData(View view) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if ((isVisibleToUser && isResumed())) {
            //fragment可见
            onResume();
        } else if (!isVisibleToUser) {
            //fragment不可见
            onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //是可见，是视频fragment,播放器部位空
        if (getUserVisibleHint() && mVideoView != null && videoOrImage()) {
            if (currentPosition != 0) {
                mVideoView.videoRestart(currentPosition);
                currentPosition = 0;
            } else {
                mVideoView.videoRestart();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mVideoView != null && videoOrImage()) {
            mVideoView.videoSuspend();
        }
    }

    @Override
    public void onDestroyView() {
        if (mVideoView != null) {
            mVideoView.videoStopPlayback();
        }
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case VIDEO_CONTROL:    //视频请求回调
                if (resultCode == 100) {
                    currentPosition = data.getIntExtra("current_position", 0);
                }
                break;
        }
    }

    /**
     * 设置播放器
     */
    private void setVideoData() {
        if (!TextUtils.isEmpty(mVideoUrl)) {
            mVideoView.setVisibility(View.VISIBLE);

            if (mWidth != 0 && mHeight != 0) {
                int mImagewidth = windowsWight;
                int mImageHeight = (mImagewidth * mHeight) / mWidth;

                ViewGroup.LayoutParams layoutParams = mVideoView.getLayoutParams();
                layoutParams.height = mImageHeight > maxHeight ? maxHeight : mImageHeight;
                mVideoView.setLayoutParams(layoutParams);

                if (onSizeVideoClickListener != null) {
                    onSizeVideoClickListener.onLoadedVideoClick(mImagewidth, mImageHeight);
                }
            }

            mVideoView.setVideoParameter(mImageUrl, mVideoUrl, windowsWight);

            //视频最大化点击
            mVideoView.setOnMaxVideoClickListener(new YueMeiVideoView.OnMaxVideoClickListener() {
                @Override
                public void onMaxVideoClick(View v) {

//                    if (getActivity() instanceof DiariesAndPostsActivity) {
//                        DiariesAndPostsActivity activity = (DiariesAndPostsActivity) getActivity();
//                        VideoShareData videoShareData = activity.videoShareData;
//                        if (videoShareData != null) {
//                            Intent intent = new Intent(mContext, VideoPlayerActivity.class);
//                            intent.putExtra("selectNum", mVideoUrl);
//                            intent.putExtra("is_network_video", true);
//                            intent.putExtra("progress", mVideoView.getCurrentPosition());
//                            intent.putExtra("shareTitle", videoShareData.getTitle());
//                            intent.putExtra("shareContent", videoShareData.getContent());
//                            intent.putExtra("shareUrl", videoShareData.getUrl());
//                            intent.putExtra("shareImgUrl", videoShareData.getImg_weix());
//                            intent.putExtra("objid", mDiaryId);
//                            startActivityForResult(intent, VIDEO_CONTROL);
//                        }
//                    }
                }
            });

            //视频宽高获取
            mVideoView.setOnSizeVideoClickListener(new YueMeiVideoView.OnSizeVideoClickListener() {
                @Override
                public void onLoadedVideoClick(int width, int height) {
                    if (mWidth == 0 || mHeight == 0) {
                        if (width != 0 && height != 0) {
                            int mImagewidth = windowsWight;
                            int mImageHeight = (mImagewidth * height) / width;

                            ViewGroup.LayoutParams layoutParams = mVideoView.getLayoutParams();
                            layoutParams.height = mImageHeight > maxHeight ? maxHeight : mImageHeight;
                            mVideoView.setLayoutParams(layoutParams);

                            if (onSizeVideoClickListener != null) {
                                onSizeVideoClickListener.onLoadedVideoClick(mImagewidth, mImageHeight);
                            }
                        }
                    }
                }
            });

            //获取视频进度条状态
            mVideoView.setOnProgressBarStateClick(new YueMeiVideoView.OnProgressBarStateClick() {
                @Override
                public void onProgressBarStateClick(int visibility) {
                    if (onProgressBarStateClickListener != null) {
                        onProgressBarStateClickListener.onProgressBarStateClick(visibility);
                    }
                }
            });

            mHandler.sendEmptyMessageDelayed(PLAY_VIDEO, 3000);
        } else {
            mVideoView.setVisibility(View.GONE);
        }
    }

    /**
     * 设置图片数据
     */
    private void setImageData() {
        if (!TextUtils.isEmpty(mImageUrl)) {
            mImageView.setVisibility(View.VISIBLE);

            if (mWidth != 0 && mHeight != 0) {
                Glide.with(mContext).load(mImageUrl).into(mImageView);

                int mImagewidth = windowsWight;
                int mImageHeight = (mImagewidth * mHeight) / mWidth;

                ViewGroup.LayoutParams layoutParams = mImageView.getLayoutParams();
                layoutParams.height = mImageHeight;
                mImageView.setLayoutParams(layoutParams);

                if (onSizeVideoClickListener != null) {
                    onSizeVideoClickListener.onLoadedVideoClick(mImagewidth, mImageHeight);
                }

            } else {
                Glide.with(mContext).load(mImageUrl).into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        int intrinsicWidth = resource.getIntrinsicWidth();
                        int intrinsicHeight = resource.getIntrinsicHeight();

                        int mImagewidth = windowsWight;
                        int mImageHeight = (mImagewidth * intrinsicHeight) / intrinsicWidth;

                        ViewGroup.LayoutParams layoutParams = mImageView.getLayoutParams();
                        layoutParams.height = mImageHeight;
                        mImageView.setLayoutParams(layoutParams);


                        if (onSizeVideoClickListener != null) {
                            onSizeVideoClickListener.onLoadedVideoClick(mImagewidth, mImageHeight);
                        }

                        mImageView.setImageDrawable(resource);
                    }
                });
            }
        } else {
            mImageView.setVisibility(View.GONE);
        }
    }

    /**
     * 判断当前页面要用视频ui还是图片ui
     *
     * @return true 视频
     */
    public boolean videoOrImage() {

        return "1".equals(mIsVideo);
    }


    //视频宽高获取
    private OnSizeVideoClickListener onSizeVideoClickListener;

    public interface OnSizeVideoClickListener {

        void onLoadedVideoClick(int width, int height);
    }

    public void setOnSizeVideoClickListener(OnSizeVideoClickListener onSizeVideoClickListener) {
        this.onSizeVideoClickListener = onSizeVideoClickListener;
    }

    //视频宽高获取
    private OnProgressBarStateClickListener onProgressBarStateClickListener;

    public interface OnProgressBarStateClickListener {

        void onProgressBarStateClick(int visibility);
    }

    public void setOnProgressBarStateClickListener(OnProgressBarStateClickListener onProgressBarStateClickListener) {
        this.onProgressBarStateClickListener = onProgressBarStateClickListener;
    }
}
