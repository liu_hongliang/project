package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.quicklyask.activity.R;

/**
 * Created by 裴成浩 on 2019/1/14
 */
public class NumberViewDialog extends Dialog implements android.view.View.OnClickListener {

    Context context;

    //左边按钮
    private Button mLeftBtn;
    //右边按钮
    private Button mRightBtn;

    private Button mViewSub;
    private EditText mViewTitle;
    private Button mViewAdd;

    private int value;          //数量
    private int minValue;       //最小值
    private int maxValue;       //最大值

    public NumberViewDialog(Context context, int value, int minValue, int maxValue) {
        super(context, R.style.mystyle);
        this.context = context;
        this.value = value;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 指定布局
        this.setContentView(R.layout.number_view_dialog);

        // 根据id在布局中找到控件对象
        mViewSub = findViewById(R.id.number_view_sub);
        mViewTitle = findViewById(R.id.number_view_title);
        mViewAdd = findViewById(R.id.number_view_add);
        mLeftBtn = findViewById(R.id.number_view_left);
        mRightBtn = findViewById(R.id.ynumber_view_right);

        //设置TextView只显示数字
        mViewTitle.setInputType(InputType.TYPE_CLASS_NUMBER);
        mViewTitle.setText(value + "");

        //设置内容
        mLeftBtn.setText("我在想想");
        mRightBtn.setText("确定");

        // 为按钮绑定点击事件监听器
        mViewSub.setOnClickListener(this);
        mViewAdd.setOnClickListener(this);
        mLeftBtn.setOnClickListener(this);
        mRightBtn.setOnClickListener(this);

        setCanceledOnTouchOutside(false);


        mViewTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    value = Integer.parseInt(mViewTitle.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    value = 0;
                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.number_view_sub:
                subNum();
                break;
            case R.id.number_view_add:
                addNum();
                break;
            case R.id.number_view_left:
                if (mBtnClickListener != null) {
                    mBtnClickListener.leftBtnClick();
                }
                break;
            case R.id.ynumber_view_right:
                if (mBtnClickListener != null) {
                    mBtnClickListener.rightBtnClick(value);
                }
                break;
        }
    }

    /**
     * 减少数据
     */
    private void subNum() {
        if (value > minValue) {
            value = value - 1;
            mViewTitle.setText(value + "");
        }
    }

    /**
     * 添加数据
     */
    private void addNum() {
        if (value < maxValue) {
            value = value + 1;
            mViewTitle.setText(value + "");
        }
    }

    public void setBtnClickListener(BtnClickListener btnClickListener) {
        mBtnClickListener = btnClickListener;
    }

    private BtnClickListener mBtnClickListener;

    public interface BtnClickListener {

        void leftBtnClick();

        void rightBtnClick(int value);
    }
}
