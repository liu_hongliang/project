package com.module.shopping.model.bean;

import java.util.Comparator;

/**
 * 订金劵降序排序
 * Created by 裴成浩 on 2018/12/10
 */
public class PreferentialRank implements Comparator<ShoppingCartPreferentialData> {

    @Override
    public int compare(ShoppingCartPreferentialData lhs, ShoppingCartPreferentialData rhs) {
        return (int)(Float.parseFloat(rhs.getMoney()) - Float.parseFloat(lhs.getMoney()));
    }
}