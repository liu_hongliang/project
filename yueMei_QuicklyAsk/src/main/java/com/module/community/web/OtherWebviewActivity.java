package com.module.community.web;

import android.content.Intent;
import android.widget.FrameLayout;

import com.module.base.view.YMBaseWebViewActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;

import java.util.Map;

import butterknife.BindView;

public class OtherWebviewActivity extends YMBaseWebViewActivity {

    @BindView(R.id.plus_vip_top)
    CommonTopBar mTop;                              //头标题
    @BindView(R.id.plus_vip_container)
    FrameLayout webViewContainer;                   //webView容器

    String url = "";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_plus_vip;
    }


    @Override
    protected void initView() {
        super.initView();

        webViewContainer.addView(mWebView);
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        url = intent.getStringExtra("url");

        loadUrl(url);
    }



    /**
     * 加载webView
     *
     * @param url
     * @param paramMap
     * @param headMap
     */
    protected void loadUrl(String url, Map<String, Object> paramMap, Map<String, Object> headMap) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, paramMap, headMap);
        mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    protected void loadUrl(String url, Map<String, Object> paramMap) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, paramMap);
        mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    protected void loadUrl(String url) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
        mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

    }
}
