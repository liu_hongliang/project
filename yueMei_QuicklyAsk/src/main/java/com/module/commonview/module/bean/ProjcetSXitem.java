package com.module.commonview.module.bean;

/**
 * Created by dwb on 16/5/10.
 */
public class ProjcetSXitem {

    private String id;
    private String name;
    private String special_style;
    private boolean is_selected = false;            //是否选中，默认不选中

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecial_style() {
        return special_style;
    }

    public void setSpecial_style(String special_style) {
        this.special_style = special_style;
    }

    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }
}
