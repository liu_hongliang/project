package com.module.home.model.bean;

import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.entity.SearchResultBoard;
import com.quicklyask.entity.SearchResultMethod;

import java.util.List;

/**
 * 搜索淘页面数据排序后的结构
 * Created by 裴成浩 on 2019/5/24
 */
public class SearchSortTaoData {
    private SearchSortTaoEnum taoEnum;
    private List<HomeTaoData> taoData;                  //淘数据 + 推荐数据
    private List<SearchResultMethod> methodsData;       //方法数据
    private List<SearchResultBoard> boardData;          //活动数据
    private List<SearchResultLike> likeData;            //你可能会喜欢
    private SearchResultPrompt promptData;              //没有更多数据

    public SearchSortTaoData(SearchSortTaoEnum taoEnum) {
        this.taoEnum = taoEnum;
    }

    public SearchSortTaoEnum getTaoEnum() {
        return taoEnum;
    }

    public void setTaoEnum(SearchSortTaoEnum taoEnum) {
        this.taoEnum = taoEnum;
    }

    public List<HomeTaoData> getTaoData() {
        return taoData;
    }

    public void setTaoData(List<HomeTaoData> taoData) {
        this.taoData = taoData;
    }

    public List<SearchResultMethod> getMethodsData() {
        return methodsData;
    }

    public void setMethodsData(List<SearchResultMethod> methodsData) {
        this.methodsData = methodsData;
    }

    public List<SearchResultBoard> getBoardData() {
        return boardData;
    }

    public void setBoardData(List<SearchResultBoard> boardData) {
        this.boardData = boardData;
    }

    public List<SearchResultLike> getLikeData() {
        return likeData;
    }

    public void setLikeData(List<SearchResultLike> likeData) {
        this.likeData = likeData;
    }

    public SearchResultPrompt getPromptData() {
        return promptData;
    }

    public void setPromptData(SearchResultPrompt promptData) {
        this.promptData = promptData;
    }
}
