package com.quicklyask.util;


import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.IOException;
import java.io.Reader;

public class XinJsonReader extends JsonReader {
    private JsonToken mPeek;

    public XinJsonReader(Reader in) {
        super(in);
    }

    @Override
    public void beginObject() throws IOException {
        try {
            super.beginObject();
        } catch (IllegalStateException e) {
            beginPeek(mPeek = peek());
        }
    }

    @Override
    public void beginArray() throws IOException {
        try {
            super.beginArray();
        } catch (IllegalStateException e) {
            mPeek = peek();
            beginPeek(mPeek = peek());
        }
    }

    @Override
    public String nextString() throws IOException {
        try {
            return super.nextString();
        } catch (IllegalStateException e) {
            JsonToken peek = peek();
            beginPeek(peek);
            endPeek(peek);
            return "";
        }
    }

    @Override
    public boolean hasNext() throws IOException {
        boolean hasNext = super.hasNext();
        if (mPeek != null) {//如果调用 begin 的时候 [],{} "" 服务器返回数据结构算合理的
            if (mPeek == JsonToken.STRING) {//如果是""没有下一个了
                hasNext = false;
            }
            if (hasNext) {//如果 [],{}中间有值 认定服务器返回数据结构有问题。抛出异常
                throw new IllegalStateException("服务器返回类型错误，无法修正 " + peek() + getPath());
            }
        }
        return hasNext;
    }

    @Override
    public void endObject() throws IOException {
        if (mPeek == null) {//正常情况
            super.endObject();
        } else {//结束[],{} 的解析
            endPeek(mPeek);
            mPeek = null;
        }
    }

    @Override
    public void endArray() throws IOException {
        if (mPeek == null) {//正常情况
            super.endArray();
        } else {//结束[],{} 的解析
            endPeek(mPeek);
            mPeek = null;
        }
    }

    private void beginPeek(JsonToken peek) throws IOException {
        switch (peek) {
            case BEGIN_ARRAY:
                super.beginArray();
                break;
            case BEGIN_OBJECT:
                super.beginObject();
                break;
            case STRING:
                String s = super.nextString();
                if (s != null && !"".equals(s.trim())) {//如果 "" 中间有值，服务器返回类型有问题
                    throw new IllegalStateException("服务器返回类型错误，无法修正 " + peek() + getPath());
                }
                break;
        }
    }

    private void endPeek(JsonToken peek) throws IOException {
        switch (peek) {
            case BEGIN_ARRAY:
                super.endArray();
                break;
            case BEGIN_OBJECT:
                super.endObject();
                break;
            case STRING:
                break;
        }
    }
}
