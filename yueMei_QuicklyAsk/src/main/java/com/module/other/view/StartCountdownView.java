package com.module.other.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 启动页面倒计时
 *
 * @author 裴成浩
 * @data 2019/10/24
 */
public class StartCountdownView extends View {

    private final Context mContext;
    private Paint mPaint;
    private final String text = "跳过";
    private final String number = "3";

    public StartCountdownView(Context context) {
        this(context, null);
    }

    public StartCountdownView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StartCountdownView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(Utils.dip2px(68), Utils.dip2px(31));
    }

    private void initView() {
        setBackground(Utils.getLocalDrawable(mContext, R.drawable.home_skip_bt));

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.WHITE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawText(text, 0, 0, mPaint);
        float measureText = mPaint.measureText(text);
        mPaint.setColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
        canvas.drawText(number, measureText, 0, mPaint);
    }
}
