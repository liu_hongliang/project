package com.module.community.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.community.model.bean.BBsListTag;
import com.module.community.model.bean.CommunityStaggeredListData;
import com.module.community.model.bean.StaggeredImgData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/7/6.
 */
public class CommunityStagFragmentAdapter extends RecyclerView.Adapter<CommunityStagFragmentAdapter.ViewHolder> {

    private final int windowsWight;
    private Activity mContext;
    private List<CommunityStaggeredListData> mData;
    private LayoutInflater mInflater;
    private String TAG = "CommunityStagFragmentAdapter";
    private final int imgWidth;
    private LinkedHashMap<Integer, Integer> imgHeights = new LinkedHashMap();

    public CommunityStagFragmentAdapter(Activity context, List<CommunityStaggeredListData> data) {
        this.mContext = context;
        this.mData = data;

        mInflater = LayoutInflater.from(mContext);

        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
        imgWidth = (windowsWight - Utils.dip2px(15)) / 2;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_community_fragment, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        CommunityStaggeredListData data = mData.get(position);

        setImage(holder, position, data);

        if ("1".equals(data.getIs_video())) {       //有视频
            holder.mImgLogo.setVisibility(View.VISIBLE);
        } else {
            holder.mImgLogo.setVisibility(View.GONE);
        }

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.itemCommunity.getLayoutParams();
        if (position == 0 || position == 1) {
            params.topMargin = Utils.dip2px(15);
        } else {
            params.topMargin = Utils.dip2px(5);
        }
        holder.itemCommunity.setLayoutParams(params);

        //标签设置
        List<BBsListTag> tags = data.getTag();
        if (tags.size() > 0) {
            holder.mTags.setText("#" + tags.get(0).getName());
            holder.mTags.setVisibility(View.VISIBLE);
        } else {
            holder.mTags.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(data.getTitle())) {

            holder.mContent.setText(data.getTitle());
            holder.mContent.setVisibility(View.VISIBLE);

        } else {

            holder.mContent.setVisibility(View.GONE);
        }

        String userName = data.getUser_name();
        String userImg = data.getUser_img();

        if (TextUtils.isEmpty(userName) && TextUtils.isEmpty(userImg)) {
            holder.userData.setVisibility(View.GONE);
        } else {
            holder.userData.setVisibility(View.VISIBLE);
            holder.mName.setText(userName);

            holder.mSee.setText(data.getView_num());

            //用户头像
            Glide.with(mContext).load(userImg).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).dontAnimate().into(holder.mPortrait);
        }
    }

    /**
     * 设置图片
     *
     * @param holder
     * @param position
     * @param data
     */
    private void setImage(@NonNull final ViewHolder holder, final int position, CommunityStaggeredListData data) {
        if (imgHeights.containsKey(position)) {
            int imgHeight = imgHeights.get(position);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.imgContainer.getLayoutParams();
            params.width = imgWidth;
            params.height = imgHeight;

            holder.imgContainer.setLayoutParams(params);

            Glide.with(mContext).load(data.getImg()
                    .getImg())
                    .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .into(holder.mImg);

        } else {
            StaggeredImgData dataImg = data.getImg();
            String imgImg = dataImg.getImg();

            int intrinsicWidth;
            try {
                intrinsicWidth = Integer.parseInt(dataImg.getWidth());
            } catch (NumberFormatException e) {
                intrinsicWidth = 0;
            }
            int intrinsicHeight;
            try {
                intrinsicHeight = Integer.parseInt(dataImg.getHeight());
            } catch (NumberFormatException e) {
                intrinsicHeight = 0;
            }

            if (intrinsicWidth != 0 && intrinsicHeight != 0) {
                int imgHeight = (imgWidth * intrinsicHeight) / intrinsicWidth;

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.imgContainer.getLayoutParams();
                params.width = imgWidth;
                params.height = imgHeight;

                Log.e(TAG, "111width == " + intrinsicWidth);
                Log.e(TAG, "111height == " + intrinsicHeight);
                holder.imgContainer.setLayoutParams(params);

                imgHeights.put(position, imgHeight);

                Glide.with(mContext).load(data.getImg()
                        .getImg())
                        .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .dontAnimate()
                        .into(holder.mImg);

            } else {

                Glide.with(mContext)
                        .load(imgImg)
                        .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .dontAnimate()
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                int intrinsicWidth = resource.getIntrinsicWidth();
                                int intrinsicHeight = resource.getIntrinsicHeight();
                                Log.e(TAG, "intrinsicWidth == " + intrinsicWidth);
                                Log.e(TAG, "intrinsicHeight == " + intrinsicHeight);

                                int imgHeight = (imgWidth * intrinsicHeight) / intrinsicWidth;

                                Log.e(TAG, "imgWidth == " + imgWidth);
                                Log.e(TAG, "imgHeight == " + imgHeight);


                                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.imgContainer.getLayoutParams();
                                params.width = imgWidth;
                                params.height = imgHeight;
                                holder.imgContainer.setLayoutParams(params);

                                imgHeights.put(position, imgHeight);

                                holder.mImg.setImageDrawable(resource);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                int imageHeight = 0;
                                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) holder.imgContainer.getLayoutParams();
                                lp.width = imgWidth;
                                lp.height = imageHeight;
                                holder.imgContainer.setLayoutParams(lp);

                                imgHeights.put(position, imageHeight);

                            }
                        });

            }
        }
    }


    @Override
    public int getItemCount() {
        Log.e(TAG, "mData.size()== " + mData.size());
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout itemCommunity;
        private FrameLayout imgContainer;
        private ImageView mImg;
        private LinearLayout userData;
        private ImageView mImgLogo;
        private TextView mContent;
        private TextView mTags;
        private ImageView mPortrait;
        private TextView mName;
        private TextView mSee;

        public ViewHolder(View itemView) {
            super(itemView);

            itemCommunity = itemView.findViewById(R.id.item_community_fragment);
            imgContainer = itemView.findViewById(R.id.community_tag_img_container);
            userData = itemView.findViewById(R.id.community_tag_head_user_data);
            mImg = itemView.findViewById(R.id.community_tag_img);
            mImgLogo = itemView.findViewById(R.id.community_tag_img_logo);
            mTags = itemView.findViewById(R.id.community_tag_head_user_tag);
            mContent = itemView.findViewById(R.id.community_tag_content);
            mPortrait = itemView.findViewById(R.id.community_tag_head_portrait);
            mName = itemView.findViewById(R.id.community_tag_name);
            mSee = itemView.findViewById(R.id.community_tag_see);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemCallBackListener != null) {
                        itemCallBackListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

        }
    }

    /**
     * 添加数据
     *
     * @param data
     */
    public void addData(List<CommunityStaggeredListData> data) {
        mData.addAll(data);
        notifyItemInserted(getItemCount());
    }

    /**
     * 清空所有数据
     */
    public void clearData() {
        mData.clear();
        imgHeights.clear();
        notifyDataSetChanged();
    }

    public List<CommunityStaggeredListData> getmData() {
        return mData;
    }

    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }

}