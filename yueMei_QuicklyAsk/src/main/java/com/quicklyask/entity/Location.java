package com.quicklyask.entity;

import com.baidu.platform.comapi.basestruct.GeoPoint;


public class Location {

	private String address;
	private GeoPoint geoPoint;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public GeoPoint getGeoPoint() {
		return geoPoint;
	}

	public void setGeoPoint(GeoPoint geoPoint) {
		this.geoPoint = geoPoint;
	}
}