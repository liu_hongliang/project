package com.module.community.model.bean;

public class CommunityPost {

    /**
     * user_name : 听说她超甜
     * user_img : https://p21.yuemei.com/avatar/085/02/29/99_avatar_120_120.jpg
     * title : 双眼皮手术
     * daren : 金牌达人
     * answer_num : 18
     * view_num : 4万+
     */

    private String user_name;
    private String user_img;
    private String title;
    private String daren;
    private String answer_num;
    private String view_num;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDaren() {
        return daren;
    }

    public void setDaren(String daren) {
        this.daren = daren;
    }

    public String getAnswer_num() {
        return answer_num;
    }

    public void setAnswer_num(String answer_num) {
        this.answer_num = answer_num;
    }

    public String getView_num() {
        return view_num;
    }

    public void setView_num(String view_num) {
        this.view_num = view_num;
    }
}
