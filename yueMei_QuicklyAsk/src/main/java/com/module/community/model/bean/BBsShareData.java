package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.commonview.module.bean.ShareWechat;

public class BBsShareData implements Parcelable {

	private String url_new;
	private String url;
	private String content;
	private String img;
	private String askorshare;
	private ShareWechat Wechat;
	private ShareDetailPictorial Pictorial;

	protected BBsShareData(Parcel in) {
		url_new = in.readString();
		url = in.readString();
		content = in.readString();
		img = in.readString();
		askorshare = in.readString();
		Wechat = in.readParcelable(ShareWechat.class.getClassLoader());
		Pictorial = in.readParcelable(ShareDetailPictorial.class.getClassLoader());
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(url_new);
		dest.writeString(url);
		dest.writeString(content);
		dest.writeString(img);
		dest.writeString(askorshare);
		dest.writeParcelable(Wechat, flags);
		dest.writeParcelable(Pictorial, flags);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<BBsShareData> CREATOR = new Creator<BBsShareData>() {
		@Override
		public BBsShareData createFromParcel(Parcel in) {
			return new BBsShareData(in);
		}

		@Override
		public BBsShareData[] newArray(int size) {
			return new BBsShareData[size];
		}
	};

	public String getUrl_new() {
		return url_new;
	}

	public void setUrl_new(String url_new) {
		this.url_new = url_new;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getAskorshare() {
		return askorshare;
	}

	public void setAskorshare(String askorshare) {
		this.askorshare = askorshare;
	}

	public ShareWechat getWechat() {
		return Wechat;
	}

	public void setWechat(ShareWechat wechat) {
		Wechat = wechat;
	}

	public ShareDetailPictorial getPictorial() {
		return Pictorial;
	}

	public void setPictorial(ShareDetailPictorial pictorial) {
		Pictorial = pictorial;
	}
}
