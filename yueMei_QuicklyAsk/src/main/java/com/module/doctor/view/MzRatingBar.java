package com.module.doctor.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.quicklyask.activity.R;

import java.util.ArrayList;

/**
 * 渐变的五颗星
 * Created by 裴成浩 on 2017/9/28.
 */

public class MzRatingBar extends android.support.v7.widget.AppCompatRatingBar {
    private final Context mContext;
    private ArrayList<Drawable> mStarDrawables;
    private Drawable huiDrawable;

    public MzRatingBar(Context context) {
        this(context, null);
    }

    public MzRatingBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MzRatingBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }

    // 加载五张星星的图片
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void createStarDrawables() {
        huiDrawable = ContextCompat.getDrawable(mContext, R.drawable.huixing_2x);
        mStarDrawables = new ArrayList<>();
        mStarDrawables.add(ContextCompat.getDrawable(mContext, R.drawable.huang_xing1));
        mStarDrawables.add(ContextCompat.getDrawable(mContext, R.drawable.huang_xing2));
        mStarDrawables.add(ContextCompat.getDrawable(mContext, R.drawable.huang_xing3));
        mStarDrawables.add(ContextCompat.getDrawable(mContext, R.drawable.huang_xing4));
        mStarDrawables.add(ContextCompat.getDrawable(mContext, R.drawable.huang_xing5));
    }


    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        createStarDrawables();
        // 先绘制原来的控件

        // 在原来控件的基础上根据进度绘制颜色不一样的星星
        Drawable progressDrawable = getProgressDrawable();
        if (progressDrawable != null) {
            canvas.save();
            // 获得进度位置
            final int pogressPos = getProgressPos();
            // 根据进度位置设置裁剪区域
            canvas.clipRect(0, 0, pogressPos, getHeight());
            int drawableLeft = getPaddingLeft();
            int drawableTop = getPaddingTop();
            // 绘制五张星星
            for (Drawable drawable : mStarDrawables) {
                drawable.setBounds(drawableLeft, drawableTop, drawableLeft + drawable.getIntrinsicWidth(), drawableTop + drawable.getIntrinsicHeight());
                drawableLeft += drawable.getIntrinsicWidth();
                drawable.draw(canvas);
            }
            canvas.restore();
        }
    }

    /**
     * 获取进度所对应的位置
     *
     * @return
     */
    private int getProgressPos() {
        int available = getWidth() - getPaddingLeft() - getPaddingRight();
        final int progressPos = (int) (getScale() * available + 0.5f) + getPaddingLeft();
        return progressPos;
    }


    /**
     * 获得当前滑动进度的百分比
     *
     * @return
     */
    private float getScale() {
        final int max = getMax(); // 最大进度
        return max > 0 ? getProgress() / (float) max : 0;
    }
}