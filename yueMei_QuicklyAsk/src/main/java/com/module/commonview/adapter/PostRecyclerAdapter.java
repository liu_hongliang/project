package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.DiaryPhotoBrowsingActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.PostContentImage;
import com.module.commonview.module.bean.PostContentList;
import com.module.commonview.module.bean.PostContentText;
import com.module.commonview.module.bean.PostContentVideo;
import com.module.commonview.view.Expression;
import com.module.commonview.view.MyURLSpan;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.commonview.view.UnderlinePageIndicator;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.controller.activity.VideoListActivity;
import com.module.community.model.bean.BBsListData550;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.model.bean.DocListData;
import com.module.doctor.model.bean.HosListData;
import com.module.doctor.view.MzRatingBar;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 适配器
 * Created by 裴成浩 on 2018/8/17.
 */
public class PostRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int windowsWight;
    private String mDiaryId;
    private String mSelfPageType;
    private Activity mContext;
    private List<PostContentList> mDatas;
    private LayoutInflater mInflater;
    private final int ITEM_TYPE_TEXT = 1;                   //富文本类型
    private final int ITEM_TYPE_IMAGE = 2;                  //图片类型
    private final int ITEM_TYPE_VIDEO = 3;                  //视频类型
    private final int ITEM_TYPE_TAO = 4;                    //淘整型
    private final int ITEM_TYPE_POST = 5;                   //帖子列表
    private final int ITEM_TYPE_DOCTOR = 6;                 //医生列表
    private final int ITEM_TYPE_HOSPITAL = 7;               //医院列表
    private final int ITEM_TYPE_NULL = 8;                   //空列表
    private String TAG = "PostRecyclerAdapter";
    private BaseWebViewClientMessage mClientManager;
    private PageJumpManager mPageJumpManager;

    public PostRecyclerAdapter(Activity context, List<PostContentList> content, String diaryId, String selfPageType) {
        this.mContext = context;
        this.mDatas = content;
        this.mDiaryId = diaryId;
        this.mSelfPageType = selfPageType;

        mInflater = LayoutInflater.from(context);
        mClientManager = new BaseWebViewClientMessage(mContext);
        mPageJumpManager = new PageJumpManager(mContext);

        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @Override
    public int getItemViewType(int position) {
        PostContentList data = mDatas.get(position);
        if (data.getText() != null) {
            return ITEM_TYPE_TEXT;
        } else if (data.getImage() != null) {
            return ITEM_TYPE_IMAGE;

        } else if (data.getVideo() != null) {
            return ITEM_TYPE_VIDEO;

        } else if (data.getTao() != null) {
            return ITEM_TYPE_TAO;

        } else if (data.getPost() != null) {
            return ITEM_TYPE_POST;

        } else if (data.getDoctor() != null) {
            return ITEM_TYPE_DOCTOR;

        } else if (data.getHospital() != null) {
            return ITEM_TYPE_HOSPITAL;
        } else {
            return ITEM_TYPE_NULL;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_TEXT:
                return new TextHolder(mInflater.inflate(R.layout.item_post_list_text, parent, false));
            case ITEM_TYPE_IMAGE:
                return new ImageHolder(mInflater.inflate(R.layout.item_post_list_image, parent, false));
            case ITEM_TYPE_VIDEO:
                return new VideoHolder(mInflater.inflate(R.layout.item_post_list_video, parent, false));
            case ITEM_TYPE_TAO:
                return new TaoHolder(mInflater.inflate(R.layout.item_post_list_tao, parent, false));
            case ITEM_TYPE_POST:
                return new PostHolder(mInflater.inflate(R.layout.item_post_list_post, parent, false));
            case ITEM_TYPE_DOCTOR:
                return new DoctorHolder(mInflater.inflate(R.layout.item_post_list_doctor, parent, false));
            case ITEM_TYPE_HOSPITAL:
                return new HospitalHolder(mInflater.inflate(R.layout.item_post_list_hospital, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TextHolder) {
            setTextView((TextHolder) holder, position);
        } else if (holder instanceof ImageHolder) {
            setImageView((ImageHolder) holder, position);
        } else if (holder instanceof VideoHolder) {
            setVideoView((VideoHolder) holder, position);
        } else if (holder instanceof TaoHolder) {
            setTaoView((TaoHolder) holder, position);
        } else if (holder instanceof PostHolder) {
            setPostView((PostHolder) holder, position);
        } else if (holder instanceof DoctorHolder) {
            setDoctorView((DoctorHolder) holder, position);
        } else if (holder instanceof HospitalHolder) {
            setHosptalView((HospitalHolder) holder, position);
        }
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof ImageHolder) {
            ImageView imageView = ((ImageHolder) holder).postImage;
            if (imageView != null) {
                imageView.refreshDrawableState();
                System.gc();
            }
        }
    }

    /**
     * 设置文本数据
     *
     * @param holder
     * @param position
     */
    private void setTextView(TextHolder holder, int position) {
        try {
            PostContentText mTextData = mDatas.get(position).getText();

            holder.postText.setText(textHtmlClick(mTextData.getContent()));

            holder.postText.setMovementMethod(LinkMovementMethod.getInstance());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置富文本连接点击
     *
     * @param content
     * @return
     * @throws IOException
     */
    private SpannableStringBuilder textHtmlClick(String content) throws IOException {
        //转化成html形式
        CharSequence text = Html.fromHtml(content);

        int end = text.length();
        Spannable sp = (Spannable) text;
        URLSpan[] urls = sp.getSpans(0, end, URLSpan.class);
        SpannableStringBuilder style = new SpannableStringBuilder(text);
        style.clearSpans();
        for (URLSpan url : urls) {
            MyURLSpan myURLSpan = new MyURLSpan(url.getURL());
            style.setSpan(myURLSpan, sp.getSpanStart(url), sp.getSpanEnd(url), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

            myURLSpan.setOnClickListener(new MyURLSpan.OnClickListener() {
                @Override
                public void onClick(View arg0, String url) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }

                    Log.e(TAG, "text ---> url == " + url);
                    if (url.startsWith("type")) {
                        mClientManager.showWebDetail(url);
                    } else {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                    }
                }
            });
        }

        //把表情加载出来
        SpannableStringBuilder textSpanned = Expression.handlerEmojiText2(style, mContext, Utils.dip2px(12));

        return textSpanned;
    }


    /**
     * 设置图片数据
     *
     * @param holder
     * @param position
     */
    private void setImageView(final ImageHolder holder, int position) {
        PostContentImage mImageData = mDatas.get(position).getImage();
        String imgUrl = mImageData.getImg();

        final String url = mImageData.getUrl();

        final int width = Integer.parseInt(mImageData.getWidth());
        final int height = Integer.parseInt(mImageData.getHeight());

        if (width != 0 && height != 0) {
            int imageWight = windowsWight - Utils.dip2px(40);

            int imageHeight = (imageWight * height) / width;

            ViewGroup.LayoutParams layoutParams = holder.postImage.getLayoutParams();
            layoutParams.width = imageWight;
            layoutParams.height = imageHeight;
            holder.postImage.setLayoutParams(layoutParams);
        }

        //加载图片不使用缓存（内存缓存和磁盘缓存都不使用）
        Glide.with(mContext).load(imgUrl).transform(new GlideRoundTransform(mContext,6)).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.postImage);

        //图片点击事件处理
        holder.postImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "11111url == " + url);
                if (!TextUtils.isEmpty(url)) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }

                    if (url.startsWith("type")) {
                        mClientManager.showWebDetail(url);
                    } else {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                    }
                }else{
                    Intent intent = new Intent(mContext, DiaryPhotoBrowsingActivity.class);
                    intent.putExtra("post_id", mDiaryId);
                    intent.putExtra("position", 0);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    /**
     * 设置视频播放器
     *
     * @param holder
     * @param position
     */
    private void setVideoView(final VideoHolder holder, final int position) {
        PostContentVideo video = mDatas.get(position).getVideo();
        String img = video.getImg();
        int width = Integer.parseInt(video.getImg_width());
        int height = Integer.parseInt(video.getImg_height());
        if (width != 0 && height != 0) {
            int imageWight = windowsWight - Utils.dip2px(40);
            int imageHeight = (imageWight * height) / width;

            ViewGroup.LayoutParams layoutParams = holder.postVideo.getLayoutParams();
            layoutParams.width = imageWight;
            layoutParams.height = imageHeight;
            holder.postVideo.setLayoutParams(layoutParams);

            Glide.with(mContext)
                    .load(img)
                    .placeholder(R.drawable.home_other_placeholder)
                    .error(R.drawable.home_other_placeholder)
                    .into(holder.postVideo);
        } else {
            Glide.with(mContext)
                    .load(img)
                    .placeholder(R.drawable.home_other_placeholder)
                    .error(R.drawable.home_other_placeholder)
                    .into(new SimpleTarget<GlideDrawable>() {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {

                            int intrinsicWidth = resource.getIntrinsicWidth();
                            int intrinsicHeight = resource.getIntrinsicHeight();

                            int imageWight = windowsWight - Utils.dip2px(40);
                            int imageHeight = (imageWight * intrinsicHeight) / intrinsicWidth;

                            ViewGroup.LayoutParams layoutParams = holder.postVideo.getLayoutParams();
                            layoutParams.width = imageWight;
                            layoutParams.height = imageHeight;
                            holder.postVideo.setLayoutParams(layoutParams);

                            holder.postVideo.setImageDrawable(resource);
                        }
                    });
        }

    }


    /**
     * 设置淘整型数据
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    private void setTaoView(TaoHolder holder, int position) {

        //初始化下边的商品列表
        ScrollLayoutManager scrollLinearLayoutManager1 = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager1.setScrollEnable(false);
        holder.postTaoRecycle.setLayoutManager(scrollLinearLayoutManager1);
        final RecommGoodSkuAdapter recommGoodSkuAdapter = new RecommGoodSkuAdapter(mContext, mDatas.get(position).getTao(),mSelfPageType);
        holder.postTaoRecycle.setAdapter(recommGoodSkuAdapter);

        //点击事件
        recommGoodSkuAdapter.setItemnClickListener(new RecommGoodSkuAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                String tao_id = recommGoodSkuAdapter.getTaoDatas().get(pos).getId();
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", mDiaryId);
                hashMap.put("to_page_type", "2");
                hashMap.put("to_page_id", tao_id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.POSTINFO_TO_TAO, "in_the_text"), hashMap,new ActivityTypeData(mSelfPageType));

                Intent intent = new Intent(mContext, TaoDetailActivity.class);
                intent.putExtra("id", tao_id);
                if (mContext instanceof DiariesAndPostsActivity) {
                    intent.putExtra("source", ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier());
                } else {
                    intent.putExtra("source", "0");
                }
                intent.putExtra("objid", mDiaryId);
                mContext.startActivity(intent);
            }
        });
    }

    /**
     * 设置帖子数据
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    private void setPostView(PostHolder holder, int position) {
        final List<BBsListData550> mPostDatas = mDatas.get(position).getPost();

        List<View> views = new ArrayList<>();
        for (int i = 0; i < mPostDatas.size(); i++) {
            BBsListData550 postData = mPostDatas.get(i);

            View view = mInflater.inflate(R.layout.item_post_list_goods_group, null);

            TextView postTitle = view.findViewById(R.id.post_list_goods_title);
            TextView postTag = view.findViewById(R.id.post_list_goods_tag);
            TextView postBrowse = view.findViewById(R.id.post_list_goods_title_browse);
            TextView postComments = view.findViewById(R.id.post_list_goods_title_comments);
            ImageView postImg = view.findViewById(R.id.post_list_goods_image);

            postTitle.setText(postData.getTitle());
            postBrowse.setText("浏览 " + postData.getBmsid());
            postComments.setText("评论 " + postData.getAgree_num());

            //标签设置
            if (postData.getTag().size() > 0) {
                postTag.setText("#" + postData.getTag().get(0).getName() + "#");
            }

            if (postData.getPic().size() > 0) {
                Glide.with(mContext).load(postData.getPic().get(0).getImg()).into(postImg);
            }

            views.add(view);
        }

        GoodsGroupViewpagerAdapter adapter = new GoodsGroupViewpagerAdapter(views);
        holder.postPostPage.setAdapter(adapter);
        holder.postPostUp.setViewPager(holder.postPostPage);

        if (mPostDatas.size() > 1) {
            holder.postPostUp.setVisibility(View.VISIBLE);
        } else if (mPostDatas.size() > 0) {
            holder.postPostUp.setVisibility(View.GONE);
        } else {
            holder.postPostUp.setVisibility(View.GONE);
        }

        //点击事件
        adapter.setOnItemCallBackListener(new GoodsGroupViewpagerAdapter.ItemCallBackListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                WebUrlTypeUtil.getInstance(mContext).urlToApp(mPostDatas.get(pos).getAppmurl(), "0", "0");
            }
        });
    }

    /**
     * 设置医生数据
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    private void setDoctorView(DoctorHolder holder, int position) {
        final List<DocListData> mDoctorDatas = mDatas.get(position).getDoctor();

        List<View> views = new ArrayList<>();
        for (int i = 0; i < mDoctorDatas.size(); i++) {
            DocListData docData = mDoctorDatas.get(i);

            View view = mInflater.inflate(R.layout.item_post_doctor_goods_group, null);

            ImageView postDoctorImg = view.findViewById(R.id.post_list_doctor_img);
            TextView postDoctorName = view.findViewById(R.id.post_list_doctor_name);
            TextView postDoctorTitle = view.findViewById(R.id.post_list_doctor_title);
            ImageView postDoctorTag = view.findViewById(R.id.post_list_doctor_tag);
            MzRatingBar postDoctorMrb = view.findViewById(R.id.post_list_doctor_mrb);
            TextView postDoctorScore = view.findViewById(R.id.post_list_doctor_score);
            TextView postDoctorNum = view.findViewById(R.id.post_list_doctor_num);
            TextView postDoctorHos = view.findViewById(R.id.post_list_doctor_hospital);

            Glide.with(mContext).load(docData.getImg()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(postDoctorImg);

            postDoctorName.setText(docData.getUsername());
            postDoctorTitle.setText(docData.getTitle());

            //评分
            postDoctorMrb.setProgress(Integer.parseInt(docData.getComment_bili()));

            //标识
            if (docData.getTalent() != null && !"0".equals(docData.getTalent())) {
                postDoctorTag.setVisibility(View.VISIBLE);
                if ("1".equals(docData.getTalent())) {
                    postDoctorTag.setBackgroundResource(R.drawable.talent_list);
                } else if ("2".equals(docData.getTalent())) {
                    postDoctorTag.setBackgroundResource(R.drawable.talent_list);
                } else if ("3".equals(docData.getTalent())) {
                    postDoctorTag.setBackgroundResource(R.drawable.talent_list);
                } else if ("4".equals(docData.getTalent())) {
                    postDoctorTag.setBackgroundResource(R.drawable.talent_list);
                } else if ("5".equals(docData.getTalent())) {
                    postDoctorTag.setBackgroundResource(R.drawable.renzheng_list);
                } else if ("6".equals(docData.getTalent())) {
                    postDoctorTag.setBackgroundResource(R.drawable.teyao);
                } else if ("7".equals(docData.getTalent())) {
                    postDoctorTag.setBackgroundResource(R.drawable.renzheng_list);
                }
            } else {
                postDoctorTag.setVisibility(View.GONE);
            }

            postDoctorScore.setText(docData.getComment_score());
            postDoctorNum.setText(docData.getSku_order_num() + "人预定");
            postDoctorHos.setText(docData.getHspital_name());

            views.add(view);
        }

        GoodsGroupViewpagerAdapter adapter = new GoodsGroupViewpagerAdapter(views);
        holder.postDoctorPage.setAdapter(adapter);
        holder.postDoctorUp.setViewPager(holder.postDoctorPage);

        if (mDoctorDatas.size() > 1) {
            holder.postDoctorUp.setVisibility(View.VISIBLE);
        } else if (mDoctorDatas.size() > 0) {
            holder.postDoctorUp.setVisibility(View.GONE);
        } else {
            holder.postDoctorUp.setVisibility(View.GONE);
        }

        //点击事件
        adapter.setOnItemCallBackListener(new GoodsGroupViewpagerAdapter.ItemCallBackListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }

                Intent it0 = new Intent();
                it0.putExtra("docId", mDoctorDatas.get(pos).getUser_id());
                it0.putExtra("docName", mDoctorDatas.get(pos).getUsername());
                it0.putExtra("partId", mDoctorDatas.get(pos).getBmsid());
                it0.setClass(mContext, DoctorDetailsActivity592.class);
                mContext.startActivity(it0);
            }
        });
    }

    /**
     * 设置医院数据
     *
     * @param holder
     * @param position
     */
    private void setHosptalView(HospitalHolder holder, int position) {
        final List<HosListData> mHospitalDatas = mDatas.get(position).getHospital();

        List<View> views = new ArrayList<>();
        for (int i = 0; i < mHospitalDatas.size(); i++) {
            final HosListData hosData = mHospitalDatas.get(i);

            View view = mInflater.inflate(R.layout.item_post_hospital_goods_group, null);

            ImageView postHospitalImg = view.findViewById(R.id.post_list_hospital_img);
            TextView postHospitalName = view.findViewById(R.id.post_list_hospital_name);
            ImageView postHospitalTag = view.findViewById(R.id.post_list_hospital_tag);
            MzRatingBar postHospitalMrb = view.findViewById(R.id.post_list_hospital_mrb);
            TextView postHospitalScore = view.findViewById(R.id.post_list_hospital_score);
            TextView postHospitalNum = view.findViewById(R.id.post_list_hospital_num);
            TextView postHospitalAddress = view.findViewById(R.id.post_list_hospital_address);
            TextView postHospitalService = view.findViewById(R.id.post_list_hospital_service);
            TextView postHospitalIm = view.findViewById(R.id.post_list_hospital_im);
            TextView postHospitalSee = view.findViewById(R.id.post_list_hospital_see);


            Glide.with(mContext).load(hosData.getImg()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(postHospitalImg);

            postHospitalName.setText(hosData.getHos_name());

            //评分
            postHospitalMrb.setProgress(Integer.parseInt(hosData.getComment_bili()));

            //标识
            if ("1".equals(hosData.getKind())) {
                postHospitalTag.setBackgroundResource(R.drawable.teyao);
            } else {
                postHospitalTag.setBackgroundResource(R.drawable.renzheng_list);
            }

            postHospitalScore.setText(hosData.getComment_score());
            postHospitalNum.setText(hosData.getComment_people());
            postHospitalAddress.setText(hosData.getAddress());

            postHospitalService.setText(hosData.getLook());

            //私信医院
            postHospitalIm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                            .setDirectId(hosData.getHos_id())
                            .setObjId("0")
                            .setObjType("2")
                            .setYmClass("0")
                            .setYmId("0")
                            .build();
                    mPageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                }
            });

            views.add(view);
        }

        GoodsGroupViewpagerAdapter adapter = new GoodsGroupViewpagerAdapter(views);
        holder.postHospitalPage.setAdapter(adapter);
        holder.postHospitalUp.setViewPager(holder.postHospitalPage);

        if (mHospitalDatas.size() > 1) {
            holder.postHospitalUp.setVisibility(View.VISIBLE);
        } else if (mHospitalDatas.size() > 0) {
            holder.postHospitalUp.setVisibility(View.GONE);
        } else {
            holder.postHospitalUp.setVisibility(View.GONE);
        }

        //点击事件
        adapter.setOnItemCallBackListener(new GoodsGroupViewpagerAdapter.ItemCallBackListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Intent it = new Intent();
                it.setClass(mContext, HosDetailActivity.class);
                it.putExtra("hosid", mHospitalDatas.get(pos).getHos_id());
                mContext.startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class TextHolder extends RecyclerView.ViewHolder {

        private TextView postText;

        public TextHolder(View itemView) {
            super(itemView);
            postText = itemView.findViewById(R.id.item_post_text);
        }
    }

    public class ImageHolder extends RecyclerView.ViewHolder {

        private ImageView postImage;

        public ImageHolder(View itemView) {
            super(itemView);
            postImage = itemView.findViewById(R.id.item_post_image);
        }
    }

    public class VideoHolder extends RecyclerView.ViewHolder {

        private ImageView postVideo;

        public VideoHolder(View itemView) {
            super(itemView);
            postVideo = itemView.findViewById(R.id.item_post_list_video);

            postVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, VideoListActivity.class);
                    intent.putExtra("progress", 0);
                    intent.putExtra("id", mDiaryId);
                    intent.putExtra("flag", "1");
                    mContext.startActivity(intent);
                }
            });
        }
    }

    public class TaoHolder extends RecyclerView.ViewHolder {

        private RecyclerView postTaoRecycle;

        public TaoHolder(View itemView) {
            super(itemView);
            postTaoRecycle = itemView.findViewById(R.id.post_list_tao_recycle);
        }
    }

    public class PostHolder extends RecyclerView.ViewHolder {

        private ViewPager postPostPage;
        private UnderlinePageIndicator postPostUp;

        public PostHolder(View itemView) {
            super(itemView);
            postPostPage = itemView.findViewById(R.id.post_list_post_page);
            postPostUp = itemView.findViewById(R.id.post_list_post_up);
        }
    }

    public class DoctorHolder extends RecyclerView.ViewHolder {

        private ViewPager postDoctorPage;
        private UnderlinePageIndicator postDoctorUp;

        public DoctorHolder(View itemView) {
            super(itemView);

            postDoctorPage = itemView.findViewById(R.id.post_list_doctor_page);
            postDoctorUp = itemView.findViewById(R.id.post_list_doctor_up);
        }
    }

    public class HospitalHolder extends RecyclerView.ViewHolder {

        private ViewPager postHospitalPage;
        private UnderlinePageIndicator postHospitalUp;

        public HospitalHolder(View itemView) {
            super(itemView);

            postHospitalPage = itemView.findViewById(R.id.post_list_hospital_page);
            postHospitalUp = itemView.findViewById(R.id.post_list_hospital_up);
        }
    }
}
