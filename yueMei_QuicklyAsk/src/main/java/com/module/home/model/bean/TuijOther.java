package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2017/8/1.
 */

public class TuijOther implements Parcelable {
    private String title;
    private String img;
    private String url;
    private String desc;

    protected TuijOther(Parcel in) {
        title = in.readString();
        img = in.readString();
        url = in.readString();
        desc = in.readString();
    }

    public static final Creator<TuijOther> CREATOR = new Creator<TuijOther>() {
        @Override
        public TuijOther createFromParcel(Parcel in) {
            return new TuijOther(in);
        }

        @Override
        public TuijOther[] newArray(int size) {
            return new TuijOther[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(img);
        dest.writeString(url);
        dest.writeString(desc);
    }
}
