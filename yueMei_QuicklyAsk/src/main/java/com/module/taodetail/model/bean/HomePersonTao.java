package com.module.taodetail.model.bean;

import java.util.List;

public class HomePersonTao {
    private List<HomeTaoData> list;
    private List<HuanDengData> huandeng;

    public List<HomeTaoData> getList() {
        return list;
    }

    public void setList(List<HomeTaoData> list) {
        this.list = list;
    }

    public List<HuanDengData> getHuandeng() {
        return huandeng;
    }

    public void setHuandeng(List<HuanDengData> huandeng) {
        this.huandeng = huandeng;
    }
}
