package com.module.shopping.model.bean;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/12/19
 */
public class PayOrderTao {
    private String hos_id;
    private String beizhu;
    private String hos_hongbao_card_id;
    private List<PayOrderTaoSku> taodata;

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }

    public String getBeizhu() {
        return beizhu;
    }

    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
    }

    public String getHos_hongbao_card_id() {
        return hos_hongbao_card_id;
    }

    public void setHos_hongbao_card_id(String hos_hongbao_card_id) {
        this.hos_hongbao_card_id = hos_hongbao_card_id;
    }

    public List<PayOrderTaoSku> getTaodata() {
        return taodata;
    }

    public void setTaodata(List<PayOrderTaoSku> taodata) {
        this.taodata = taodata;
    }
}
