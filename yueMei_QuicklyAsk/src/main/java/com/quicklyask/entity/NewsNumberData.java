package com.quicklyask.entity;

public class NewsNumberData {

	private String ask_num;//	问题未读消息数
	private String share_num;//评论未读消息数
	private String notice_num;//通知未读消息数
	private String sixin_num;//私信未读消息数
	private String zong_num;//总未读消息数
	private String agree_num;//赞未读消息数
	private String follow_num;//关注未读消息数

	public String getAsk_num() {
		return ask_num;
	}

	public void setAsk_num(String ask_num) {
		this.ask_num = ask_num;
	}

	public String getShare_num() {
		return share_num;
	}

	public void setShare_num(String share_num) {
		this.share_num = share_num;
	}

	public String getNotice_num() {
		return notice_num;
	}

	public void setNotice_num(String notice_num) {
		this.notice_num = notice_num;
	}

	public String getSixin_num() {
		return sixin_num;
	}

	public void setSixin_num(String sixin_num) {
		this.sixin_num = sixin_num;
	}

	public String getZong_num() {
		return zong_num;
	}

	public void setZong_num(String zong_num) {
		this.zong_num = zong_num;
	}

	public String getAgree_num() {
		return agree_num;
	}

	public void setAgree_num(String agree_num) {
		this.agree_num = agree_num;
	}

	public String getFollow_num() {
		return follow_num;
	}

	public void setFollow_num(String follow_num) {
		this.follow_num = follow_num;
	}
}
