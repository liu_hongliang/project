package com.module.commonview.module.bean;

/**
 * Created by dwb on 16/3/29.
 */
public class CashBackData {

    private String id;
    private String cashback_require_id;
    private String cashback_require;
    private String cashback_require_desc;
    private String is_complete;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCashback_require_id() {
        return cashback_require_id;
    }

    public void setCashback_require_id(String cashback_require_id) {
        this.cashback_require_id = cashback_require_id;
    }

    public String getCashback_require() {
        return cashback_require;
    }

    public void setCashback_require(String cashback_require) {
        this.cashback_require = cashback_require;
    }

    public String getCashback_require_desc() {
        return cashback_require_desc;
    }

    public void setCashback_require_desc(String cashback_require_desc) {
        this.cashback_require_desc = cashback_require_desc;
    }

    public String getIs_complete() {
        return is_complete;
    }

    public void setIs_complete(String is_complete) {
        this.is_complete = is_complete;
    }
}
