/**
 * 
 */
package com.module.my.controller.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.my.model.bean.BaikeItemData;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class BaikeItemAdapter extends BaseAdapter {

	private final String TAG = "BaikeItemAdapter";
	private final Activity mActivity;

	private List<BaikeItemData> mBaikeItemData;
	private LayoutInflater inflater;
	private BaikeItemData baikeItemData;
	ViewHolder viewHolder;

	public BaikeItemAdapter(Activity mContext, List<BaikeItemData> mBaikeItemData) {
		this.mActivity = mContext;
		this.mBaikeItemData = mBaikeItemData;
		inflater = LayoutInflater.from(mContext);
	}

	static class ViewHolder {
		public TextView mtitleTv;
		public TextView mshiyingTv;
		public ImageView guanzhuIv;
		public ImageView safeIv;
		public ImageView fuzaIv;
	}

	@Override
	public int getCount() {
		return mBaikeItemData.size();
	}

	@Override
	public Object getItem(int position) {
		return mBaikeItemData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_collect_baike, null);
			viewHolder = new ViewHolder();

			viewHolder.mtitleTv = convertView
					.findViewById(R.id.item_baike_title_tv);
			viewHolder.mshiyingTv = convertView
					.findViewById(R.id.item_baike_shiying_tv);
			viewHolder.guanzhuIv =  convertView
					.findViewById(R.id.item_baike_guanzhu_iv);
			viewHolder.safeIv = convertView
					.findViewById(R.id.item_baike_safe_iv);
			viewHolder.fuzaIv =  convertView
					.findViewById(R.id.item_baike_fuza_iv);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		baikeItemData = mBaikeItemData.get(position);

		viewHolder.mtitleTv.setText(baikeItemData.getTitle());
		viewHolder.mshiyingTv.setText(baikeItemData.getSuitable_crowd());

		String guanzhu = baikeItemData.getWelcome();
		if (guanzhu.equals("0")) {
			viewHolder.guanzhuIv.setBackgroundResource(R.drawable.item_bk_d0);
		} else if (guanzhu.equals("1")) {
			viewHolder.guanzhuIv.setBackgroundResource(R.drawable.item_bk_d1);
		} else if (guanzhu.equals("2")) {
			viewHolder.guanzhuIv.setBackgroundResource(R.drawable.item_bk_d2);
		} else if (guanzhu.equals("3")) {
			viewHolder.guanzhuIv.setBackgroundResource(R.drawable.item_bk_d3);
		} else if (guanzhu.equals("4")) {
			viewHolder.guanzhuIv.setBackgroundResource(R.drawable.item_bk_d4);
		} else if (guanzhu.equals("5")) {
			viewHolder.guanzhuIv.setBackgroundResource(R.drawable.item_bk_d5);
		}

		String safe = baikeItemData.getSafety();
		if (safe.equals("0")) {
			viewHolder.safeIv.setBackgroundResource(R.drawable.item_bk_d0);
		} else if (safe.equals("1")) {
			viewHolder.safeIv.setBackgroundResource(R.drawable.item_bk_d1);
		} else if (safe.equals("2")) {
			viewHolder.safeIv.setBackgroundResource(R.drawable.item_bk_d2);
		} else if (safe.equals("3")) {
			viewHolder.safeIv.setBackgroundResource(R.drawable.item_bk_d3);
		} else if (safe.equals("4")) {
			viewHolder.safeIv.setBackgroundResource(R.drawable.item_bk_d4);
		} else if (safe.equals("5")) {
			viewHolder.safeIv.setBackgroundResource(R.drawable.item_bk_d5);
		}

		String fuza = baikeItemData.getComplexity();
		if (fuza.equals("0")) {
			viewHolder.fuzaIv.setBackgroundResource(R.drawable.item_bk_d0);
		} else if (fuza.equals("1")) {
			viewHolder.fuzaIv.setBackgroundResource(R.drawable.item_bk_d1);
		} else if (fuza.equals("2")) {
			viewHolder.fuzaIv.setBackgroundResource(R.drawable.item_bk_d2);
		} else if (fuza.equals("3")) {
			viewHolder.fuzaIv.setBackgroundResource(R.drawable.item_bk_d3);
		} else if (fuza.equals("4")) {
			viewHolder.fuzaIv.setBackgroundResource(R.drawable.item_bk_d4);
		} else if (fuza.equals("5")) {
			viewHolder.fuzaIv.setBackgroundResource(R.drawable.item_bk_d5);
		}

		return convertView;
	}

	public void addData(List<BaikeItemData> infos) {
		mBaikeItemData.addAll(infos);
	}

	public List<BaikeItemData> getmBaikeItemData() {
		return mBaikeItemData;
	}
}
