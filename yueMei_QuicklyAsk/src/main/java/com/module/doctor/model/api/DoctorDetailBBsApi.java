package com.module.doctor.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.community.model.bean.BBsListData550;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/10/24.
 */

public class DoctorDetailBBsApi implements BaseCallBackApi {
    private String TAG = "DoctorDetailBBsApi";

    private HashMap<String, Object> hashMap;  //传值容器

    public DoctorDetailBBsApi() {
        hashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.DOCTOR, "userpost", maps, new ServerCallback() {


            @Override
            public void onServerCallback(ServerData mData) {
                if ("1".equals(mData.code)){
                    try {

                        ArrayList<BBsListData550> bBsListData550 = JSONUtil.jsonToArrayList(mData.data, BBsListData550.class);
                        Log.e(TAG, "listener == " + listener);
                        Log.e(TAG, "bBsListData550 == " + bBsListData550);
                        listener.onSuccess(bBsListData550);

                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return hashMap;
    }

    public void addData(String key, String value) {
        hashMap.put(key, value);
    }
}
