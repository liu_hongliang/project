package com.module.other.adapter;

import android.widget.ImageView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.module.MyApplication;
import com.quicklyask.activity.R;
import com.taobao.weex.adapter.IWXImgLoaderAdapter;
import com.taobao.weex.common.WXImageStrategy;
import com.taobao.weex.dom.WXImageQuality;

public class ImageAdapter implements IWXImgLoaderAdapter {
    @Override
    public void setImage(String url, ImageView imageView, WXImageQuality quality, WXImageStrategy wxImageStrategy) {
        DrawableTypeRequest<String> loadRequest = Glide.with(MyApplication.getContext()).load(url);
        if (wxImageStrategy != null && wxImageStrategy.placeHolder != null) { // 如果设置有展位图，则展位图和加载失败都用展位图来显示
            int resId = 0;
            try {
                resId = R.drawable.class.getField(wxImageStrategy.placeHolder.replace("local:///", "")).getInt(null);
            } catch (Exception e) {
            }
            if (resId != 0) {
                loadRequest.placeholder(resId).error(resId);
            }
        }
        loadRequest.into(imageView);


    }
}
