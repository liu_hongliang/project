package com.module.commonview.view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;

/**
 * 红底白字关注按钮
 * Created by 裴成浩 on 2018/6/5.
 */
public class FocusButton extends LinearLayout {

    private Context mContext;
    private ImageView mFocusFlag;   //图标
    private TextView mFocusTile;    //文字

    public FocusButton(Context context) {
        this(context, null);
    }

    public FocusButton(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FocusButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    /**
     * 初始化按钮样式
     */
    private void initView() {
        setOrientation(HORIZONTAL); //水平方向
        setGravity(Gravity.CENTER); //内部居中

        mFocusFlag = new ImageView(mContext);
        MarginLayoutParams marginLayoutParams1 = new MarginLayoutParams(Utils.dip2px(mContext, 8), Utils.dip2px(mContext, 8));
        marginLayoutParams1.leftMargin = Utils.dip2px(mContext, 7);
        mFocusFlag.setLayoutParams(marginLayoutParams1);

        mFocusTile = new TextView(mContext);
        MarginLayoutParams marginLayoutParams2 = new MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        marginLayoutParams2.leftMargin = Utils.dip2px(mContext, 5);
        marginLayoutParams2.rightMargin = Utils.dip2px(mContext, 7);
        mFocusTile.setLayoutParams(marginLayoutParams2);

        addView(mFocusFlag);
        addView(mFocusTile);
    }

    /**
     * 设置按钮类型
     */
    public void setFocusType(FocusType focusType) {
        switch (focusType) {
            case NOT_FOCUS:             //未关注
                setFocusView(R.drawable.shape_focu_gradient_ff5c77, R.drawable.focus_plus_sign, "#ffffff", "关注");
                break;
            case HAS_FOCUS:             //已关注
                setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.focus_plus_sign_yes, "#999999", "已关注");
                break;
            case EACH_FOCUS:            //互相关注
                setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.each_focus, "#999999", "互相关注");
                break;
        }
    }

    /**
     * 修改关注样式
     *
     * @param drawable：
     * @param image
     * @param color
     * @param text
     */
    private void setFocusView(int drawable, int image, String color, String text) {
        setBackground(ContextCompat.getDrawable(mContext, drawable));
        mFocusFlag.setImageResource(image);
        mFocusTile.setTextColor(Color.parseColor(color));
        mFocusTile.setText(text);

        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        switch (text.length()) {
            case 2:
                layoutParams.width = Utils.dip2px(mContext, 68);
                break;
            case 3:
                layoutParams.width = Utils.dip2px(mContext, 78);
                break;
            case 4:
                layoutParams.width = Utils.dip2px(mContext, 90);
                break;
        }

        setLayoutParams(layoutParams);
    }

    public interface ClickCallBack {
        void onClick(View v);
    }

    /**
     * 关注按钮点击
     *
     * @param clickCallBack
     */
    public void setFocusClickListener(final String isFollowing, final ClickCallBack clickCallBack) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)) {
                    switch (isFollowing) {
                        case "0":          //改为已关注
                            setFocusType(FocusButton.FocusType.HAS_FOCUS);
                            clickCallBack.onClick(v);
                            break;
                        case "1":          //改为未关注
                        case "2":          //改为未关注
                            showDialogExitEdit(clickCallBack);
                            break;
                    }
                }
            }
        });
    }

    /**
     * 未关注、已关注、互相关注
     */
    public enum FocusType {
        NOT_FOCUS, HAS_FOCUS, EACH_FOCUS
    }

    private void showDialogExitEdit(final ClickCallBack clickCallBack) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("确定取消关注？");
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确认");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                clickCallBack.onClick(v);
                setFocusType(FocusButton.FocusType.NOT_FOCUS);
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("取消");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }
}
