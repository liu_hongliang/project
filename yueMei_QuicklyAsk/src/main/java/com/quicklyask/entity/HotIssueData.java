package com.quicklyask.entity;

import java.util.List;

/**
 * 大家都在聊数据
 * 
 * @author Rubin
 * 
 */
public class HotIssueData {

	private String url;
	private List<HotPic> pic;
	private String _id;
	private String q_id;
	private String title;
	private String user_id;
	private String user_name;
	// answer_num
	private String answer_num;
	private String time;

	private String askorshare;

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the pic
	 */
	public List<HotPic> getPic() {
		return pic;
	}

	/**
	 * @param pic
	 *            the pic to set
	 */
	public void setPic(List<HotPic> pic) {
		this.pic = pic;
	}

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the q_id
	 */
	public String getQ_id() {
		return q_id;
	}

	/**
	 * @param q_id
	 *            the q_id to set
	 */
	public void setQ_id(String q_id) {
		this.q_id = q_id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the user_id
	 */
	public String getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id
	 *            the user_id to set
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return the user_name
	 */
	public String getUser_name() {
		return user_name;
	}

	/**
	 * @param user_name
	 *            the user_name to set
	 */
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	/**
	 * @return the answer_num
	 */
	public String getAnswer_num() {
		return answer_num;
	}

	/**
	 * @param answer_num
	 *            the answer_num to set
	 */
	public void setAnswer_num(String answer_num) {
		this.answer_num = answer_num;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the askorshare
	 */
	public String getAskorshare() {
		return askorshare;
	}

	/**
	 * @param askorshare
	 *            the askorshare to set
	 */
	public void setAskorshare(String askorshare) {
		this.askorshare = askorshare;
	}

}
