package com.module.home.view;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.home.model.bean.HotWordsData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/7/26
 */
public class SearchInitHotFlowLayout {
    private final String TAG = "SearchInitFlowLayout";
    private Activity mContext;
    private FlowLayout mFlowLayout;
    private List<HotWordsData> mSearchWordes;

    public SearchInitHotFlowLayout(Activity context, FlowLayout flowLayout, List<HotWordsData> searchWordes) {
        this.mContext = context;
        this.mFlowLayout = flowLayout;
        this.mSearchWordes = searchWordes;
        initView();
    }

    /**
     * 初始化样式
     */
    private void initView() {
        mFlowLayout.removeAllViews();
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(Utils.dip2px(10), 0, Utils.dip2px(10), 0);
        for (int i = 0; i < mSearchWordes.size(); i++) {
            HotWordsData hotWordsData = mSearchWordes.get(i);
            TextView view = new TextView(mContext);
            view.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            view.setText(hotWordsData.getKeywords());

            //设置热卖图标
            if ("1".equals(hotWordsData.getIs_push())) {
                Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.search_init_hot);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                view.setCompoundDrawables(null, null, drawable, null);
                view.setCompoundDrawablePadding(Utils.dip2px(1));
            }

            view.setBackgroundResource(R.drawable.shape_bian_yuanjiao_f6f6f6);

            mFlowLayout.addView(view, i, lp);

            view.setTag(i);
            view.setOnClickListener(new onClickView());
        }
    }

    /**
     * 点击按钮回调
     */
    class onClickView implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int selected = (int) v.getTag();
            for (int i = 0; i < mSearchWordes.size(); i++) {
                if (i == selected) {
                    if (clickCallBack != null) {
                        clickCallBack.onClick(v, i, mSearchWordes.get(i));
                    }
                }
            }
        }
    }

    private ClickCallBack clickCallBack;

    public interface ClickCallBack {
        void onClick(View v, int pos, HotWordsData data);
    }

    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

}
