package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.module.commonview.module.bean.DiaryReplyListPic;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform2;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import org.xutils.common.util.DensityUtil;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/7.
 */
public class MoreReplyImgListAdapter extends RecyclerView.Adapter<MoreReplyImgListAdapter.ViewHolder> {

    private String TAG = "MoreReplyImgListAdapter";
    private Context mContext;
    private List<DiaryReplyListPic> mDatas;
    private LayoutInflater mInflater;
    private int mWindowsWight;

    public MoreReplyImgListAdapter(Context context, List<DiaryReplyListPic> datas, int windowsWight) {
        this.mContext = context;
        this.mDatas = datas;
        this.mWindowsWight = windowsWight;
        mInflater = LayoutInflater.from(context);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_diary_list_recycview_comments_img_list, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DiaryReplyListPic data = mDatas.get(position);
        if (mDatas != null && mDatas.size() == 1) {
            ViewGroup.LayoutParams layoutParams = holder.mImg.getLayoutParams();
            layoutParams.height = (mWindowsWight - DensityUtil.dip2px(93)) / 3;
            layoutParams.width = (mWindowsWight - DensityUtil.dip2px(93)) / 3;
            holder.mImg.setLayoutParams(layoutParams);
            //一张宽度固定 高度自适应
            Glide.with(mContext)
                    .load(data.getYuan())
                    .bitmapTransform(new CenterCrop(mContext), new GlideRoundTransform(mContext, DensityUtil.dip2px(7)))
                    .placeholder(R.drawable.ten_ball_placeholder)
                    .error(R.drawable.ten_ball_placeholder)
                    .into(holder.mImg);
        } else {
            ViewGroup.LayoutParams layoutParams = holder.mImg.getLayoutParams();
            layoutParams.height = (mWindowsWight - DensityUtil.dip2px(93)) / 3;
            layoutParams.width = (mWindowsWight - DensityUtil.dip2px(93)) / 3;
            holder.mImg.setLayoutParams(layoutParams);
            //不为一张  长宽固定 缩放
            Glide.with(mContext)
                    .load(data.getYuan())
                    .bitmapTransform(new CenterCrop(mContext), new GlideRoundTransform(mContext, DensityUtil.dip2px(7)))
                    .placeholder(R.drawable.ten_ball_placeholder)
                    .error(R.drawable.ten_ball_placeholder)
                    .into(holder.mImg);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mImg;

        public ViewHolder(View itemView) {
            super(itemView);
            mImg = itemView.findViewById(R.id.item_diary_list_recycview_comments_img_list);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (itemJumpCallBackListener != null) {
                        itemJumpCallBackListener.onItemLongClick(v);
                    }
                    return false;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemJumpCallBackListener != null) {
                        itemJumpCallBackListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    //查看更多评论点击回调
    private ItemJumpCallBackListener itemJumpCallBackListener;

    public interface ItemJumpCallBackListener {
        void onItemClick(View v, int position);

        void onItemLongClick(View v);
    }

    public void setItemJumpCallBackListener(ItemJumpCallBackListener itemJumpCallBackListener) {
        this.itemJumpCallBackListener = itemJumpCallBackListener;
    }
}