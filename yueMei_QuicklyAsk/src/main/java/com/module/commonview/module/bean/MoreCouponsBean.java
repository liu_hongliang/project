package com.module.commonview.module.bean;

import java.util.List;

public class MoreCouponsBean {

    private List<MessageBean.CouponsBean>receiveSuccess;

    public List<MessageBean.CouponsBean> getReceiveSuccess() {
        return receiveSuccess;
    }

    public void setReceiveSuccess(List<MessageBean.CouponsBean> receiveSuccess) {
        this.receiveSuccess = receiveSuccess;
    }
}
