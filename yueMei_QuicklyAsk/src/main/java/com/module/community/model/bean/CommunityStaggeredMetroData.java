package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2018/7/17.
 */
public class CommunityStaggeredMetroData {
    private String url;
    private String img;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
