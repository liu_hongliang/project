package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class SearchEntry implements Parcelable {
    private String key_type;
    private String show_title;
    private String search_title;
    private String link;
    private HashMap<String,String> event_params;


    public String getKey_type() {
        return key_type;
    }

    public void setKey_type(String key_type) {
        this.key_type = key_type;
    }

    public String getShow_title() {
        return show_title;
    }

    public void setShow_title(String show_title) {
        this.show_title = show_title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSearch_title() {
        return search_title;
    }

    public void setSearch_title(String search_title) {
        this.search_title = search_title;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.key_type);
        dest.writeString(this.show_title);
        dest.writeString(this.link);
        dest.writeString(this.search_title);
        dest.writeSerializable(this.event_params);
    }

    public SearchEntry() {
    }

    protected SearchEntry(Parcel in) {
        this.key_type = in.readString();
        this.show_title = in.readString();
        this.link = in.readString();
        this.search_title = in.readString();
        this.event_params = (HashMap<String, String>) in.readSerializable();
    }

    public static final Parcelable.Creator<SearchEntry> CREATOR = new Parcelable.Creator<SearchEntry>() {
        @Override
        public SearchEntry createFromParcel(Parcel source) {
            return new SearchEntry(source);
        }

        @Override
        public SearchEntry[] newArray(int size) {
            return new SearchEntry[size];
        }
    };
}
