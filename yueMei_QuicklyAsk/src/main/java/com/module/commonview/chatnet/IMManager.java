package com.module.commonview.chatnet;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.cookie.store.CookieStore;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.module.commonview.activity.ChatActivity;
import com.module.commonview.adapter.ChatRecyclerAdapter;
import com.module.commonview.broadcast.MessageNumChangeReceive;
import com.module.commonview.broadcast.MessageService;
import com.module.commonview.broadcast.SendMessageReceiver;
import com.module.commonview.module.bean.LocusData;
import com.module.commonview.module.bean.MessageBean;
import com.module.commonview.module.bean.Pong;
import com.module.commonview.module.bean.WebSocketBean;
import com.module.commonview.module.other.MessageCallBack;
import com.module.other.netWork.SignUtils;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Cookie;
import okhttp3.HttpUrl;
import okhttp3.Response;
import okhttp3.WebSocket;


/**
 * Created by Administrator on 2018/1/12.
 */

public class IMManager implements ReceiveMessageCallBack, MessageStatus {
    public static final String TAG = "IMManager";
    private static IMManager instance;
    private String domain = "chat.yuemei.com";
    private long expiresAt = 1544493729973L;
    private String name = "";
    private String path = "/";
    private String value = "";
    public static MessageCallBack mMessageCallBack;
    private Context mContext;

    private IMManager(Context context) {
        mContext = context;
        IMNetWork.getInstance(mContext).setMessageCallBack(this);
        IMNetWork.getInstance(mContext).setMessageStatus(this);
    }

    public static void setMessageCallBack(MessageCallBack messageCallBack) {
        mMessageCallBack = messageCallBack;
    }


    public static IMManager getInstance(Context context) {
        if (instance == null) {
            synchronized (IMManager.class) {
                if (instance == null) {
                    instance = new IMManager(context);

                }
            }
        }
        return instance;
    }

    public IMNetWork getIMNetInstance() {
        return IMNetWork.getInstance(mContext);
    }

    @Override
    public void messageStatus(Status status) {

    }

    @Override
    public void webSocketCallBack(Context context, WebSocket webSocket, String text) {
        String type = JSONUtil.resolveJson(text, "type");
        switch (type) {
            case "init":
                String mReplace = Utils.getYuemeiInfo();
                bindWebSocket(text, mReplace);
                break;
            case "ping":
                Log.e(TAG, "ping<<<<<<<<<<<<<<<");
                webSocket.send(new Gson().toJson(new Pong("pong")));
                break;
            case "say":
                try {
                    Log.e(TAG, "say>>>>>>>>>>>>>>>>>>>>>"+text);
                    WebSocketBean webSocketBean = JSONUtil.TransformSingleBean(text, WebSocketBean.class);
                    Gson gson = new GsonBuilder()
                            .registerTypeAdapter(MessageBean.DataBean.class, new MessageBean.DataBean.MessageDeserializer())
                            .create();
                    MessageBean.DataBean dataBean = gson.fromJson(text, MessageBean.DataBean.class);

                    dataBean.setContent(webSocketBean.getAppcontent());
                    dataBean.setUser_avatar(webSocketBean.getFrom_client_img());
                    dataBean.setFromUserId(webSocketBean.getFrom_client_id());
                    dataBean.setFromName(webSocketBean.getFrom_client_name());
                    dataBean.handlerMessageTypeAndViewStatus();

                    Intent messageNumChangeIntent = new Intent(MessageNumChangeReceive.ACTION);
                    messageNumChangeIntent.putExtra(MessageNumChangeReceive.HOS_ID,webSocketBean.getHos_id()+"");
                    context.sendBroadcast(messageNumChangeIntent);



                    if (mMessageCallBack != null){
                        mMessageCallBack.receiveMessage(dataBean,webSocketBean.getGroup_id()+"");
                    }


                    if (Utils.isBackground(context)){ //如果在后台发送广播
                        Intent intent = new Intent(SendMessageReceiver.ACTION);
                        intent.putExtra("message", webSocketBean.getAppcontent());
                        intent.putExtra("clientid", webSocketBean.getFrom_client_id() +"");
                        Log.e(TAG, "message === " + webSocketBean.getAppcontent());
                        Log.e(TAG, "clientid === " + webSocketBean.getFrom_client_id() +"");
                        context.sendBroadcast(intent);

                    }else {
                        if (!isMainActivityTop()){  //如果在前台其他页面（非聊天页ChatBaseActivity）
                            Intent intent = new Intent(FinalConstant.REFRESH_MESSAGE);
                            intent.putExtra("message", webSocketBean.getAppcontent());
                            intent.putExtra("time", webSocketBean.getTime());
                            intent.putExtra("clientid", webSocketBean.getFrom_client_id() + "");
                            context.sendBroadcast(intent);
                            Intent intent1 = new Intent(context, MessageService.class);
                            intent1.putExtra("message",webSocketBean.getAppcontent());
                            intent1.putExtra("clientid",webSocketBean.getFrom_client_id() + "");
                            intent1.putExtra("hosname",webSocketBean.getHos_name());
                            intent1.putExtra("hosimg",webSocketBean.getFrom_client_img());
                            intent1.putExtra("hosid",webSocketBean.getHos_id()+"");
                            intent1.putExtra("pos",webSocketBean.getPos());
                            context.startService(intent1);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "e === " + e.toString());
                    e.printStackTrace();
                }
                break;
            case "say_self":
                break;
            case"onfocus":
                Log.e(TAG, "onfocus<<<<<<<<<<<<<<<");
                WebSocketBean webSocketBean = JSONUtil.TransformSingleBean(text, WebSocketBean.class);
                if (mMessageCallBack != null){
                    mMessageCallBack.onFocusCallBack(webSocketBean.getContent());
                }
                break;
        }
    }




    private boolean isMainActivityTop(){
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        String name = manager.getRunningTasks(1).get(0).topActivity.getClassName();
        return name.equals(ChatActivity.class.getName());
    }

    private void bindWebSocket(String string, String mReplace) {
        Log.e(TAG, "bindWebSocket == " + string);
        String client_id = JSONUtil.resolveJson(string, "client_id");
        Map<String, Object> maps = new HashMap<>();
        maps.put("client_id", client_id);
        HttpHeaders headers = SignUtils.buildHttpHeaders(maps);
        HttpParams httpParams = SignUtils.buildHttpParam5(maps);
        CookieConfig.getInstance().setCookie("https","chat.yuemei.com","chat.yuemei.com");
        CookieStore cookieStore = OkGo.getInstance().getCookieJar().getCookieStore();
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("chat.yuemei.com")
                .build();

        List<Cookie> cookies = cookieStore.loadCookie(httpUrl);
        Log.e(TAG, "cookies == " + cookies);

        for (Cookie cookie : cookies) {
            domain = cookie.domain();
            expiresAt = cookie.expiresAt();
            name = cookie.name();
            path = cookie.path();
            value = cookie.value();

            Log.e(TAG, "domain == " + domain);
            Log.e(TAG, "expiresAt == " + expiresAt);
            Log.e(TAG, "name == " + name);
            Log.e(TAG, "path == " + path);
            Log.e(TAG, "value == " + value);
        }

        Log.e(TAG, "replace == " + mReplace);
        cookieStore.removeCookie(httpUrl);
        Cookie yuemeiinfo = new Cookie.Builder()
                .name("yuemeiinfo")
                .value(mReplace)
                .domain(domain)
                .expiresAt(expiresAt)
                .path(path)
                .build();

        cookieStore.saveCookie(httpUrl, yuemeiinfo);
        List<Cookie> cookies1 = cookieStore.loadCookie(httpUrl);
        OkGo.post(FinalConstant.baseNewsUrl + FinalConstant.BIND)
                .cacheMode(CacheMode.DEFAULT)
                .headers(headers)
                .params(httpParams)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.d("MyApplication", "bind--------" + s);
                    }
                });
    }

}




