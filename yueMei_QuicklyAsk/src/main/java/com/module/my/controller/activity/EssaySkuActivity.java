package com.module.my.controller.activity;

import android.content.Intent;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.Expression;
import com.module.my.model.api.PostTextQueApi;
import com.module.my.model.bean.ForumTextData;
import com.module.my.model.bean.NoteBookListData;
import com.module.my.model.bean.PostingUploadImage;
import com.module.my.view.fragment.PostingAndNoteImageVideoFragment;
import com.module.my.view.view.EssaySkuDialog;
import com.module.my.view.view.PostAndNoteContentEditText;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.other.RequestAndResultCode;
import com.quicklyask.activity.R;
import com.quicklyask.entity.WriteResultData;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 短评详情
 */
public class EssaySkuActivity extends YMBaseActivity {

    @BindView(R.id.essay_sku_top)
    CommonTopBar mTop;
    @BindView(R.id.essay_sku_sku_image)
    ImageView mSkuImage;
    @BindView(R.id.essay_sku_sku_title)
    TextView mSkuTitle;
    @BindView(R.id.essay_sku_all_rating)
    ProgressBar mAllRating;
    @BindView(R.id.essay_sku_all_copy)
    TextView mAllCopy;
    @BindView(R.id.essay_sku_service_rating)
    RatingBar mServiceRating;
    @BindView(R.id.essay_sku_service_copy)
    TextView mServiceCopy;
    @BindView(R.id.essay_sku_surgery_rating)
    RatingBar mSurgeryRating;
    @BindView(R.id.essay_sku_surgery_copy)
    TextView mSurgeryCopy;
    @BindView(R.id.essay_sku_doctor_rating)
    RatingBar mDoctorRating;
    @BindView(R.id.essay_sku_doctor_copy)
    TextView mDoctorCopy;
    @BindView(R.id.essay_sku_content)
    PostAndNoteContentEditText mContent;
    private PostingAndNoteImageVideoFragment postingAndNoteImageVideoFragment;
    private PostTextQueApi postTextQueApi;
    private String TAG = "EssaySkuActivity";

    private final String POOR = "差";
    private final String NOT_SATISFIED_WITH = "一般";
    private final String GENERAL = "还可以";
    private final String SATISFIED = "满意";
    private final String VERY_SATISFIED_WITH = "非常满意";
    private NoteBookListData skuData;
    private Gson mGson;
    private WriteResultData mWriteResultData;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_essay_sku;
    }

    @Override
    protected void initView() {

        String data = getIntent().getStringExtra("data");
        Log.e(TAG, "data == " + data);
        skuData = JSONUtil.TransformSingleBean(data, NoteBookListData.class);

        //关闭按钮点击
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                showDialogExitEdit(EssaySkuDialog.DialogType.TYPE1,"评价一下再走吧，我们需要你~");
            }
        });

        //设置SKU图片
        mFunctionManager.setRoundImageSrc(mSkuImage, skuData.getImg(), Utils.dip2px(7));

        //设置SKU文案
        mSkuTitle.setText(skuData.getTitle());

        //发布按钮点击
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (uploadingPrompt()) {
                    if (mContent.getContent().length() >= 8) {
                        if (mContent.getContent().length() <= 100) {
                            mDialog.startLoading();
                            uploadData();
                        } else {
                            mFunctionManager.showShort("文字数量不能大于100");
                        }
                    } else {
                        mFunctionManager.showShort("文字最少大于8");
                    }
                }
            }
        });

        //服务评分
        mServiceRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 0) {
                    ratingBar.setRating(1);
                    setRatingTitle(mServiceCopy, 1);
                } else {
                    setRatingTitle(mServiceCopy, rating);
                }
            }
        });

        //手术评分
        mSurgeryRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 0) {
                    ratingBar.setRating(1);
                    setRatingTitle(mSurgeryCopy, 1);
                } else {
                    setRatingTitle(mSurgeryCopy, rating);
                }
            }
        });

        //医生评分
        mDoctorRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 0) {
                    ratingBar.setRating(1);
                    setRatingTitle(mDoctorCopy, 1);
                } else {
                    setRatingTitle(mDoctorCopy, rating);
                }
            }
        });

        //初始化编辑框
        mContent.setContentHint(PostAndNoteContentEditText.EditTitle5);
        //编辑框回调
        mContent.setClickCallBack(new PostAndNoteContentEditText.ClickCallBack() {
            @Override
            public void onContentEditorClick(View v) {
                Intent it = new Intent(mContext, WriteFontActivity.class);
                it.putExtra("text_str", mContent.getContent());
                startActivityForResult(it, RequestAndResultCode.POSTING_NOTE_FONT);
            }

            @Override
            public void onReleaseClick(boolean isClick) {
            }
        });

        //初始化图片视频上传
        postingAndNoteImageVideoFragment = PostingAndNoteImageVideoFragment.newInstance();
        setActivityFragment(R.id.essay_sku_img_video_framelayout, postingAndNoteImageVideoFragment);
    }

    @Override
    protected void initData() {
        postTextQueApi = new PostTextQueApi();
        mGson = new Gson();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestAndResultCode.POSTING_NOTE_FONT && data != null) {        //内容编辑返回
            setQequestContent(data);
        }
    }

    /**
     * 上传数据
     */
    private void uploadData() {
        mDialog.startLoading();

        postTextQueApi.addData("hosname", skuData.getHosname());            //医院名称
        postTextQueApi.addData("docname", skuData.getDocname());            //医生名称
        postTextQueApi.addData("hosid", skuData.getHos_id());               //医院id
        postTextQueApi.addData("taoid", skuData.get_id());                  //SkuId
        postTextQueApi.addData("title", Utils.getSecondTimestamp() + "");             //日记本标题

        postTextQueApi.addData("server_id", skuData.getServer_id());
        postTextQueApi.addData("content", mGson.toJson(new ForumTextData(mContent.getContent())));         //内容
        postTextQueApi.addData("askorshare", "11");                       //标识

        //判断是上传图片还是上传视频
        final String videoJson = postingAndNoteImageVideoFragment.mImgVideo.getVideoJson();
        final String imageJson = postingAndNoteImageVideoFragment.mImgVideo.getImageJson();
        if (!TextUtils.isEmpty(videoJson)) {
            postTextQueApi.addData("video", videoJson);                                 //视频json
            postTextQueApi.addData("cover_photo", postingAndNoteImageVideoFragment.mImgVideo.getCoverJson());                     //封面图
            Log.e(TAG, "video == " + videoJson);
        } else if (!TextUtils.isEmpty(imageJson)) {
            postTextQueApi.addData("image", imageJson);                                 //图片json
            postTextQueApi.addData("cover_photo", postingAndNoteImageVideoFragment.mImgVideo.getCoverJson());                     //封面图
            Log.e(TAG, "image == " + imageJson);
        }

        postTextQueApi.addData("service", mServiceRating.getRating() + "");                       //服务评分
        postTextQueApi.addData("effect", mSurgeryRating.getRating() + "");                        //手术评分
        postTextQueApi.addData("pf_doctor", mDoctorRating.getRating() + "");                      //医生评分


//        Log.e(TAG, "hosname == " + skuData.getHosname());
//        Log.e(TAG, "docname == " + skuData.getDocname());
//        Log.e(TAG, "hosid == " + skuData.getHos_id());
//        Log.e(TAG, "taoid == " + skuData.get_id());
//        Log.e(TAG, "title == " + mContent.getContent());
//
//        Log.e(TAG, "server_id == " + skuData.getServer_id());
//        Log.e(TAG, "content == " + mGson.toJson(new ForumTextData(mContent.getContent())));
//
//        Log.e(TAG, "cover_photo == " + postingAndNoteImageVideoFragment.mImgVideo.getCoverJson());
//
//        Log.e(TAG, "service == " + mServiceRating.getRating());
//        Log.e(TAG, "effect == " + mSurgeryRating.getRating());
//        Log.e(TAG, "pf_doctor == " + mDoctorRating.getRating());


        postTextQueApi.getCallBack(mContext, postTextQueApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "serverData == " + serverData.toString());
                mDialog.stopLoading();
                if ("1".equals(serverData.code)) {
                    mWriteResultData = JSONUtil.TransformSingleBean(serverData.data, WriteResultData.class);

                    ArrayList<PostingUploadImage> postingUploadImages = JSONUtil.jsonToArrayList(imageJson, PostingUploadImage.class);
                    if (mContent.getContent().length() >= 50 && (!TextUtils.isEmpty(videoJson) || (!TextUtils.isEmpty(imageJson) && postingUploadImages.size() >= 3))) {
                        showDialogExitEdit(EssaySkuDialog.DialogType.TYPE3,serverData.message);
                    } else {
                        showDialogExitEdit(EssaySkuDialog.DialogType.TYPE2,serverData.message);
                    }
                } else {
                    mFunctionManager.showShort(serverData.message);
                }
            }
        });
    }

    /**
     * 评分文案设置
     *
     * @param mCopy
     * @param rating
     */
    private void setRatingTitle(TextView mCopy, float rating) {
        switch ((int) rating) {
            case 1:
                mCopy.setText(POOR);
                break;
            case 2:
                mCopy.setText(NOT_SATISFIED_WITH);
                break;
            case 3:
                mCopy.setText(GENERAL);
                break;
            case 4:
                mCopy.setText(SATISFIED);
                break;
            case 5:
                mCopy.setText(VERY_SATISFIED_WITH);
                break;
            default:
                mCopy.setText("");
        }

        //设置总评分
        float serviceNumber = mServiceRating.getRating();
        float surgeryNumber = mSurgeryRating.getRating();
        float doctorNumber = mDoctorRating.getRating();
        int totalNumber = (int) ((serviceNumber + surgeryNumber + doctorNumber) / 3);
        mAllRating.setProgress(totalNumber);
        switch (totalNumber) {
            case 1:
                mAllCopy.setText(POOR);
                break;
            case 2:
                mAllCopy.setText(NOT_SATISFIED_WITH);
                break;
            case 3:
                mAllCopy.setText(GENERAL);
                break;
            case 4:
                mAllCopy.setText(SATISFIED);
                break;
            case 5:
                mAllCopy.setText(VERY_SATISFIED_WITH);
                break;
            default:
                mAllCopy.setText("");
        }
    }

    /**
     * 设置编辑框内容
     *
     * @param data
     */
    private void setQequestContent(Intent data) {
        String ss = data.getStringExtra("text_str");
        if (ss.length() > 0) {
            try {
                SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(ss, mContext, Utils.dip2px(12));
                mContent.setText(stringBuilder);
                mContent.setSelection(mContent.getContentLength()); //将光标移至文字末尾
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 图片或视频上传中提示
     *
     * @return true 上传完成  false正在上传
     */
    public boolean uploadingPrompt() {
        if (postingAndNoteImageVideoFragment != null) {
            return postingAndNoteImageVideoFragment.mImgVideo.uploadingPrompt();
        } else {
            return true;
        }
    }

    /**
     * 返回按钮弹层
     */
    private void showDialogExitEdit(final EssaySkuDialog.DialogType dialogType,String message) {
        EssaySkuDialog essaySkuDialog = new EssaySkuDialog(mContext);
        essaySkuDialog.initView(dialogType,message);
        essaySkuDialog.show();
        essaySkuDialog.setBtnClickListener(new EssaySkuDialog.BtnClickListener() {
            @Override
            public void leftBtnClick() {
                onBackPressed();
            }

            @Override
            public void rightBtnClick() {
                switch (dialogType) {
                    case TYPE2:
                    case TYPE3:
//                        String appmurl = mWriteResultData.getAppmurl();
//                        if (!TextUtils.isEmpty(appmurl)) {
//                            WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl);
//                        }
                        Intent it = new Intent(mContext, SelectNoteActivity.class);
                        it.putExtra("cateid", mWriteResultData.get_id());
                        it.putExtra("userid", "");
                        it.putExtra("hosid", "");
                        it.putExtra("hosname", "");
                        it.putExtra("docname", "");
                        it.putExtra("fee", "");
                        it.putExtra("taoid", "");
                        it.putExtra("server_id", "");
                        startActivity(it);
                        onBackPressed();
                        break;
                }
            }
        });
    }
}
