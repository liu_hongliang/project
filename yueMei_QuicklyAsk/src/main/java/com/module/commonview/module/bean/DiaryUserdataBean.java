package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryUserdataBean implements Parcelable{
    private String name;            //昵称
    private String id;              //楼主id
    private String avatar;          //头像
    private String talent;          //标签
    private String lable;           //时间
    private String obj_id;          //关注对象id
    private String obj_type;        //obj_type
    private String url;             //obj_type
    private String is_following;    //是否关注，0未关注，1已关注，2互相关注
    private String group_id;
    private String is_rongyun;
    private String hos_userid;
    private HashMap<String,String> event_params;

    protected DiaryUserdataBean(Parcel in) {
        name = in.readString();
        id = in.readString();
        avatar = in.readString();
        talent = in.readString();
        lable = in.readString();
        obj_id = in.readString();
        obj_type = in.readString();
        url = in.readString();
        is_following = in.readString();
        group_id = in.readString();
        is_rongyun = in.readString();
        hos_userid = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(avatar);
        dest.writeString(talent);
        dest.writeString(lable);
        dest.writeString(obj_id);
        dest.writeString(obj_type);
        dest.writeString(url);
        dest.writeString(is_following);
        dest.writeString(group_id);
        dest.writeString(is_rongyun);
        dest.writeString(hos_userid);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryUserdataBean> CREATOR = new Creator<DiaryUserdataBean>() {
        @Override
        public DiaryUserdataBean createFromParcel(Parcel in) {
            return new DiaryUserdataBean(in);
        }

        @Override
        public DiaryUserdataBean[] newArray(int size) {
            return new DiaryUserdataBean[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTalent() {
        return talent;
    }

    public void setTalent(String talent) {
        this.talent = talent;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public String getObj_id() {
        return obj_id;
    }

    public void setObj_id(String obj_id) {
        this.obj_id = obj_id;
    }

    public String getObj_type() {
        return obj_type;
    }

    public void setObj_type(String obj_type) {
        this.obj_type = obj_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIs_following() {
        return is_following;
    }

    public void setIs_following(String is_following) {
        this.is_following = is_following;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
