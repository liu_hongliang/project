package com.module.my.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.module.commonview.view.CommonTopBar;
import com.module.my.controller.adapter.ViewPagerAdapter;
import com.quicklyask.activity.R;

import org.kymjs.aframe.ui.BindView;

import java.util.ArrayList;

/**
 * 我的订单
 *
 * @author Rubin
 */
public class MyOrdersActivity extends FragmentActivity {

    private final String TAG = "MyOrdersActivity";

    @BindView(id = R.id.wan_beautifu_linearlayout4, click = true)
    private LinearLayout contentWeb;

    private MyOrdersActivity mContex;

    private int type;

    private String[] mTitles = new String[]{"全部", "待支付", "待消费", "已使用", "退款"};
    private ArrayList<OrderVpFragment> mVpFragments =new ArrayList<>();
    private TabLayout mOrderTablayout;

    private ViewPager mOrderViewPager;
    private CommonTopBar mTop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acty_my_order);
        mContex = MyOrdersActivity.this;

        Intent it = getIntent();
        type = it.getIntExtra("item",0);
        initFragments();
        initView();
        initEvent();



    }
    private void initFragments() {
        for (int i = 0; i < mTitles.length; i++) {
            mVpFragments.add(OrderVpFragment.newInstance(i+""));

        }
    }
    private void initView() {
        mTop = findViewById(R.id.my_order_top);
        mOrderTablayout = findViewById(R.id.order_tablelayout);
        mOrderViewPager = findViewById(R.id.order_viewpager);
        mTop.setCenterText("我的订单");
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.setTitles(mTitles);
        viewPagerAdapter.setViewPagerFragments(mVpFragments);
        mOrderViewPager.setAdapter(viewPagerAdapter);
        mOrderTablayout.setupWithViewPager(mOrderViewPager);
        if (type != 0){
            mOrderViewPager.setCurrentItem(type);
        }

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void initEvent() {
        mOrderTablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



}
