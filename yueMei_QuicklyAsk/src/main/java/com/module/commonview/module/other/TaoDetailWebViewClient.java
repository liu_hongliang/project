package com.module.commonview.module.other;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.InstructionWebActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.fragment.CommentFragment;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;

import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;

/**
 * Created by 裴成浩 on 2018/1/10.
 */

public class TaoDetailWebViewClient implements BaseWebViewClientCallback {

    private Intent intent;
    private TaoDetailActivity mActivity;
    private String phone400;
    private String mTaoId;
    private String TAG = "TaoDetailWebViewClient";

    public TaoDetailWebViewClient(TaoDetailActivity activity) {
        this.mActivity = activity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }

    @SuppressLint("MissingPermission")
    private void showWebDetail(String urlStr) throws Exception {
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 医生详情页
                String id1 = obj.getString("id");
                intent.putExtra("docId", id1);
                intent.putExtra("docName", "");
                intent.putExtra("partId", "");
                intent.setClass(mActivity, DoctorDetailsActivity592.class);
                mActivity.startActivity(intent);
                break;

            case "6":// 问答详情

                String link6 = obj.getString("link");
                String qid6 = obj.getString("id");

                intent.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER + link6);
                intent.putExtra("qid", qid6);
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                mActivity.startActivity(intent);

                break;

            case "521":   // 咨询列表
//                mActivity.kefuChat();
                break;
            case "535": //打电话
                Log.e(TAG, "phone400 == " + phone400);
                ViewInject.toast("正在拨打中·····");
                Intent it535 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone400));
                try {
                    mActivity.startActivity(it535);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "e == " + e.toString());
                }
                break;

            case "541":   // 预约流程
                intent.putExtra("type", "6");
                intent.setClass(mActivity, InstructionWebActivity.class);
                mActivity.startActivity(intent);
                break;
            case "5414": //售前咨询
//                mActivity.kefuChat();
                break;
            case "6550":
                CommentFragment commentFragment = new CommentFragment();
                FragmentManager fm = mActivity.getSupportFragmentManager();
                Bundle bundle = new Bundle();
                String sort = obj.getString("sort");
                String flag = obj.getString("flag");
                bundle.putString("url",FinalConstant.TAO_DELY);
                bundle.putString("sort",sort);
                bundle.putString("flag",flag);
                bundle.putString("tao_id",mTaoId);
                commentFragment.setArguments(bundle);
                FragmentTransaction transaction = fm.beginTransaction();
                transaction.replace(R.id.sku_container, commentFragment);
                transaction.commit();

                break;

        }
    }

    public void setPhone400(String phone400) {
        this.phone400 = phone400;
    }
    public void setTaoId(String taoid){
        this.mTaoId=taoid;
    }
}
