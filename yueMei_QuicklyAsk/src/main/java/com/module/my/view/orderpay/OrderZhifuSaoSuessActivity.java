package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * 扫描 支付成功页
 * 
 * @author dwb
 * 
 */
public class OrderZhifuSaoSuessActivity extends BaseActivity {

	private final String TAG = "OrderZhiFuStatus1Activity";

	@BindView(id = R.id.order_phone_test_back, click = true)
	private RelativeLayout back;// 返回

	@BindView(id = R.id.zhifu_suess_orderid_tv)
	private TextView orderIdTv;// 订单号
	@BindView(id = R.id.order_sao_time)
	private TextView orderTimeTv;// 支付时间
	@BindView(id = R.id.order_sao_price)
	private TextView orderPriceTv;// 支付金额

	private Context mContex;

	private String order_id;
	private String time_str;
	private String price;

	@BindView(id = R.id.sumbit_order_bt, click = true)
	private Button finishBt;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_zhifu_sao_sufess);
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = OrderZhifuSaoSuessActivity.this;

		Intent it = getIntent();
		order_id = it.getStringExtra("order_id");
		price = it.getStringExtra("price");

		StatService.onEvent(OrderZhifuSaoSuessActivity.this, "002", "支付成功", 1);

		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy年MM月dd日  HH:mm:ss");
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		time_str = formatter.format(curDate);

		orderIdTv.setText(order_id);
		orderPriceTv.setText("￥" + price);
		orderTimeTv.setText(time_str);

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						OrderZhifuSaoSuessActivity.this.finish();
					}
				});
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("NewApi")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.order_phone_test_back:
			if (Utils.isFastDoubleClick()) {
				return;
			}
			onBackPressed();
			break;
		case R.id.sumbit_order_bt:
			finish();
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}