package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoDataS;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 裴成浩 on 2018/3/2.
 */

public class LoadProjectDataApi implements BaseCallBackApi {
    private String TAG = "LoadProjectDataApi";
    private HashMap<String, Object> mHashMap;  //传值容器

    public LoadProjectDataApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.BOARD, "boardtao", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData == " + mData.toString());
                try {
                    if ("1".equals(mData.code)) {
                        HomeTaoDataS homeTaoDataS = JSONUtil.TransformSingleBean(mData.data, HomeTaoDataS.class);
                        listener.onSuccess(homeTaoDataS);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
