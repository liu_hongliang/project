package com.module.other.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.module.other.module.bean.MakeNewNoteData;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dwb on 16/3/22.
 */
public class NoteTipsAdapter extends BaseAdapter {

    private final String TAG = "HotCityAdapter";

    private List<MakeNewNoteData> mGroupData = new ArrayList<MakeNewNoteData>();
    private Context mContext;
    private LayoutInflater inflater;
    private MakeNewNoteData groupData;
    ViewHolder viewHolder;
    private boolean isChice[];

    public NoteTipsAdapter(Context mContext, List<MakeNewNoteData> mGroupData) {
        this.mContext = mContext;
        this.mGroupData = mGroupData;
        inflater = LayoutInflater.from(mContext);

        isChice=new boolean[mGroupData.size()];
        for (int i = 0; i <mGroupData.size(); i++) {
            isChice[i]=false;
        }
    }

    static class ViewHolder {
        public TextView groupNameTV;
    }

    @Override
    public int getCount() {
        return mGroupData.size();
    }

    @Override
    public Object getItem(int position) {
        return mGroupData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_pop_home_xiala, null);
            viewHolder = new ViewHolder();
            viewHolder.groupNameTV = convertView
                    .findViewById(R.id.home_pop_part_name_tv);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        groupData = mGroupData.get(position);

        viewHolder.groupNameTV.setText(groupData.getTitle());

        if(isChice[position]==true){
            viewHolder.groupNameTV.setBackgroundResource(R.drawable.shape_bian_tuoyuan_ff5c77);
            viewHolder.groupNameTV.setTextColor(Color.parseColor("#ff5c77"));
        }else {
            viewHolder.groupNameTV.setBackgroundResource(R.drawable.shape_tuiyuan_cdcdcd);
            viewHolder.groupNameTV.setTextColor(Color.parseColor("#666666"));
        }

        return convertView;
    }

    public void chiceState(int post)
    {
        isChice[post]= isChice[post] != true;
        this.notifyDataSetChanged();
    }

    public void add(List<MakeNewNoteData> infos) {
        mGroupData.addAll(infos);
    }
}

