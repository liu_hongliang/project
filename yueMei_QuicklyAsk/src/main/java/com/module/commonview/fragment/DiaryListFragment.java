package com.module.commonview.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.DiaryListListApi;
import com.module.commonview.activity.DiaryPhotoBrowsingActivity;
import com.module.commonview.adapter.DiaryRecyclerAdapter;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.DiaryListListData;
import com.module.commonview.module.bean.DiaryRadioBean;
import com.module.commonview.module.bean.DiaryUserdataBean;
import com.module.commonview.view.DiaryFlowLayout;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.my.controller.activity.WriteNoteActivity;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 日记本列表
 * Created by 裴成浩 on 2018/6/14.
 */
public class DiaryListFragment extends YMBaseFragment {

    @BindView(R.id.adiary_list_adiary_sorting)
    TextView mAdiarySorting;                         //日记排序按钮
    @BindView(R.id.adiary_list_adiary_falg_container)
    FlowLayout mAdiaryFalgContainer;              //日记标志容器
    @BindView(R.id.diary_list_recycler)
    RecyclerView mDiaryRecycler;                    //日记列表
    @BindView(R.id.diary_list_recycler_more)
    LinearLayout mDiaryRecyclerMoew;                    //日记列表查看更多

    private String TAG = "DiaryListFragment";
    private String mDiaryId;                           //当前查看帖子的id

    private DiaryListListApi mDiaryListListApi;         //日记本列表数据请求类

    public DiaryRecyclerAdapter mDiaryRecyclerAdapter;     //日记本列表适配器

    private DiaryListData mData;                        //页面总数据
    private DiaryUserdataBean mUserdata;                //楼主信息

    private String mSort = "1";                     //默认倒序查询
    private int mDiaryPage = 1;                     //日记列表页面
    public DiaryFlowLayout mDiaryFlowLayout;
    private DiaryListMyFragment diaryListMyFragment;
    public final int TO_WRITE_A_PAGE = 2;                  //再写一篇日记回调


    public static DiaryListFragment newInstance(DiaryListData data, String id) {
        DiaryListFragment fragment = new DiaryListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("id", id);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = getArguments().getParcelable("data");
        mDiaryId = getArguments().getString("id");
        mUserdata = mData.getUserdata();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diary_list;
    }

    @Override
    protected void initView(View view) {

    }

    @Override
    protected void initData(View view) {
        mDiaryListListApi = new DiaryListListApi();

        initDiaryData();                                                                    //初始化日记本数据
        loadDiaryListData(0, "0");                                              //加载日记本列表数据
    }

    /**
     * 初始化日记本日记
     */
    @SuppressLint("SetTextI18n")
    private void initDiaryData() {

        //浏览自己的
        if (isSeeOneself()) {
            diaryListMyFragment = DiaryListMyFragment.newInstance(mData, mDiaryId);
            setActivityFragment(R.id.diary_data_container, diaryListMyFragment);

            diaryListMyFragment.setOnWritePageCallBackListener(new DiaryListMyFragment.OnWritePageCallBackListener() {
                @Override
                public void onWritePageClick(View v) {

                    writeAPage();
                }
            });
        } else {        //浏览其他人日记时
            setActivityFragment(R.id.diary_data_container, DiaryListOtherFragment.newInstance(mData, mDiaryId));
        }

        if (mData.getRadio().size() <= 1) {
            mAdiaryFalgContainer.setVisibility(View.GONE);
        } else {
            mAdiaryFalgContainer.setVisibility(View.VISIBLE);
        }

        //加载日记标签
        mDiaryFlowLayout = new DiaryFlowLayout(mContext, mAdiaryFalgContainer, mData.getRadio());

        //标签回调
        mDiaryFlowLayout.setOnDiaryFlowCallBackListener(new DiaryFlowLayout.DiaryFlowCallBackListener() {
            @Override
            public void onSelectedClickView(int pos) {
                DiaryRadioBean diaryRadioBean = mData.getRadio().get(pos);
                diaryRadioBean.setSelected(true);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_LIST_TAG_CLICK, diaryRadioBean.getRadio_id()), diaryRadioBean.getEvent_params(), new ActivityTypeData("45"));
                mDiaryPage = 1;
                loadDiaryListData(1, "0");
            }

            @Override
            public void onUncheckClickView(int pos) {
                mData.getRadio().get(pos).setSelected(false);
            }
        });
    }

    public void writeAPage() {
        Log.e(TAG, "mData.getId() === " + mData.getId());
        Log.e(TAG, "mDiaryId === " + mDiaryId);
        Intent intent = new Intent(mContext, WriteNoteActivity.class);
        intent.putExtra("cateid", "");
        intent.putExtra("userid", "");
        intent.putExtra("hosid", "");
        intent.putExtra("hosname", "");
        intent.putExtra("docname", "");
        intent.putExtra("fee", "");
        intent.putExtra("taoid", "");
        intent.putExtra("server_id", "");
        intent.putExtra("sharetime", mData.getSharetime());                   //手术时间
        intent.putExtra("type", "3");
        intent.putExtra("noteid", mData.getId());
        intent.putExtra("notetitle", "");
        intent.putExtra("addtype", "2");
        intent.putExtra("beforemaxday", mData.getBefore_max_day());
        intent.putExtra("consumer_certificate", "0");
        intent.putExtra("write_page", true);
        startActivityForResult(intent, TO_WRITE_A_PAGE);
    }

    /**
     * 加载日记本列表数据
     *
     * @param type
     * @param post_id :最后一条的id,第一次加载为0
     */
    private void loadDiaryListData(final int type, String post_id) {
        mDiaryListListApi.addData("id", mDiaryId);
        mDiaryListListApi.addData("tag_id", mDiaryFlowLayout.getSelected());           //筛选id
        mDiaryListListApi.addData("sort", mSort);                                   //正序是1，倒序是0
        mDiaryListListApi.addData("page", mDiaryPage + "");                         //日记列表页面
        mDiaryListListApi.addData("post_id", post_id);                               //最后一条的id
        Log.e(TAG, "id === " + mDiaryId);
        Log.e(TAG, "tag_id === " + mDiaryFlowLayout.getSelected());
        Log.e(TAG, "sort === " + mSort);
        Log.e(TAG, "page === " + mDiaryPage);
        Log.e(TAG, "mDiaryPage === " + mDiaryPage);
        Log.e(TAG, "post_id === " + post_id);
        mDiaryListListApi.getCallBack(mContext, mDiaryListListApi.getHashMap(), new BaseCallBackListener<List<DiaryListListData>>() {
            @Override
            public void onSuccess(List<DiaryListListData> diaryListListDatas) {
                mDiaryPage++;
                initDiaryList(type, diaryListListDatas);                       //初始化日记列表
            }
        });
    }

    /**
     * 初始化日记列表
     *
     * @param diaryListListDatas:日记列表数据
     */
    private void initDiaryList(int type, final List<DiaryListListData> diaryListListDatas) {
        switch (type) {
            case 0:    //第一次加载
                ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                scrollLinearLayoutManager.setScrollEnable(false);
                mDiaryRecycler.setLayoutManager(scrollLinearLayoutManager);
                mDiaryRecyclerAdapter = new DiaryRecyclerAdapter(mContext, diaryListListDatas);
                mDiaryRecycler.setAdapter(mDiaryRecyclerAdapter);

                if (diaryListListDatas.size() < 8) {
                    mDiaryRecyclerMoew.setVisibility(View.GONE);
                } else {
                    try {
                        if (Integer.parseInt(mData.getRadio().get(0).getShare_num()) == 8) {
                            mDiaryRecyclerMoew.setVisibility(View.GONE);
                        } else {
                            mDiaryRecyclerMoew.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        mDiaryRecyclerMoew.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case 1:    //标签、正序倒序加载
                mDiaryRecyclerAdapter.removeDatas(diaryListListDatas);
                if (diaryListListDatas.size() < 8) {
                    mDiaryRecyclerMoew.setVisibility(View.GONE);
                } else {
                    try {
                        if (Integer.parseInt(mData.getRadio().get(0).getShare_num()) == 8) {
                            mDiaryRecyclerMoew.setVisibility(View.GONE);
                        } else {
                            mDiaryRecyclerMoew.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        mDiaryRecyclerMoew.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case 2:    //加载更多
                if (diaryListListDatas.size() != 0) {
                    if (diaryListListDatas.size() < 8) {
                        mDiaryRecyclerMoew.setVisibility(View.GONE);
                    }
                    mDiaryRecyclerAdapter.addDatas(diaryListListDatas);
                } else {
                    mFunctionManager.showShort("没有更多日记了");
                    mDiaryRecyclerMoew.setVisibility(View.GONE);
                }

                break;
        }

        mDiaryRecyclerAdapter.setOnEventClickListener(new DiaryRecyclerAdapter.OnEventClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                onEventClickListener.onItemClick(v, pos);
            }

            @Override
            public void onItemLikeClick(DiariesReportLikeData data) {
                onEventClickListener.onLikeClick(data);
            }

            @Override
            public void onItemImgClick(View v, int pos, int imgPos) {
                DiaryListListData diaryListListData = mDiaryRecyclerAdapter.getmDatas().get(pos);
                HashMap<String, String> event_params = diaryListListData.getEvent_params();
                if(event_params == null){
                    event_params = new HashMap<>();
                }
                event_params.put("to_page_type","150");
                event_params.put("id",mDiaryId);
                event_params.put("to_page_id",mDiaryId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_CONTENT_IMG_CLICK, (imgPos + 1) + ""), event_params, new ActivityTypeData("45"));

                Intent intent = new Intent(mContext, DiaryPhotoBrowsingActivity.class);
                intent.putExtra("post_id", diaryListListData.getId());
                intent.putExtra("position", imgPos);
                mContext.startActivity(intent);
            }

            @Override
            public void onItemDeleteClick(DiariesDeleteData deleteData) {
                onEventClickListener.onItemDeleteClick(deleteData);
            }
        });

        //日记列表查看更多
        mDiaryRecyclerMoew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<DiaryListListData> diaryListListData = mDiaryRecyclerAdapter.getmDatas();
                loadDiaryListData(2, diaryListListData.get(diaryListListData.size() - 1).getId());
            }
        });

        //日记本排序
        mAdiarySorting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSortPosiAndPour()) {
                    mSort = "1";            //时间正序
                    mAdiarySorting.setText("时间倒序");
                } else {
                    mSort = "0";            //时间正序
                    mAdiarySorting.setText("时间正序");
                }

                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", mDiaryId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_TIME_SORT_CLICK, mSort), hashMap,new ActivityTypeData("45"));
                mDiaryPage = 1;
                loadDiaryListData(1, "0");
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TO_WRITE_A_PAGE:             //在写一页回调
                if (resultCode == 100) {

                    DiaryListListData diaryData = data.getParcelableExtra("data");

                    //重新请求日记列表数据
                    loadDiaryListData(0, "0");
                    //mDiaryRecyclerAdapter.addData(diaryData);

                    //标签刷新
                    if (diaryData.getPic().size() == 1) {
                        mDiaryFlowLayout.addData(diaryData.getPic().get(0).getIs_video());
                    } else {
                        mDiaryFlowLayout.addData("0");
                    }
                }
                break;
        }
    }

    /**
     * 判断是查看的自己日记还是别人的日记
     *
     * @return ：ture:查看自己的，false:查看别人的
     */
    private boolean isSeeOneself() {
        return mUserdata.getId().equals(Utils.getUid());
    }

    /**
     * 判断是正序还是倒序
     *
     * @return ：true：正序，false：倒序
     */
    private boolean isSortPosiAndPour() {
        return "0".equals(mSort);
    }

    public interface OnEventClickListener {
        void onItemClick(View v, int pos);                      //item点击回调

        void onLikeClick(DiariesReportLikeData data);           //点赞接口回调

        void onItemDeleteClick(DiariesDeleteData deleteData);   //删除点击回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
