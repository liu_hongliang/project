/**
 *
 */
package com.module.community.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.view.MyElasticScrollView.OnRefreshListener1;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.net.URLDecoder;

/**
 * 活动首页
 *
 * @author Robin
 */
public class HomeHuodongActivity extends BaseActivity {

    private final String TAG = "HomeHuodongActivity";

    @BindView(id = R.id.wan_beautifu_web_det_scrollview3, click = true)
    private MyElasticScrollView scollwebView;
    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private LinearLayout contentWeb;

    private WebView docDetWeb;

    private HomeHuodongActivity mContex;

    private String url;

    public Handler handler = new Handler();

    public JSONObject obj_http;

    @BindView(id = R.id.huodong_top)
    private CommonTopBar mTop;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private LoadingProgress mDialog;


    @Override
    public void setRootView() {
        setContentView(R.layout.web_huodong);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = HomeHuodongActivity.this;

        mDialog = new LoadingProgress(mContex);
        scollwebView.GetLinearLayout(contentWeb);
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String urlStr) {
                showWebDetail(urlStr);
            }
        });
        initWebview();

        mDialog.startLoading();
        LodUrl(FinalConstant.HUODONG_WEB_HOME);

        mTop.setCenterText("活动页");

        scollwebView.setonRefreshListener(new OnRefreshListener1() {

            @Override
            public void onRefresh() {
                webReload();
            }
        });

    }

    @SuppressLint({"SetJavaScriptEnabled", "InlinedApi"})
    public void initWebview() {
        docDetWeb = new WebView(mContex);
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            docDetWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.getSettings().supportMultipleWindows();
        docDetWeb.getSettings().setNeedInitialFocus(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        contentWeb.addView(docDetWeb);
    }

    protected void OnReceiveData(String str) {
        scollwebView.onRefreshComplete();
    }

    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }

    public void showWebDetail(String urlStr) {
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;
            obj_http = obj;

            if (obj.getString("type").equals("1")) {// 医生详情页
                try {
                    String id = obj.getString("id");
                    String docname = URLDecoder.decode(obj.getString("docname"), "utf-8");

                    Intent it = new Intent();
                    it.setClass(mContex, DoctorDetailsActivity592.class);
                    it.putExtra("docId", id);
                    it.putExtra("docName", docname);
                    it.putExtra("partId", "");
                    startActivity(it);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            if (obj.getString("type").equals("6")) {// 问答详情
                String link = obj.getString("link");
                String qid = obj.getString("_id");

                Intent it = new Intent();
                it.setClass(mContex, DiariesAndPostsActivity.class);
                it.putExtra("url", link);
                it.putExtra("qid", qid);
                startActivity(it);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 加载web
     */
    public void LodUrl(String url) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        mDialog.stopLoading();
    }


    public void onResume() {
        super.onResume();
        StatService.onResume(this);
        MobclickAgent.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        StatService.onPause(this);
        MobclickAgent.onPause(this);
        TCAgent.onPause(this);
    }


}
