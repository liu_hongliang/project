package com.module.home.model.bean;

public class EventParams {

    /**
     * event_name : {"click":"search_tao_to_recommend","exposure":"search_tao_recommend_exposure"}
     * type : 39
     * show_style : 692_oneside
     * id : 184
     * search_hit_id : board_184
     * search_word : 瘦脸针
     * operate_area_id : 0
     */

    private EventNameBean event_name;
    private String type;
    private String show_style;
    private String id;
    private String search_hit_id;
    private String search_word;
    private int operate_area_id;

    public EventNameBean getEvent_name() {
        return event_name;
    }

    public void setEvent_name(EventNameBean event_name) {
        this.event_name = event_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getShow_style() {
        return show_style;
    }

    public void setShow_style(String show_style) {
        this.show_style = show_style;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSearch_hit_id() {
        return search_hit_id;
    }

    public void setSearch_hit_id(String search_hit_id) {
        this.search_hit_id = search_hit_id;
    }

    public String getSearch_word() {
        return search_word;
    }

    public void setSearch_word(String search_word) {
        this.search_word = search_word;
    }

    public int getOperate_area_id() {
        return operate_area_id;
    }

    public void setOperate_area_id(int operate_area_id) {
        this.operate_area_id = operate_area_id;
    }

    public static class EventNameBean {
        /**
         * click : search_tao_to_recommend
         * exposure : search_tao_recommend_exposure
         */

        private String click;
        private String exposure;

        public String getClick() {
            return click;
        }

        public void setClick(String click) {
            this.click = click;
        }

        public String getExposure() {
            return exposure;
        }

        public void setExposure(String exposure) {
            this.exposure = exposure;
        }
    }
}
