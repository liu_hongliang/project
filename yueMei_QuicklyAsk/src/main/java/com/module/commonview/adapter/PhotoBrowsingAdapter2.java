package com.module.commonview.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.module.commonview.fragment.DiariesPhotoFragment;
import com.module.commonview.fragment.DiariesVideoFragment;
import com.module.commonview.module.bean.PhotoBrowsListPic;

import java.util.List;

public class PhotoBrowsingAdapter2 extends FragmentPagerAdapter {

    private List<PhotoBrowsListPic> imageUrls;
    private String mDiaryId;

    public PhotoBrowsingAdapter2(String id, FragmentManager fm, List<PhotoBrowsListPic> imageUrls) {
        super(fm);
        this.mDiaryId = id;
        this.imageUrls = imageUrls;
    }

    @Override
    public Fragment getItem(int position) {
        PhotoBrowsListPic photoBrowsListPic = imageUrls.get(position);
        if ("0".equals(photoBrowsListPic.getIs_video())) {
            return DiariesPhotoFragment.newInstance(photoBrowsListPic);
        } else {
            return DiariesVideoFragment.newInstance(photoBrowsListPic, mDiaryId);
        }

    }

    @Override
    public int getCount() {
        return imageUrls.size();
    }

    public void setImageUrls(int index, List<PhotoBrowsListPic> imageUrls) {
        imageUrls.addAll(index, imageUrls);
        notifyDataSetChanged();
    }

    public List<PhotoBrowsListPic> getImageUrls() {
        return imageUrls;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }
}
