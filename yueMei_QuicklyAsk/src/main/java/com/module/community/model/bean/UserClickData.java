package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class UserClickData implements Parcelable {
    private HashMap<String,String> event_params;

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.event_params);
    }

    public UserClickData() {
    }

    protected UserClickData(Parcel in) {
        this.event_params = (HashMap<String, String>) in.readSerializable();
    }

    public static final Parcelable.Creator<UserClickData> CREATOR = new Parcelable.Creator<UserClickData>() {
        @Override
        public UserClickData createFromParcel(Parcel source) {
            return new UserClickData(source);
        }

        @Override
        public UserClickData[] newArray(int size) {
            return new UserClickData[size];
        }
    };
}
