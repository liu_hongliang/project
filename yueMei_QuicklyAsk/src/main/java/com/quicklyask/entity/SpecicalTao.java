/**
 * 
 */
package com.quicklyask.entity;

/**
 * @author lenovo17
 * 
 */
public class SpecicalTao {
	private String _id;
	private String title;
	private String price;
	private String price_x;
	private String order_num;
	private String img;

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the price_x
	 */
	public String getPrice_x() {
		return price_x;
	}

	/**
	 * @param price_x
	 *            the price_x to set
	 */
	public void setPrice_x(String price_x) {
		this.price_x = price_x;
	}

	/**
	 * @return the order_num
	 */
	public String getOrder_num() {
		return order_num;
	}

	/**
	 * @param order_num
	 *            the order_num to set
	 */
	public void setOrder_num(String order_num) {
		this.order_num = order_num;
	}

	/**
	 * @return the img
	 */
	public String getImg() {
		return img;
	}

	/**
	 * @param img
	 *            the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}


}
