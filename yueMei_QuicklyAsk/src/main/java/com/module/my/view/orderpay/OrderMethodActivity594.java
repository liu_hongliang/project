package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alipay.sdk.app.PayTask;
import com.baidu.mobstat.StatService;
import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.SpeltActivity;
import com.module.my.controller.activity.JDzhifuWebActivity;
import com.module.my.controller.activity.UserAgreementWebActivity;
import com.module.my.model.api.InitHBFenqiApi;
import com.module.my.model.api.PayALiPay;
import com.module.my.model.api.PayWeixinApi;
import com.module.my.model.api.PayYinlianApi;
import com.module.my.model.api.ShenHeApi;
import com.module.my.model.bean.ALiPayData;
import com.module.my.model.bean.HBMMFenqi;
import com.module.my.model.bean.HbFenqiItem;
import com.module.my.model.bean.MimoData;
import com.module.my.model.bean.ShenHeData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.activity.interfaces.PayStatusCallBack;
import com.quicklyask.activity.weixin.Constants;
import com.quicklyask.activity.wxapi.WXPayEntryActivity;
import com.quicklyask.entity.PrePayIdData;
import com.quicklyask.entity.Result;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.BaoxianPopWindow;
import com.quicklyask.view.EditExitDialog;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tendcloud.appcpa.TalkingDataAppCpa;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.unionpay.UPPayAssistEx;
import com.unionpay.uppay.PayActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.kymjs.aframe.database.KJDB;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;


/**
 * 支付 选择支付方式
 *
 * @author dwb
 */
public class OrderMethodActivity594 extends YMBaseActivity {

    private final String TAG = "OrderMethodActivity594";
    @BindView(R.id.sumbit_order_bt)
    Button next1;// 确认支付
    @BindView(R.id.order_method_shifu_tv)
    TextView shifuTv;// 实付的价钱

    @BindView(R.id.order_zhifubao_check)
    CheckBox check1;// 支付宝
    @BindView(R.id.order_yinhangka_check)
    CheckBox check3;// 银行卡转账
    @BindView(R.id.order_weixin_check)
    CheckBox check2;// 微信支付
    @BindView(R.id.order_yinlian_check)
    CheckBox check4;// 银联支付
    @BindView(R.id.order_jd_check)
    CheckBox check5;// 京东支付
    @BindView(R.id.order_hbfenqi_check)
    CheckBox check6;// 花呗分期
    @BindView(R.id.order_lebaifenfenqi_check)
    CheckBox check7;// 乐百分分期

    @BindView(R.id.order_paytype_zhifubao1)
    RelativeLayout zhifubaoLy;// 支付宝1
    @BindView(R.id.order_paytype_weixin)
    RelativeLayout weixinLy;// 微信2
    @BindView(R.id.order_paytype_yinhang)
    RelativeLayout yinhangLy;// 银行3
    @BindView(R.id.order_paytype_yinlian)
    RelativeLayout yinlianLy;// 银联支付4
    @BindView(R.id.order_paytype_jd)
    RelativeLayout jdLy;// 京东支付
    @BindView(R.id.order_paytype_hbfneqi)
    RelativeLayout hbpayLy;// 花呗
    @BindView(R.id.oder_paytype_hbfenqi_content)
    LinearLayout hbpayLLy;// 花呗
    @BindView(R.id.order_hbfenqi_tips_rly)
    RelativeLayout hbTipsLy;
    @BindView(R.id.hbfenqi_price_tv)
    TextView hbfenqiTv;
    @BindView(R.id.hbfenqi_sxf_tv)
    TextView hbsxffenqiTv;
    @BindView(R.id.order_paytype_fneqi)
    RelativeLayout lefenqiLy;// 乐百分
    @BindView(R.id.order_lebaifenfenqi_tips_rly)
    RelativeLayout lefenTipsLy;
    @BindView(R.id.lebaifenfenqi_price_tv)
    TextView lefenqiTv;
    //分期
    @BindView(R.id.oder_paytype_fenqi_content)
    LinearLayout fenqiContentLy;
    @BindView(R.id.paytype_fenqi_tips_iv)
    ImageView fenqiTipsIv;
    @BindView(R.id.paytype_hbfenqi_tips_iv)
    ImageView hbfenqiTipsIv;
    @BindView(R.id.oder_method_title_ly)
    LinearLayout xiadanLy;
    @BindView(R.id.yuemei_yinsi_ly)
    LinearLayout yuemeiYinsiLy;
    @BindView(R.id.order_time_all_ly)
    LinearLayout allLy;

    private String pay_id = "6";// 支付类型 默认支付宝支付

    private static final int SDK_PAY_FLAG = 1;
    private static final int SDK_CHECK_FLAG = 2;

    // 微信
    PayReq req;
    final IWXAPI msgApi = WXAPIFactory.createWXAPI(this, null);
    StringBuffer sb;

    private List<HbFenqiItem> hbdata;
    private List<HbFenqiItem> mimoNandata;
    private List<HbFenqiItem> mimoNvdata;

    private String server_id;
    private String order_id;
    private String price;
    private String finalPrice;
    private String order_time;

    private String type;
    private String sku_type;
    private String is_repayment;//是否可以分期 1可以
    private String is_repayment_mimo;//是否可以分期 1可以

    private static final int REQUEST_JD_CODE = 888888;
    private static final int REQUEST_FENQI_CODE = 999999;


    private double fenqi6;
    private double fenqi12;
    private String fenqiPaytype = "0";

    private String[] hbfenqiS;
    private String[] hbSxffenqiS;
    private String hbfenqiPaytype = "0";

    private String[] mimofenqiS;
    private String[] mimofSxenqiS;

    private BaoxianPopWindow baoxianPop;
    private HbPopupWindows hbPop;
    private boolean ifHbpop = true;

    private LeBaiFenPopupWindows lebaifenPop;
    private boolean iflebaifenpop = true;

    // 城市缓存
    private KJDB kjdb;
    private List<MimoData> mimolistCache = new ArrayList<>();
    private String weikuan;
    private String number;
    private String isGroup; //是否是拼团，1是拼团。0不是拼团
    private String groupId;
    private HashMap<String, Object> urlMap = new HashMap<>();
    private Bundle successBundle;


    @Override
    protected int getLayoutId() {
        return R.layout.order_method_594;
    }

    @Override
    protected void initView() {
        kjdb = KJDB.create(mContext, "yuemei_mimo");

        req = new PayReq();
        sb = new StringBuffer();
        msgApi.registerApp(Constants.APP_ID);

        Bundle data = getIntent().getBundleExtra("data");
        price = data.getString("price");
        server_id = data.getString("server_id");
        order_id = data.getString("order_id");
        type = data.getString("type");
        order_time = data.getString("order_time");
        sku_type = data.getString("sku_type");
        is_repayment = data.getString("is_repayment");
        is_repayment_mimo = data.getString("is_repayment_mimo");
        weikuan = data.getString("weikuan");
        number = data.getString("number");
        groupId = data.getString("group_id");
        isGroup = data.getString("is_group");

        Log.e(TAG, "groupId == " + groupId);

        finalPrice = price;
        shifuTv.setText("￥" + price);

        successBundle = new Bundle();
        successBundle.putString("payType", "2");
        successBundle.putString("pay_type", "1");
        successBundle.putString("server_id", server_id);
        successBundle.putString("order_id", order_id);
        successBundle.putString("sku_type", sku_type);
        successBundle.putString("is_repayment", is_repayment);
        successBundle.putString("is_repayment_mimo", is_repayment_mimo);
        successBundle.putString("price", price);

        initShenHe();
        initHbfenqi();

        if ("1".equals(type)) {
            xiadanLy.setVisibility(View.VISIBLE);
        } else {
            xiadanLy.setVisibility(View.GONE);
        }

        if (null != is_repayment) {
            if ("1".equals(is_repayment)) {

                if ("yingyongbao".equals(FinalConstant1.YUEMEI_MARKET)) {
                    fenqiContentLy.setVisibility(View.GONE);
                } else {
                    fenqiContentLy.setVisibility(View.VISIBLE);
                }

                fenqi6 = Float.parseFloat(price) / 6;
                fenqi12 = Float.parseFloat(price) / 12;

                lebaifenPop = new LeBaiFenPopupWindows(mContext);

            } else {
                fenqiContentLy.setVisibility(View.GONE);
            }
        } else {
            fenqiContentLy.setVisibility(View.GONE);
        }
        check1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {//支付宝
                if (arg1) {
                    pay_id = "6";
                    check2.setChecked(false);
                    check3.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(false);
                    check7.setChecked(false);

                    shifuTv.setText("￥" + price);
                }
            }
        });

        check2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {//微信
                if (arg1) {
                    pay_id = "2";
                    check1.setChecked(false);
                    check3.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(false);
                    check7.setChecked(false);

                    shifuTv.setText("￥" + price);
                }
            }
        });

        check3.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                if (arg1) {
                    pay_id = "3";
                    check1.setChecked(false);
                    check2.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(false);
                    check7.setChecked(false);

                    shifuTv.setText("￥" + price);
                }
            }
        });

        check4.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                if (arg1) {
                    pay_id = "4";
                    check1.setChecked(false);
                    check2.setChecked(false);
                    check3.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(false);
                    check7.setChecked(false);

                    shifuTv.setText("￥" + price);
                }
            }
        });

        check5.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {//京东支付
                if (arg1) {
                    pay_id = "7";
                    check1.setChecked(false);
                    check2.setChecked(false);
                    check3.setChecked(false);
                    check4.setChecked(false);
                    check6.setChecked(false);
                    check7.setChecked(false);

                    shifuTv.setText("￥" + price);
                }
            }
        });


        check6.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {//花呗分期
                if (arg1) {
                    pay_id = "15";
                    check1.setChecked(false);
                    check2.setChecked(false);
                    check3.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check7.setChecked(false);

                    if (hbfenqiPaytype.equals("3")) {
                        shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + hbfenqiS[0] + "</font>" + "<small><font color=\"#333333\">" + "x3期" + "</font></small>"));

                    } else if (hbfenqiPaytype.equals("6")) {
                        shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + hbfenqiS[1] + "</font>" + "<small><font color=\"#333333\">" + "x3期" + "</font></small>"));

                    } else if (hbfenqiPaytype.equals("12")) {
                        shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + hbfenqiS[2] + "</font>" + "<small><font color=\"#333333\">" + "x3期" + "</font></small>"));

                    }

                }
            }
        });

        check7.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {//乐百分分期
                if (arg1) {
                    pay_id = "10";
                    check1.setChecked(false);
                    check2.setChecked(false);
                    check3.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(false);


                    if (fenqiPaytype.equals("6")) {
                        shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + formatDouble4(fenqi6) + "</font>" + "<small><font color=\"#333333\">" + "x6期" + "</font></small>"));

                    } else if (fenqiPaytype.equals("12")) {
                        shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + formatDouble4(fenqi12) + "</font>" + "<small><font color=\"#333333\">" + "x6期" + "</font></small>"));

                    }

                }
            }
        });

        setMultiOnClickListener(next1, zhifubaoLy, weixinLy, yinhangLy, yinlianLy, jdLy, hbpayLy, hbpayLLy, hbTipsLy, lefenqiLy, lefenTipsLy, fenqiTipsIv, hbfenqiTipsIv, yuemeiYinsiLy);

        WXPayEntryActivity.setPayStatusCallBack(new PayStatusCallBack() {
            @Override
            public void onPayStatusCallBack(BaseResp resp) {
                switch (resp.errCode){
                    case 0:  //支付成功
                        Log.e(TAG,"pay------success");
                        finish();
                        mFunctionManager.goToActivity(OrderZhiFuStatus1Activity.class, successBundle);
                        break; //配置信息错误
                    case -1:
                        break;
                    case -2:  //支付取消
                        Log.e(TAG,"pay----cancle");
                        mFunctionManager.showShort("支付取消");
                        break;
                }
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.sumbit_order_bt:

                if (number != null && Integer.parseInt(number) > 1) {
                    showDialogExitEdit3();
                } else {
                    sumbitOrder();
                }
                break;
            case R.id.order_paytype_zhifubao1:
                check1.setChecked(true);
                check2.setChecked(false);
                check3.setChecked(false);
                check4.setChecked(false);
                check5.setChecked(false);
                check6.setChecked(false);
                check7.setChecked(false);

                hbTipsLy.setVisibility(View.GONE);
                lefenTipsLy.setVisibility(View.GONE);
                break;
            case R.id.order_paytype_yinhang:
                check1.setChecked(false);
                check3.setChecked(true);
                check2.setChecked(false);
                check4.setChecked(false);
                check5.setChecked(false);
                check6.setChecked(false);
                check7.setChecked(false);

                hbTipsLy.setVisibility(View.GONE);
                lefenTipsLy.setVisibility(View.GONE);
                break;
            case R.id.order_paytype_weixin:
                check1.setChecked(false);
                check3.setChecked(false);
                check2.setChecked(true);
                check4.setChecked(false);
                check5.setChecked(false);
                check6.setChecked(false);
                check7.setChecked(false);

                hbTipsLy.setVisibility(View.GONE);
                lefenTipsLy.setVisibility(View.GONE);
                break;
            case R.id.order_paytype_yinlian:
                check1.setChecked(false);
                check3.setChecked(false);
                check2.setChecked(false);
                check4.setChecked(true);
                check5.setChecked(false);
                check6.setChecked(false);
                check7.setChecked(false);

                hbTipsLy.setVisibility(View.GONE);
                lefenTipsLy.setVisibility(View.GONE);
                break;
            case R.id.order_paytype_jd:
                check1.setChecked(false);
                check3.setChecked(false);
                check2.setChecked(false);
                check4.setChecked(false);
                check5.setChecked(true);
                check6.setChecked(false);
                check7.setChecked(false);

                hbTipsLy.setVisibility(View.GONE);
                lefenTipsLy.setVisibility(View.GONE);
                break;
            case R.id.order_paytype_hbfneqi:

                if (ifHbpop) {
                    if (null != hbPop) {
                        hbPop.showAtLocation(allLy, Gravity.BOTTOM, 0, 0);
                    }
                } else {
                    hbTipsLy.setVisibility(View.VISIBLE);
                    lefenTipsLy.setVisibility(View.GONE);

                    check1.setChecked(false);
                    check3.setChecked(false);
                    check2.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(true);
                    check7.setChecked(false);
                }
                break;
            case R.id.order_hbfenqi_tips_rly://花呗分期修改
                hbPop.showAtLocation(allLy, Gravity.BOTTOM, 0, 0);
                break;
            case R.id.order_paytype_fneqi:

                if (iflebaifenpop) {
                    if (null != lebaifenPop) {
                        lebaifenPop.showAtLocation(allLy, Gravity.BOTTOM, 0, 0);
                    }
                } else {
                    lefenTipsLy.setVisibility(View.VISIBLE);
                    hbTipsLy.setVisibility(View.GONE);

                    check1.setChecked(false);
                    check3.setChecked(false);
                    check2.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(false);
                    check7.setChecked(true);
                }
                break;
            case R.id.order_lebaifenfenqi_tips_rly:
                lebaifenPop.showAtLocation(allLy, Gravity.BOTTOM, 0, 0);
                break;
            case R.id.yuemei_yinsi_ly://悦美隐私政策
                Intent it = new Intent();
                it.setClass(OrderMethodActivity594.this, UserAgreementWebActivity.class);
                startActivity(it);
                break;
            case R.id.paytype_fenqi_tips_iv:
                String urlss = FinalConstant.LEBAIFEN;
                urlMap.put("type", "3");
                baoxianPop = new BaoxianPopWindow(mContext, urlss, urlMap);
                baoxianPop.showAtLocation(findViewById(R.id.order_time_all_ly), Gravity.CENTER, 0, 0);
                break;
            case R.id.paytype_hbfenqi_tips_iv:
                String urlss1 = FinalConstant.LEBAIFEN;
                urlMap.put("type", "2");
                baoxianPop = new BaoxianPopWindow(mContext, urlss1, urlMap);
                baoxianPop.showAtLocation(findViewById(R.id.order_time_all_ly), Gravity.CENTER, 0, 0);
                break;

        }
    }


    public static String formatDouble4(double d) {
        DecimalFormat df = new DecimalFormat("##0.00");

        return df.format(d);
    }

    private void initHbfenqi() {
        new InitHBFenqiApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {

                    try {
                        HBMMFenqi hbmmFenqi = JSONUtil.TransformSingleBean(serverData.data, HBMMFenqi.class);
                        hbdata = hbmmFenqi.getHb();
                        int hbsize = hbmmFenqi.getHb().size();
                        hbfenqiS = new String[hbsize];
                        hbSxffenqiS = new String[hbsize];

                        for (int i = 0; i < hbsize; i++) {
                            float fee = Float.parseFloat(hbdata.get(i).getHb_fq_percent());
                            BigDecimal totalFeeInDecimal = BigDecimal.valueOf((int) (Float.parseFloat(price) * 100)).multiply(BigDecimal.valueOf(fee));
                            long totalFeeInLong = totalFeeInDecimal.setScale(0, BigDecimal.ROUND_HALF_EVEN).longValue();
                            BigDecimal eachFee = BigDecimal.valueOf(totalFeeInLong).divide(new BigDecimal(hbdata.get(i).getHb_fq_num()), BigDecimal.ROUND_DOWN);
                            long qif = eachFee.setScale(0, BigDecimal.ROUND_DOWN).longValue();
                            BigDecimal yuanf = BigDecimal.valueOf(qif, 2);
                            BigDecimal eachPrin = BigDecimal.valueOf((int) (Float.parseFloat(price) * 100)).divide(new BigDecimal(hbdata.get(i).getHb_fq_num()), BigDecimal.ROUND_DOWN);
                            BigDecimal prinAndFee = eachFee.add(eachPrin);
                            long qi = prinAndFee.setScale(0, BigDecimal.ROUND_DOWN).longValue();
                            BigDecimal yuan = BigDecimal.valueOf(qi, 2);

                            hbfenqiS[i] = yuan + "";
                            hbSxffenqiS[i] = yuanf + "";
                        }

                        hbPop = new HbPopupWindows(mContext, allLy);

                        mimoNvdata = hbmmFenqi.getMomo().getGirl();
                        int mimosize = mimoNvdata.size();
                        mimoNandata = hbmmFenqi.getMomo().getBoy();
                        int nanMmsize = mimoNandata.size();

                        mimofenqiS = new String[mimosize + nanMmsize];
                        mimofSxenqiS = new String[mimosize + nanMmsize];

                        for (int i = 0; i < mimosize; i++) {

                            float fee = Float.parseFloat(mimoNvdata.get(i).getHb_fq_percent());
                            BigDecimal totalFeeInDecimal = BigDecimal.valueOf((int) (Float.parseFloat(price) * 100)).multiply(BigDecimal.valueOf(fee));
                            long totalFeeInLong = totalFeeInDecimal.setScale(0, BigDecimal.ROUND_HALF_EVEN).longValue();
                            BigDecimal eachFee = BigDecimal.valueOf(totalFeeInLong).divide(new BigDecimal(mimoNvdata.get(i).getHb_fq_num()), BigDecimal.ROUND_DOWN);
                            long qif = eachFee.setScale(0, BigDecimal.ROUND_DOWN).longValue();
                            BigDecimal yuanf = BigDecimal.valueOf(qif, 2);
                            BigDecimal eachPrin = BigDecimal.valueOf((int) (Float.parseFloat(price) * 100)).divide(new BigDecimal(mimoNvdata.get(i).getHb_fq_num()), BigDecimal.ROUND_DOWN);
                            BigDecimal prinAndFee = eachFee.add(eachPrin);
                            long qi = prinAndFee.setScale(0, BigDecimal.ROUND_DOWN).longValue();
                            BigDecimal yuan = BigDecimal.valueOf(qi, 2);

                            mimofenqiS[i] = yuan + "";
                            mimofSxenqiS[i] = yuanf + "";
                        }

                        for (int i = 0; i < nanMmsize; i++) {

                            float fee = Float.parseFloat(mimoNandata.get(i).getHb_fq_percent());
                            BigDecimal totalFeeInDecimal = BigDecimal.valueOf((int) (Float.parseFloat(price) * 100)).multiply(BigDecimal.valueOf(fee));
                            long totalFeeInLong = totalFeeInDecimal.setScale(0, BigDecimal.ROUND_HALF_EVEN).longValue();
                            BigDecimal eachFee = BigDecimal.valueOf(totalFeeInLong).divide(new BigDecimal(mimoNandata.get(i).getHb_fq_num()), BigDecimal.ROUND_DOWN);
                            long qif = eachFee.setScale(0, BigDecimal.ROUND_DOWN).longValue();
                            BigDecimal yuanf = BigDecimal.valueOf(qif, 2);
                            BigDecimal eachPrin = BigDecimal.valueOf((int) (Float.parseFloat(price) * 100)).divide(new BigDecimal(mimoNandata.get(i).getHb_fq_num()), BigDecimal.ROUND_DOWN);
                            BigDecimal prinAndFee = eachFee.add(eachPrin);
                            long qi = prinAndFee.setScale(0, BigDecimal.ROUND_DOWN).longValue();
                            BigDecimal yuan = BigDecimal.valueOf(qi, 2);

                            mimofenqiS[i + nanMmsize] = yuan + "";
                            mimofSxenqiS[i + nanMmsize] = yuanf + "";
                        }

                        mimolistCache = kjdb.findAll(MimoData.class);
                        if (null != mimolistCache && mimolistCache.size() > 0) {

                            for (int i = 0; i < mimolistCache.size(); i++) {
                                if (mimolistCache.get(i).getOderid().equals(order_id)) {

                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

        });
    }


    void initShenHe() {
        new ShenHeApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ShenHeData>() {
            @Override
            public void onSuccess(ShenHeData sdata) {
                String alipay = sdata.getAlipay();
                if (alipay.equals("1")) {
                    zhifubaoLy.setVisibility(View.GONE);
                } else {
                    zhifubaoLy.setVisibility(View.VISIBLE);
                }
                String hb_alipay = sdata.getHb_alipay();
                if (hb_alipay.equals("1")) {
                    hbpayLLy.setVisibility(View.GONE);
                } else {
                    hbpayLLy.setVisibility(View.VISIBLE);
                }

                String wxpay = sdata.getWxpay();
                if (wxpay.equals("1")) {
                    weixinLy.setVisibility(View.GONE);
                } else {
                    weixinLy.setVisibility(View.VISIBLE);
                }

                String bankpay = sdata.getBankpay();
                if (bankpay.equals("1")) {
                    yinlianLy.setVisibility(View.GONE);
                } else {
                    yinlianLy.setVisibility(View.VISIBLE);
                }
            }

        });
    }

    /**
     * dialog提示
     */
    void showDialogExitEdit3() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dilog_newuser_yizhuce1);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
        titleTv77.setVisibility(View.VISIBLE);
        titleTv77.setText("提示");

        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("您本次购买了多个商品，一旦到院确认并消费一个后，其余的商品将不支持退款，但可以继续消费使用");

        titleTv88.setHeight(Utils.dip2px(mContext, 80));

        LinearLayout llFengexian = editDialog.findViewById(R.id.ll_fengexian);
        llFengexian.setVisibility(View.VISIBLE);

        //确定
        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText("去支付");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                sumbitOrder();
            }
        });

        //取消
        Button cancelBt99 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt99.setVisibility(View.VISIBLE);
        cancelBt99.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    private void sumbitOrder() {
        if (Utils.isFastDoubleClick()) {
            return;
        }
        if (check1.isChecked() || check2.isChecked() || check3.isChecked() || check4.isChecked() || check5.isChecked() || check6.isChecked() || check7.isChecked() || !fenqiPaytype.equals("0")) {

            if (pay_id.equals("6")) {// 支付宝
                payALiPay(order_id, finalPrice, "0");
            } else if (pay_id.equals("7")) {// 京东支付
                Intent it = new Intent();
                it.setClass(mContext, JDzhifuWebActivity.class);
                it.putExtra("order_id", order_id);
                it.putExtra("weikuan", weikuan);
                startActivityForResult(it, REQUEST_JD_CODE);
            } else if (pay_id.equals("2")) {// 微信
                payWeixin(order_id, finalPrice);
            } else if (pay_id.equals("4")) {// 银联支付
                payYinlian(order_id, finalPrice);
            } else if (pay_id.equals("3")) {// 银行转账
                Intent it = new Intent();
                it.setClass(mContext, BankTransferActivity.class);
                it.putExtra("order_id", order_id);
                it.putExtra("price", finalPrice);
                it.putExtra("weikuan", weikuan);
                startActivity(it);
            } else if (pay_id.equals("10")) {// 分期支付
                Intent it = new Intent(mContext, FenQiZhifuWebActivity.class);
                it.putExtra("order_id", order_id);
                it.putExtra("installments_num", fenqiPaytype);
                it.putExtra("weikuan", weikuan);
                startActivityForResult(it, REQUEST_FENQI_CODE);
            } else if (pay_id.equals("15")) {//花呗分期
                payALiPay(order_id, finalPrice, hbfenqiPaytype);
            }

        } else {
            mFunctionManager.showShort("请选择支付方式");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_JD_CODE://京东支付返回
                if (null != data) {
                    String type_code = data.getStringExtra("type_code");

                    if (type_code.equals("1")) {
                        mFunctionManager.showShort("支付成功");

                        if ("0".equals(isGroup)) {
                            mFunctionManager.goToActivity(OrderZhiFuStatus1Activity.class, successBundle);
                        } else if ("1".equals(isGroup)) {
                            Intent intent = new Intent(mContext, SpeltActivity.class);
                            intent.putExtra("group_id", groupId);
                            intent.putExtra("order_id", order_id);
                            intent.putExtra("type", "2");
                            startActivity(intent);
                        }

                        finish();

                        TalkingDataAppCpa.onOrderPaySucc(Utils.getUid(), order_id, (int) (Float.parseFloat(finalPrice) * 100), "CNY", "京东支付");
                    } else if (type_code.equals("2")) {
                        finish();
                    } else if (type_code.equals("0")) {
                        mFunctionManager.showShort("支付失败");

                        Bundle bundle = new Bundle();
                        bundle.putString("server_id", server_id);
                        bundle.putString("order_id", order_id);
                        bundle.putString("price", price);
                        bundle.putString("order_time", order_time);
                        bundle.putString("sku_type", sku_type);
                        bundle.putString("is_repayment", is_repayment);
                        bundle.putString("is_repayment_mimo", is_repayment_mimo);
                        bundle.putString("weikuan", weikuan);
                        bundle.putString("group_id", groupId);
                        bundle.putString("is_group", isGroup);
                        mFunctionManager.goToActivity(OrderZhiFuStatus2Activity.class, bundle);
                        finish();
                    }
                }
                break;
            case REQUEST_FENQI_CODE://分期支付返回
                if (null != data) {
                    String type_code = data.getStringExtra("type_code");

                    if (type_code.equals("1")) {
                        mFunctionManager.showShort("支付成功");

                        if ("0".equals(isGroup)) {
                            mFunctionManager.goToActivity(OrderZhiFuStatus1Activity.class, successBundle);
                        } else if ("1".equals(isGroup)) {
                            Intent intent = new Intent(mContext, SpeltActivity.class);
                            intent.putExtra("group_id", groupId);
                            intent.putExtra("order_id", order_id);
                            intent.putExtra("type", "2");
                            startActivity(intent);
                        }

                        finish();

                        TalkingDataAppCpa.onOrderPaySucc(Utils.getUid(), order_id, (int) (Float.parseFloat(finalPrice) * 100), "CNY", "京东支付");
                    } else if (type_code.equals("2")) {
                        finish();
                    } else if (type_code.equals("0")) {
                        mFunctionManager.showShort("支付失败");

                        Bundle bundle = new Bundle();
                        bundle.putString("server_id", server_id);
                        bundle.putString("order_id", order_id);
                        bundle.putString("price", price);
                        bundle.putString("order_time", order_time);
                        bundle.putString("sku_type", sku_type);
                        bundle.putString("is_repayment", is_repayment);
                        bundle.putString("is_repayment_mimo", is_repayment_mimo);
                        bundle.putString("weikuan", weikuan);
                        bundle.putString("group_id", groupId);
                        bundle.putString("is_group", isGroup);
                        mFunctionManager.goToActivity(OrderZhiFuStatus2Activity.class, bundle);

                        finish();
                    }
                }
                break;

            default:                                                            //银联支付回调
                String str = data.getExtras().getString("pay_result");

                if (str.equalsIgnoreCase("success")) {
                    mFunctionManager.showShort("支付成功");

                    if ("0".equals(isGroup)) {
                        mFunctionManager.goToActivity(OrderZhiFuStatus1Activity.class, successBundle);
                    } else if ("1".equals(isGroup)) {
                        Intent intent = new Intent(mContext, SpeltActivity.class);
                        intent.putExtra("group_id", groupId);
                        intent.putExtra("order_id", order_id);
                        intent.putExtra("type", "2");
                        startActivity(intent);
                    }

                    finish();

                    TalkingDataAppCpa.onOrderPaySucc(Utils.getUid(), order_id, (int) (Float.parseFloat(finalPrice) * 100), "CNY", "银联支付");
                } else if (str.equalsIgnoreCase("fail")) {
                    mFunctionManager.showShort("支付失败");

                    Bundle bundle = new Bundle();
                    bundle.putString("server_id", server_id);
                    bundle.putString("order_id", order_id);
                    bundle.putString("price", price);
                    bundle.putString("order_time", order_time);
                    bundle.putString("sku_type", sku_type);
                    bundle.putString("is_repayment", is_repayment);
                    bundle.putString("is_repayment_mimo", is_repayment_mimo);
                    bundle.putString("weikuan", weikuan);
                    bundle.putString("group_id", groupId);
                    bundle.putString("is_group", isGroup);
                    mFunctionManager.goToActivity(OrderZhiFuStatus2Activity.class, bundle);

                    finish();
                } else if (str.equalsIgnoreCase("cancel")) {

                    mFunctionManager.showShort("用户取消了支付");

                    Bundle bundle = new Bundle();
                    bundle.putString("server_id", server_id);
                    bundle.putString("order_id", order_id);
                    bundle.putString("price", price);
                    bundle.putString("order_time", order_time);
                    bundle.putString("sku_type", sku_type);
                    bundle.putString("is_repayment", is_repayment);
                    bundle.putString("is_repayment_mimo", is_repayment_mimo);
                    bundle.putString("weikuan", weikuan);
                    bundle.putString("group_id", groupId);
                    bundle.putString("is_group", isGroup);
                    mFunctionManager.goToActivity(OrderZhiFuStatus2Activity.class, bundle);

                    finish();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 支付宝支付返回结果
     */
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    Result resultObj = new Result((String) msg.obj);
                    String resultStatus = resultObj.resultStatus;

                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        mFunctionManager.showShort("支付成功");
                        if ("0".equals(isGroup)) {
                            mFunctionManager.goToActivity(OrderZhiFuStatus1Activity.class, successBundle);
                            finish();
                        } else if ("1".equals(isGroup)) {
                            Log.e(TAG, "groupId  == " + groupId);
                            Intent intent = new Intent(OrderMethodActivity594.this, SpeltActivity.class);
                            intent.putExtra("group_id", groupId);
                            intent.putExtra("order_id", order_id);
                            intent.putExtra("type", "2");
                            startActivity(intent);
                            OrderMethodActivity594.this.finish();
                        }

                        TalkingDataAppCpa.onOrderPaySucc(Utils.getUid(), order_id, (int) (Float.parseFloat(finalPrice) * 100), "CNY", "支付宝");
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”
                        // 代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            mFunctionManager.showShort("支付结果确认中");
                        } else {
                            mFunctionManager.showShort("支付失败");
                        }

                        Bundle bundle = new Bundle();
                        bundle.putString("server_id", server_id);
                        bundle.putString("order_id", order_id);
                        bundle.putString("price", price);
                        bundle.putString("order_time", order_time);
                        bundle.putString("sku_type", sku_type);
                        bundle.putString("is_repayment", is_repayment);
                        bundle.putString("is_repayment_mimo", is_repayment_mimo);
                        bundle.putString("weikuan", weikuan);
                        bundle.putString("group_id", groupId);
                        bundle.putString("is_group", isGroup);
                        mFunctionManager.goToActivity(OrderZhiFuStatus2Activity.class, bundle);
                        finish();
                    }
                    break;
                }
                case SDK_CHECK_FLAG: {
                    mFunctionManager.showShort("检查结果为：" + msg.obj);
                    break;
                }
                default:
                    break;
            }
        }
    };

    private void payALiPay(String order_no, String price, String qinum) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("product_name", "悦美");
        maps.put("order_no", order_no);
        maps.put("order_price", price);
        maps.put("installments_num", qinum);
        maps.put("weikuan", weikuan);

        Log.e(TAG, "order_no == " + order_no);
        Log.e(TAG, "price == " + price);
        Log.e(TAG, "qinum == " + qinum);
        Log.e(TAG, "weikuan == " + weikuan);

        new PayALiPay().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {

                    try {
                        ALiPayData alidata = JSONUtil.TransformSingleBean(serverData.data, ALiPayData.class);

                        final String payinfo = alidata.getPayInfo();

                        Runnable payRunnable = new Runnable() {

                            @Override
                            public void run() {
                                // 构造PayTask 对象
                                PayTask alipay = new PayTask(OrderMethodActivity594.this);
                                // 调用支付接口
                                String result = alipay.pay(payinfo, true);

                                Message msg = new Message();
                                msg.what = SDK_PAY_FLAG;
                                msg.obj = result;
                                mHandler.sendMessage(msg);
                            }
                        };

                        Thread payThread = new Thread(payRunnable);
                        payThread.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    mFunctionManager.showShort(serverData.message);
                }
            }

        });
    }

    private void payYinlian(String order_no, String price) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("product_name", "悦美");
        maps.put("order_no", order_no);
        maps.put("order_price", price);
        maps.put("weikuan", weikuan);
        new PayYinlianApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    PrePayIdData payData = JSONUtil.TransformWXpayidShare(serverData.data);
                    String tn = payData.getPrepay_id();

                    if (null != tn && tn.length() > 0) {
                        UPPayAssistEx.startPayByJAR(OrderMethodActivity594.this, PayActivity.class, null, null, tn, "00");
                    } else {
                        mFunctionManager.showShort("请稍后重试");
                    }

                } else {
                    mFunctionManager.showShort(serverData.message);
                }
            }
        });
    }

    /**
     * 微信支付
     *
     * @param order_no
     * @param price
     */
    private void payWeixin(String order_no, String price) {

        Map<String, Object> maps = new HashMap<>();
        maps.put("product_name", "悦美");//
        maps.put("order_no", order_no);//
        maps.put("order_price", price);
        maps.put("weikuan", weikuan);
        Log.e(TAG, "order_no == " + order_no);
        Log.e(TAG, "order_price == " + price);
        Log.e(TAG, "weikuan == " + weikuan);
        new PayWeixinApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    Log.e(TAG, "222isGroup === " + isGroup);

                    PrePayIdData payData = JSONUtil.TransformWXpayidShare(serverData.data);
                    String prepayId = payData.getPrepay_id();
                    String sign = payData.getSign();
                    String noncestr = payData.getNoncestr();
                    String timestamp = payData.getTimestamp();
                    String orderid = payData.getOrder_id();

                    Log.e(TAG, "orderid === " + orderid);
                    mFunctionManager.saveStr("server_id", server_id);
                    mFunctionManager.saveStr("order_id", order_id);
//                    mFunctionManager.saveStr("order_id", orderid);
                    mFunctionManager.saveStr("price", finalPrice);
                    mFunctionManager.saveStr("taotitle", "悦美");
                    mFunctionManager.saveStr("sku_type", sku_type);
                    mFunctionManager.saveStr("is_repayment", is_repayment);
                    mFunctionManager.saveStr("is_repayment_mimo", is_repayment_mimo);
                    mFunctionManager.saveStr("weikuan", weikuan);
                    mFunctionManager.saveStr("is_group", isGroup);
                    mFunctionManager.saveStr("group_id",groupId);
                    mFunctionManager.saveStr("pay_sao", "1");

                    genPayReq(prepayId, sign, noncestr, timestamp);
                } else {
                    mFunctionManager.showShort(serverData.message);
                }
            }

        });
    }

    /**
     * 微信支付
     *
     * @param prepay_id
     */
    private void genPayReq(String prepay_id, String sign, String noncestr, String timestamp) {

        req.appId = Constants.APP_ID;
        req.partnerId = Constants.MCH_ID;
        req.prepayId = prepay_id;
        req.packageValue = "prepay_id=" + prepay_id;
        req.nonceStr = noncestr;
        req.timeStamp = timestamp;

        List<NameValuePair> signParams = new LinkedList<>();
        signParams.add(new BasicNameValuePair("appid", req.appId));
        signParams.add(new BasicNameValuePair("noncestr", req.nonceStr));
        signParams.add(new BasicNameValuePair("package", req.packageValue));
        signParams.add(new BasicNameValuePair("partnerid", req.partnerId));
        signParams.add(new BasicNameValuePair("prepayid", req.prepayId));
        signParams.add(new BasicNameValuePair("timestamp", req.timeStamp));
        req.sign = sign;

        sb.append("sign\n" + req.sign + "\n\n");

        // 在支付之前，如果应用没有注册到微信，应该先调用IWXMsg.registerApp将应用注册到微信
        msgApi.registerApp(Constants.APP_ID);
        msgApi.sendReq(req);
    }

    public class HbPopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public HbPopupWindows(final Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.pop_hbfenqi_qinum, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            TextView fenqi_[] = new TextView[3];
            fenqi_[0] = view.findViewById(R.id.hbfenqi_price_tv_3);
            fenqi_[1] = view.findViewById(R.id.hbfenqi_price_tv_6);
            fenqi_[2] = view.findViewById(R.id.hbfenqi_price_tv_12);

            TextView fei_[] = new TextView[3];
            fei_[0] = view.findViewById(R.id.hbfenqi_fei_tv_3);
            fei_[1] = view.findViewById(R.id.hbfenqi_fei_tv_6);
            fei_[2] = view.findViewById(R.id.hbfenqi_fei_tv_12);

            final ImageView checkBox_hb[] = new ImageView[3];
            checkBox_hb[0] = view.findViewById(R.id.order_fenqi_check_3);
            checkBox_hb[1] = view.findViewById(R.id.order_fenqi_check_6);
            checkBox_hb[2] = view.findViewById(R.id.order_fenqi_check_12);


            RelativeLayout clickRly[] = new RelativeLayout[3];
            clickRly[0] = view.findViewById(R.id.hb_fenqi_rly_3);
            clickRly[1] = view.findViewById(R.id.hb_fenqi_rly_6);
            clickRly[2] = view.findViewById(R.id.hb_fenqi_rly_12);


            fenqi_[0].setText("￥" + hbfenqiS[0] + " x 3期");
            fenqi_[1].setText("￥" + hbfenqiS[1] + " x 6期");
            fenqi_[2].setText("￥" + hbfenqiS[2] + " x 12期");

            fei_[0].setText("(含手续费" + hbSxffenqiS[0] + "元/期)");
            fei_[1].setText("(含手续费" + hbSxffenqiS[1] + "元/期)");
            fei_[2].setText("(含手续费" + hbSxffenqiS[2] + "元/期)");


            clickRly[0].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ifHbpop = false;
                    check1.setChecked(false);
                    check3.setChecked(false);
                    check2.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(true);
                    check7.setChecked(false);
                    dismiss();
                    checkBox_hb[0].setBackgroundResource(R.drawable.raido_dui3x);
                    checkBox_hb[1].setBackgroundResource(R.drawable.raido_cuo3x);
                    checkBox_hb[2].setBackgroundResource(R.drawable.raido_cuo3x);
                    hbTipsLy.setVisibility(View.VISIBLE);
                    lefenTipsLy.setVisibility(View.GONE);

                    hbfenqiPaytype = "3";
                    hbfenqiTv.setText("￥" + hbfenqiS[0] + " x 3期");
                    hbsxffenqiTv.setText("(含手续费" + hbSxffenqiS[0] + "元/期)");


                    shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + hbfenqiS[0] + "</font>" + "<small><font color=\"#333333\">" + "x3期" + "</font></small>"));
                }
            });

            clickRly[1].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ifHbpop = false;
                    check1.setChecked(false);
                    check3.setChecked(false);
                    check2.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(true);
                    check7.setChecked(false);
                    dismiss();
                    checkBox_hb[0].setBackgroundResource(R.drawable.raido_cuo3x);
                    checkBox_hb[1].setBackgroundResource(R.drawable.raido_dui3x);
                    checkBox_hb[2].setBackgroundResource(R.drawable.raido_cuo3x);
                    hbTipsLy.setVisibility(View.VISIBLE);
                    lefenTipsLy.setVisibility(View.GONE);

                    hbfenqiPaytype = "6";
                    hbfenqiTv.setText("￥" + hbfenqiS[1] + " x 6期");
                    hbsxffenqiTv.setText("(含手续费" + hbSxffenqiS[1] + "元/期)");

                    shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + hbfenqiS[1] + "</font>" + "<small><font color=\"#333333\">" + "x6期" + "</font></small>"));

                }
            });

            clickRly[2].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ifHbpop = false;
                    check1.setChecked(false);
                    check3.setChecked(false);
                    check2.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(true);
                    check7.setChecked(false);
                    dismiss();
                    checkBox_hb[0].setBackgroundResource(R.drawable.raido_cuo3x);
                    checkBox_hb[1].setBackgroundResource(R.drawable.raido_cuo3x);
                    checkBox_hb[2].setBackgroundResource(R.drawable.raido_dui3x);
                    hbTipsLy.setVisibility(View.VISIBLE);
                    lefenTipsLy.setVisibility(View.GONE);

                    hbfenqiPaytype = "12";
                    hbfenqiTv.setText("￥" + hbfenqiS[2] + " x 12期");
                    hbsxffenqiTv.setText("(含手续费" + hbSxffenqiS[2] + "元/期)");

                    shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + hbfenqiS[2] + "</font>" + "<small><font color=\"#333333\">" + "x12期" + "</font></small>"));

                }
            });


            ImageView closeIv = view.findViewById(R.id.close_iv);

            closeIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            // 点击 之外 消失
            view.setOnTouchListener(new View.OnTouchListener() {

                @SuppressLint("ClickableViewAccessibility")
                public boolean onTouch(View v, MotionEvent event) {

                    int height = view.findViewById(R.id.ll_popup).getTop();
                    int y = (int) event.getY();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (y < height) {
                            dismiss();
                        }
                    }
                    return true;
                }
            });

        }
    }

    public class LeBaiFenPopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public LeBaiFenPopupWindows(final Context mContext) {

            final View view = View.inflate(mContext, R.layout.pop_lebaifenfenqi_qinum, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            TextView fenqi_[] = new TextView[2];
            fenqi_[0] = view.findViewById(R.id.hbfenqi_price_tv_3);
            fenqi_[1] = view.findViewById(R.id.hbfenqi_price_tv_6);

            TextView fei_[] = new TextView[2];
            fei_[0] = view.findViewById(R.id.hbfenqi_fei_tv_3);
            fei_[1] = view.findViewById(R.id.hbfenqi_fei_tv_6);

            final ImageView checkBox_hb[] = new ImageView[2];
            checkBox_hb[0] = view.findViewById(R.id.order_fenqi_check_3);
            checkBox_hb[1] = view.findViewById(R.id.order_fenqi_check_6);


            RelativeLayout clickRly[] = new RelativeLayout[2];
            clickRly[0] = view.findViewById(R.id.hb_fenqi_rly_3);
            clickRly[1] = view.findViewById(R.id.hb_fenqi_rly_6);


            fenqi_[0].setText("￥" + formatDouble4(fenqi6) + " x 6期");
            fenqi_[1].setText("￥" + formatDouble4(fenqi12) + " x 12期");

            clickRly[0].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iflebaifenpop = false;
                    check1.setChecked(false);
                    check3.setChecked(false);
                    check2.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(false);
                    check7.setChecked(true);
                    dismiss();
                    checkBox_hb[0].setBackgroundResource(R.drawable.raido_dui3x);
                    checkBox_hb[1].setBackgroundResource(R.drawable.raido_cuo3x);

                    fenqiPaytype = "6";

                    lefenTipsLy.setVisibility(View.VISIBLE);
                    hbTipsLy.setVisibility(View.GONE);
                    lefenqiTv.setText("￥" + formatDouble4(fenqi6) + " x 6期");

                    shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + formatDouble4(fenqi6) + "</font>" + "<small><font color=\"#333333\">" + "x6期" + "</font></small>"));

                }
            });

            clickRly[1].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iflebaifenpop = false;
                    check1.setChecked(false);
                    check3.setChecked(false);
                    check2.setChecked(false);
                    check4.setChecked(false);
                    check5.setChecked(false);
                    check6.setChecked(false);
                    check7.setChecked(true);
                    dismiss();
                    checkBox_hb[0].setBackgroundResource(R.drawable.raido_cuo3x);
                    checkBox_hb[1].setBackgroundResource(R.drawable.raido_dui3x);

                    fenqiPaytype = "12";

                    lefenTipsLy.setVisibility(View.VISIBLE);
                    hbTipsLy.setVisibility(View.GONE);
                    lefenqiTv.setText("￥" + formatDouble4(fenqi12) + " x 12期");

                    shifuTv.setText(Html.fromHtml("<small><font color=\"#ff5c77\">" + "￥" + "</font></small>" + "<font color=\"#ff5c77\" size=\"40\">" + formatDouble4(fenqi12) + "</font>" + "<small><font color=\"#333333\">" + "x6期" + "</font></small>"));

                }
            });

            ImageView closeIv = view.findViewById(R.id.close_iv);

            closeIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            // 点击 之外 消失
            view.setOnTouchListener(new View.OnTouchListener() {

                @SuppressLint("ClickableViewAccessibility")
                public boolean onTouch(View v, MotionEvent event) {

                    int height = view.findViewById(R.id.ll_popup).getTop();
                    int y = (int) event.getY();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (y < height) {
                            dismiss();
                        }
                    }
                    return true;
                }
            });

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
