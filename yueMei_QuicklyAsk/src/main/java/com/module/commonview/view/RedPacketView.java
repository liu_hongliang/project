package com.module.commonview.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class RedPacketView extends LinearLayout {

    private TextView mTextView;
    public RedPacketView(Context context) {
        this(context,null);
    }

    public RedPacketView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public RedPacketView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }



    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_red_packet, this, true);
        mTextView=view.findViewById(R.id.redpacket_text);
    }

    public void setTextView(String content){
        mTextView.setText(content);
    }
}
