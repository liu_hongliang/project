/**
 * 
 */
package com.module.home.model.bean;


import com.module.taodetail.model.bean.HomeTaoData;

import java.util.List;

/**
 * 首页淘整形列表
 * 
 * @author Robin
 * 
 */
public class HomeTao {

	private String code;
	private String message;
	private List<HomeTaoData> data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public List<HomeTaoData> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<HomeTaoData> data) {
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HomeTao [code=" + code + ", message=" + message + ", data="
				+ data + "]";
	}

}
