package com.module.doctor.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyask.activity.R;
import com.quicklyask.view.ElasticScrollView;
import com.quicklyask.view.ElasticScrollView.OnRefreshListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 医生解决问题的详情页
 * 
 * @author Rubin
 * 
 */
public class DocSloveQueWebActivity extends BaseActivity {

	private final String TAG = "DocSloveQueWebActivity";

	@BindView(id = R.id.doc_slove_que_web_det_scrollview1, click = true)
	private ElasticScrollView scollwebView;
	@BindView(id = R.id.doc_slove_que_linearlayout, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.doc_slove_que_web_back, click = true)
	private RelativeLayout back;// 返回

	private WebView docDetWeb;

	private DocSloveQueWebActivity mContex;

	private String link;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.web_acty_doc_solve_question);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = DocSloveQueWebActivity.this;

		Intent it0 = getIntent();
		link = it0.getStringExtra("link");

		scollwebView.GetLinearLayout(contentWeb);
		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		initWebview();
		scollwebView.setonRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				webReload();
			}
		});
		LodUrl(link);
	}

	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}


	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 加载web
	 */
	public void LodUrl(String link) {
		baseWebViewClientMessage.startLoading();

		WebSignData addressAndHead = SignUtils.getAddressAndHead(link);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.doc_slove_que_web_back:
			finish();
			break;
		}
	}


	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
