package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Selection;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.view.CommonTopBar;
import com.module.doctor.model.api.CallAndSecurityCodeApi;
import com.module.home.controller.activity.ScanCaptureAct;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.LoginApi;
import com.module.my.model.api.LoginOtherHttpApi;
import com.module.my.model.api.SendEMSApi;
import com.module.my.model.api.VerificationSecurityCodeApi;
import com.module.my.model.bean.LoginData;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.GetPhoneCodePopWindow;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.kjframe.utils.SystemTool;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.HashMap;
import java.util.Map;

public class LoginBackActivity extends BaseActivity {

	private final String TAG = "LoginBackActivity";

	private LoginBackActivity mContext;

	@BindView(id = R.id.login_top)
	private CommonTopBar mTop;

	//账号密码登录
	@BindView(id = R.id.login_username)
	private EditText userNameEt;// 输入名字
	@BindView(id = R.id.login_password)
	private EditText userPasswordEt;// 输入密码
	@BindView(id = R.id.login_bt, click = true)
	private Button loginBt;// 登录
	@BindView(id = R.id.login_forget_mima_tv, click = true)
	private TextView forgetMimaTv;// 忘记密码

	private UserData userData;
	private LoginData loginData;

	private String flag = "";

	private final int BACK3 = 3;//注册返回回调

	//第三方平台登录
	@BindView(id = R.id.login_weixin_bt_rly, click = true)
	private RelativeLayout weixinBt;
	@BindView(id = R.id.login_qq_bt_rly, click = true)
	private RelativeLayout qqBt;
	@BindView(id = R.id.login_weibo_bt_rly, click = true)
	private RelativeLayout weiboBt;
	// 第三方登录
	// 第三方登录
	private UMShareAPI mShareAPI = null;
	private String loginSex;
	private String loginName;
	private String oauthId;
	private String fromSite;
	private String openid="";
	private String unionid="";

	private String access_token="";
	private String refresh_token="";


	@BindView(id = R.id.password_if_ming_iv, click = true)
	private ImageView passIfShowIv;
	private boolean passIsShow = false;

	@BindView(id = R.id.login_caidan_type1, click = true)
	private RelativeLayout loginType1;//无密码登录
	@BindView(id = R.id.login_caidan_type2, click = true)
	private RelativeLayout loginType2;//账号密码登录
	@BindView(id = R.id.login_type_1)
	private LinearLayout loginTypeLy1;//无密码登录界面
	@BindView(id = R.id.login_type_2)
	private LinearLayout loginTypeLy2;//账号密码登录界面

	//无密码快捷登录
	@BindView(id = R.id.no_pass_login_phone_et)
	private EditText phoneNumberEt; //无密码登录手机输入框
	@BindView(id = R.id.no_pass_login_code_et)
	private EditText codeEt;//无密码登录验证码输入框
	@BindView(id = R.id.no_pass_login_bt, click = true)
	private Button np_loginBt;//无密码登录按钮

	@BindView(id = R.id.no_pass_yanzheng_code_rly,click = true)
	private RelativeLayout sendEMSRly;//无密码登录发送验证码
	@BindView(id = R.id.yanzheng_code_tv)
	private TextView emsTv;

	@BindView(id = R.id.nocde_message_tv, click = true)
	private TextView noCodeTv;// 没收到验证码

	private PopupWindows yuyinCodePop;
	@BindView(id = R.id.order_time_all_ly)
	private LinearLayout allcontent;

	private GetPhoneCodePopWindow phoneCodePop;


	ImageOptions imageOptions;
	private String np_phone;
	private String np_codeStr;
	//第三方登录
	@BindView(id = R.id.login_disanfang_onandoff, click = true)
	private LinearLayout onAndoffLy;
	@BindView(id = R.id.login_qq_weiobo_ly)
	private LinearLayout qqweiboweixinLy;
	@BindView(id = R.id.login_line_qqweibo_jiantou_iv)
	private ImageView qqweiboJiantouIv;

	private String loginType="1";
	private String loginUser="";

	@BindView(id = R.id.yuemei_yinsi_ly, click = true)
	private LinearLayout yuemeiYinsiLy;
	private LoadingProgress mDialog;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_login_605);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = LoginBackActivity.this;

		mDialog = new LoadingProgress(mContext);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		/** init auth api**/
		mShareAPI = UMShareAPI.get(LoginBackActivity.this);

		imageOptions = new ImageOptions.Builder()
				.setUseMemCache(false)
				.setImageScaleType(ImageView.ScaleType.FIT_XY)
				.build();

		setCutLoginType("1");

		mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				Intent it2 = new Intent();
				it2.setClass(mContext, RegisterActivity548.class);
				startActivityForResult(it2, BACK3);
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}


	void setCutLoginType(String tab){
		if(tab.equals("1")){
			loginType1.setBackground(null);
			loginType2.setBackgroundResource(R.drawable.bian_ecec_dbdb);
			loginTypeLy1.setVisibility(View.VISIBLE);
			loginTypeLy2.setVisibility(View.GONE);
		} else {
			loginType1.setBackgroundResource(R.drawable.bian_ecec_dbdb);
			loginType2.setBackground(null);
			loginTypeLy1.setVisibility(View.GONE);
			loginTypeLy2.setVisibility(View.VISIBLE);
		}
	}



	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
			case R.id.yuemei_yinsi_ly:
				Intent it=new Intent();
				it.setClass(mContext,UserAgreementWebActivity.class);
				startActivity(it);
				break;
			case R.id.no_pass_yanzheng_code_rly://发送验证码
				String phone = phoneNumberEt.getText().toString();
				if (phone.length() > 0) {
					if (ifPhoneNumber()) {
						sendEMS();
						noCodeTv.setVisibility(View.VISIBLE);
						noCodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？"
								+ "</u>"));
					} else {
						ViewInject.toast("请输入正确的手机号");
					}
				} else {
					ViewInject.toast("请输入手机号");
				}
				break;
			case R.id.login_caidan_type1:
				setCutLoginType("1");
				break;
			case R.id.login_caidan_type2:
				setCutLoginType("2");
				break;
			case R.id.login_disanfang_onandoff:
				if(qqweiboweixinLy.getVisibility()==View.VISIBLE){
					qqweiboweixinLy.setVisibility(View.GONE);
					qqweiboJiantouIv.setBackgroundResource(R.drawable.login_line_up_2x);
				}else {
					qqweiboweixinLy.setVisibility(View.VISIBLE);
					qqweiboJiantouIv.setBackgroundResource(R.drawable.login_line_down_2x);
				}
				break;
			case R.id.no_pass_login_bt:// 登录

				loginType="1";

				np_codeStr = codeEt.getText().toString();
				np_phone = phoneNumberEt.getText().toString().trim();
				if (np_phone.length() > 0) {
					if (np_codeStr.length() > 0) {
						mDialog.startLoading();
						initCode1();
					} else {
						ViewInject.toast("请输入验证码！");
					}
				} else {
					ViewInject.toast("请输入手机号！");
				}
				break;
			case R.id.nocde_message_tv:// 没收到验证码
				yuyinCodePop = new PopupWindows(mContext, allcontent);
				yuyinCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

				Glide.with(mContext).load(FinalConstant.TUXINGCODE).into(codeIv);

				break;
			case R.id.login_bt:

				loginType="1";

				if (!judgeEmailAndPhone()) {
					ViewInject.toast("请输入正确的手机号或邮箱");
				} else {
					// ViewInject.toast("Login");
					String nameStr = userNameEt.getText().toString();
					loginUser=nameStr;
//					Cfg.saveStr(mContext,FinalConstant.UUSERNAME,nameStr);
					String passStr = userPasswordEt.getText().toString();
					// startLoading();
					if (null != passStr && passStr.length() > 0) {
						loginBt.setBackgroundResource(R.drawable.shape_f6cbd1);
						loginBt.setText("登录中...");
						loginBt.setClickable(false);
						Map<String,Object>maps=new HashMap<>();
						maps.put("username",nameStr);
						maps.put("password",passStr);
						new LoginApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
							@Override
							public void onSuccess(ServerData serverData) {
								if (serverData.code.equals("1")) {
									UserData userData = JSONUtil.TransformLogin1(serverData.data);
									String id = userData.get_id();

									String img = userData.getImg();
									String nickName = userData.getNickname();

//									Cfg.saveStr(mContext,FinalConstant.UUSERNAME,loginUser);
									ViewInject.toast("登录成功");

									flag = "1";
									PushManager.startWork(getApplicationContext(),
											PushConstants.LOGIN_TYPE_API_KEY, Utils.BAIDU_PUSHE_KEY);
									getUserInfoLogin(id);

								} else {
									ViewInject.toast(serverData.message);
									loginBt.setBackgroundResource(R.drawable.login_bt);
									loginBt.setText("登录");
									loginBt.setClickable(true);
								}
							}

						});
					}

				}
				break;
			case R.id.login_forget_mima_tv:
				Intent it1 = new Intent();
				it1.putExtra("type", "1");
				it1.setClass(mContext, RestPassWordActivity.class);
				startActivity(it1);
				break;
			case R.id.login_weixin_bt_rly:

				if(mShareAPI.isInstall(this, SHARE_MEDIA.WEIXIN)){
					loginType="2";
					mShareAPI.doOauthVerify(LoginBackActivity.this, SHARE_MEDIA.WEIXIN, umAuthListener);
				}else {
					ViewInject.toast("请安装微信客户端");
				}

				break;
			case R.id.login_qq_bt_rly:

				if(mShareAPI.isInstall(this, SHARE_MEDIA.QQ)){
					loginType="2";
					mShareAPI.doOauthVerify(LoginBackActivity.this, SHARE_MEDIA.QQ, umAuthListener);
				}else {
					ViewInject.toast("请安装QQ客户端");
				}

				break;
			case R.id.login_weibo_bt_rly:

				if(mShareAPI.isInstall(this, SHARE_MEDIA.SINA)){
					loginType="2";
					mShareAPI.doOauthVerify(LoginBackActivity.this, SHARE_MEDIA.SINA, umAuthListener);
				}else {
					ViewInject.toast("请安装新浪微博客户端");
				}

				break;
			case R.id.password_if_ming_iv:// 密码是否铭文
				if (passIsShow) {
					passIfShowIv.setBackgroundResource(R.drawable.miwen_);
					userPasswordEt.setInputType(InputType.TYPE_CLASS_TEXT
							| InputType.TYPE_TEXT_VARIATION_PASSWORD);
					Editable etext = userPasswordEt.getText();
					Selection.setSelection(etext, etext.length());
					passIsShow = false;
				} else {
					passIfShowIv.setBackgroundResource(R.drawable.mingwen_);
					userPasswordEt
							.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
					Editable etext = userPasswordEt.getText();
					Selection.setSelection(etext, etext.length());
					passIsShow = true;
				}
				break;
		}
	}


	/**
	 * 无密码登录验证验证码
	 */
	void initCode1() {
		np_codeStr = codeEt.getText().toString();
		Map<String,Object>maps=new HashMap<>();
		new VerificationSecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if ("1".equals(serverData.code)) {

					UserData userData = JSONUtil.TransformLogin1(serverData.data);
					String id = userData.get_id();

//					Cfg.saveStr(mContext, FinalConstant.UUSERNAME, np_phone);

					flag = "1";
					PushManager.startWork(getApplicationContext(),
							PushConstants.LOGIN_TYPE_API_KEY, Utils.BAIDU_PUSHE_KEY);
					getUserInfoLogin(id);

					SystemTool.hideKeyBoard(LoginBackActivity.this);
				}
			}

		});
	}


	@SuppressLint("NewApi")
	void loginOtherHttp() {
		Map<String,Object>params=new HashMap<>();
		params.put("from_site", fromSite);
		params.put("oauth_id", oauthId);
		params.put("openid",openid );
		params.put("unionid", unionid);
		params.put("name", loginName);
		params.put("sex", loginSex);
		params.put("access_token", access_token);
		params.put("access_token", refresh_token);
		new LoginOtherHttpApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				mDialog.stopLoading();
				if ("1".equals(serverData.code)){
					userData = JSONUtil.TransformLogin(serverData.data);
					String id = userData.get_id();

					String img = userData.getImg();
					String nickName = userData.getNickname();

					ViewInject.toast("登录成功");


					getUserInfoLogin(id);
				}else{
					ViewInject.toast(serverData.message);
				}
			}
		});

	}

	private void getUserInfoLogin(String uid) {
		BBsDetailUserInfoApi userInfoApi = new BBsDetailUserInfoApi();
		Map<String,Object> params=new HashMap<>();
		params.put("id",uid);
		userInfoApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if ("1".equals(serverData.code)){
					UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
					String id = userData.get_id();
					String img = userData.getImg();
					String nickName = userData.getNickname();
					String birthday = userData.getBirthday();
					String province = userData.getProvince();
					String city = userData.getCity();
					String sex = userData.getSex();

					String phoneStr = userData.getPhone();
					String loginPhone=userData.getLoginphone();

					Utils.setUid(id);
					Cfg.saveStr(mContext, FinalConstant.UHEADIMG,
							img);
					Cfg.saveStr(mContext, FinalConstant.UNAME,
							nickName);
					Cfg.saveStr(mContext, FinalConstant.UPROVINCE,
							province);
					Cfg.saveStr(mContext, FinalConstant.UCITY, city);
					Cfg.saveStr(mContext, FinalConstant.USEX, sex);
					Cfg.saveStr(mContext, FinalConstant.UBIRTHDAY, birthday);

					if (phoneStr.equals("0")) {
						Cfg.saveStr(mContext, FinalConstant.UPHONE,
								"");
					} else {
						Cfg.saveStr(mContext, FinalConstant.UPHONE,
								phoneStr);
					}

					if (loginPhone.equals("0")) {
						Cfg.saveStr(mContext, FinalConstant.ULOGINPHONE,
								"");
					} else {
						Cfg.saveStr(mContext, FinalConstant.ULOGINPHONE,
								loginPhone);
					}

//								RongIMManager.getInstance(mContext, null,"")
//										.init();

					PushManager.startWork(getApplicationContext(),
							PushConstants.LOGIN_TYPE_API_KEY, Utils.BAIDU_PUSHE_KEY);

					Intent it888 = new Intent();
					it888.setClass(mContext, ScanCaptureAct.class);
					setResult(88, it888);
					finish();


					if(loginType.equals("2")){
						if (loginPhone.equals("0")) {
							Utils.jumpBindingPhone(mContext);
						} else {
						}

					}else {
					}
				}else {
					Toast.makeText(mContext,serverData.message,Toast.LENGTH_SHORT).show();
				}


			}
		});
	}

	private boolean judgeEmailAndPhone() {
		String nameStr = userNameEt.getText().toString();
		if (nameStr.contains("@")) {
            return Utils.emailFormat(nameStr);
		} else {
            return Utils.isMobile(nameStr);
		}
    }


	private boolean ifPhoneNumber() {
		String textPhn = phoneNumberEt.getText().toString();
        return Utils.isMobile(textPhn);
    }

	void sendEMS() {
		np_phone = phoneNumberEt.getText().toString();
		Map<String,Object>maps=new HashMap<>();
		maps.put("phone",np_phone);
		maps.put("flag","1");
		new SendEMSApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					ViewInject.toast(serverData.message);

					sendEMSRly.setClickable(false);
					codeEt.requestFocus();

					new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

						@Override
						public void onTick(long millisUntilFinished) {
							sendEMSRly
									.setBackgroundResource(R.drawable.biankuang_hui);
							emsTv.setTextColor(getResources().getColor(
									R.color.button_zi));
							emsTv.setText("(" + millisUntilFinished / 1000
									+ ")重新获取");
						}

						@Override
						public void onFinish() {
							sendEMSRly
									.setBackgroundResource(R.drawable.shape_bian_ff5c77);
							emsTv.setTextColor(getResources().getColor(
									R.color.button_bian_hong1));
							sendEMSRly.setClickable(true);
							emsTv.setText("重发验证码");
						}
					}.start();
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
			case BACK3:
				if (data != null) {
					Intent it888 = new Intent();
					it888.setClass(mContext, ScanCaptureAct.class);
					setResult(88, it888);
					finish();
				}
				break;
		}

	}


	EditText codeEt1;
	ImageView codeIv;

	/**
	 * 获取手机号 并验证
	 *
	 * @author dwb
	 *
	 */
	public class PopupWindows extends PopupWindow {

		@SuppressWarnings("deprecation")
		public PopupWindows(Context mContext, View parent) {

			final View view = View.inflate(mContext, R.layout.pop_yuyincode,
					null);

			view.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.fade_ins));

			setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
			setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			// showAtLocation(parent, Gravity.BOTTOM, 0, 0);
			update();

			Button cancelBt = view.findViewById(R.id.cancel_bt);
			Button tureBt = view.findViewById(R.id.zixun_bt);
			codeEt1 = view.findViewById(R.id.no_pass_login_code_et);
			codeIv = view.findViewById(R.id.yuyin_code_iv);

			RelativeLayout rshCodeRly = view
					.findViewById(R.id.no_pass_yanzheng_code_rly);

			rshCodeRly.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					x.image().bind(codeIv,FinalConstant.TUXINGCODE,imageOptions);
				}
			});

			tureBt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					String codes = codeEt1.getText().toString();
					if (codes.length() > 1) {
						yanzhengCode(codes);
					} else {
						ViewInject.toast("请输入图中数字");
					}
				}
			});

			cancelBt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

		}
	}

	void yanzhengCode(String codes) {
		String phones = phoneNumberEt.getText().toString().trim();
		Map<String,Object>maps=new HashMap<>();
		maps.put("phone",phones);
		maps.put("code",codes);
		maps.put("flag","codelogin");
		new CallAndSecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData s) {
				if ("1".equals(s.code)){
					yuyinCodePop.dismiss();
					ViewInject.toast("正在拨打您的电话，请注意接听");
				}else{
					ViewInject.toast("数字错误，请重新输入");
				}
			}

		});
	}


	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}


	/** auth callback interface**/
	private UMAuthListener umAuthListener = new UMAuthListener() {
		@Override
		public void onStart(SHARE_MEDIA share_media) {

		}

		@Override
		public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {

//            Toast.makeText(getApplicationContext(), "Authorize succeed", Toast.LENGTH_SHORT).show();

			if(platform.equals(SHARE_MEDIA.WEIXIN)){


				mShareAPI.getPlatformInfo(LoginBackActivity.this, platform, new UMAuthListener() {
					@Override
					public void onStart(SHARE_MEDIA share_media) {

					}

					@Override
					public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {

						loginSex = map.get("gender")
								+ "";
						loginName = map
								.get("screen_name")
								+ "";
						fromSite = "weixin";
						oauthId = map
								.get("unionid")
								+ "";
						openid = map.get("openid") + "";
						unionid = map.get("unionid") + "";
						loginOtherHttp();
					}

					@Override
					public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {

					}

					@Override
					public void onCancel(SHARE_MEDIA share_media, int i) {

					}
				});

			}else if(platform.equals(SHARE_MEDIA.QQ)){

//                Log.e("AAAAAAAAAAuser info","qq:"+data.toString());

				oauthId = data.get("openid") + "";
				openid = data.get("openid") + "";


				mShareAPI.getPlatformInfo(LoginBackActivity.this, platform, new UMAuthListener() {
					@Override
					public void onStart(SHARE_MEDIA share_media) {

					}

					@Override
					public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {
//                        Log.e("AAAAAAAAAAuser info","qq_uu:"+map.toString());

						loginSex = map.get("gender")
								+ "";
						if(loginSex.equals("男")){
							loginSex="1";
						}else {
							loginSex="2";
						}
						loginName = map
								.get("screen_name")
								+ "";
						fromSite = "qq";

//                        Log.e("AAAAA","qqqqqqq=="+loginSex+","+loginName+","+openid);

						loginOtherHttp();
					}

					@Override
					public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {

					}

					@Override
					public void onCancel(SHARE_MEDIA share_media, int i) {

					}
				});


			}else if(platform.equals(SHARE_MEDIA.SINA)){

				oauthId = data.get("uid") + "";

				mShareAPI.getPlatformInfo(LoginBackActivity.this, platform, new UMAuthListener() {
					@Override
					public void onStart(SHARE_MEDIA share_media) {

					}

					@Override
					public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {
//                        Log.e("AAAAAAAAAAuser info","sina_uu:"+map.toString());

						String sex = map.get("gender")
								+ "";
						if(sex.equals("m")){
							loginSex="1";
						}else {
							loginSex="2";
						}
						loginName = map
								.get("screen_name")
								+ "";
						fromSite = "sina";

						loginOtherHttp();
					}

					@Override
					public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {

					}

					@Override
					public void onCancel(SHARE_MEDIA share_media, int i) {

					}
				});

			}
		}

		@Override
		public void onError(SHARE_MEDIA platform, int action, Throwable t) {
			Toast.makeText( getApplicationContext(), "Authorize fail", Toast.LENGTH_SHORT).show();

			Log.e("AAAAA","onError=="+t);

		}

		@Override
		public void onCancel(SHARE_MEDIA platform, int action) {
			Toast.makeText( getApplicationContext(), "Authorize cancel", Toast.LENGTH_SHORT).show();
		}
	};
}
