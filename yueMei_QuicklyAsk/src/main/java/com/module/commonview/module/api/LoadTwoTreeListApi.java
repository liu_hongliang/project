package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.module.bean.MakeTagData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/11/2.
 */

public class LoadTwoTreeListApi implements BaseCallBackApi {
    private String TAG = "LoadTwoTreeListApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.BOARD, "seltagtree", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if("1".equals(mData.code)){
                    Log.e(TAG, "mData.data == " + mData.data);
                    List<MakeTagData> dataBeen = JSONUtil.jsonToArrayList(mData.data,MakeTagData.class);
                    listener.onSuccess(dataBeen);
                }
            }
        });
    }
}
