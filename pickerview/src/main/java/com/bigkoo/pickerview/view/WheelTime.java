package com.bigkoo.pickerview.view;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.bigkoo.pickerview.R;
import com.bigkoo.pickerview.TimePickerView.Type;
import com.bigkoo.pickerview.adapter.NumericWheelAdapter;
import com.bigkoo.pickerview.lib.WheelView;
import com.bigkoo.pickerview.listener.OnItemSelectedListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class WheelTime {
    public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private View view;
    private WheelView wv_year;
    private WheelView wv_month;
    private WheelView wv_day;
    private WheelView wv_hours;
    private WheelView wv_mins;

    private Type type;
    public static final int DEFULT_START_YEAR = 1990;
    public static final int DEFULT_END_YEAR = 2100;
    private int startYear = DEFULT_START_YEAR;
    private int endYear = DEFULT_END_YEAR;
    private int endMonth = -1;
    private int endDay = -1;
    private String TAG = "WheelTime";
    private int dq_year = Calendar.getInstance().get(Calendar.YEAR);            //当前年份
    private int dq_month = Calendar.getInstance().get(Calendar.MONTH) + 1;      //当前月份
    private int dq_day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);     //当前日期

    private int year_num = dq_year;           //当前选中的年，默认选中当前年
    private int month_num = dq_month;       //当前选中的月份，默认选中当前月
    private int day_num = dq_day;           //当前选中的日期，默认选中当前日

    public WheelTime(View view) {
        super();
        this.view = view;
        type = Type.ALL;
        setView(view);
    }

    public WheelTime(View view, Type type) {
        super();
        this.view = view;
        this.type = type;
        setView(view);
    }

    public void setPicker(int year, int month, int day) {
        this.setPicker(year, month, day, 0, 0);
    }

    public void setPicker(int year, int month, int day, int h, int m) {
        Log.e(TAG, "endDay == " + endDay);
        // 添加大小月月份并将其转换为list,方便之后的判断
        String[] months_big = {"1", "3", "5", "7", "8", "10", "12"};
        String[] months_little = {"4", "6", "9", "11"};

        final List<String> list_big = Arrays.asList(months_big);
        final List<String> list_little = Arrays.asList(months_little);

        year_num = year;
        month_num = month;
        day_num = day;

        Context context = view.getContext();
        // 年
        wv_year = view.findViewById(R.id.year);
        wv_year.setAdapter(new NumericWheelAdapter(startYear, endYear));// 设置"年"的显示数据
        wv_year.setLabel(context.getString(R.string.pickerview_year));// 添加文字
        wv_year.setCurrentItem(year_num - startYear);// 初始化时显示的数据

        // 月
        wv_month = view.findViewById(R.id.month);
        if (year_num == dq_year && month_num == dq_month) {
            wv_month.setAdapter(new NumericWheelAdapter(1, endMonth));
        } else {
            wv_month.setAdapter(new NumericWheelAdapter(1, 12));
        }
        wv_month.setLabel(context.getString(R.string.pickerview_month));
        wv_month.setCurrentItem(month_num);

        // 日
        wv_day = view.findViewById(R.id.day);
        // 判断大小月及是否闰年,用来确定"日"的数据
        if (list_big.contains(String.valueOf(month_num + 1))) {
            if (year_num == dq_year && month_num == dq_month && day_num == dq_day) {
                wv_day.setAdapter(new NumericWheelAdapter(1, endDay));
            } else {
                wv_day.setAdapter(new NumericWheelAdapter(1, 31));
            }
        } else if (list_little.contains(String.valueOf(month_num + 1))) {
            if (year_num == dq_year && month_num == dq_month && day_num == dq_day) {
                wv_day.setAdapter(new NumericWheelAdapter(1, endDay));
            } else {
                wv_day.setAdapter(new NumericWheelAdapter(1, 30));
            }
        } else {
            // 闰年
            if ((year_num % 4 == 0 && year_num % 100 != 0) || year_num % 400 == 0) {
                if (year_num == dq_year && month_num == dq_month && day_num == dq_day) {
                    wv_day.setAdapter(new NumericWheelAdapter(1, endDay));
                } else {
                    wv_day.setAdapter(new NumericWheelAdapter(1, 29));
                }
            } else {
                if (year_num == dq_year && month_num == dq_month && day_num == dq_day) {
                    wv_day.setAdapter(new NumericWheelAdapter(1, endDay));
                } else {
                    wv_day.setAdapter(new NumericWheelAdapter(1, 28));
                }
            }
        }
        wv_day.setLabel(context.getString(R.string.pickerview_day));
        wv_day.setCurrentItem(day_num - 1);


        wv_hours = view.findViewById(R.id.hour);
        wv_hours.setAdapter(new NumericWheelAdapter(0, 23));
        wv_hours.setLabel(context.getString(R.string.pickerview_hours));// 添加文字
        wv_hours.setCurrentItem(h);

        wv_mins = view.findViewById(R.id.min);
        wv_mins.setAdapter(new NumericWheelAdapter(0, 59));
        wv_mins.setLabel(context.getString(R.string.pickerview_minutes));// 添加文字
        wv_mins.setCurrentItem(m);

        // 添加"年"监听
        OnItemSelectedListener wheelListener_year = new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                year_num = index + startYear;
                int maxItem = 12;
                //年
                if (year_num == dq_year) {
                    wv_month.setAdapter(new NumericWheelAdapter(1, Calendar.getInstance().get(Calendar.MONTH) + 1));
                    maxItem = Calendar.getInstance().get(Calendar.MONTH) + 1;

                } else {
                    wv_month.setAdapter(new NumericWheelAdapter(1, 12));
                }

                if (wv_month.getCurrentItem() > maxItem - 1) {
                    wv_month.setCurrentItem(maxItem - 1);
                }

                //月
                if (year_num == dq_year && month_num == dq_month && endDay > 0) {
                    wv_day.setAdapter(new NumericWheelAdapter(1, endDay));
                } else {
                    if (list_big.contains(String.valueOf(wv_month.getCurrentItem() + 1))) {
                        wv_day.setAdapter(new NumericWheelAdapter(1, 31));
                    } else if (list_little.contains(String.valueOf(wv_month.getCurrentItem() + 1))) {
                        wv_day.setAdapter(new NumericWheelAdapter(1, 30));
                    } else {
                        if ((year_num % 4 == 0 && year_num % 100 != 0) || year_num % 400 == 0) {
                            wv_day.setAdapter(new NumericWheelAdapter(1, 29));
                        } else {
                            wv_day.setAdapter(new NumericWheelAdapter(1, 28));
                        }
                    }
                }

                if (wv_day.getCurrentItem() > maxItem - 1) {
                    wv_day.setCurrentItem(maxItem - 1);
                }

            }
        };
        // 添加"月"监听
        OnItemSelectedListener wheelListener_month = new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                month_num = index + 1;
                int maxItem = 30;
                Log.e(TAG, "year_num == " + year_num);
                Log.e(TAG, "month_num == " + month_num);
                if (dq_year == year_num && dq_month == month_num) {
                    wv_day.setAdapter(new NumericWheelAdapter(1, Calendar.getInstance().get(Calendar.DAY_OF_MONTH)));
                    maxItem = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                } else {
                    // 判断大小月及是否闰年,用来确定"日"的数据
                    if (list_big.contains(String.valueOf(month_num))) {
                        wv_day.setAdapter(new NumericWheelAdapter(1, 31));
                        maxItem = 31;
                    } else if (list_little.contains(String.valueOf(month_num))) {
                        wv_day.setAdapter(new NumericWheelAdapter(1, 30));
                    } else {
                        if (((wv_year.getCurrentItem() + startYear) % 4 == 0 && (wv_year.getCurrentItem() + startYear) % 100 != 0) || (wv_year.getCurrentItem() + startYear) % 400 == 0) {
                            wv_day.setAdapter(new NumericWheelAdapter(1, 29));
                            maxItem = 29;
                        } else {
                            wv_day.setAdapter(new NumericWheelAdapter(1, 28));
                            maxItem = 28;
                        }
                    }
                }
                if (wv_day.getCurrentItem() > maxItem - 1) {
                    wv_day.setCurrentItem(maxItem - 1);
                }
            }
        };
        wv_year.setOnItemSelectedListener(wheelListener_year);
        wv_month.setOnItemSelectedListener(wheelListener_month);

        // 根据屏幕密度来指定选择器字体的大小(不同屏幕可能不同)
        int textSize = 6;
        switch (type) {
            case ALL:
                textSize = textSize * 3;
                break;
            case YEAR_MONTH_DAY:
                textSize = textSize * 4;
                wv_hours.setVisibility(View.GONE);
                wv_mins.setVisibility(View.GONE);
                break;
            case HOURS_MINS:
                textSize = textSize * 4;
                wv_year.setVisibility(View.GONE);
                wv_month.setVisibility(View.GONE);
                wv_day.setVisibility(View.GONE);
                break;
            case MONTH_DAY_HOUR_MIN:
                textSize = textSize * 3;
                wv_year.setVisibility(View.GONE);
                break;
            case YEAR_MONTH:
                textSize = textSize * 4;
                wv_day.setVisibility(View.GONE);
                wv_hours.setVisibility(View.GONE);
                wv_mins.setVisibility(View.GONE);
        }
        wv_day.setTextSize(textSize);
        wv_month.setTextSize(textSize);
        wv_year.setTextSize(textSize);
        wv_hours.setTextSize(textSize);
        wv_mins.setTextSize(textSize);

    }

    /**
     * 设置是否循环滚动
     *
     * @param cyclic
     */
    public void setCyclic(boolean cyclic) {
        wv_year.setCyclic(cyclic);
        wv_month.setCyclic(cyclic);
        wv_day.setCyclic(cyclic);
        wv_hours.setCyclic(cyclic);
        wv_mins.setCyclic(cyclic);
    }

    public String getTime() {
        StringBuffer sb = new StringBuffer();
        sb.append((wv_year.getCurrentItem() + startYear)).append("-").append((wv_month.getCurrentItem() + 1)).append("-").append((wv_day.getCurrentItem() + 1)).append(" ").append(wv_hours.getCurrentItem()).append(":").append(wv_mins.getCurrentItem());
        return sb.toString();
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }
}
