package com.module.commonview.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.view.YueMeiVideoView;

import java.util.List;

public class LGalleryPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<TaoDetailBean.PicBean> imgDatas;
    private List<TaoDetailBean.VideoBean> videoDatas;
    private String mSkuId;
    private String TAG = "LGalleryPagerAdapter";


    public LGalleryPagerAdapter(Context context, List<TaoDetailBean.PicBean> imgDatas, List<TaoDetailBean.VideoBean> videoDatas,String skuId) {
        mContext = context;
        this.imgDatas = imgDatas;
        this.videoDatas = videoDatas;
        this.mSkuId = skuId;
    }

    @Override
    public int getCount() {
        if (videoDatas.size() == 0) {
            return imgDatas.size();
        } else {
            return imgDatas.size() + videoDatas.size();
        }
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        if (videoDatas.size() != 0) {
            if (position == 0) {
                final TaoDetailBean.VideoBean videoBean = videoDatas.get(0);
                final YueMeiVideoView yueMeiVideoView = new YueMeiVideoView(mContext);
                yueMeiVideoView.setTaoId(mSkuId);
                String img = videoBean.getImg();
                final String url = videoBean.getUrl();
                yueMeiVideoView.setNetworkChanges(false);
                Log.e(TAG, "img == " + img);
                yueMeiVideoView.setVideoParameter(img, url);
                container.addView(yueMeiVideoView);

                yueMeiVideoView.setOnMaxVideoClickListener(new YueMeiVideoView.OnMaxVideoClickListener() {
                    @Override
                    public void onMaxVideoClick(View v) {
                        if (onAdapterClickListener != null) {
                            onAdapterClickListener.onMaxVideoClick(yueMeiVideoView, url, yueMeiVideoView.getCurrentPosition());
                        }
                    }
                });

                return yueMeiVideoView;
            } else {
                //43831
                TaoDetailBean.PicBean picBean = imgDatas.get(position - 1);
                ImageView mImageView = new ImageView(mContext);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
                mImageView.setLayoutParams(layoutParams);
                mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                String img = picBean.getImg();
                Log.e(TAG,"img === "+img);
                Glide.with(mContext).load(img).
                        placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .dontAnimate()
                        .into(mImageView);
                container.addView(mImageView);
                return mImageView;
            }
        } else {
            TaoDetailBean.PicBean picBean = imgDatas.get(position);
            ImageView mImageView = new ImageView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
            mImageView.setLayoutParams(layoutParams);
            mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            String img = picBean.getImg();
            Log.e(TAG,"img2 === "+img);
            Glide.with(mContext).load(img).
                    placeholder(R.drawable.home_other_placeholder)
                    .error(R.drawable.home_other_placeholder)
                    .dontAnimate()
                    .into(mImageView);
            container.addView(mImageView);
            return mImageView;
        }

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    //放大视频的监听
    private OnAdapterClickListener onAdapterClickListener;

    public interface OnAdapterClickListener {
        void onMaxVideoClick(YueMeiVideoView videoView, String videoUrl, int currentPosition);
    }

    public void setOnAdapterClickListener(OnAdapterClickListener onAdapterClickListener) {
        this.onAdapterClickListener = onAdapterClickListener;
    }
}
