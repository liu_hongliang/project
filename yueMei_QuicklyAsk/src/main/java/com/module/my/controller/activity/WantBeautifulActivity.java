package com.module.my.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.view.ElasticScrollView;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;

/**
 * 我想变美 页
 *
 * @author Rubin
 */
public class WantBeautifulActivity extends BaseActivity {

    private final String TAG = "WantBeautifulActivity";

    @BindView(id = R.id.wan_beautifu_web_det_scrollview1, click = true)
    private ElasticScrollView scollwebView;
    @BindView(id = R.id.wan_beautifu_linearlayout, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.want_beautiful_top)
    private CommonTopBar mTop;// 返回

    private WebView docDetWeb;

    private WantBeautifulActivity mContex;

    private String po;
    private String hosid;
    private String docid;
    private String shareid;
    private String uid;
    private String cityId;
    private String partId;
    public JSONObject obj_http;
    private BaseWebViewClientMessage baseWebViewClientMessage;

    @Override
    public void setRootView() {
        setContentView(R.layout.web_acty_want_beautiful);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = WantBeautifulActivity.this;
        uid = Utils.getUid();

        mTop.setCenterText("在线留言");

        Intent it0 = getIntent();
        po = it0.getStringExtra("po");
        hosid = it0.getStringExtra("hosid");
        docid = it0.getStringExtra("docid");
        shareid = it0.getStringExtra("shareid");
        cityId = it0.getStringExtra("cityId");
        partId = it0.getStringExtra("partId");

        String urlStr = po + "/hosid/" + hosid + "/docid/" + docid
                + "/shareid/" + shareid + "/cityId/" + cityId + "/partId/"
                + partId + "/";

        scollwebView.GetLinearLayout(contentWeb);
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String urlStr) {
                showWebDetail(urlStr);
            }
        });

        initWebview();

        LodUrl1(urlStr);

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        WantBeautifulActivity.this.finish();
                    }
                });
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT));
        contentWeb.addView(docDetWeb);
    }

    protected void OnReceiveData(String str) {
        scollwebView.onRefreshComplete();
    }

    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }

    /**
     * 处理webview里面的按钮
     *
     * @param urlStr
     */
    public void showWebDetail(String urlStr) {
        Log.e(TAG, urlStr);
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;
            obj_http = obj;

            if (obj.getString("type").equals("1")) {// 返回键
                finish();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        baseWebViewClientMessage.startLoading();

        String tyUrl = FinalConstant.WANTBEAUTIFUL;

        HashMap<String, Object> urlMap = new HashMap<>();
        urlMap.put("group", "0");
        urlMap.put("po", urlstr);

        WebSignData addressAndHead = SignUtils.getAddressAndHead(tyUrl);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

    }

    /**
     * 加载web
     */
    public void LodUrl2(String po) {
        baseWebViewClientMessage.startLoading();

        String tyUrl = FinalConstant.WANTBEAUTIFUL + "/po/" + po + "/"
                + Utils.getTokenStr();

        docDetWeb.loadUrl(tyUrl);
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
