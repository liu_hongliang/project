package com.module.commonview.module.bean;

import java.util.List;

/**
 * 帖子列表数据
 * Created by 裴成浩 on 2018/8/15.
 */
public class PostListData2{

    /**
     * pic : [{"img":"https://p31.yuemei.com/postimg/20191231/500_500/20191231152852_6a3800.jpg","width":"1080","height":"1920","video_url":"","is_video":"0"}]
     * userdata : {"name":"悦Mer_576047","id":"88631712","avatar":"https://p21.yuemei.com/avatar/088/63/17/12_avatar_120_120.jpg","talent":"0","group_id":"1","lable":"沈阳 刚刚","url":"https://m.yuemei.com/u/home/88631712/","hospital_id":"0","doctor_id":"0","is_rongyun":"0","hos_userid":"0","obj_id":"88631712","obj_type":"6","title":"","cityName":"","hospitalName":"","is_following":"3"}
     * title : hkggvvhh
     * id : 7973548
     * content : [{"text":{"content":"<p>ggvfvtvtvtvtvgvgvgvg<\/p>","url":""}}]
     * taolist : [{"sale_type":2,"appmurl":"https://m.yuemei.com/tao/19895/","coupon_type":0,"is_show_member":"0","pay_dingjin":"100","number":"2","start_number":"1","pay_price_discount":"289","member_price":"-1","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191227095250_771.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220232254_ec199a.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220232249_95f748.jpg","title":"除皱针","subtitle":"衡力除皱（热销13000+）北医三院医学博士注射 高纯度保证疗效 除皱性价比之王 现场可查验","hos_name":"悦美好医医疗美容门诊部","doc_name":"张利民","price":"1580","price_discount":"289","price_range_max":"0","id":"19895","_id":"19895","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191227095232_907.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191227095239_405.png","repayment":"","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"13522人预订","rateNum":13522,"service":"5.0","feeScale":"/部位","is_fanxian":"0","hospital_id":"10647","doctor_id":"86345464","is_rongyun":"3","hos_userid":"700893","business_district":"","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/086/00/31/58_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/00/78/86_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/01/05/77_avatar_50_50.jpg"],"surgInfo":{"id":"8863","title":"注射除皱","name":"注射除皱","alias":"","url_name":"roudusu"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E9%99%A4%E7%9A%B1%E9%92%88%E3%80%91%E8%A1%A1%E5%8A%9B%E9%99%A4%E7%9A%B1%EF%BC%88%E7%83%AD%E9%94%8013000%2B%EF%BC%89%E5%8C%97%E5%8C%BB%E4%B8%89%E9%99%A2%E5%8C%BB%E5%AD%A6%E5%8D%9A%E5%A3%AB%E6%B3%A8%E5%B0%84%20%E9%AB%98%E7%BA%AF%E5%BA%A6%E4%BF%9D%E8%AF%81%E7%96%97%E6%95%88%20%E9%99%A4%E7%9A%B1%E6%80%A7%E4%BB%B7%E6%AF%94%E4%B9%8B%E7%8E%8B%20%E7%8E%B0%E5%9C%BA%E5%8F%AF%E6%9F%A5%E9%AA%8C%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":19895,"event_others":0}},{"sale_type":2,"appmurl":"https://m.yuemei.com/tao/37817/","coupon_type":0,"is_show_member":"0","pay_dingjin":"200","number":"2","start_number":"1","pay_price_discount":"890","member_price":"-1","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191227095250_771.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220232709_d090a9.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220232705_996e88.jpg","title":"注射微整","subtitle":"伊婉C玻尿酸（热销13000+）北医三院医学博士注射 轻柔舒适 填苹果肌/法令纹/面颊/额头","hos_name":"悦美好医医疗美容门诊部","doc_name":"张利民","price":"3980","price_discount":"890","price_range_max":"0","id":"37817","_id":"37817","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191227095232_907.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191227095239_405.png","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"13279人预订","rateNum":13279,"service":"5.0","feeScale":"/支(1ml)","is_fanxian":"1","hospital_id":"10647","doctor_id":"86345464","is_rongyun":"3","hos_userid":"700893","business_district":"","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/25/42/05_avatar_50_50.jpg","https://www.yuemei.com/images/weibo/noavatar3_50_50.jpg","https://www.yuemei.com/images/weibo/noavatar3_50_50.jpg"],"surgInfo":{"id":"18157","title":"填充塑形","name":"填充塑形","alias":"填充塑形","url_name":"fill"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%B3%A8%E5%B0%84%E5%BE%AE%E6%95%B4%E3%80%91%E4%BC%8A%E5%A9%89C%E7%8E%BB%E5%B0%BF%E9%85%B8%EF%BC%88%E7%83%AD%E9%94%8013000%2B%EF%BC%89%E5%8C%97%E5%8C%BB%E4%B8%89%E9%99%A2%E5%8C%BB%E5%AD%A6%E5%8D%9A%E5%A3%AB%E6%B3%A8%E5%B0%84%20%E8%BD%BB%E6%9F%94%E8%88%92%E9%80%82%20%E5%A1%AB%E8%8B%B9%E6%9E%9C%E8%82%8C%2F%E6%B3%95%E4%BB%A4%E7%BA%B9%2F%E9%9D%A2%E9%A2%8A%2F%E9%A2%9D%E5%A4%B4%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":37817,"event_others":0}},{"sale_type":2,"appmurl":"https://m.yuemei.com/tao/67289/","coupon_type":0,"is_show_member":"0","pay_dingjin":"200","number":"1","start_number":"1","pay_price_discount":"580","member_price":"-1","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191227095250_771.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220233453_db36a8.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220233449_9ee6d7.jpg","title":"美国M22光子嫩肤","subtitle":"光子嫩肤（销量TOP1）M22王者之心 美白嫩肤 祛黄祛暗 赠面膜","hos_name":"悦美好医医疗美容门诊部","doc_name":"","price":"3800","price_discount":"580","price_range_max":"0","id":"67289","_id":"67289","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191227095232_907.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191227095239_405.png","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"12136人预订","rateNum":12136,"service":"5.0","feeScale":"/次","is_fanxian":"1","hospital_id":"10647","doctor_id":"","is_rongyun":"3","hos_userid":"700893","business_district":"","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[{"style_type":"1","title":"1小时前有人购买"}],"userImg":["https://p21.yuemei.com/avatar/085/11/78/25_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/78/35_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/78/43_avatar_50_50.jpg"],"surgInfo":{"id":"542","title":"美白嫩肤","name":"美白嫩肤","alias":"美容美白、全身美白","url_name":"skinwhiten"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E7%BE%8E%E5%9B%BDM22%E5%85%89%E5%AD%90%E5%AB%A9%E8%82%A4%E3%80%91%E5%85%89%E5%AD%90%E5%AB%A9%E8%82%A4%EF%BC%88%E9%94%80%E9%87%8FTOP1%EF%BC%89M22%E7%8E%8B%E8%80%85%E4%B9%8B%E5%BF%83%20%E7%BE%8E%E7%99%BD%E5%AB%A9%E8%82%A4%20%E7%A5%9B%E9%BB%84%E7%A5%9B%E6%9A%97%20%E8%B5%A0%E9%9D%A2%E8%86%9C%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":67289,"event_others":0}},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/51459/","coupon_type":0,"is_show_member":"0","pay_dingjin":"200","number":"20","start_number":"1","pay_price_discount":"489","member_price":"-1","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191227095250_771.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220232845_d3809a.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220232838_6645bc.jpg","title":"水光针","subtitle":"菁芙水光针（热销11000+）深层补水亮肤 赠医用面膜 现场可拆验","hos_name":"悦美好医医疗美容门诊部","doc_name":"","price":"1280","price_discount":"489","price_range_max":"0","id":"51459","_id":"51459","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191227095232_907.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191227095239_405.png","repayment":"","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"11554人预订","rateNum":11554,"service":"4.9","feeScale":"/次","is_fanxian":"0","hospital_id":"10647","doctor_id":"","is_rongyun":"3","hos_userid":"700893","business_district":"","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/086/00/31/58_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/00/78/86_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/01/05/77_avatar_50_50.jpg"],"surgInfo":{"id":"10615","title":"补水保湿","name":"补水保湿","alias":"补水保湿","url_name":"bsbs1"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%B0%B4%E5%85%89%E9%92%88%E3%80%91%E8%8F%81%E8%8A%99%E6%B0%B4%E5%85%89%E9%92%88%EF%BC%88%E7%83%AD%E9%94%8011000%2B%EF%BC%89%E6%B7%B1%E5%B1%82%E8%A1%A5%E6%B0%B4%E4%BA%AE%E8%82%A4%20%E8%B5%A0%E5%8C%BB%E7%94%A8%E9%9D%A2%E8%86%9C%20%E7%8E%B0%E5%9C%BA%E5%8F%AF%E6%8B%86%E9%AA%8C%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":51459,"event_others":0}},{"sale_type":1,"appmurl":"https://m.yuemei.com/tao/15165/","coupon_type":0,"is_show_member":"1","pay_dingjin":"100","number":"1","start_number":"1","pay_price_discount":"198","member_price":"188","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191227095250_771.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1214/200_200/jt191214132702_6972f1.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1214/jt191214132702_6972f1.jpg","title":"润百颜玻尿酸","subtitle":"成都军大润百颜玻尿酸 1ml 精微填充 小分子自然 泪沟/法令纹","hos_name":"成都军大医院","doc_name":"张作香","price":"1980","price_discount":"198","price_range_max":"0","id":"15165","_id":"15165","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191227095232_907.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191227095239_405.png","repayment":"","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"11193人预订","rateNum":11193,"service":"4.8","feeScale":"/支(1ml)","is_fanxian":"0","hospital_id":"7859","doctor_id":"509043","is_rongyun":"3","hos_userid":"657133","business_district":"","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/00/39/51_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/00/40/03_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/00/59/97_avatar_50_50.jpg"],"surgInfo":{"id":"18157","title":"填充塑形","name":"填充塑形","alias":"填充塑形","url_name":"fill"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%B6%A6%E7%99%BE%E9%A2%9C%E7%8E%BB%E5%B0%BF%E9%85%B8%E3%80%91%E6%88%90%E9%83%BD%E5%86%9B%E5%A4%A7%E6%B6%A6%E7%99%BE%E9%A2%9C%E7%8E%BB%E5%B0%BF%E9%85%B8%201ml%20%E7%B2%BE%E5%BE%AE%E5%A1%AB%E5%85%85%20%E5%B0%8F%E5%88%86%E5%AD%90%E8%87%AA%E7%84%B6%20%E6%B3%AA%E6%B2%9F%2F%E6%B3%95%E4%BB%A4%E7%BA%B9%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":15165,"event_others":0}},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/21019/","coupon_type":0,"is_show_member":"1","pay_dingjin":"99","number":"20","start_number":"1","pay_price_discount":"99","member_price":"94","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1223/200_200/jt191223111822_eb8ffe.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1223/jt191223111822_eb8ffe.jpg","title":"注射微整","subtitle":"（大家都在买）衡力 国产肉毒素 衡力除皱针 可拆验 足量原装","hos_name":"北京凯润婷医疗美容医院","doc_name":"赵洋","price":"980","price_discount":"99","price_range_max":"0","id":"21019","_id":"21019","showprice":"1","specialPrice":"1","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"11158人预订","rateNum":11158,"service":"4.9","feeScale":"/部位","is_fanxian":"0","hospital_id":"3345","doctor_id":"528213","is_rongyun":"3","hos_userid":"86056009","business_district":"","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/43/05/03_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/43/05/61_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/44/40/53_avatar_50_50.jpg"],"surgInfo":{"id":"8863","title":"注射除皱","name":"注射除皱","alias":"","url_name":"roudusu"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%B3%A8%E5%B0%84%E5%BE%AE%E6%95%B4%E3%80%91%EF%BC%88%E5%A4%A7%E5%AE%B6%E9%83%BD%E5%9C%A8%E4%B9%B0%EF%BC%89%E8%A1%A1%E5%8A%9B%20%E5%9B%BD%E4%BA%A7%E8%82%89%E6%AF%92%E7%B4%A0%20%E8%A1%A1%E5%8A%9B%E9%99%A4%E7%9A%B1%E9%92%88%20%E5%8F%AF%E6%8B%86%E9%AA%8C%20%E8%B6%B3%E9%87%8F%E5%8E%9F%E8%A3%85%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":21019,"event_others":0}},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/21291/","coupon_type":0,"is_show_member":"1","pay_dingjin":"140","number":"20","start_number":"1","pay_price_discount":"680","member_price":"646","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1223/200_200/jt191223112224_0c77f7.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1223/jt191223112224_0c77f7.jpg","title":"注射微整","subtitle":"（大家都在买）月销10000+伊婉C   进口玻尿酸 可拆验 足量原装","hos_name":"北京凯润婷医疗美容医院","doc_name":"赵洋","price":"2980","price_discount":"680","price_range_max":"0","id":"21291","_id":"21291","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"满10000减1000","hos_red_packet":"满100减20,满1000减100,满3000减200,满5000减400,满10000减1000","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"11131人预订","rateNum":11131,"service":"4.8","feeScale":"/次","is_fanxian":"0","hospital_id":"3345","doctor_id":"528213","is_rongyun":"3","hos_userid":"86056009","business_district":"","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/29/65/61_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/29/67/35_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/29/67/63_avatar_50_50.jpg"],"surgInfo":{"id":"18157","title":"填充塑形","name":"填充塑形","alias":"填充塑形","url_name":"fill"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%B3%A8%E5%B0%84%E5%BE%AE%E6%95%B4%E3%80%91%EF%BC%88%E5%A4%A7%E5%AE%B6%E9%83%BD%E5%9C%A8%E4%B9%B0%EF%BC%89%E6%9C%88%E9%94%8010000%2B%E4%BC%8A%E5%A9%89C%20%20%20%E8%BF%9B%E5%8F%A3%E7%8E%BB%E5%B0%BF%E9%85%B8%20%E5%8F%AF%E6%8B%86%E9%AA%8C%20%E8%B6%B3%E9%87%8F%E5%8E%9F%E8%A3%85%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":21291,"event_others":0}},{"sale_type":1,"appmurl":"https://m.yuemei.com/tao/203614/","coupon_type":0,"is_show_member":"1","pay_dingjin":"110","number":"8","start_number":"1","pay_price_discount":"528","member_price":"501","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191227095250_771.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1122/200_200/jt191122201632_05f27c.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1122/jt191122201632_05f27c.jpg","title":"吸脂瘦脸","subtitle":"30余年（双博士）团队14项专利面部吸脂瘦脸/专利下面部吸脂瘦下颌缘吸脂双下巴","hos_name":"北京润美玉之光医疗美容门诊部","doc_name":"朱金成","price":"1980","price_discount":"528","price_range_max":"0","id":"203614","_id":"203614","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191227095232_907.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191227095239_405.png","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"满10000减500","hos_red_packet":"满1000减50,满3000减150,满10000减500","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"11130人预订","rateNum":11130,"service":"5.0","feeScale":"/部位","is_fanxian":"0","hospital_id":"3354","doctor_id":"86373205","is_rongyun":"3","hos_userid":"85280507","business_district":"","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/32/46/87_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/33/54/93_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/34/06/35_avatar_50_50.jpg"],"surgInfo":{"id":"329","title":"瘦脸","name":"瘦脸","alias":"","url_name":"faceslimm"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E5%90%B8%E8%84%82%E7%98%A6%E8%84%B8%E3%80%9130%E4%BD%99%E5%B9%B4%EF%BC%88%E5%8F%8C%E5%8D%9A%E5%A3%AB%EF%BC%89%E5%9B%A2%E9%98%9F14%E9%A1%B9%E4%B8%93%E5%88%A9%E9%9D%A2%E9%83%A8%E5%90%B8%E8%84%82%E7%98%A6%E8%84%B8%2F%E4%B8%93%E5%88%A9%E4%B8%8B%E9%9D%A2%E9%83%A8%E5%90%B8%E8%84%82%E7%98%A6%E4%B8%8B%E9%A2%8C%E7%BC%98%E5%90%B8%E8%84%82%E5%8F%8C%E4%B8%8B%E5%B7%B4%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":203614,"event_others":0}}]
     * tag : []
     * p_id : 7973548
     * askorshare : 12
     * agree_num : 0
     * answer_num : 0
     * collect_num : 0
     * is_agree : 0
     * is_collect : 0
     * replyList : []
     * other_post : []
     * other_question : []
     * taoDataList : []
     * contentSkuList : []
     * baoming_is_end : 1
     * moban : 0
     * baoming : {"show_title":"报名结束","alert_title":"报名已结束","baoming_is_end":"1"}
     * vote : 1
     * vote_is_end : 1
     * ask_entry : {"url":"type:eq:6651","img":"https://p1.yuemei.com/tag/1554344360dcd30.png","img_width":"750","img_height":"200"}
     * loadHtmlUrl :
     * shareClickData : {"event_name":null,"event_pos":"post","id":"7973548","type":15}
     * followClickData : {"event_name":"_follow","event_pos":"post","id":"7973548","type":15}
     * baomingClickData : {"event_name":"baoming","event_pos":1,"id":"7973548","type":15}
     * selfPageType : 15
     * loadZtUrl : https://m.yuemei.com/tao_zt/9439.html
     * vote_list : {"id":"38","post_id":"7973548","vote_title":"ggbnnnjj","vote_type":"2","option":[{"id":"278","vote_num":"0","tao_id":"96226"},{"id":"279","vote_num":"0","tao_id":"133351"},{"id":"280","vote_num":"0","tao_id":"173383"}],"is_vote_option":"0"}
     * chatData : {"ymaq_class":"15","ymaq_id":"7973548","is_rongyun":"3","hos_userid":"86056009","event_name":"chat_hospital","event_pos":"post_bottom_1","event_params":{"hos_id":"0","doc_id":"0","tao_id":"","event_others":"0","id":"7973548","type":15},"show_message":"","guiji":{"title":"hkggvvhh","image":"https://p31.yuemei.com/postimg/20191231/500_500/20191231152852_6a3800.jpg","url":"https://m.yuemei.com/p/7973548.html","price":"","member_price":"-1"},"obj_type":"5","obj_id":"7973548"}
     */

    private UserdataBean userdata;
    private String title;
    private String id;
    private String p_id;
    private String askorshare;
    private String agree_num;
    private String answer_num;
    private String collect_num;
    private String is_agree;
    private String is_collect;
    private String baoming_is_end;
    private String moban;
    private BaomingBean baoming;
    private int vote;
    private String vote_is_end;
    private AskEntryBean ask_entry;
    private String loadHtmlUrl;
    private ShareClickDataBean shareClickData;
    private FollowClickDataBean followClickData;
    private BaomingClickDataBean baomingClickData;
    private String selfPageType;
    private String loadZtUrl;
    private VoteListBean vote_list;
    private ChatDataBean chatData;
    private List<PicBean> pic;
    private List<ContentBean> content;
    private List<TaolistBean> taolist;
    private List<?> tag;
    private List<?> replyList;
    private List<?> other_post;
    private List<?> other_question;
    private List<?> taoDataList;
    private List<?> contentSkuList;

    public UserdataBean getUserdata() {
        return userdata;
    }

    public void setUserdata(UserdataBean userdata) {
        this.userdata = userdata;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getAskorshare() {
        return askorshare;
    }

    public void setAskorshare(String askorshare) {
        this.askorshare = askorshare;
    }

    public String getAgree_num() {
        return agree_num;
    }

    public void setAgree_num(String agree_num) {
        this.agree_num = agree_num;
    }

    public String getAnswer_num() {
        return answer_num;
    }

    public void setAnswer_num(String answer_num) {
        this.answer_num = answer_num;
    }

    public String getCollect_num() {
        return collect_num;
    }

    public void setCollect_num(String collect_num) {
        this.collect_num = collect_num;
    }

    public String getIs_agree() {
        return is_agree;
    }

    public void setIs_agree(String is_agree) {
        this.is_agree = is_agree;
    }

    public String getIs_collect() {
        return is_collect;
    }

    public void setIs_collect(String is_collect) {
        this.is_collect = is_collect;
    }

    public String getBaoming_is_end() {
        return baoming_is_end;
    }

    public void setBaoming_is_end(String baoming_is_end) {
        this.baoming_is_end = baoming_is_end;
    }

    public String getMoban() {
        return moban;
    }

    public void setMoban(String moban) {
        this.moban = moban;
    }

    public BaomingBean getBaoming() {
        return baoming;
    }

    public void setBaoming(BaomingBean baoming) {
        this.baoming = baoming;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    public String getVote_is_end() {
        return vote_is_end;
    }

    public void setVote_is_end(String vote_is_end) {
        this.vote_is_end = vote_is_end;
    }

    public AskEntryBean getAsk_entry() {
        return ask_entry;
    }

    public void setAsk_entry(AskEntryBean ask_entry) {
        this.ask_entry = ask_entry;
    }

    public String getLoadHtmlUrl() {
        return loadHtmlUrl;
    }

    public void setLoadHtmlUrl(String loadHtmlUrl) {
        this.loadHtmlUrl = loadHtmlUrl;
    }

    public ShareClickDataBean getShareClickData() {
        return shareClickData;
    }

    public void setShareClickData(ShareClickDataBean shareClickData) {
        this.shareClickData = shareClickData;
    }

    public FollowClickDataBean getFollowClickData() {
        return followClickData;
    }

    public void setFollowClickData(FollowClickDataBean followClickData) {
        this.followClickData = followClickData;
    }

    public BaomingClickDataBean getBaomingClickData() {
        return baomingClickData;
    }

    public void setBaomingClickData(BaomingClickDataBean baomingClickData) {
        this.baomingClickData = baomingClickData;
    }

    public String getSelfPageType() {
        return selfPageType;
    }

    public void setSelfPageType(String selfPageType) {
        this.selfPageType = selfPageType;
    }

    public String getLoadZtUrl() {
        return loadZtUrl;
    }

    public void setLoadZtUrl(String loadZtUrl) {
        this.loadZtUrl = loadZtUrl;
    }

    public VoteListBean getVote_list() {
        return vote_list;
    }

    public void setVote_list(VoteListBean vote_list) {
        this.vote_list = vote_list;
    }

    public ChatDataBean getChatData() {
        return chatData;
    }

    public void setChatData(ChatDataBean chatData) {
        this.chatData = chatData;
    }

    public List<PicBean> getPic() {
        return pic;
    }

    public void setPic(List<PicBean> pic) {
        this.pic = pic;
    }

    public List<ContentBean> getContent() {
        return content;
    }

    public void setContent(List<ContentBean> content) {
        this.content = content;
    }

    public List<TaolistBean> getTaolist() {
        return taolist;
    }

    public void setTaolist(List<TaolistBean> taolist) {
        this.taolist = taolist;
    }

    public List<?> getTag() {
        return tag;
    }

    public void setTag(List<?> tag) {
        this.tag = tag;
    }

    public List<?> getReplyList() {
        return replyList;
    }

    public void setReplyList(List<?> replyList) {
        this.replyList = replyList;
    }

    public List<?> getOther_post() {
        return other_post;
    }

    public void setOther_post(List<?> other_post) {
        this.other_post = other_post;
    }

    public List<?> getOther_question() {
        return other_question;
    }

    public void setOther_question(List<?> other_question) {
        this.other_question = other_question;
    }

    public List<?> getTaoDataList() {
        return taoDataList;
    }

    public void setTaoDataList(List<?> taoDataList) {
        this.taoDataList = taoDataList;
    }

    public List<?> getContentSkuList() {
        return contentSkuList;
    }

    public void setContentSkuList(List<?> contentSkuList) {
        this.contentSkuList = contentSkuList;
    }

    public static class UserdataBean {
        /**
         * name : 悦Mer_576047
         * id : 88631712
         * avatar : https://p21.yuemei.com/avatar/088/63/17/12_avatar_120_120.jpg
         * talent : 0
         * group_id : 1
         * lable : 沈阳 刚刚
         * url : https://m.yuemei.com/u/home/88631712/
         * hospital_id : 0
         * doctor_id : 0
         * is_rongyun : 0
         * hos_userid : 0
         * obj_id : 88631712
         * obj_type : 6
         * title :
         * cityName :
         * hospitalName :
         * is_following : 3
         */

        private String name;
        private String id;
        private String avatar;
        private String talent;
        private String group_id;
        private String lable;
        private String url;
        private String hospital_id;
        private String doctor_id;
        private String is_rongyun;
        private String hos_userid;
        private String obj_id;
        private String obj_type;
        private String title;
        private String cityName;
        private String hospitalName;
        private String is_following;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getTalent() {
            return talent;
        }

        public void setTalent(String talent) {
            this.talent = talent;
        }

        public String getGroup_id() {
            return group_id;
        }

        public void setGroup_id(String group_id) {
            this.group_id = group_id;
        }

        public String getLable() {
            return lable;
        }

        public void setLable(String lable) {
            this.lable = lable;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getHospital_id() {
            return hospital_id;
        }

        public void setHospital_id(String hospital_id) {
            this.hospital_id = hospital_id;
        }

        public String getDoctor_id() {
            return doctor_id;
        }

        public void setDoctor_id(String doctor_id) {
            this.doctor_id = doctor_id;
        }

        public String getIs_rongyun() {
            return is_rongyun;
        }

        public void setIs_rongyun(String is_rongyun) {
            this.is_rongyun = is_rongyun;
        }

        public String getHos_userid() {
            return hos_userid;
        }

        public void setHos_userid(String hos_userid) {
            this.hos_userid = hos_userid;
        }

        public String getObj_id() {
            return obj_id;
        }

        public void setObj_id(String obj_id) {
            this.obj_id = obj_id;
        }

        public String getObj_type() {
            return obj_type;
        }

        public void setObj_type(String obj_type) {
            this.obj_type = obj_type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getHospitalName() {
            return hospitalName;
        }

        public void setHospitalName(String hospitalName) {
            this.hospitalName = hospitalName;
        }

        public String getIs_following() {
            return is_following;
        }

        public void setIs_following(String is_following) {
            this.is_following = is_following;
        }
    }

    public static class BaomingBean {
        /**
         * show_title : 报名结束
         * alert_title : 报名已结束
         * baoming_is_end : 1
         */

        private String show_title;
        private String alert_title;
        private String baoming_is_end;

        public String getShow_title() {
            return show_title;
        }

        public void setShow_title(String show_title) {
            this.show_title = show_title;
        }

        public String getAlert_title() {
            return alert_title;
        }

        public void setAlert_title(String alert_title) {
            this.alert_title = alert_title;
        }

        public String getBaoming_is_end() {
            return baoming_is_end;
        }

        public void setBaoming_is_end(String baoming_is_end) {
            this.baoming_is_end = baoming_is_end;
        }
    }

    public static class AskEntryBean {
        /**
         * url : type:eq:6651
         * img : https://p1.yuemei.com/tag/1554344360dcd30.png
         * img_width : 750
         * img_height : 200
         */

        private String url;
        private String img;
        private String img_width;
        private String img_height;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getImg_width() {
            return img_width;
        }

        public void setImg_width(String img_width) {
            this.img_width = img_width;
        }

        public String getImg_height() {
            return img_height;
        }

        public void setImg_height(String img_height) {
            this.img_height = img_height;
        }
    }

    public static class ShareClickDataBean {
        /**
         * event_name : null
         * event_pos : post
         * id : 7973548
         * type : 15
         */

        private Object event_name;
        private String event_pos;
        private String id;
        private int type;

        public Object getEvent_name() {
            return event_name;
        }

        public void setEvent_name(Object event_name) {
            this.event_name = event_name;
        }

        public String getEvent_pos() {
            return event_pos;
        }

        public void setEvent_pos(String event_pos) {
            this.event_pos = event_pos;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }

    public static class FollowClickDataBean {
        /**
         * event_name : _follow
         * event_pos : post
         * id : 7973548
         * type : 15
         */

        private String event_name;
        private String event_pos;
        private String id;
        private int type;

        public String getEvent_name() {
            return event_name;
        }

        public void setEvent_name(String event_name) {
            this.event_name = event_name;
        }

        public String getEvent_pos() {
            return event_pos;
        }

        public void setEvent_pos(String event_pos) {
            this.event_pos = event_pos;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }

    public static class BaomingClickDataBean {
        /**
         * event_name : baoming
         * event_pos : 1
         * id : 7973548
         * type : 15
         */

        private String event_name;
        private int event_pos;
        private String id;
        private int type;

        public String getEvent_name() {
            return event_name;
        }

        public void setEvent_name(String event_name) {
            this.event_name = event_name;
        }

        public int getEvent_pos() {
            return event_pos;
        }

        public void setEvent_pos(int event_pos) {
            this.event_pos = event_pos;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }

    public static class VoteListBean {
        /**
         * id : 38
         * post_id : 7973548
         * vote_title : ggbnnnjj
         * vote_type : 2
         * option : [{"id":"278","vote_num":"0","tao_id":"96226"},{"id":"279","vote_num":"0","tao_id":"133351"},{"id":"280","vote_num":"0","tao_id":"173383"}]
         * is_vote_option : 0
         */

        private String id;
        private String post_id;
        private String vote_title;
        private String vote_type;
        private String is_vote_option;
        private List<OptionBean> option;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPost_id() {
            return post_id;
        }

        public void setPost_id(String post_id) {
            this.post_id = post_id;
        }

        public String getVote_title() {
            return vote_title;
        }

        public void setVote_title(String vote_title) {
            this.vote_title = vote_title;
        }

        public String getVote_type() {
            return vote_type;
        }

        public void setVote_type(String vote_type) {
            this.vote_type = vote_type;
        }

        public String getIs_vote_option() {
            return is_vote_option;
        }

        public void setIs_vote_option(String is_vote_option) {
            this.is_vote_option = is_vote_option;
        }

        public List<OptionBean> getOption() {
            return option;
        }

        public void setOption(List<OptionBean> option) {
            this.option = option;
        }

        public static class OptionBean {
            /**
             * id : 278
             * vote_num : 0
             * tao_id : 96226
             */

            private String id;
            private String vote_num;
            private String tao_id;
            private String vote_option_rate;
            private String is_vote_option;
            private List<TaoBeanVote> tao;

            public List<TaoBeanVote> getTao() {
                return tao;
            }

            public void setTao(List<TaoBeanVote> tao) {
                this.tao = tao;
            }

            public String getVote_option_rate() {
                return vote_option_rate;
            }

            public void setVote_option_rate(String vote_option_rate) {
                this.vote_option_rate = vote_option_rate;
            }

            public String getIs_vote_option() {
                return is_vote_option;
            }

            public void setIs_vote_option(String is_vote_option) {
                this.is_vote_option = is_vote_option;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getVote_num() {
                return vote_num;
            }

            public void setVote_num(String vote_num) {
                this.vote_num = vote_num;
            }

            public String getTao_id() {
                return tao_id;
            }

            public void setTao_id(String tao_id) {
                this.tao_id = tao_id;
            }
        }
    }

    public static class TaoBeanVote {
        private String id;
        private String sale_price;
        private String lable;
        private String title;
        private String list_cover_image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSale_price() {
            return sale_price;
        }

        public void setSale_price(String sale_price) {
            this.sale_price = sale_price;
        }

        public String getLable() {
            return lable;
        }

        public void setLable(String lable) {
            this.lable = lable;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getList_cover_image() {
            return list_cover_image;
        }

        public void setList_cover_image(String list_cover_image) {
            this.list_cover_image = list_cover_image;
        }
    }

    public static class ChatDataBean {
        /**
         * ymaq_class : 15
         * ymaq_id : 7973548
         * is_rongyun : 3
         * hos_userid : 86056009
         * event_name : chat_hospital
         * event_pos : post_bottom_1
         * event_params : {"hos_id":"0","doc_id":"0","tao_id":"","event_others":"0","id":"7973548","type":15}
         * show_message :
         * guiji : {"title":"hkggvvhh","image":"https://p31.yuemei.com/postimg/20191231/500_500/20191231152852_6a3800.jpg","url":"https://m.yuemei.com/p/7973548.html","price":"","member_price":"-1"}
         * obj_type : 5
         * obj_id : 7973548
         */

        private String ymaq_class;
        private String ymaq_id;
        private String is_rongyun;
        private String hos_userid;
        private String event_name;
        private String event_pos;
        private EventParamsBean event_params;
        private String show_message;
        private GuijiBean guiji;
        private String obj_type;
        private String obj_id;

        public String getYmaq_class() {
            return ymaq_class;
        }

        public void setYmaq_class(String ymaq_class) {
            this.ymaq_class = ymaq_class;
        }

        public String getYmaq_id() {
            return ymaq_id;
        }

        public void setYmaq_id(String ymaq_id) {
            this.ymaq_id = ymaq_id;
        }

        public String getIs_rongyun() {
            return is_rongyun;
        }

        public void setIs_rongyun(String is_rongyun) {
            this.is_rongyun = is_rongyun;
        }

        public String getHos_userid() {
            return hos_userid;
        }

        public void setHos_userid(String hos_userid) {
            this.hos_userid = hos_userid;
        }

        public String getEvent_name() {
            return event_name;
        }

        public void setEvent_name(String event_name) {
            this.event_name = event_name;
        }

        public String getEvent_pos() {
            return event_pos;
        }

        public void setEvent_pos(String event_pos) {
            this.event_pos = event_pos;
        }

        public EventParamsBean getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBean event_params) {
            this.event_params = event_params;
        }

        public String getShow_message() {
            return show_message;
        }

        public void setShow_message(String show_message) {
            this.show_message = show_message;
        }

        public GuijiBean getGuiji() {
            return guiji;
        }

        public void setGuiji(GuijiBean guiji) {
            this.guiji = guiji;
        }

        public String getObj_type() {
            return obj_type;
        }

        public void setObj_type(String obj_type) {
            this.obj_type = obj_type;
        }

        public String getObj_id() {
            return obj_id;
        }

        public void setObj_id(String obj_id) {
            this.obj_id = obj_id;
        }

        public static class EventParamsBean {
            /**
             * hos_id : 0
             * doc_id : 0
             * tao_id :
             * event_others : 0
             * id : 7973548
             * type : 15
             */

            private String hos_id;
            private String doc_id;
            private String tao_id;
            private String event_others;
            private String id;
            private int type;

            public String getHos_id() {
                return hos_id;
            }

            public void setHos_id(String hos_id) {
                this.hos_id = hos_id;
            }

            public String getDoc_id() {
                return doc_id;
            }

            public void setDoc_id(String doc_id) {
                this.doc_id = doc_id;
            }

            public String getTao_id() {
                return tao_id;
            }

            public void setTao_id(String tao_id) {
                this.tao_id = tao_id;
            }

            public String getEvent_others() {
                return event_others;
            }

            public void setEvent_others(String event_others) {
                this.event_others = event_others;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }
        }

        public static class GuijiBean {
            /**
             * title : hkggvvhh
             * image : https://p31.yuemei.com/postimg/20191231/500_500/20191231152852_6a3800.jpg
             * url : https://m.yuemei.com/p/7973548.html
             * price :
             * member_price : -1
             */

            private String title;
            private String image;
            private String url;
            private String price;
            private String member_price;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getMember_price() {
                return member_price;
            }

            public void setMember_price(String member_price) {
                this.member_price = member_price;
            }
        }
    }

    public static class PicBean {
        /**
         * img : https://p31.yuemei.com/postimg/20191231/500_500/20191231152852_6a3800.jpg
         * width : 1080
         * height : 1920
         * video_url :
         * is_video : 0
         */

        private String img;
        private String width;
        private String height;
        private String video_url;
        private String is_video;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getVideo_url() {
            return video_url;
        }

        public void setVideo_url(String video_url) {
            this.video_url = video_url;
        }

        public String getIs_video() {
            return is_video;
        }

        public void setIs_video(String is_video) {
            this.is_video = is_video;
        }
    }

    public static class ContentBean {
        /**
         * text : {"content":"<p>ggvfvtvtvtvtvgvgvgvg<\/p>","url":""}
         */

        private TextBean text;

        public TextBean getText() {
            return text;
        }

        public void setText(TextBean text) {
            this.text = text;
        }

        public static class TextBean {
            /**
             * content : <p>ggvfvtvtvtvtvgvgvgvg</p>
             * url :
             */

            private String content;
            private String url;

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }
    }

    public static class TaolistBean {
        /**
         * sale_type : 2
         * appmurl : https://m.yuemei.com/tao/19895/
         * coupon_type : 0
         * is_show_member : 0
         * pay_dingjin : 100
         * number : 2
         * start_number : 1
         * pay_price_discount : 289
         * member_price : -1
         * m_list_logo : https://p11.yuemei.com/taobigpromotion/20191227095250_771.png
         * bmsid : 0
         * seckilling : 0
         * img : https://p24.yuemei.com/tao/2019/1220/200_200/191220232254_ec199a.jpg
         * img360 : https://p24.yuemei.com/tao/360_360/2019/1220/191220232249_95f748.jpg
         * title : 除皱针
         * subtitle : 衡力除皱（热销13000+）北医三院医学博士注射 高纯度保证疗效 除皱性价比之王 现场可查验
         * hos_name : 悦美好医医疗美容门诊部
         * doc_name : 张利民
         * price : 1580
         * price_discount : 289
         * price_range_max : 0
         * id : 19895
         * _id : 19895
         * showprice : 1
         * specialPrice : 0
         * show_hospital : 1
         * invitation : 0
         * lijian : 0
         * baoxian :
         * insure : {"is_insure":"0","insure_pay_money":"0","title":""}
         * img66 : https://p11.yuemei.com/taobigpromotion/20191227095232_907.png
         * app_slide_logo : https://p11.yuemei.com/taobigpromotion/20191227095239_405.png
         * repayment :
         * bilateral_coupons :
         * hos_red_packet :
         * mingyi : 0
         * hot : 0
         * newp : 0
         * shixiao : 0
         * extension_user :
         * postStr :
         * depreciate :
         * rate : 13522人预订
         * rateNum : 13522
         * service : 5.0
         * feeScale : /部位
         * is_fanxian : 0
         * hospital_id : 10647
         * doctor_id : 86345464
         * is_rongyun : 3
         * hos_userid : 700893
         * business_district :
         * hospital_top : {"level":0,"desc":"","sales":1}
         * is_have_video : 1
         * promotion : []
         * userImg : ["https://p21.yuemei.com/avatar/086/00/31/58_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/00/78/86_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/01/05/77_avatar_50_50.jpg"]
         * surgInfo : {"id":"8863","title":"注射除皱","name":"注射除皱","alias":"","url_name":"roudusu"}
         * highlight_title : %3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E9%99%A4%E7%9A%B1%E9%92%88%E3%80%91%E8%A1%A1%E5%8A%9B%E9%99%A4%E7%9A%B1%EF%BC%88%E7%83%AD%E9%94%8013000%2B%EF%BC%89%E5%8C%97%E5%8C%BB%E4%B8%89%E9%99%A2%E5%8C%BB%E5%AD%A6%E5%8D%9A%E5%A3%AB%E6%B3%A8%E5%B0%84%20%E9%AB%98%E7%BA%AF%E5%BA%A6%E4%BF%9D%E8%AF%81%E7%96%97%E6%95%88%20%E9%99%A4%E7%9A%B1%E6%80%A7%E4%BB%B7%E6%AF%94%E4%B9%8B%E7%8E%8B%20%E7%8E%B0%E5%9C%BA%E5%8F%AF%E6%9F%A5%E9%AA%8C%3C%2Fspan%3E
         * videoTaoTitle :
         * event_params : {"to_page_type":2,"to_page_id":19895,"event_others":0}
         */

        private int sale_type;
        private String appmurl;
        private int coupon_type;
        private String is_show_member;
        private String pay_dingjin;
        private String number;
        private String start_number;
        private String pay_price_discount;
        private String member_price;
        private String m_list_logo;
        private String bmsid;
        private String seckilling;
        private String img;
        private String img360;
        private String title;
        private String subtitle;
        private String hos_name;
        private String doc_name;
        private String price;
        private String price_discount;
        private String price_range_max;
        private String id;
        private String _id;
        private String showprice;
        private String specialPrice;
        private String show_hospital;
        private String invitation;
        private String lijian;
        private String baoxian;
        private InsureBean insure;
        private String img66;
        private String app_slide_logo;
        private String repayment;
        private String bilateral_coupons;
        private String hos_red_packet;
        private String mingyi;
        private String hot;
        private String newp;
        private String shixiao;
        private String extension_user;
        private String postStr;
        private String depreciate;
        private String rate;
        private int rateNum;
        private String service;
        private String feeScale;
        private String is_fanxian;
        private String hospital_id;
        private String doctor_id;
        private String is_rongyun;
        private String hos_userid;
        private String business_district;
        private HospitalTopBean hospital_top;
        private String is_have_video;
        private SurgInfoBean surgInfo;
        private String highlight_title;
        private String videoTaoTitle;
        private EventParamsBeanX event_params;
        private List<?> promotion;
        private List<String> userImg;

        public int getSale_type() {
            return sale_type;
        }

        public void setSale_type(int sale_type) {
            this.sale_type = sale_type;
        }

        public String getAppmurl() {
            return appmurl;
        }

        public void setAppmurl(String appmurl) {
            this.appmurl = appmurl;
        }

        public int getCoupon_type() {
            return coupon_type;
        }

        public void setCoupon_type(int coupon_type) {
            this.coupon_type = coupon_type;
        }

        public String getIs_show_member() {
            return is_show_member;
        }

        public void setIs_show_member(String is_show_member) {
            this.is_show_member = is_show_member;
        }

        public String getPay_dingjin() {
            return pay_dingjin;
        }

        public void setPay_dingjin(String pay_dingjin) {
            this.pay_dingjin = pay_dingjin;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getStart_number() {
            return start_number;
        }

        public void setStart_number(String start_number) {
            this.start_number = start_number;
        }

        public String getPay_price_discount() {
            return pay_price_discount;
        }

        public void setPay_price_discount(String pay_price_discount) {
            this.pay_price_discount = pay_price_discount;
        }

        public String getMember_price() {
            return member_price;
        }

        public void setMember_price(String member_price) {
            this.member_price = member_price;
        }

        public String getM_list_logo() {
            return m_list_logo;
        }

        public void setM_list_logo(String m_list_logo) {
            this.m_list_logo = m_list_logo;
        }

        public String getBmsid() {
            return bmsid;
        }

        public void setBmsid(String bmsid) {
            this.bmsid = bmsid;
        }

        public String getSeckilling() {
            return seckilling;
        }

        public void setSeckilling(String seckilling) {
            this.seckilling = seckilling;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getImg360() {
            return img360;
        }

        public void setImg360(String img360) {
            this.img360 = img360;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getHos_name() {
            return hos_name;
        }

        public void setHos_name(String hos_name) {
            this.hos_name = hos_name;
        }

        public String getDoc_name() {
            return doc_name;
        }

        public void setDoc_name(String doc_name) {
            this.doc_name = doc_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice_discount() {
            return price_discount;
        }

        public void setPrice_discount(String price_discount) {
            this.price_discount = price_discount;
        }

        public String getPrice_range_max() {
            return price_range_max;
        }

        public void setPrice_range_max(String price_range_max) {
            this.price_range_max = price_range_max;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getShowprice() {
            return showprice;
        }

        public void setShowprice(String showprice) {
            this.showprice = showprice;
        }

        public String getSpecialPrice() {
            return specialPrice;
        }

        public void setSpecialPrice(String specialPrice) {
            this.specialPrice = specialPrice;
        }

        public String getShow_hospital() {
            return show_hospital;
        }

        public void setShow_hospital(String show_hospital) {
            this.show_hospital = show_hospital;
        }

        public String getInvitation() {
            return invitation;
        }

        public void setInvitation(String invitation) {
            this.invitation = invitation;
        }

        public String getLijian() {
            return lijian;
        }

        public void setLijian(String lijian) {
            this.lijian = lijian;
        }

        public String getBaoxian() {
            return baoxian;
        }

        public void setBaoxian(String baoxian) {
            this.baoxian = baoxian;
        }

        public InsureBean getInsure() {
            return insure;
        }

        public void setInsure(InsureBean insure) {
            this.insure = insure;
        }

        public String getImg66() {
            return img66;
        }

        public void setImg66(String img66) {
            this.img66 = img66;
        }

        public String getApp_slide_logo() {
            return app_slide_logo;
        }

        public void setApp_slide_logo(String app_slide_logo) {
            this.app_slide_logo = app_slide_logo;
        }

        public String getRepayment() {
            return repayment;
        }

        public void setRepayment(String repayment) {
            this.repayment = repayment;
        }

        public String getBilateral_coupons() {
            return bilateral_coupons;
        }

        public void setBilateral_coupons(String bilateral_coupons) {
            this.bilateral_coupons = bilateral_coupons;
        }

        public String getHos_red_packet() {
            return hos_red_packet;
        }

        public void setHos_red_packet(String hos_red_packet) {
            this.hos_red_packet = hos_red_packet;
        }

        public String getMingyi() {
            return mingyi;
        }

        public void setMingyi(String mingyi) {
            this.mingyi = mingyi;
        }

        public String getHot() {
            return hot;
        }

        public void setHot(String hot) {
            this.hot = hot;
        }

        public String getNewp() {
            return newp;
        }

        public void setNewp(String newp) {
            this.newp = newp;
        }

        public String getShixiao() {
            return shixiao;
        }

        public void setShixiao(String shixiao) {
            this.shixiao = shixiao;
        }

        public String getExtension_user() {
            return extension_user;
        }

        public void setExtension_user(String extension_user) {
            this.extension_user = extension_user;
        }

        public String getPostStr() {
            return postStr;
        }

        public void setPostStr(String postStr) {
            this.postStr = postStr;
        }

        public String getDepreciate() {
            return depreciate;
        }

        public void setDepreciate(String depreciate) {
            this.depreciate = depreciate;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public int getRateNum() {
            return rateNum;
        }

        public void setRateNum(int rateNum) {
            this.rateNum = rateNum;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getFeeScale() {
            return feeScale;
        }

        public void setFeeScale(String feeScale) {
            this.feeScale = feeScale;
        }

        public String getIs_fanxian() {
            return is_fanxian;
        }

        public void setIs_fanxian(String is_fanxian) {
            this.is_fanxian = is_fanxian;
        }

        public String getHospital_id() {
            return hospital_id;
        }

        public void setHospital_id(String hospital_id) {
            this.hospital_id = hospital_id;
        }

        public String getDoctor_id() {
            return doctor_id;
        }

        public void setDoctor_id(String doctor_id) {
            this.doctor_id = doctor_id;
        }

        public String getIs_rongyun() {
            return is_rongyun;
        }

        public void setIs_rongyun(String is_rongyun) {
            this.is_rongyun = is_rongyun;
        }

        public String getHos_userid() {
            return hos_userid;
        }

        public void setHos_userid(String hos_userid) {
            this.hos_userid = hos_userid;
        }

        public String getBusiness_district() {
            return business_district;
        }

        public void setBusiness_district(String business_district) {
            this.business_district = business_district;
        }

        public HospitalTopBean getHospital_top() {
            return hospital_top;
        }

        public void setHospital_top(HospitalTopBean hospital_top) {
            this.hospital_top = hospital_top;
        }

        public String getIs_have_video() {
            return is_have_video;
        }

        public void setIs_have_video(String is_have_video) {
            this.is_have_video = is_have_video;
        }

        public SurgInfoBean getSurgInfo() {
            return surgInfo;
        }

        public void setSurgInfo(SurgInfoBean surgInfo) {
            this.surgInfo = surgInfo;
        }

        public String getHighlight_title() {
            return highlight_title;
        }

        public void setHighlight_title(String highlight_title) {
            this.highlight_title = highlight_title;
        }

        public String getVideoTaoTitle() {
            return videoTaoTitle;
        }

        public void setVideoTaoTitle(String videoTaoTitle) {
            this.videoTaoTitle = videoTaoTitle;
        }

        public EventParamsBeanX getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBeanX event_params) {
            this.event_params = event_params;
        }

        public List<?> getPromotion() {
            return promotion;
        }

        public void setPromotion(List<?> promotion) {
            this.promotion = promotion;
        }

        public List<String> getUserImg() {
            return userImg;
        }

        public void setUserImg(List<String> userImg) {
            this.userImg = userImg;
        }

        public static class InsureBean {
            /**
             * is_insure : 0
             * insure_pay_money : 0
             * title :
             */

            private String is_insure;
            private String insure_pay_money;
            private String title;

            public String getIs_insure() {
                return is_insure;
            }

            public void setIs_insure(String is_insure) {
                this.is_insure = is_insure;
            }

            public String getInsure_pay_money() {
                return insure_pay_money;
            }

            public void setInsure_pay_money(String insure_pay_money) {
                this.insure_pay_money = insure_pay_money;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public static class HospitalTopBean {
            /**
             * level : 0
             * desc :
             * sales : 1
             */

            private int level;
            private String desc;
            private int sales;

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public int getSales() {
                return sales;
            }

            public void setSales(int sales) {
                this.sales = sales;
            }
        }

        public static class SurgInfoBean {
            /**
             * id : 8863
             * title : 注射除皱
             * name : 注射除皱
             * alias :
             * url_name : roudusu
             */

            private String id;
            private String title;
            private String name;
            private String alias;
            private String url_name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAlias() {
                return alias;
            }

            public void setAlias(String alias) {
                this.alias = alias;
            }

            public String getUrl_name() {
                return url_name;
            }

            public void setUrl_name(String url_name) {
                this.url_name = url_name;
            }
        }

        public static class EventParamsBeanX {
            /**
             * to_page_type : 2
             * to_page_id : 19895
             * event_others : 0
             */

            private int to_page_type;
            private int to_page_id;
            private int event_others;

            public int getTo_page_type() {
                return to_page_type;
            }

            public void setTo_page_type(int to_page_type) {
                this.to_page_type = to_page_type;
            }

            public int getTo_page_id() {
                return to_page_id;
            }

            public void setTo_page_id(int to_page_id) {
                this.to_page_id = to_page_id;
            }

            public int getEvent_others() {
                return event_others;
            }

            public void setEvent_others(int event_others) {
                this.event_others = event_others;
            }
        }
    }
}
