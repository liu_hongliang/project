package com.module.other.activity

import android.annotation.SuppressLint
import com.module.base.view.YMBaseActivity
import com.quicklyask.activity.R
import kotlinx.android.synthetic.main.activity_kotlin_ceshi.*

/**
 * 新语言学习页面
 */
class KotlinCeshiActivity : YMBaseActivity() {

    var chuan: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.activity_kotlin_ceshi
    }

    @SuppressLint("SetTextI18n")
    override fun initView() {
        ceshi_img_camer.setOnClickListener {
            mFunctionManager.showShort("点击了一下")
        }

        tv_ceshi_ceshi.text = "字符串上的值是：$chuan"
    }

    override fun initData() {

    }

}
