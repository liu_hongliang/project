package com.quicklyask.entity;

public class Pic {
    private String img;
    private String width;
    private String height;
    private String have_pic;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHave_pic() {
        return have_pic;
    }

    public void setHave_pic(String have_pic) {
        this.have_pic = have_pic;
    }
}
