package com.module.commonview.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.CityWideLocalActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.PostoperativeRecoveryActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.adapter.GoodsGroupViewpagerAdapter;
import com.module.commonview.module.bean.DiaryCompareBean;
import com.module.commonview.module.bean.DiaryHosDocBean;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.DiaryRateBean;
import com.module.commonview.module.bean.DiaryTagList;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.other.DiaryHeadSkuItemView;
import com.module.commonview.view.PostFlowLayoutGroup;
import com.module.commonview.view.UnderlinePageIndicator;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.view.StaScoreBar;
import com.module.home.controller.activity.SearchAllActivity668;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 浏览他人时的Fragment
 * Created by 裴成浩 on 2018/6/14.
 */
public class DiaryListOtherFragment extends YMBaseFragment {

    @BindView(R.id.diary_list_before_after_photo_container)
    LinearLayout mBeforeAndAfterContainer;
    @BindView(R.id.diary_list_before_photo_container)
    RelativeLayout mBeforeContainer;
    @BindView(R.id.diary_list_before_photo_backgrod)
    LinearLayout mBeforeBackgrod;
    @BindView(R.id.diary_list_before_photo)
    ImageView mBeforePhoto;
    @BindView(R.id.diary_list_before_atlas)
    LinearLayout mBeforeAtlas;
    @BindView(R.id.diary_list_before_atlas_num)
    TextView mBeforeAtlasNum;
    @BindView(R.id.diary_list_before_editor)
    TextView mBeforeEditor;

    @BindView(R.id.diary_list_after_photo_container)
    RelativeLayout mAfterContainer;
    @BindView(R.id.diary_list_after_photo_backgrod)
    LinearLayout mAfterBackgrod;
    @BindView(R.id.diary_list_after_photo)
    ImageView mAfterPhoto;
    @BindView(R.id.diary_list_after_atlas)
    LinearLayout mAfterAtlas;
    @BindView(R.id.diary_list_after_atlas_num)
    TextView mAfterAtlasNum;
    @BindView(R.id.diary_list_after_editor)
    TextView mAfterEditor;

    @BindView(R.id.diary_list_other_goods_group2)
    ViewPager otherGoodsGroup2;
    @BindView(R.id.diary_list_other_goods_indicator2)
    UnderlinePageIndicator otherGoodsIndicator2;

    @BindView(R.id.other_illustrated_room_ratingbar)
    StaScoreBar otherRoomRatingbar;

    @BindView(R.id.other_illustrated_service_container)
    LinearLayout otherServicecontainer;
    @BindView(R.id.other_illustrated_room_container)
    LinearLayout otherRoomContainer;
    @BindView(R.id.other_illustrated_service_num)
    TextView otherServiceNum;
    @BindView(R.id.other_illustrated_operation_num)
    TextView otherOperationNum;
    @BindView(R.id.other_illustrated_hos_num)
    TextView otherHosNum;

    @BindView(R.id.other_illustrated_tag_container)
    FlowLayout otherFlowLayout;

    private DiaryListData mData;
    private String mDiaryId;
    private String TAG = "DiaryListOtherFragment";
    private DiaryHeadSkuItemView diarySkuItemView;
    private String mPid;

    public static DiaryListOtherFragment newInstance(DiaryListData data, String id) {
        DiaryListOtherFragment fragment = new DiaryListOtherFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("id", id);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = getArguments().getParcelable("data");
        mDiaryId = getArguments().getString("id");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_adiary_list_other_user;
    }

    @Override
    protected void initView(View view) {
        //术前术后照片上传按钮隐藏
        mBeforeBackgrod.setVisibility(View.GONE);
        mAfterBackgrod.setVisibility(View.GONE);

        //术前术后图集显示
        mBeforeAtlas.setVisibility(View.VISIBLE);
        mAfterAtlas.setVisibility(View.VISIBLE);

        //术前术后编辑按钮隐藏
        mBeforeEditor.setVisibility(View.GONE);
        mAfterEditor.setVisibility(View.GONE);

        //设置照片的高度
        int photoHeigth = (windowsWight - Utils.dip2px(50)) / 2;

        ViewGroup.LayoutParams layoutParams1 = mBeforeContainer.getLayoutParams();
        layoutParams1.height = photoHeigth;
        mBeforeContainer.setLayoutParams(layoutParams1);
        ViewGroup.LayoutParams layoutParams2 = mAfterContainer.getLayoutParams();
        layoutParams2.height = photoHeigth;
        mAfterContainer.setLayoutParams(layoutParams2);

    }

    @Override
    protected void initData(View view) {
        diarySkuItemView = new DiaryHeadSkuItemView(mContext, mDiaryId,"1");
        initDiaryData();
    }

    /**
     * 设置数据
     */
    @SuppressLint("SetTextI18n")
    private void initDiaryData() {
        List<DiaryTaoData> taoData = mData.getTaoData();    //下单商品数据
        DiaryHosDocBean hosDoc = mData.getHosDoc();         //服务的医生医院
        DiaryRateBean mRateData = mData.getRate();          //服务评分数据
        mPid = mData.getP_id();

        //设置SKU
        setSkuData(taoData, hosDoc);

        //初始化前后对比图数据
        Log.e(TAG, "mData.getCompare() == " + mData.getCompare());
        DiaryCompareBean mCompareData = mData.getCompare();
        if (mCompareData != null){
            final DiaryCompareBean.BeforeBean before = mCompareData.getBefore();
            final DiaryCompareBean.AfterBean after = mCompareData.getAfter();

            if (TextUtils.isEmpty(before.getImg()) || TextUtils.isEmpty(after.getImg())) {
                mBeforeAndAfterContainer.setVisibility(View.GONE);
            } else {
                mBeforeAndAfterContainer.setVisibility(View.VISIBLE);
                mFunctionManager.setRoundImageSrc(mBeforePhoto, before.getImg(), Utils.dip2px(4));
                mBeforeAtlasNum.setText(before.getPic_num());
                mFunctionManager.setRoundImageSrc(mAfterPhoto, after.getImg(), Utils.dip2px(4));
                mAfterAtlasNum.setText(after.getPic_num());
            }

            if (TextUtils.isEmpty(before.getImg()) || TextUtils.isEmpty(after.getImg())) {
                mBeforeAndAfterContainer.setVisibility(View.GONE);
            } else {
                mBeforeAndAfterContainer.setVisibility(View.VISIBLE);
                mFunctionManager.setRoundImageSrc(mBeforePhoto, before.getImg(), Utils.dip2px(8));
                mFunctionManager.setRoundImageSrc(mAfterPhoto, after.getImg(), Utils.dip2px(8));
            }

            //初始化楼主评分
            String service = mRateData.getService();
            String effect = mRateData.getEffect();
            String pf_doctor = mRateData.getPf_doctor();
            Log.e(TAG, "service == " + service);
            Log.e(TAG, "effect == " + effect);
            Log.e(TAG, "pf_doctor == " + pf_doctor);
            if (!TextUtils.isEmpty(service) && !TextUtils.isEmpty(effect) && !TextUtils.isEmpty(pf_doctor) && !"0".equals(service) && !"0".equals(effect) && !"0".equals(pf_doctor)) {
                otherServicecontainer.setVisibility(View.VISIBLE);
                otherServiceNum.setText(service);                            //服务
                otherOperationNum.setText(effect);                           //手术效果
                otherHosNum.setText(pf_doctor);                              //医院评价
            } else {
                otherServicecontainer.setVisibility(View.GONE);
            }

            int rateSale = Integer.parseInt(mRateData.getRateSale());
            if (rateSale != 0) {
                otherRoomContainer.setVisibility(View.VISIBLE);
                otherRoomRatingbar.setProgressBar(rateSale);    //评分
            }else{
                otherRoomContainer.setVisibility(View.GONE);
            }

            //加载日记标签
            setTagData();

            //术前照片
            mBeforePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_COMPARED_IMG_CLICK, "0"), before.getEvent_params(), new ActivityTypeData("45"));
                    jumpDiariesAdnPostsActivity();
                }
            });

            //术后照片
            mAfterPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_COMPARED_IMG_CLICK, "1"), after.getEvent_params(), new ActivityTypeData("45"));
                    jumpDiariesAdnPostsActivity();
                }
            });
        }

    }

    @NonNull
    private void setSkuData(List<DiaryTaoData> taoData, DiaryHosDocBean hosDoc) {
        //初始化淘数据
        GoodsGroupViewpagerAdapter adapter2 = new GoodsGroupViewpagerAdapter(diarySkuItemView.initTaoData(taoData, hosDoc));
        otherGoodsGroup2.setAdapter(adapter2);

        ViewGroup.LayoutParams layoutParams = otherGoodsGroup2.getLayoutParams();
        if (taoData.size() > 1) {
            otherGoodsIndicator2.setVisibility(View.VISIBLE);
            otherGoodsIndicator2.setViewPager(otherGoodsGroup2);
        } else if (taoData.size() > 0) {
            otherGoodsGroup2.setVisibility(View.VISIBLE);
            otherGoodsIndicator2.setVisibility(View.GONE);
        } else {
            otherGoodsGroup2.setVisibility(View.GONE);
            otherGoodsIndicator2.setVisibility(View.GONE);
        }

        String hos_userid = hosDoc.getHos_userid();
        String doctor_id = hosDoc.getDoctor_id();
        if (!TextUtils.isEmpty(hos_userid) && !"0".equals(hos_userid) && !TextUtils.isEmpty(doctor_id) && !"0".equals(doctor_id)) {
            layoutParams.height = Utils.dip2px(160);
        } else if ((TextUtils.isEmpty(hos_userid) || "0".equals(hos_userid)) && (TextUtils.isEmpty(doctor_id) || "0".equals(doctor_id))) {
            layoutParams.height = Utils.dip2px(90);
        } else {
            layoutParams.height = Utils.dip2px(120);
        }

        otherGoodsGroup2.setLayoutParams(layoutParams);

        adapter2.setOnItemCallBackListener(new GoodsGroupViewpagerAdapter.ItemCallBackListener() {
            @Override
            public void onItemClick(View v, int pos) {
                String tao_id = mData.getTaoData().get(pos).getId();
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", mDiaryId);
                hashMap.put("to_page_type", "2");
                hashMap.put("to_page_id", tao_id);
                if (isDiaryOrDetails()) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_TO_TAO, "in_the_text"), hashMap, new ActivityTypeData(mData.getSelfPageType()));
                } else {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SHAREINFO_TO_TAO, "in_the_text"), hashMap, new ActivityTypeData(mData.getSelfPageType()));
                }

                Intent intent = new Intent(mContext, TaoDetailActivity.class);
                intent.putExtra("id", tao_id);
                if (mContext instanceof DiariesAndPostsActivity) {
                    intent.putExtra("source", ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier());
                } else {
                    intent.putExtra("source", "0");
                }
                intent.putExtra("objid", mDiaryId);
                startActivity(intent);
            }
        });

        diarySkuItemView.setOnEventClickListener(new DiaryHeadSkuItemView.OnEventClickListener() {
            @Override
            public void onItemClick(View view, DiaryTaoData data) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("hos_id",data.getHospital_id());
                hashMap.put("doc_id",data.getDoc_id());
                hashMap.put("tao_id",data.getId());
                hashMap.put("id",mDiaryId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_HOSPITAL, "diary_tao",data.getHospital_id()),hashMap,new ActivityTypeData("45"));
            }
        });
    }

    /**
     * 设置标签数据
     */
    private void setTagData() {
        List<DiaryTagList> tags = mData.getTag();

        //适配数据
        PostFlowLayoutGroup mPostFlowLayoutGroup = new PostFlowLayoutGroup(mContext, otherFlowLayout, tags);
        mPostFlowLayoutGroup.setClickCallBack(new PostFlowLayoutGroup.ClickCallBack() {
            @Override
            public void onClick(View v, int pos, DiaryTagList data) {
                if (!TextUtils.isEmpty(data.getUrl())) {
                    HashMap<String, String> event_params = data.getEvent_params();
                    if (event_params != null) {
                        event_params.put("id",mDiaryId);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.POSTINFO_TAG_CLICK, (pos + 1) + ""), event_params);
                        if ("1".equals(data.getJump_type())) {
                            Intent it = new Intent(mContext, SearchAllActivity668.class);
                            it.putExtra("keys", data.getName());
                            it.putExtra("results", true);
                            it.putExtra("position", 0);
                            startActivity(it);
                        } else {
                            Intent intent = new Intent(mContext, CityWideLocalActivity.class);
                            intent.putExtra("title", data.getName());
                            intent.putExtra("id", mDiaryId);
                            startActivity(intent);
                        }
                    }
                }
            }
        });
    }

    /**
     * 判断是日记还是日记本
     *
     * @return ：true 日记本
     */
    private boolean isDiaryOrDetails() {
        if (!TextUtils.isEmpty(mPid)) {
            return "0".equals(mPid);
        } else {
            return false;
        }
    }

    /**
     * 跳转到术前术后照片页面
     */
    private void jumpDiariesAdnPostsActivity() {
        Intent intent = new Intent(mContext, PostoperativeRecoveryActivity.class);
        intent.putExtra("diary_id", mDiaryId);
        startActivity(intent);
    }
}
