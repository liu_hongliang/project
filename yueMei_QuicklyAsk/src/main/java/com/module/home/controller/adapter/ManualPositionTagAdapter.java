package com.module.home.controller.adapter;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.home.model.bean.ManualPositionBtnBoard;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * @author 裴成浩
 * @data 2019/11/6
 */
public class ManualPositionTagAdapter extends RecyclerView.Adapter<ManualPositionTagAdapter.ViewHolder> {


    private final Activity mContext;
    private final List<ManualPositionBtnBoard> mBoardLists;
    private final LayoutInflater mInflater;
    private final int spanCount;
    private final int parentWidth;          //父类宽度
    private String TAG = "ManualPositionTagAdapter";

    public ManualPositionTagAdapter(Activity context, List<ManualPositionBtnBoard> boardLists, int spanCount, int parentWidth) {
        this.mContext = context;
        this.mBoardLists = boardLists;
        this.spanCount = spanCount;
        this.parentWidth = parentWidth;
        mInflater = LayoutInflater.from(mContext);

        Log.e(TAG, "parentWidth == " + parentWidth);
    }

    @NonNull
    @Override
    public ManualPositionTagAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(mInflater.inflate(R.layout.project_list_item_txt, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ManualPositionTagAdapter.ViewHolder viewHolder, int pos) {
        ManualPositionBtnBoard btnBoard = mBoardLists.get(pos);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.itemTxt.getLayoutParams();
        if (pos / spanCount > 0) {
            layoutParams.topMargin = Utils.dip2px(15);
        }

        int width = 0;
        if (spanCount == 3) {
            width = (parentWidth - ((layoutParams.leftMargin + layoutParams.rightMargin) * spanCount)) / spanCount;
        } else if (spanCount == 2) {
            width = (parentWidth - ((layoutParams.leftMargin + layoutParams.rightMargin) * spanCount)) / spanCount;
        }

        if (width > 0) {
            layoutParams.width = width;
            layoutParams.height = width * 30 / 90;
        }

        setGradientBackground(viewHolder.itemTxt, btnBoard.getBack_color());
        viewHolder.itemTxt.setText(btnBoard.getTitle());
    }

    @Override
    public int getItemCount() {
        return mBoardLists.size();
    }

    /**
     * 设置渐变背景
     *
     * @param view             :要设置渐变色组件
     * @param back_color：渐变色颜色
     */
    private void setGradientBackground(View view, List<String> back_color) {
        try {
            if (back_color.size() >= 2) {
                String colorLeft = back_color.get(0);
                String colorRight = back_color.get(1);
                if (colorLeft.startsWith("#") && colorRight.startsWith("#")) {
                    GradientDrawable aDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{Utils.setCustomColor(colorLeft), Utils.setCustomColor(colorRight)});
                    aDrawable.setCornerRadius(Utils.dip2px(15));
                    view.setBackground(aDrawable);
                } else {
                    if (colorLeft.startsWith("#")) {
                        view.setBackgroundColor(Utils.setCustomColor(colorLeft));
                    } else if (colorRight.startsWith("#")) {
                        view.setBackgroundColor(Utils.setCustomColor(colorRight));
                    } else {
                        view.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_text_shap));
                    }
                }
            } else {
                view.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_text_shap));
            }
        } catch (NumberFormatException e) {
            view.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_text_shap));
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemTxt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemTxt = itemView.findViewById(R.id.project_list_item_txt);

            //点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getLayoutPosition(),mBoardLists.get(getLayoutPosition()));
                    }
                }
            });
        }
    }

    private OnItemClickListener onItemClickListener;

    //点击事件
    public interface OnItemClickListener {
        void onItemClick(View view, int position, ManualPositionBtnBoard data);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
