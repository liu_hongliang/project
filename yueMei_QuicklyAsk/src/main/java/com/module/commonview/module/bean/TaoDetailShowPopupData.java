package com.module.commonview.module.bean;

import android.view.View;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2018/2/26.
 */

public class TaoDetailShowPopupData {
    private View conetnLy;
    private YuDingData ydData;
    private HashMap<String, String> mMap;
    private String groupId;
    private String source;
    private String objid;
    private String type;   //是拼团时，判断是单人购0，还是团购1
    private String kefu_nickName;
    private String is_fanxian; //是否显示返现


    public View getConetnLy() {
        return conetnLy;
    }

    public TaoDetailShowPopupData setConetnLy(View conetnLy) {
        this.conetnLy = conetnLy;
        return this;
    }

    public YuDingData getYdData() {
        return ydData;
    }

    public TaoDetailShowPopupData setYdData(YuDingData ydData) {
        this.ydData = ydData;
        return this;
    }

    public HashMap<String, String> getmMap() {
        return mMap;
    }

    public TaoDetailShowPopupData setmMap(HashMap<String, String> mMap) {
        this.mMap = mMap;
        return this;
    }

    public String getGroupId() {
        return groupId;
    }

    public TaoDetailShowPopupData setGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    public String getSource() {
        return source;
    }

    public TaoDetailShowPopupData setSource(String source) {
        this.source = source;
        return this;
    }

    public String getObjid() {
        return objid;
    }

    public TaoDetailShowPopupData setObjid(String objid) {
        this.objid = objid;
        return this;
    }

    public String getType() {
        return type;
    }

    public TaoDetailShowPopupData setType(String type) {
        this.type = type;
        return this;
    }

    public String getKefu_nickName() {
        return kefu_nickName;
    }

    public TaoDetailShowPopupData setKefu_nickName(String kefu_nickName) {
        this.kefu_nickName = kefu_nickName;
        return this;
    }

    public String getIs_fanxian() {
        return is_fanxian;
    }

    public TaoDetailShowPopupData setIs_fanxian(String is_fanxian) {
        this.is_fanxian = is_fanxian;
        return this;
    }
}
