package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/7/10.
 */
public class HomeCommunityPartsData implements Parcelable {
    private String _id;
    private String url_name;
    private String name;
    private String desc;
    private String jia;
    private String img;

    protected HomeCommunityPartsData(Parcel in) {
        _id = in.readString();
        url_name = in.readString();
        name = in.readString();
        desc = in.readString();
        jia = in.readString();
        img = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeString(url_name);
        dest.writeString(name);
        dest.writeString(desc);
        dest.writeString(jia);
        dest.writeString(img);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeCommunityPartsData> CREATOR = new Creator<HomeCommunityPartsData>() {
        @Override
        public HomeCommunityPartsData createFromParcel(Parcel in) {
            return new HomeCommunityPartsData(in);
        }

        @Override
        public HomeCommunityPartsData[] newArray(int size) {
            return new HomeCommunityPartsData[size];
        }
    };

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUrl_name() {
        return url_name;
    }

    public void setUrl_name(String url_name) {
        this.url_name = url_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getJia() {
        return jia;
    }

    public void setJia(String jia) {
        this.jia = jia;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
