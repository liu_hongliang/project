package com.module.my.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.GetaddressData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 获取收货地址
 * Created by 裴成浩 on 2018/9/6.
 */
public class GetaddressApi implements BaseCallBackApi {
    private String TAG = "GetaddressApi";
    private HashMap<String, Object> mGetaddressApiHashMap;  //传值容器

    public GetaddressApi() {
        mGetaddressApiHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "getaddress", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, mData.toString());
                try {
                    if ("1".equals(mData.code)) {
                        GetaddressData getaddressData = JSONUtil.TransformSingleBean(mData.data, GetaddressData.class);

                        listener.onSuccess(getaddressData);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "e == " + e.toString());
                    e.printStackTrace();
                }

            }
        });
    }

    public HashMap<String, Object> getGetaddressHashMap() {
        return mGetaddressApiHashMap;
    }

    public void addData(String key, String value) {
        mGetaddressApiHashMap.put(key, value);
    }
}
