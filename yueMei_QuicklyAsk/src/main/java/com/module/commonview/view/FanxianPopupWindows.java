package com.module.commonview.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.module.bean.CashBackD;
import com.module.commonview.module.bean.CashBackData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/1/23.
 */

public class FanxianPopupWindows extends PopupWindow {
    private List<CashBackData> cashList;

    @SuppressWarnings("deprecation")
    public FanxianPopupWindows(final Activity mActivity, View parent, CashBackD asa) {

        final View view = View.inflate(mActivity, R.layout.pop_fanxian,
                null);

        view.startAnimation(AnimationUtils.loadAnimation(mActivity,
                R.anim.fade_ins));

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(false);
        setContentView(view);
        showAtLocation(parent, Gravity.CENTER, 0, 0);
        update();
        String desc = asa.getDesc();
        cashList = asa.getData();
        int casize = cashList.size();
        ImageView[] fxiv = new ImageView[casize];
        TextView[] fxtv = new TextView[casize];
        TextView[] fxiftv = new TextView[casize];

        LayoutInflater mInflater = mActivity.getLayoutInflater();
        LinearLayout itemContentLy = view.findViewById(R.id.fanxian_item_cotent_ly);

        for (int i = 0; i < cashList.size(); i++) {
            View headView = mInflater.inflate(R.layout.item_fanxian_yaoqiu, null);
            fxiv[i] = headView.findViewById(R.id.fx_check_iv1);
            fxtv[i] = headView.findViewById(R.id.fx_check_tv1);
            fxiftv[i] = headView.findViewById(R.id.fx_check_if_tv1);

            fxtv[i].setText(cashList.get(i).getCashback_require_desc());
            String ifc = cashList.get(i).getIs_complete();
            if (ifc.equals("1")) {
                fxiv[i].setBackgroundResource(R.drawable.xuanzhong_2x);
                fxiftv[i].setText("已完成");
                fxiftv[i].setTextColor(Color.parseColor("#999999"));
            } else {
                fxiv[i].setBackgroundResource(R.drawable.raido_cuo3x);
                fxiftv[i].setText("未完成");
                fxiftv[i].setTextColor(Color.parseColor("#ff6666"));
            }

            itemContentLy.addView(headView);
        }

        Button aaBt = view.findViewById(R.id.fx_iknwn_bt);
        TextView descTv = view.findViewById(R.id.fx_desc);
        descTv.setText(Html.fromHtml(desc + "  更多规则点击“<b><u><font color='#4d7bbc'>详细规则</font></u></b>”"));

        descTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it1 = new Intent();
                String url1 = FinalConstant.baseUrl + FinalConstant.VER
                        + "/forum/postinfo/id/939555/";
                it1.putExtra("url", url1);
                it1.putExtra("qid", "939555");
                it1.setClass(mActivity, DiariesAndPostsActivity.class);
                mActivity.startActivity(it1);
            }
        });
        aaBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

    }

}
