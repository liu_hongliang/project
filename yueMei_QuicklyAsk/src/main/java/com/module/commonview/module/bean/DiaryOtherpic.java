package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/6.
 */
public class DiaryOtherpic implements Parcelable {
    private String img;
    private String y_width;
    private String y_height;
    private String video_url = "";
    private String is_video = "0";

    private DiaryOtherpic(Parcel in) {
        img = in.readString();
        y_width = in.readString();
        y_height = in.readString();
        video_url = in.readString();
        is_video = in.readString();
    }

    public static final Creator<DiaryOtherpic> CREATOR = new Creator<DiaryOtherpic>() {
        @Override
        public DiaryOtherpic createFromParcel(Parcel in) {
            return new DiaryOtherpic(in);
        }

        @Override
        public DiaryOtherpic[] newArray(int size) {
            return new DiaryOtherpic[size];
        }
    };

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getY_width() {
        return y_width;
    }

    public void setY_width(String y_width) {
        this.y_width = y_width;
    }

    public String getY_height() {
        return y_height;
    }

    public void setY_height(String y_height) {
        this.y_height = y_height;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getIs_video() {
        return is_video;
    }

    public void setIs_video(String is_video) {
        this.is_video = is_video;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
        dest.writeString(y_width);
        dest.writeString(y_height);
        dest.writeString(video_url);
        dest.writeString(is_video);
    }
}
