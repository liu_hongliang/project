package sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 数据库创建类
 * Created by 裴成浩 on 2018/4/25.
 */
public class OrderDBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;                //数据库版本
    private static final String DB_NAME = "yuemei.db";      //数据库名称
    public static final String MESSAGE_NUMBER = "message_number";  //消息数表名
    private String TAG = "OrderDBHelper";

    public OrderDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createMessNumTable(db);
    }

    /**
     * 创建消息数表
     *
     * @param db
     */
    private void createMessNumTable(SQLiteDatabase db) {
        //共三条数据：1：私信，2：回复，3通知数
        String sql = "create table if not exists " + MESSAGE_NUMBER + " (messageId integer primary key, number integer)";
        String sql1 = "insert into " + MESSAGE_NUMBER + "(messageId,number) values(1,0)";
        String sql2 = "insert into " + MESSAGE_NUMBER + "(messageId,number) values(2,0)";
        String sql3 = "insert into " + MESSAGE_NUMBER + "(messageId,number) values(3,0)";
        db.execSQL(sql);
        db.execSQL(sql1);
        db.execSQL(sql2);
        db.execSQL(sql3);
    }

    /**
     * 升级数据库
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + MESSAGE_NUMBER;
        db.execSQL(sql);
        onCreate(db);
    }

}
