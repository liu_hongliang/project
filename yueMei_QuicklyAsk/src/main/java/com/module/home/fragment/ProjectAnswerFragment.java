package com.module.home.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.adapter.ProjectAnswerAdapter;
import com.module.home.model.api.ProjectDetailsApi;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.home.model.bean.ProjectDetailsData;
import com.module.home.model.bean.QuestionListData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/5/14
 */
public class ProjectAnswerFragment extends YMBaseFragment {

    private String TAG = "ProjectDiaryFragment";
    @BindView(R.id.project_answer_refresh)
    SmartRefreshLayout mAnswerRefresh;
    @BindView(R.id.project_answer_rec)
    RecyclerView mAnswerRecycler;

    private ArrayList<QuestionListData> mFragmentData;
    private String positioningCity;
    private ProjectAnswerAdapter mProjectAnswerAdapter;
    private ProjectDetailsBean detailsBean;
    private ProjectDetailsApi mProjectDetailsApi;
    private int mPage = 1;

    public static ProjectAnswerFragment newInstance(ArrayList<QuestionListData> datas, ProjectDetailsBean bean) {
        ProjectAnswerFragment fragment = new ProjectAnswerFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("bean", bean);
        bundle.putParcelableArrayList("datas", datas);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_project_answer_diary;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            detailsBean = getArguments().getParcelable("bean");
            mFragmentData = getArguments().getParcelableArrayList("datas");
        }

        Log.e(TAG, "mFragmentData == " + mFragmentData);

        positioningCity = mFunctionManager.loadStr(FinalConstant.DWCITY, "全国");

        //上拉加载更多
        mAnswerRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mAnswerRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                getProjectAnswerData();
            }
        });
    }

    @Override
    protected void initData(View view) {
        mProjectDetailsApi = new ProjectDetailsApi();
        if (mFragmentData.size() == 0) {
            getProjectAnswerData();
        } else {
            mPage++;

            setRecyclerData(mFragmentData);
        }
    }

    /**
     * 获取页面数据
     */
    private void getProjectAnswerData() {

        Log.e(TAG, "mLabelId == " + detailsBean.getFourLabelId());
        Log.e(TAG, "mHomeSource == " + detailsBean.getHomeSource());
        Log.e(TAG, "positioningCity == " + positioningCity);

        mProjectDetailsApi.addData("parentLabelID", detailsBean.getTwoLabelId());
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId())) {
            mProjectDetailsApi.addData("labelID", detailsBean.getFourLabelId());
        }
        mProjectDetailsApi.addData("listType", "5");
        mProjectDetailsApi.addData("homeSource", detailsBean.getHomeSource());
        mProjectDetailsApi.addData("city", positioningCity);
        mProjectDetailsApi.addData("page", mPage + "");
        mProjectDetailsApi.getCallBack(mContext, mProjectDetailsApi.getProjectDetailsHashMap(), new BaseCallBackListener<ProjectDetailsData>() {
            @Override
            public void onSuccess(ProjectDetailsData data) {
                mPage++;
                if (data.getQuestionList().size() == 0) {
                    mAnswerRefresh.finishLoadMoreWithNoMoreData();
                } else {
                    mAnswerRefresh.finishLoadMore();
                }

                if (mProjectAnswerAdapter == null) {

                    if (data.getQuestionList().size() != 0) {
                        mAnswerRefresh.setVisibility(View.VISIBLE);

                        setRecyclerData(data.getQuestionList());
                    } else {
                        mAnswerRefresh.setVisibility(View.GONE);
                    }

                } else {
                    mProjectAnswerAdapter.addData(data.getQuestionList());
                }

                Log.e(TAG, "data == " + data.getQuestionList().size());
            }
        });
    }

    /**
     * 设置列表数据
     *
     * @param questionList
     */
    private void setRecyclerData(ArrayList<QuestionListData> questionList) {
        mAnswerRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mProjectAnswerAdapter = new ProjectAnswerAdapter(mContext, questionList,"");
        mAnswerRecycler.setAdapter(mProjectAnswerAdapter);

        //item点击事件
        mProjectAnswerAdapter.setOnItemClickListener(new ProjectAnswerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos, QuestionListData data) {
                String jumpUrl = data.getJumpUrl();
                HashMap<String, String> event_params = mProjectAnswerAdapter.getDatas().get(pos).getEvent_params();
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_ASKLIST_CLICK,""),event_params,new ActivityTypeData("121"));
                if (!TextUtils.isEmpty(jumpUrl)) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(jumpUrl);
                }
            }
        });
    }

    /**
     * 获取选中标签
     *
     * @return
     */
    private String getTagId() {
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId()) && !"0".equals(detailsBean.getFourLabelId())) {
            return detailsBean.getFourLabelId();
        } else {
            return detailsBean.getTwoLabelId();
        }
    }

}
