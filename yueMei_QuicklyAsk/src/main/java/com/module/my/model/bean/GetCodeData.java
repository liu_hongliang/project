package com.module.my.model.bean;

public class GetCodeData {
	private String invitationCode;
	private String logo;
	private String url;

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "GetCodeData [invitationCode=" + invitationCode + ", logo="
				+ logo + ", url=" + url + "]";
	}

}
