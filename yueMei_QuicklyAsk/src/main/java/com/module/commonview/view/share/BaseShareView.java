package com.module.commonview.view.share;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.widget.Toast;

import com.module.commonview.module.bean.SharePictorial;
import com.module.commonview.module.bean.ShareWechat;
import com.module.community.model.bean.ShareDetailPictorial;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.shareboard.ShareBoardConfig;

import java.util.List;

/**
 * 分享控制面板
 * Created by 裴成浩 on 2018/1/4.
 */

public class BaseShareView {

    private String shareContent;
    private Activity mActivity;
    private MyShareBoardlistener shareBoardlistener;
    private boolean isFavorite = false;         //是否显示微信收藏
    private boolean isSms = true;              //是否显示短信分享
    private SharePictorial mSharePictorial = null;      //画报数据
    private ShareDetailPictorial mShareDetailPictorial = null;  //日记本数据

    public BaseShareView(Activity activity) {
        this.mActivity = activity;

    }

    public BaseShareView setShareContent(String shareContent) {
        this.shareContent = shareContent;
        return this;
    }

    /**
     * 是否显示微信收藏
     *
     * @param isFavorite
     * @return
     */
    public BaseShareView setShareFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
        return this;
    }

    /**
     * 是否显示短信
     *
     * @param isSms
     * @return
     */
    public BaseShareView setShareSms(boolean isSms) {
        this.isSms = isSms;
        return this;
    }

    /**
     * SKU画报数据
     *
     * @param sharePictorial
     * @return
     */
    public BaseShareView setShareIllustrated(SharePictorial sharePictorial) {
        this.mSharePictorial = sharePictorial;
        return this;
    }

    /**
     * 日记本画报数据传递
     *
     * @param shareDetailPictorial
     * @return
     */
    public BaseShareView setShareIllustrated(ShareDetailPictorial shareDetailPictorial) {
        this.mShareDetailPictorial = shareDetailPictorial;
        return this;
    }

    /**
     * 普通分享开始
     */
    public void ShareAction() {
        ShareAction(null);
    }

    /**
     * 分享给朋友小程序分享
     *
     * @param mWechat：小程序所需要的参数类
     */
    public void ShareAction(ShareWechat mWechat) {
        if (mSharePictorial != null) {
            shareBoardlistener = new MyShareBoardlistener(mActivity, mWechat, mSharePictorial);
        } else if (mShareDetailPictorial != null) {
            shareBoardlistener = new MyShareBoardlistener(mActivity, mWechat, mShareDetailPictorial);
        } else {
            shareBoardlistener = new MyShareBoardlistener(mActivity, mWechat);
        }
        versionPanel();
    }

    private void versionPanel() {
        //版本判断
        if (Build.VERSION.SDK_INT >= 23) {
            Acp.getInstance(mActivity).request(new AcpOptions.Builder().setPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                @Override
                public void onGranted() {
                    setPanelOption();
                }

                @Override
                public void onDenied(List<String> permissions) {
                    Toast.makeText(mActivity, "没有存储权限", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            setPanelOption();

        }
    }

    /**
     * 设置控制面板的样式
     */
    private void setPanelOption() {
        ShareAction shareAction = new ShareAction(mActivity);
        shareAction.withText(shareContent);

        //是否显示收藏
        if (isFavorite) {
            if (isSms) {
                shareAction.setDisplayList(SHARE_MEDIA.SINA, SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.WEIXIN_FAVORITE, SHARE_MEDIA.SMS);
            } else {
                shareAction.setDisplayList(SHARE_MEDIA.SINA, SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.WEIXIN_FAVORITE);
            }
        } else {
            if (isSms) {
                shareAction.setDisplayList(SHARE_MEDIA.SINA, SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.SMS);
            } else {
                shareAction.setDisplayList(SHARE_MEDIA.SINA, SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE);
            }
        }

        //是否显示画报
        if (mSharePictorial != null || mShareDetailPictorial != null) {
            shareAction.addButton("umeng_sharebutton_huabao", "umeng_sharebutton_huabao", "umeng_socialize_huabao", "umeng_socialize_huabao");   // 自定义按钮
        }

        shareAction.setShareboardclickCallback(shareBoardlistener);
        shareAction.open(setControlPanelStyle());

    }

    /**
     * 设置控制面板的样式
     *
     * @return
     */
    private ShareBoardConfig setControlPanelStyle() {
        ShareBoardConfig config = new ShareBoardConfig();
        config.setTitleVisibility(false);                                   //隐藏标题
        config.setMenuItemBackgroundShape(ShareBoardConfig.BG_SHAPE_NONE);  //无背景无背景
        config.setCancelButtonText("取消");                                   //设置取消文案
        config.setShareboardBackgroundColor(Color.parseColor("#ffffff"));   //设置背景颜色
        config.setIndicatorVisibility(false);                               //隐藏指示器
        config.setMenuItemTextColor(Color.parseColor("#666666"));
        return config;
    }

    public MyShareBoardlistener getShareBoardlistener() {
        return shareBoardlistener;
    }
}
