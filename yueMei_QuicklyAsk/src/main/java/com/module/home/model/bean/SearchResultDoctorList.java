package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/6/5
 */
public class SearchResultDoctorList implements Parcelable {
    private String doctorsUserID;
    private String doctorsImg;
    private String doctorsName;
    private String doctorsTitle;
    private String doctorsTag;
    private String hospitalName;
    private String diary_pf;
    private String sku_order_num;
    private String jumpUrl;
    private String type;
    private SearchResultDoctorControlParams typeControlParams;
    private String doctorsCenterUrl;
    private String doctorsTagID;
    private HashMap<String,String> event_params;

    protected SearchResultDoctorList(Parcel in) {
        doctorsUserID = in.readString();
        doctorsImg = in.readString();
        doctorsName = in.readString();
        doctorsTitle = in.readString();
        doctorsTag = in.readString();
        hospitalName = in.readString();
        diary_pf = in.readString();
        sku_order_num = in.readString();
        jumpUrl = in.readString();
        type = in.readString();
        typeControlParams = in.readParcelable(SearchResultDoctorControlParams.class.getClassLoader());
        doctorsCenterUrl = in.readString();
        doctorsTagID = in.readString();
        event_params = (HashMap<String, String>) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(doctorsUserID);
        dest.writeString(doctorsImg);
        dest.writeString(doctorsName);
        dest.writeString(doctorsTitle);
        dest.writeString(doctorsTag);
        dest.writeString(hospitalName);
        dest.writeString(diary_pf);
        dest.writeString(sku_order_num);
        dest.writeString(jumpUrl);
        dest.writeString(type);
        dest.writeParcelable(typeControlParams, flags);
        dest.writeString(doctorsCenterUrl);
        dest.writeString(doctorsTagID);
        dest.writeMap(event_params);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchResultDoctorList> CREATOR = new Creator<SearchResultDoctorList>() {
        @Override
        public SearchResultDoctorList createFromParcel(Parcel in) {
            return new SearchResultDoctorList(in);
        }

        @Override
        public SearchResultDoctorList[] newArray(int size) {
            return new SearchResultDoctorList[size];
        }
    };

    public String getDoctorsUserID() {
        return doctorsUserID;
    }

    public void setDoctorsUserID(String doctorsUserID) {
        this.doctorsUserID = doctorsUserID;
    }

    public String getDoctorsImg() {
        return doctorsImg;
    }

    public void setDoctorsImg(String doctorsImg) {
        this.doctorsImg = doctorsImg;
    }

    public String getDoctorsName() {
        return doctorsName;
    }

    public void setDoctorsName(String doctorsName) {
        this.doctorsName = doctorsName;
    }

    public String getDoctorsTitle() {
        return doctorsTitle;
    }

    public void setDoctorsTitle(String doctorsTitle) {
        this.doctorsTitle = doctorsTitle;
    }

    public String getDoctorsTag() {
        return doctorsTag;
    }

    public void setDoctorsTag(String doctorsTag) {
        this.doctorsTag = doctorsTag;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getDiary_pf() {
        return diary_pf;
    }

    public void setDiary_pf(String diary_pf) {
        this.diary_pf = diary_pf;
    }

    public String getSku_order_num() {
        return sku_order_num;
    }

    public void setSku_order_num(String sku_order_num) {
        this.sku_order_num = sku_order_num;
    }

    public String getJumpUrl() {
        return jumpUrl;
    }

    public void setJumpUrl(String jumpUrl) {
        this.jumpUrl = jumpUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public SearchResultDoctorControlParams getTypeControlParams() {
        return typeControlParams;
    }

    public void setTypeControlParams(SearchResultDoctorControlParams typeControlParams) {
        this.typeControlParams = typeControlParams;
    }

    public String getDoctorsCenterUrl() {
        return doctorsCenterUrl;
    }

    public void setDoctorsCenterUrl(String doctorsCenterUrl) {
        this.doctorsCenterUrl = doctorsCenterUrl;
    }

    public String getDoctorsTagID() {
        return doctorsTagID;
    }

    public void setDoctorsTagID(String doctorsTagID) {
        this.doctorsTagID = doctorsTagID;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
