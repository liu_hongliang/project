package com.module.community.model.bean;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2019/7/17
 */
public class OcpaUserActionsDatas {
    private int account_id;
    private int user_action_set_id;
    private ArrayList<OcpaActionsData> actions;

    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    public int getUser_action_set_id() {
        return user_action_set_id;
    }

    public void setUser_action_set_id(int user_action_set_id) {
        this.user_action_set_id = user_action_set_id;
    }

    public ArrayList<OcpaActionsData> getActions() {
        return actions;
    }

    public void setActions(ArrayList<OcpaActionsData> actions) {
        this.actions = actions;
    }
}
