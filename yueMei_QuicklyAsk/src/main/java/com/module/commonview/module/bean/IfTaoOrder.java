package com.module.commonview.module.bean;

public class IfTaoOrder {

	private String code;
	private String message;
	private IfTaoOrderData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public IfTaoOrderData getData() {
		return data;
	}

	public void setData(IfTaoOrderData data) {
		this.data = data;
	}

}
