package com.module;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

/**
 * 文 件 名: AnalyticsConfig
 * 创 建 人: 原成昊
 * 创建日期: 2019-12-11 15:47
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class AnalyticsConfig {
    private String channel;
    private String version;
    private String brand;
    private String model;
    private String modelDisplay;
    private String androidVersion;
    private double[] location;

    private long timeStamp;

    static class Holder {
        static AnalyticsConfig instance = new AnalyticsConfig();
    }

    public static AnalyticsConfig getInstance() {
        return Holder.instance;
    }

    private AnalyticsConfig() {
        init();
    }

    private void init() {
        Context context = MyApplication.getContext();
        this.channel = "ziran";
        String pkName = context.getPackageName();
        try {
            this.version = context.getPackageManager().getPackageInfo(pkName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.brand = Build.BRAND;
        this.model = Build.MODEL;
        this.modelDisplay = Build.DISPLAY;
        this.androidVersion = Build.VERSION.RELEASE;
        this.timeStamp = System.currentTimeMillis();
    }

    public void updateConfig(){
        init();
    }

    long getTimeStamp() {
        return timeStamp;
    }

    void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    String getChannel() {
        return channel;
    }

    void setChannel(String channel) {
        this.channel = channel;
    }

    String getVersion() {
        return version;
    }

    void setVersion(String version) {
        this.version = version;
    }

    String getBrand() {
        return brand;
    }

    void setBrand(String brand) {
        this.brand = brand;
    }

    String getModel() {
        return model;
    }

    void setModel(String model) {
        this.model = model;
    }

    String getModelDisplay() {
        return modelDisplay;
    }

    void setModelDisplay(String modelDisplay) {
        this.modelDisplay = modelDisplay;
    }

    String getAndroidVersion() {
        return androidVersion;
    }

    void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    double[] getLocation() {
        return location;
    }

    void setLocation(double[] location) {
        this.location = location;
    }

    /**
     * Caution: this method is called in AppExceptionHandler.
     * The index of lines should be kept as it is,
     * which is more convenient for analytics.
     * So, if this method is changed, remember to modify the AppExceptionHandler.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("channel: ");
        sb.append(this.channel);
        sb.append("\n");
        sb.append("version: ");
        sb.append(this.version);
        sb.append("\n");
        sb.append("brand: ");
        sb.append(this.brand);
        sb.append("\n");
        sb.append("model: ");
        sb.append(this.model);
        sb.append("\n");
        sb.append("model_display: ");
        sb.append(this.modelDisplay);
        sb.append("\n");
        sb.append("android_version: ");
        sb.append(this.androidVersion);
        sb.append("\n");
        return new String(sb);
    }

}

