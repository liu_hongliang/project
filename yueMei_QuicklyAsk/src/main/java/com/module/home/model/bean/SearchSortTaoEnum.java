package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2019/5/24
 */
public enum SearchSortTaoEnum {
    DATA,                       //淘数据
    METHODS,                    //方法（多选）
    ACTIVITY,                   //活动（单选）
    LIKE,                       //你可能会喜欢
    CHANGE_WORD                 //提示标签
//    NO_MORE,                    //没有更多数据
//    NOT_FAILED,                 //未能查到相关数据
//    CHANGE_WORD                 //提示换词搜索
}
