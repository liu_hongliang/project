package com.quicklyask.entity;

/**
 * Created by dwb on 16/9/7.
 */
public class HjToen {

    private String code;
    private String message;
    private HjToenData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HjToenData getData() {
        return data;
    }

    public void setData(HjToenData data) {
        this.data = data;
    }
}
