package com.quicklyask.entity;

/**
 * Created by 裴成浩 on 2017/8/21.
 */

public class tupianData {
    private ZhuangTai zhaungtai;
    private String  title;
    private String jindu;
    private String bendiPath;
    private String wanluoPath;

    public ZhuangTai getZhaungtai() {
        return zhaungtai;
    }

    public void setZhaungtai(ZhuangTai zhaungtai) {
        this.zhaungtai = zhaungtai;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJindu() {
        return jindu;
    }

    public void setJindu(String jindu) {
        this.jindu = jindu;
    }

    public String getBendiPath() {
        return bendiPath;
    }

    public void setBendiPath(String bendiPath) {
        this.bendiPath = bendiPath;
    }

    public String getWanluoPath() {
        return wanluoPath;
    }

    public void setWanluoPath(String wanluoPath) {
        this.wanluoPath = wanluoPath;
    }

    class ZhuangTai{
        private String dengdai;
        private String chenggong;
        private String scz;

        public String getDengdai() {
            return dengdai;
        }

        public void setDengdai(String dengdai) {
            this.dengdai = dengdai;
        }

        public String getChenggong() {
            return chenggong;
        }

        public void setChenggong(String chenggong) {
            this.chenggong = chenggong;
        }

        public String getScz() {
            return scz;
        }

        public void setScz(String scz) {
            this.scz = scz;
        }
    }
}
