package com.quicklyask.entity;

public class WriteResultData {

	private String onelogin;
	private String _id;
	private String appmurl;
	private String is_new;

	public String getOnelogin() {
		return onelogin;
	}

	public void setOnelogin(String onelogin) {
		this.onelogin = onelogin;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getAppmurl() {
		return appmurl;
	}

	public void setAppmurl(String appmurl) {
		this.appmurl = appmurl;
	}

	public String getIs_new() {
		return is_new;
	}

	public void setIs_new(String is_new) {
		this.is_new = is_new;
	}
}
