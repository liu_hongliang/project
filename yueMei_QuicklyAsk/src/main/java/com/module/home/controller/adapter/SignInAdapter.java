package com.module.home.controller.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.entity.SignInResult;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ${裴成浩} on 2017/6/6.
 */

public class SignInAdapter extends RecyclerView.Adapter<SignInAdapter.ViewHolder>{


    private final Context mContext;
    private final LayoutInflater mInflater;
    private final int mContinueSignin;
    private final List<SignInResult.DataBean.IsDaySigninBean> mDatas;
    private boolean mIsSignin = false;
    public HashMap<String,ViewHolder> map = new HashMap<>();

    public SignInAdapter(Context context, List<SignInResult.DataBean.IsDaySigninBean> mDatas, String continueSignin) {
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        this.mDatas = mDatas;
        int anInt = Integer.parseInt(continueSignin);
        this.mContinueSignin = anInt;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.sign_in_item, parent, false);
        return new ViewHolder(itemView) ;        //把这个布局传到ViewHolder中
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        map.put(position+"",holder);
        SignInResult.DataBean.IsDaySigninBean isDaySigninBean = mDatas.get(position);

        if ("1".equals(isDaySigninBean.getIs_signin())) {                       //已签到的天数
            Log.e("TAG", "aaa");
            holder.ivItemSign.setVisibility(View.VISIBLE);                      //已签到图标显示
            holder.ivItemSign.setImageResource(R.drawable.already_signed_in);   //图标改为已签图标

            if(mIsSignin){                                                           //是局部刷新时调用的
                Log.e("TAG", "aaa111");
                holder.ivItemSignQd.setVisibility(View.INVISIBLE);                       //对钩隐藏
            }else {                                                                 //是初始化调用的
                Log.e("TAG", "aaa222");
                holder.ivItemSignQd.setVisibility(View.VISIBLE);                    //对钩显示
            }

            holder.tvNoSign.setVisibility(View.GONE);                           //未签到文本隐藏
            holder.tvItemSign.setText("已签");                                   //文本改为已签
            holder.tvItemSign.setTextColor(Color.parseColor("#333333"));

            if(!"".equals(isDaySigninBean.getUrl())){            //如果是领奖天数
                Log.e("TAG", "bbb");
                holder.ivItemSign.setImageResource(R.drawable.gift_bag_yes);       //图标换为领奖图标
            }


        } else if ("0".equals(isDaySigninBean.getIs_signin())) {                    //未签到的天数
            Log.e("TAG", "ccc");
            holder.ivItemSignQd.setVisibility(View.GONE);                               //对钩隐藏
            holder.tvNoSign.setVisibility(View.VISIBLE);                                //未签到显示
            holder.ivItemSign.setVisibility(View.GONE);
            holder.tvItemSign.setText(isDaySigninBean.getDay() + "天");                 //文本改为天数
            holder.tvItemSign.setTextColor(Color.parseColor("#b5b5b5"));
            holder.tvNoSign.setText("+" + isDaySigninBean.getIntegral());               //未签到文本改变

            if(!"".equals(isDaySigninBean.getUrl())){                                   //如果是领奖天数
                Log.e("TAG", "ddd");
                holder.ivItemSign.setVisibility(View.VISIBLE);                          //签到图标显示
                holder.tvNoSign.setVisibility(View.GONE);                               //未签到文本隐藏
                holder.ivItemSign.setImageResource(R.drawable.gift_bag_no);             //图标换为未领奖图标
            }

        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public void setIsSignin(boolean isSignin){
        mIsSignin = isSignin;
    }

    public HashMap<String,ViewHolder> getHolder(){
        return map;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public ImageView ivItemSign;
        public ImageView ivItemSignQd;
        public TextView tvItemSign;
        public TextView tvNoSign;

        public ViewHolder(View itemView) {
            super (itemView);
            view = itemView;
            ivItemSign = itemView.findViewById(R.id.iv_item_sign);
            tvItemSign = itemView.findViewById(R.id.tv_item_sign);
            tvNoSign = itemView.findViewById(R.id.tv_no_sign);
            ivItemSignQd = itemView.findViewById(R.id.iv_item_sign_qd);
        }
    }
}
