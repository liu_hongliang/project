package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseActivity;
import com.module.commonview.PageJumpManager;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.my.controller.other.QuestionAnswerWebViewClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.umeng.socialize.utils.Log;

import org.json.JSONObject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

import static com.quicklyask.activity.R.id.wan_beautifu_docname;

/**
 * 我的问答
 */
public class QuestionAnswerActivity extends YMBaseActivity {
    private String TAG = "QuestionAnswerActivity";

    private RelativeLayout contentWeb;

    private QuestionAnswerActivity mActivity;
    public PageJumpManager pageJumpManager;
    private String uid;
    private String url;
    private String mTitle;
    private WebView docDetWeb;
    private JSONObject obj_http;
    private String userAgent;
    private RelativeLayout mBack;
    private TextView titleBarTv;
    private CookieManager cm;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private RelativeLayout rl;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_question_answer;
    }


    protected void initView() {
        mActivity = QuestionAnswerActivity.this;
        pageJumpManager = new PageJumpManager(mActivity);
        uid = Utils.getUid();
        cm = CookieManager.getInstance();
        Intent it0 = getIntent();
        url = it0.getStringExtra("url");
        mTitle = it0.getStringExtra("title");

        mBack = findViewById(R.id.wan_beautiful_web_back);
        titleBarTv = findViewById(wan_beautifu_docname);
        contentWeb = findViewById(R.id.rl_question_answer);
        rl = findViewById(R.id.rl);
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rl.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;

        baseWebViewClientMessage = new BaseWebViewClientMessage(mActivity);
        baseWebViewClientMessage.setBaseWebViewClientCallback(new QuestionAnswerWebViewClient(mActivity));

        initWebview();
        LodUrl(url, "1");

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void initData() {

    }


    @SuppressLint("InlinedApi")
    public void initWebview() {
        docDetWeb = new WebView(mActivity);

        docDetWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示
        docDetWeb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        docDetWeb.loadData("", "text/html", "UTF-8");

        docDetWeb.setLongClickable(true);
        docDetWeb.setScrollbarFadingEnabled(true);
        docDetWeb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        docDetWeb.setDrawingCacheEnabled(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                String[] strs = message.split("\n");

                showDialogExitEdit2("确定", message, result, strs.length);

                return true;
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if ("0".equals(mTitle)) {
                    titleBarTv.setText(title);
                } else {
                    titleBarTv.setText(mTitle);
                }

            }

        });

        //设置 缓存模式
        WebSettings settings = docDetWeb.getSettings();
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);

        // 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setLoadsImagesAutomatically(true);    //支持自动加载图片
        settings.setLoadWithOverviewMode(true);
        settings.setAllowFileAccess(true);
        settings.setSaveFormData(true);    //设置webview保存表单数据
        settings.setSavePassword(true);    //设置webview保存密码
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        settings.setSupportZoom(true);    //支持缩放
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        settings.setJavaScriptEnabled(true);                //支持js
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.supportMultipleWindows();
        settings.setNeedInitialFocus(true);
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(
                    WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }

        userAgent = settings.getUserAgentString() + "/yuemei.com";
        settings.setUserAgentString(userAgent);

        Log.e(TAG, "contentWeb == " + contentWeb);
        Log.e(TAG, "docDetWeb == " + docDetWeb);
        contentWeb.addView(docDetWeb);
    }

    public void LodUrl(String url, String flag) {
        baseWebViewClientMessage.startLoading();

        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("flag",flag);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url,keyValues);
        if (null != docDetWeb) {
            docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }
    }

    /**
     * dialog提示
     *
     * @param title
     * @param content
     * @param num
     */
    void showDialogExitEdit2(String title, String content, final JsResult result, int num) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        result.confirm();       //关闭js的弹窗
        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dilog_newuser_yizhuce1);


        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mActivity, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(title);
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });

    }
}
