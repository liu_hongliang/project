package com.module.community.controller.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.home.model.bean.SearchTaolistData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

public class SearchActiveItemAdapter extends BaseQuickAdapter<SearchTaolistData, BaseViewHolder> {
    public SearchActiveItemAdapter(int layoutResId, @Nullable List<SearchTaolistData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SearchTaolistData item) {
        Glide.with(mContext)
                .load(item.getImg())
                .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .into((ImageView) helper.getView(R.id.active_list_item_img));
        helper.setText(R.id.active_list_item_title,item.getBilateral_title())
                .setText(R.id.active_list_item_price,"¥"+item.getPrice());
        Glide.with(mContext).load(item.getImg_resouce()).into((ImageView) helper.getView(R.id.active_list_item_topimg));
    }
}
