package com.module.commonview.utils;

import android.os.Build;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.community.model.bean.HomeCommunityTagData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 自定义item设置TabLayout指示器长度设置
 * Created by 裴成浩 on 2018/7/26.
 */
public class TabLayoutIndicator1 {
    private String TAG = "TabLayoutIndicator1";

    public static TabLayoutIndicator1 newInstance() {
        TabLayoutIndicator1 layoutIndicator1 = new TabLayoutIndicator1();
        return layoutIndicator1;
    }

    public void reflex(final TabLayout tabLayout, final int left, final int right, final List<HomeCommunityTagData> tags) {
        //了解源码得知 线的宽度是根据 tabView的宽度来设置的
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                try {
                    //拿到tabLayout的mTabStrip属性
                    //在API28下 调用：tabLayout.getDeclaredField("mTabStrip")

                    Log.e(TAG, "Build.VERSION.SDK_INT == " + Build.VERSION.SDK_INT);
//                    Field mTabStripField;
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//                        mTabStripField = tabLayout.getClass().getDeclaredField("slidingTabIndicator");
//                    } else {
//                        mTabStripField = tabLayout.getClass().getDeclaredField("mTabStrip");
//                    }

                    Field mTabStripField = tabLayout.getClass().getDeclaredField("slidingTabIndicator");
                    mTabStripField.setAccessible(true);
                    LinearLayout mTabStrip = (LinearLayout) mTabStripField.get(tabLayout);

                    for (int i = 0; i < mTabStrip.getChildCount(); i++) {

                        int isHot = tags.get(i).getIs_hot();

                        View tabView = mTabStrip.getChildAt(i);
                        TextView mTextView = tabView.findViewById(R.id.comminoty_tab_title);

                        tabView.setPadding(0, 0, 0, 0);

                        //因为我想要的效果是   字多宽线就多宽，所以测量mTextView的宽度
                        int width = 0;
                        width = mTextView.getWidth();
                        if (width == 0) {
                            mTextView.measure(0, 0);
                            width = mTextView.getMeasuredWidth();
                        }

                        //设置tab左右间距为10dp  注意这里不能使用Padding 因为源码中线的宽度是根据 tabView的宽度来设置的
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tabView.getLayoutParams();
                        if (isHot == 1) {
                            params.width = width + Utils.dip2px(20);
                            params.rightMargin = left - Utils.dip2px(15) < 0 ? 0 : left - Utils.dip2px(15);

                        } else {
                            params.width = width;
                            params.rightMargin = left;
                        }

                        params.leftMargin = right;
                        tabView.setLayoutParams(params);

                        tabView.invalidate();
                    }

                } catch (NoSuchFieldException e) {
                    Log.e(TAG, "NoSuchFieldException === " + e.toString());
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    Log.e(TAG, "IllegalAccessException === " + e.toString());
                    e.printStackTrace();
                }
            }
        });

    }
}
