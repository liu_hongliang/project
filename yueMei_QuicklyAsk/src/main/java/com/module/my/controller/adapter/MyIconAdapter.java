package com.module.my.controller.adapter;

import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.MyApplication;
import com.module.my.model.bean.MyIconBean;
import com.quicklyask.activity.R;

import java.util.List;

public class MyIconAdapter extends BaseQuickAdapter<MyIconBean, BaseViewHolder> {
    private final int windowsWight;
    private List<MyIconBean> mData;

    public MyIconAdapter(int layoutResId, @Nullable List<MyIconBean> data, int windowsWight) {
        super(layoutResId, data);
        mData=data;
        this.windowsWight = windowsWight;
    }

    @Override
    protected void convert(BaseViewHolder helper, MyIconBean item) {
        LinearLayout itemView = helper.getView(R.id.my_icon_list_item);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) itemView.getLayoutParams();
        layoutParams.width = windowsWight / 4;
        itemView.setLayoutParams(layoutParams);

        helper.setText(R.id.my_item_name, item.getName());
        ImageView imageView = helper.getView(R.id.my_item_img);
        Glide.with(mContext).load(item.getIcon()).into(imageView);
//        helper.setImageDrawable(R.id.my_item_img, ContextCompat.getDrawable(MyApplication.getContext(), item.getIcon()));
        String num = item.getNum();
        if (!TextUtils.isEmpty(num) && !"0".equals(num)) {
            helper.setVisible(R.id.my_staypay_news, true);
            if (Integer.parseInt(num) <= 99) {
                helper.setText(R.id.my_staypay_news, num);

            } else {
                helper.setText(R.id.my_staypay_news, "99+");
            }

        } else {
            helper.setVisible(R.id.my_staypay_news, false);
        }
        String isSign = item.getIsSign();
        if (!TextUtils.isEmpty(isSign) && "0".equals(isSign)){
            helper.setVisible(R.id.sign_dot,true);
        }else {
            helper.setVisible(R.id.sign_dot,false);
        }

    }


    public void addData(int position,MyIconBean item){
        mData.add(position,item);
        notifyItemInserted(position);
    }

    public void removeData(int position){
        mData.remove(position);
        //删除动画
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }
}
