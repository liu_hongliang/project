package com.module.home.model.bean;

/**
 * 首页四卡数据
 * Created by 裴成浩 on 2019/9/19
 */
public class HomeFourColumnData {
    private HomeActivityData leftData;
    private HomeActivityData rightData;


    public HomeActivityData getLeftData() {
        return leftData;
    }

    public void setLeftData(HomeActivityData leftData) {
        this.leftData = leftData;
    }

    public HomeActivityData getRightData() {
        return rightData;
    }

    public void setRightData(HomeActivityData rightData) {
        this.rightData = rightData;
    }
}
