package com.module.community.statistical;

import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.module.MainTableActivity;
import com.module.MyApplication;
import com.module.community.controller.activity.CommunityActivity;
import com.module.community.model.bean.HomeCommunityTagData;
import com.module.community.model.bean.StatisticalData;
import com.module.community.model.bean.StatisticalPathData;
import com.module.home.fragment.SearchResultsFragment;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.BindData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * 无埋点路径统计
 * Created by 裴成浩 on 2018/10/11.
 */
@Aspect
public class AspectJPath {

    private String TAG = "AspectJPath";
    private List<String> mPaths = new ArrayList<>();                            //activity执行顺序列表
    private Map<String, StatisticalData> mPathParams = new Hashtable<>();       //通过activity的对象获取当前页面的链接和参数
    private Map<String, String> acMaps = new Hashtable<>();                     //通过控制器+方法名获取到当前链接的参数

    /**
     * 所有Activity的onCreate()统计
     *
     * @param joinPoint
     */
    @Before("execution(* android.app.Activity.onCreate(android.os.Bundle))")
    public void methodCreate(JoinPoint joinPoint) {
        String canonicalName = joinPoint.getThis().toString();
        addActivityList(canonicalName);
    }


    /**
     * 主页面5个导航按钮
     *
     * @param joinPoint
     */
    @Before("execution(* com.module.commonview.view.MainTableButtonView.bnBottomClickListener(..))")
    public void methodBottomClick(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        int arg = (int) args[0];
//        Log.e(TAG, "methodBottomClick ---> arg == " + arg);
        switch (arg) {
            case 0:
                activitySorting("HomeActivity623");
                break;
            case 1:
                activitySorting("TaoActivity623");
                break;
            case 2:
                activitySorting("CommunityActivity");
                break;
            case 3:
                activitySorting("MessageFragmentActivity1");
                break;
            case 4:
                activitySorting("HomePersonalActivity550");
                break;
        }
    }

    /**
     * 对avtivity进行排序
     *
     * @param avtivity :要换位置的avtivity
     */
    private void activitySorting(String avtivity) {
        for (int i = 0; i < mPaths.size(); i++) {
            String s = mPaths.get(i);
            if (s.contains(avtivity)) {
                mPaths.remove(i);
                mPaths.add(s);
                break;
            }
        }
    }

    /**
     * 社区页面tab标签选中
     *
     * @param joinPoint
     * @throws UnsupportedEncodingException
     */
    @Before("execution(* com.module.community.controller.activity.CommunityActivity.tabSelected(android.support.design.widget.TabLayout.Tab))")
    public void communityTab(JoinPoint joinPoint) throws UnsupportedEncodingException {

        for (String key : mPathParams.keySet()) {
            Log.e(TAG, "key == " + key);
            if (key.contains("CommunityActivity")) {
                //获取社区页面所有的页面链接
                CommunityActivity activity = (CommunityActivity) joinPoint.getThis();
                List<HomeCommunityTagData> mData = activity.tags;

                //获取社区当前选中页面
                Object[] args = joinPoint.getArgs();
                TabLayout.Tab tab = (TabLayout.Tab) args[0];
                int position = tab.getPosition();

                //切换为当前的fragment的链接
                HomeCommunityTagData tagData = mData.get(position);

                String params = acMaps.get(tagData.getController() + tagData.getAction());
                mPathParams.put(key, new StatisticalData(URLEncoder.encode(tagData.getUrl(), "utf-8"), TextUtils.isEmpty(params) ? "" : params));

                break;
            }
        }
    }

    /**
     * 搜索页面tab标签选中
     *
     * @param joinPoint
     * @throws UnsupportedEncodingException
     */
    @Before("execution(* com.module.home.fragment.SearchResultsFragment.tabSelected(int))")
    public void searchAllTab(JoinPoint joinPoint) throws UnsupportedEncodingException {

        for (String key : mPathParams.keySet()) {
            Log.e(TAG, "key == " + key);
            Object aThis = joinPoint.getThis();
            //获取搜索页面所有的页面链接
            if (key.contains("SearchAllActivity") && aThis instanceof SearchResultsFragment) {
                SearchResultsFragment fragment = (SearchResultsFragment) joinPoint.getThis();
                String[] mData = fragment.fragmentUrlList;

                //获取搜索当前选中页面
                Object[] args = joinPoint.getArgs();
                int position = (int) args[0];

                //切换为当前的fragment的参数
                String params = mData[position];
                Log.e(TAG, "params == " + params);

                mPathParams.put(key, new StatisticalData(URLEncoder.encode(FinalConstant.SEARCH_613, "utf-8"), TextUtils.isEmpty(params) ? "" : params));
                break;
            }
        }
    }

    /**
     * 我的收藏页面tab标签选中
     *
     * @param joinPoint
     * @throws UnsupportedEncodingException
     */
    @Before("execution(* com.module.my.controller.activity.MyCollectActivity550.initTab(..))")
    public void collectTab(JoinPoint joinPoint) throws UnsupportedEncodingException {
        for (String key : mPathParams.keySet()) {
            Log.e(TAG, "key == " + key);
            if (key.contains("MyCollectActivity550")) {

                Object[] args = joinPoint.getArgs();
                int position = (int) args[0];

                String aAndC = "";
                switch (position) {
                    case 0:
                        aAndC = "usernewmycollecttao";
                        break;
                    case 1:
                        aAndC = "usernewmycollectpost";
                        break;
                    case 2:
                        aAndC = "usernewmycollectbaike";
                        break;
                }

                String params = acMaps.get(aAndC);
                BindData bindData = NetWork.getInstance().getBindData(aAndC);

                String url = "";
                if (bindData != null) {
                    url = bindData.getUrl();
                }

                mPathParams.put(key, new StatisticalData(URLEncoder.encode(url, "utf-8"), TextUtils.isEmpty(params) ? "" : params));

                break;
            }
        }
    }

    /**
     * 联网请求时保存参数
     *
     * @param joinPoint
     */
    @SuppressWarnings("unchecked")
    @After("execution(* com.module.other.netWork.netWork.NetWork.call(java.lang.String, java.lang.String, java.util.Map<java.lang.String,java.lang.Object>, com.lzy.okgo.model.HttpParams, com.module.other.netWork.netWork.ServerCallback))")
    public void classApi(JoinPoint joinPoint) throws UnsupportedEncodingException {
        //上级来源设置
        setSource(joinPoint);

    }

    /**
     * 上级来源设置
     *
     * @param joinPoint
     * @throws UnsupportedEncodingException
     */
    private void setSource(JoinPoint joinPoint) throws UnsupportedEncodingException {
        Object[] args = joinPoint.getArgs();
        String controller = (String) args[0];

        String action = (String) args[1];
        String requestParams = new Gson().toJson(SignUtils.buildHttpParamMap((Map<String, Object>) args[2]));

        //循环找出这个网络请求是否是要统计的请求
        List<StatisticalPathData> pathData = StatisticalPath.getInstance().getPathData();
        if (pathData.size() == 0) {
            String messageArr = Cfg.loadStr(MyApplication.getContext(), FinalConstant.MESSAGE_ARR, "");
            if (!TextUtils.isEmpty(messageArr)) {
                pathData = JSONUtil.jsonToArrayList(messageArr, StatisticalPathData.class);
            }
        }

        for (StatisticalPathData data : pathData) {
            if (controller.equals(data.getController()) && action.equals(data.getAction())) {
                //获取网络请求当前的Activity
                for (int i = mPaths.size() - 1; i >= 0; i--) {
                    String path = mPaths.get(i);
                    String topActivity = MyApplication.activityAddress;                 //Application获取栈顶是地址值
                    BindData bindData = NetWork.getInstance().getBindData(controller + action);
                    String url = "";
                    if (bindData != null) {
                        url = bindData.getUrl();
                    }

                    //添加链接的参数
                    acMaps.put(controller + action, requestParams);

                    if (topActivity != null && path.equals(topActivity)) {
                        mPathParams.put(path, new StatisticalData(URLEncoder.encode(url, "utf-8"), requestParams));
                        getUploadParameter(topActivity);
                        break;
                    }
                }
                break;
            }
        }
    }

    /**
     * web页面统计
     *
     * @param joinPoint
     */
    @SuppressWarnings("unchecked")
    @After("execution(* com.module.other.netWork.SignUtils.getAddressAndHead(java.lang.String, java.util.Map<java.lang.String,java.lang.Object>, java.util.Map<java.lang.String,java.lang.Object>))")
    public void classWebView(JoinPoint joinPoint) throws UnsupportedEncodingException {
        Object[] args = joinPoint.getArgs();
        String url = (String) args[0];
        String requestParams = new Gson().toJson(SignUtils.buildHttpParamMap((Map<String, Object>) args[1]));

        List<StatisticalPathData> pathData = StatisticalPath.getInstance().getPathData();
        if (pathData.size() == 0) {
            String messageArr = Cfg.loadStr(MyApplication.getContext(), FinalConstant.MESSAGE_ARR, "");
            if (!TextUtils.isEmpty(messageArr)) {
                pathData = JSONUtil.jsonToArrayList(messageArr, StatisticalPathData.class);
            }
        }

        //循环找出这个网络请求是否是要统计的请求
        for (StatisticalPathData data : pathData) {
            if (url != null) {
                if (url.contains(data.getControllerAction())) {
                    //获取网络请求当前的Activity
                    for (int i = mPaths.size() - 1; i >= 0; i--) {
                        String path = mPaths.get(i);
                        String topActivity = MyApplication.activityAddress;                 //Application获取栈顶是地址值

                        //添加链接的参数
                        acMaps.put(data.getController() + data.getAction(), requestParams);

                        if (topActivity != null && path.equals(topActivity)) {
                            mPathParams.put(path, new StatisticalData(URLEncoder.encode(url, "utf-8"), requestParams));
                            getUploadParameter(topActivity);
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    /**
     * 所有Activity的onDestroy()统计
     *
     * @param joinPoint
     */
    @Before("execution(* android.app.Activity.onDestroy())")
    public void methodDestroy(JoinPoint joinPoint) {
        String canonicalName = joinPoint.getThis().toString();

        mPaths.remove(canonicalName);
        mPathParams.remove(canonicalName);
    }

    /**
     * 获取上传的参数
     *
     * @param canonicalName
     */
    private void getUploadParameter(String canonicalName) {
        StatisticalData statisticalData = mPathParams.get(canonicalName);

        Log.e(TAG, "statisticalData == " + statisticalData);
        if (statisticalData != null) {
            for (int i = mPaths.size() - 1; i >= 0; i--) {
                String path = mPaths.get(i);

                if (path.equals(canonicalName) && i > 0) {


                    /********************获取来源********************/

                    //获取来源
                    String refererUrl = "";
                    String refererParams = "";

                    //首页弹层特殊处理
                    if ((mPaths.get(i - 1).contains("HomeActivity623") || mPaths.get(i - 1).contains("TaoActivity623") || mPaths.get(i - 1).contains("CommunityActivity") || mPaths.get(i - 1).contains("MessageFragmentActivity1") || mPaths.get(i - 1).contains("HomePersonActivity")) && !TextUtils.isEmpty(MainTableActivity.tancengUrl)) {
                        refererUrl = MainTableActivity.tancengUrl;
                        MainTableActivity.tancengUrl = "";
                    } else {
                        StatisticalData statisticalData1 = mPathParams.get(mPaths.get(i - 1));

                        if (statisticalData1 != null) {
                            refererUrl = statisticalData1.getUrl();
                            refererParams = statisticalData1.getParams();
                        }
                    }

                    /********************获取来源********************/
                    //当前目标
                    String requestUrl = statisticalData.getUrl();
                    String requestParams = statisticalData.getParams();

                    //联网请求
                    uploadPath(refererUrl, refererParams, requestUrl, requestParams);
                    break;
                }
            }
        }
    }

    /**
     * 上传统计路径来源
     *
     * @param refererUrl：来源url
     * @param refererParams：来源参数
     * @param requestUrl：目标url
     * @param requestParams：目标参数
     */
    private void uploadPath(String refererUrl, String refererParams, String requestUrl, String requestParams) {

        Log.e(TAG, "uploadPath --- > 上级来源url == " + refererUrl);
        Log.e(TAG, "uploadPath --- > 上级来源Params == " + refererParams);
        Log.e(TAG, "uploadPath --- > 当前目标url == " + requestUrl);
        Log.e(TAG, "uploadPath --- > 当前目标Params == " + requestParams);

        //获取要统计的路径
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("referer_url", refererUrl);
        hashMap.put("referer_params", refererParams);
        hashMap.put("request_url", requestUrl);
        hashMap.put("request_params", requestParams);

        NetWork.getInstance().call(FinalConstant1.MESSAGE, "tongji", hashMap, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if ("1".equals(mData.code)) {
                    Log.e(TAG, "统计成功");
                } else {
                    Log.e(TAG, "统计失败");
                }
            }
        });
    }

    /**
     * 添加activity数据集合
     *
     * @param canonicalName
     */
    private void addActivityList(String canonicalName) {
        //判断是否存在相同的
        for (String path : mPaths) {
            if (canonicalName.equals(path)) {
                return;
            }
        }
        mPaths.add(canonicalName);
    }
}
