package com.quicklyask.entity;

public class LoginUserData {
	private String code;
	private String message;
	private UserData1 data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public UserData1 getData() {
		return data;
	}

	public void setData(UserData1 data) {
		this.data = data;
	}


}
