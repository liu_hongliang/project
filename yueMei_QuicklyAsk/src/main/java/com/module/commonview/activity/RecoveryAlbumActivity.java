package com.module.commonview.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.stream.JsonReader;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.module.api.PostoperativeRecoveryApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.adapter.PostoperativeAdapter;
import com.module.commonview.module.bean.PostoperativeRecoverBean;
import com.module.commonview.module.bean.UploadBean;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.UploadImgView;
import com.module.other.netWork.netWork.QiNuConfig;
import com.module.other.netWork.netWork.ServerData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.MyUploadImage2;
import com.quicklyask.util.Utils;
import com.quicklyask.util.XinJsonReader;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

public class RecoveryAlbumActivity extends YMBaseActivity {

    public static final String TAG="RecoveryAlbumActivity";
    @BindView(R.id.recover_album_container)
    RelativeLayout recoverAlbumContainer;
    @BindView(R.id.title_bar)
    CommonTopBar titleBar;
    @BindView(R.id.myself_postoperative_recyclerview)
    XRecyclerView myselfPostoperativeRecyclerview;
    TextView mMyselfItemTitle;  //术前标题
    TextView mMyselfItemStatus;  //状态
    LinearLayout mHeadContainer;
    private Activity mContext;

    private View mHeader;
    private UploadImgView fontUploadView;
    private UploadImgView sideUploadView;
    private UploadImgView otherUploadView;
    private UploadBean fontuploadBean;
    private UploadBean sideuploadBean;
    private UploadBean otheruploadBean;
    private PostoperativeRecoverBean.DataBean mDataBean;
    private PostoperativeAdapter mPostoperativeAdapter;
    private boolean mIsMyself = false;
    private boolean isShowHead = true;
    private boolean isClickable = false;
    private int[] mImageWidthHeight;
    private String mKey;
    public static final int SUCCESS = 11;
    public static final int ERROR = 12;
    ArrayList<PostoperativeRecoverBean.DataBean> dataBeans = new ArrayList<>(); //相册加载集合
    private ArrayList<String> mResults = new ArrayList<>();  //图片上传之后返回的本地路径单独一张的
    private ArrayList<String> mResultsMore = new ArrayList<>();  //图片上传之后返回的本地路径
    @Override
    protected int getLayoutId() {
        return R.layout.activity_postoperative_recovery;
    }

    private Handler mHandler = new MyHandler(this);
    private static class MyHandler extends Handler {
        private final WeakReference<RecoveryAlbumActivity> mActivity;

        public MyHandler(RecoveryAlbumActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            RecoveryAlbumActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case SUCCESS:
                        int pos = msg.arg1;
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("width", theActivity.mImageWidthHeight[0]);
                            jsonObject.put("height", theActivity.mImageWidthHeight[1]);
                            jsonObject.put("img", theActivity.mKey);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        theActivity.beforeData.put(pos + 1 + "", jsonObject);            //设置要上传的集合值
                        theActivity.mFunctionManager.showShort("上传成功");
                        theActivity.titleBar.setRightTextEnabled(true);
                        theActivity.isClickable = true;

                        break;
                    case ERROR:
                        break;
                }
            }
        }
    }

    @Override
    protected void initView() {
        mContext=RecoveryAlbumActivity.this;
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) recoverAlbumContainer.getLayoutParams();
        layoutParams.topMargin=statusbarHeight;
        recoverAlbumContainer.setLayoutParams(layoutParams);

        mHeader = LayoutInflater.from(mContext).inflate(R.layout.recovery_album_head, (ViewGroup) findViewById(android.R.id.content), false);
        mMyselfItemTitle = mHeader.findViewById(R.id.myself_item_title);
        mMyselfItemStatus = mHeader.findViewById(R.id.myself_item_status);
        mHeadContainer = mHeader.findViewById(R.id.head_container);

        fontUploadView = new UploadImgView(mContext);
        mHeadContainer.addView(fontUploadView);
        sideUploadView = new UploadImgView(mContext);
        mHeadContainer.addView(sideUploadView);
        otherUploadView = new UploadImgView(mContext);
        mHeadContainer.addView(otherUploadView);
        fontuploadBean = new UploadBean();
        fontuploadBean.setName("正面");
        sideuploadBean = new UploadBean();
        sideuploadBean.setName("侧面");
        otheruploadBean = new UploadBean();
        otheruploadBean.setName("其他");


    }

    @Override
    protected void initData() {
        HashMap<String, Object> maps = new HashMap<>();
        maps.put("id", "7302550");
        maps.put("page", 1 + "");
        new PostoperativeRecoveryApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)){
                    JsonReader jsonReader = new XinJsonReader(new StringReader(s.data));
                    jsonReader.setLenient(true);
                    PostoperativeRecoverBean postoperativeRecoverBean = JSONUtil.TransformSingleBean(jsonReader, PostoperativeRecoverBean.class);
                    List<PostoperativeRecoverBean.DataBean> data = postoperativeRecoverBean.getData();
                    mDataBean = data.get(0);//取出head头里面的数据
                    //把剩下加载列表的数据整合
                    for (int i = 1; i < data.size(); i++) {
                        dataBeans.add(data.get(i));
                    }
                    String user_id = mDataBean.getUser_id();
                    if (user_id.equals(Utils.getUid())){
                        mIsMyself=true;
                        mMyselfItemStatus.setVisibility(View.VISIBLE);
                        titleBar.setRightText("保存");
                        titleBar.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                    }
                    fontuploadBean.setMyself(mIsMyself);
                    sideuploadBean.setMyself(mIsMyself);
                    otheruploadBean.setMyself(mIsMyself);
                    List<PostoperativeRecoverBean.DataBean.PicBean> pic = mDataBean.getPic();
                    if (pic.size() > 0) {
                        setUploadBeanData(pic);
                    } else {
                        if (!mIsMyself) {
                            isShowHead = false;
                        } else {

                        }
                    }

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                    myselfPostoperativeRecyclerview.setHasFixedSize(true);
                    myselfPostoperativeRecyclerview.setNestedScrollingEnabled(false);
                    myselfPostoperativeRecyclerview.setFocusableInTouchMode(false);
                    myselfPostoperativeRecyclerview.setLayoutManager(linearLayoutManager);
                    mPostoperativeAdapter = new PostoperativeAdapter(mContext, dataBeans, true);
                    myselfPostoperativeRecyclerview.setAdapter(mPostoperativeAdapter);
                    myselfPostoperativeRecyclerview.addHeaderView(mHeader);
                }else {
                    mFunctionManager.showShort(s.message);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case UploadImgView.FROM_GALLERY:
                if (resultCode == RESULT_OK) {
                    if (null != data) {
                        int pos = data.getIntExtra("pos", 0);
                        mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                        Log.e(TAG, "onActivityResult  mResults === " + mResults.size());
                        if (mResults != null && mResults.size() > 0) {
                            mResultsMore.add(mResults.get(0));
                            upLoadFile(pos);
                            isClickable = false;
                            titleBar.setRightTextEnabled(false);

                        }
                    }
                }
                break;
        }
    }

    void upLoadFile(int pos) {
        // 压缩图片
        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);// 建立这个路径的文件或文件夹
        if (!path1.exists()) {// 如果不存在，就建立文件夹
            path1.mkdirs();
        }
        File file = new File(path1, "yuemei_" + System.currentTimeMillis() + ".jpg");
        String desPath = file.getPath();
        FileUtils.compressPicture(mResults.get(0), desPath);
        Bitmap bitmap = BitmapFactory.decodeFile(desPath);
        mImageWidthHeight = FileUtils.getImageWidthHeight(desPath);

//        switch (pos) {
//            case 0:
//                mPostoperativePhotoFront.setVisibility(View.VISIBLE);
//                mPostoperativePhotoFront.setImageBitmap(bitmap);
//                mPostoperativeFrontClick.setVisibility(View.GONE);
//                mPostoperativeDeleteFront.setVisibility(View.VISIBLE);
//                break;
//            case 1:
//                mPostoperativeSidePhoto.setVisibility(View.VISIBLE);
//                mPostoperativeSidePhoto.setImageBitmap(bitmap);
//                mPostoperativeSideClick.setVisibility(View.GONE);
//                mPostoperativeDeleteSide.setVisibility(View.VISIBLE);
//                break;
//            case 2:
//                mPostoperativeOtherPhoto.setVisibility(View.VISIBLE);
//                mPostoperativeOtherPhoto.setImageBitmap(bitmap);
//                mPostoperativeOtherClick.setVisibility(View.GONE);
//                mPostoperativeDeleteOther.setVisibility(View.VISIBLE);
//                break;
//        }
//        mResultData.add(mResults.get(0));
        isClickable = false;
        titleBar.setRightTextEnabled(false);
        mKey = QiNuConfig.getKey();
        MyUploadImage2.getMyUploadImage(mContext, pos, mHandler, desPath).uploadImage(mKey);
    }

    /**
     * 设置数据
     * @param pic
     */
    private void setUploadBeanData(List<PostoperativeRecoverBean.DataBean.PicBean> pic){
            for (int i = 0; i < pic.size(); i++) {
                if ("1".equals(pic.get(i).getWeight())) {
                    fontuploadBean.setHeight(pic.get(i).getHeight());
                    fontuploadBean.setImages(pic.get(i).getImages());
                    fontuploadBean.setImg(pic.get(i).getImg());
                    fontuploadBean.setIs_video(pic.get(i).getIs_video());
                    fontuploadBean.setPic_id(pic.get(i).getPic_id());
                    fontuploadBean.setWeight(pic.get(i).getWeight());
                    fontuploadBean.setWidth(pic.get(i).getWidth());
                    fontuploadBean.setVideo_url(pic.get(i).getVideo_url());
                    fontuploadBean.setIs_cover(true);
                    fontUploadView.setData(fontuploadBean);


                }
                if ("2".equals(pic.get(i).getWeight())) {
                    sideuploadBean.setHeight(pic.get(i).getHeight());
                    sideuploadBean.setImages(pic.get(i).getImages());
                    sideuploadBean.setImg(pic.get(i).getImg());
                    sideuploadBean.setIs_video(pic.get(i).getIs_video());
                    sideuploadBean.setPic_id(pic.get(i).getPic_id());
                    sideuploadBean.setWeight(pic.get(i).getWeight());
                    sideuploadBean.setWidth(pic.get(i).getWidth());
                    sideuploadBean.setVideo_url(pic.get(i).getVideo_url());
                    sideUploadView.setData(sideuploadBean);
                }
                if ("3".equals(pic.get(i).getWeight())) {
                    otheruploadBean.setHeight(pic.get(i).getHeight());
                    otheruploadBean.setImages(pic.get(i).getImages());
                    otheruploadBean.setImg(pic.get(i).getImg());
                    otheruploadBean.setIs_video(pic.get(i).getIs_video());
                    otheruploadBean.setPic_id(pic.get(i).getPic_id());
                    otheruploadBean.setWeight(pic.get(i).getWeight());
                    otheruploadBean.setWidth(pic.get(i).getWidth());
                    otheruploadBean.setVideo_url(pic.get(i).getVideo_url());
                    otherUploadView.setData(otheruploadBean);

                }
            }
        }



}
