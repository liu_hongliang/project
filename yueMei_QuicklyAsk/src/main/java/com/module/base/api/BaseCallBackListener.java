package com.module.base.api;

/**
 * Created by 裴成浩 on 2017/9/29.
 */

public interface BaseCallBackListener<T> {
    void onSuccess(T t);
}
