package com.module.taodetail.controller.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.cpoopc.scrollablelayoutlib.ScrollableLayout;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.activity.MainCitySelectActivity560;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.adapter.MyFragmentPagerAdapter;
import com.module.home.model.api.SearchShowDataApi;
import com.module.home.model.bean.SearchXSData;
import com.module.home.view.LoadingProgress;
import com.module.home.view.fragment.ScrollAbleFragment;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.module.taodetail.controller.activity.adapter.TagNavigationAdapter;
import com.module.taodetail.controller.activity.adapter.TaoWonderfulAdapter;
import com.module.taodetail.model.api.LoadHomeDataApi;
import com.module.taodetail.model.bean.TaoBoardItem;
import com.module.taodetail.model.bean.TaoTagItem;
import com.module.taodetail.model.bean.TaoZtItem;
import com.module.taodetail.model.bean.TaoZtItemTao;
import com.module.taodetail.view.fragment.ListTaoFragment;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoData623;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.CustomDialog;
import com.quicklyask.view.PagerSlidingTabStrip;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.utils.SystemTool;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import simplecache.ACache;


/**
 * 陶详情页面
 * Created by 裴成浩 on 17/7/28.
 */
public class TaoActivity623 extends FragmentActivity {
    //常用变量
    private final String TAG = "TaoActivity623";
    private TaoActivity623 mContext;
    private ACache mCache;

    //城市
    private String curCity;
    private String city = "全国";

    //标题栏变量
    private RelativeLayout citySelectRly;
    private TextView cityTv;
    private LinearLayout serachRly123;
    private TextView searchEt;
    private RelativeLayout mShoppingCart;
    private TextView taoCartNum;

    //导航球列表
    private GridView navigation;

    //精彩活动
    private LinearLayout wonderful;

    //淘整形横滑
    private PagerSlidingTabStrip pagerSlidingTabStrip;
    private ViewPager viewPager;
    private ArrayList<ScrollAbleFragment> fragmentList = new ArrayList<>();
    public static String[] partNameData;
    public static String[] partIdData;
    private List<String> titleList = new ArrayList<>();


    //首页json数据实体类
    private TaoData623 home;
    private TaoData623 homeData;
    private List<TaoTagItem> tagList;           //导航球实体类
    private List<TaoZtItem> ztList;             //中间模块的实体类
    private List<TaoBoardItem> boardList;       //底部淘整形实体类

    //其他
    private PtrClassicFrameLayout mPtrFrame;    //上拉加载下拉刷新
    private ScrollableLayout mScrollLayout;     //滚动

    private int windowsWight;           //屏幕的宽
    private SearchXSData searchShow;    //搜索框显示
    private LayoutInflater mInflater;   //加载xml资源
    private PageJumpManager pageJumpManager;
    private Map<String, String> projectDetailMap = new ArrayMap<>();
    private int statusbarHeight;
    private FrameLayout homeTitle;
    private LoadingProgress mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acty_tao_623);
        mContext = TaoActivity623.this;

        QMUIStatusBarHelper.setStatusBarLightMode(mContext);

        statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        Log.e(TAG, "statusbarHeight222 === " + statusbarHeight);

        pageJumpManager = new PageJumpManager(mContext);
        mDialog = new LoadingProgress(mContext);
        mCache = ACache.get(this);

        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
        mInflater = LayoutInflater.from(mContext);

        //图形初始化
        initView();
        //回调初始化
        initSetOnListener();
        //判断是否联网。来获取本地缓存数据，或者是网络数据
        getCachedData();
    }

    /**
     * 获取本地缓存数据
     */
    private void getCachedData() {
        if (SystemTool.checkNet(mContext)) {
            mDialog.startLoading();
            loadHomeData();      //获取初始化数据
        } else {
            String taojson = mCache.getAsString("taojson");
            Log.e("7777", "taojson === " + taojson);
            if (taojson != null) {
                laodHomeDataToView(taojson);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);

        curCity = Cfg.loadStr(mContext, FinalConstant.DWCITY, "");
        Log.e("dingwei", "陶整形获取的城市是 == " + curCity);

        if (curCity.length() > 0) {
            if (curCity.equals("失败")) {
                city = "全国";
                cityTv.setText(city);

            } else if (curCity.equals(city)) {

            } else {
                city = curCity;
                cityTv.setText(city);

                mDialog.startLoading();
                loadHomeData();
            }
        } else {
            city = "全国";
            cityTv.setText("全国");
        }
    }

    void initView() {
        //整体变量
        homeTitle = findViewById(R.id.ll_home_title);
        mPtrFrame = findViewById(R.id.rotate_header_web_view_frame);
        mScrollLayout = findViewById(R.id.scrollableLayout);

        //标题栏变量
        citySelectRly = findViewById(R.id.tab_doc_city_rly);
        cityTv = findViewById(R.id.tab_doc_list_city_tv);
        serachRly123 = findViewById(R.id.search_rly_click);
        searchEt = findViewById(R.id.home_input_edit1);
        mShoppingCart = findViewById(R.id.tao_title_shopping_cart);
        taoCartNum = findViewById(R.id.tao_cart_num);

        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            taoCartNum.setVisibility(View.VISIBLE);
            taoCartNum.setText(cartNumber);
        } else {
            taoCartNum.setVisibility(View.GONE);
        }

        ViewGroup.LayoutParams layoutParams = homeTitle.getLayoutParams();
        layoutParams.height = statusbarHeight + Utils.dip2px(50);
        homeTitle.setLayoutParams(layoutParams);

        initSearhShowData();     //搜索框按钮点击回调初始化

        //导航球列表
        navigation = findViewById(R.id.tao_navigation_623);

        //精彩活动
        wonderful = findViewById(R.id.tao_wonderful_623);

        //淘整形横滑
        viewPager = findViewById(R.id.viewpager123);
        pagerSlidingTabStrip = findViewById(R.id.pagerStrip123);

    }

    /**
     * 回调初始化
     */
    void initSetOnListener() {

        curCity = Cfg.loadStr(mContext, FinalConstant.TAOCITY, "");
        Cfg.saveStr(mContext, FinalConstant.TAOCITY, curCity);


        if (curCity.length() > 0) {
            if (curCity.equals("失败")) {
                city = "全国";
                cityTv.setText(city);
            } else {
                cityTv.setText(curCity);
                city = curCity;
            }
        } else {
            city = "全国";
            cityTv.setText("全国");
        }

        //吸顶统计
        mScrollLayout.setOnScrollListener(new ScrollableLayout.OnScrollListener() {
            @Override
            public void onScroll(int currentY, int maxY) {
                if (currentY == maxY) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAOTAB_CEILING, "1"));
                }
            }
        });

        citySelectRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Intent addIntent = new Intent();
                addIntent.setClass(mContext, MainCitySelectActivity560.class);
                addIntent.putExtra("curcity", city);
                addIntent.putExtra("type", "2");
                startActivityForResult(addIntent, 4);

                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_CITY, "tao", "tao"));
            }
        });


        //搜索框点击事件
        serachRly123.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (Utils.isFastDoubleClick()) {
                    return;
                }
                try {
                    TCAgent.onEvent(TaoActivity623.this, "首页搜索", "首页搜索");
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_SEARCH, "tao", "tao"));

                    Intent it = new Intent(mContext, SearchAllActivity668.class);
                    if (TextUtils.isEmpty(searchShow.getSearch_key())) {
                        it.putExtra(SearchAllActivity668.KEYS, searchShow.getSearch_key());
                    } else {
                        it.putExtra(SearchAllActivity668.KEYS, "");
                    }
                    it.putExtra(SearchAllActivity668.TYPE, "2");
                    startActivity(it);

                } catch (Exception e) {
                }

            }
        });

        //跳转到购物车
        mShoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, ShoppingCartActivity.class));
            }
        });

        //下拉刷新
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return mScrollLayout.canPtr();
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                mPtrFrame.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        home = null;
                        homeData = null;
                        tagList = null;
                        ztList = null;
                        boardList = null;

                        mDialog.startLoading();
                        loadHomeData();
                        initSearhShowData();
                    }
                }, 1000);

            }
        });

        // 下面是默认设置
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // 默认的是假的
        mPtrFrame.setPullToRefresh(false);
        // 默认是正确的
        mPtrFrame.setKeepHeaderWhenRefresh(true);

    }

    /**
     * 淘详情页数据加载
     */
    void loadHomeData() {

        Map<String, Object> keyValues = new HashMap<>();
        try {
            keyValues.put("city", URLEncoder.encode(city, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        new LoadHomeDataApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    mCache.put("taojson", serverData.data, 60 * 60 * 24);
                    laodHomeDataToView(serverData.data);
                } else {
                    mDialog.stopLoading();
                    mPtrFrame.refreshComplete();
                }
            }
        });

    }

    /**
     * 搜索框显示加载
     * InitSearhShowDataApi
     */
    void initSearhShowData() {
        new SearchShowDataApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<SearchXSData>() {
            @Override
            public void onSuccess(SearchXSData searchXSData) {
                searchShow = searchXSData;
                searchEt.setText(searchShow.getShow_key());
            }
        });
    }


    /**
     * 淘详情数据解析
     *
     * @param jsons：json数据
     */
    void laodHomeDataToView(String jsons) {
        try {
            homeData = JSONUtil.TransformSingleBean(jsons, TaoData623.class);
            tagList = homeData.getTag();         //导航球实体类
            ztList = homeData.getZt();          //中间模块的实体类
            boardList = homeData.getBoard();     //底部淘整形实体类

            mDialog.stopLoading();
            mPtrFrame.refreshComplete();

            //导航球列表
            tagList();

            //精彩活动
            wonderfulList();

            //淘整形列表
            taoList();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "e == " + e.toString());
        }

    }

    /**
     * 导航球列表
     */
    private void tagList() {
        if (null != tagList && tagList.size() > 0) {
            navigation.setVisibility(View.VISIBLE);
            //设置高度
            int size = tagList.size();     //导航球个数
            int lineNum = size / 5 + (size % 5 == 0 ? 0 : 1);//行数

            Log.e(TAG, "size == " + size);
            Log.e(TAG, "lineNum == " + lineNum);

            int navigationHeight = lineNum * (Utils.dip2px(mContext, 42) + Utils.dip2px(mContext, 8) + Utils.dip2px(mContext, 12) + navigation.getVerticalSpacing());//获取高度
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) navigation.getLayoutParams();
            params.height = navigationHeight;

            int margins = Utils.dip2px(mContext, 24) - (windowsWight / 5 - Utils.dip2px(mContext, 42)) / 2;
            params.setMargins(margins, Utils.dip2px(mContext, 16), margins, Utils.dip2px(mContext, 8));
            navigation.setLayoutParams(params);

            //适配数据
            TagNavigationAdapter adapter = new TagNavigationAdapter(mContext, tagList);
            navigation.setAdapter(adapter);

            navigation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                    if (tagList != null) {
                        TaoTagItem taoTagItem = tagList.get(pos);
                        //自己的统计
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TAG, (pos + 1) + "", "tao"));

                        //百度统计
                        baiduTongJi("068", taoTagItem.getTitle());

                        String one_id = taoTagItem.getOne_id();
                        String title = taoTagItem.getTitle();
                        String two_id = taoTagItem.getTwo_id();
                        String three_id = taoTagItem.getThree_id();
                        String medthod = taoTagItem.getMedthod();

                        projectDetailMap.put("oneid", one_id);
                        projectDetailMap.put("onetitle", title);
                        projectDetailMap.put("twoid", two_id);
                        projectDetailMap.put("twotitle", title);
                        projectDetailMap.put("threeid", three_id);
                        projectDetailMap.put("threetitle", title);
                        projectDetailMap.put("medthod", medthod);
                        pageJumpManager.jumpToProjectDetailActivity550(projectDetailMap);
                    }
                }
            });
        } else {
            navigation.setVisibility(View.GONE);
        }
    }

    /**
     * 精彩活动
     */
    private void wonderfulList() {
        if (null != ztList && ztList.size() > 0) {
            //先清空
            wonderful.removeAllViews();
            wonderful.setVisibility(View.VISIBLE);
            //添加item
            for (int i = 0; i < ztList.size(); i++) {
                View childView = mInflater.inflate(R.layout.item_tao_wonderfu, wonderful, false);
                ImageView wonderImg = childView.findViewById(R.id.wonder_img);
                RecyclerView wonderList = childView.findViewById(R.id.wonder_list);

                ViewGroup.LayoutParams params = wonderImg.getLayoutParams();
                params.width = windowsWight;
                params.height = (windowsWight * Utils.dip2px(5.5f) / Utils.dip2px(12.5f));
                wonderImg.setLayoutParams(params);

//                Picasso.with(mContext)
//                        .load(ztList.get(i).getImg())
//                        .config(Bitmap.Config.RGB_565)
//                        .placeholder(R.drawable.radius_gray80)
//                        .error(R.drawable.radius_gray80)
//                        .into(wonderImg);
                Glide.with(mContext)
                        .load(ztList.get(i).getImg())
                        .placeholder(R.drawable.radius_gray80)
                        .error(R.drawable.radius_gray80)
                        .into(wonderImg);

                final int wonderfulPos = i;
                wonderImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ztList != null) {
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_ZT, (wonderfulPos + 1) + "", "tao"), ztList.get(wonderfulPos).getEvent_params());
                            baiduTongJi("069", wonderfulPos + "");
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(ztList.get(wonderfulPos).getUrl(), "49", "0");
                        }
                    }
                });

                final List<TaoZtItemTao> taoData = ztList.get(wonderfulPos).getTao();
                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                wonderList.setLayoutManager(layoutManager);
                TaoWonderfulAdapter adapter = new TaoWonderfulAdapter(mContext, taoData, i);
                wonderList.setAdapter(adapter);

                //查看更多点击事件
                adapter.setOnItemClickMoreListener(new TaoWonderfulAdapter.OnItemClickMoreListener() {
                    @Override
                    public void onItemClickMore(int pos) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_ZT_TAO, (wonderfulPos + 1) + "-" + (pos + 1), "tao", "1"), ztList.get(wonderfulPos).getEvent_params());
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(ztList.get(wonderfulPos).getUrl(), "49", "0");
                    }
                });

                wonderful.addView(childView);
            }

            //添加分割线
            addDivider(10, "#f6f6f6", wonderful);

        } else {
            wonderful.setVisibility(View.GONE);
        }
    }


    /**
     * 淘整形列表
     */
    private void taoList() {

        if (fragmentList.size() > 0) {
            fragmentList.clear();
        }

        int num = boardList.size();
        partNameData = new String[num];
        partIdData = new String[num];

        for (int i = 0; i < num; i++) {
            String partNameStr = boardList.get(i).getTitle();
            partNameData[i] = partNameStr;
            Log.e(TAG, "partNameStr == " + partNameStr);
            titleList.add(partNameStr);
        }
        for (int i = 0; i < num; i++) {
            String parIdStr = boardList.get(i).getId();
            partIdData[i] = parIdStr;
        }

        for (int i = 0; i < num; i++) {
            String partId = partIdData[i];
            fragmentList.add(ListTaoFragment.newInstance(city, partId));
        }

        viewPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager(), fragmentList, titleList));
        mScrollLayout.getHelper().setCurrentScrollableContainer(fragmentList.get(0));

        pagerSlidingTabStrip.setViewPager(viewPager);
        pagerSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                Log.e("onPageSelected", "page:" + i);
                /** 标注当前页面 **/
                mScrollLayout.getHelper().setCurrentScrollableContainer(fragmentList.get(i));

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        viewPager.setCurrentItem(0);
    }

    /**
     * 添加分割线
     *
     * @param high：分割线高度（要传过来的是  dp）
     * @param color：分割线颜色
     * @param container：添加分割线的容器
     */
    private void addDivider(int high, String color, ViewGroup container) {
        //添加头部的分割线
        View view = new View(mContext);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dip2px(mContext, high));
        view.setBackgroundColor(Color.parseColor(color));
        container.addView(view, params);
    }

    /**
     * 百度统计
     *
     * @param eventId:事件ID,注意要先在mtj.baidu.com中注册此事件ID(产品注册)
     * @param label:自定义事件标签
     */
    private void baiduTongJi(String eventId, String label) {
        StatService.onEvent(
                mContext,
                eventId, label, 1);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            final CustomDialog dialog = new CustomDialog(getParent(),
                    R.style.mystyle, R.layout.customdialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            taoCartNum.setVisibility(View.VISIBLE);
            taoCartNum.setText(cartNumber);
        } else {
            taoCartNum.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
