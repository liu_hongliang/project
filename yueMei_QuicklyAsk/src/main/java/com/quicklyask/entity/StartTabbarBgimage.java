package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/9/28.
 */
public class StartTabbarBgimage implements Parcelable{
    private String img;
    private String img640;
    private String img750;
    private String img1125;
    private String img1242;

    protected StartTabbarBgimage(Parcel in) {
        img = in.readString();
        img640 = in.readString();
        img750 = in.readString();
        img1125 = in.readString();
        img1242 = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
        dest.writeString(img640);
        dest.writeString(img750);
        dest.writeString(img1125);
        dest.writeString(img1242);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StartTabbarBgimage> CREATOR = new Creator<StartTabbarBgimage>() {
        @Override
        public StartTabbarBgimage createFromParcel(Parcel in) {
            return new StartTabbarBgimage(in);
        }

        @Override
        public StartTabbarBgimage[] newArray(int size) {
            return new StartTabbarBgimage[size];
        }
    };

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg640() {
        return img640;
    }

    public void setImg640(String img640) {
        this.img640 = img640;
    }

    public String getImg750() {
        return img750;
    }

    public void setImg750(String img750) {
        this.img750 = img750;
    }

    public String getImg1125() {
        return img1125;
    }

    public void setImg1125(String img1125) {
        this.img1125 = img1125;
    }

    public String getImg1242() {
        return img1242;
    }

    public void setImg1242(String img1242) {
        this.img1242 = img1242;
    }
}
