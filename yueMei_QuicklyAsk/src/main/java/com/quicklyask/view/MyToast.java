package com.quicklyask.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 自定义提示Toast
 *
 * @author zihao
 */
public class MyToast extends Toast {
    public static final int SHOW_TIME = 1000;
    public static final int LENGTH_LONG = 3000;

    public MyToast(Context context) {
        super(context);
    }

    /**
     * 生成一个图文并存的Toast
     *
     * @param context  // 上下文对象
     * @param drawable // 要显示的图片
     * @param text     // 要显示的文字
     * @param duration // 显示时间
     * @return
     */
    @SuppressWarnings("deprecation")
    public static MyToast makeImgAndTextToast(Context context,
                                              Drawable drawable, CharSequence text, int duration) {
        MyToast result = new MyToast(context);

        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips, null);

        ImageView img = v.findViewById(R.id.tips_icon);
        img.setBackgroundDrawable(drawable);
        TextView tv = v.findViewById(R.id.tips_msg);
        tv.setText(text);

        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }

    /**
     * 生成一个只有文本的自定义Toast
     *
     * @param context  // 上下文对象
     * @param text     // 要显示的文字
     * @param duration // Toast显示时间
     * @return
     */
    public static MyToast makeTextToast(Context context, CharSequence text,
                                        int duration) {
        MyToast result = new MyToast(context);

        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips, null);
        TextView tv = v.findViewById(R.id.tips_msg);
        tv.setText(text);

        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }

    /**
     * 生成一个只有文本的自定义Toast
     *
     * @param context  // 上下文对象
     * @param text     // 要显示的文字
     * @param duration // Toast显示时间
     * @return
     */
    public static MyToast makeTexttext1Toast(Context context,
                                             CharSequence text, int duration) {
        MyToast result = new MyToast(context);

        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips2, null);
        TextView tv = v.findViewById(R.id.tips_msg2);
        tv.setText(text);

        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }

    /**
     * 生成一个只有文本的自定义Toast
     *
     * @param context  // 上下文对象
     * @param text     // 要显示的文字
     * @param duration // Toast显示时间
     * @return
     */
    public static MyToast makeTexttext2Toast(Context context,
                                             CharSequence text, int duration) {
        MyToast result = new MyToast(context);

        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips2, null);
        TextView tv = v.findViewById(R.id.tips_msg2);
        tv.setText(text);

        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }

    /**
     * 生成一个只有文本的自定义Toast
     *
     * @param context  // 上下文对象
     * @param text     // 要显示的文字
     * @param duration // Toast显示时间
     * @return
     */
    public static MyToast makeTexttext3Toast(Context context,
                                             CharSequence text, int duration) {
        MyToast result = new MyToast(context);

        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips3, null);
        TextView tv = v.findViewById(R.id.tips_msg3);
        tv.setText(text);

        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }

    /**
     * 生成一个只有文本的自定义Toast
     *
     * @param context  // 上下文对象
     * @param text     // 要显示的文字
     * @param duration // Toast显示时间
     * @return
     */
    public static MyToast makeTexttext4Toast(Context context,
                                             CharSequence text, CharSequence text1, int duration) {
        MyToast result = new MyToast(context);

        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips4, null);

        TextView tv = v.findViewById(R.id.tips_msg4);
        tv.setText(text);
        TextView tv1 = v.findViewById(R.id.tips_msg5);
        tv1.setText(text1);

        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }

    /**
     * 生成一个只有图片的自定义Toast
     *
     * @param context  // 上下文对象
     * @param drawable // 图片对象
     * @param duration // Toast显示时间
     * @return
     */
    @SuppressWarnings("deprecation")
    public static MyToast makeImgToast(Context context, Drawable drawable,
                                       int duration) {
        MyToast result = new MyToast(context);
        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips1, null);
        ImageView img = v.findViewById(R.id.tips_icon);
        img.setBackgroundDrawable(drawable);
        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }

    /**
     * 生成一个只有图片的自定义Toast
     *
     * @param context  // 上下文对象
     * @param drawable // 图片对象
     * @param duration // Toast显示时间
     * @return
     */
    @SuppressWarnings("deprecation")
    public static MyToast makeImg1Toast(Context context, Drawable drawable,
                                        int duration) {
        MyToast result = new MyToast(context);
        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips_pic, null);
        ImageView img = v.findViewById(R.id.tips_icon);
        img.setBackgroundDrawable(drawable);
        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }


    /**
     * 生成一个只有文字的自定义Toast
     *
     * @param context  // 上下文对象
     * @param text     // 显示文字
     * @param duration // Toast显示时间
     * @return
     */
    @SuppressWarnings("deprecation")
    public static MyToast makeTextToast1(Context context, String text, int duration) {
        MyToast result = new MyToast(context);
        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips5, null);

        TextView tex = v.findViewById(R.id.tips_msg3);
        tex.setText(text);
        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }

    /**
     * 生成一个只有文字的自定义Toast
     *
     * @param context
     * @param text
     * @param duration
     * @return
     */
    public static MyToast makeTextToast2(Context context, String text, int duration) {
        MyToast result = new MyToast(context);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tips6, null);

        TextView tex = v.findViewById(R.id.tips_msg3);
        tex.setText(text);
        tex.setGravity(Gravity.CENTER);
        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);

        return result;
    }
}