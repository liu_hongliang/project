package com.module.community.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.my.controller.activity.TypeProblemActivity;
import com.quicklyask.activity.R;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

public class SwitchPhotoPublicIfActivity extends BaseActivity {

	@BindView(id = R.id.bother_open_photo, click = true)
	private RelativeLayout openRly;
	@BindView(id = R.id.bother_close_photo, click = true)
	private RelativeLayout closeRly;

	@BindView(id = R.id.bother_open_iv_photo)
	private ImageView opneIv;
	@BindView(id = R.id.bother_close_iv_photo)
	private ImageView closeIv;

	@BindView(id = R.id.bother_back_photo, click = true)
	private RelativeLayout backIv;
	@BindView(id = R.id.photo_sumbit_rely, click = true)
	private RelativeLayout sumbitkBt;

	private Context mContext;

	private String type = "1";

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_photo_if_public_edit);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = SwitchPhotoPublicIfActivity.this;

		Intent intent = getIntent();
		if (intent != null) {
			type = intent.getStringExtra("type");
		}

		if (type.equals("1")) {
			opneIv.setVisibility(View.GONE);
			closeIv.setVisibility(View.VISIBLE);
		} else {
			opneIv.setVisibility(View.VISIBLE);
			closeIv.setVisibility(View.GONE);
		}

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						SwitchPhotoPublicIfActivity.this.finish();
					}
				});

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@Override
	protected void initWidget() {
		super.initWidget();
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.bother_open_photo:
			opneIv.setVisibility(View.VISIBLE);
			closeIv.setVisibility(View.GONE);
			type = "0";
			break;
		case R.id.bother_close_photo:
			opneIv.setVisibility(View.GONE);
			closeIv.setVisibility(View.VISIBLE);
			type = "1";
			break;
		case R.id.bother_back_photo:
			onBackPressed();
			break;
		case R.id.photo_sumbit_rely:
            Intent intent = new Intent(mContext, TypeProblemActivity.class);
            startActivity(intent);
			setResult(4, intent);
			onBackPressed();
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
