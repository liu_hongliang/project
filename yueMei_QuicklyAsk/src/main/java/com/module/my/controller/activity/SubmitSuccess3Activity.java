package com.module.my.controller.activity;

import com.baidu.mobstat.StatService;
import com.quicklyask.activity.R;

import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 我要变美提交成功
 * 
 * @author Rubin
 * 
 */
public class SubmitSuccess3Activity extends BaseActivity {

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_submit_success3);
	}

	public void onResume() {
		super.onResume();

		/**
		 * 页面起始（每个Activity中都需要添加，如果有继承的父Activity中已经添加了该调用，那么子Activity中务必不能添加）
		 * 不能与StatService.onPageStart一级onPageEnd函数交叉使用
		 */
		StatService.onResume(this);
	}

	public void onPause() {
		super.onPause();
		/**
		 * 页面结束（每个Activity中都需要添加，如果有继承的父Activity中已经添加了该调用，那么子Activity中务必不能添加）
		 * 不能与StatService.onPageStart一级onPageEnd函数交叉使用
		 */
		StatService.onPause(this);
	}
}
