package com.module.home.controller.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.fragment.ChannelBottomFragment;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.HashMap;

import butterknife.BindView;

public class ChannelFourActivity extends YMBaseActivity {

    @BindView(R.id.four_channel_top)
    CommonTopBar mTop;
    public String id;
    public ChannelBottomFragment channelBottomFragment;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_channel_four;
    }

    @Override
    protected void initView() {
        id = getIntent().getStringExtra("id");
        String title = getIntent().getStringExtra("title");
        String homeSource = getIntent().getStringExtra("home_source");
        mTop.setCenterText(title);

        //要传入的值
        ProjectDetailsBean detailsBean = new ProjectDetailsBean();
        detailsBean.setTwoLabelId("");
        detailsBean.setFourLabelId(id);
        detailsBean.setHomeSource(homeSource);

        channelBottomFragment = ChannelBottomFragment.newInstance(detailsBean);
        setActivityFragment(R.id.four_channel_fragment, channelBottomFragment);


        View view = View.inflate(mContext, R.layout.channel_shopping_cart_view, null);
        RelativeLayout cartView = view.findViewById(R.id.channel_shopping_cart_view);
        TextView num = view.findViewById(R.id.channel_shopping_cart_num);
        setCartNum(num);
        mTop.setCustomContainer(view, Utils.dip2px(30),Utils.dip2px(35));
        //跳转购物车页面
        cartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SHOPPING_CART_CLICK),hashMap,new ActivityTypeData("96"));
                startActivity(new Intent(mContext, ShoppingCartActivity.class));
            }
        });

    }


    @Override
    protected void initData() {

    }


    /**
     * 设置购物车数量
     */
    public void setCartNum(TextView num) {
        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            num.setVisibility(View.VISIBLE);
            num.setText(cartNumber);
        } else {
            num.setVisibility(View.GONE);
        }
    }
}
