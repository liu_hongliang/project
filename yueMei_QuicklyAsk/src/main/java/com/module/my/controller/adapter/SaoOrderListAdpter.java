package com.module.my.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.my.model.bean.SaoOrderListData;
import com.module.my.view.orderpay.SaoOrderMessageActivity;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

public class SaoOrderListAdpter extends BaseAdapter {

	private final String TAG = "SaoOrderListAdpter";

	private List<SaoOrderListData> mTaoPopItemData = new ArrayList<SaoOrderListData>();
	private Context mContext;
	private LayoutInflater inflater;
	private SaoOrderListData TaoPopItemData;
	ViewHolder viewHolder;

	public SaoOrderListAdpter(Context mContext,
			List<SaoOrderListData> mTaoPopItemData) {
		this.mContext = mContext;
		this.mTaoPopItemData = mTaoPopItemData;
		inflater = LayoutInflater.from(mContext);
	}

	static class ViewHolder {
		public TextView mNameTv;
		public TextView mPriceTv;
		public ImageView mIv;
	}

	@Override
	public int getCount() {
		return mTaoPopItemData.size();
	}

	@Override
	public Object getItem(int position) {
		return mTaoPopItemData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater
					.inflate(R.layout.item_sao_ordermessage, null);
			viewHolder = new ViewHolder();

			viewHolder.mNameTv = convertView
					.findViewById(R.id.item_order_title);
			viewHolder.mPriceTv = convertView
					.findViewById(R.id.item_order_price);
			viewHolder.mIv = convertView
					.findViewById(R.id.item_sao_check);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		TaoPopItemData = mTaoPopItemData.get(position);

		viewHolder.mNameTv.setText(TaoPopItemData.getDoc_name());

		String status = TaoPopItemData.getStatus();

		if (status.equals("1")) {
			viewHolder.mPriceTv.setText("未付订金");
		} else {
			String price = TaoPopItemData.getPrice();
			if (price.equals("0")) {
				viewHolder.mPriceTv.setText("未付订金");
			} else {
				viewHolder.mPriceTv.setText("已付订金￥" + price);
			}
		}

		if (position == SaoOrderMessageActivity.postion_sao) {
			viewHolder.mIv.setBackgroundResource(R.drawable.raido_dui3x);
		} else {
			viewHolder.mIv.setBackgroundResource(R.drawable.raido_cuo3x);
		}

		return convertView;
	}

	public void add(List<SaoOrderListData> infos) {
		mTaoPopItemData.addAll(infos);
	}
}
