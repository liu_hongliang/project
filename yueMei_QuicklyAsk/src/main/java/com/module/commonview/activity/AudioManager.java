package com.module.commonview.activity;

import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.module.MyApplication;
import com.quicklyask.util.Utils;
import com.zlw.main.recorderlib.BuildConfig;
import com.zlw.main.recorderlib.RecordManager;


import java.io.File;


public class AudioManager {
    public static final String TAG = "AudioManager";

    private final RecordManager recordManager;
    private String mDir;
    private String mCurrentFilePath;

    private static AudioManager mInstance;

    private boolean isPrepared;
    public AudioManager(String dir){
        mDir = dir;
        recordManager= RecordManager.getInstance();
        recordManager.init(MyApplication.getInstance(), BuildConfig.DEBUG);

    }


    /**
     * 回调准备完毕
     */
    public interface AudioStateListener {
        void wellPrepared();
    }

    public AudioStateListener mListener;

    public void setOnAudioStateListener(AudioStateListener listener){
        mListener = listener;
    }

    public static AudioManager getInstance(String dir){
        if (mInstance == null) {
            synchronized (AudioManager.class) {
                if (mInstance == null) {
                    mInstance = new AudioManager(dir);
                }
            }
        }
        return mInstance;
    }


    /**
     * 准备
     */
    public void prepareAudio(String _id) {

        isPrepared = false;
        File dir = new File(mDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }


        //设置输出文件
        recordManager.changeRecordDir(mDir);

        recordManager.setId(_id);
        recordManager.setUid(Utils.getUid());

        recordManager.start();

        //准备结束
        isPrepared = true;
        if (mListener != null) {
            mListener.wellPrepared();
        }

    }

    //    生成UUID唯一标示符
//    算法的核心思想是结合机器的网卡、当地时间、一个随即数来生成GUID
//    .amr音频文件
    private String generateFileName(String _id) {
        String uid = Utils.getUid();
        long timeMillis = System.currentTimeMillis();
        return "Android_"+uid+"_"+_id+"_"+timeMillis+".mp3";
    }

//    public int getVoiceLevel(int maxLevel) {
//        if (isPrepared) {
//            //获得最大的振幅getMaxAmplitude() 1-32767
//            try {
//                return maxLevel * mMediaRecorder.getMaxAmplitude()/32768+1;
//            } catch (Exception e) {
//
//            }
//        }
//        return 1;
//    }


    public void release() {
        recordManager.stop();
    }

    public void cancel(){
        release();
        if(mCurrentFilePath!=null) {
            File file = new File(mCurrentFilePath);
            file.delete();
            mCurrentFilePath = null;
        }
    }
    public String getCurrentFilePath() {
        if (recordManager.getFilePath() != null){
            return recordManager.getFilePath().getAbsolutePath();
        }else {
            return null;
        }
    }
}
