package com.module.community.model.bean;

import com.module.my.model.bean.MyFansData;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2018/7/11.
 */
public class CommunityPostData {

    private ArrayList<BBsListData550> data;
    private ArrayList<MyFansData> interest;

    public ArrayList<BBsListData550> getData() {
        return data;
    }

    public void setData(ArrayList<BBsListData550> data) {
        this.data = data;
    }

    public ArrayList<MyFansData> getInterest() {
        return interest;
    }

    public void setInterest(ArrayList<MyFansData> interest) {
        this.interest = interest;
    }
}
