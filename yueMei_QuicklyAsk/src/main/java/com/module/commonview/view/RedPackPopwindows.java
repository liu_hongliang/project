package com.module.commonview.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.commonview.module.bean.MessageBean;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class RedPackPopwindows extends PopupWindow {

    public RedPackPopwindows(Context mContext, View parent, List<MessageBean.CouponsBean> couponsBeans) {
        final View view = View.inflate(mContext, R.layout.pop_redpack, null);
        view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();
        RecyclerView chatList = view.findViewById(R.id.chat_pop_list);
        RelativeLayout container = view.findViewById(R.id.all_titttt);
        ViewGroup.LayoutParams layoutParams = container.getLayoutParams();
        if (couponsBeans.size() == 1){
            layoutParams.height=Utils.dip2px(220);
        }else {
            layoutParams.height=Utils.dip2px(333);
        }
        layoutParams.width=Utils.dip2px(286);
        container.setLayoutParams(layoutParams);
        RelativeLayout colseClick = view.findViewById(R.id.colse_rly);
        ChatPopAdapter chatPopAdapter = new ChatPopAdapter(R.layout.chat_pop_item, couponsBeans);
        chatList.setAdapter(chatPopAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        chatList.setLayoutManager(linearLayoutManager);
        colseClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        // 点击 之外 消失
        view.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = view.findViewById(R.id.pop_redpack_content_rly).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
}
