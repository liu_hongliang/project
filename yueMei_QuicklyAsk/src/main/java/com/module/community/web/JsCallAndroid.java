package com.module.community.web;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;

/**
 * H5调用andorid原生方法
 * Created by 裴成浩 on 2019/8/30
 */
public class JsCallAndroid {

    private String TAG = "JsCallAndroid";

    @JavascriptInterface
    public int getStatusBarHeight(Context context) {
        Log.e(TAG, "状态栏高度 === " + QMUIStatusBarHelper.getStatusbarHeight(context));
        return QMUIStatusBarHelper.getStatusbarHeight(context);
    }

    /**
     * 刷新当前页面
     */
    @JavascriptInterface
    public void refreshWebView(){
    }
}
