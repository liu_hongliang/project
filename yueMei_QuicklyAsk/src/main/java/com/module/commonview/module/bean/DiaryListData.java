package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.taodetail.model.bean.HomeTaoData;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryListData implements Parcelable {

    private String title;                           //标题
    private String id;                              //再写一页帖子id
    private String sharetime;                       //手术时间（	再写一页用）
    private String before_max_day;                  //再写一页用
    private DiaryUserdataBean userdata;             //用户信息
    private DiaryCompareBean compare;               //前后对比图
    private DiaryHosDocFanXian fanxian;        //返现（自己浏览时）
    private List<DiaryTaoData> taoData;             //下单商品列表
    private DiaryHosDocBean hosDoc;                 //做手术的医生和医院（自己浏览时）
    private DiaryRateBean rate;                     //楼主评分
    private List<DiaryOtherPostBean> other_post;    //日记推荐
    private List<HomeTaoData> taolist;               //淘推荐
    private List<DiaryRadioBean> radio;             //日记列表标签
    private DiaryVideoData video;                    //视频
    private List<PostOtherpic> pic;                 //
    private String content;
    private DiaryLocationData location;
    private String p_id;
    private String askorshare;
    private String agree_num;
    private String answer_num;
    private String collect_num;
    private List<DiaryTagList> tag;                 //标签
    private String is_collect;                      //判断是否收藏
    private List<DiaryReplyList> replyList;        //回复列表
    private ChatDataBean chatData;
    private HashMap<String,String> shareClickData;      //分享按钮点击事件统计用
    private HashMap<String,String> followClickData;     //关注按钮点击事件统计用
    private String selfPageType;                        //当前页面类型
    private String is_agree;                        //是否点赞
    private ChatDataBean bottomChatData;
    private String shareTotalNumber;


    protected DiaryListData(Parcel in) {
        title = in.readString();
        id = in.readString();
        sharetime = in.readString();
        before_max_day = in.readString();
        userdata = in.readParcelable(DiaryUserdataBean.class.getClassLoader());
        fanxian = in.readParcelable(DiaryHosDocFanXian.class.getClassLoader());
        taoData = in.createTypedArrayList(DiaryTaoData.CREATOR);
        hosDoc = in.readParcelable(DiaryHosDocBean.class.getClassLoader());
        rate = in.readParcelable(DiaryRateBean.class.getClassLoader());
        other_post = in.createTypedArrayList(DiaryOtherPostBean.CREATOR);
        taolist = in.createTypedArrayList(HomeTaoData.CREATOR);
        radio = in.createTypedArrayList(DiaryRadioBean.CREATOR);
        video = in.readParcelable(DiaryVideoData.class.getClassLoader());
        pic = in.createTypedArrayList(PostOtherpic.CREATOR);
        content = in.readString();
        location = in.readParcelable(DiaryLocationData.class.getClassLoader());
        p_id = in.readString();
        askorshare = in.readString();
        agree_num = in.readString();
        answer_num = in.readString();
        collect_num = in.readString();
        tag = in.createTypedArrayList(DiaryTagList.CREATOR);
        is_collect = in.readString();
        replyList = in.createTypedArrayList(DiaryReplyList.CREATOR);
        chatData = in.readParcelable(ChatDataBean.class.getClassLoader());
        selfPageType = in.readString();
        is_agree = in.readString();
        bottomChatData = in.readParcelable(ChatDataBean.class.getClassLoader());
        shareTotalNumber = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(id);
        dest.writeString(sharetime);
        dest.writeString(before_max_day);
        dest.writeParcelable(userdata, flags);
        dest.writeParcelable(fanxian, flags);
        dest.writeTypedList(taoData);
        dest.writeParcelable(hosDoc, flags);
        dest.writeParcelable(rate, flags);
        dest.writeTypedList(other_post);
        dest.writeTypedList(taolist);
        dest.writeTypedList(radio);
        dest.writeParcelable(video, flags);
        dest.writeTypedList(pic);
        dest.writeString(content);
        dest.writeParcelable(location, flags);
        dest.writeString(p_id);
        dest.writeString(askorshare);
        dest.writeString(agree_num);
        dest.writeString(answer_num);
        dest.writeString(collect_num);
        dest.writeTypedList(tag);
        dest.writeString(is_collect);
        dest.writeTypedList(replyList);
        dest.writeParcelable(chatData, flags);
        dest.writeString(selfPageType);
        dest.writeString(is_agree);
        dest.writeParcelable(bottomChatData, flags);
        dest.writeString(shareTotalNumber);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryListData> CREATOR = new Creator<DiaryListData>() {
        @Override
        public DiaryListData createFromParcel(Parcel in) {
            return new DiaryListData(in);
        }

        @Override
        public DiaryListData[] newArray(int size) {
            return new DiaryListData[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSharetime() {
        return sharetime;
    }

    public void setSharetime(String sharetime) {
        this.sharetime = sharetime;
    }

    public String getBefore_max_day() {
        return before_max_day;
    }

    public void setBefore_max_day(String before_max_day) {
        this.before_max_day = before_max_day;
    }

    public DiaryUserdataBean getUserdata() {
        return userdata;
    }

    public void setUserdata(DiaryUserdataBean userdata) {
        this.userdata = userdata;
    }

    public DiaryCompareBean getCompare() {
        return compare;
    }

    public void setCompare(DiaryCompareBean compare) {
        this.compare = compare;
    }

    public DiaryHosDocFanXian getFanxian() {
        return fanxian;
    }

    public void setFanxian(DiaryHosDocFanXian fanxian) {
        this.fanxian = fanxian;
    }

    public List<DiaryTaoData> getTaoData() {
        return taoData;
    }

    public void setTaoData(List<DiaryTaoData> taoData) {
        this.taoData = taoData;
    }

    public DiaryHosDocBean getHosDoc() {
        return hosDoc;
    }

    public void setHosDoc(DiaryHosDocBean hosDoc) {
        this.hosDoc = hosDoc;
    }

    public DiaryRateBean getRate() {
        return rate;
    }

    public void setRate(DiaryRateBean rate) {
        this.rate = rate;
    }

    public List<DiaryOtherPostBean> getOther_post() {
        return other_post;
    }

    public void setOther_post(List<DiaryOtherPostBean> other_post) {
        this.other_post = other_post;
    }

    public List<HomeTaoData> getTaolist() {
        return taolist;
    }

    public void setTaolist(List<HomeTaoData> taolist) {
        this.taolist = taolist;
    }

    public List<DiaryRadioBean> getRadio() {
        return radio;
    }

    public void setRadio(List<DiaryRadioBean> radio) {
        this.radio = radio;
    }

    public DiaryVideoData getVideo() {
        return video;
    }

    public void setVideo(DiaryVideoData video) {
        this.video = video;
    }

    public List<PostOtherpic> getPic() {
        return pic;
    }

    public void setPic(List<PostOtherpic> pic) {
        this.pic = pic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public DiaryLocationData getLocation() {
        return location;
    }

    public void setLocation(DiaryLocationData location) {
        this.location = location;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getAskorshare() {
        return askorshare;
    }

    public void setAskorshare(String askorshare) {
        this.askorshare = askorshare;
    }

    public String getAgree_num() {
        return agree_num;
    }

    public void setAgree_num(String agree_num) {
        this.agree_num = agree_num;
    }

    public String getAnswer_num() {
        return answer_num;
    }

    public void setAnswer_num(String answer_num) {
        this.answer_num = answer_num;
    }

    public String getCollect_num() {
        return collect_num;
    }

    public void setCollect_num(String collect_num) {
        this.collect_num = collect_num;
    }

    public List<DiaryTagList> getTag() {
        return tag;
    }

    public void setTag(List<DiaryTagList> tag) {
        this.tag = tag;
    }

    public String getIs_collect() {
        return is_collect;
    }

    public void setIs_collect(String is_collect) {
        this.is_collect = is_collect;
    }

    public List<DiaryReplyList> getReplyList() {
        return replyList;
    }

    public void setReplyList(List<DiaryReplyList> replyList) {
        this.replyList = replyList;
    }

    public ChatDataBean getChatData() {
        return chatData;
    }

    public void setChatData(ChatDataBean chatData) {
        this.chatData = chatData;
    }

    public HashMap<String, String> getShareClickData() {
        return shareClickData;
    }

    public void setShareClickData(HashMap<String, String> shareClickData) {
        this.shareClickData = shareClickData;
    }

    public HashMap<String, String> getFollowClickData() {
        return followClickData;
    }

    public void setFollowClickData(HashMap<String, String> followClickData) {
        this.followClickData = followClickData;
    }

    public String getSelfPageType() {
        return selfPageType;
    }

    public void setSelfPageType(String selfPageType) {
        this.selfPageType = selfPageType;
    }

    public String getIs_agree() {
        return is_agree;
    }

    public void setIs_agree(String is_agree) {
        this.is_agree = is_agree;
    }

    public ChatDataBean getBottomChatData() {
        return bottomChatData;
    }

    public void setBottomChatData(ChatDataBean bottomChatData) {
        this.bottomChatData = bottomChatData;
    }

    public String getShareTotalNumber() {
        return shareTotalNumber;
    }

    public void setShareTotalNumber(String shareTotalNumber) {
        this.shareTotalNumber = shareTotalNumber;
    }
}
