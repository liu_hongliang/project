package com.module.commonview.view;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class RepaymentTextView extends LinearLayout {


    private TextView skuRepayTitle;
    private TextView skuRepayContent;

    public RepaymentTextView(Context context) {
        this(context,null);
    }

    public RepaymentTextView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public RepaymentTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.repayment_textview, this, true);
        skuRepayTitle = view.findViewById(R.id.sku_repay_title);
        skuRepayContent = view.findViewById(R.id.sku_repay_content);
    }


    public TextView getSkuRepayTitle() {
        return skuRepayTitle;
    }

    public void setSkuRepayTitle(TextView skuRepayTitle) {
        this.skuRepayTitle = skuRepayTitle;
    }

    public TextView getSkuRepayContent() {
        return skuRepayContent;
    }

    public void setSkuRepayContent(TextView skuRepayContent) {
        this.skuRepayContent = skuRepayContent;
    }
}
