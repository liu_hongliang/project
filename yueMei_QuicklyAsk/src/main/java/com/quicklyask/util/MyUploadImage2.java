package com.quicklyask.util;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.QiNiuTokenApi;
import com.module.commonview.module.bean.QiNiuBean;
import com.module.other.netWork.netWork.QiNiuConfigration;
import com.module.other.netWork.netWork.ServerData;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.quicklyask.view.ProcessImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;


/**
 * Created by 裴成浩 on 2017/7/7.
 */

public class MyUploadImage2 {
    public static final String TAG = "MyUploadImage2";
    private final File uploadFile;
    private final Activity mActivity;
    private Handler mHandler;
    private ProcessImageView mImgs;
    private final int mPos;
    private String code;
    private String imgUrl;

    public static MyUploadImage2 getMyUploadImage(Activity activity, int pos, Handler handler, String mFilePath, ProcessImageView imgs) {
        return new MyUploadImage2(activity, pos, handler, mFilePath, imgs);
    }

    public static MyUploadImage2 getMyUploadImage(Activity activity, int pos, Handler handler, String mFilePath) {
        return new MyUploadImage2(activity, pos, handler, mFilePath);
    }

    private MyUploadImage2(Activity activity, int pos, Handler handler, String mFilePath, ProcessImageView imgs) {
        this.mActivity = activity;
        this.mPos = pos;
        this.mHandler = handler;
        this.mImgs = imgs;
        uploadFile = new File(mFilePath);
    }

    private MyUploadImage2(Activity activity, int pos, Handler handler, String mFilePath) {
        this.mActivity = activity;
        this.mPos = pos;
        this.mHandler = handler;
        uploadFile = new File(mFilePath);
    }

    public void uploadImage(String key) {
        String qiniutoken = Cfg.loadStr(mActivity, "qiniutoken", "");
        UploadManager uploadManager = QiNiuConfigration.getInstance().init();
        uploadManager.put(uploadFile, key, qiniutoken, new UpCompletionHandler() {
            @Override
            public void complete(final String key, ResponseInfo info, JSONObject response) {
                if (info.isOK()) {
                    Log.e(TAG, "Upload Success");
                    sendMessage(11);
                } else {
                    try {
                        if (response != null) {
                            String error = response.getString("error");
                            if ("expired token".equals(error)) {
                                new QiNiuTokenApi().getCallBack(mActivity, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData serverData) {
                                        if ("1".equals(serverData.code)) {
                                            try {
                                                QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                                                Cfg.saveStr(mActivity, "qiniutoken", qiNiuBean.getQiniu_token());
                                                uploadImage(key);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            } else {
                                Log.e(TAG, "Upload Fail");
                                sendMessage(12);
                            }
                        } else {
                            sendMessage(12);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                Log.e(TAG, "key==" + key + "info==" + info + "response==" + response);
            }
        }, null);

    }

    private void sendMessage(int what) {
        Message msg = Message.obtain();
        msg.arg1 = mPos;
        msg.what = what;
        mHandler.sendMessage(msg);
    }
}
