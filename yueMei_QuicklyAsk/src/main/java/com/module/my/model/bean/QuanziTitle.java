package com.module.my.model.bean;

/**
 * Created by dwb on 16/3/9.
 */
public class QuanziTitle {

    private String code;
    private String message;
    private QuanziTitleData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public QuanziTitleData getData() {
        return data;
    }

    public void setData(QuanziTitleData data) {
        this.data = data;
    }
}
