/**
 * 
 */
package com.module.my.controller.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 签到开关
 * 
 * @author Rubin
 * 
 */
public class SwitchQianDaoActivity extends BaseActivity {

	@BindView(id = R.id.bother_open, click = true)
	private RelativeLayout openRly;
	@BindView(id = R.id.bother_close, click = true)
	private RelativeLayout closeRly;

	@BindView(id = R.id.bother_open_iv, click = true)
	private ImageView opneIv;
	@BindView(id = R.id.bother_close_iv, click = true)
	private ImageView closeIv;

	@BindView(id = R.id.switch_bother_top)
	private CommonTopBar mTop;// 返回

	private Context mContext;
	private String uid = "";
	private String switchFlag;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_switch_bother);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = SwitchQianDaoActivity.this;
		uid = Utils.getUid();

		mTop.setCenterText("签到开关");

		initOpen();

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						SwitchQianDaoActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@Override
	protected void initWidget() {
		super.initWidget();
	}

	void initOpen() {

		switchFlag = Cfg.loadStr(mContext, FinalConstant.SWITTCH, "");
		if (switchFlag.equals("0")) {
			opneIv.setVisibility(View.GONE);
			closeIv.setVisibility(View.VISIBLE);
		} else {
			opneIv.setVisibility(View.VISIBLE);
			closeIv.setVisibility(View.GONE);
		}

	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.bother_open:
			opneIv.setVisibility(View.VISIBLE);
			closeIv.setVisibility(View.GONE);
			Cfg.saveStr(mContext, FinalConstant.SWITTCH, "1");
			onBackPressed();
			break;
		case R.id.bother_close:
			opneIv.setVisibility(View.GONE);
			closeIv.setVisibility(View.VISIBLE);
			Cfg.saveStr(mContext, FinalConstant.SWITTCH, "0");
			onBackPressed();
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
