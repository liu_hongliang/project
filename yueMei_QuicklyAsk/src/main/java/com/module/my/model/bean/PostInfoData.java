package com.module.my.model.bean;

import java.util.List;

/**
 * 补充更新获取数据接口
 * 
 * @author dwb
 * 
 */
public class PostInfoData {

	private String service;
	private String server_id;
	private String effect;
	private String pf_doctor;
	private String docname;
	private String hosname;
	private String money;
	private String title;
	private String hos_id;
	private String doc_id;
	private List<PostInfoPic> pic;
	private String is_consumer_voucher;

	public String getIs_consumer_voucher() {
		return is_consumer_voucher;
	}

	public void setIs_consumer_voucher(String is_consumer_voucher) {
		this.is_consumer_voucher = is_consumer_voucher;
	}
	public String getServer_id() {
		return server_id;
	}

	public void setServer_id(String server_id) {
		this.server_id = server_id;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public String getPf_doctor() {
		return pf_doctor;
	}

	public void setPf_doctor(String pf_doctor) {
		this.pf_doctor = pf_doctor;
	}

	public String getDocname() {
		return docname;
	}

	public void setDocname(String docname) {
		this.docname = docname;
	}

	public String getHosname() {
		return hosname;
	}

	public void setHosname(String hosname) {
		this.hosname = hosname;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHos_id() {
		return hos_id;
	}

	public void setHos_id(String hos_id) {
		this.hos_id = hos_id;
	}

	public String getDoc_id() {
		return doc_id;
	}

	public void setDoc_id(String doc_id) {
		this.doc_id = doc_id;
	}

	public List<PostInfoPic> getPic() {
		return pic;
	}

	public void setPic(List<PostInfoPic> pic) {
		this.pic = pic;
	}
}
