package com.module.my.view.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Scroller;

import com.qmuiteam.qmui.widget.QMUIObservableScrollView;
import com.quicklyask.util.Utils;

/**
 * Vip卡片自定义组件
 * Created by 裴成浩 on 2018/9/14.
 */
public class VipScrollView extends QMUIObservableScrollView {
    private String TAG = "VipScrollView";
    private int scrollY;
    private boolean isRolling = false;  //判断当前是否是在滚动中，默认是静止的
    private MyHandler mHandler;
    private final int IS_ROLLING = 66;
    private Scroller mScroller;
    private boolean isActionDown = false;       //判断当前手指是否是按下状态

    public VipScrollView(Context context) {
        this(context, null);
    }

    public VipScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VipScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mScroller = new Scroller(context);
        mHandler = new MyHandler();
        initView();
    }

    private void initView() {
        addOnScrollChangedListener(new OnScrollChangedListener() {
            @Override
            public void onScrollChanged(QMUIObservableScrollView scrollView, int l, int t, int oldl, int oldt) {
                if (scrollY != t) {
                    scrollY = t;
                    isRolling = true;
                    mHandler.removeMessages(IS_ROLLING);
                    mHandler.sendEmptyMessageDelayed(IS_ROLLING, 100);
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                isActionDown = true;
                if (!isRolling) {
                    return false;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                isActionDown = true;
                break;
            case MotionEvent.ACTION_UP:
                isActionDown = false;
                mHandler.removeMessages(IS_ROLLING);
                isRolling = false;
                setSpringback();
                break;
        }
        return super.onTouchEvent(ev);
    }


    /**
     * 设置回弹
     */
    public void setSpringback() {
        this.post(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "scrollY === " + scrollY);
                Log.e(TAG, "Utils.dip2px(100) === " + Utils.dip2px(100));
                if (scrollY < Utils.dip2px(100)) {
                    Log.e(TAG, "回弹");
                    smoothScrollTo(0, Utils.dip2px(100));

//                    //如果不是按下的状态
//                    if (!isActionDown) {
//                        smoothScrollTo1(Utils.dip2px(100));
//                    }
                }
            }
        });
    }

//    @Override
//    public void computeScroll() {
//        super.computeScroll();
//        Log.e(TAG, "mScroller.computeScrollOffset() == " + mScroller.computeScrollOffset());
//        if (mScroller.computeScrollOffset()) {
//            scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
//            invalidate();
//        }
//    }
//
//    public void smoothScrollTo1(int dextY) {
//        int scrollY = getScrollY();
//        int delta = dextY - scrollY;
//        mScroller.startScroll(0, scrollY, 0, delta, 300);
//        invalidate();
//    }


    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case IS_ROLLING:
                    Log.e(TAG, "IS_ROLLING === ");
                    if (isRolling) {
                        isRolling = false;
                        setSpringback();
                    }
                    break;
            }
        }
    }
}
