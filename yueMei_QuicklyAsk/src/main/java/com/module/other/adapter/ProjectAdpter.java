package com.module.other.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.doctor.model.bean.GroupDiscData;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

public class ProjectAdpter extends BaseAdapter {

	private final String TAG = "ProjectAdpter";

	private List<GroupDiscData> mGroupData = new ArrayList<GroupDiscData>();
	private Context mContext;
	private LayoutInflater inflater;
	private GroupDiscData groupData;
	ViewHolder viewHolder;


	public ProjectAdpter(Context mContext, List<GroupDiscData> mGroupData) {
		this.mContext = mContext;
		this.mGroupData = mGroupData;
		inflater = LayoutInflater.from(mContext);

//		bitmapConfig = new BitmapDisplayConfig();
//		bitmapConfig.setLoadingDrawable(pic);
//		bitmapConfig.setLoadFailedDrawable(pic);
//		bitmapConfig.setAnimation(ain);
//		bitmapConfig.setBitmapConfig(Bitmap.Config.RGB_565);
//		bitmapUtils = BitmapHelp.getBitmapUtils(mContext);

	}

	static class ViewHolder {
		public TextView groupNameTV;
		public ImageView groupIv;
	}

	@Override
	public int getCount() {
		return mGroupData.size();
	}

	@Override
	public Object getItem(int position) {
		return mGroupData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "NewApi", "InlinedApi" })
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.project_part_item, null);
			viewHolder = new ViewHolder();
			viewHolder.groupNameTV = convertView
					.findViewById(R.id.project_all_list_item_name);
			viewHolder.groupIv = convertView
					.findViewById(R.id.project_item_list_img);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		groupData = mGroupData.get(position);

		viewHolder.groupNameTV.setText(groupData.getCate_name());


		Glide.with(mContext).load(groupData.getPic_url()).into(viewHolder.groupIv);

		return convertView;
	}

	public void add(List<GroupDiscData> infos) {
		mGroupData.addAll(infos);
	}
}
