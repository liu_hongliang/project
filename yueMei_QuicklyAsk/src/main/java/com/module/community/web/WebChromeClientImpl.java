package com.module.community.web;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.nfc.Tag;
import android.util.Log;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.module.MyApplication;
import com.module.community.view.YueMeiBaseDialog;
import com.quicklyask.view.MyToast;

/**
 * H5打开相机相册的回调监听
 * 进度条的回调监听
 * Created by 裴成浩 on 2019/6/11
 */
public class WebChromeClientImpl extends WebChromeClient {
    // WebView打开相机相册的请求码
    public static final int FILE_REQUEST_CODE = 0x011;
    /**
     * 进度条的回调监听
     */
    private OnWebChromeListener onWebChromeListener;

    /**
     * 打开相册 本地文件等等
     */
    private ValueCallback<Uri> uploadFile;
    private ValueCallback<Uri[]> uploadFiles;
    private Activity mActivity;

    public WebChromeClientImpl(Activity activity) {
        this.mActivity = activity;
    }

    /**
     * 进度发生改变
     */
    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        if (onWebChromeListener != null) {
            onWebChromeListener.onProgressChanged(view, newProgress);
        }
    }

    /**
     * 接收到标题
     */
    @Override
    public void onReceivedTitle(WebView view, String title) {
        if (onWebChromeListener != null) {
            onWebChromeListener.onReceivedTitle(view, title);
        }
    }

    /**
     * js提示框无回调，等同于Toast
     *
     * @param view
     * @param url     ：当前链接
     * @param message : 提示文案
     * @param result
     * @return true:android端处理，false:js处理
     */
    @Override
    public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
        result.confirm();           //js弹窗取消
        MyToast.makeTextToast2(mActivity, message, MyToast.SHOW_TIME).show();
        return true;
    }


    /**
     * 确认取消按钮的提示框有回调的操作
     *
     * @param view
     * @param url
     * @param message
     * @param result
     * @return ：true:客户端处理，flase:js处理
     */
    @Override
    public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {

            final YueMeiBaseDialog dialog = YueMeiBaseDialog.getInstance(mActivity);
            dialog.initView(message);

            if (!mActivity.isFinishing()){
                try {
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            dialog.setBtnClickListener(new YueMeiBaseDialog.BtnClickListener() {
                @Override
                public void leftBtnClick() {
                    result.cancel();
                    dialog.dismiss();
                }

                @Override
                public void rightBtnClick() {
                    result.confirm();
                    dialog.dismiss();
                }
            });

        return true;
    }

    /**
     * @param view
     * @param url
     * @param message
     * @param defaultValue
     * @param result
     * @return ：true:客户端处理，flase:js处理
     */
    @Override
    public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
        return super.onJsPrompt(view, url, message, defaultValue, result);
    }

    // For Android  >= 5.0
    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        handleFileChooser(null, filePathCallback);
        return true;
    }

    /**
     * 打开相册 本地文件等等
     */
    private void handleFileChooser(ValueCallback<Uri> uploadMsg, ValueCallback<Uri[]> filePathCallback) {
        if (mActivity == null || mActivity.isFinishing()) {
            return;
        }
        uploadFile = uploadMsg;
        uploadFiles = filePathCallback;
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        mActivity.startActivityForResult(Intent.createChooser(intent, "请选择"), FILE_REQUEST_CODE);
    }

    /**
     * Activity回调处理
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // 处理相机相册选择
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case FILE_REQUEST_CODE:
                    if (null != uploadFile) {
                        Uri result = data == null || resultCode != Activity.RESULT_OK ? null : data.getData();
                        uploadFile.onReceiveValue(result);
                        uploadFile = null;
                    }
                    if (null != uploadFiles) {
                        Uri result = data == null || resultCode != Activity.RESULT_OK ? null : data.getData();
                        uploadFiles.onReceiveValue(new Uri[]{result});
                        uploadFiles = null;
                    }
                    break;
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            if (null != uploadFile) {
                uploadFile.onReceiveValue(null);
                uploadFile = null;
            }
            if (uploadFiles != null) {
                uploadFiles.onReceiveValue(null);
                uploadFiles = null;
            }
        }else{
            WebViewActivityResult.getInstance().onActivityResult(mActivity,requestCode,resultCode,data);
        }
    }


    // 页面标题、加载进度回调监听接口
    public interface OnWebChromeListener {
        void onReceivedTitle(WebView view, String title);

        void onProgressChanged(WebView view, int newProgress);
    }

    public void setOnWebChromeListener(OnWebChromeListener onWebChromeListener) {
        this.onWebChromeListener = onWebChromeListener;
    }

}