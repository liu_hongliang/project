package com.quicklyask.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.quicklyask.activity.R;

/**
 * 解决H5横滑幻灯片的
 * 
 * @author dwb
 * 
 */
public class MyElasticScrollView1 extends ScrollView {
	private final static int RELEASE_To_REFRESH1 = 0;
	private final static int PULL_To_REFRESH1 = 1;
	private final static int REFRESHING = 2;
	private final static int DONE = 3;
	private final static int LOADING = 4;

	private final static int RATIO = 3;

	private int headContentWidth;
	private int headContentHeight;

	private LinearLayout innerLayout;
	private LinearLayout headView;
	private ImageView arrowImageView;
	private ProgressBar progressBar;
	private TextView tipsTextview;
	private TextView lastUpdatedTextView;
	private OnRefreshListener2 refreshListener;
	private boolean isRefreshable;
	private int state;
	private boolean isBack;

	private RotateAnimation animation;
	private RotateAnimation reverseAnimation;

	private boolean canReturn;
	private boolean isRecored;
	private int startY;
	private int startX;
	private LayoutInflater inflater;

	public static int loac_scrol;

	private ImageView sxIv;
	private TextView sxTv;
	private AnimationDrawable animationDrawable;

	private float FistXLocation;
	private float FistYlocation;
	private boolean Istrigger = false;
	private final int TRIGER_LENTH = 10;
	private final int HORIZOTAL_LENTH = 20;

	public MyElasticScrollView1(Context context) {
		super(context);
		inflater = LayoutInflater.from(context);
	}

	public MyElasticScrollView1(Context context, AttributeSet attrs) {
		super(context, attrs);
		inflater = LayoutInflater.from(context);
	}

	public void GetLinearLayout(LinearLayout f_LinearLayout) {
		init(f_LinearLayout);
	}

	private void init(LinearLayout f_LinearLayout) {

		innerLayout = f_LinearLayout;

		headView = (LinearLayout) inflater.inflate(R.layout.mylistview_head2,
				null);

		arrowImageView = headView
				.findViewById(R.id.head_arrowImageView);
		progressBar = headView
				.findViewById(R.id.head_progressBar);
		tipsTextview = headView.findViewById(R.id.head_tipsTextView);
		lastUpdatedTextView = headView
				.findViewById(R.id.head_lastUpdatedTextView);

		sxIv = headView.findViewById(R.id.sx_iv);
		sxTv = headView.findViewById(R.id.sx_tv);

		measureView(headView);

		headContentHeight = headView.getMeasuredHeight();

		headContentWidth = headView.getMeasuredWidth();
		headView.setPadding(0, -1 * headContentHeight, 0, 0);
		headView.invalidate();

		innerLayout.addView(headView);

		animation = new RotateAnimation(0, -180,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		animation.setInterpolator(new LinearInterpolator());
		animation.setDuration(250);
		animation.setFillAfter(true);

		reverseAnimation = new RotateAnimation(-180, 0,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		reverseAnimation.setInterpolator(new LinearInterpolator());
		reverseAnimation.setDuration(200);
		reverseAnimation.setFillAfter(true);

		state = DONE;
		isRefreshable = false;
		canReturn = false;

	}

	@SuppressLint("NewApi")
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		int deltaX = 0;
		int deltaY = 0;

		final float x = ev.getX();
		final float y = ev.getY();
		switch (ev.getAction()) {
		case MotionEvent.ACTION_MOVE:
			deltaX = (int) (FistXLocation - x);
			deltaY = (int) (FistYlocation - y);
			if (Math.abs(deltaY) > TRIGER_LENTH
					&& Math.abs(deltaX) < HORIZOTAL_LENTH) {

				Istrigger = true;
				return super.onInterceptTouchEvent(ev);
			}

			return false;// 没有触发拦截条件，不拦截事件，继续分发至viewpager

		case MotionEvent.ACTION_DOWN:
			FistXLocation = x;
			FistYlocation = y;
			if (getScaleY() < -400) {
				System.out.println(getScaleY());
			}
			requestDisallowInterceptTouchEvent(false);
			return super.onInterceptTouchEvent(ev);

		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			if (Istrigger) {
				Istrigger = false;
				return super.onInterceptTouchEvent(ev);
			}
			break;
		}
		return super.onInterceptTouchEvent(ev);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return super.dispatchTouchEvent(ev);
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (isRefreshable) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (getScrollY() == 0 && !isRecored) {
					isRecored = true;
					startY = (int) event.getY();
					startX = (int) event.getX();
				}
				break;
			case MotionEvent.ACTION_UP:
				if (state != REFRESHING && state != LOADING) {
					if (state == DONE) {

					}
					if (state == PULL_To_REFRESH1) {
						state = DONE;
						changeHeaderViewByState();
					}
					if (state == RELEASE_To_REFRESH1) {
						state = REFRESHING;
						changeHeaderViewByState();
						onRefresh();
					}
				}
				isRecored = false;
				isBack = false;

				break;
			case MotionEvent.ACTION_MOVE:

				// Log.e("ACTION_MOVE", "event.getY()==" + event.getY() + "");
				// Log.e("ACTION_MOVE", "event.getX()==" + event.getX() + "");

				int tempY = (int) event.getY();
				if (!isRecored && getScrollY() == 0) {
					isRecored = true;
					startY = tempY;
				}

				if (state != REFRESHING && isRecored && state != LOADING) {

					if (state == RELEASE_To_REFRESH1) {
						canReturn = true;

						if (((tempY - startY) / RATIO < headContentHeight)
								&& (tempY - startY) > 0) {
							state = PULL_To_REFRESH1;
							changeHeaderViewByState();
						}

						else if (tempY - startY <= 0) {
							state = DONE;
							changeHeaderViewByState();
						} else {

						}
					}
					if (state == PULL_To_REFRESH1) {
						canReturn = true;

						if ((tempY - startY) / RATIO >= headContentHeight) {
							state = RELEASE_To_REFRESH1;
							isBack = true;
							changeHeaderViewByState();
						} else if (tempY - startY <= 0) {
							state = DONE;
							changeHeaderViewByState();
						}
					}

					if (state == DONE) {
						if (tempY - startY > 0) {
							state = PULL_To_REFRESH1;
							changeHeaderViewByState();
						}
					}

					if (state == PULL_To_REFRESH1) {
						headView.setPadding(0, -1 * headContentHeight
								+ (tempY - startY) / RATIO, 0, 0);

					}

					if (state == RELEASE_To_REFRESH1) {
						headView.setPadding(0, (tempY - startY) / RATIO
								- headContentHeight, 0, 0);
					}
					if (canReturn) {
						canReturn = false;
						return true;
					}
				}
				break;
			}
		}
		return super.onTouchEvent(event);
	}

	//
	private void changeHeaderViewByState() {
		switch (state) {
		case RELEASE_To_REFRESH1:
			arrowImageView.setVisibility(View.VISIBLE);
			progressBar.setVisibility(View.GONE);
			tipsTextview.setVisibility(View.VISIBLE);
			lastUpdatedTextView.setVisibility(View.VISIBLE);

			arrowImageView.clearAnimation();
			arrowImageView.startAnimation(animation);

			tipsTextview.setText("松开可以更新");

			sxTv.setText("释放刷新");
			break;
		case PULL_To_REFRESH1:
			progressBar.setVisibility(View.GONE);
			tipsTextview.setVisibility(View.VISIBLE);
			lastUpdatedTextView.setVisibility(View.VISIBLE);
			arrowImageView.clearAnimation();
			arrowImageView.setVisibility(View.VISIBLE);

			if (isBack) {
				isBack = false;
				arrowImageView.clearAnimation();
				arrowImageView.startAnimation(reverseAnimation);

				tipsTextview.setText("下拉可以更新");
			} else {
				tipsTextview.setText("下拉可以更新");
			}

			sxTv.setText("下拉刷新");
			sxIv.clearAnimation();
			sxIv.setImageResource(R.drawable.sx_three_mao_start);
			animationDrawable = (AnimationDrawable) sxIv.getDrawable();
			animationDrawable.start();

			break;

		case REFRESHING:

			headView.setPadding(0, 0, 0, 0);

			progressBar.setVisibility(View.VISIBLE);
			arrowImageView.clearAnimation();
			arrowImageView.setVisibility(View.GONE);
			tipsTextview.setText("正在更新...");
			lastUpdatedTextView.setVisibility(View.VISIBLE);

			sxTv.setText("正在刷新");
			sxIv.clearAnimation();
			sxIv.setImageResource(R.drawable.sx_three_mao);
			animationDrawable = (AnimationDrawable) sxIv.getDrawable();
			animationDrawable.start();

			startAction();

			break;
		case DONE:
			try {

				headView.setPadding(0, -1 * headContentHeight, 0, 0);

				progressBar.setVisibility(View.GONE);
				arrowImageView.clearAnimation();
				arrowImageView.setImageResource(R.drawable.goicon);
				tipsTextview.setText("下拉可以更新");
				lastUpdatedTextView.setVisibility(View.VISIBLE);

				sxTv.setText("下拉刷新");
				sxIv.clearAnimation();
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
	}

	@SuppressWarnings("deprecation")
	private void measureView(View child) {
		ViewGroup.LayoutParams p = child.getLayoutParams();
		if (p == null) {
			p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
		}
		int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0 + 0, p.width);
		int lpHeight = p.height;
		int childHeightSpec;
		if (lpHeight > 0) {
			childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight,
					MeasureSpec.EXACTLY);
		} else {
			childHeightSpec = MeasureSpec.makeMeasureSpec(0,
					MeasureSpec.UNSPECIFIED);
		}
		child.measure(childWidthSpec, childHeightSpec);
	}

	public void setonRefreshListener(OnRefreshListener2 refreshListener) {
		this.refreshListener = refreshListener;
		isRefreshable = true;
	}

	public interface OnRefreshListener2 {
		void onRefresh();
	}

	public void onRefreshComplete() {
		state = DONE;
		// Log.d("999999999999999999", lastUpdatedTextView.toString());
		// lastUpdatedTextView.setText("更新时间：" + new Date().toLocaleString());
		// changeHeaderViewByState();
		invalidate();
		scrollTo(0, 0);
	}

	private void onRefresh() {
		if (refreshListener != null) {
			// refreshListener.onRefresh();

			new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

				@Override
				public void onTick(long millisUntilFinished) {

				}

				@Override
				public void onFinish() {
					refreshListener.onRefresh();
				}
			}.start();
		}
	}

	public void addChild(View child) {
		innerLayout.addView(child);
	}

	public void addChild(View child, int position) {
		innerLayout.addView(child, position);
	}

	// du_add
	public void startAction() {

	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		loac_scrol = t;
		// Log.e("onScrollChanged", "onScrollChanged==||t==" + t);
	}

}
