package com.module.my.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class TaoDataInfo7043 {

    /**
     * source : 168
     * objid : 0
     * tao_id : 96226
     * number : 1
     */

    private String source;
    private String objid;
    private String tao_id;
    private String number;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getObjid() {
        return objid;
    }

    public void setObjid(String objid) {
        this.objid = objid;
    }

    public String getTao_id() {
        return tao_id;
    }

    public void setTao_id(String tao_id) {
        this.tao_id = tao_id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
