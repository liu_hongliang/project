/**
 * 
 */
package com.module.doctor.model.bean;


import com.module.commonview.module.bean.ShareWechat;

/**
 * 医生分享类
 * 
 * @author Robin
 * 
 */
public class DocShareData {
	private String _id;
	private String doc_name;
	private String hos_name;
	private String range;
	private String img;
	private String url;
	private ShareWechat Wechat;

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the doc_name
	 */
	public String getDoc_name() {
		return doc_name;
	}

	/**
	 * @param doc_name
	 *            the doc_name to set
	 */
	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	/**
	 * @return the hos_name
	 */
	public String getHos_name() {
		return hos_name;
	}

	/**
	 * @param hos_name
	 *            the hos_name to set
	 */
	public void setHos_name(String hos_name) {
		this.hos_name = hos_name;
	}

	/**
	 * @return the range
	 */
	public String getRange() {
		return range;
	}

	/**
	 * @param range
	 *            the range to set
	 */
	public void setRange(String range) {
		this.range = range;
	}

	/**
	 * @return the img
	 */
	public String getImg() {
		return img;
	}

	/**
	 * @param img
	 *            the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	public ShareWechat getWechat() {
		return Wechat;
	}

	public void setWechat(ShareWechat wechat) {
		Wechat = wechat;
	}
}
