package com.quicklyask.entity;

import com.module.home.model.bean.BaikeSearchItemData;

import java.util.List;

public class BaikeSearchItem {

	private String code;
	private String message;
	private List<BaikeSearchItemData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<BaikeSearchItemData> getData() {
		return data;
	}

	public void setData(List<BaikeSearchItemData> data) {
		this.data = data;
	}

}
